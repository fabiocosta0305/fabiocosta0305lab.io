#!/bin/sh

PROGRESS_CURR=0
PROGRESS_TOTAL=84                          

# This file was autowritten by rmlint
# rmlint was executed from: /home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/
# Your command line was: rmlint -vvvv

RMLINT_BINARY="/usr/bin/rmlint"

# In special cases --equal needs special args to mimic
# the behaviour that lead to finding the duplicates below.
RMLINT_EQUAL_EXTRA_ARGS=" --no-followlinks"

USER='19219948869'
GROUP='SERPRO_RLSL'

# Set to true on -n
DO_DRY_RUN=

# Set to true on -p
DO_PARANOID_CHECK=

# Set to true on -r
DO_CLONE_READONLY=

# Set to true on -q
DO_SHOW_PROGRESS=true

# Set to true on -c
DO_DELETE_EMPTY_DIRS=

##################################
# GENERAL LINT HANDLER FUNCTIONS #
##################################

COL_RED='[0;31m'
COL_BLUE='[1;34m'
COL_GREEN='[0;32m'
COL_YELLOW='[0;33m'
COL_RESET='[0m'

print_progress_prefix() {
    if [ -n "$DO_SHOW_PROGRESS" ]; then
        PROGRESS_PERC=0
        if [ $((PROGRESS_TOTAL)) -gt 0 ]; then
            PROGRESS_PERC=$((PROGRESS_CURR * 100 / PROGRESS_TOTAL))
        fi
        printf "$COL_BLUE[% 3d%%]$COL_RESET " $PROGRESS_PERC
        PROGRESS_CURR=$((PROGRESS_CURR+1))
    fi
}

handle_emptyfile() {
    print_progress_prefix
    echo "${COL_GREEN}Deleting empty file:${COL_RESET}" "$1"
    if [ -z "$DO_DRY_RUN" ]; then
        rm -f "$1"
    fi
}

handle_emptydir() {
    print_progress_prefix
    echo "${COL_GREEN}Deleting empty directory: ${COL_RESET}" "$1"
    if [ -z "$DO_DRY_RUN" ]; then
        rmdir "$1"
    fi
}

handle_bad_symlink() {
    print_progress_prefix
    echo "${COL_GREEN} Deleting symlink pointing nowhere: ${COL_RESET}" "$1"
    if [ -z "$DO_DRY_RUN" ]; then
        rm -f "$1"
    fi
}

handle_unstripped_binary() {
    print_progress_prefix
    echo "${COL_GREEN} Stripping debug symbols of: ${COL_RESET}" "$1"
    if [ -z "$DO_DRY_RUN" ]; then
        strip -s "$1"
    fi
}

handle_bad_user_id() {
    print_progress_prefix
    echo "${COL_GREEN}chown ${USER}${COL_RESET}" "$1"
    if [ -z "$DO_DRY_RUN" ]; then
        chown "$USER" "$1"
    fi
}

handle_bad_group_id() {
    print_progress_prefix
    echo "${COL_GREEN}chgrp ${GROUP}${COL_RESET}" "$1"
    if [ -z "$DO_DRY_RUN" ]; then
        chgrp "$GROUP" "$1"
    fi
}

handle_bad_user_and_group_id() {
    print_progress_prefix
    echo "${COL_GREEN}chown ${USER}:${GROUP}${COL_RESET}" "$1"
    if [ -z "$DO_DRY_RUN" ]; then
        chown "$USER:$GROUP" "$1"
    fi
}

###############################
# DUPLICATE HANDLER FUNCTIONS #
###############################

check_for_equality() {
    if [ -f "$1" ]; then
        # Use the more lightweight builtin `cmp` for regular files:
        cmp -s "$1" "$2"
        echo $?
    else
        # Fallback to `rmlint --equal` for directories:
        $RMLINT_BINARY -pp --equal $RMLINT_EQUAL_EXTRA_ARGS "$1" "$2"
        echo $?
    fi
}

original_check() {
    if [ ! -e "$2" ]; then
        echo $COL_RED "^^^^^^ Error: original has disappeared - cancelling....." $COL_RESET
        return 1
    fi

    if [ ! -e "$1" ]; then
        echo $COL_RED "^^^^^^ Error: duplicate has disappeared - cancelling....." $COL_RESET
        return 1
    fi

    # Check they are not the exact same file (hardlinks allowed):
    if [ "$1" = "$2" ]; then
        echo $COL_RED "^^^^^^ Error: original and duplicate point to the *same* path - cancelling....." $COL_RESET
        return 1
    fi

    # Do double-check if requested:
    if [ -z "$DO_PARANOID_CHECK" ]; then
        return 0
    else
        if [ $(check_for_equality "$1" "$2") -ne 0 ]; then
            echo $COL_RED "^^^^^^ Error: files no longer identical - cancelling....." $COL_RESET
        fi
    fi
}

cp_hardlink() {
    print_progress_prefix
    echo "${COL_YELLOW}Hardlinking to original: ${COL_RESET}" "$1"
    if original_check "$1" "$2"; then
        if [ -z "$DO_DRY_RUN" ]; then
            # If it's a directory cp will create a new copy into
            # the destination path. This would lead to more wasted space...
            if [ -d "$1" ]; then
                rm -rf "$1"
            fi
            cp --remove-destination --archive --link "$2" "$1"
        fi
    fi
}

cp_symlink() {
    print_progress_prefix
    echo "${COL_YELLOW}Symlinking to original: ${COL_RESET}" "$1"
    if original_check "$1" "$2"; then
        if [ -z "$DO_DRY_RUN" ]; then
            touch -mr "$1" "$0"
            if [ -d "$1" ]; then
                rm -rf "$1"
            fi
            cp --remove-destination --archive --symbolic-link "$2" "$1"
            touch -mr "$0" "$1"
        fi
    fi
}

cp_reflink() {
    print_progress_prefix
    # reflink $1 to $2's data, preserving $1's  mtime
    echo "${COL_YELLOW}Reflinking to original: ${COL_RESET}" "$1"
    if original_check "$1" "$2"; then
        if [ -z "$DO_DRY_RUN" ]; then
            touch -mr "$1" "$0"
            if [ -d "$1" ]; then
                rm -rf "$1"
            fi
            cp --archive --reflink=always "$2" "$1"
            touch -mr "$0" "$1"
        fi
    fi
}

clone() {
    print_progress_prefix
    # clone $1 from $2's data
    echo "${COL_YELLOW}Cloning to: ${COL_RESET}" "$1"
    if [ -z "$DO_DRY_RUN" ]; then
        if [ -n "$DO_CLONE_READONLY" ]; then
            sudo $RMLINT_BINARY --btrfs-clone -r "$2" "$1"
        else
            $RMLINT_BINARY --btrfs-clone "$2" "$1"
        fi
    fi
}

skip_hardlink() {
    print_progress_prefix
    echo "${COL_BLUE}Leaving as-is (already hardlinked to original): ${COL_RESET}" "$1"
}

skip_reflink() {
    print_progress_prefix
    echo "{$COL_BLUE}Leaving as-is (already reflinked to original): ${COL_RESET}" "$1"
}

user_command() {
    print_progress_prefix
    # You can define this function to do what you want:
    echo 'no user command defined.'
}

remove_cmd() {
    print_progress_prefix
    echo "${COL_YELLOW}Deleting: ${COL_RESET}" "$1"
    if original_check "$1" "$2"; then
        if [ -z "$DO_DRY_RUN" ]; then
            rm -rf "$1"

            if [ ! -z "$DO_DELETE_EMPTY_DIRS" ]; then
                DIR=$(dirname "$1")
                while [ ! "$(ls -A $DIR)" ]; do
                    rmdir "$DIR"
                    DIR=$(dirname "$DIR")
                done
            fi
        fi
    fi
}

original_cmd() {
    print_progress_prefix
    echo "${COL_GREEN}Keeping:  ${COL_RESET}" "$1"
}

##################
# OPTION PARSING #
##################

ask() {
    cat << EOF

This script will delete certain files rmlint found.
It is highly advisable to view the script first!

Rmlint was executed in the following way:

   $ rmlint -vvvv

Execute this script with -d to disable this informational message.
Type any string to continue; CTRL-C, Enter or CTRL-D to abort immediately
EOF
    read eof_check
    if [ -z "$eof_check" ]
    then
        # Count Ctrl-D and Enter as aborted too.
        echo $COL_RED "Aborted on behalf of the user." $COL_RESET
        exit 1;
    fi
}

usage() {
    cat << EOF
usage: $0 OPTIONS

OPTIONS:

  -h   Show this message.
  -d   Do not ask before running.
  -x   Keep rmlint.sh; do not autodelete it.
  -p   Recheck that files are still identical before removing duplicates.
  -r   Allow btrfs-clone to clone to read-only snapshots. (requires sudo)
  -n   Do not perform any modifications, just print what would be done. (implies -d and -x)
  -c   Clean up empty directories while deleting duplicates.
  -q   Do not show progress.
EOF
}

DO_REMOVE=
DO_ASK=

while getopts "dhxnrpqc" OPTION
do
  case $OPTION in
     h)
       usage
       exit 1
       ;;
     d)
       DO_ASK=false
       ;;
     x)
       DO_REMOVE=false
       ;;
     n)
       DO_DRY_RUN=true
       DO_REMOVE=false
       DO_ASK=false
       ;;
     r)
       DO_CLONE_READONLY=true
       ;;
     p)
       DO_PARANOID_CHECK=true
       ;;
     c)
       DO_DELETE_EMPTY_DIRS=true
       ;;
     q)
       DO_SHOW_PROGRESS=
       ;;
  esac
done

if [ -z $DO_ASK ]
then
  usage
  ask
fi

if [ ! -z $DO_DRY_RUN  ]
then
    echo "#$COL_YELLOW ////////////////////////////////////////////////////////////" $COL_RESET
    echo "#$COL_YELLOW ///" $COL_RESET "This is only a dry run; nothing will be modified! " $COL_YELLOW "///" $COL_RESET
    echo "#$COL_YELLOW ////////////////////////////////////////////////////////////" $COL_RESET
fi

######### START OF AUTOGENERATED OUTPUT #########

original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/Gemfile.lock' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/Gemfile.lock.gitlab' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/Gemfile.lock' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/favicons/android-icon-72x72.png' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/favicons/apple-icon-72x72.png' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/favicons/android-icon-72x72.png' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/favicon.ico' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/favicons/favicon.ico' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/favicon.ico' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/favicons/android-icon-96x96.png' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/favicons/favicon-96x96.png' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/favicons/android-icon-96x96.png' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/_posts/Personagens/iHunt/2022-09-22-WellingtonBatista.md' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/_posts/Personagens/iHunt/2022-09-22-SaraMendes.md~' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/_posts/Personagens/iHunt/2022-09-22-WellingtonBatista.md' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/_posts/Personagens/iHunt/2022-09-22-CarlosRios.md~' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/_posts/Personagens/iHunt/2022-09-22-WellingtonBatista.md~' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/_posts/Personagens/iHunt/2022-09-22-CarlosRios.md~' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/_posts/Personagens/iHunt/2022-09-22-SaraMendes.md' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/_posts/Personagens/iHunt/2022-09-22-TimoteoRodrigues.md~' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/_posts/Personagens/iHunt/2022-09-22-SaraMendes.md' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/_posts/Personagens/iHunt/2022-09-22-TimoteoRodrigues.md' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/_posts/Personagens/iHunt/2022-09-22-EsperanzaGuanaes.md~' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/_posts/Personagens/iHunt/2022-09-22-TimoteoRodrigues.md' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/_posts/Personagens/iHunt/2022-09-22-ValterCeleste.md' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/_posts/Personagens/iHunt/2022-09-22-SelmaGuerreiro.md~' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/_posts/Personagens/iHunt/2022-09-22-ValterCeleste.md' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/_posts/Personagens/iHunt/2022-09-22-EsperanzaGuanaes.md' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/_posts/Personagens/iHunt/2022-09-22-ValterCeleste.md~' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/_posts/Personagens/iHunt/2022-09-22-EsperanzaGuanaes.md' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/Dia_Nacional_do_RPG-Materiais/Dry_Fate-Ficha.odt' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/DryFate/Dry Fate - Ficha.odt' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/Dia_Nacional_do_RPG-Materiais/Dry_Fate-Ficha.odt' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/Dia_Nacional_do_RPG-Materiais/Dry_Fate-Ficha.pdf' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/DryFate/Dry Fate - Ficha.pdf' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/Dia_Nacional_do_RPG-Materiais/Dry_Fate-Ficha.pdf' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/Dia_Nacional_do_RPG-Materiais/Dry_Fate.md' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/DryFate/Dry Fate.md' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/Dia_Nacional_do_RPG-Materiais/Dry_Fate.md' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/favicons/android-icon-144x144.png' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/favicons/apple-icon-144x144.png' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/favicons/android-icon-144x144.png' # duplicate
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/favicons/ms-icon-144x144.png' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/favicons/android-icon-144x144.png' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/favicons/apple-icon-precomposed.png' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/favicons/apple-icon.png' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/favicons/apple-icon-precomposed.png' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/camundongos.gif' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/camundongos.gif' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/camundongos.gif' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/NotAtMyTable.jpg' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/NotAtMyTable.jpg' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/NotAtMyTable.jpg' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/AvatarCoelhoBrancoTiny.png' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/AvatarCoelhoBrancoTiny.png' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/AvatarCoelhoBrancoTiny.png' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/Zamek-Falkenstein-Galeria-01.jpg' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/Zamek-Falkenstein-Galeria-01.jpg' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/Zamek-Falkenstein-Galeria-01.jpg' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/falkenstein.jpg' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/falkenstein.jpg' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/falkenstein.jpg' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/falkdrag.JPG.jpeg' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/falkdrag.JPG.jpeg' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/falkdrag.JPG.jpeg' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/falken13.jpg' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/falken13.jpg' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/falken13.jpg' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/falkdrag.jpg' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/falkdrag.jpg' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/falkdrag.jpg' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/tumblr_o15qavstTB1qbq2txo1_1280.jpg' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/tumblr_o15qavstTB1qbq2txo1_1280.jpg' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/tumblr_o15qavstTB1qbq2txo1_1280.jpg' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/Capturado.png' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/Capturado.png' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/Capturado.png' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/1632273_1.jpg' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/presentations/aybabtu.jpg' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/1632273_1.jpg' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/falkenstein_b3ff0422-24a5-471a-b1b4-1f6bb1fbb5b8_1024x1024.PNG' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/falkenstein_b3ff0422-24a5-471a-b1b4-1f6bb1fbb5b8_1024x1024.PNG' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/falkenstein_b3ff0422-24a5-471a-b1b4-1f6bb1fbb5b8_1024x1024.PNG' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/invictus_2009_f_001.jpg' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/invictus_2009_f_001.jpg' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/invictus_2009_f_001.jpg' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/falkenstein21.png' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/falkenstein21.png' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/falkenstein21.png' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/FabioChapeleiroLoucoSP465Avatar.jpg' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/facebook_1548681191297.jpg' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/FabioChapeleiroLoucoSP465Avatar.jpg' # duplicate
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/facebook_1548681191297.jpg' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/FabioChapeleiroLoucoSP465Avatar.jpg' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/rpg-a-day-2015.jpg' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/rpg-a-day-2015.jpg' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/rpg-a-day-2015.jpg' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/morrolan_auberon.png' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/morrolan_auberon.png' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/morrolan_auberon.png' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/meuAvatar2015-orig.png' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/meuAvatar2015-orig.png' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/meuAvatar2015-orig.png' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/2160a7e6ea18a506f445dd7fd6a6c307e1190b4024f8578df4160b5cd568a576.png' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/Castelo_Falkenstein.png' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/2160a7e6ea18a506f445dd7fd6a6c307e1190b4024f8578df4160b5cd568a576.png' # duplicate
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/2160a7e6ea18a506f445dd7fd6a6c307e1190b4024f8578df4160b5cd568a576.png' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/2160a7e6ea18a506f445dd7fd6a6c307e1190b4024f8578df4160b5cd568a576.png' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/rpg-a-day-2014.jpg' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/rpg-a-day-2014.jpg' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/rpg-a-day-2014.jpg' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/18f5b4df6ef0c72a0884b01d0506867fe9dffbb64ae729ac38e6d641bdc4c4f2.png' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/baviera.png' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/18f5b4df6ef0c72a0884b01d0506867fe9dffbb64ae729ac38e6d641bdc4c4f2.png' # duplicate
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/18f5b4df6ef0c72a0884b01d0506867fe9dffbb64ae729ac38e6d641bdc4c4f2.png' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/18f5b4df6ef0c72a0884b01d0506867fe9dffbb64ae729ac38e6d641bdc4c4f2.png' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/Adversario_Hostes.png' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/a84dd99cacbc1d4ffb4f315e487dab66beb2b3e8664ea292543fdbfd845c4b75.png' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/Adversario_Hostes.png' # duplicate
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/a84dd99cacbc1d4ffb4f315e487dab66beb2b3e8664ea292543fdbfd845c4b75.png' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/Adversario_Hostes.png' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/FabioChapeleiroLoucoSP465.jpeg' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/FabioChapeleiroLoucoSP465.jpg' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/FabioChapeleiroLoucoSP465.jpeg' # duplicate
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/FabioChapeleiroLoucoSP465.jpeg' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/FabioChapeleiroLoucoSP465.jpeg' # duplicate
original_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/AvatarCoelhoBranco.png' # original
remove_cmd '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/images/AvatarCoelhoBranco.png' '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/assets/img/AvatarCoelhoBranco.png' # duplicate
                                               
                                               
                                               
######### END OF AUTOGENERATED OUTPUT #########
                                               
if [ $PROGRESS_CURR -le $PROGRESS_TOTAL ]; then
    print_progress_prefix                      
    echo "${COL_BLUE}Done!${COL_RESET}"      
fi                                             
                                               
if [ -z $DO_REMOVE ] && [ -z $DO_DRY_RUN ]     
then                                           
  echo "Deleting script " "$0"             
  rm -f '/home/19219948869/Documentos/Personal/fabiocosta0305.gitlab.io/rmlint.sh';                                     
fi                                             
