---
subheadline: A Desenho Policial Rachadora do Distrito de Polícia da Desenholândia
title: Samantha “Sammy” Fastti
layout: personagens
categories:
  - personagens
tags:
  - "1º Distrito da Desenholândia"
header: no
language: br
---

Maserati Mistral de Desenho, Samantha Fastti (pronuncia-se Fas-tii) é a especialista em perseguição do 1° Distrito da Desenholândia. Adora espaguete, óleo de motor e gasolina. Nunca é vista sem seus pneus Pirelli legítimos. Entrou para a polícia após ter sido pega como rachadora e condenada a prestar serviços comunitários no Distrito da Desenholândia, o que acabou gostando de fazer. Pode ser guiada por outros, mas em geral não gosta quando _"Metem a mão boba nela"_. Normalmente aceita pessoas dentro dela.

## Aspectos

|           **Tipo** | **Aspecto**                                                           |
|-------------------:|-----------------------------------------------------------------------|
| __Alto-Conceito:__ | Maserati Mistral Oficial (DE VERDADE) do 1° Distrito da Desenholândia |
|      __Problema:__ | Rápida para correr, lenta para frear                                  |
|                    | Indianápolis, Talladega e Daytona                                     |
|                    | Orgulhosa quanto a seu design esportivo                               |
|                    | _“Pneus Pirelli, sempre...”_                                          |

## Abordagens

|  **Abordagem** | **Nível**     |
|---------------:|---------------|
|   __Pujança:__ | Médio (+1)    |
| __Ligeireza:__ | Bom (+3)      |
|   __Argúcia:__ | Razoável (+2) |
|     __Logro:__ | Médio (+1)    |

## Manobras: [Reforço: 2]

+ ***As Curvas de Maranello:*** +2 em *Defesas* com *Ligeireza* para se esquivar de ataques, desde que tenha distância o bastante realizar manobras 
+ ***Ataque de Carga:*** +3 em Ataques com *Ligeireza*, desde que tenha distância o bastante para ganhar velocidade. Em caso de Falha, o Ataque se torna ***Indiscriminado*** e todos os personagens na mesma Zona (incluindo a própria) devem se defender do mesmo Esforço obtido ou sua *Ligeireza* (incluindo o bônus do Ataque da Manobra), o que for maior
