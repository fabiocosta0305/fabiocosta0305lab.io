---
subheadline: O Saxofonista do 1º Distrito da Desenholândia
title: Terrance "Terry" Jones
layout: personagens
categories:
  - personagens
tags:
  - "1º Distrito da Desenholândia"
header: no
language: br
---

Oficial Negro do 1° Distrito de Polícia da Desenholândia, é aplicado ao trabalho, mas foi parar no distrito por preconceito na força policial. Também é reconhecido como saxofonista pelos poucos que o ouvem

## Aspectos

|           **Tipo** | **Aspecto**                                                             |
|-------------------:|-------------------------------------------------------------------------|
| __Alto-Conceito:__ | Policial do Primeiro Distrito da Desenholândia                          |
|      __Problema:__ | _"Sei o que é sofrer... Portanto prefiro não fazer os outros sofrerem"_ |
|                    | O Negão do Grupo - Vítima de preconceito com quem não o conhece         |
|                    | Talento desconhecido - Saxofonista de Jazz e _Bebop_                    |
|                    | _"Só compreensão permite entender, e só entender permite resolver"_     |

## Abordagens

|  **Abordagem** | **Nível**     |
|---------------:|---------------|
| __Cauteloso:__ | Bom (+3)      |
|     __Sagaz:__ | Médio (+1)    |
|    __Ousado:__ | Razoável (+2) |
|  __Violento:__ | Medíocre (+0) |
|     __Veloz:__ | Razoável (+2) |
|   __Furtivo:__ | Médio (+1)    |

## Manobras: [Reforço: 3]

+ ***Voz da Tolerância:*** +2 ao *Superar* de *Maneira Cautelosa* problemas ao acalmar ânimos;
+ ***Policial bom:*** +2 ao *Criar Vantagens* de *Maneira Cautelosa* para obter informações durante interrogatórios;
+ ***Percepção Extrema:*** +2 ao *Criar Vantagens* de *Maneira Cautelosa* para descobrir pistas em cenas de crime;
