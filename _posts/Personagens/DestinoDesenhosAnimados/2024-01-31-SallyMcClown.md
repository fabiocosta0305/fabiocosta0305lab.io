---
subheadline: A Policial Palhaça do Distrito de Polícia da Desenholândia
title: Sally “Silly Sally” McClown
layout: personagens
categories:
  - personagens
tags:
  - "1º Distrito da Desenholândia"
header: no
language: br
---

Palhaça de desenho animado, seu sonho era estar no _Ringling Bros. Barnum and Bailey Circus_. Mas ela na realidade não é tão engraçada quanto imagina. Apesar disso, é uma espécie de “mãezona” dos Policiais da Primeira Delegacia de Polícia da Desenholândia e sabe manter a cabeça no lugar quando não pode ser engraçada, pois isso pode comprometer suas tarefas. Possui aparência de um “ser humano de desenho”

## Aspectos

|           **Tipo** | **Aspecto**                                                                   |
|-------------------:|-------------------------------------------------------------------------------|
| __Alto-Conceito:__ | Palhaça de Desenho Animado Policial da Desenholândia                          |
|      __Problema:__ | Piadas do Senhor Kaioh - Na realidade não é tão engraçada quanto pensa que é  |
|                    | O Sonho do Ringling Bros, Barnum and Bailey Circus                            |
|                    | _“Existe hora e lugar certos para ser engraçado e bobo, E NÃO ESTAMOS NELA!”_ |
|                    | Os mais diversos cacarecos em suas roupas                                     |

## Abordagens

|  **Abordagem** | **Nível**     |
|---------------:|---------------|
|   __Pujança:__ | Médio (+1)    |
| __Ligeireza:__ | Bom (+3)      |
|   __Argúcia:__ | Razoável (+2) |
|     __Logro:__ | Médio (+1)    |

## Manobras: [Reforço: 1]

+ ***Bolsos das Maravilhas:*** *uma vez por sessão* pode utilizar seus Bolsos das Maravilhas sem usar Pontos de Destino para retirar qualquer objeto útil, sob autorização do Mestre, como um ***Aspecto***. Usos adicionais seguem a regra padrão (1 Ponto de Destino por uso)
+ ***Faro (LITERAL) da Verdade:*** +2 para *Superar Obstáculo* com *Argúcia* quando alguém tenta mentir para ela
+ ***Quando ser engraçada?:*** - +2 ao *Criar Vantagens* usando *Logro* envolvendo *Divertir os Outros* (mesmo que esse não seja o objetivo inicial)
