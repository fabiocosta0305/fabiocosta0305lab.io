---
subheadline: O Batedor Aéreo Desenho do Distrito de Polícia da Desenholândia
title: Till Lasker
layout: personagens
categories:
  - personagens
tags:
  - "1º Distrito da Desenholândia"
header: no
language: br
---

Águia de desenho animado, é considerado peça chave no 1° Distrito da Desenholândia por ser muito hábil voando, e é bem convencido quanto a isso. Em compensação, em terra é tão ágil quanto uma galinha. É um narcisista que se preocupa muito com sua aparência e limpeza de seu uniforme e equipamento feito sob medida 

## Aspectos

|           **Tipo** | **Aspecto**                                          |
|-------------------:|------------------------------------------------------|
| __Alto-Conceito:__ | Águia Policial do Primeiro Distrito da Desenholândia |
|      __Problema:__ | Voa como águia, anda como galinha                    |
|                    | Arrogante quanto a própria aparência                 |
|                    | Equipamento próprio para o tamanho                   |
|                    | _"Deixa comigo, eu vou encontrar!"_                  |

## Abordagens

|  **Abordagem** | **Nível**     |
|---------------:|---------------|
|   __Pujança:__ | Médio (+1)    |
| __Ligeireza:__ | Razoável (+2) |
|   __Argúcia:__ | Bom (+3)      |
|     __Logro:__ | Médio (+1)    |

## Manobras: [Reforço: 2]

+ ***Ataque de Carga:*** +2 ao *Atacar* com *Pujança*, desde que tenha espaço o bastante para manter-se voando de maneira adequada;
+ ***Visão do predador:*** +2 ao *Criar Vantagens* com *Argúcia* para localizar pessoas;
