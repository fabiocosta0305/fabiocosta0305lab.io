---
subheadline: A Oficial Recepcionista do 1º Distrito da Desenholândia
title: Manny Lindberg
layout: personagens
categories:
  - personagens
tags:
  - "1º Distrito da Desenholândia"
header: no
language: br
---

Policial recepcionista do 1° Distrito Policial da Desenholândia, é um tanto incompetente para o trabalho de rua, mas muito eficiente no trabalho burocrático. É bastante responsável e sabe como organizar tudo direitinho. Tem um complexo de Cinderela e ainda sonha com seu Príncipe Encantado. É uma grande mexeriqueira, conhecendo todas as fofocas quentes da Desenholândia.

## Aspectos

|           **Tipo** | **Aspecto**                                                                     |
|-------------------:|---------------------------------------------------------------------------------|
| __Alto-Conceito:__ | Policial Recepcionista do Distrito da Desenholândia                             |
|      __Problema:__ | Complexo de Cinderela - Romântica ao Extremo                                    |
|                    | Mexeriqueira, conhece todas as fofocas quentes e todas as pessoas interessantes |
|                    | Qualquer coisa vira um banquete nas minhas mãos                                 |
|                    | Ótima no Distrito, Péssima nas Ruas                                             |

## Abordagens

|  **Abordagem** | **Nível**     |
|---------------:|---------------|
| __Cauteloso:__ | Bom (+3)      |
|     __Sagaz:__ | Razoável (+2) |
|    __Ousado:__ | Medíocre (+0) |
|  __Violento:__ | Médio (+1)    |
|     __Veloz:__ | Médio (+1)    |
|   __Furtivo:__ | Razoável (+2) |

## Manobras: [Reforço: 3]

+ ***Benção de Epícuro:*** +2 ao *Criar Vantagens* de maneira *Sagaz* usando comida
+ ***Tudo no seu devido lugar:*** +2 ao *Criar Vantagens* de maneira *Cautelosa* ao lembrar-se de coisas ou locais
+ ***Meu caderninho de fofocas:*** +2 ao *Criar Vantagens* de maneira *Furtiva* ao se lembrar de alguma fofoca quente sobre alguém
