---
subheadline: O Policial Humano virando Coelho do 1º Distrito da Desenholândia
title: Cody O'Mara
layout: personagens
categories:
  - personagens
tags:
  - "1º Distrito da Desenholândia"
header: no
language: br
---

Cody é um policial que foi o primeiro policial a trabalhar na Desenholândia, antes mesmo do Distrito ser Formado. Já vive a bastante tempo junto com desenhos animados, tanto que ele já está sendo influenciado por eles e já se encontra em processo de tornar-se um desenho, tendo grandes orelhas brancas e felpudas de coelho

> **Nota:** Esse personagem já começa com uma Consequência Moderada de **Desenhificação *Orelhonas de Coelho*** sofrida. Como é por Desenhificação, não há como ela ser limpa de início, ficando a critério do Narrador como ela será removida

## Aspectos

|           **Tipo** | **Aspecto**                                             |
|-------------------:|---------------------------------------------------------|
| __Alto-Conceito:__ | Primeiro policial humano da Desenholândia               |
|      __Problema:__ | Muito tempo da Desenholândia, começou a virar um coelho |
|                    | Baladas e Canções de Bar e Bebida                       |
|                    | Kung-Fu do Bêbado                                       |
|                    | _“Amo meus cachorros!”_                                 |

## Abordagens

|  **Abordagem** | **Nível**     |
|---------------:|---------------|
| __Cauteloso:__ | Bom (+3)      |
|     __Sagaz:__ | Razoável (+2) |
|    __Ousado:__ | Medíocre (+0) |
|  __Violento:__ | Razoável (+2) |
|     __Veloz:__ | Médio (+1)    |
|   __Furtivo:__ | Médio (+1)    |

## Manobras: [Reforço: 3]

+ ***Poder da Bebida:*** Quando ***Bêbado*** recebe +3 ao *Atacar* de *Maneira Violenta* com mãos vazias (***Bêbado*** deverá ser tratado como uma *Consequência Leve*)
+ ***Voz do Banshee:*** Quando ***Bêbado*** recebe +3 ao *Atacar* de *Maneira Sagaz* usando sua cantoria horrível (***Bêbado*** deverá ser tratado como uma *Consequência Leve*)
+ ***Mas que Cachorrada! (Permissão: Aspecto de Cachorros):*** Pode usar os seus ***Cachorros*** em um *Conflito físico uma vez por sessão*
  + ***Cachorros:*** (PELO MENOS uma dúzia: `6`{: .fate_font})
  + ***Bons (+2) em:*** Morder, Obedecer Ordens, Brincar; 
  + ***Ruins (-2) em:*** Auto-Controle;
