---
subheadline: O Policial Brigão do 1º Distrito da Desenholândia
title: Alvin McDaggert
layout: personagens
categories:
  - personagens
tags:
  - "1º Distrito da Desenholândia"
header: no
language: br
---

Oficial do 1° Distrito de Polícia da Desenholândia e descendente de irlandeses. Brigão, adora confusão e não tem paciência contra o “mal”. Apesar de tudo isso, tem sempre um sorriso e uma história muito doida para as crianças

## Aspectos

|           **Tipo** | **Aspecto**                                                             |
|-------------------:|-------------------------------------------------------------------------|
| __Alto-Conceito:__ | Policial Arruaceiro e Gentil do Primeiro Distrito da Desenholândia      |
|      __Problema:__ | Sem tato e pouco paciente com "o mal"                                   |
|                    | _"O Sangue da Ilha Esmeralda aparece em meu cabelo"_                    |
|                    | Sempre com um sorriso para crianças                                     |
|                    | Última parada na Desenholândia - por um fio de ser exonerado da polícia |

## Abordagens

|  **Abordagem** | **Nível**     |
|---------------:|---------------|
| __Cauteloso:__ | Medíocre (+0) |
|     __Sagaz:__ | Médio (+1)    |
|    __Ousado:__ | Médio (+1)    |
|  __Violento:__ | Bom (+3)      |
|     __Veloz:__ | Razoável (+2) |
|   __Furtivo:__ | Razoável (+2) |

## Manobras: [Reforço: 3]

+ ***A Força de Finn McCool*** - +2 ao *Superar* de *Maneira Violenta* obstáculos Físicos;
+ ***A Honra dos Heróis*** - +2 ao *Criar Vantagens* de *Maneira Ousada* para Estabelecer Confiança;
+ ***Um Sorriso nos Lábios*** - +2 ao *Superar* de *Maneira Ousada* obstáculos relacionados a pressão e medo que outros estejam sentido
