---
subheadline: O Seco Detetive humano do 1º Distrito da Desenholândia
title: Matthew “Matty” McCormick
layout: personagens
categories:
  - personagens
tags:
  - "1º Distrito da Desenholândia"
header: no
language: br
---

Detetive do 1° Distrito Policial da Desenholândia, veste-se de maneira sempre sensata e de bom gosto, mas nunca é visto sorrindo. Costuma dizer que só está no 1° Distrito Policial da Desenholândia porque foi forçado

## Aspectos

|           **Tipo** | **Aspecto**                                                            |
|-------------------:|------------------------------------------------------------------------|
| __Alto-Conceito:__ | Detetive sensato do Departamento de Polícia de Los Angeles             |
|      __Problema:__ | _“Estou aqui contra a minha vontade”_                                  |
|                    | Sério demais, tem dificuldade ao lidar com as travessuras dos desenhos |
|                    | _“Leis são para todos”_                                                |
|                    | Rusgas com os grandões do Departamento                                 |

## Abordagens

|  **Abordagem** | **Nível**     |
|---------------:|---------------|
| __Cauteloso:__ | Razoável (+2) |
|     __Sagaz:__ | Bom (+3)      |
|    __Ousado:__ | Medíocre (+0) |
|  __Violento:__ | Razoável (+2) |
|     __Veloz:__ | Médio (+1)    |
|   __Furtivo:__ | Médio (+1)    |

## Manobras: [Reforço: 3]

+ ***Lógica Fria:*** pode *uma vez por sessão* anular o uso de Física de Desenho Animado por uma cena ao custo de 1 Ponto de Destino;
+ ***A Força da Lei:*** +2 ao *Criar Vantagens* de *maneira Sagaz* usando sua autoridade legal como Detetive;
