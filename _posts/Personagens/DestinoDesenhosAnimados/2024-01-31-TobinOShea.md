---
subheadline: O Sorrateiro Policial do 1º Distrito da Desenholândia
title: Tobin "Toby" O'Shea
layout: personagens
categories:
  - personagens
tags:
  - "1º Distrito da Desenholândia"
header: no
language: br
---

Oficial do 1° Distrito de Polícia da Desenholândia e descendente de irlandeses. Muitos dizem que ele é um pequeno trapaceiro, e o fato de ser muito pequeno faz com que muitos vejam nele um pequeno _leprechaun_, um travesso descendente das fadas… Mas na realidade, ele é muito mais esperto do que aparenta, fazendo-se de bobo para aproveitar-se do pensamento das pessoas e pegá-las desprevenidas, no que ele é muito bom

## Aspectos

|           **Tipo** | **Aspecto**                                                         |
|-------------------:|---------------------------------------------------------------------|
| __Alto-Conceito:__ | Matreiro policial do Primeiro Distrito da Desenholândia             |
|      __Problema:__ | Um trapaceiro honesto no coração                                    |
|                    | _“Pequeno Leprechaun”_ - As lendas Celtas                           |
|                    | Pequeno trapaceiro (1,60m), ninguém leva a sério até o pior momento |
|                    | Não é tão tolo quanto aparenta, muito pelo contrário                |

## Abordagens

|  **Abordagem** | **Nível**     |
|---------------:|---------------|
| __Cauteloso:__ | Médio (+1)    |
|     __Sagaz:__ | Razoável (+2) |
|    __Ousado:__ | Razoável (+2) |
|  __Violento:__ | Medíocre (+0) |
|     __Veloz:__ | Médio (+1)    |
|   __Furtivo:__ | Bom (+3)      |

## Manobras: [Reforço: 3]

+ ***Trapaças Lendárias*** - +2 ao *Criar Vantagens* de *maneira Furtiva* para engabelar alvos de alguma forma;
+ ***Um Sorriso nos Lábios*** - +2 ao *Superar* de *Maneira Ousada* obstáculos relacionados a pressão e medo que outros estejam sentido
+ ***Perceber Intenções*** - +2 para *me Defender* de *maneira Sagaz* de qualquer tentativa de me enganar;
