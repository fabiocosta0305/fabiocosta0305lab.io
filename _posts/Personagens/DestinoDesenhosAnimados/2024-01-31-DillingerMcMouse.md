---
subheadline: O 1° Policial Desenho do Distrito de Polícia da Desenholândia
title: Dillinger “Dilly Dally” McMouse
layout: personagens
categories:
  - personagens
tags:
  - "1º Distrito da Desenholândia"
header: no
language: br
---

Primeiro desenho oficial de polícia, Dilly Dally parece um rato comum, mas tem quase a mesma altura de um ser humano (altura de uma criança de 8 anos). Normalmente é visto apenas vestindo a farda da polícia. Quando fora de serviço, gosta de uma boa bebida, mas normalmente não fica de porre.

## Aspectos

|           **Tipo** | **Aspecto**                                                |
|-------------------:|------------------------------------------------------------|
| __Alto-Conceito:__ | Primeiro Desenho Policial (Policial MESMO)                 |
|      __Problema:__ | Alguém realmente dedicado ao trabalho                      |
|                    | Faz coisas erradas para fazer a coisa certa                |
|                    | Os Espíritos Irlandeses – Não fica de porre… Normalmente…  |
|                    | Minhas bigornas sempre atingem caras maus!                 |

## Abordagens

|  **Abordagem** | **Nível**     |
|---------------:|---------------|
|   __Pujança:__ | Médio (+1)    |
| __Ligeireza:__ | Médio (+1)    |
|   __Argúcia:__ | Razoável (+2) |
|     __Logro:__ | Bom (+3)      |

## Manobras: [Reforço: 2]

+ ***Tamanho Importa!:*** +2 para *Criar Vantagens* com *Argúcia* aproveitando-se da diferença de altura;
+ ***Trapaceiro:*** +2 ao *Criar Vantagens* com *Logro* ao recorrer à Lógica de Desenho contra caras maus;
