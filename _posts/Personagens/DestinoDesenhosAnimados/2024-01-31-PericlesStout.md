---
subheadline: O Comandante do 1° Distrito de Polícia da Desenholândia
title: Pericles Adamastor “Pinky” Stout
layout: personagens
categories:
  - personagens
tags:
  - "1º Distrito da Desenholândia"
header: no
language: br
---

Chefe do 1° Distrito Policial da Desenholândia, é pequeno e magro para a idade. Tem cara de bebê e parece muito bobo, mas quando em serviço é bastante competente. Vive a tanto tempo na Desenholândia que já é capaz de usar a Lógica de Desenho

## Aspectos

|           **Tipo** | **Aspecto**                                               |
|-------------------:|-----------------------------------------------------------|
| __Alto-Conceito:__ | Comandante do 1º Distrito de Polícia da Desenholândia     |
|      __Problema:__ | Ex-Palhaço do *Ringling Bros*, bobo e com cara de bebê    |
|                    | _“Tenho uma Família no Circo e uma na Desenholândia”_     |
|                    | Primeiro humano a se fixar como policial da Desenholândia |
|                    | Sabe recorrer (um pouco) à Lógica de Desenho              |

## Abordagens

|  **Abordagem** | **Nível**     |
|---------------:|---------------|
| __Cauteloso:__ | Médio (+1)    |
|     __Sagaz:__ | Razoável (+2) |
|    __Ousado:__ | Bom (+3)      |
|  __Violento:__ | Médio (+1)    |
|     __Veloz:__ | Razoável (+2) |
|   __Furtivo:__ | Medíocre (+0) |

## Manobras: [Reforço: 3]

+ ***Figurante de Desenho:*** pode recorrer à *Lógica de Desenho* sem custos *uma vez por sessão*, mas apenas em ***Áreas de Desenho***;
+ ***É como no Circo:*** não sofre redutor ao recorrer à *Lógica de Desenho*;
+ ***Antigos truques de circo:*** +2 para *Criar Vantagens* de maneira *Ousada*;
