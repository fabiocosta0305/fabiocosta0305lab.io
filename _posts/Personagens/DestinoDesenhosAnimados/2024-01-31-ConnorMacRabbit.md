---
subheadline: O Coelho (Insano?) Legista do Distrito de Polícia da Desenholândia
title: Connor MacRabbit
layout: personagens
categories:
  - personagens
tags:
  - "1º Distrito da Desenholândia"
header: no
language: br
---

Coelho de Desenho Animado Escocês, estudioso e conhece ciência e medicina como poucos, tanto humana quanto para desenhos. Lembra o Coelho Branco de _Alice no País nas Maravilhas_ com terno de tweed e jaleco de laboratório. É baixo (1,60m, contando os 20 cm de orelhas) e totalmente branco como a neve, excluindo os olhos rosa albinos e o focinho rosado. Fala com sotaque escocês e é sempre bem didático e articulado ao falar. Atualmente trabalha no necrotério da Primeira Delegacia de Polícia da Desenholândia.

## Aspectos

|           **Tipo** | **Aspecto**                                                           |
|-------------------:|-----------------------------------------------------------------------|
| __Alto-Conceito:__ | Coelho de Desenho Animado Médico Legista                              |
|      __Problema:__ | Um pé (pata?) perto de ser um Cientista Maluco                        |
|                    | Extremamente Caxias, faz tudo conforme tem que ser (ou assim imagina) |
|                    | _“Ah… O velho método Escocês… Nunca Falha!”_                          |
|                    | Civilizado e Sociável não são a mesma coisa                           |

## Abordagens

|  **Abordagem** | **Nível**     |
|---------------:|---------------|
|   __Pujança:__ | Médio (+1)    |
| __Ligeireza:__ | Médio (+1)    |
|   __Argúcia:__ | Bom (+3)      |
|     __Logro:__ | Razoável (+2) |

## Manobras: [Reforço: 1]

+ ***Conhecimento Inter-Espécies:*** +2 ao *Superar* com *Argúcia* problemas ao diferenciar humanos de desenhos
+ ***CIÊNCIA!!!!!:*** +2 em *Argúcia* ao *Criar Vantagens* por meio de ***Teorias Científicas*** (não importando o quão bizarra seja)
