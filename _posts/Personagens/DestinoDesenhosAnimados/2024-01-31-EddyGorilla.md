---
subheadline: O Gorila Bate-Pau do Distrito de Polícia da Desenholândia
title: Edward “Eddy” Gorilla
layout: personagens
categories:
  - personagens
tags:
  - "1º Distrito da Desenholândia"
header: no
language: br
---

Ex-Leão de Chácara, atualmente é um policial de choque (vulgo Bate-Pau) do 1° Distrito Policial da Desenholândia. Seu objetivo é tornar-se Detetive do Distrito, no que vem sendo apoiado por Matty, que vê nisso uma garantia dele sumir dessa loucura toda. Gorila de Desenho animado pouco maior que a média (altura de jogador de basquete profissional). É apaixonado em uma coelha cantora de cabaré chamada Tina Leanna.

## Aspectos

|           **Tipo** | **Aspecto**                                                          |
|-------------------:|----------------------------------------------------------------------|
| __Alto-Conceito:__ | Gorila Bate-Pau do Distrito da Desenholândia                         |
|      __Problema:__ | Sempre alguém acha que é esperto o bastante para encarar o Eddy      |
|                    | Dedicado ao trabalho para fazer um pé de meia                        |
|                    | Apaixonado por Tina Leanna, faria tudo por ela                       |
|                    | Estudando para ser detetive, ainda que seja mais músculo que cérebro |

## Abordagens

|  **Abordagem** | **Nível**     |
|---------------:|---------------|
|   __Pujança:__ | Bom (+3)      |
| __Ligeireza:__ | Médio (+1)    |
|   __Argúcia:__ | Médio (+1)    |
|     __Logro:__ | Razoável (+2) |

## Manobras: [Reforço: 1]

+ ***Eddy Amassa!:*** +2 ao *Atacar* com *Pujança* quando *Desarmado*
+ ***Cara durão:*** +2 ao *Criar Vantagens* com *Logro* por meio de intimidação
