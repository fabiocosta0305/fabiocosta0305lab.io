---
subheadline: O Camundongo Policial Infiltrador do Distrito de Polícia da Desenholândia
title: Andraas Mousekewitz
layout: personagens
categories:
  - personagens
tags:
  - "1º Distrito da Desenholândia"
header: no
language: br
---

Camundongo de Desenho Animado, é pequeno e aparentemente fraco, mas super ágil e muito capaz de se esquivar e invadir locais que precisa. É leal acima de tudo aos desenhos e a seus companheiros do Distrito de Polícia da Desenholândia

## Aspectos

|           **Tipo** | **Aspecto**                                                            |
|-------------------:|------------------------------------------------------------------------|
| __Alto-Conceito:__ | Camundongo Policial do Primeiro Distrito da Desenholândia              |
|      __Problema:__ | _“Meus amigos vêem em primeiro lugar, junto com os demais desenhos!”_  |
|                    | Carcereiro de Gaiola – apenas 1,30cm                                   |
|                    | Muito Bom ao usar Lógica de Desenho, em especial para mudar de tamanho |
|                    | A arte da Ladinagem                                                    |

## Abordagens

|  **Abordagem** | **Nível**     |
|---------------:|---------------|
|   __Pujança:__ | Médio (+1)    |
| __Ligeireza:__ | Médio (+1)    |
|   __Argúcia:__ | Razoável (+2) |
|     __Logro:__ | Bom (+3)      |

## Manobras: [Reforço: 1]

+ ***Encolher:*** +2 ao *Criar Vantagens* com *Logro* para invadir locais mudando de tamanho, usando *1 Ponto de Destino* (voltar ao tamanho original não custa nada);
