---
title: "Príncipe Alberich Van Der Waar"
layout: personagens
categories:
  - personagens
tags:
  - Loose Threads
header: no
language: br
---

_**N.A.:**_ Segue as regras de [Loose Threads][loose-threads]

## Aspectos

|          __*Tipo:*__ | __*Aspecto*__                                                           |
|---------------------:|-------------------------------------------------------------------------|
|          _Conceito:_ | Um príncipe em busca de respostas profundas                             |
| _Desejo do Coração:_ | Desejo tornar-me um rei sábio e justo como meu pai                      |
|            _Tensão:_ | Pela Arma ou Pela mente, como devo governar? (Guerreiro/Sábio)          |
|  _Motivação/Método:_ | Às Vezes, é bom o _status_ de ser um príncipe e o que vem com ele       |
|    _Relacionamento:_ | Aubergine parece ser confiável… Desde que ele consiga permanecer focado |
|    _Relacionamento:_ | Regina é muito perigosa… Ainda bem que ela está do nosso lado           |
|    _Sétimo Aspecto:_ |                                                                         |

## Perícias

|         ***Nível*** | ***Perícia*** | ***Perícia*** | ***Perícia*** | ***Perícia*** |
|--------------------:|:-------------:|:-------------:|:-------------:|:-------------:|
|    ***Ótimo (+4)*** | Lutar         |               |               |               |
|      ***Bom (+3)*** | Conhecimento  | Comunicação   |               |               |
| ***Razoável (+2)*** | Atirar        | Empatia       | Provocar      |               |
|  ***Regular (+1)*** | Vigor         | Atletismo     | Furtividade   | Recursos      |

## Façanhas 

+ _**Mão Pesada:**_  – Quando é bem-sucedido com estilo em um ataque usando Lutar e e escolher reduzir o resultado para obter um impulso, você ganha um aspecto de situação com uma invocação grátis ao invés do impulso.
+ _**Escudo da Razão:**_  Você pode usar Conhecimento como defesa contra tentativas de Provocar, contanto que consiga justificar sua habilidade de superar o medo através de pensamento racional e razão.
+ _**Demagogo:**_  +2 em Comunicação quando fizer um discurso inspirador em frente a uma multidão (se houverem PdNs importantes ou PJs na cena, é possível atingir a todos simultaneamente com uma rolagem ao invés de dividir as tensões).

[loose-threads]: http://www.drivethrurpg.com/product/196127/Loose-Threads-o-A-World-of-Adventure-for-Fate-Core
