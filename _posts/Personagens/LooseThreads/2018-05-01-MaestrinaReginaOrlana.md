---
title: "Maestrina Regina Orlana, a Rainha Má"
layout: personagens
categories:
  - personagens
tags:
  - Loose Threads
header: no
language: br
---

_**N.A.:**_ Segue as regras de [Loose Threads][loose-threads]

## Aspectos

| __*Tipo:*__          | __*Aspecto*__                                                             |
|---------------------:|---------------------------------------------------------------------------|
|  _Conceito:_         | Ex-Rainha Má em busca de Redenção                                         |
| _Desejo do Coração:_ | Eu desejo encontrar um local onde possa viver em Paz                      | 
| _Tensão:_            | O Poder é um Vício que me puxa para fora do caminho da Paz (Poder/Paz)    | 
| _Motivação/Método:_  | As antigas formas de se resolver as coisas ainda funcionam                | 
| _Relacionamento:_    | Todos são tão inocentes… Ao menos uma pessoa tem que ver a verdade        | 
| _Relacionamento:_    | O Grilo Falante do Ceticismo                                              | 
| _Sétimo Aspecto:_    |                                                                           | 

## Perícias

| ***Nível***         | ***Perícia*** | ***Perícia***   | ***Perícia*** | ***Perícia*** |
|--------------------:|:-------------:|:---------------:|:-------------:|:-------------:|
| ***Ótimo (+4)***    | Magia (Zaps)  |                 |               |               |
| ***Bom (+3)***      | Vontade       | Conhecimentos   |               |               |
| ***Razoável (+2)*** | Recursos      | Enganar         |  Comunicação  |               |
| ***Regular (+1)***  | Empatia       | Percepção       |  Provocar     | Investigar    |

## Façanhas 

+ _**Demonstração de Poder**_ – Pode usar _Magia_ ao invés de _Provocar_ para intimidar usando uma Demonstração de Poder
+ _**Vara de Poder**_ – Recebe +2 em seus Rolamentos de Magia quando usar uma Vara como foco de seu Poder
+ _**Já li sobre isso!**_ – Você pode gastar um Ponto de Destino para usar _Conhecimentos_ no lugar de qualquer outra perícia em uma rolagem, desde que consiga explicar seu conhecimento sobre a ação que quer realizar.
+ _**Escudo da Razão**_ – Você pode usar _Conhecimento_ como defesa contra tentativas de _Provocar_, desde que explique como 

[loose-threads]: http://www.drivethrurpg.com/product/196127/Loose-Threads-o-A-World-of-Adventure-for-Fate-Core
