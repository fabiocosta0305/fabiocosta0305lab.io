---
title: "Leon, _el Leon de la pradera_ - Um rato espadachim"
layout: personagens
categories:
  - personagens
tags:
  - Loose Threads
header: no
language: br
---

_**N.A.:**_ Segue as regras de [Loose Threads][loose-threads]

## Aspectos

|          __*Tipo:*__ | __*Aspecto*__                                                                      |
|---------------------:|------------------------------------------------------------------------------------|
|          _Conceito:_ | Um rato espadachim em busca de aventura                                            |
| _Desejo do Coração:_ | Montarei _mi_ própria academia de esgrima                                          |
|            _Tensão:_ | Instintos de roedor que ocasionalmente atrapalham a minha vida  (Coragem/Instinto) |
|  _Motivação/Método:_ | Sempre confie no meu Florete                                                       |
|    _Relacionamento:_ | Alberich é leal comigo… E eu com ele                                               |
|    _Relacionamento:_ | _"Fang quer ser gente? Ele continua cheirando a predador, meu nariz não nega!"_    |
|    _Sétimo Aspecto:_ |                                                                                    |

## Perícias

|         ***Nível*** | ***Perícia*** | ***Perícia*** | ***Perícia*** | ***Perícia***     |
|--------------------:|:-------------:|:-------------:|:-------------:|:-----------------:|
|    ***Ótimo (+4)*** | Lutar         |               |               |                   |
|      ***Bom (+3)*** | Atletismo     | Vigor         |               |                   |
| ***Razoável (+2)*** | Furtividade   | Provocar      | Roubo         |                   |
|  ***Regular (+1)*** | Conhecimento  | Empatia       | Atirar        | Ofício (Ferreiro) |

## Façanhas 

+ _**Arma Reserva:**_ Sempre que alguém estiver prestes a adicionar o aspecto de situação Desarmado  ou similar em você, gaste um ponto de destino para declarar que você possui uma segunda arma. Ao invés de ganhar o aspecto de situação, seu oponente ganha um impulso, representando o momento de distração durante a troca de arma.
+ **Panache _com Florete:_** no início de um Conflito, antes de todos agirem, você pode tentar um teste de Provocar para Criar Vantagens ao tentar demonstrar quão incrível você é como combatente
+ _**Reação Atordoante:**_  Quando for bem-sucedido com estilo em uma defesa contra um oponente, você automaticamente contra-ataca com algum tipo de ataque contundente. Você poderá colocar o aspecto Atordoado no seu oponente e ganhar uma invocação gratuita dele, ao invés de apenas um impulso.

[loose-threads]: http://www.drivethrurpg.com/product/196127/Loose-Threads-o-A-World-of-Adventure-for-Fate-Core
