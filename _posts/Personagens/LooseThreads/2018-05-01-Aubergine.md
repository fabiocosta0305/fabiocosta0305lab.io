---
title: "Aubergine, o Coelho Branco"
layout: personagens
categories:
  - personagens
tags:
  - Loose Threads
header: no
language: br
---

## Aspectos

| __*Tipo:*__          | __*Aspecto*__                                                             |
|---------------------:|---------------------------------------------------------------------------|
|  _Conceito:_         | Um Coelho Sempre Atrasado                                                 |
| _Desejo do Coração:_ | Desejo um pouco de paz e sossego                                          | 
| _Tensão:_            | As Amarras Sociais (Liberdade / Educação)                                 | 
| _Motivação/Método:_  | Sempre ver as coisas como elas são, não como esperamos que elas devem ser | 
| _Relacionamento:_    | Devo minha vida ao Filho da Loba Má                                       | 
| _Relacionamento:_    | Regina é a única pessoa sensata dentro dessa Companhia... Além de mim!    | 
| _Sétimo Aspecto:_    |                                                                           | 

## Perícias 

| ***Nível***         | ***Perícia*** | ***Perícia***   | ***Perícia*** | ***Perícia*** |
|--------------------:|:-------------:|:---------------:|:-------------:|:-------------:|
| ***Ótimo (+4)***    | Comunicação   |                 |               |               |
| ***Bom (+3)***      | Empatia       | Percepção       |               |               |
| ***Razoável (+2)*** | Investigar    | Vontade         |  Conhecimento |               |
| ***Regular (+1)***  | Provocar      | Ofícios (Corte) |  Furtividade  | Atletismo     |

## Façanhas 

### Fate Básico

+ __*Filho da Corte:*__ _recebe +2_ ao _Criar Vantagens_ usando _Comunicação_ quando realizar alguma função aristocrata
+ __*Faro para Problemas:*__ Você pode usar _Empatia_ ao invés de _Percepção_ para determinar o seu turno em um conflito, contanto que tenha a chance de observar ou conversar com os envolvidos por alguns minutos antes.
+ __*Bisbilhoteiro:*__ Em uma rolagem bem-sucedida de _Investigar_ usando a ação de _Criar Vantagem_ na tentativa de espionar uma conversa, você pode descobrir ou criar um Aspecto adicional (porém sem receber uma invocação grátis).

[loose-threads]: http://www.drivethrurpg.com/product/196127/Loose-Threads-o-A-World-of-Adventure-for-Fate-Core
