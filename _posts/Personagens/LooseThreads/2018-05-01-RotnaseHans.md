---
title: "Rotnase Hans (Hans do Nariz Vermelho), o Bufão"
layout: personagens
categories:
  - personagens
tags:
  - Loose Threads
header: no
language: br
---

_**N.A.:**_ Segue as regras de [Loose Threads][loose-threads]

## Aspectos

|          __*Tipo:*__ | __*Aspecto*__                                                                 |
|---------------------:|-------------------------------------------------------------------------------|
|          _Conceito:_ | Um Bufão Simplório, cansado de uma vida sem razão                             |
| _Desejo do Coração:_ | Eu desejo ter uma vida melhor… Se possível me tornando Rei!                   |
|            _Tensão:_ | _"Qualquer idiota pode ser um rei, por que não eu???"_ (Liberdade/Reverência) |
|  _Motivação/Método:_ | Mais sábio do que aparenta (por isso se finge de idiota)                      |
|    _Relacionamento:_ | Cada um tem sua forma de ser sábio… louco… ou idiota                          |
|    _Relacionamento:_ | Vive-se apenas uma vez… Não se preocupe.                                      |
|    _Sétimo Aspecto:_ |                                                                               |

## Perícias

| ***Nível***         | ***Perícia*** | ***Perícia***   | ***Perícia*** | ***Perícia*** |
|--------------------:|:-------------:|:---------------:|:-------------:|:-------------:|
| ***Ótimo (+4)***    | Comunicação   |               |               |               |
| ***Bom (+3)***      | Provocar      | Atletismo       |               |               |
| ***Razoável (+2)*** | Conhecimento  | Empatia         |  Vontade      |               |
| ***Regular (+1)***  | Vigor         | Lutar           |  Percepção    | Atirar        |

## Façanhas 

+ _**Psicólogo**_  – Uma vez por sessão você pode reduzir um nível de consequência de alguém (severa para moderada, moderada para suave, suave para nenhuma) se for bem-sucedido em uma rolagem de Empatia com uma dificuldade Razoável (+2) para uma consequência suave, Boa (+3) para uma consequência moderada ou Ótima (+4) para severa
+ _**Verdade dolorosa**_  – pode usar Comunicação para Atacar, colocando verdades sobre um alvo dentro de uma pilhéria
+ _**Faro para Problemas**_ - Você pode usar Empatia ao invés de Percepção para determinar o seu turno em um conflito, contanto que tenha a chance de observar ou conversar com os envolvidos por alguns minutos antes.

[loose-threads]: http://www.drivethrurpg.com/product/196127/Loose-Threads-o-A-World-of-Adventure-for-Fate-Core
