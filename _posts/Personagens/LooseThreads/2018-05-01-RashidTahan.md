---
title: "Rashid Tahan, o mestre alquimista"
layout: personagens
categories:
  - personagens
tags:
  - Loose Threads
header: no
language: br
---

_**N.A.:**_ Segue as regras de [Loose Threads][loose-threads]

## Aspectos

|          __*Tipo:*__ | __*Aspecto*__                                                                     |
|---------------------:|-----------------------------------------------------------------------------------|
|          _Conceito:_ | Um alquimista em busca dos mistérios supremos da natureza                         |
| _Desejo do Coração:_ | Quero descobrir os elementos supremos da Alquimia                                 |
|            _Tensão:_ | Longe de minha casa conseguirei encontrar as minhas respostas? (Ocidente/Oriente) |
|  _Motivação/Método:_ | Gaste o tempo necessário, para agir com certeza                                   |
|    _Relacionamento:_ | Leon é um ratinho que precisa se conformar com o que é                            |
|    _Relacionamento:_ | Alberich é muito jovem, ainda tem o que aprender                                  |
|    _Sétimo Aspecto:_ |                                                                                   |

## Perícias

|         ***Nível*** | ***Perícia***      | ***Perícia***    | ***Perícia*** | ***Perícia*** |
|--------------------:|:------------------:|:----------------:|:-------------:|:-------------:|
|    ***Ótimo (+4)*** | Conhecimento       |                  |               |               |
|      ***Bom (+3)*** | Vontade            | Magia (Alquimia) |               |               |
| ***Razoável (+2)*** | Ofícios (Alquimia) | Empatia          | Comunicação   |               |
|  ***Regular (+1)*** | Provocar           | Atletismo        | Atirar        | Lutar         |

## Façanhas 

+ _**Grimório**_ -  quando preparando alguma coisa que já fez antes, recebe +2 no seu rolamento de Alquimia
+ _**Filtros Alquímicos**_ - pode usar Alquimía no lugar de Zaps ou Geasa, gastanto 1 Ponto de Destino. Ainda precisa preparar normalmente a poção
+ _**Já li sobre isso!**_ – Você pode gastar um Ponto de Destino para usar Conhecimentos no lugar de qualquer outra perícia em uma rolagem, desde que consiga explicar seu conhecimento sobre a ação que quer realizar.

[loose-threads]: http://www.drivethrurpg.com/product/196127/Loose-Threads-o-A-World-of-Adventure-for-Fate-Core
