---
title: "Mefistófeles, o antigo gato da Bruxa Má"
layout: personagens
categories:
  - personagens
tags:
  - Loose Threads
header: no
language: br
---

## História

Mefistófeles era o familiar, ajudante e possivelmente o único amigo da Bruxa Má. Um belo dia o Príncipe Encantado apareceu e matou a Bruxa Má para salvar a princesa que ela tinha sequestrado. De repente sozinho e abandonado no mundo, Mefistófeles vagou por muito tempo refletindo sobre a vida. Ele viu como as ações de uma pessoa podiam afetar o mundo ao seu redor, e ele decidiu que passaria a afetar o mundo para melhor. Ele decidiu ser um herói. Ele se juntou à Companhia para se tornar um herói.

_**N.A.:**_ Segue as regras de [Loose Threads][loose-threads]

## Aspectos

|          __*Tipo:*__ | __*Aspecto*__                                                           |
|---------------------:|-------------------------------------------------------------------------|
|          _Conceito:_ | O ex-familiar e ajudante felino da Bruxa Má                             |
| _Desejo do Coração:_ | Eu desejo ser o herói de uma história                                   |
|            _Tensão:_ | Ser proativo... depois do cochilo da tarde (Dever  /  Autogratificação) |
|  _Motivação/Método:_ | Um gato-herói deve ser silencioso e furtivo como uma sombra             |
|    _Relacionamento:_ | A Companhia precisa do meu heroísmo, eu preciso deles para    ser herói |
|    _Relacionamento:_ |                                                                         |
|   _Sétimo Aspecto:_  |                                                                         |

## Perícias

|         ***Nível*** | ***Perícia*** | ***Perícia*** | ***Perícia*** | ***Perícia*** |
|--------------------:|:-------------:|:-------------:|:-------------:|:-------------:|
|    ***Ótimo (+4)*** | Alquimia      |               |               |               |
|      ***Bom (+3)*** | Atletismo     | Furtividade   |               |               |
| ***Razoável (+2)*** | Investigar    | Percepção     | Comunicação   |               |
|  ***Regular (+1)*** | Enganar       | Roubo         | Empatia       | Vontade       |

## Façanhas 

+ ___Ex-familiar de bruxa:___ Mefistófeles recebe +2 em _Alquimia_ para _Superar_ ao lidar com poções
+ ___Combate felino:___ Mefistófeles pode atacar com _Atletismo_ no lugar de _Lutar_ com suas garras e presas
+ ____Silencioso como eu:___ Por ser um gato, Mefistófeles recebe +2 ao _Superar_ por _Furtividade_ para para mover-se furtivamente em lugares escuros

[loose-threads]: http://www.drivethrurpg.com/product/196127/Loose-Threads-o-A-World-of-Adventure-for-Fate-Core
