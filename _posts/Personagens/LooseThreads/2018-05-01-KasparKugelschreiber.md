---
title: "Kaspar Kugelschreiber, o Bardo"
layout: personagens
categories:
  - personagens
tags:
  - Loose Threads
header: no
language: br
---

_**N.A.:**_ Segue as regras de [Loose Threads][loose-threads]

## Aspectos

|          __*Tipo:*__ | __*Aspecto*__                                                               |
|---------------------:|-----------------------------------------------------------------------------|
|          _Conceito:_ | Um Bardo que Conhece Várias Baladas                                         |
| _Desejo do Coração:_ | Eu desejo criar minha própria Balada                                        |
|            _Tensão:_ | Conhecimento é muito bom, ainda mais se obtido ao vivo (Aventura/Diligente) |
|  _Motivação/Método:_ | A Música Encanta até Feras                                                  |
|    _Relacionamento:_ | _"Cada história diferente que temos aqui"_                                  |
|    _Relacionamento:_ | Rir do perigo só serve para loucos… Ou para os bem preparados               |
|    _Sétimo Aspecto:_ |                                                                             |

## Perícias

|         ***Nível*** | ***Perícia***   | ***Perícia*** | ***Perícia*** | ***Perícia*** |
|--------------------:|:---------------:|:-------------:|:-------------:|:-------------:|
|    ***Ótimo (+4)*** | Ofício (Músico) |               |               |               |
|      ***Bom (+3)*** | Comunicação     | Conhecimento  |               |               |
| ***Razoável (+2)*** | Recursos        | Empatia       | Vontade       |               |
|  ***Regular (+1)*** | Investigação    | Atletismo     | Percepção     | Atirar        |

## Façanhas 

+ _**Pérolas de Sabedoria**_ – +2 ao _Criar Vantagens_ ao utilizar _Ofícios_ para obter pistas envolvendo histórias e comparações com os eventos
+ _**Já li sobre isso!**_ – Você pode gastar um Ponto de Destino para usar Conhecimentos no lugar de qualquer outra perícia em uma rolagem, desde que consiga explicar seu conhecimento sobre a ação que quer realizar.
+ _**Balada de Escárnio e de Maldizer**_ – Pode utilizar _Comunicação_ no lugar de _Provocar_ para Atacar um alvo mentalmente usando suas canções e histórias

[loose-threads]: http://www.drivethrurpg.com/product/196127/Loose-Threads-o-A-World-of-Adventure-for-Fate-Core
