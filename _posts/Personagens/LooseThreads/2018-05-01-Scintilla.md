---
title: "Scintilla, a Fadinha"
layout: personagens
categories:
  - personagens
tags:
  - Loose Threads
header: no
language: br
---

_**N.A.:**_ Segue as regras de [Loose Threads][loose-threads]

## Aspectos

|          __*Tipo:*__ | __*Aspecto*__                                                                                 |
|---------------------:|-----------------------------------------------------------------------------------------------|
|          _Conceito:_ | Fadinha Travessa Pura de Coração                                                              |
| _Desejo do Coração:_ | Eu desejo obter a sabedoria de meus Pais e meus Avôs                                          |
|            _Tensão:_ | Viver é divertido, mas as Regras que os outros determinam são chatas demais! (Dever/Diversão) |
|  _Motivação/Método:_ | Fé, Confiança… E um pouco de Perlimpimpim… Resolvem qualquer coisa                            |
|    _Relacionamento:_ | Regina é tão chata… Será que ela não consegue se divertir?                                    |
|    _Relacionamento:_ | Às vezes passo dos limites… Mas quem nunca?                                                   |
|    _Sétimo Aspecto:_ |                                                                                               |

## Perícias

| ***Nível***         | ***Perícia*** | ***Perícia***   | ***Perícia*** | ***Perícia*** |
|--------------------:|:-------------:|:---------------:|:-------------:|:-------------:|
| ***Ótimo (+4)***    | Magia (Geasa) |                 |               |               |
| ***Bom (+3)***      | Vontade       | Conhecimentos   |               |               |
| ***Razoável (+2)*** | Recursos      | Empatia         |  Comunicação  |               |
| ***Regular (+1)***  | Enganar       | Percepção       |  Provocar     | Investigar    |

## Façanhas 

+ _**Pirlimpimpim**_ – Pode automaticamente _Encantar_ uma pessoa por um _Ponto de Destino_
+ _**Faro para Problema**_ –  Você pode usar _Empatia_ ao invés de _Percepção_ para determinar o seu turno em um conflito, contanto que tenha a chance de observar ou conversar com os envolvidos por alguns minutos antes.
+ _**Psicólogo**_ – Uma vez por sessão você pode reduzir um nível de consequência mental de alguém (severa para moderada, moderada para suave, suave para nenhuma) se for bem-sucedido em uma rolagem de _Empatia_
+ _**Varinha de Condão**_ – Recebe +2 em seus Rolamentos de Magia quando usar uma Vara como foco de seu Poder

[loose-threads]: http://www.drivethrurpg.com/product/196127/Loose-Threads-o-A-World-of-Adventure-for-Fate-Core
