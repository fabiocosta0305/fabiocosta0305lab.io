---
title: Lauro Bosconi, Espírito das Artes
layout: personagens
language: br
categories:
  - personagens
tags:
  - Young Centurions
header: no
---

## Aspectos

|      ***Tipo*** | ***Aspecto***                                                    |
|----------------:|:----------------------------------------------------------------:|
|    __Conceito__ | Sopranino de um Coral que aprecia ver o belo                     |
| __Dificuldade__ | _“Se precisa de violência, não merece ser resolvido!”_           |
|                 | O Mal cai por si próprio                                         |
|                 | Minha voz é capaz de guiar para a paz                            |
|                 | Cresceu em um Orfanato – extremamente tímido quando não cantando |

## Abordagem

| ***Approach*** | ***Level***     |
|---------------:|:---------------:|
|       __Ágil__ | _Medíocre (+0)_ |
|  __Cuidadoso__ | _Razoável (+2)_ |
|    __Esperto__ | _Regular (+1)_  |
|   __Estiloso__ | _Bom (+3)_      |
|   __Poderoso__ | _Regular (+1)_  |
| __Sorrateiro__ | _Razoável (+2)_ |

## Façanha Centuriã

+ __Ode à Alegra:__ Pode gerar automaticamente um Aspecto com uma _Invocação Gratuita_ representando algum clima ou alguma ação que possa gerar com sua voz, _duas vezes por sessão_. Não pode usar isso para machucar outras pessoas. Normalmente dura uma cena esse Aspecto.

## Façanhas Comuns

+ __Ator:__  não sofre penalidades ao tentar se passar por pessoas muito mais velhas, altas ou baixas que ele mesmo, com sotaques diferentes, e por aí afora.
+ __Contador de Histórias:__ +2 ao tentar Criar Vantagens sendo Esiloso ao contar algum tipo de história, como ao tentar gerar empatia por meio de uma história triste, ganhar confiança por meio de piadas, ou distrair outros com uma história longa.

