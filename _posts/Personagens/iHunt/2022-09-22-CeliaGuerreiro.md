---
title: Célia Guerreiro (Ela/Dela)
subheadline: "Das pistas para o carnaval e para as festas infantis dos Alegres Trovadores em #iHunt"
layout: personagens
categories:
 - personagens
comments: true
tags:
  - "#iHunt"
language: br
header: no
---

## Aspectos

|            ***Tipo*** | ***Aspecto***                                  |
|----------------------:|:-----------------------------------------------|
|              **Tara** | FUI (Piloto)                                   |
|     **Alto Conceito** | Não apenas uma mulherzinha                     |
|             **Drama** | Sempre tenho que provar duas vezes mais        |
| **Quadro dos Sonhos** | Entrar para o mundo do automobilismo           |
|           **Emprego** | Construção de equipamentos, animadora de festa |
|           **Aspecto** |                                                |
|           **Aspecto** |                                                |

## Habilidades

|       ***Nível*** | **Habilidade**          | **Habilidade** | **Habilidade** | **Habilidade** | **Habilidade** |
|------------------:|:-----------------------:|:--------------:|:--------------:|:--------------:|:--------------:|
|    **Ótima (+5)** |                         |                |                |                |                |
|   **Grande (+4)** | Profissional (Mecânica) |                |                |                |                |
|      **Boa (+3)** | Sobrevivente            | Atleta         |                |                |                |
| **Razoável (+2)** | Serviço Social          | Investigador   | Assassino      |                |                |
|    **Média (+1)** | Ocultista               | Acadẽmico      | Trambiqueiro   | Organizador    |                |

## Manobras (Reforço: 3)

+ ___Estoque de FUI:___ Se tiver acesso a um conjunto elementar de ferramentas, _uma vez por cena_ pode eliminar um Aspecto de Situação sem precisar realizar rolamentos.
+ ___Pilotagem Sagaz:___ Qualquer aspecto que você criar relacionado a veículos ou drones ganha uma _invocação_ adicional sem custo. Além disso, qualquer ação de defesa enquanto estiver dirigindo ou pilotando ganha +2.
+ ___Eu consigo, pode deixar!:___ Pode usar _Sobrevivente_ no lugar de alguma perícia qualquer se for contestada previamente, pagando 1 PD
+ ___Envenenando o carro:___ +2 ao _Criar Vantagens_ com _Profissional_ para melhorar qualquer equipamento. Pode ser usado mesmo quando não é algo relacionado a Mecânica

## Estresse

+ Físico: 5
+ Mental: 7
