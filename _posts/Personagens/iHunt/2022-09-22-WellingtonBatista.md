---
title: Wellington Batista (Ele/Dele)
subheadline: "O _Go-Go boy_ em busca de grandes experiência"
layout: personagens
categories:
 - personagens
comments: true
tags:
  - "#iHunt"
language: br
header: no
---

## Aspectos

|            ***Tipo*** | ***Aspecto***                            |
|----------------------:|:-----------------------------------------|
|              **Tara** | Malina (Alquimista)                      |
|     **Alto Conceito** | Alguém em busca de experiências diversas |
|             **Drama** | Woodstock não acabou                     |
| **Quadro dos Sonhos** | Rodar o mundo, experimentar de tudo      |
|           **Emprego** | Chapeleiro Maluco e _Go-Go Boy_          |
|           **Aspecto** |                                          |
|           **Aspecto** |                                          |

## Habilidades

|       ***Nível*** | **Habilidade** | **Habilidade** | **Habilidade** | **Habilidade** | **Habilidade** |
|------------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|
|    **Ótima (+5)** |                |                |                |                |                |
|   **Grande (+4)** | Trambiqueiro   |                |                |                |                |
|      **Boa (+3)** | Ocultista      | Atleta         |                |                |                |
| **Razoável (+2)** | Sobrevivente   | Influencer     | Guerrilheiro   |                |                |
|    **Média (+1)** | Organizador    | Acadêmico      | Serviço Social | Criador        |                |

## Manobras (Reforço: 3)

+ ___Sabe das Coisas:___ _Você pode “segurar” um Aspecto sobre seu conhecimento quanto a monstros e trazê-lo a jogo a qualquer momento_, com uma invocação gratuita Entretanto, ao o fazer, algum outro Aspecto irá sumir
+ ___“Poções”:___ Poções de alquimista te tornam mais forte, mais rápida, mais gostosa, mais maravilhosa. Se você investir um ponto de destino para fazer uma poção, você pode ligá-la a um aspecto para compensar os seus efeitos. Quando alguém beber a poção, essa pessoa ganha aquele aspecto e duas invocações de graça.
+ ___Yo soy el quebra ley:___ +2 ao Superar obstáculos relacionados a detectar ou contornar a presença de forças da lei
+ ___Chá de Desaniversário:___ pode preparar um certo preparado que oferece +2 nos testes dele de Trambiqueiro para convencer alguém a lhe ajudar, mudando o estado de consciência da mesma, uma vez por sessão. Role Ocultista, dificuldade Medíocre (+0): o total de viradas define o número de doses (como Invocações Gratuitas) e a dificuldade mínima do alvo para Superar os efeitos do mesmo. Os efeitos duram uma Cena.

## Estresse

+ Físico: 7
+ Mental: 5
