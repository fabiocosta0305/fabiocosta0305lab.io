---
title: Helena Tobias (Ela/Dela)
subheadline: "A Coordenadora dos Trovadores Caçadores de #iHunt"
layout: personagens
categories:
 - personagens
comments: true
tags:
  - "#iHunt"
language: br
header: no
---

## Aspectos

|            ***Tipo*** | ***Aspecto***                                                 |
|----------------------:|:--------------------------------------------------------------|
|              **Tara** | 66 (Recorte)                                                  |
|     **Alto Conceito** | Alguém procurando ajustar sua vida como sempre fazia no circo |
|             **Drama** | Doenças demais na família, resultam em contas demais          |
| **Quadro dos Sonhos** | Poder viajar um pouco sem preocupações                        |
|           **Emprego** | Atriz e personagem vivo, coordenadora dos Alegres Trovadores  |
|           **Aspecto** |                                                               |
|           **Aspecto** |                                                               |

## Habilidades

|       ***Nível*** | **Habilidade** | **Habilidade** | **Habilidade** | **Habilidade** | **Habilidade** |
|------------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|
|    **Ótima (+5)** |                |                |                |                |                |
|   **Grande (+4)** | Organizador    |                |                |                |                |
|      **Boa (+3)** | Influencer     | Serviço Social |                |                |                |
| **Razoável (+2)** | Trambiqueiro   | Investigador   | Hacker         |                |                |
|    **Média (+1)** | Ocultista      | Acadêmico      | Guerrilheiro   | Atleta         |                |

## Manobras (Reforço: 3)

+ ___Pessoas que Conhecem Pessoas:___ _ao usar um Ponto de Destino para adicionar algo à história_, se esse algo for uma pessoa que você conhece, ela será um expert, com ao menos um pacote de Habilidades em Ótimo (+4), ou em Bom (+3) com um grupo de aliados. Ele aparece como um Aspecto em cena, com uma invocação gratuita
+ ___Imunidade Diplomática:___ _Se você criar uma vantagem relacionada à aceitação geral de um grupo_, esse Aspecto permanece enquanto for relevante, e a cada cena você ganha uma invocação de graça para esse aspecto. Além disso, toda vez que você tiver sucesso em se defender de alguém que esteja tentando abalar esse relacionamento, considere que você defendeu com estilo.
+ ___Montando uma Cena:___ _uma vez por cena_, caso alguém aja de uma maneira que você previamente determinou, você pode oferecer a ela um bônus +2 por trabalho em equipe ao invés do padrão +1.
+ ___Analisando a Situação:___ +2 ao _Criar Vantagens com Organizador_ para posicionar aliados em uma situação vantajosa. Pode, uma vez por sessão, sacrificar o bônus para, em caso de Sucesso no teste, trazer a Vantagem para o grupo.

## Estresse

+ Físico: 5
+ Mental: 3
