---
title: Esperanza Guanaes (O mais conveniente)
subheadline: "O(a) príncipe(esa) agênero da Alegres Trovadores em #iHunt"
layout: personagens
categories:
 - personagens
comments: true
tags:
  - "#iHunt"
language: br
header: no
---

## Aspectos

|            ***Tipo*** | ***Aspecto***                                              |
|----------------------:|:-----------------------------------------------------------|
|              **Tara** | Malina (Arcanista)                                         |
|     **Alto Conceito** | Uma pessoa agênero em continua afirmação e descoberta      |
|             **Drama** | _“O mundo é tão ou mais oscilante que eu”_                 |
| **Quadro dos Sonhos** | Família é você quem faz, não necessariamente de onde vem   |
|           **Emprego** | Príncipe **E** Princesa: sendo ambos eu pago minhas contas |
|           **Aspecto** |                                                            |
|           **Aspecto** |                                                            |

## Habilidades

|       ***Nível*** | **Habilidade** | **Habilidade** | **Habilidade** | **Habilidade** | **Habilidade** |
|------------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|
|    **Ótima (+5)** |                |                |                |                |                |
|   **Grande (+4)** | Ocultista      |                |                |                |                |
|      **Boa (+3)** | Sobrevivente   | Influencer     |                |                |                |
| **Razoável (+2)** | Organizador    | Investigador   | Trambiqueiro   |                |                |
|    **Média (+1)** | Criador        | Guerrilheiro   | Serviço Social | Acadêmico      |                |

## Manobras (Reforço: 3)

+ ___Sabe das Coisas:___ Você pode “segurar” um Aspecto sobre seu conhecimento quanto a monstros e trazê-lo a jogo a qualquer momento, com uma invocação gratuíta. Entretanto, ao o fazer, algum outro Aspecto irá sumir
+ ___Hoje eu sou essa pessoa:___ _uma vez por sessão_, se tiver tempo e materiais  você pode se passar por outra pessoa de maneira quase perfeita. Crie um ___Alto-Conceito___ com o nome da Pessoa, e o Narrador cria uma ___Dificuldade___ representando detalhes que passaram batido. Role _Sobrevivente_ ou _Influencer_ contra Medíocre (+0) como uma ação de _Criar Vantagem_. O Resultado final é a dificuldade para alguém notar o disfarce e determina Invocações Gratuítas no _Alto-Conceito_
+ ___Pirlimpimpim:___ apesar do nome, não é algo mágico, mas uma combinação especial que Hope produz e que pode ser usado para criar uma ___Aura Mágica Falsa___ (como um Aspecto), com uma _Invocação Gratuita_. O primeiro uso em uma sessão é gratuito: os demais demandam PDs.

## Extra: Embruxação

+ ___Embruxação:___ Cada vez que você fizer essa manobra, você pode escolher dois pontos de dons mágicos para representar seu conhecimento estranho sobre magia. Mas se você pegar alguma outra coisa, você precisa descobrir como seu personagem irá fazer isso com um ritual simples ou com magia de símbolos. Pague qualquer custo de essência com stress mental ou físico, 1 para 1, você escolhe a combinação.
  + ___Cura (2), Custo 3:___ Este feitiço cura um ser vivo rapidamente, talvez até salvando-o da beira da morte. Role uma ação de _Superar_ cuja dificuldade será determinada pelo ferimento atual. Adicione os níveis do feitiço à rolagem. Se a rolagem tiver a mesma quantidade de viradas do que a consequência ou espaço de stress, você pode gastar as viradas para removê-las. Uma pessoa só pode receber este feitiço _uma vez por episódio_. Se for permanente, uma única essência pode reativar este feitiço, mas ele deve estar ligado a uma pessoa, e só poderá ser usado nela

## Estresse

+ Físico: 3
+ Mental: 7
