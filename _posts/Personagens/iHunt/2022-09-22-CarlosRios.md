---
title: Carlos Rios (Ele/Dele)
subheadline: "O DJ que caça com a música em #iHunt"
layout: personagens
categories:
 - personagens
comments: true
tags:
  - "#iHunt"
language: br
header: no
---

## Aspectos

|            ***Tipo*** | ***Aspecto***                                                        |
|----------------------:|:---------------------------------------------------------------------|
|              **Tara** | Fui (Hacker)                                                         |
|     **Alto Conceito** | Um músico profissional, um artista da música                         |
|             **Drama** | Venezuelano de Nascença, Chileno de Naturalidade, Brasileiro de amor |
| **Quadro dos Sonhos** | Glastonbury é o palco definitivo!                                    |
|           **Emprego** | DJ e músico em festas                                                |
|           **Aspecto** |                                                                      |
|           **Aspecto** |                                                                      |

## Habilidades

|       ***Nível*** | **Habilidade**         | **Habilidade** | **Habilidade** | **Habilidade** | **Habilidade** |
|------------------:|:----------------------:|:--------------:|:--------------:|:--------------:|:--------------:|
|    **Ótima (+5)** |                        |                |                |                |                |
|   **Grande (+4)** | Influencer             |                |                |                |                |
|      **Boa (+3)** | Investigador           | Hacker         |                |                |                |
| **Razoável (+2)** | Guerrilheiro           | Médico         | Ocultista      |                |                |
|    **Média (+1)** | Profissão (Eletrônica) | Acadêmico      | Sobrevivente   | Atleta         |                |

## Manobras (Reforço: 3)

+ ___Estoque de FUI:___ Se eles tiverem acesso a um conjunto elementar de ferramentas, _uma vez por cena_ pode eliminar um Aspecto de Situação sem precisar realizar rolamentos.
+ ___Referência Hacker:___ _se a vantagem não estiver definida_, e suas ações estejam diretamente ligadas a usar um computador, você começa com a vantagem. Se você puder criar um aspecto situacional usando um sistema de computador que esteja conectado a outro personagem, superá-lo parte de um limiar igual ao de sua habilidade Hacker, +2 se você estiver ativamente se opondo à ação. Se você puder fazer isso sem usar um computador, você sempre pode apostar aspectos contra alguém, uma vez por cena.
+ ___I’m the operator with my Pocket Calculator:___ pode utilizar _Influencer_ para _Atacar mentalmente um inimigo com música_, desde que tenha descoberto um Aspecto relativo aos gostos dele;
+ ___One more time:___ _pode utilizar uma ação de Superar usando Influencer para tomar a Vantagem para o grupo_, desde que ele tenha gerar uma grande quantidade de distrações sensoriais contra o alvo. O alvo se defende normalmente.

## Estresse

+ Físico: 5
+ Mental: 5
