---
title: Sara Mendes (Ela/Dela)
subheadline: "A técnica de efeitos especiais (e explosões) que tapa buracos nos Alegres Trovador"
layout: personagens
categories:
 - personagens
comments: true
tags:
  - "#iHunt"
language: br
header: no
---

## Aspectos

|            ***Tipo*** | ***Aspecto***                                                 |
|----------------------:|:--------------------------------------------------------------|
|              **Tara** | Cavalo (Jogadora)                                             |
|     **Alto Conceito** | Alguém bonita e esperta demais para seu próprio bem           |
|             **Drama** | _“A vida acha que eu sou John McLane?”_                       |
| **Quadro dos Sonhos** | Um cantinho só meu                                            |
|           **Emprego** | Montagem de efeitos especiais, ocasionalmente personagem vivo |
|           **Aspecto** |                                                               |
|           **Aspecto** |                                                               |

## Habilidades

|       ***Nível*** | **Habilidade** | **Habilidade** | **Habilidade** | **Habilidade** | **Habilidade** |
|------------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|
|    **Ótima (+5)** |                |                |                |                |                |
|   **Grande (+4)** | Guerrilheiro   |                |                |                |                |
|      **Boa (+3)** | Criador        | Atleta         |                |                |                |
| **Razoável (+2)** | Sobrevivente   | Assassino      | Trambiqueiro   |                |                |
|    **Média (+1)** | Ocultista      | Influencer     | Serviço Social | Investigador   |                |

## Manobras (Reforço: 3)

+ ___A Melhor defesa...:___ _ao realizar um Ataque_, você pode “salvar” o mesmo, obtendo o valor final e o travando para uso posterior. Anote o Esforço final, após todos os usos de Aspectos e Manobras. A qualquer momento que precisar se Defender, você pode usar esse resultado “salvo” no lugar do rolamento (mesmo que ele já tenha sido executado)
+ ___Espírito de Equipe:___ Cada vez que alguém da sua equipe agir de acordo com o plano e gastar um ponto de destino para invocar um aspecto situacional que você criou, esse aspecto ganha uma invocação de graça. Uma vez por sessão, gaste 1 PD para resetar uma ação, explicando como isso encaixa no seu plano
+ ___Em caso de dúvida... C4:___ _uma vez por sessão_, role _Criador_ contra dificuldade _Medíocre (+0)_, como uma ação de _Criar Vantagem_ para criar ___Explosivos Poderosos___ que são considerados como tendo um _nível de Arma_ igual o resultado do rolamento+1. Baixe em 1 o nível de Arma para obter uma Invocação Gratuita adicional. Esse Aspecto dura 1 Cena.
+ ___Vai Explodir!:___ pode utilizar _Criador_ para _Atacar_, porém ataca ___indiscriminadamente___ todos os alvos em uma mesma Zona. Com 1 Ponto de Destino, aumenta o alcance do Ataque para um número de Zonas igual o bônus de Criador. Todos os alvos devem resistir ao mesmo Esforço (incluindo você)

## Estresse

+ Físico: 7
+ Mental: 5
