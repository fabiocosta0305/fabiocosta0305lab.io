---
title: Os Caçadores Trovadores (ou também _Alegres Trovadores Animação e Festas_)
subheadline: Os Animadores de Festa que saem para caçar monstros após o trabalho em #iHunt
#date: 2015-07-20 16:01:00 +0300
layout: post
categories:
 - RPG
tags:
 - Fate
 - Fate Core
 - Fate-Core
 - Artigos
 - "#iHunt"
header: no
---

Ninguém pensa muito em um grupo de animadores de festa infantil como caçadores, mas como eles também trabalham na noite (no início, mas à noite), umas caçadas ocasionais podem ser consideradas apenas um período de hora extra para tirar algum a mais.

Normalmente eles andam com uma _van_ toda decorada e colorida que é mantida limpa e sem resíduos de sujeira graças ao trabalho de limpeza contínua (se as pessoas soubessem a sujeira que rola em festas infantis!) por parte do grupo. E curiosamente às vezs caçam usando as roupas das festas: uma Cabeça de Mickey pode ajudar muito para não ter uma Luta Justa na hora H.

São conhecidos frequentadores da _Padaria Nova Luanda_, na região do centro velho de São Paulo, popularmente conhecido como _Padaria do António_, cujo dono é filho de pai Angolano e mãe Caboverdana que vieram para o Brasil evitando as guerras de independência de lá e acabaram montando uma padaria. Essa padaria é o ponto de encontro de #iHunters, pois nele sempre dá para conseguir uma média quente e um pão bem quente com mantegia a beça, ou um caldo verde ou de cebola dependendo do horário, além da mais pura culinária africana em geral e angolana em especial. Não é difícil os ver lá, algumas vezes ainda nas fantasias das festas.

+ [___Helena___][helena] é a 66 do grupo. Ela já é mais velha, na casa dos 40 anos, e entrou para o #ihunt devido à falta de dinheiro após a morte do pai e do AVC da mãe. Ainda mantem um pouco do patrimônio que o pai deixo de herança aos trancos e barrancos, incluindo três apartamentos no Edifício Copan que funcionam como casa, depósito, escritório e centro de operações, além de ocasionalmente dormitório para os demais membros. Helena sabe se desdobrar como poucos, seja fisicamente (foi contorcionista circense quando criança, o que aprendeu com a mãe) quanto mentalmente, mas vive cortando dobrados para manter os tratamentos e fisioterapias da mãe. Entrou para o #ihunt por meio de um amigo em comum da época do circo, que trabalhava como palhaço de festas infantis e que precisava de uma ajuda para pegar um alvo que rodava no meio de festas, um predador de crianças que por um acaso era um demônio.
+ [___Esperanza___][esperanza] é a Malina do Grupo. Depois de se descobrir enquanto pessoa agenero, Esperanza (não seu nome de batismo, mas o que usa) foi deserdada pela família e vice-versa. Depois de um tempo se virando com um talento adicional em roupas e fantasias, ela se revelou um rosto com um estilo útil para Helena que a trouxe para dentro de sua companhia de personagens vivos, oferecendo dinheiro e um teto: Esperanza pode se passar como um lindo príncipe ou uma formosa princesa, para pagar as contas. Como sempre se ligou em coisas ocultas, ela (ou ele, ela não se incomoda tanto com pronomes) começou a pesquisar coisas e agora tem seu ponto de estudo de oculto. Agora ela tem um local próprio, uma kitinete na região da Guilhermina-Esperança, onde ela vive, e onde lê cartas quando não está trabalhando para Helena, como Príncipe, Princesa ou Caçadora.
+ [___Célia___][celia], a Fui do Grupo. Nascida na região de Interlagos, sempre cresceu com o ronco dos motores do Autódromo e isso a fascinou. Com 12 anos, começou a se envolver com rachas, para desgosto do pai, que apesar de mecânico, sempre acreditou que _"mulher de respeito não mexe com graxa"_ apesar dos pôsteres de mulher pelada na borracharia da família. Com 16, entrou para _Escola Básica de Mecânica de Veículos_, que ficava dentro do Autódromo, e amou de vez. Apesar disso, trabalhar com automobilismo não dava tanto dinheiro, sem falar no Mundo de Marlboro que meio que a expulsou de lá (ainda faz uns serviços ocasionais para equipes de categorias menores). Para fazer dinheiro, aprendeu a montar estruturas de carros alegóricos em escolas de samba, e nessa conheceu via contatos Helena, que precisava de umas coisas para Festas Infantis... E para caçar monstros no #ihunt...
+ [___Timóteo___][timoteo], o Cavalo do Grupo. De uma família tradicional de circo, desde pequeno treinou seu corpo para os feitos de graça, força, habilidade e flexibilidade do circo. Com o quase total desaparecimento do circo, mudou de foco ao treinar várias artes marciais, Esgrima e Atuação. Por algum tempo atuou como parte do time de Esgrima do Clube Pinheiros, mas depois acabou fazendo parte de um grupo de teatro que faziam versões de contos de fadas para o teatro. Quando o grupo se desfez, acabou conhecendo Helena por acaso, quando ele ajudou a negociar as fantasias e materiais cenográficos do grupo, e acabou entrando para a equipe de Helena. Às vezes anacronista demais (usa relógio de bolso e calças de sarja com colete e suspensório), mas ao menos um lobisomem já teve o peito perfurado por um florete de prata manuseado por ele.

[helena]: {% post_url 2022-09-22-HelenaTobias %}
[esperanza]: {% post_url 2022-09-22-EsperanzaGuanaes %}
[celia]: {% post_url 2022-09-22-CeliaGuerreiro %}
[timoteo]: {% post_url 2022-09-22-TimoteoRodrigues %}
