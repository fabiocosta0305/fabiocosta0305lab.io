---
title: Valter Celeste (Elu/Delu)
subheadline: "E palhaço de coração e sem-teto profissional da Alegres Trovadores em #iHunt"
layout: personagens
categories:
 - personagens
comments: true
tags:
  - "#iHunt"
language: br
header: no
---

## Aspectos

|            ***Tipo*** | ***Aspecto***                                  |
|----------------------:|:-----------------------------------------------|
|              **Tara** | 66 (Face)                                      |
|     **Alto Conceito** | Um espírito livre que procura conhecer o mundo |
|             **Drama** | Sem teto profissional                          |
| **Quadro dos Sonhos** | Minha própria lona                             |
|           **Emprego** | Palhaço profissional, bobo de coração          |
|           **Aspecto** |                                                |
|           **Aspecto** |                                                |

## Habilidades

|       ***Nível*** | **Habilidade** | **Habilidade** | **Habilidade** | **Habilidade** | **Habilidade** |
|------------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|
|    **Ótima (+5)** |                |                |                |                |                |
|   **Grande (+4)** | Serviço Social |                |                |                |                |
|      **Boa (+3)** | Sobrevivente   | Organizador    |                |                |                |
| **Razoável (+2)** | Influencer     | Investigador   | Trambiqueiro   |                |                |
|    **Média (+1)** | Ocultista      | Acadẽmico      | Atleta         | Guerrilheiro   |                |

## Manobras (Reforço: 3)

+ ___Pessoas que Conhecem pessoas:___ ao usar um Ponto de Destino para adicionar algo à história, e esse algo for uma pessoa que você conhece, ela será um expert, com ao menos um pacote de Habilidades em Ótimo (+4), ou em Bom (+3) com um grupo de aliados. Ele aparece como um Aspecto em cena, com uma invocação gratuíta
+ ___Disfarce Secreto:___ Quando você criar uma vantagem relacionada a se misturar com uma subcultura ou grupo ao qual você não pertence, você pode escolher uma habilidade relevante que esteja relacionada à área de interesse daquele grupo. Enquanto esse aspecto existir e continuar relevante, você tem acesso a essa habilidade no nível 3. Outra opção é escolher uma manobra relacionada àquela habilidade, e você pode usar essa manobra. Você só pode ter um desses aspectos/habilidades por vez
+ ___Todo mundo ama um palhaço:___ +2 ao Criar Vantagens com Serviço Social para descobrir informações sobre os outros enquanto faz alguma bobagem;
+ ___Tudo que tenho está na mochila:___ +2 ao Criar Vantagens com Sobrevivente para verificar se tem algo apropriado consigo

## Estresse

+ Físico: 5
+ Mental: 7
