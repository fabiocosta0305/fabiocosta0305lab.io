---
title: Timóteo Rodrigues (Ele/Dele)
subheadline: "O príncipe esgrimista da Alegres Trovadores em #iHunt"
layout: personagens
categories:
 - personagens
comments: true
tags:
  - "#iHunt"
language: br
header: no
---

## Aspectos

|            ***Tipo*** | ***Aspecto***                                                    |
|----------------------:|:-----------------------------------------------------------------|
|              **Tara** | Cavalo (Operativo)                                               |
|     **Alto Conceito** | Um príncipe encantado do século XXI                              |
|             **Drama** | Anacronicamente cavalheiresco                                    |
| **Quadro dos Sonhos** | Uma academia de Artes Marciais e Esgrima só minha                |
|           **Emprego** | Instrutor de Artes Marciais e Príncipe Encantado nas horas vagas |
|           **Aspecto** |                                                                  |
|           **Aspecto** |                                                                  |

## Habilidades

|       ***Nível*** | **Habilidade** | **Habilidade** | **Habilidade** | **Habilidade** | **Habilidade** |
|------------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|
|    **Ótima (+5)** |                |                |                |                |                |
|   **Grande (+4)** | Guerrilheiro   |                |                |                |                |
|      **Boa (+3)** | Influencer     | Atleta         |                |                |                |
| **Razoável (+2)** | Organizador    | Assassino      | Trambiqueiro   |                |                |
|    **Média (+1)** | Ocultista      | Espião         | Serviço Social | Investigador   |                |

## Manobras (Reforço: 3)

+ ___A Melhor defesa...:___ _ao realizar um Ataque_, você pode “salvar” o mesmo, obtendo o valor final e o travando para uso posterior. Anote o Esforço final, após todos os usos de Aspectos e Manobras. A qualquer momento que precisar se Defender, você pode usar esse resultado “salvo” no lugar do rolamento (mesmo que ele já tenha sido executado)
+ ___Consciência Situacional:___ Quando um aspecto situacional seu, relacionado de alguma forma ao seu ambiente físico, for chamado ou apostado, você ganha um impulso. Além disso, quando você tiver um aspecto que reflita sua habilidade de utilizar o cenário como arma, ele é invocado como +3 em vez de +2.
+ ___Panache:___ caso tenha tempo para demonstrar suas habilidades, role _Guerrilheiro_ como um _Ataque Mental_. Em caso de Sucesso, você obtêm a Vantagem para o seu lado. A oposição se defende normalmente
+ ___Arma Cenográgfica... #sqn:___ uma vez por sessão, pode declarar que possui uma arma que, embora aparentemente cenográfica, na verdade é muito poderosa. Dê uma descrição simples, que entra como um Aspecto em jogo. Essa arma oferece +2 em todos os _Ataques_ por _Guerrilheiro_. Não pode ser usado para ir contra Fraquezas do alvo

## Estresse

+ Físico: 7
+ Mental: 3
