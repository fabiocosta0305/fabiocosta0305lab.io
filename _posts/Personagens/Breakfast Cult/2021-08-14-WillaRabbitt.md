---
title: "Willamina \"Willa\" Rabbitt, a coelha branca"
subheadline: O bobo integrante do Sket-Dan do Colégio Itazura
layout: personagens
categories:
 - personagens
comments: true
tags:
  - Breakfast Cult
  - Personagem
language: br
header: no
---

A pequena Willamina Rabbit (cujo nome só soube devido aos bordados em suas roupas de bebê) cresceu sendo empurrada de orfanato em orfanato no norte da Bretanha, cruzando as fronteiras da Escócia, Gales e Inglaterra livremente. Sempre mau-tratada pelos mais velhos, devido à sua aparência estranha, ela viveu em orfanatos desde que se entende por gente. Uma investigação da Fundação rastreou informações sobre um Culto e sobre como experiências bizarras envolvendo uma modificação de crianças por meio de Occultech para criar fábulas e mitos modernos foi descoberto. O Culto foi capturado e todas as pesquisas enviadas para a Ilha Occultar, junto com um dos poucos resultados que descobriu-se legítimo: uma menina de Aberdeen foi misturada com o que se acredita serem pelos do Coelho Branco, Senescal da Rainha de Copas, uma aberração extraplanar segundo os resultados da Investigação. Ela teria desenvolvido poderes relacionados ao tempo e a sincronicidade. Essa garota foi obtida pelo grupo de maneiras ainda não determinadas e, após ter sido transformada, foi enviada a orfanatos por alguma razão desconhecida. O nome dela é Willamina Rabbit

Willamina deseja amizade acima de tudo, mas o estresse que sofreu quando criança a tornou extremamente tensa e tímida, sendo ocasionalmente difícil de lidar, em especial pelo fato de vestir-se de maneira altamente anacrônica, usando relógios e cartas de baralho como acessórios de maneira estranhas (há quem diga que algumas delas sejam Occultech). Além disso, barulhos altos e desordem a deixam realmente tensa. Além disso, ela ODEIA estar atrasada, a um ponto patológico. 

Diferentemente da maioria dos _Newtype_ que possuem aparência muito humana, seu rosto é quase lapino: lábios leporinos mostrando dentes incisivos um pouco avançados, pele alva, grandes olhos azuis com iris de tamanho enorme, cabelo branco e tão encaracolado que chega quase a ser felpudo e orelhas que são maiores que a própria cabeça (e isso é perceptível quando ela se chateia e as orelhas caem). 

## ID

+  __Student ID #__ AXF06-2540
+  __Nascida em__ Aberdeen, Escócia
+  __Data de Nascimento:__ 4 de Julho
+  __Altura__ 1,53m (1,83, contando orelhas)
+  __Peso__ 55 kg
+  __Olhos__ Azuis com grandes irises
+  __Cabelos__ Brancos, meio felpudos
+  __Características:__ Rosto lapino, lábios leporinos, grandes orelhas
+  __Tipo Sanguineo:__ AB+
+  __Gosta de:__ Momentos calmos, livros, relógios, chapéus, baralhos, roupas lolita e steampunk, a cor vermelha, chá
+  __Não gosta de:__ Agitação, barulhos altos (em especial explosivos), desordem, café, atrasos

## Aspectos

|    ***Tipo*** | ***Aspecto***                                                          |
|--------------:|------------------------------------------------------------------------|
|  **Conceito** | Uma elegante meio-coelha saída do País das Maravilhas (Literalmente?!) |
|   **Talento** | Sincronicidade sobrenatural (_Newtype_)                                |
| **Estudante** | Todos os horários estão na minha agenda                                |
|     **Drive** | Quero ser amiga das pessoas, mas elas são _tão estranhas_              |
|               | Baralhos e xícaras de chá... Relógios e Cartolas...                    |

## Abordagens

| ***Abordagem*** | ***Nível*** |
|----------------:|-------------|
|            Ágil | A           |
|       Cuidadoso | C           |
|         Esperto | B           |
|        Estiloso | B           |
|        Poderoso | C           |
|      Sorrateiro | D           |

## Façanhas

- _Sem querer querendo:_ +2 ao _Criar Vantagens_ sendo _Esperta_ ao realizar algo que possa aparentemente ser não intencional.
- _È tarde, é tarde, é tarde!:_ +2 ao _Superar_ obstáculos relacionados ao momentos certos (chegar na aula no momento certo)
- _"Que orelhas enormes você tem:"_ +2 ao _Superar_ e _Criar Vantagem_ para notar barulhos estranhos ou ouvir coisas que não deveria. Entretanto, qualquer falha é tratada automáticamente como sucesso a custo

## Segredos

+ Willamina é na verdade uma criatura do País das Maravilhas (literalmente). Seus objetivos na Terra são obscuros, mas está do lado da humanidade (até onde se sabe)
+ Willamina é na verdade uma _Phooka_, uma fada nascida dos sonhos das pessoas
+ Willamina na verdade é uma extraterrestre, seus pais tendo morrido em um acidente ao cairem na Terra

## O Clube dos Jogos Mentais Clássicos

+ ***Conceito:*** Clube dos Jogos Mentais Clássicos
+ ***Objetivos:*** Preservar a existência e materialidade dos Clássicos Jogos de Tabuleiro (de antes da segunda metade do Século XX)
+ ***Problema:*** Clássicos que não possuem apelos para pessoas **comuns**

### Membros:

+ Willamina Rabbitt
    + Especialista em Jogos de Carta
+ Shindou Akira 
    + Especialista em Go, 2º Ano
    + Diz ser possuído pelo fantasma de um jogador de Go do Passado Remoto
+ Yelena Griegskaya 
    + Jogadora de Xadrez e Shougi, 1º Ano
    + Diz ser "abençoada por Baba Yaga"
+ Robert Willows 
    + Jogador Inveterado de Damas e Gamão, 1º Ano
    + Aperentemente tem uma Serendipidade Sobrenatural
