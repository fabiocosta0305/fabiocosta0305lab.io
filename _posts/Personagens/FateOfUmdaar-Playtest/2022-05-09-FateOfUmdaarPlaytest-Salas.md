---
title: Salas, The Bartender
layout: personagens
categories:
  - personagens
tags:
  - Fate Of Umdaar 
header: no
language: en
---

Born as a slave in Shankar, Salas was trained from the early age as a Gladiator. Salas became one of the best ones, and with 18 you alread was a Champion from Shankar's Pit Areana. No gladiator was stronger than him. He used the money he won to buy his liberty and everything he wanted, and discovered his beloved Neskitra.

He had a good life, until Arkenahsar show some of the rebels to be executed by gladiators on his name. And it was Neskitra that was shown for him, as (he didn't know at time) she was an agent of the Freelands. He was obliged to take his warm heart for him to eat as a delicacy. They looked to each other, and Neskitra nodded, and Salas knew what to do: exactly what Arkenahsar wanted, to live to fight for another day (and for her).

Since that day, Salas wasn't the same, althought he was still someone really skilled at battle. But he needed to end his career as Gladiator on the day his left eye was ripped apart by a Tecsaurus. He joined the training grounds and a minotaur calf with lots of potential shown himself. His name: Oberon. His agent's name: Nicodemus. Nicodemus was a sneaky but boasty man when the Luck Gods smiled to him. He show Oberon, that he "bought" (he freed him immediately after, but needed to put a façade on Oberon as a Slave). The calf was really good and had potential. He became friends with Nicodemus and Oberon, and discovered he was part of the Freelands, fighting against Arkenahsar, and Laktan, his master.

This made him an ally that worked on a tavern to help Nicodemus on his cover business, while gathering intel and serving as support for the blooming resistence against Arkenahsar.

And he hopes to have the chance of "retributing the favor" he did for Neskitra on Arkenahsar.

<!-- excerpt -->

## Aspects

|          **Type** | **Aspect**                                                                               |
|------------------:|------------------------------------------------------------------------------------------|
| __High Concept:__ | A former Gladiator, friend of Nicodemus and owner of _The Fried Kr'i'ssh_                |
|   __Motivation:__ | I'll take Arkenahsar warm heart from his body, as he made me doing to my lovely Neskitra |
|   __Background:__ | My smile is the mask I use to hide my pains and losses                                   |
| __Relationship:__ | Oberon is still inexperienced, but has potential                                         |
|         __Free:__ | Have no more the skill from the past, but can do some party                              |
	
## Approaches and Skills

| **Careful (D)** | **Clever (C)** | **Flashy (A)**   | **Forceful (B)** | **Quick (F)**   | **Sneaky (E)**   |
|:---------------:|:--------------:|:----------------:|:----------------:|:---------------:|:----------------:|
| _Average (+1)_  | _Fair (+2)_    | _Good (+3)_      | _Fair (+2)_      | _Mediocre (+0)_ | _Average (+1)_   |
| Investigate     | Craft          | **Rapport (Z)**  | **Fight (S)**    | Athletics       | **Burglary (Z)** |
| **Will (Z)**    | **Lore (Z)**   | **Contacts (S)** | Physique         | Notice          | Stealth          |
| Empathy         | Provoke        | Resources        | **Drive (Z)**    | Shoot           | Deceive          |


## Stunts: [Refresh: 2]

+ ___"I know all the arena tricks, try harder!":___ when fighting on a more advantageous way for Gladiators (Tight Spaces, Exotic Weapons and so), +2 whe _Attacking_ with _Fight_
+ ___Bragging Stance:___ you can _Defend_ on Physical Attacks with _Fight_ instead of _Athletics_. If you are _Successful_ on your Defense, you can, by paying 1 FP (or forfeiting the Boost on reducing Stress), put a ___Bragging Shenaningan___ Aspect with a _Free Invoke_
+ ___To the snake pit!___ you can Throw objects by _Physique_ instead of _Shoot_ when _Attacking_
+ ___This one is on the house:___ while on _The Fried Kr'i'ssh, Salas can pay some drinks for someone, giving him +2 on any test involving gather intel about Shankar
