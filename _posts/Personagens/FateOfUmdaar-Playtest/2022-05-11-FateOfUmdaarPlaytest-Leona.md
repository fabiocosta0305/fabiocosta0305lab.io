---
title: "Leona, the Beast-Tamer"
layout: personagens
categories:
  - personagens
tags:
  - Fate Of Umdaar 
header: no
language: en
---


<!-- excerpt -->

## Aspects

|          **Type** | **Aspect**                                                            |
|------------------:|-----------------------------------------------------------------------|
| __High Concept:__ | A Human that grew with the beasts                                     |
|   __Motivation:__ | Bring back the equilibrium between Man and Nature                     |
|   __Background:__ | _"The Demiurge used Nature as playground, the Masters exploits them"_ |
| __Relationship:__ | That K-23 thing is dangerous, even being at our side                  |
|         __Free:__ | Ta'reen, my laserbear soul-sister, is my biggest ally                |
	
## Approaches and Skills

| **Careful (F)** | **Clever (B)**  | **Flashy (E)**   | **Forceful (C)** | **Quick (A)**  | **Sneaky (D)**  |
|:---------------:|:---------------:|:----------------:|:----------------:|:--------------:|:---------------:|
| _Mediocre (+0)_ | _Fair (+2)_     | _Average (+1)_   | _Fair (+2)_      | _Good (+3)_    | _Average (+1)_  |
|:---------------:|:---------------:|:----------------:|:----------------:|:--------------:|:---------------:|
| Investigate     | Craft           | Rapport          | Fight            | Athletics      | Burglary        |
| Will            | **Lore (Z)**    | **Contacts (Z)** | Physique         | **Notice (S)** | Stealth         |
| Empathy         | **Provoke (S)** | Resources        | **Drive (Z)**    | **Shoot (Z)**  | **Deceive (Z)** |
	
## Stunts: [Refresh: 3]

+ ___Heart of the Beast:___ +2 to _Create Advantages_ with _Rapport_ when dealing with her knowledge on beasts
+ ___Leader of the Pack:___ _Once per Scene_ she can bring some kind of animal (including mutants, but not inteligent beasts) and command them to act on her behalf via _Rapport_ rolls, as _Overcome_ rolls. She can't force them to do suicide actions or actions against their nature. The player picks the _bioform_ as part of a _High Concept_ Aspect and name the species (or specific individual, if wanted). The GM gives an Aspect representing the most bestial essence of the animal.
+ ___Ta'reen, the laser-bear:___ Leona has a _soul-sister_, a special animal to whom she's somewhat linked via her essence. Ta'reen can be brought to the game to join Leona on battle or do actions on her behalf. This can be done _once per session_ at free (extra calls needs 1 FP). Ta'reen can stay on scene for one scene. Ta'reen can be attacked, and her damage can be swapped with Leona and vice-versa.
  + ___Ta'reen, the laser-bear:___
	+ ___Humans are weird and foul-smelling (except Leona)___
	+ _Skilled (+2) on:_ bear stuff
	+ Stress 4, Weapon 2 (Laser Claws)
