---
title: Natva, the Skunk-Man Warrior
layout: personagens
categories:
  - personagens
tags:
  - Fate Of Umdaar 
header: no
language: en
---

Born a slave, his family and clan was sold as slave to one of the Masters. When he proved himself strong enough in combat when he was five, he was trained as a gladiator, surviving lots of robeats and fights, even when the robeast was really bigger than himself.

In his heart, however, he still wanted freedom and revenge. He faked his loyalty to the Master because he knew about his power, but also used his fame to contact some rebels that was working to topple the Master.

With time, he grew as one of the favorite of the rabble, as he defeated enemies more and more powerful in the arenas. And his power and influence grew.

It was when Natva chose that the time had came to rise the people against the Master...

A rebellion that started and fell in a blink of eye.

Natva and his allies' power and influence where less than nothing against the Artifacts and forbbiden knowledge the Master had.

Many of his allies had died, his blood mixing with the people that risen against the Master and was killed by example

Natva received a destiny worse than death,

On a very specially made ceremony, ready by the Master himself to gloat on his power, Natva was forced to drink from the ***Chalice of Changing***, a very rare Artifact that is said was created by some god or by the Demiurges. The liquid on it, from a nauseating smell and even worse taste, made Natva pass through a very shameful, humiliating transformation, that turned him form the very powerful and vigorous gladiator to a Mephitum, a skunk-man.

Why, on the populace the Mephita where always saw as raiders and dishonarable rabble and scoundrels. Add on this that he was one of the leaders of the failed rebellion, and Natva fell in disgrace. And to worse even more, the Master declared Natva a _Persona Non Grata_, someone that could be humiliated, ashamed and even killed for anyone without consequence.

No wonder he ran away to the Wilderness Nexuses in shame, wander around withou any destiny, until he found a clan of Mephita scholars called the _Speakers of Dead_, that celebrated (and chastised) the lives of those recently deceased.

During months, he stayed with them, learning the Ways of the Mephita, how to fight and control the worst piece of his shameful transformation, the nauseating stink from his tail now turned into a weapon he could control. After that time, understanding what he was now, when the Speakers of Dead came to the _Fartime Elysium_, a Freeland where he could stay with Delaware, teaching what he could for those who needed to learn the combat arts.

And Natva now lives to protect his new home.

<!-- excerpt -->

## Aspects

|          **Type** | **Aspect**                                                                             |
|------------------:|----------------------------------------------------------------------------------------|
| __High Concept:__ | An ex-Gladiator that fell of grace after a rebellion                                   |
|   __Motivation:__ | I'll protect this place that accepted me, the Fartime Elysium                          |
|   __Background:__ | Cursed as a Skunk-man - From two people, rejected by both                              |
| __Relationship:__ | Delaware accepted me, even with my condition, and we'll stay together as long he wants |
|         __Free:__ | Still dealing with his new condition                                                   |
	
## Approaches and Skills

| **Careful (B)**     | **Clever (C)** | **Flashy (E)**    | **Forceful (D)** | **Quick (A)**     | **Sneaky (F)**  |
|:-------------------:|:--------------:|:-----------------:|:----------------:|:-----------------:|:---------------:|
| _Fair (+2)_         | _Fair (+2)_    | _Average (+1)_    | _Average (+1)_   | _Good (+3)_       | _Mediocre (+0)_ |
|:-------------------:|:--------------:|:-----------------:|:----------------:|:-----------------:|:---------------:|
| **Investigate (Z)** | Craft          | Rapport           | Fight            | **Athletics (S)** | Burglary        |
| Will                | **Lore (Z)**   | Contacts          | Physique         | Notice            | Stealth         |
| **Empathy (S)**     | Provoke        | **Resources (Z)** | **Drive (Z)**    | **Shoot (Z)**     | Deceive         |

## Stunts: [Refresh: 3]

+ ___Skunk Miasma:___ Can _Attack_ with _Shoot_ with the Approach using this stink jolt. Never can be __Disarmed__.
  + ___Stink bomb:___ On a _successfull with a style_ attack_ with the ___Skunk Miasma___, can reduce the Stress by one and create a _Stinking_ zone. It works like a _Weapon 1_ Barricade
+ ___Martial Style:___ Weapon 1 when attacking bare-handed with _Athletics_. However, on a Failure, the target can strike back automatically by paying 1 Fate Point

