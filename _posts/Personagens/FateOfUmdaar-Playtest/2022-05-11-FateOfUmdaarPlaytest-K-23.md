---
title: K-23, Scholastic Android from the Past (it/it)
layout: personagens
categories:
  - personagens
tags:
  - Fate Of Umdaar 
header: no
language: en
---


<!-- excerpt -->

## Aspects

|          **Type** | **Aspect**                                                              |
|------------------:|-------------------------------------------------------------------------|
| __High Concept:__ | Automaton Scholar from before the Masters' Ascension                    |
|   __Motivation:__ | Learn what happened to the ones I was created by                        |
|   __Background:__ | The world changed a lot. Need to learn about this new world             |
| __Relationship:__ | Interesting people I have around.... New Directive: observe and counsel |
|         __Free:__ | Need to take the arrivists self-called Masters down                     |

	
## Approaches and Skills

| **Careful (A)**     | **Clever (B)**  | **Flashy (D)**   | **Forceful (E)** | **Quick (D)**   | **Sneaky (C)**  |
|:-------------------:|:---------------:|:----------------:|:----------------:|:---------------:|:---------------:|
| _Good (+3)_         | _Fair (+2)_     | _Average (+1)_   | _Average (+1)_   | _Mediocre (+0)_ | _Fair (+2)_     |
|:-------------------:|:---------------:|:----------------:|:----------------:|:---------------:|:---------------:|
| **Investigate (S)** | Craft           | Rapport          | **Fight (Z)**    | Athletics       | Burglary        |
| **Will (Z)**        | **Lore (S)**    | **Contacts (Z)** | Physique         | Notice          | **Stealth (Z)** |
| Empathy             | **Provoke (Z)** | Resources        | Drive            | Shoot           | Deceive         |

## Stunts: [Refresh: 3]

+ ___X-Ray Vision:___ While _Overcoming vision-related Obstacles_ using _Investigate_, any roll will become a step best on result (Failures became Ties, Ties become Sucesses, Sucesses become Sucesses with Style), and no vision-related Aspects can be Forced or Invoked against it _Once per scene_
+ ___Encyclopediac Memory:___ +2 on _Overcome Obstacles_ with  _Lore_ when using the sheer amounts of data and knowledge stored on its data banks
+ ___Tactical Mastery:___ On a Conflict, by sacrificing all his actions (including Defenses), he can provide +2 _Teamwork_ bonus for all his allies on a number of Zones equals his lore
