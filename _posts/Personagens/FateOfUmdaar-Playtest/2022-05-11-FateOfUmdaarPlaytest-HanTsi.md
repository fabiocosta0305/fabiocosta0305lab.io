---
title: "Han T'si, the Rabbitoh Weaponmaster"
layout: personagens
categories:
  - personagens
tags:
  - Fate Of Umdaar 
header: no
language: en
---


<!-- excerpt -->

## Aspects

|          **Type** | **Aspect**                                                                     |
|------------------:|--------------------------------------------------------------------------------|
| __High Concept:__ | A Rabbitoh Weaponmaster that saw too much horror on Umdaar even being so young |
|   __Motivation:__ | Gather the Demiurge Knowledge to improve everyone's life                       |
|   __Background:__ | The young face that hides the old heart                                        |
| __Relationship:__ | _"How interesting is this Demiurge stuff... Think we can use this..."_         |
|         __Free:__ | No one can escape from my bolas                                                |
	
## Approaches and Skills

| **Careful (B)**     | **Clever (A)**  | **Flashy (C)**  | **Forceful (F)** | **Quick (E)**  | **Sneaky (D)**  |
|:-------------------:|:---------------:|:---------------:|:----------------:|:--------------:|:---------------:|
| _Fair (+2)_         | _Good (+3)_     | _Fair (+2)_     | _Mediocre (+0)_  | _Average (+1)_ | _Average (+1)_  |
|:-------------------:|:---------------:|:---------------:|:----------------:|:--------------:|:---------------:|
| **Investigate (S)** | **Craft (S)**   | **Rapport (Z)** | Fight            | Athletics      | Burglary        |
| **Will (Z)**        | Lore            | Contacts        | Physique         | **Notice (Z)** | **Stealth (Z)** |
| Empathy             | **Provoke (Z)** | Resources       | Drive            | Shoot          | Deceive         |
	
## Stunts: [Refresh: 3]

+ ___Tidbits from the Demiurge:___ +2 to _Overcome Obstacles_ related with the lack of knowledge on the Demiurge and his Technologi with _Lore_ 
+ ___Jury-rig:___ +2 on _Creating Advantages_ using _Crafts_ while trying to use some kind of equipment he really don't have at time
+ ___Bolas:___ By becoming ___Disarmed___, +2 on _Attack_ with _Shoot_. If Sucessful, reduce Stress by 1 to put a ___Grappled___ Aspect with a Free Invoke (2 on _Success with Style_)

