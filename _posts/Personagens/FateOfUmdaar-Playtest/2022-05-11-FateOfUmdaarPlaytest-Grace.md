---
title: Grace, the Element-Bender
layout: personagens
categories:
  - personagens
tags:
  - Fate Of Umdaar 
header: no
language: en
---


<!-- excerpt -->

## Aspects

|          **Type** | **Aspect**                                              |
|------------------:|---------------------------------------------------------|
| __High Concept:__ | A small orphan raised in the Fartime Monastery          |
|   __Motivation:__ | I want to discover why I was left behind                |
|   __Background:__ | A superior mind, hunter by others                       |
| __Relationship:__ | Everyone is so nice with me, I'll protect them          |
|         __Free:__ | Still have issues to control the Element-Bending powers |
	
## Approaches and Skills

| **Careful (B)**     | **Clever (A)**  | **Flashy (E)**    | **Forceful (F)** | **Quick (C)**     | **Sneaky (D)**   |
|:-------------------:|:---------------:|:-----------------:|:----------------:|:-----------------:|:----------------:|
| _Fair (+2)_         | _Good (+3)_     | _Average (+1)_    | __Mediocre (+0)_ | _Fair (+2)_       | _Average (+1)_   |
|:-------------------:|:---------------:|:-----------------:|:----------------:|:-----------------:|:----------------:|
| **Investigate (Z)** | Craft           | Rapport           | Fight            | **Athletics (Z)** | **Burglary (Z)** |
| **Will (S)**        | **Lore (S)**    | Contacts          | Physique         | Notice            | Stealth          |
| Empathy             | **Provoke (Z)** | **Resources (Z)** | Drive            | Shoot             | Deceive          |

## Stunts: [Refresh: 2]

+ ___Elemental Bending:___ As long there's enough of the Elements she knows how to bend somewhere, she can use _Lore_ to bend those elements. She receive +2 when _Create Advantages_ using the Elements. Each Element counts as 1 Stunt and she can bend Water, Air, Light and Fire.
