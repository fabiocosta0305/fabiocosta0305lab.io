---
title: Tu'an-Yao, the Scholar from the Viswa order
layout: personagens
categories:
  - personagens
tags:
  - Fate Of Umdaar 
header: no
language: en
---

The Rabbitohs are peaciful rabbit-men from Verdant Valley, a flourish and almost self-sufficient Freeland nearby the Obsidian Valley and the Four Moon Tower, a Demiurge place as beautiful as misterious. They are easily recognized for their big, powerful haunches, that they use to jump and kick and run with prowess; big big-pupiled eyes, many times on albino pink; big cowls with big noses and nostrils that allow them to distinguish smells (with training) and big ears, some times a fifty from his full height, with is a lot as many of them are from the size of a small man, not counting the ears.

However, they never closed their eyes for the suffering and evil outside the Valley. Here and there all around you can see Rabbitohs Scholars, Warriors, Sages, Rogues and Monks helping the free peoples. And Tu'an-Yao is one of those Scholars: when he finished his scholastic training, where he learned some mind techniques with his knowledge, he was sent around as a representative from his order, the Viswa, to gather knowledge the Rabbitohs could use for improve their lives and those nearby the Verdant Valley.

He saw all kind of people and learned lot of things. He even found some comrades, like Shukul, the Crystal-blade heir; and Sheila, the Rogue that allied with them after some events. As he's not exactly a fighter, he thought it was a good idea to gather aside Shukul, Sheila and others.

Tu'an-Yao is average-sized for an Rabbitoh (165cm height, including 30cm ears). He's a little too much chubby for a Rabbitoh (that normally looks more like a hare-man than a rabbit-man), but he's nimbler than he could look. He always dresses on blue light fabrics. His big pupiled eyes are on aquamarine, and he has a grey spot over his right eye. He has only some parchment on tubes, with records from his errands and what he learned from them, and a small _chakram_, the only weapon he uses as defense.

Tu'an-Yao tries to solve everything without resorting on violence, and trust his own mind: not only on his knowledge, but his skill as an ambassador and negotiator, and on his mind powers. He doesn't cope with injustice and is a sucker for the weaker. This can even be a very big problem, as Rabbitohs are known as very dangerous fighters when put against all odds, and there's a burning flame hidden on every Rabbitohs eyes, and Tu'an-Yao isn't exception.

<!-- excerpt -->

## Aspects
	
|          **Type** | **Aspect**                                                             |
|------------------:|------------------------------------------------------------------------|
| __High Concept:__ | A Rabbit-Man Scholar                                                   |
|   __Motivation:__ | I need the Demiurges' knowledge to protect people                      |
|   __Background:__ | A unquenchable wanderlust                                              |
| __Relationship:__ | Shukul has will and power to change the world, but need the wisdom too |
|         __Free:__ | Sheila is too much hot-headed, needs to learn self-control             |

	
## Approaches and Skills

| **Careful (B)**     | **Clever (A)** | **Flashy (E)**    | **Forceful (D)** | **Quick (C)** | **Sneaky (F)**  |
|:-------------------:|:--------------:|:-----------------:|:----------------:|:-------------:|:---------------:|
| _Fair (+2)_         | _Good (+3)_    | _Average (+1)_    | _Average (+1)_   | _Fair (+2)_   | _Mediocre (+0)_ |
|:-------------------:|:--------------:|:-----------------:|:----------------:|:-------------:|:---------------:|
| **Investigate (Z)** | Craft          | Rapport           | Fight            | Athletics     | Burglary        |
| **Will (S)**        | Lore (S)       | Contacts          | Physique         | Notice        | Stealth         |
| Empathy             | **Provoke(Z)** | **Resources (Z)** | **Drive (Z)**    | **Shoot (Z)** | Deceive         |

## Stunts: [Refresh: 3]

+ ___Mindreader:___ Can use _Investigate_ as by the Approach when trying to read other's minds
+ ___Mindtwist:___ can use _Lore_ to Attack other's minds. On a _Success with a Style__ can reduce by 1 the Stress and discover an Aspect from the Target. Can't be **Disarmed**
+ ___Powerful haunches:___ +1 when _Overcoming_ or _Creating Advantages_ with _Athletics_ by using jumps or his big haunches

