---
title: Shukul, the Heir of the Crystalblade
layout: personagens
categories:
  - personagens
tags:
  - Fate Of Umdaar 
header: no
language: en
---


<!-- excerpt -->

## Aspects

|          **Type** | **Aspect**                                                  |
|------------------:|-------------------------------------------------------------|
| __High Concept:__ | Human warrior, heir of the _Crystalblade_                   |
|   __Motivation:__ | Gather power to rescue my tribe and protect the ones I love |
|   __Background:__ | Destined to defeat the Masters                              |
| __Relationship:__ | Tu'an-Yao is wise, but too much naïve                       |
|         __Free:__ | Sheila worries too much about me                            |
	
## Approaches and Skills

| **Careful (D)** | **Clever (C)** | **Flashy (E)**    | **Forceful (A)** | **Quick (B)** | **Sneaky (F)**  |
|:---------------:|:--------------:|:-----------------:|:----------------:|:-------------:|:---------------:|
| _Average (+1)_  | _Fair (+2)_    | _Average (+1)_    | _Good (+3)_      | _Fair (+2)_   | _Mediocre (+0)_ |
|:---------------:|:--------------:|:-----------------:|:----------------:|:-------------:|:---------------:|
| Investigate     | Craft          | Rapport           | **Fight (S)**    | Athletics     | Burglary        |
| **Will (Z)**    | **Lore (Z)**   | Contacts          | Physique         | Notice        | Stealth         |
| Empathy         | Provoke        | **Resources (Z)** | **Drive (Z)**    | **Shoot (Z)** | Deceive         |

## Stunts: [Refresh: 3]

+ ***The Crystalblade***
  + **Magic Weapon:** _Once per Scene_, you may loudly call out the weapon's name, granting you +2 to Fight on an Attack
  + **Light blade:** _Once per Conflict_, can choose and _Attack_ every target on the same Zone: however, this is an ___Indiscriminated Attack___, you can choose the target. Everyone in the Zone, Enemies or not, can be hit by this, and so Defends nornmally and individually
+ **Battle Cry:** +2 when _Creating Advantages_ with _Provoke_ to instill fear on targets
