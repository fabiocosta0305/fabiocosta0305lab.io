---
title: Taranis, the Centaur Runekeeper (they/them)
layout: personagens
categories:
  - personagens
tags:
  - Fate Of Umdaar 
header: no
language: en
---


<!-- excerpt -->

## Aspects

|          **Type** | **Aspect**                                                     |
|------------------:|----------------------------------------------------------------|
| __High Concept:__ | A Centaur Runekeeper that is weaker than other Centaurs        |
|   __Motivation:__ | Gather to Knowledge to fulfill his destiny: topple the Masters |
|   __Background:__ | Many know me, few are friends of mine                          |
| __Relationship:__ | Don't mess up with my friends                                  |
|         __Free:__ | Doesn't respect hierarchies                                    |
	
## Approaches and Skills

| **Careful (A)** | **Clever (B)** | **Flashy (C)**   | **Forceful (F)** | **Quick (D)**  | **Sneaky (E)**   |
|:---------------:|:--------------:|:----------------:|:----------------:|:--------------:|:----------------:|
| _Good (+3)_     | _Fair (+2)_    | _Fair (+2)_      | __Mediocre (+0)_ | _Average (+1)_ | _Average (+1)_   |
|:---------------:|:--------------:|:----------------:|:----------------:|:--------------:|:----------------:|
| Investigate     | **Craft (Z)**  | Rapport          | Fight            | Athletics      | **Burglary (Z)** |
| **Will (S)**    | **Lore (S)**   | **Contacts (Z)** | Physique         | Notice         | Stealth          |
| **Empathy (E)** | Provoke        | Resources        | Drive            | **Shoot (Z)**  | Deceive          |

## Stunts: [Refresh: 3]

+ ___Elemental Bending:___ As long there's enough of the Elements they knows how to bend somewhere, they can use _Will_ to bend those elements. They receive +2 when _Create Advantages_ using the Elements. However, on a Failure, it becomes a _Success with a Cost_ making the ___Element Run Rampant___ as a Barricade with level equal the Roll, Armor 2, Weapon 2, Stress 2. Each Element counts as 1 Stunt and they can bend Air, Light and Fire.
