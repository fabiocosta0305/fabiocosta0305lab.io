---
title: Nicodemus, The Runesearcher and Merchant
layout: personagens
categories:
  - personagens
tags:
  - Fate Of Umdaar 
header: no
language: en
---


<!-- excerpt -->

## Aspects

|          **Type** | **Aspect**                                                                           |
|------------------:|--------------------------------------------------------------------------------------|
| __High Concept:__ | A Human Merchant and skilled Negotiator with some extra tricks                       |
|   __Motivation:__ | There's loads of knowledge left behind, and I want to use them to defeat the Masters |
|   __Background:__ | Some brag doesn't kill (I think so)                                                  |
| __Relationship:__ | Salas and Oberon are my best friends                                                 |
|         __Free:__ | Arkenahsar is Shankar's living horror, and I'll topple him                           |
	
## Approaches and Skills

| **Careful (B)**     | **Clever (A)**  | **Flashy (E)**   | **Forceful (F)** | **Quick (D)**     | **Sneaky (C)**  |
|:-------------------:|:---------------:|:----------------:|:----------------:|:-----------------:|:---------------:|
| _Fair (+2)_         | _Good (+3)_     | _Average (+1)_   | _Mediocre (+0)_  | _Average (+1)_    | _Fair (+2)_     |
|:-------------------:|:---------------:|:----------------:|:----------------:|:-----------------:|:---------------:|
| **Investigate (Z)** | Craft           | Rapport          | Fight            | **Athletics (Z)** | Burglary        |
| Will                | **Lore (S)**    | **Contacts (Z)** | Physique         | Notice            | **Stealth (Z)** |
| **Empathy (S)**     | **Provoke (Z)** | Resources        | Drive            | Shoot             | Deceive         |

## Stunts: [Refresh: 3]

+ __Loremaster:__ can use _Lore_ instead of _Investigate_ when looking upon knowledge from the Past
+ ___"I think we can have some good business":___ Can use _Resources_ instead of _Contacts_ when using supplying some nice stuff for others (an Aspect he declares that can be useful for the other side) as a small "friendship token" 
+ __Inspirational Talk:__ +2 on _Creat Advantages_ with __Rapport__ to inspire people to action. They will not do suicidal actions, though
