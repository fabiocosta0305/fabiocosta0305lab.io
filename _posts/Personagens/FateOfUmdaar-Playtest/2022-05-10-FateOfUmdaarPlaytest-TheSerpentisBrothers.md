---
title: "The Serpentis Brothers, Larkvan's main Assassin"
layout: personagens
categories:
  - personagens
tags:
  - Fate Of Umdaar 
header: no
language: en
---


<!-- excerpt -->

## Aspects

|          **Type** | **Aspect**                      |
|------------------:|---------------------------------|
| __High Concept:__ | Larkvan's Serpent-men assassins |
|   __Motivation:__ | Our loyalty is with Larkvan     |
|         __Free:__ | Complementary brothers          |

	
## Approaches and Skills

| **Careful**     | **Clever**      | **Flashy**      | **Forceful**  | **Quick**      | **Sneaky**       |
|:---------------:|:---------------:|:---------------:|:-------------:|:--------------:|:----------------:|
| _Fair (+2)_     | _Fair (+2)_     | _Mediocre (+0)_ | _Good (+3)_   | _Good (+3)_    | _Good (+3)_      |
| Investigate     | Craft (Z)       | Rapport         | **Fight (S)** | Athletics      | **Burglary (Z)** |
| Will            | **Lore (Z)**    | Contacts        | Physique      | **Notice (Z)** | **Stealth (S)**  |
| **Empathy (Z)** | **Provoke (S)** | Resources       | **Drive (Z)** | **Shoot (S)**  | Deceive          |

## Stunts: 

+ __Camouflage:__ +2 on _Creating Advantages_ with _Stealth_ to hide themselves 
+ __Bite:__ +2 on _Attack_ with _Fight_ against ___Constrained___ targets
  + __Venom:__ on a _Success with Style_ on a ___Bite___ attack, can reduce the Stress on 1 to put a ***Fair (+2), Weapon 1 Serpent Venon*** on the target, as a _Hazard_. This _Hazard_ rolls in the same turn the character.
+ __Acid Spit:__ +2 on _Attack_ with _Shoot_
