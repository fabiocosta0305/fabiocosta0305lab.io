---
title: Sheila, the Cat-Woman Rogue
layout: personagens
categories:
  - personagens
tags:
  - Fate Of Umdaar 
header: no
language: en
---


<!-- excerpt -->

## Aspects

|          **Type** | **Aspect**                                                          |
|------------------:|---------------------------------------------------------------------|
| __High Concept:__ | Numan (Cat-woman) scout that doesn't know the concept of _property_ |
|   __Motivation:__ | Know ALL THE PLACES!!!                                              |
|   __Background:__ | Gather ALL THE ARTIFACTS!!!                                         |
| __Relationship:__ | Don't mess up with my friends                                       |
|         __Free:__ | Doesn't respect hierarchies                                         |
	
## Approaches and Skills

| **Careful (C)**     | **Clever (F)**  | **Flashy (E)**  | **Forceful (D)** | **Quick (A)**     | **Sneaky (B)**   |
|:-------------------:|:---------------:|:---------------:|:----------------:|:-----------------:|:----------------:|
| _Fair (+2)_         | _Mediocre (+0)_ | _Average (+1)_  | _Average (+1)_   | _Good (+3)_       | _Fair (+2)_      |
|:-------------------:|:---------------:|:---------------:|:----------------:|:-----------------:|:----------------:|
| **Investigate (Z)** | Craft           | **Rapport (Z)** | Fight            | **Athletics (S)** | **Burglary (S)** |
| Will                | Lore            | Contacts        | Physique         | **Notice (Z)**    | Stealth          |
| Empathy             | Provoke         | Resources       | **Drive (Z)**    | Shoot             | **Deceive (Z)**  |

## Stunts: [Refresh: 3]

+ **Claws:** Weapon 1 when _Fighting_ bare-handed, on a _Success with a Style_ becomes Weapon 2. Can't be ___Disarmed___
+ **Chakram:** Weapon 2 when _Attacking_ with _Shoot_, ___Disarmed___ on a Failure
+ **Heightened Senses:** can use _Notice_ as by the Approach if using her senses to _Overcoming obstacles_ 
