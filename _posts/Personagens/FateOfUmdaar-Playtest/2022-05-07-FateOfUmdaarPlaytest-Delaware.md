---
title: Delaware, the Shooter
layout: personagens
categories:
  - personagens
tags:
  - Fate Of Umdaar 
header: no
language: en
---

All the Masters of Umdaar needs warriors and lieutenants and other lackey. Some came in the form of slaves, but some are taken under their wings from local people. There's even those that _legitly_ believes the Masters wants everyone's wellbeing, and that slaves and rebels are subversive scroundels and culgels at large.

Delaware was one of those men.

From a family aligned with some of the Masters, he was trained since a child to be a soldier, until he became one of  the Master's many lieutenants, receiving orders straigth from himself, and receiving from him his _Plasma Crossbow_, that he skillfully used to kill ciminals and subversives.

That untli he saw the truth.

When he came back home from a common mission, he found it empty. Father, Mother, brothers... All gone. No message. No nothing.

He searched and searched all aroung the Dread Domain, when he found his father's diary, and discovered his father wanted to tackle down the Master, as he know all the criminal horrors the Master did, how he use slaves and rebels (like Natva) on terrible experiments, how he throw children as staple to the laser-bears.

Disiluded and lost on himself, Delaware defected the Master, looking for his family.

If the Master was the one behind their abduction, certainly it would not reveal: Delaware always was strong, only his loyalty avoided him to rise an insurgency against the Master.

But there would have a chance that at least some of his family was safe under Archeonauts protections, or under the Freelands that fight against the Masters' incursions.

With only his crossbow, he became a Soldier of Fortune, looking for clues for his family. Obviously, he got into Freelands and they discovered his past and, even under some (justified, to be honest) grudge, they accept (somewhat) him under their banners.

But, even helping the Freelands and Archeonauts, he is drive to find his family.

And, if the Master was his abductor, Delaware have a called shot for him


<!-- excerpt -->

## Aspects

|          **Type** | **Aspect**                                                |
|------------------:|-----------------------------------------------------------|
| __High Concept:__ | A Human Shooter that fails no shots, but has a shady past |
|   __Motivation:__ | My family disappeared, and I want to discover why         |
|   __Background:__ | Defector from the Masters' troops                         |
| __Relationship:__ | Natva is an pitiful outcast like me, and so have my back  |
|         __Free:__ | A painful truth is better then a sugar-coated lie         |
	
## Approaches and Skills

| **Careful (B)**     | **Clever (D)** | **Flashy (F)**  | **Forceful (C)** | **Quick (A)**  | **Sneaky (E)**  |
|:-------------------:|:--------------:|:---------------:|:----------------:|:--------------:|:---------------:|
| _Fair (+2)_         | _Average (+1)_ | _Mediocre (+0)_ | _Fair (+2)_      | _Good (+3)_    | _Average (+1)_  |
|:-------------------:|:--------------:|:---------------:|:----------------:|:--------------:|:---------------:|
| **Investigate (S)** | Craft          | Rapport         | Fight            | Athletics      | Burglary        |
| Will                | **Lore (Z)**   | Contacts        | Physique         | **Notice (Z)** | Stealth         |
| **Empathy (Z)**     | Provoke        | Resources       | **Drive (Z)**    | **Shoot (S)**  | **Deceive (Z)** |

## Stunts: [Refresh: 3]

+ ___Plasma Crossbow:___ Weapon: 1
  + __Plasma:__ When _Successful with Style_ when attacking with the Crossbow, I can reduce Stress in 1 to place an ___Plasmaburn___ Aspect on the hitted target, with a Free Invoke, as long the targer is inflammable
+ ___Battle Senses:___ while on a _Conflict_, I can use _Notice_ to Defend myself as by the Approach

