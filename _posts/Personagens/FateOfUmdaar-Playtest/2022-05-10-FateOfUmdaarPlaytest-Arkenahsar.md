---
title: Arkenahsar, Shankar Dictator
layout: personagens
categories:
  - personagens
tags:
  - Fate Of Umdaar 
header: no
language: en
---


<!-- excerpt -->

## Aspects

|          **Type** | **Aspect**                                                  |
|------------------:|-------------------------------------------------------------|
| __High Concept:__ | Larkvan's, Master of the Obsidian Desert, Second-in-command |
|   __Motivation:__ | Uphold and gather even more power (preferably for himself)  |
|   __Background:__ | Blood-tainted opulence                                      |
| __Relationship:__ | Larkvan has my undying loyalty... _By Now_                  |
|         __Free:__ | Love power games (preferably those he can win)              |
	
## Approaches and Skills

| **Careful**     | **Clever**      | **Flashy**        | **Forceful**     | **Quick**         | **Sneaky**       |
|:---------------:|:---------------:|:-----------------:|:----------------:|:-----------------:|:----------------:|
| _Average (+1)_  | _Good (+3)_     | _Fair (+2)_       | _Fair (+2)_      | _Average (+1)_    | _Good (+3)_      |
|:---------------:|:---------------:|:-----------------:|:----------------:|:-----------------:|:----------------:|
| Investigate     | **Craft (Z)**   | **Rapport (Z)**   | Fight            | **Athletics (Z)** | **Burglary (Z)** |
| Will            | Lore            | Contacts          | **Physique (Z)** | Notice            | **Stealth (S)**  |
| **Empathy (Z)** | **Provoke (S)** | **Resources (S)** | Drive            | **Shoot (S)**     | Deceive          |

## Stunts: 

+ ___"Guards! Guards!":___ once each three turns, can bring _City Guards_ on a number equals the result of a _Resources_ roll
+ __Power Cane:__ +2 on any roll to _Create_ obstacles
+ __Energy Shot:__ on a _Success with style_ when _Attacking_ with _Shoot_, can reduce on 1 the result to push the target back 2 Zones
