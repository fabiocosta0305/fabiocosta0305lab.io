---
title: "Sa'akhir, the _Faeryn_ bard"
layout: personagens
categories:
  - personagens
tags:
  - Fate Of Umdaar 
header: no
language: en
---


<!-- excerpt -->

## Aspects

|          **Type** | **Aspect**                                                    |
|------------------:|---------------------------------------------------------------|
| __High Concept:__ | A Gallant _Faeryn_ bard looking for stories and their glories |
|   __Motivation:__ | I need to collect all histories in Umdaar                     |
|   __Background:__ | Bold, boasty and chilvarious even under pressure              |
| __Relationship:__ | Natva's history is so tragic everyone in Umdaar should know   |
|         __Free:__ | One of the last _Faeryn_ alive                                |

	
## Approaches and Skills

| **Careful (F)** | **Clever (B)** | **Flashy (A)**    | **Forceful (E)** | **Quick (D)**   | **Sneaky (C)**  |
|:---------------:|:--------------:|:-----------------:|:----------------:|:---------------:|:---------------:|
| _Mediocre (+0)_ | _Fair (+2)_    | _Good (+3)_       | _Average (+1)_   | _Average (+1)_  | _Fair (+2)_     |
|:---------------:|:--------------:|:-----------------:|:----------------:|:---------------:|:---------------:|
| Investigate     | **Craft (Z)**  | **Rapport (S)**   | Fight            | Athletics       | Burglary        |
| Will            | **Lore (S)**   | Contacts          | **Physique (Z)** | **Notice  (Z)** | **Stealth (Z)** |
| Empathy         | Provoke        | **Resources (Z)** | Drive            | Shoot           | Deceive         |

## Stunts: [Refresh: 3]

+ ___Inspiration Songs:___ as long he can be heard and sacrifice all his actions (including Defenses), he can provide +2 _Teamwork_ bonus for all actions in a turn, _once per scene_.
+ ___Love and War Songs:___ +2 on _Creating Advantages_ with _Rapport_ when using his songs and histories to inspire or remember past things
+ ___Scoff and cuss Songs:___ +2 on _Attack_ with _Provoke_ when using his songs and histories to scorn or provoke fear on enemies

