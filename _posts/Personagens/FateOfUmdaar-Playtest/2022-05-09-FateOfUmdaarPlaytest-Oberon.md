---
title: Oberon, The Minotaur Gladiator
layout: personagens
categories:
  - personagens
tags:
  - Fate Of Umdaar 
header: no
language: en
---


<!-- excerpt -->

## Aspects

|          **Type** | **Aspect**                                                       |
|------------------:|------------------------------------------------------------------|
| __High Concept:__ | An Minotaur Gladiator allied to the Freelands                    |
|   __Motivation:__ | Help Nicodemus as possible                                       |
|   __Background:__ | A bumpkin in the heart, with simple tastes and simpler ambitions |
| __Relationship:__ | He knows what he is, and what he ___isn't___                     |
|         __Free:__ | A heart as big as his strenght                                   |
	
## Approaches and Skills

| **Careful (D)**     | **Clever (F)**  | **Flashy (C)**    | **Forceful (A)** | **Quick (B)**     | **Sneaky (E)**   |
|:-------------------:|:---------------:|:-----------------:|:----------------:|:-----------------:|:----------------:|
| _Average (+1)_      | _Mediocre (+0)_ | _Fair (+2)_       | _Good (+3)_      | _Fair (+2)_       | _Average (+1)_   |
|:-------------------:|:---------------:|:-----------------:|:----------------:|:-----------------:|:----------------:|
| **Investigate (Z)** | Craft           | Rapport           | Fight            | **Athletics (S)** | **Burglary (Z)** |
| Will                | Lore            | Contacts          | **Physique (S)** | Notice            | Stealth          |
| Empathy             | Provoke         | **Resources (Z)** | **Drive (Z)**    | **Shoot (Z)**     | Deceive          |

## Stunts: [Refresh: 2]

+ __Bull's Fight:__ can use _Physique_ instead of _Fight_ to _Attack_ bare-handed
+ __Bull's Strength:__ can use _Physique_ instead of _Shoot_ to throw objects
+ ___"The same attack doesn't work two times against me!":___ if someone phisically attacks sequentially with the same technique, weapon or descriptions (or no description at all), Oberon receives +2 on his Defense. 

