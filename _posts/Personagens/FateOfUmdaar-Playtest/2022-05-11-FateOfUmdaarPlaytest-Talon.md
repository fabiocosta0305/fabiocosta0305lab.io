---
title: "Talon, the Birdman Scout"
layout: personagens
categories:
  - personagens
tags:
  - Fate Of Umdaar 
header: no
language: en
---


<!-- excerpt -->

## Aspects

|          **Type** | **Aspect**                                                   |
|------------------:|--------------------------------------------------------------|
| __High Concept:__ | A Birdman Scout that lives for his own and his tribe's honor |
|   __Motivation:__ | I'll see the Ancient Escarp free from Master's stench        |
|   __Background:__ | A old-fashioned, and sometimes antiquated, scout and warrior |
| __Relationship:__ | I need to be the scout people needs me to be                 |
|         __Free:__ | Run, fly, swim, fight, hike, fish, hunt and think            |

	
## Approaches and Skills

| **Careful (B)**     | **Clever (A)** | **Flashy (F)**  | **Forceful (E)** | **Quick (C)** | **Sneaky (D)**  |
|:-------------------:|:--------------:|:---------------:|:----------------:|:-------------:|:---------------:|
| _Fair (+2)_         | _Good (+3)_    | _Mediocre (+0)_ | _Average (+1)_   | _Fair (+2)_   | _Average (+1)_  |
|:-------------------:|:--------------:|:---------------:|:----------------:|:-------------:|:---------------:|
| **Investigate (S)** | **Craft (Z)**  | Rapport         | Fight            | Athletics     | Burglary        |
| Will                | **Lore (S)**   | Contacts        | Physique         | Notice        | Stealth         |
| **Empathy (Z)**     | Provoke        | Resources       | **Drive (Z)**    | **Shoot (Z)** | **Deceive (Z)** |
	
## Stunts: [Refresh: 3]

+ ___Charge:___ if he have enought space to ready it, he can receive +2 when _Attacking_ using _Athletics_ falling from the sky against his targets. However, on a Failure, suffers an Attack with Stress equals the difference to be Successful.
+ ___Eagle Eyes:___ +2 when _Overcoming Obstacles_ with _Investigate_ when looking and finding things on distance
+ ___Grappling Hook:___ +2 on _Attack_ with _Shoot_ 

