---
teaser: O Podcast dos Algozes dos Jogadores!
subheadline: O Autômato da Era dos Demiurgos que Despertou para derrubar os Mestres de Umdaar
title:  Sargon, o Cristalóide
layout: personagens
categories:
  - personagens
tags:
  - Mestres de Umdaar 
header: no
language: br
---

O povo cristalóide foi criado pelos Demiurgos a partir dos Cristais de Comunicação e de uma combinação de magia e superciência, algo além da imaginação até mesmo dos Mestres de Umdaar e dos Guardiões das Runas. Com o tempo, esses servos tornaram-se sencientes e leais a seus senhores, desenvolvendo seus próprios mecanismos em uma forma alienígena de vida, algo fora do padrão, reproduzindo-se por meio de uma estranha cisão, onde aos poucos seus pais perdiam a vida para dar origem aos filhos.

Sargon foi um dos últimos cristalóides a nascer, e portanto pode ver a terrível verdade que levou ao desaparecimento dos Demiurgos, ao mesmo tempo em que para ele foi confiado segredos que ele deve manter seguro até que Umdaar esteja pronta.

Sargon vive de certa forma solitário em meio ao Vale dos Cristais, cercado pelas formações que na realidade são os corpos e as memórias dos seus amigos e família, cristalizados em um sono sem volta que apenas os cristalóides são capazes de compreender.

Sargon possui poucos amigos e contato com o mundo fora do Vale dos Cristais, aproveitando o fato que tanto o Pântano Necrótico quanto o Pico dos Anciões se tornam uma proteção ao mesmo. Ele apenas deseja observar, até o momento em que os segredos confiados a ele pelos seus senhores, os Demiurgos, puderem ser revelados.


## Informações Iniciais

+ _Bioforma:_ Ser de Energia
+ _Classe:_ Sábio

## Aspectos

|       **Tipo** | **Aspecto**                                                            |
|---------------:|------------------------------------------------------------------------|
|  __Conceito:__ | Último (!?) Herdeiro do Povo de Cristal                                |
| __Motivação:__ | Proteger o conhecimento sagrado dos Demiurgos                          |
|   __Pessoal:__ | Umdaar não está preparada para a verdade                               |
|                | Parte de uma mente coletiva, ainda que tenha sua própria personalidade |
|                | Sabe segredos demais para seu próprio bem                              |

## Abordagens

|  **Abordagem** | **Nível**     |
|---------------:|---------------|
|       __Ágil__ | Medíocre (+0) |
|  __Cuidadoso__ | Razoável (+2) |
|    __Esperto__ | Bom (+3)      |
|   __Estiloso__ | Regular (+1)  |
|   __Poderoso__ | Razoável (+2) |
| __Sorrateiro__ | Regular (+1)  |

## Façanhas: [Recarga: 3]

+ ___Raio Energético (Poderoso):___ Escolha um tipo de energia — como fogo, luz, eletricidade ou gravidade. Sempre que você tiver um sucesso com estilo num ataque Poderoso ou Estiloso, poderá ignorar o impulso que ganharia e mover o defensor por até duas zonas. Além disso, você nunca será  ___Desarmado.___
+ ___Superforça (Poderoso):___ Uma vez por cena, quando tentar superar um obstáculo tentando quebrá-lo de modo Poderoso, ganhe um _Pulso de Performance_. Sempre que usar esta façanha, coloque Escombros como um  specto de situação sem invocações gratuitas.
+ ___Análise Estatística (Esperto):___ Você pode sentir o cheiro do problema. Uma vez por cena, ao se defender de modo Esperto contra uma armadilha ou outra ameaça inanimada, você pode usar um Pulso de Performance no resultado.
