---
title: "Ritsu \"Cyberia\" Shimitzu"
subheadline: A insana construtora de robôs do Sket-Dan do Colégio Itazura
layout: personagens
categories:
 - personagens
comments: true
tags:
  - Bukatsu! Sket Dance - New Dance Generation
language: br
---

## Aspectos

|                  ___Tipo___ | ___Aspectos___                                       |
|----------------------------:|:-----------------------------------------------------|
|                     __Ano__ | 2                                                    |
|                  __Clubes__ | Sket-Dan/Robótica                                    |
|             __Estereótipo__ | Primeiro Lugar do Japão no Estudo                    |
|           __Alívio Cômico__ | _Otaku_ de robôs                                     |
|                    __Lema__ | _"Mil HP? Fala sério, isso não dá nem para a saída!”_ |
|                __Objetivo__ | Construir um _mecha_ economicamente viável           |
|          __Tipo Sanguíneo__ | AB                                                   |
| __Característica Positiva__ | Adaptável                                            |
| __Característica Negativa__ | Severa                                               |

## Abordagem

| ___Abordagem___ | ___Nível___   |
|----------------:|---------------|
|        __Agil__ | Razoável (+2) |
|   __Cuidadoso__ | Medíocre (+0) |
|     __Esperto__ | Bom (+3)      |
|    __Estiloso__ | Regular (+1)  |
|    __Poderoso__ | Regular (+1)  |
|  __Sorrateiro__ | Razoável (+2) |

## Façanhas [ Recarga: 3 ]

+ _**Troubleshoot** 喝 :_ Ao analisar a situação visando ajudar a resolver problemas, o Sket-Dan consegue enxergar, graças ao fato de ver de fora, coisas que o personagem não conseguiria ver. Role contra _Esperto_, dificuldade _Medíocre (+0)_. Cada tensão resultante gera um Aspecto adicional em jogo, mas apenas um dele recebe a invocação gratuíta (dois, em caso de Sucesso com Estilo)
+ _**Estudante Compulsivo:**_ você está sempre estudando novas matérias para não perder seu posto de melhor aluno. A cada nova sessão você pode adicionar +1 ao valor de duas abordagens representando como seus estudos atuais aumentam suas capacidades gerais. Até mesmo as físicas! Este bônus permanece até o final da sessão.
+ _**Controle Neural:**_ você pode controlar seu robô à distância, mesmo que não esteja na cena. Desta forma é possível ir à enfermaria recuperar-se e ainda continuar jogando, por exemplo.
