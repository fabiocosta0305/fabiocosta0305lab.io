---
title: "Kuromatsu “Kura” Fukugawa"
subheadline: O Luchador genérico do Sket-Dan do Colégio Itazura
layout: personagens
categories:
 - personagens
comments: true
tags:
  - Bukatsu! Sket Dance - New Dance Generation
language: br
---

## Aspectos

|                  ___Tipo___ | ___Aspectos___                          |
|----------------------------:|:----------------------------------------|
|                     __Ano__ | 2                                       |
|                  __Clubes__ | Sket-Dan/_Puroresu_ (Luta-Livre)        |
|             __Estereótipo__ | Protagonista Genérico                   |
|           __Alívio Cômico__ | Sempre usando máscara de _Luchador_     |
|                    __Lema__ | “La passion de la Lucha _vive em mim!”_ |
|                __Objetivo__ | Ser campeão de Puroresu                 |
|          __Tipo Sanguíneo__ | B                                       |
| __Característica Positiva__ | Ativo                                   |
| __Característica Negativa__ | Imprevisível                            |

## Abordagem

| ___Abordagem___ | ___Nível___   |
|----------------:|---------------|
|        __Agil__ | Razoável (+2) |
|   __Cuidadoso__ | Bom (+3)      |
|     __Esperto__ | Regular (+1)  |
|    __Estiloso__ | Razoável (+2) |
|    __Poderoso__ | Regular (+1)  |
|  __Sorrateiro__ | Medíocre (+0) |

## Façanhas [ Recarga: 5 ]

+ _**Kindness:**_ Ser carinhoso com o próximo permite ao Sket-Dan oferecer um ombro amigo para seus amigos, e compreender melhor os problemas que estão acontecendo. +2 ao Superar sendo Esperto quando tentar entender o que está acontecendo;
+ _**Contrapeso Genérico:**_ Sua recarga é de cinco pontos, ao contrário da normal de três. 
+ _**La Passion de La Lucha: 喝**_ Ao usar La Passion de La Lucha, você recebe +2 em todos os seus ataques de mãos vazias, independentemente da Abordagem, desde que exista uma plateia assistindo sua Luta.
