---
title: "Sayuki \"Sniper\" Kimishima"
subheadline: A maníaca por armas de airsoft do Sket-Dan do Colégio Itazura
layout: personagens
categories:
 - personagens
comments: true
tags:
  - Bukatsu! Sket Dance - New Dance Generation
language: br
---

## Aspectos

|                  ___Tipo___ | ___Aspectos___                               |
|----------------------------:|:---------------------------------------------|
|                     __Ano__ | 2                                            |
|                  __Clubes__ | Sket-Dan/Paintball                           |
|             __Estereótipo__ | Garota Moleca (Bokukko)                      |
|           __Alívio Cômico__ | _Trigger Happy_ maníaca por armas            |
|                    __Lema__ | _“Um tiro, um alvo”_                         |
|                __Objetivo__ | Entrar para as Forças de Autodefesa do Japão |
|          __Tipo Sanguíneo__ | O                                            |
| __Característica Positiva__ | Atlética                                     |
| __Característica Negativa__ | Fria                                         |

## Abordagem

| ___Abordagem___ | ___Nível___   |
|----------------:|---------------|
|        __Agil__ | Razoável (+2) |
|   __Cuidadoso__ | Bom (+3)      |
|     __Esperto__ | Regular (+1)  |
|    __Estiloso__ | Regular (+1)  |
|    __Poderoso__ | Razoável (+2) |
|  __Sorrateiro__ | Medíocre (+0) |

## Façanhas [ Recarga: 3 ]

+ ___Support:___ Ao apoiar os outros, o Sket-Dan sempre aprende como o fazer de maneira mais efetiva que o normal. Ao realizar um teste de Criar Vantagem com o objeto de oferecer apoio a outro por meio de um Aspecto, recebe +2 no seu teste;
+ ___Precisão Misteriosa:___ Uma vez por conflito, adicione uma invocação grátis de uma vantagem criada que represente o tempo gasto apontando ou alinhando o tiro (como Na Mira).
+ ___Força Deselegante:___ você é boa em qualquer coisa de menino, mas em poucas de menina. Você sempre rola 5DF quando usar Poderosa, mas rola apenas 2DF quando for fazer qualquer coisa de menina.
