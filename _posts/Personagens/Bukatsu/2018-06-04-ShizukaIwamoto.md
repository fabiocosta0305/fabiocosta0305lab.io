---
title: "Shizuka “Ou” Iwamoto"
subheadline: A sacerdotisa jogadora de shogi do Sket-Dan do Colégio Itazura
layout: personagens
categories:
 - personagens
comments: true
tags:
  - Bukatsu! Sket Dance - New Dance Generation
language: br
---

## Aspectos

|                  ___Tipo___ | ___Aspectos___                                   |
|----------------------------:|:-------------------------------------------------|
|                     __Ano__ | 2                                                |
|                  __Clubes__ | Sket-Dan/Shogi                                   |
|             __Estereótipo__ | Sacerdotisa Xintoísta (Miko)                     |
|           __Alívio Cômico__ | Sempre pronta para uma partida                   |
|                    __Lema__ | _"O_ tao _flui. O inimigo Avança. A força vem.”_ |
|                __Objetivo__ | Manter o templo da família                       |
|          __Tipo Sanguíneo__ | O                                                |
| __Característica Positiva__ | Otimista                                         |
| __Característica Negativa__ | Fria                                             |

## Abordagem

| ___Abordagem___ | ___Nível___   |
|----------------:|---------------|
|        __Agil__ | Regular (+1)  |
|   __Cuidadoso__ | Razoável (+2) |
|     __Esperto__ | Bom (+3)      |
|    __Estiloso__ | Medíocre (+0) |
|    __Poderoso__ | Regular (+1)  |
|  __Sorrateiro__ | Razoável (+2) |

## Façanhas [ Recarga: 3 ]

+ _**Support:**_ Ao apoiar os outros, o Sket-Dan sempre aprende como o fazer de maneira mais efetiva que o normal. Ao realizar um teste de Criar Vantagem com o objeto de oferecer apoio a outro por meio de um Aspecto, recebe +2 no seu teste;
+ _**Clériga Oriental:**_ você tem uma reserva de pontos espirituais que você pode usar para determinados fins, gastando a quantidade que quiser desde que não ultrapasse seu limite diário de 6 Pontos. 
  + Conceder um dado extra para si ou para um aliado a cada ponto espiritual gasto ou Remover um dado de um inimigo de forma similar;
  + Remover o dano de estresse de um aliado (uma caixa de valor 2 custa 2 pontos, etc.);
  + Criar um aspecto situacional adverso contra um inimigo na forma de maldição ou prece, gastando 2 pontos para tal, que permanece até o final da sessão.
+ _**Drop 喝 :**_ Pode invocar um Inimigo previamente derrotado como um aliado, sem custo nenhum
