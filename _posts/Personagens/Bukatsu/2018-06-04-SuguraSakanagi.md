---
title: "Sugura \"Portos\" Sakanagi"
subheadline: O espadachim cavalheiresco do Sket-Dan do Colégio Itazura
layout: personagens
categories:
 - personagens
comments: true
tags:
  - Bukatsu! Sket Dance - New Dance Generation
language: br
---

## Aspectos

|                  ___Tipo___ | ___Aspectos___                                    |
|----------------------------:|:--------------------------------------------------|
|                     __Ano__ | 2                                                 |
|                  __Clubes__ | Sket-Dan/Esgrima                                  |
|             __Estereótipo__ | Aluno Popular                                     |
|           __Alívio Cômico__ | _Flamboyant_ Anacronista                          |
|                    __Lema__ | _“Sinta o fio da minha espada, seu sacripantas!”_ |
|                __Objetivo__ | Formar uma Escola de Esgrima                      |
|          __Tipo Sanguíneo__ | B                                                 |
| __Característica Positiva__ | Ativo                                             |
| __Característica Negativa__ | Irresponsável                                     |

## Abordagem

| ___Abordagem___ | ___Nível___   |
|----------------:|---------------|
|        __Agil__ | Razoável (+2) |
|   __Cuidadoso__ | Regular (+1)  |
|     __Esperto__ | Regular (+1)  |
|    __Estiloso__ | Bom (+3)      |
|    __Poderoso__ | Razoável (+2) |
|  __Sorrateiro__ | Medíocre (+0) |

## Façanhas [ Recarga: 3 ]

+ __*Groupies* (Os Mosqueteiros):__ 喝 você pode comandar seus seguidores, invocando 4 alunos genéricos para fazerem algo por você, inclusive lutar. Estes seguidores podem ser simplesmente fãs ou qualquer grupo de pessoas que faça sentido estar com você, como uma equipe de filmagem para uma atriz famosa, por exemplo. Eles somem ao final da cena.
+ _**Encouragement:**_ Encorajando os outros, o Sket-Dan faz com que tudo fique mais fácil de suportar. O Integrante do Sket-Dan pode oferecer Trabalho em Equipe para Defesa, desde que não seja por meio de uma Abordagem na qual ele seja Medíocre (+0). Além disso, ele recebe +1 para Trabalho em Equipe quando o fizer, para um total de +2;
+ _**Reviravolta:**_ em caso de Sucesso em uma Defesa pode, pagando 1 Ponto de Destino, Atacar automaticamente o atacante, rolando normalmente o Ataque. Isso não conta como sua ação.
