---
title:  Jenna Sheridan, a Chefe da Equipe
subheadline: A chefe jornalista da Weird World News UK
layout: personagens
categories:
 - personagens
tags:
 - Weird World News
header: no
language: br
---

Jenna é a "chefe" da equipe da Weird World News UK, já que ela é a "face" da equipe, quem aparece quando as matérias vão ao ar. 

Jenna fez a faculdade de jornalismo e nunca foi muito aplicada, mas ela realmente se interessa em pesquisar criptídios.

Ou melhor, passou a se interessar quando eles viram um fantasma legítimo ocupando um castelo abandonado!

Jenna conseguiu negociar uma solução para que o castelo pudesse ser usado e o fantasma não ser incomodado, e chegou até a convencer ele a trabalhar com os donos para ser um "Castelo Genuinamente Assombrado" nos Halloween.

Mas ela ficou também possessa com pessoas que usam criptídios como uma forma de manipular as coisas. Isso a torna muito cética quanto a ser ou não criptídios envolvidos.

Ela não é muito boa como investigadora, mas tem seus pontos fortes, e é quem coordena as coisas, inclusive sendo a pessoa que resolve conflitos e concilia as coisas. No fundo, todos confiam na pequena garota que parece ter saído (e saiu) de um condomínio popular para o "estrelato" na Weird World News UK

## Aspectos

|          **Tipo** | **Aspectos**                              |
|------------------:|-------------------------------------------|
|    ___Conceito___ | Jornalista Investigativa Inveterada       |
| ___Dificuldade___ | Cética até o último momento               |
|   ___Motivação___ | Contar as histórias envolvidas na notícia |

## Atitudes

| ___Correr!___ | ___Enganar!___ | ___Enredar!___ | ___Investigar!___ |
|:-------------:|:--------------:|:--------------:|:-----------------:|
| Razoável (+2) | Bom (+3)       | Regular (+1)   | Regular (+1)      |

## Façanhas (Recarga: 3)

+ ___Conheço um Cara que sabe isso___ – +2 ao _Criar Vantagens_ em _Investigar!_, desde que tenha como se comunicar com outras pessoas, ou de outra forma passar informações adiantes para outras pessoas
+ ___Revelador Supremo___ – Quando revelada a identidade do monstro você pode gastar 1 ponto de Destino para adicionar dois detalhes narrativos sobre a motivação e história do mesmo.

## Condições

| ___Valor___ | ___Condição___             |
|:-----------:|:--------------------------:|
| `1`{: .fate_font}           | Assustada                  |
| `1`{: .fate_font}           | Deixa eu ver minhas notas! |
| `2`{: .fate_font}           | Coração na mão!            |
| `2`{: .fate_font}           | Corta! Corta!!!            |

