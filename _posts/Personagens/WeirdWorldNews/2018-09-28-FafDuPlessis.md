---
title:  Faf du Plessis, Caçador de Emoções
subheadline: O Sul-Africano caçador de emoções da Weird World News UK
layout: personagens
categories:
 - personagens
tags:
 - Weird World News
header: no
language: br
---



Faf nasceu na África do Sul em uma transição do regime do _Apartheid_ para a _Nação Arco Íris_. Alto e razoavelmente forte, chegou a tentar o Rugby como todo garoto _afrikaans_, mas ele não tinha a força física necessária, ainda que até a juventude tenha sido um Asa razoavelmente competente. Mas continuou ligado em esportes, tendo tentado também cricket e surfe.

Foi quando, uma certa vez alguns amigos ficaram apavorados: um tubarão gigante, vindo da pré-história, começou a atacar os surfistas. Faf e seus amigos tiveram que se arriscar para manter o _point_...

...e descobriram que na verdade eram piratas usando um submarino para enganar as pessoas e fazer todos fugirem de seu ponto de refúgio.

A Interpol acabou capturando os caras, mas isso colocou a cabeça de Faf a prêmio pelos piratas, e ele teve que ir para Inglaterra para ficar mais seguro. Acabou formando-se em fotojornalismo, especializado em esportes de ação. Mas uma maré de azar no serviço (mais algumas câmeras destruídas) o fizeram ir parar na Weird World News UK. Lá, entretanto, a coisa ficou muito melhor, com um horário flexível que permite a ele tentar alguns esportes radicais quando pode. E pegar uns sacanas também é muito legal, quando não encontram algo verdadeiro, como Leroy, Lassie ou Ellie.

## Aspectos

|          **Tipo** | **Aspectos**                                 |
|------------------:|----------------------------------------------|
|    ___Conceito___ | Caçador de Emoções Sul-Africano              |
| ___Dificuldade___ | Viciado em Adrenalina perseguido por piratas |
|   ___Motivação___ | _”Quanto mais radical melhor!”_              |

## Atitudes

| ___Correr!___ | ___Enganar!___ | ___Enredar!___ | ___Investigar!___ |
|:-------------:|:--------------:|:--------------:|:-----------------:|
| Bom (+3)      | Razoável (+2)  | Regular (+1)   | Regular (+1)      |

## Façanhas (Recarga: 3)

+ ___PARKOUR!!!___ – +2 em todos os rolamentos de _Correr!_ durante cenas de Perseguição, desde que em locais ou situações perigosas.
+ ___Dando no pé!___ – +2 em seu primeiro rolamento de _Correr!_ em uma cena quando correndo atrás de, ou fugindo de, um monstro

## Condições

| ___Valor___ | ___Condição___             |
|:-----------:|:--------------------------:|
| `1`{: .fate_font}           | Pesada essa...             |
| `1`{: .fate_font}           | Caramba, essa foi radical! |
| `2`{: .fate_font}           | Coração na mão!            |
| `2`{: .fate_font}           | Quase me machuquei… _Ai…_. |

