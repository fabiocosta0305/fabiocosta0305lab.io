---
title: Taabish 'Taylor' Mehndi, Filósofo e Músico _Bhangra_
subheadline: Um filósofo Sikh entre dois mundos da Weird World News UK
layout: personagens
categories:
 - personagens
tags:
 - Weird World News
header: no
language: br
---

Nascido na Inglaterra como parte da Diáspora Indiana, Taabish Mehndi sempre foi um homem de dois mundos: por um lado, seus pais o mandaram para uma boa escola pública, onde aprendeu muito e bem o conhecimento secular ocidental. Por outro, em casa e no templo _Sikh_ que seus pais frequentavam, teve contato com religião e, acima de tudo, filosofias espirituais orientais, mais exatamente da Índia. 

Em meio a seus estudos, ele também se envolveu com _Bhangra Pop_, o movimento musical onde misturava-se o ritmo das músicas tradicionais _Bhangra_ com a música eletrônica e pop dos anos 80. Para ele, sempre fez muito sentido: afinal, em sua concepção ambos os lados do mundo tinham algo bom para acrescentar. _Karma_ derivava, em sua opinião, dos excessos de ação ou inação, de espiritualidade ou de materialismo. Dançar entre duas culturas era algo que em sua cabeça fazia de melhor, e _Bhangra Pop_ o ajudava.

Foi quando um demônio começou a atacar os shows de sua banda, e perseguia-os até onde pudesse. Todos os componentes começaram a fugir, mas "Taylor" (o apelido dado pelos seus amigos não-Indianos) enfrentou esse demônio: aparentemente, ele desejava expulsar os que (segundo ele) conpurscavam o _Bhangra_. Taabish, entretanto, provou que _Bhangra Pop_ era uma nova forma de ler as tradições, e isso não era mal.

Foi quando descobriu que o demônio, na verdade, era um dos anciões da comunidade _Sikh_, que achava que os jovens estavam se desviando do caminho. Tão logo o ancião, arrependido, pediu perdão e comprometeu-se a compensar os prejuízos, Taabish deixou por isso mesmo, não o denunciando diante do resto da comunidade ou à polícia. Entretanto, foi o fim de seu grupo.

Taabish acabou então se envolvendo com algumas investigações sobre criptídios, já que seus conhecimentos, incluindo o tempo que passou com o seu pai na _Mehndi Computers_, uma pequena loja que vendia BBC Micros, ZX Spectrums e Amstrads, lhe preencheu com ferramentas para ganhar a vida honestamente...

... Ainda que trabalhando para um tabloide sensacionalista como a Weird World News UK.

Taabish é moreno, tem barba longa e sempre usa turbante (conforme os mandamentos do sikhismo), mas nas demais roupas tende a usar roupas confortáveis e descontraídas, tanto batas indianas quanto combinações de camiseta e calças. Sempre usa pulseiras e correntes. Sempre tem algum tipo de música indiana tocando em momentos de descontração. Parece um pouco afastado, mas não julga nenhum dos demais membros do grupo da Weird World News UK do qual faz parte.

## Aspectos

|          **Tipo** | **Aspectos**                                        |
|------------------:|-----------------------------------------------------|
|    ___Conceito___ | Estudioso, Músico e Filósofo _Sikh_                 |
| ___Dificuldade___ | Ocidente e Oriente, Materialismo e Espiritualidade  |
|   ___Motivação___ | Descobrir novas verdades e enterrar velhas mentiras |

## Atitudes

| ___Correr!___ | ___Enganar!___ | ___Enredar!___ | ___Investigar!___ |
|:-------------:|:--------------:|:--------------:|:-----------------:|
| Razoável (+2) | Bom (+3)       | Regular (+1)   | Regular (+1)      |

## Façanhas (Recarga: 3)

+ __Bhangra, *Ritmo da Vida*__ – +2 para _Defender-se_ durante a Perseguição, desde que naquele turno algum aliado tenha sido bem-sucedido antes.
+ ___Disfarce rápido___ – _uma vez por cena_ você pode declarar que você automaticamente se disfarçou, desde que ninguém esteja o observando

## Condições

| ___Valor___ | ___Condição___      |
|:-----------:|:-------------------:|
| `1`{: .fate_font}           | Que coisa estranha! |
| `1`{: .fate_font}           | Ah, fala sério!     |
| `2`{: .fate_font}           | Hora de dar no pé!  |
| `2`{: .fate_font}           | Reza… Reza… Reza... |

