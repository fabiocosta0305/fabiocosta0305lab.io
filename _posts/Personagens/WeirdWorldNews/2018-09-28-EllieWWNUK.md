---
title:  Ellie, a Fadinha
subheadline: A fadinha espevitada da Weird World News UK
layout: personagens
categories:
 - personagens
tags:
 - Weird World News
header: no
language: br
---

Ellie é uma Fadinha que sempre foi muito espevitada e ativa. Na prática, espevitada demais para seu próprio bem.

Ela acabou fugindo de sua comunidade e encontrou o time da Weird World News UK quando eles estavam investigando algumas ações. Ela decidiu que seria uma boa ideia atrapalhar a investigação, fazendo _cantrips_ e ilusões por meio de seu _glamour_ para bagunçar pistas e causar dores de cabeça.

Até que ela viu o verdadeiro culpado: um incendiário passando-se por um monstro alienígena que cuspia fogo em orfanatos.

Ela procurou fazer algo para ajudar, mas ela era tão pequenina que quase morreu no processo. Ela então teve que recorrer aos humanos da Weird World News UK, revelando seu segredo para eles. Acabaram capturando o incendiário, mas isso levou Ellie a perder seu lar, já que um dos últimos alvos do incendiário era sua comunidade.

Ellie decidiu acompanhar o time da Weird World News como forma de redimir-se do passado. Claro que ela não conta isso para eles: ela prefere dizer que na prática ela ficaria com esses "patéticos peripatéticos" até que eles virassem gente.

Ellie tem pouco mais de 6 centímetros de altura, mas sua velocidade voando compensa. Além disso, ela pode mudar sua forma de maneira mística por meio de uma ilusão, sendo que no máximo tem a altura de uma menina de 10 anos. Ela é espevitada, bagunceira e encrenqueira, mas de bom coração, e todos sabem que, no fim das contas, pode confiar nela. Ela ainda está aprendendo a não abusar de seus poderes.

## Aspectos

|          **Tipo** | **Aspectos**                                              |
|------------------:|-----------------------------------------------------------|
|    ___Conceito___ | Uma fadinha espevitada, expulsa de sua comunidade         |
| ___Dificuldade___ | _“Não me confunda com aquela maluca da Sininho!”_         |
|   ___Motivação___ | Em busca de diversão… E que melhor diversão que aprontar? |

## Atitudes

| ___Correr!___ | ___Enganar!___ | ___Enredar!___ | ___Investigar!___ |
|:-------------:|:--------------:|:--------------:|:-----------------:|
| Regular (+1)  | Bom (+3)       | Razoável (+2)  | Regular (+1)      |

## Façanhas (Recarga: 3)

+ ___Voar___ – +2 em todos os rolamentos de _Correr!_ durante cenas de _Perseguição_, desde que tenha espaço o bastante para você voar
+ ___Glamour___ – Pode usar _Enganar!_ para _Criar Vantagens_ por meio de ilusões mágicas

## Condições

| ___Valor___ | ___Condição___                   |
|:-----------:|:--------------------------------:|
| `1`{: .fate_font}           | Que sufoco!                      |
| `1`{: .fate_font}           | Dando o fora                     |
| `2`{: .fate_font}           | Que nojo!                        |
| `2`{: .fate_font}           | Agora estou _REALMENTE_ zangada! |

