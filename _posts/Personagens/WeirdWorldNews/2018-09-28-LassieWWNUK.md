---
title:  Lassie, a filhote de monstro marinho
subheadline: A filhote de monstro marinho da Weird World News UK
layout: personagens
categories:
 - personagens
tags:
 - Weird World News
header: no
language: br
---

Lassie sempre viveu em um lago, uma pequenina filhote de criaturas abissais que sobrevivem desde antes da pré-história. Pequena  e esguia, ela se divertia subindo para mais perto do espelho d'água, onde via os humanos e pregava pequenas peças.

Isso foi até o dia em que alguns caçadores inescrupulosos de criptídios a pegaram. Ela estava brincando, quando percebeu a rede de pesca a prendendo. Ela ficou apavorada, e tentou gritar pela mãe, que não podia ouvir: ela havia sido morta alguns instantes antes. 

Lassie estava assustada, achando que seria morta como a mãe (existe um mercado negro para órgãos de criptídios), quando a equipe da Weird World News UK encontrou-a e ela foi salva.

Sem ter para onde ir, Lassie passou a ir com os mesmos.

Lassie é pequena para um monstro marinho (apenas 60 centímetros de comprimento) e não é muito grossa (10 centímetros de diâmetro). Embora possa se arrastar em terreno seco, ela prefere ficar em água sempre que pode, ou mesmo passar por cima de poças d'água onde ela pode hidratar sua pele. Lassie pode comer de tudo, mas particularmente gosta de doces e biscoitos. Lassie é assustadiça, mas muito útil quando calma. Ela não fala "humano", mas compreende perfeitamente o que lhe perguntam, respondendo por mímica e ocasional "escrita", onde ela "desenha" com o corpo letras.

## Aspectos

|          **Tipo** | **Aspectos**                                 |
|------------------:|----------------------------------------------|
|    ___Conceito___ | Filhotinha de monstro marinho carinhosa      |
| ___Dificuldade___ | Uma gatinha… ou melhor… monstrinha assustada |
|   ___Motivação___ | Me sinto segura com essas pessoas            |

## Atitudes

| ___Correr!___ | ___Enganar!___ | ___Enredar!___ | ___Investigar!___ |
|:-------------:|:--------------:|:--------------:|:-----------------:|
| Bom (+3)      | Regular (+1)   | Razoável (+2)  | Regular (+1)      |

## Façanhas (Recarga: 3)

+ ___Mascote Animal___ – _uma vez por cena_, você pode declarar um detalhe de cena sobre algum transeunte bajulando você sem gastar Pontos de Destino
+ ___Abraço gostoso!___ – se você for bem-sucedido em um teste de _Enredar!_ para _Criar Vantagens_ ao tentar prender ou amarrar um monstro, você pode, com um Ponto de Destino, declarar que o Monstro está se sentindo ___confortável___ com o fato, aumentando a dificuldade para escapar do abraço em +2

## Condições

| ___Valor___ | ___Condição___               |
|:-----------:|:----------------------------:|
| `1`{: .fate_font}           | Amedrontada                  |
| `1`{: .fate_font}           | Se escondendo em algum lugar |
| `2`{: .fate_font}           | Assustada                    |
| `2`{: .fate_font}           | Deslizando para fugir!       |

