---
title: Seamus 'Senile Sean' O’Malley, o Anarquista
subheadline: Um punk irlandês anarquista da Weird World News UK
layout: personagens
categories:
 - personagens
tags:
 - Weird World News
header: no
language: br
---

Seamus O'Malley, o _"Senile Sean"_ (Sean Maluco) é uma lenda viva: ao 14 anos se envolveu no circuito do Punk Rock na Irlanda e Irlanda do Norte como _roadie_, e até mesmo entrou para uma banda de punk rock, os _Rotten Roots_, vivendo basicamente em noites de bebida, droga, rock'n'roll, filosofia Anarquista e ocasional magia.

Isso até quando tudo deu errado.

Algum jênio com J teve a ideia de fazer uns rituais que viu em um livro de São Cipriano por diversão e para tentar conseguir elevar a banda ao estrelato.

Péssima ideia.

Quando o tal demônio apareceu, eles tiveram que enfrentar o mesmo, que estava enfurecido. Além disso, o círculo de proteção que eles tinham feito falhou. Seamus ainda deu sorte que tinha uma adaga de prata com ele, e ele conseguiu golpear o demônio o bastante para ele se mandar.

Mas nisso o guitarrista estava morto, provavelmente sua alma arrastada pelo demônio.

Depois disso, a banda se desfez e ele acabou meio sem grana, mas um amigo apareceu com uma proposta de emprego. Coisa bem básica e que dava pouca grana, mas que para um ferrado como Seamus na época, parecia legal.

Dois dias depois Seamus pegou sua primeira van da Weird World News UK, e ficou amarradão: cada coisa mais doida que a outra e um ocasional criptídio. Anarquia, cara!

Isso até que, muitos anos depois, junto com a atual equipe, eles encontraram um vilarejo de Leprechauns próximo à fronteira entre Inglaterra e Gales: nada tão comum, mas que na prática é possível. Eles descobriram que o "clurichaun", o espírito agoureiro que assustava os fazendeiros da região vindas de um antigo castelo era um picareta safado que queria transformar tudo aquilo em um _resort_ para golfistas.

Claro que eles levaram um amiguinho com eles: Leroy Patrick Shoemaker Goldberry McLoony, um chapa que conquistou o coração de Seamus. E aquele almofadinha do Harold ajudou bastante, diga-se de passagem.

Baixo e magrelo, Seamus sempre veste roupas que parecem (apenas parecem) como farrapos: na prática ele "artisticamente" decora suas roupas com símbolos anarquistas e as rasga em pontos específicos. Apesar da cara dura e sorriso cínico, ele não gosta de admitir que a experiência com o demônio acabou lhe deixando sequelas: ele é muito supersticioso, o que explica os trevos que Leroy faz para ele como uma forma de garantir sorte. Ele gosta de bancar o durão, em especial com Harold, mas na prática ele gosta de todos. Ele é o motorista da van, mas também é um quebra galho cheio de opções, incluindo de quando em quando pegar de volta sua guitarra para um pouco de punk rock!

## Aspectos

|          **Tipo** | **Aspectos**                                |
|------------------:|---------------------------------------------|
|    ___Conceito___ | _Punk-Rocker_ da velha guarda irlandês      |
| ___Dificuldade___ | Não admite, mas é supersticioso             |
|   ___Motivação___ | Anarquia!!! E manter meu chapa Leroy seguro |

## Atitudes

| ___Correr!___ | ___Enganar!___ | ___Enredar!___ | ___Investigar!___ |
|:-------------:|:--------------:|:--------------:|:-----------------:|
| Regular (+1)  | Bom (+3)       | Razoável (+2)  | Regular (+1)      |

## Façanhas (Recarga: 3)

+ ___Anarchy in UK!___ - +2 ao _Criar Vantagens_ com _Enganar!_ enquanto sacaneia, enerva ou zoa alguém
+ ___Prima Donna___ – _uma vez por sessão_, você recebe um ponto de destino adicional ao ter um Aspecto seu forçado

## Condições

| ___Valor___ | ___Condição___      |
|:-----------:|:-------------------:|
| `1`{: .fate_font}           | Bravo de Verdade    |
| `1`{: .fate_font}           | Quero quebrar tudo! |
| `2`{: .fate_font}           | Furioso!            |
| `2`{: .fate_font}           | _I don't care!_     |

