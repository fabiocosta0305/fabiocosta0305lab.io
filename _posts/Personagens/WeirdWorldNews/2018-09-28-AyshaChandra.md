---
title:   Aysha Chandra, Criptidiologa
subheadline: A _Dalit_ estudiosa de criptidios da Weird World News UK
layout: personagens
categories:
 - personagens
tags:
 - Weird World News
header: no
language: br
---

Aysha Chandra nasceu na Índia. Lá, vivia em uma comunidade Dalit em Mumbai. Apesar disso, seu pai a estimulava a estudar bastante, para, quem sabe, furar o sistema de castas, ainda que isso fosse quase impossível.

Aos sete anos, entretanto, ela viu uma certa noite, antes de dormir, uma criaturinha de pele vermelha brilhante e cabelo de fogo: ela sabia que tratava-se de um _Asura_, um dos "demônios" hindus que vinham da ação furiosa. Ele fugiu antes que ela pudesse fazer qualquer coisa.

Alguns dias depois, entretanto, um demônio apareceu colocando tudo em chamas na comunidade pobre onde morava. Enquanto as pessoas fugiam, ele ria, caminhando pelas ruas e utilizando chamas de suas mãos. Seu pai e mãe tentaram a proteger, quando ele soltou uma chama estranha de suas mãos, que literalmente colou em seus pais, queimando-os até a morte. 

Ela percebeu que algo estava errado: aquele _Asura_ não tinha olhos de fogo ou os cabelos incandescentes...

Era um impostor!

Foi quando um verdadeiro demônio apareceu para a proteger: ele voltou-se ao impostor e atiçou fogo nos tanques de napalm às costas do mesmo, explodindo-os. Por muito pouco o próprio impostor não foi engolido pelo _napalm_, o que permitir que ele fosse capturado com vida. Aysha teve que passar um tempo em proteção a testemunhas, e o fato de ser uma _Dalit_ quase colocou seu depoimento em xeque, já que o impostor era um empreiteiro de uma casta superior que decidiu acelerar um processo de gentrificação da região onde eles moravam, queimando as comunidades _Dalit_ da mesma.

Passado o processo, que resultou em prisão perpétua do impostor, ela se viu em uma encruzilhada, até que alguns parentes que moravam na Inglaterra a buscaram para viver em Guilford, perto de Londres, onde passou a estudar. A adaptação não foi simples, já que ainda falava Inglês com um sotaque carregado, mas com o tempo foi dando o seu melhor, como seu pai lhe ensinara.

Entretanto ela ainda lembrava-se do _Asura_ que lhe salvara. E percebera que tinha muito no mundo para ser investigado...

Enquanto em Oxford, onde estava estudando para se tornar uma bióloga, ela descobriu a cátedra de criptidologia, e lhe pareceu que, na pior das hipóteses, ela teria uma melhor chance se obtivesse uma especialização no mesmo.

Descobriu, entretanto, que o campo de pequisa e instituições investindo no mesmo eram limitados.

Mas foi quando uma amiga do curso de jornalismo lhe apresentou a Weird World News UK e disse que eles estavam sempre precisando de pessoas que conhecessem bem sobre criptídios, como os _Asura_. O Salário não era dos melhores, mas ao menos ela estaria em contato com criptídios (ou pessoas se passando pelo mesmo).

E foi assim que ela caiu nesse negócio.

Aysha mistura peças etnicas indianas às suas vestimentas ocidentais. Seu inglês ainda tem algum sotaque indiano, além de ela misturar ocasionalmente imprecações em marata ou urdu, idiomas que faziam parte de seu cotidiano na infância. Ela ainda tem algumas marcas da tratativa ruim que sofria enquanto _Dalit_ na Índia, o que a leva a ter uma atitude de rebeldia contra qualquer autoridade ou pessoa que se julga superiora, o que explica sua leve antipatia quanto a Harold. Possui em Taabish um amigo inestimável, devido a sua origem levemente comum, já que Taabish também tem algo de indiano nele. Seu pior problema é o fato de sempre estar com uma Navalha de Occam muito ativa, procurando explicações as mais simples possíveis para eventos, o que a coloca às vezes em rota de colisão com outras formas de pensamento.

## Aspectos

|          **Tipo** | **Aspectos**                                         |
|------------------:|------------------------------------------------------|
|    ___Conceito___ | Pesquisadora de criptidologia _Dalit_                |
| ___Dificuldade___ | Navalha de Occam sempre Afiada                       |
|   ___Motivação___ | Desvendar a verdade sobre as criaturas sobrenaturais |

## Atitudes

| ___Correr!___ | ___Enganar!___ | ___Enredar!___ | ___Investigar!___ |
|:-------------:|:--------------:|:--------------:|:-----------------:|
| Regular (+1)  | Regular (+1)   | Razoável (+2)  | Bom (+3)          |

## Façanhas (Recarga: 3)

+ ___Mestre de Armadilhas___ - +2 ao _Criar Vantagens_ com _Enredar!_ quando tentar prender o monstro na cena de Armadilha
+ ___Criptidologia___ – +2 ao _Criar Vantagens_ por _Investigar!_ Relativas aos hábitos e tendências de criptídios

## Condições

| ___Valor___ | ___Condição___                |
|:-----------:|:-----------------------------:|
| `1`{: .fate_font}           | Fascinada                     |
| `1`{: .fate_font}           | Deixe-me coletar uma amostra! |
| `2`{: .fate_font}           | Preciso olhar bem de perto!   |
| `2`{: .fate_font}           | Que fofo! Quero abraçar!      |

