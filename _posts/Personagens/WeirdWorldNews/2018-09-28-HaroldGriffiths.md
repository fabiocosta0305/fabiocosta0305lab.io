---
title:  Harold Griffiths, Advogado de Criptídios 
subheadline: O fotógrafo/advogado almofadinha da Weird World News UK
layout: personagens
categories:
 - personagens
tags:
 - Weird World News
header: no
language: br
---

Existem pessoas cujo o destino vira de cabeça para baixo depois de um evento específico, mesmo quando não afetado diretamente por isso.

Harold Griffiths sempre foi de uma família de classe média alta, com tudo do bom e do melhor. Aluno exemplar, desenvolveu desde a mais tenra juventude um gosto (mais uma forma de diletantismo) pela fotografia. Como filho de um dos donos da companhia de advogados _Griffiths and Major_, entretanto, desde o início seu destino estava traçado, estudando em Eton e Exeter para tornar-se um advogado.

Foi quando, durante uma viagem com seu clube de fotografia para a Itália, ele viu uma criatura da floresta, um estranho ser parecido com uma fadinha, tirando uma série de fotos.

Ele descobriu que essa fadinha era uma mascote de uma das garotas do grupo, uma irlandesa que estava com bolsa de estudos. Ele prometeu não revelar nada...

... e isso mexeu com sua cabeça.

Suas notas deram uma desestabilizada que, ainda que não o impedisse de se formar, fizeram com que seu pai ficasse questionando se deixar o escritório para Harold era uma boa. Depois de algumas más escolhas, seu pai percebeu que havia errado com ele e tirou ele do escritório, o colocando como consultor: ele ainda tinha muitos conhecimentos, mas sua "irresponsabilidade" e "falta de pé no chão" o impedia de ser um advogado como seu irmão mais novo na corte.

Mesmo com uma casa razoavelmente boa em Surrey e o dinheiro que recebia do Escritório... Algo faltava.

Harold sentia que podia ser útil de outras formas, e apenas ficar pesquisando tomos antigos de _Vade Mecum_ ou dados nas bases jurídicas britânicas não lhe parecia a melhor forma de viver.

Foi quando a menina em questão apareceu em programa noticialesco. Harold nem lembrava porque a TV estava passando esse tipo de baboseira, mas ele ficou interessado: era um caso de um cara que estava se passando por vampiro para aplicar um golpe em clube de Rugby, para tomar-lhe o terreno. Ele nunca imaginara que tinha picaretas que faziam esse tipo de coisa: lendas e histórias sobre os tais criptídios rolavam em todos os locais, e mesmo Eton tinha um clube de investigação de criptídios. Mas ele nunca tinha se ligado nele...

Até que ele viu as fotos da fadinha, que estava escondida em sua coleção pessoal de fotos.

Ele descobrira o que tinha que fazer.

Quatro dias depois, ele tinha sido deslocado para uma equipe da Weird World News UK como fotógrafo (para tremendo desgosto de seu pai). E eles encontraram Leroy e descobriram um picareta que estava tentando tomar um terreno onde ficava o vilarejo de Leroy pela (pouca) prata da região. Harold ajudou a abrir um processo para declarar a região sítio histórico, devido a certas características que indicavam a presença de povos como os pictos e os saxões na região.

Oficialmente, Harold é o fotografo da equipe, mas na prática ele também ajuda com detalhes jurídicos obscuros. Ele ainda ajuda seu pai como consultor, mas sempre que possível sem se afastar dos demais.

Harold é baixo, pesado, forte e compacto, como esperado de um ex-jogador de Rugby. Ainda que viva com os outros na estrada, dividindo a mesma van de todos, suas roupas ainda demonstram bom gosto e um certo ar burguês almofadinha. Harold tenta ser sem preconceitos, mas existem coisas que o irritam em Seamus e Thomas, além de ter uma certa condescendência com Aysha e Taabish.

## Aspectos

|          **Tipo** | **Aspectos**                                      |
|------------------:|---------------------------------------------------|
|    ___Conceito___ | Fotógrafo de coração, Advogado de “Profissão”     |
| ___Dificuldade___ | _Cockney_ – burguês até a alma                    |
|   ___Motivação___ | _“Oh dear, sempre tem algo interessante por aí!”_ |

## Atitudes

| ___Correr!___ | ___Enganar!___ | ___Enredar!___ | ___Investigar!___ |
|:-------------:|:--------------:|:--------------:|:-----------------:|
| Regular (+1)  | Regular (+1)   | Bom (+3)       | Razoável (+2)     |

## Façanhas (Recarga: 3)

+ ___Tackle de Rugby___ – +2 para _Criar Vantagens_ usando _Enredar!_ ao tentar derrubar ou agarrar um alvo
+ ___Vamos logo com isso___ – ao _Criar Vantagens_ irritando, reclamando ou apressando seus amigos, eles recebem +3 na primeira vez que usarem tal Aspecto, ao invés de +2

## Condições

| ___Valor___ | ___Condição___                 |
|:-----------:|:------------------------------:|
| `1`{: .fate_font}           | Não me ensinaram isso em Eton! |
| `1`{: .fate_font}           | OK… A situação complicou-se    |
| `2`{: .fate_font}           | Despirocado!                   |
| `2`{: .fate_font}           | Nos veremos na corte!          |

