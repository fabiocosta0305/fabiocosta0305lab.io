---
title:  Leroy McLoony, o Leprechaun
subheadline: O travesso Leprechaun da Weird World News UK
layout: personagens
categories:
 - personagens
tags:
 - Weird World News
header: no
language: br
---

Leroy Patrick Shoemaker Goldberry McLoony vivia uma vida comum em seu vilarejo Leprechaun até que um cara começou a se passar por um _Clurichaun_, um Leprechaun que está em uma poderosa maré de azar e que contamina todos ao redor com tal Azar. 

Foi quando ele foi encontrado pela equipe da Weird World News UK, que estava investigando os estranhos acidentes em uma mina de prata abandonada próxima ao vilarejo leprechaun e o tomaram pelo verdadeiro culpado. Ele resolveu se unir aos mesmos para desmentir os fatos e impedir que seu vilarejo fosse ocupado por humanos. E ele acabou tomando a decisão de seguir com os essa turma animada de pessoas, já que ele estava cansado dos sapatos e ouro e tudo o mais.

Leroy é um tipo baixinho e mirrado, imberbe mas com fartas costeletas e um cavanhaque fino de leprechaun. Seu cabelo é vermelho como se espera de um leprechaun e suas roupas são o que normalmente se espera de um duendinho irlandês. É capaz de usar seus _cantrips_ para aparentar ser um pouco maior do que realmente é, mas ele acaba aparentando um garoto de 14 a 16 anos, já que ele ainda é muito jovem para um Leprechaun, além de, não importa a forma que assuma, ele _**SEMPRE**_ aparecerá com sua cartola verde com um trevo de quatro folhas.

Ele se sente muito a vontade com todo mundo, mas em especial gosta da atitude punk de Seamus O'Malley, com quem divide também a origem na Ilha Esmeralda.

## Aspectos

|          **Tipo** | **Aspectos**                                             |
|------------------:|----------------------------------------------------------|
|    ___Conceito___ | Leprechaun conhecendo o mundo moderno                    |
| ___Dificuldade___ | _“Que diabos é isso?”_                                   |
|   ___Motivação___ | _"Era isso ou ficar na vila fazendo sapatinhos mágicos"_ |

## Atitudes

| ___Correr!___ | ___Enganar!___ | ___Enredar!___ | ___Investigar!___ |
|:-------------:|:--------------:|:--------------:|:-----------------:|
| Regular (+1)  | Regular (+1)   | Bom (+3)       | Razoável (+2)     |

## Façanhas (Recarga: 3)

+ ___Cantrips___ – Pode usar _Enredar!_ para _Criar Vantagens_ por meio de truques mágicos
+ ___Biscoitos de Coragem___ – Sempre que invocar um Aspecto para re-rolar, você tem a opção de re-rolar uma segunda vez, ficando com o resultado desse último rolamento

## Condições

| ___Valor___ | ___Condição___                  |
|:-----------:|:-------------------------------:|
| `1`{: .fate_font}           | Confuso                         |
| `1`{: .fate_font}           | Dançando Freneticamente         |
| `2`{: .fate_font}           | Apavorado                       |
| `2`{: .fate_font}           | Clurichaun! (Má sorte a todos!) |

