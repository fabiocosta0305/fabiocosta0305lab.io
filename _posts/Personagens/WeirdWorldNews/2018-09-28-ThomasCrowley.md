---
title:  Thomas Crowley, Investigador do Absurdo
subheadline: O Investigador do Estranho da Weird World News UK
layout: personagens
categories:
 - personagens
tags:
 - Weird World News
header: no
language: br
---

Nascido em uma família de pessoas envolvidas desde a Era Vitoriana (?!) na investigação dos Criptídios e de eventos do sobrenatural, incluindo magia e outras coisas ocultas, Thomas acabou sendo desde sempre o garoto esquisito, já que por algum motivo seus pais sempre demandaram que ele estudasse, além das matérias comuns, coisas como Ocultismo, Cabala, Tarô e Magia Hermética.

Isso foi até que um evento mexeu com Thomas:

Seus pais e ele estavam em um ritual místico padrão, de primavera, até que uma criatura abissal apareceu diante deles, soltando fogo e enxofre. No caos do bosque pegando fogo, seus pais foram gravemente feridos.

Ele decidiu investigar o que se tratava e começou a se envolver com Magias mais perigosas para descobrir que entidade havia feito aquilo: como seus pais tinham falhado no ritual?

A descoberta foi a pior possível.

Era um picareta tentando tomar o bosque para um projeto imobiliário, se passando por um demônio ao estilo de Chernabog de Fantasia! Usando uma espécie de Jetpack e produtos químicos, ele estava simulando ser um grande vilão.

Ele então decidiu: se esse cara estava afim de brincar nesse nível, ele ia cair no jogo! Usando suas técnicas mágicas ilusórias (na prática o uso de certas ervas e preparados) ele fez com que o cara ficasse quase louco, até que um time da Weird World News UK revelou toda a trapaça e mandou o picareta para a cadeia.

Mas seus pais estavam feridos, e tudo levava ele para ir para o Reformatório. Ele perguntou se não tinha como participar do grupo. Eles aceitaram-o como um Estagiário, investigando informações em fontes de ocultismo para saber separar o joio do trigo.

Cheio de tatuagens e com uma aparência meio gótica, mesmo quando está quente usa roupas pseudo vitorianas, quase como um figurante de _Entrevista com o Vampiro_, o que leva aqueles que nunca o viram a o achar estranho e até mesmo perigoso. Entretanto, quem o conhece bem sabe que tudo isso é fachada: mesmo suas "magias" são muito mais mentalismo, conhecimentos de ervas e similares do que magias de verdade.

Particularmente, ele não se entende bem com Seamus, que acha um pobre coitado de mente pequena, e nem com Harold, que acha um almofadinha empolado, mas ele sempre trabalha em equipe, mesmo com os arranca rabo. Sempre possui consigo um amuleto que ele próprio construiu, com prata e carvalho, com símbolos de magia hermética e druídica, além de trazer símbolos de cabala. Ele acredita que esse símbolo lhe proteja nas piores situações.

## Aspectos

|          **Tipo** | **Aspectos**                                     |
|------------------:|--------------------------------------------------|
|    ___Conceito___ | Mago do Caos Auto-Proclamado                     |
| ___Dificuldade___ | Trevoso apenas para quem não o conhece           |
|   ___Motivação___ | Descobrir os mistérios herméticos dos Criptídios |

## Atitudes

| ___Correr!___ | ___Enganar!___ | ___Enredar!___ | ___Investigar!___ |
|:-------------:|:--------------:|:--------------:|:-----------------:|
| Regular (+1)  | Razoável (+2)  | Bom (+3)       | Regular (+1)      |

## Façanhas (Recarga: 3)

+ ___Meu Amuleto___ – Sempre que usar um _Aspecto_ para bônus de +2, você recebe +3 no lugar, desde que seja capaz de descrever como esse item especial o beneficia
+ ___Biblioteca Hermética___ – +2 ao _Criar Vantagems_ por _Investigar!_ para obter pistas envolvendo criptídios, criaturas  e eventos sobrenaturais

## Condições

| ___Valor___ | ___Condição___              |
|:-----------:|:---------------------------:|
| `1`{: .fate_font}           | Invocação das Profundezas   |
| `1`{: .fate_font}           | Olhar Assustador            |
| `2`{: .fate_font}           | Procurando as Linhas de Ley |
| `2`{: .fate_font}           | Iä! Iä! Cthulhu Fthaghn!    |

