---
title:  "The Bewitched Chocolate"
subtitle: "A _Little Wizards_ Tale adapted as a _Do - Fate of the Flying Temple_ Letter"
language: en
layout: aventuras
categories:
  - Adventures
tags:
  - Do - Fate of the Flying Temple
---

__Stamps:__ Knot, Pen, Lotus

> _"Dear Pilgrims of The Flying Temple:_
>
> I'm Maxi, and I'm a Lil' Mage from Coinworld. Coinnworld is a very good place, where magic exists and we can play with things magical, like fairies and pixies in Heads and werewolves and ghosts in Tails. Our world is shaped as a coin, and you can go from one half to another somewhat easily. I'm from Shivers Archipelago in Tails, where we can take some shivers with some horror stories, about werewolves that, although accursed, do their best to give good chills by musically howling. 
> 
> I'm a Lil' Mage, travelling Coinworld to understand better my magic and people. My best friend is Sora, a Lil' Sorcerer (there's some differences between Lil' Mages and Sorcerers, but we live in peace) that looks like a clown all the time, because she was born with her face that way, and lived in Smiles Archipelago, from Heads, and is very cheerful, although somewhat clumsy (like me). I like too much Sora, because, as I lived into the so-called Skunk Haven, people somewhat thinks I smell like a skunk because my tail (I need to say that I don't smell like a skunk) and she's one of the few that don't think I smell. 
>
> My other friends are my familiar and Sora's. In ancient times only cats could be Mages and Sorcerers familiars, but this rule changed. The animal just need to be black (don't know why, it's Tradition, my dad said). My familiar is Puffer, a black skunk that have sage eyes and help me a lot (although sometimes he could be stinky). Sora's is Horace, a black rabbit, very shy and meeky, that hides itself into Sora's hat. Yup, as Wizards we use also brooms, wands and hats, and Sora's is as flashy as flashy can be, and mine is made of some skunk hide, including the tail (that don't smell).
> 
> But enough on me or Sora.
>
> My dad said if I had any problem that even our Magic could not solve, I should write to the Flying Temple that they would provide help.
>
> And is exactly what's happening now:
>
> Me and Sora came recently to Smiles to visit her family at the circus, and we saw that people here is behaving weirdly. 
> 
> The baker had left his bakery and is playing in the fountain, swimming on it all dressed - apron, hat and all!  A mother had left his child behind and sat into his stroller and happily get away, cruising the city! And, curioser and curioser! (as Sora said), some teenagers are into tantrums, like  spoiled little babies! Even we are more grown-up than them! (and we are 7!)
> 
> We talked with some Doctors and they don't know what is happening. Our only clue is that recently a new Chocolate shop, called _Ellys Delicacies_, was opened, and started to sell his chocolate. We don't know if there's something about it, but it was the only thing Sora saw that was different since she came back for some vacation with his family.
>
> This is very weird, and even our familiars, Puffer and Horace can't help us...
>
> Please, please, pretty please (it's Sora saying me to write this), came here in Coinworld and help us. We promise we'll pay you a voyage to many places in Coinworld, including all Smiles archipelago to see cheerful things and to my home at Shivers, so you can feel the very good chill of hearing a zombie orchestra playing the Shivering Flute.
>
> Thank you, Pilgrims, and we are waiting you in Smiles.
>
> _Maxi, Lil' Mage & Sora, Lil' Sorcerer_

## NPCs

### Maxi, Lil' Mage

+ ___A Lil' Mage with some skunks features: tail, whiskers, hair, but not (thankfully) smell; From Tails, likes cool places and scary things; From Skunk Haven; Timid and clumsy; Likes Sora and Puffers___
  + ___Skilled (+2) at:___ Alchemy, scary stories, cmaping
  + ___Bad (-2) at:___ Like being alone _and_ on big crowds
  + ___Alchemy:___ can _Create Advantages_ using special potions. However, any fail is automatically treated as a Success with a Cost, where the user of the potion can suffer any kind of weird effects
  + ___Spells:___ can use magic rolling _Mediocre (+0)_. However, any fail is automatically treated as a Success with a Cost, where the user of the potion can suffer any kind of weird effects
  + ___Broom Riding:___ can _Fly_ into Coinworld using any _Enchanted Broom_, rolling _Mediocre (+0)_ if needed

### Sora, Lil' Sorcerer

+ __*A Lil' Sorcerer from Heads; Like funny, cuddly, colorful things; Not Enough Hats; A* REAL REAL *clown face; Bubbly, cheerful and clumsy, but curiously graceful when needed; Likes to be with Maxi;* "Get out below my hat, Horace!"__
  + ___Skilled (+2) at:___ Shapechanging, funny tricks
  + ___Bad (-2) at:___ get into boring situation
  + ___Shapechanging:___ can _Create Advantages_ transforming things. However, any fail is automatically treated as a Success with a Cost, where the user of the potion can suffer any kind of weird effects
  + ___Spells:___ can use magic rolling _Mediocre (+0)_. However, any fail is automatically treated as a Success with a Cost, where the user of the potion can suffer any kind of weird effects
  + ___Broom Riding:___ can _Fly_ into Coinworld using any _Enchanted Broom_, rolling _Mediocre (+0)_ if needed

### Puffers, Maxi's Familiar

+ __*A Sage Skunk Familiar; Older than Maxi knows; Like to talk with difficult words;* "Don't touch Maxi or my friends., you churl, if you want to live!"__
  + ___Skilled (+2) at:___ wisdom, stink
  + ___Bad (-2) at:___ tolerate shenanigans (except those from Maxi and Sora)
  + ___Stink bomb:___ he can Attack anyone into two zones with a stink bomb attack _once per conflict_. Each target defends individually
  + ___Stink Jolt:___ if Puffers hit a target with a Successful with Style Attack, he can reduce stress in 1 to put a ___Stinky___ Aspect in the target, with a Free Invoke

### Horace, Sora's Familiar

+ ___A Meek and Shy Rabbit Familiar; So small that can hide into Sora's hat(s); Can be braver in the need than you should think___
  + ___Skilled (+2) at:___ run, hide, being a rabbit
  + ___Bad (-2) at:___ courage
  + ___Just a little rabbit:___ while _hidden_, receive +2 on _Overcome_ or _Create Advantages_ discovering things

### Greedy Brownies 

+ ___A Small (8) Horde of Problem!; Tricksters, but not (too much) mean; REALLY greedy, LOVES fruits and sweets___
  + ___Skilled (+2) at:___ using their _Brownie Powder_, plçay tricks
  + ___Bad (-2) at:___ courage when alone and/or against the odds
  + ___Stress:___ [ ][ ][ ][ ]

### Childfied People

+ __"Wanna play!"; *Eating Lots of* Enchanted Chocolate__
  + ___Skilled (+2) at:___ Eat chocolate
  + ___Bad (-2) at:___  acting in non-childish ways

### Extras

+ ___The Van___ Fair (+2), ___No one's there___ Terrible (-2)
+ __*Mr. Cocoa, Pot-Bellied Chocolatiere and* Elly's Delicacies *Owner* Fair (+2), ___Not Exactly Athletic___ Terrible (-2)
