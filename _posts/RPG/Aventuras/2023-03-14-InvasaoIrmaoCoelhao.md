---
title: "A invasão do Irmão Coelhão"
#subtitle: "An Young Centurions Easter Adventure"
#language: br
layout: page
categories:
  - Aventuras
tags:
  - Shadow of the Century
excerpt_separator: <!-- excerpt -->
---

**Um filme apresentado por *Shadow of the Century***

> + ***Big Issue:*** Um Exército de Homens-coelho está se formando
> + ***Subplot Issue:*** Pessoas comuns resolvendo problemas bizarros

Essa aventura funciona bem tanto como uma abertura, encerramento ou especial em uma Série de _Shadow of the Century_, ou como um Filme em One-Shot para eventos. Ele não usa nenhum dos Chefões do livro, mas você pode colocar que exista algo que interesse os mesmos, em especial se o envolvimento da _Sombra da Fantasia do Século XX_, _Filippo Geppeto_ (em sua identidade de _Christian Matteo_) for presente. 

Embora ela faça citação aos eventos da Aventura de _Young Centurions_ [***O Ovo Místico e A Lebre do Campo***](http://fabiocosta0305.gitlab.io/aventuras/AventuraPascoaYoungCenturions/) (em especial por causa do Lebre do Campo, Marshall Hare), ter jogado ou não a aventura não faz muita diferença.

Ela é uma aventura planejada para 2-6 jogadores, integrantes da NOVA (***Network of Variable Operatives* - Rede Variável de Operativos**), mas pode ser adaptada para outras organizações e quantidades de jogadores sem muitos problemas...

<!-- excerpt -->

## Introdução - a Ameaça do Irmão Coelhão

> *“O Irmão Coelhão está aqui para te ajudar!*
>
> *O Irmão Coelhão sempre vai te amparar!*
>
> *Seja na Terra, no céu, na água ou no mar!*
>
> *Seja gigante, bruxa, pirata ou outra pessoa má!*
>
> *Não precisa ter medo e nem tampouco se assustar!*
>
> *O Sempre Atento Coelhão com certeza vai lhe salvar!"*

Essa cantiga, a _Canção do Irmão Coelhão_, está na cabeça de muitos jovens que cresceram nos anos 60 e 70 como o tema principal do show ***Aventuras do Irmão Coelhão***, onde um Ator vestido na roupa do personagem Irmão Coelhão enfrentava todo tipo de bichos que tentavam o devorar e procurava ajudar seus amigos. Nomes como Fred Raposa ou Bruce Urso (inimigos) e Betina Bow-Wow, Lena Ovelha e Suzan a Pequena Pastora (amigos) estão na mente de muitos.

Mas o show acabou de maneira abrupta no final dos 70, devido a alguns desaparecimentos estranhos e nunca confirmados de crianças, onde se suspeitava que o Irmão Coelhão estava exercendo má influência. 

Mas isso não impediu que uma companhia nova, mas crescente em poder no cenário dos programas infantis o trouxesse com uma nova roupagem: a _Peek-a-boo Entertainment_, propriedade do Estranho Christian Matteo, comprou os direitos do personagem e está recriando o show, em uma nova roupagem como uma animação, e licenciando o desenho para outros programas infantis, provocando um ressurgimento das _Tocas do Coelhão_, os fã-clubes dos fãs do Irmão Coelhão. 

Entretanto, recentemente, algumas notícias estranhas de desaparecimentos envolvendo crianças e até adultos fãs do Irmão Coelhão tem dado o que falar, ainda mais com o crescente _Pânico Satânico_ que está aparecendo nos Estados Unidos e outros países. 

E isso pode estar acontecendo em qualquer lugar, já que alguns contratos para exibir o Irmão Coelhão foram assinados para países como Japão, Alemanha Ocidental, Brasil e África do Sul.

E nem estes estão a salvo dos desaparecimentos...

... pois parece que dessa vez o Irmão Coelhão veio para amedrontar.

## Cena 1 - Alpha

Os jogadores são contactados por Alpha, o contato e organizadora da NOVA e são recrutados para uma missão estranha, mas condizente com o lema da mesma, _“Ordinarius populus adversus minas aliena”_, ***pessoas comuns enfrentando problemas bizarros***.

> James pediu para colocar vocês a par de uma situação que está demandando a ação da NOVA: em uma cidade do interior, recentemente começaram a desaparecer pessoas em geral, na maioria crianças. Isso não seria uma ação que demandaria intervenção da NOVA se não fossem alguns detalhes:
> 
> + Isso ocorreu uma semana após um show do Irmão Coelhão, do programa ***As Aventuras do Irmão Coelhão***
> + Esses desaparecimento estão ocorrendo em várias cidades próximas
> + Todos os desaparecidos eram dos fã-clubes ***Toca do Coelhão*** das cidades
> + Há também alguns avistamentos segundo alguns de criaturas com forma de Coelho, mas com a altura de pessoas adultas ou até maiores!
> 
> James suspeita que tem mais nessa jogada do que quiseram lhe contar, quando o procuraram... Então, ele resolveu colocar vocês para investigar. Aliás, por algum motivo, ele pediu para que vocês observassem os eventos da ***Páscoa dos Coelhos de 1911*** para mais pistas. Eu tentei encontrar coisas sobre isso, mas nas BBSes tudo que encontrei foram algumas teorias da conspiração bizarras.

A partir daqui, algumas perguntas que eles podem se fazer:

+ ***O que diabos foi essa Páscoa dos Coelhos de 1911?***

Se alguém pedir para ver se sabe, seria um teste de ***Conhecimentos*** dificuldade _Excepcional (+5)_, já que é uma lenda perdida no tempo. Caso questionem Alpha, ela passará a seguinte informação, que é o que realmente ela sabe:

> _"Bem, dizem que em 1911 houve um evento esquisito durante a Páscoa: alguém vendeu ovos de chocolate tão saborosos que as pessoas estavam quase saindo no braço por eles. E pior... Segundo os tabloides da época, as pessoas estavam virando criaturas mutantes bizarras, metade homem, metade coelho, e para piorar um certo Lebre do Campo queria transformar Nova Iorque em seu País das Maravilhas particular. Dizem que quem parou ele foi um grupo de adolescentes ligado a uma instituição filantrópica que no futuro cairia em desgraça, chamada Clube do Século. Sinceramente... Me parece algo fantasioso demais, mesmo envolvendo o Clube do Século"_

A existência do Clube do Século caiu nas sombras nesse período mais cínico, e mesmo entre os que conhecem as lendas sobre os Espíritos do Século, as versões conhecidas são tão diferentes quanto contraditórias: alguns dizem que eram o Último Bastião de Defesa da Humanidade contra Coisas que o Ser Humano não Deveriam Conhecer e Guardiões de Perigosos Segredos! Outros dizem que eram fracos ou, ainda pior, parte do Perigo Vermelho por não terem fornecido segredos que poderiam ajudar no Esforço de Guerra, como apontado pelo Senador McCarthy! Há até aqueles que acreditam que isso seja uma Alucinação Coletiva onde todos imaginavam esses heróis como uma fuga da realidade. 

Isso é realmente o que o cidadão médio conhece, mas existe muito mais segredos que eles imaginam, mas como isso não faz tanto sentido no momento, por agora fica essa ideia.

+ ***Quem é esse tal Irmão Coelhão***

> _"Ele é o personagem principal de um antigo show infantil, **As Aventuras do Irmão Coelhão**, que passou até 1976 quando foi cancelado em condições estranhas até que a três anos atrás foi relançado como um desenho animado e teve um ressurgimento e angariou novos fãs, o bastante para que voltassem a ter shows do Irmão Coelhão pelo país."_

Os personagens investigarem mais sobre o Show vão descobrir que o show foi cancelado depois que algumas crianças foram para uma floresta e se perderam por algum tempo. Ninguém sabe o que aconteceu com essas crianças, e as famílias não dão entrevistas de jeito nenhum.

Também vão descobrir que existiam vários atores que faziam o Irmão Coelhão ao redor dos Estados Unidos: assim como Bozo, _Fred Rogers' Neighborhood_, Capitão Canguru e _Howdy Doody_, apesar de serem shows nacionais, não havia na época a tecnologia de transmissão via satélite que permitem as redes de TV, então muitas vezes os shows eram feitos por atores locais e alguns quadros eram gravados em _videotape_ e passados pré-gravados. 

Curiosamente, todos os atores que faziam o Irmão Coelhão já morreram de causas naturais, exceto um, um certo _Eaton Rabbit_ que nunca deu entrevistas fora da roupa do Irmão Coelhão, e mesmo assim deu muitas poucas: desde que o show original acabou, ele deu apenas duas entrevistas (ainda vestido nas roupas do Irmão Coelhão), e desde então desapareceu: ninguém nunca o encontrou, e esse é um mistério que até hoje chama a atenção de alguns fãs (agora pais).

+ ***OK... E essas Tocas do Coelhão?***

> _"São Fã-clubes do Irmão Coelhão... Sabe, como os Clubes de Mistério Hu Dunnit, os Mouseketeers do Mickey Mouse e afins? Pelo que sei, no original o Irmão Coelhão sempre estimulava as crianças a participar da sociedade, seja por meio de grupos como os Escoteiros ou mesmo por outras formas de atividade social. E muitos se uniram a partir das Tocas do Coelhão. Havia até mesmo brindes raros dados aos participantes de Tocas que comprovassem terem_ agido como bons coelhinhos e  se unido pelo bem de todos, _como orelhas de coelho e pins do Coelhão, e até mesmo livros ilustrados."_

Os personagens não precisam fazer testes para saber as informações públicas dos _Clubes de Mistérios Hu Dunnit_ ou sobre os [_Mouseketeers do Mickey Mouse_ (M-I-C-K-E-Y M-O-U-S-E)](https://en.wikipedia.org/wiki/The_Mickey_Mouse_Club). Qualquer informação adicional sobre os _Clubes de Mistério Hu Dunnit_ como uma das Sementes Douradas demandariam testes à parte, mas caso seja necessário consulte as páginas 156 e 157 de _Shadow of the Century_. 

Se notarem algo estranho no comportamento de Alpha, ela vai assumir que ***quando criança foi parte da* Toca do Coelhão *da sua cidade***, e que ainda tem as Orelhas que ganhou quando ela e seus colegas de classe da _Toca do Coelhão_ coletaram brinquedos para crianças pobres no Natal. Isso é tudo que ela vai dizer sobre as _Tocas do Coelhão_ já que _"faz bastante tempo, e acho meio brega o Irmão Coelhão atualmente. Limpinho e idealista demais. Sonho de criança que não cabe no nosso mundo."_

+ ***O que se sabe dessa companhia que tá com os direitos do Irmão Coelhão?***

> "Bem, o dono da mesma é um certo Christian Matteo: nascido em 1942 em Wichita, Kansas,  formado em 1962 pela Universidade de Nova York, trabalhou por um tempo em musicais na _Broadway_, sempre como produtor, mas curiosamente, apesar dos shows acabarem não muito tempo depois de ele entrar nos mesmos, ele ascendeu bem rapidamente como um dos melhores produtores da Broadway, até que depois de um tempo comprou uma antiga produtora de programas infantis de baixo orçamento, chamada ***Peek-a-boo Entertainment*** e a lançou rapidamente como uma produtora muito produtiva e rentável. Tem alguns boatos esquisitos sobre doenças mentais entre atores e equipe de produção desses shows, mas nada comprovado, então ele continua produzindo shows e conseguiu a sorte grande de comprar do espólio da antiga produtora do Irmão Coelho os direitos do mesmo. E o negócio foi meteórico... Até demais. James tem algumas suspeitas, mas não pode afirmar nada, então ele não disse nada sobre isso.""

Se os PCs questionarem a inação de James Marconi, fundador da NOVA, Alpha vai deixar claro que _"se ela não sabe nada, eles também não. Se existe uma coisa que James sabe fazer é deixar claro que informações podem ser passadas ou não"_

Se investigarem, vão descobrir que Christian tem um passado tão polêmico quanto bem-sucedido: nos bastidores da Broadway, ele ficou conhecido por suas explosões de temperamento quase infantis, relacionamentos estranhos e habilidade de lidar com o _glamour_ e colocar mesmo as maiores estrelas sob sua supervisão na linha, não importa o quão rebelde elas sejam.

Se alguém tentar uma investigação mais séria (e que vai depender de teste com dificuldade mais alta e que vão gastar muito tempo), vai descobrir uma coisa muito estranhas: ***Não existe nenhum Christian Matteo nos papeis!***, ainda que qualquer investigação em sistemas computadorizados o deem como tudo certo! 

Existe uma razão para isso, mas fica a critério do Narrador colocar isso em jogo: ele na verdade é _Filippo Geppeto_, uma antiga _Sombra da Fantasia do Século 20_. Existem registros de eventos no passado, nos anos 30 e 40, de ações dele tentando manipular a realidade por meio de ações envolvendo contos do fada e atividades artísticas.

O quanto isso tem a ver com o Irmão Coelhão fica a critério do Narrador, já que (ao menos nesse caso), Filippo Geppeto não está envolvido diretamente nas ações.

+ **O que sabemos sobre os desaparecidos?**

> "Bem... Os mais recente foram quatro crianças na região _Amish_ do Indiana. Bem... Se considerarmos que os _Amish_ não usam nada que seja de antes do século 18, é de se surpreender que tenha tido um show de um personagem de programas infantis lá, mas isso também explica os desaparecimentos estão espalhados geograficamente na região. O mais recente, há dois dias atrás, foi de uma certa Amanda Williams, 7 anos. Típica princesinha local: escoteira, ótima estudante, parte do clube de balé e da fanfarra da sua escola, a única coisa que ela tinha de incomum do perfil era o fato de ser parte da Toca do  Coelhão local."

Se questionarem Alpha sobre brindes que ela tenha recebido do Irmão Coelhão, ela não saberá informar: lembre que são os anos 80, e embora Alpha tenha incríveis habilidades computacionais para a época, os computadores ainda eram pouco usados em órgãos públicos, e menos ainda eram conectados em rede. Mesmo a Internet ainda era uma esbórnia de protocolos de comunicação rodando sobre as estruturas da ARPANet (Privada, de baixo alcance), Milnet (Militar, herdeira da ARPANet original) e da NSFnet (pública, liga universidades e centro de pesquisas), fora uma meia dúzia de sistemas que trocam informações entre si por telefones usando protocolos antigos como Usenet, FTP, UUCP (Unix-to-Unix Copy), e a recém fundada FidoNET, entre outros.

+ **Qual ajuda teremos?**

> "James providenciou dinheiro para que possam alugar um carro para irem até a região Amish. Se me permitem a dica, vão até Angola, Indiana: temos alguns contatos lá que podem ajudar, inclusive entre os Amish caso seja necessário."

Na realidade, os Amish não tem nada a ver com a história: se conhecer mais sobre os mesmos e quiser se divertir com as reações dos personagens, pode colocar alguns, mas como isso é um Filme de _Shadow of the Century_, não perca muito tempo: _go big or go home_, ou você chega arrebentando ou vai para casa, não tem pontos para o segundo lugar!

Antes de dispensar os jogadores, Alpha informa que estará disponível para contato pelos meios convencionais (telefones públicos ou BBSes exclusivas), das maneiras que os membros da Alpha conhecem. O que isso significa, fica a critério do Narrador com os Jogadores. 

Apesar de incipientes, existem computadores "transportáveis" como o [Osbourne 1](https://pt.wikipedia.org/wiki/Osborne_1) e o [IBM Portable](https://en.wikipedia.org/wiki/IBM_Portable), e até mesmo computadores miniaturizados, mas com certas capacidades de conectividade que apesar de incipientes, mesmo no período eram úteis o suficiente para quem manja, como os [HX-20](https://en.wikipedia.org/wiki/Epson_HX-20) e [P-8 Geneva](https://en.wikipedia.org/wiki/Epson_PX-8_Geneva) e o [TRS-80 Model 100](https://en.wikipedia.org/wiki/TRS-80_Model_100)... Ou, se for no futurista 1986, temos o [IBM PC Convertible](https://en.wikipedia.org/wiki/IBM_PC_Convertible). Mas deixe claro que não é como hoje que basta tirar o celular do bolso e chamar sua Baby Girl.

De qualquer modo, agora fica a critério dos jogadores descobrir qual é a ___Ameaça do Irmão Coelhão!___

## Cena 2 - Amanda

Seja como for, se os jogadores seguirem a dica de Alpha, vão levar dois dias na estrada, ouvindo fitas de música ou apreciando os melhores _hits_ de Dolly Parton ou Elvis Presley... Com alguma sorte, talvez peguem alguma rádio onde algum doido toque aquelas loucuras vindas da Europa, como Kraftwerk, Kate Bush, Mike Oldfield, Spandau Ballet ou Neu! 99 Red Baloons é provavelmente o melhor que vão encontrar.

De qualquer modo, se os jogadores perguntarem por mais informações dos desaparecimentos, você sempre pode usar os ___televangelistas da vez espalhando o Pânico Satânico contra o Irmão Coelhão___, dizendo todo tipo de absurdos, e até mesmo _"revelando as mensagens satânicas escondidas nas músicas do Irmão Coelhão"_. Tipo, é o velho lance da mensagem satânica reproduzida a partir do disco tocado ao contrário: Beatles, Elvis, Jimi Hendrix... Até _Hotel California_ dos Eagles já foram considerados portadores de mensagens ocultas do Sete-Peles.

Use isso para reforçar alguns rumores, como homens-coelho nas matas, mensagens satânicas, corrupção de menores... Aproveite o cardápio do melhor do pior das teorias da conspiração: pode ser a hora de tirar seu **GURPS Illuminati** do armário e lembrar que FNORD aconteceu em FNORD por causa de FNORD, FNORD, FNORD e FNORD! FNORD!

Ao chegarem em Angola, Indiana, vão perceber que a cidade é o que você pode esperar de uma cidade Americana do interior, aquelas que chamariam a atenção tanto de Mark Twain, Ernest Hemingway e Jack Kerouac. O motel é pequeno, simples, mas limpo e confortável. A lanchonete/restaurante local é pequena e a música mais moderna que a Jukebox tem é _The End_ do Doors, mas o hambúrguer é daqueles que quase dá para ouvir o boi mugir quando você morde! Toda casa tem uma bandeira americana diante da porta hasteada, mas os canteiros são lindos e as pessoas são convidativas em geral, e sempre podem oferecer um refresco se questionadas.

E caso sejam questionadas, algumas respostas possíveis:

+ ***O que você pode dizer sobre a criança desaparecida?***

> "Tá falando da pequena Amanda Williams? Ela é um docinho! Sempre prestativa, a melhor escoteira da região, canta no coral da igreja, uma maravilha de menina. A única coisa era toda essa história envolvendo o Irmão Coelhão: ela, junto com o Toby Michaels e o Henry Mitchell, formaram uma Toca do Coelhão depois de começarem a assistir o Irmão Coelhão, junto com o pai de Henry, o Justin... Coitado, ele não voltou certo do Vietnã, lelé da cuca! E não sei não se os vermelhos não fizeram algo com ele!"

A verdade é essa: Amanda se ajuntou com alguns amiguinhos para montar uma Toca do Coelhão com o apoio do pai de algum deles, no caso o senhor Justin Mitchell, pai de Henry.

+ ***Por que um adulto se envolveria em um clube de um programa infantil?***

> "Bem, pelo que entendemos, toda Toca do Coelhão tem que ter um responsável adulto, para qualquer atividade que envolva a Toca, ou foi ao menos o que o senhor Mitchell disse... Mas... Ele voltou esquisito da guerra, sabe? Antes mesmo do Irmão Coelhão voltar a passar na TV, ele vira e mexe ia para fora e voltava com alguma coisa esquisita do Irmão Coelhão para sua coleção! E no ano passado, depois que o show ficou popular, ele voluntariou a Toca do Coelhão a participar do Desfile de Ação de Graças! Bem... Ninguém falou nada pois parecia inofensivo, mas agora... Sempre pareceu algo esquisito"

Se os personagens pesquisarem informações sobre isso, vão ver que estava tudo OK, e que a Toca do Coelhão na época era um pouco maior, com cerca de umas dez crianças, mais uns três adultos. Um deles, inclusive, na foto que acharem caso investiguem, parece em uma fantasia do Irmão Coelhão, completa com focinho falso, a cartola passando pelas orelhas e as grandes patas de coelho. Se questionarem, vão descobrir que trata-se do tal Justin Mitchell. A foto do jornal não informa isso, apenas informando que _"A Toca do Irmão Coelhão de Angola participou do Desfile de Ação de Graças"_, junto com outras agremiações sociais locais, como os maçons, o Rotary Club, o Coral da Igreja local, o Clube de Tradições da Guerra Civil e o _Clown Alley_, a agremiação de palhaços da cidade.

+ ***O que podem dizer sobre esse tal Justin***

> "Ele voltou esquisito do Vietnã: ele perdeu o irmão lá, e voltou meio... pancada, sabe? Ele não sai, não vai para o bar, mal pode ser visto fora da igreja ou de quando vai ao mercado. Além disso ele tem que ir de tempos em tempos até o Hospital do Exército em Michigan se tratar com aqueles remédio para loucos. Agora, esses negócios não funcionam direito se por um acaso ele se veste como um coelho gigante."

A verdade é que Justin _realmente_ faz uso de medicações psicotrópicas, e _realmente_ ele se vestiu como Irmão Coelhão, e tal... Mas existe uma razão mais séria para isso, que os personagens vão descobrir se procurarem Justin, na cena 2a.

Agora, se procurarem a família Williams, irão para a cena 2b e vão descobrir ainda coisas mais estranhas!

Por fim, se pensarem em procurar Toby, na cena 2c, vão notar que ele parece meio esquisito, para preocupação de sua mãe, a senhora Wilhelmina "Willa" Michaels.

## Cena 2a - Justin Mitchell

A casa de Justin parece igual a maioria das casas da região: um sobrado típico americano, uma bandeira dos Estados Unidos tremulando apoiada no parapeito da porta, um Plymouth Voyager antigo para fora da garagem. Assim que os personagens tentarem se apresentar, a porta não se abrirá totalmente. Uma senhora se colocará para ver e dirá:

> "Deixa eu deixar bem claro: se vocês são mais um grupo ridículo de jornalistas, vão procurar o Delegado McMahon! Ele tá doido para dar entrevistas, diferentemente do meu marido!"

Essa é a senhora Justin Mitchell (_neé_ Shelly Hamon). A senhora Shelly é ___extremamente leal ao marido___ (Razoável (+2) para qualquer teste relacionado) e sabe que ele ___precisa de mais que o tratamento que recebe das pessoas e do Exército___. Se os jogadores mostrarem que estão dispostos a ajudar Justin, ela irá (de maneira reticente) abrir a porta.

As janelas estão fechadas com cortinas claras, mas grossas, garantindo alguma privacidade necessária agora que Justin é o centro das atenções, o que deixa a casa um pouco mais fresca e escura. Ao entrarem vão notar que a casa tem pouca mobília, mas tudo bem organizado. Uma farda de gala do Exército está em um manequim no canto da sala, e sob o console da lareira, dois conjuntos de medalhas estão expostas. São medalhas de bravura em combate para veteranos do Vietnã, em especial nas batalhas mais sangrentas no fim da guerra. Entre elas, a foto de um homem de uns 20 anos, aparentemente bem comum, ainda que saudável, está apoiada sobre uma bandeira dos Estados Unidos dobrada. Se questionarem, qualquer teste simples ajudará a perceber que trata-se da dobra feita para entregar-se a bandeira que cobriu o caixão de um soldado caído em guerra.

Ao redor da sala, uma série de fotos mostra sempre os meios dois jovens, bastante similares entre si, algumas vezes com estranhas cartolas com orelhas de coelho na cabeça, algumas vezes com outros jovens que parecem amigos deles. Há também fotos desses jovens, já mais velhos, em uniforme militar e _dogtags_ à mostra.

No sofá, assistindo a TV, um homem de aparência similar a um desses jovens, mas bem mais velho. Parece forte para a idade, mas meio descuidado com a saúde e com uma cara bem desanimada.

> "Ele está assim desde que a Amanda sumiu... O Delegado foi bem cruel com ele. Porco! Meu marido não perdeu o irmão dele e  no Vietnã para ser achincalhado por esses idiotas dessa cidade!"

Justin observará a esposa, que então irá buscar algo para beber. _"Nada com álcool: o Justin não pode beber nada, o tratamento dele não permite."_

Se os jogadores se aproximarem corretamente, vão obter algumas respostas.

+ ***A gente ouviu por aí que você gosta do Irmão Coelhão... Até mesmo montou a Toca do Coelhão...***

> "Sim... Eu sempre gostei do Irmão Coelhão! Foi assistindo quando criança ***As aventuras do Irmão Coelhão*** que eu procurei estudar para me tornar professor, sabe... Mas bem... A coisa no Vietnã piorou justo quando estava para ir para Indianápolis para a Faculdade, e eu e meu irmão acabamos sendo alistados. O pior é que, diferentemente de outros aqui da cidade, fomos mandados ***realmente*** para o _front_, diferente dos engomadinhos e dos filhos do pastor que foram para o administrativo... Foi só a _Canção do Irmão Coelhão_ que nos manteve com a cabeça no lugar, sabe?"

Ele vai cantar a _Canção do Irmão Coelhão_, com uma voz até meio infantil. Fica claro que, apesar de ter tido sua cota (aparentemente maior que deveria) de trauma da guerra, não dá a entender que ele possa ser um psicopata da machadinha ou qualquer coisa do gênero. Ele só é alguém que sobreviveu ao moedor de carne no Vietnã e conseguiu achar uma forma de lidar com isso sem ficar totalmente pinéu.

+ ***Então você curtia o Irmão Coelhão desde criança?***

> "Sim! Eu e meu irmão Terry fundamos a _Toca do Coelhão_ aqui em Angola. Ainda tenho muitos dos brindes que conseguimos naquele período: nossa Toca era uma das mais ativas, sempre o Irmão Coelhão mencionava _'Os coelhinhos sempre ativos da Toca do Coelhão de Angola, Indiana'_ no show"

Se aceitarem, os jogadores vão ser conduzidos até o porão, onde eles verão o que parece ser ___quase um museu não-oficial do Irmão Coelhão___: uma velha câmera Super 8 ao lado de vários rolos de filme com filmagens de episódios antigos do Irmão Coelhão, inclusive com uma participação dos irmãos Mitchell, livros, discos, canecas, faixas, revistinhas, chapéus e orelhas, uniformes dos Coelhinhos Amigos do Irmão Coelhão com o nome dele e do irmão... E no canto uma fantasia do Irmão Coelhão de tamanho grande e alta qualidade: claro que não parece o Irmão Coelhão ***de verdade***, como eles podem ver pela capa dos discos antigos das _Histórias Incríveis do Irmão Coelhão_, mas com certeza é de alta qualidade. Se viram a foto da parada, vão reconhecer a fantasia como a usada pelo adulto na mesma, e Justin confirmará ser o adulto em questão.

+ ***Bem... Mas como foi que você se envolveu com essas crianças?***

> "Bem... Desde a primeira versão do Irmão Coelhão, toda Toca tinha que ter o Líder Coelhão, um adulto responsável para ajudar nas atividades da Toca. É como ser o Mestre Escoteiro, sabe? Quando eu era pequeno, o Líder era meu pai, e sempre achei que meu irmão seria o líder, por ser mais velho... Mas cancelaram o programa e ele morreu na guerra... E foi quando, com a retomada do programa nessa nova versão, meu filho começou a assistir e lembrou de algumas revistinhas minhas de quando eu era criança, e quis que montássemos a Toca aqui em Angola, ainda mais que descobriu que alguns amigos dele da escola também gostavam do Irmão Coelhão, como Amanda e Toby, os que eram mais ativos junto com o Henry, além de alguns outros... Mas..."

+ ***Mas?***

> "Eu tenho que dizer... Não gosto como eles estão fazendo o Irmão Coelhão agora. Me chamem de antiquado, mas o Irmão Coelhão na minha época era inocente e esperto: ele não batia nos seus inimigos, como o Bruce Urso ou o Fred Raposa, ele sempre escapava das armadilhas deles com perspicácia e com a ajuda de amigos que fazia no caminho. Mas agora... o Irmão Coelhão é pouco diferente de Tom e Jerry, sabe? Toda essa violência de Desenho Animado não tem nada a ver com o Irmão Coelhão."

Acima de tudo, Justin é um pai preocupado (até demais) com a família, já que depois da morte do irmão é o que tem de mais importante na vida. Ele pode mostrar alguns vídeos clássicos do Irmão Coelhão: descreva para os jogadores como ele nunca machuca os inimigos, mas faz lances de esperteza e conta com a ajuda de amigos para se salvar, ora um jato de gambá, ora usar a água acumulada da chuva para cegar os inimigos e fazer eles descerem rolando um pequeno buraco até um monte de urtigas, e coisas do gênero. Sempre com esperteza e contando os amigos.

Já nos novos, o Irmão Coelhão ainda conta com ajuda, mas agora também "parte para as cabeças" aos socos e chutes, não muito diferente de um Tom e Jerry, como vão notar se verem o episódio do dia do Irmão Coelhão junto com a família Mitchell.

+ ***E seu irmão?***

> "A gente lutou muito no _front_, e mesmo assim usávamos o que aprendemos com o Irmão Coelhão para tentar ajudar as crianças lá. Elas eram inocentes e tal, a guerra não era culpa delas... A gente odiava aquele lance de _napalm_ e tal, nunca usávamos isso no nosso batalhão por nossa sugestão, mesmo indo contra as ordens do Pentágono. Mas isso foi até um dia onde a batalha foi tão intensa que as granadas e morteiros estavam voando de todos os lados e era  difícil saber se estávamos sobre fogo amigo ou inimigo. Isso até que um morteiro caiu perto da gente: eu tive o ombro quebrado por destroços, meu irmão não teve a mesma sorte... O maior pedaço dele que acharam foi sua cabeça e tronco... Nunca vou esquecer quando vi o corpo... Como estava ferido, fui mandado de volta, mas a cabeça não estava legal., e deram baixa em mim, me descartando porque... Tinha pesadelos com meu irmão morto... Foi difícil e sem a ajuda da Shelly seria pior. A gente se conhece da Toca, sabe? E ela também perdeu um irmão na Guerra, e ela me ajudou a superar. Ela é professora na escola, mas está afastada desde que aconteceu tudo isso com a Amanda."

Se questionarem Shelly, ela vai dar a resposta cliché (e verdadeira): eles se conheciam de criança, ela gostava dele e vice-versa, mas os dois tímidos não davam o braço a torcer, ele foi para o Vietnã e voltou com a cabeça zoada. Ela o ajudou com algumas coisas e os dois decidiram assumir a paixão, se casar e tiveram Henry. Justin ainda trabalha ocasionalmente com reparos reparos em geral, como consertar tratores e ocasionais torneiras de casas, mas a fonte de renda principal é com Shelly.

+ ***E Henry, como está lidando com o desaparecimento da Amanda?***

> "Ele tá chateado... Mas também meio culpado: ele sentiu que tinha algo de errado com o comportamento de Amanda, e eles bateram boca no dia antes do desaparecimento. Toby foi em apoio a Amanda. A real é que o Henry entende o que eu disse sobre esses novos desenhos do Irmão Coelhão: são muito violentos para crianças, e estimula comportamentos que não são tão legais, como trapacear e mentir, diferentemente do Irmão Coelhão da minha época, onde ele estimula a criatividade e a engenhosidade."

Se os personagens questionarem Toby, ele vai confirmar o que o pai disse (e que é verdade):

> "A gente brigou: Toby e Amanda gostaram de um episódio recente onde achei que o Irmão Coelhão foi muito mal, mesmo sendo contra o Fred Raposa, o trapaceando e arremessando uma caixa de abelhas na cabeça dele! Eu fiquei chateado e xinguei eles de bobos e chatos... E no dia seguinte a Amanda sumiu e o Toby ficou em casa!"

A verdade é que tudo isso seria apenas uma briga infantil, não fosse o fato de Amanda ter desaparecido.

+ ***Como Amanda desapareceu?***

> "Eu não sei... Sinceramente, acordei com o Xerife quase derrubando a porta aqui de casa, querendo me interrogar sobre esse desaparecimento, por ser o Líder da Toca do Coelhão. Eles reviraram a casa, quase destruíram minha coleção do Irmão Coelhão... Como se eu fosse um criminoso! Um criminoso! Eles falaram que Amanda teria sido vista na Floresta e que poderia ter sido algo comigo, mas eu fiquei em casa desde após o culto na igreja no dia anterior. Passei a noite em casa, brincando com o Toby. Não fui para o bar nem nada do gênero, não posso por causa dos remédios."

A real é que Justin não sabe nada do desaparecimento de Amanda. Ele ficou em casa a noite toda, mas apesar disso as pessoas não acham esse álibi forte o bastante, já que os únicos que podem confirmar o Álibi são sua esposa e filho, o que na realidade não é grande coisa. E como Justin sempre foi visto como alguém que voltou esquisito do Vietnã, em especial com o lance do Irmão Coelhão, então já ficou meio marcado de uma maneira negativa na cidade.

Se os personagens decidirem investigar mais, podem tanto procurar Toby (Cena 2c) ou ir na casa de Amanda (Cena 2b), ou ainda tentar irem para a floresta (Cena 3).

### Cena 2b - A família Williams

A família Williams vive em uma casa bem maior que a família Mitchell, inclusive com dois carros na garagem: uma Dodge Caravan novinha e um clássico Chevrolet Camaro de 1976. Mas, no momento em que os personagens chegam, apesar dos carros estarem na frente da garagem, limpos e bonitos, não parece ter ninguém em casa.

Os jogadores podem tentar entrar arrombando a casa (por _Roubo_, dificuldade ***Razoável (+2)***), ou sempre possível recorrer ao velho truque de ver se tem uma chave debaixo da porta de trás (1 PD ou sucesso em _Intuição_ ***Razoável (+2)***). Caso role falhas, pode ser que a Polícia acabe encontrando os personagens logo!

De qualquer modo, ao entrarem, vão ver que a casa é muito chique, limpa e bonita, incluindo até mesmo um poderoso IBM PC em um canto! Entretanto, um teste de _Percepção_ ***Razoável (+2)*** irá indicar algo esquisito... Um levo ***cheiro de Coelho ao redor*** da casa. Além disso, vão notar o detalhe de que existem muitas coisas que ***remetem a um passado* hippie *do casal***, como algumas fotos com camisetas *tie-dye* e símbolos *hippie* em manifestações contra a Guerra no Vietnã e alguns livros da época no alto de uma estante. Por fim, uma foto de quando eram crianças, incluindo os irmãos Mitchell Justin e Terry. Uma foto igual estava na casa dos Mitchell se os personagens questionarem, e poderá ser vista também na casa dos Michaels na Cena 2c.

Se procurarem o quarto de Amanda, vão notar que é o típico quarto de menininha rica dos anos 80: cama de dossel, casa de bonecas, muitos bichos de pelúcia, uma mesa com um espelho, um armário cheio de roupas bonitas, incluindo uniformes de _ballet_ e de fanfarra. A única coisa fora do estereótipo são algumas coisas do Irmão Coelhão, incluindo um Chapéu com Orelhas e alguns livros de história, além de um conjunto de fitas do Irmão Coelhão que podem ser ouvidas no toca-fitas sobre a mesa.

Com um teste de *Percepção* ou *Intuição* ***Bom (+3)*** vão achar o ***Diário de Amanda***. Se resolverem ler, vão descobrir que ela vinha já tendo sonhos estranhos com o Irmão Coelhão o chamando para as matas ***antes do show do Irmão Coelhão***, mas depois do show ela ficou ainda mais convencida que ela estava sendo chamada para ser parte da Família Especial do Irmão Coelhão (o que Justin poderia confirmar que não existe). Ela também estaria ouvindo as fitas com as Histórias do Irmão Coelhão ao dormir para ajudar, mas ela também dizia ter algo errado, como se ele dissesse para abandonar família e amigos, e isso ela não queria.

A última entrada do diário, entretanto, é ela decidindo ir para a mata: ela sabia que seus pais entenderiam, como o Irmão Coelhão afirmava, e dizia que Henry era _"um bobo fedido e chato"_ e que Toby a entenderia, já que ele ouviu as fitas do Irmão Coelhão (se questionarem Justin, ele vai afirmar que o próprio Henry não quis ouvir mais, depois de ouvir uma que Amanda emprestou durante uma reunião da Toca).

A cama está desorganizada, e curiosamente tem um pouco de poeira, assim como todo o quarto... Como se ninguém tivesse entrado a algum tempo. Isso pode levantar uma lebre, com o perdão da piada, para o fato de parecer que os pais não se importavam com o desaparecimento de Amanda... 

O que não deixa de ser verdade.

No quarto dos pais, bem mais limpo, vão notar dois detalhes: a cama está desfeita (o que pode ou não levantar bandeiras), e o fato de haver um cheiro estranho de coelho no quarto.

Se eles procurarem, vão notar um par de _Walkman_ no quarto e uma certa quantidade de fitas gravadas, como se fossem cópias _bootleg_ de alguma coisa, marcadas sempre com o nome _Mensagem do Irmão Coelhão_!

Se tentarem ouvir, peça um teste de _Vontade_, com dificuldade começando em _Medíocre (+0)_. Em caso de _Falha_, coloque um Aspecto escondido sob o personagem com a _Mensagem Subliminar do Coelhão_ e um número de Invocações igual ao de falhas. Enquanto o personagem permanecer ouvindo, ele deve rolar testes de Vontade com dificuldade aumentando em +1 por Falha, e deve fazer um último teste para parar de ouvir. Cada vez que o personagem for bem sucedido, o personagem pode rolar _Intuição_ ou _Conhecimentos_ ***Razoável (+2)*** ou ainda pagar PD para notar o que tá errado! Eles vão notar que existe a _Mensagem Subliminar do Coelhão_, um _Sucesso com Estilo_ ou um segundo teste (incorrendo na chance de ficar sob efeito da Mensagem Subliminar) irá indicar que existe algo sob _o futuro da Lapinidade_ fazendo com que eles _deixem para trás a humanidade e desçam pela Toca do Coelho para o País da Maravilhas do Lebre do Campo_.

Por fim, se investigarem o Sótão, vão notar um certo cheiro de Coelho, bem forte: o local é uma bagunça, com algumas coisas do Irmão Coelhão espalhada e, notarão, dejetos que podem indicar a presença de coelhos no local, como felpas e fezes de coelho, mas com uma coisa curiosa: elas são muito maiores do que se esperaria de um coelho! Isso pode indicar que algo esteve aqui. E a verdade, ainda que os jogadores não tenham como saber, é essa, já que os Williams escondiam (sem Amanda saber) no Sótão alguns ***Homens-coelho do Lebre do Campo***.

Se eles falharam no teste para entrar na casa, ou então demorarem demais, faça com que o Carro da Polícia local chegue, com alguns policiais armados. O suficiente para dar um susto e colocar _"esses arruaceiros de fora para correr"_, mas não o suficiente para derrotarem os personagens.

Por fim, eles podem visitar Justin (Cena 2a) ou ir para a casa de Toby (Cena 2c)... Ou ir para a Floresta (Cena 3)

### Cena 2c - Toby e Wilhelmina Michaels

Diferentemente dos Williams e dos Mitchells, os Michaels vivem em um dos apartamentos no prédio da _General Store_ local. São apartamentos confortáveis, mas obviamente menores que os das outras famílias. Se os personagens procurarem Toby, Wilhelmina será bastante reticente em deixar conversar com Toby, mas caso convençam de que vão tomar cuidado ao conversar (*Persuasão*), ela aceitará os mesmos dentro de casa e então Toby irá conversar com os mesmos.

Toby é um pouco mais velho que Henry e Amanda, e ele parece um pouco maior e mais forte que os dois, mas é extremamente tímido, e dá para notar o porquê se ver o quarto dele: ele, apesar de maior e mais forte, é o _nerd_ da turma, seu quarto lotado de merchandising de séries (em especial do Irmão Coelhão), livros de fantasia e contos infantis, dicionários e livros de estudo. 

Toby está assistindo ao episódio do dia do Irmão Coelhão, e poderão entender as críticas de Justin e Henry: apesar do que ele tenta transparecer, não é diferente de Tom e Jerry, o que é bem diferente dos desenhos mais açucarados e menos violentos como _Nossa Turma_, _Kissifur_, _Ursinhos Carinhosos_ ou _Popples_.

Ao observar a casa, vão notar que existe uma foto de Justin quando pequeno, com os Williams e com Willa, além de uma foto de um senhor de idade. Se questionarem, Willa revelará que era seu esposo, que faleceu a mais ou menos um ano em um infarto fulminante. Ela ainda sofre com isso, mas está superando.

Se conversarem com Toby, Willa sempre estará perto mas vai deixar Toby à vontade para responder dentro da timidez dele. Se os personagens convencerem Toby que estão procurando ajudar Amanda, ele se abrirá mais aos personagens:

+ ***Você parece ser o mais velho dos seus amigos, certo?***

> "Sim. Mas os outros garotos da escola caçoavam de mim, em especial depois que meu pai morreu. Mas Amanda e Henry me receberam na Toca do Coelhão, e tem sido tudo divertido. Eu já ia antes, mas depois que papai morreu, tem sido mais fácil lidar com tudo depois que frequentei mais a Toca do Coelhão e a casa do Henry... Ao menos até os últimos dias, antes de Amanda desaparecer."

Toby deixa claro que procurou a Toca do Coelhão como uma forma de lidar com a morte do pai. Nada demais, mas era a forma que ele tinha de não pensar na morte, o que para uma criança de 8 anos não é muito fácil.

+ ***O que você acha do senhor Mitchell?***

> "Eu não acredito que foi ele! Ele gosta muito de todos nós e é um Líder Coelhão maravilhoso, sempre seguindo o que o Irmão Coelhão ensina. Eu sei o que as pessoas acham do senhor Mitchell, mas ele não é uma pessoa malvada e jamais faria algo com a Amanda!"

Toby acredita totalmente na inocência de Justin, ao ponto de ficar hostil se alguém falar coisas ruins sobre Justin.

+ ***E o desfile no ano passado? Não é esquisito um cara vestido de Coelho?***

> "Não mais que os palhaços, ou os caras vestidos de índio, ou de Mickey, ou... Enfim: as pessoas falam muitas coisas ruins do senhor Mitchell, eu sei o que as pessoas falam dele, de que ele é esquisito, mas lembro que ele foi uma das poucas pessoas que foi visitar meu pai no Hospital antes dele morrer e deixou que Henry me levasse para a casa dele para passar a noite!"

A verdade é que _apenas parece_ que Justin tinha até uma boa quantidade de amigos até ir para o Vietnã: depois que voltou, as pessoas começaram a o achar esquisito, em especial pela sua obsessão com o Irmão Coelhão. Se os personagens ainda não conversaram com Justin, Willa mencionará os fatos sobre Justin e seu irmão na Guerra, e confirmará uma informação que os personagens poderão ter: ela, o marido dela, os Williams e os Mitchell eram os principais Coelhinhos da Toca do Coelhão de Angola do passado. Willa, formada como enfermeira e que está afastada para tomar conta de Toby, irá dizer que Justin reabriu a Toca com o retorno do Irmão Coelhão para ajudar os demais pais, que eram muito ocupados (o Senhor e a Senhora Williams são engenheiros civis). 

> "Não fosse por Justin reabrir a Toca do Coelhão, eu não sei o que seria de Toby: ele sempre foi um menino tímido, e como ele parece alto e forte os demais meninos sempre tentam ver se ele é mesmo forte, mas o que Toby tem forte, ele tem de gentil, o que as pessoas atualmente leem como ser mariquinhas..."

Ela diz isso com um certo rancor, e dá para perceber nas entrelinhas que Angola não é tão perfeita quanto aparenta. Se questionarem mais, vão notar que Justin já não era muito querido antes de ir para o Vietnã, apenas não falavam na cara... E como ele REALMENTE voltou meio bagunçado de lá, as pessoas apenas começaram a "bater no bêbado".

> "Justin e seu irmão eram muito gentis... Justin ainda é, mas por causa do tratamento pesado para a cabeça, ele não é mais ou mesmo. Ele passou a não sair muito mais de casa, muito recluso. A Shelly é um doce por cuidar dele, e ele é um doce por fazer o possível para manter a vida de maneira tão normal quanto possível, cuidando da casa, fazendo comida e tal, mas... A verdade é que por causa do Estresse Pós-Traumático da guerra, ele se isolou demais das pessoas, e as pessoas aqui não gostam disso. A única atividade social que ele tinha, à exceção da Igreja, era a Toca do Coelhão. E eu sei muito bem o que adultos pensam de um outro adulto cuidando de um monte de crianças sozinho, sem nenhum ganho financeiro incluso."

A realidade é que a cidade de Angola já não via Justin muito bem, e como ele cuidava da Toca do Coelhão, o único adulto cercado por crianças sem nenhuma outra razão, então era a "escolha óbvia" o ver como um molestador em potencial. Mas Willa sabe mais, por ser um ponto de controle de Justin na sua cidade, sem que ele precise ir ao hospital do Exército o tempo todo, então ela tem uma noção do quanto a cidade judia dele.

+ ***O que você pensa de Henry e Amanda, Toby?***

> "São meus melhores amigos, de longe! Henry é muito criativo, e bastante corajoso, e a Amanda é muito fofa, sempre preocupada com a aparência e com os estudos... Apesar que..."

+ ***Apesar que?***

> "No dia antes de ela desaparecer, a gente teve uma discussão séria: para ser honesto, Amanda parecia esquisita desde que fomos assistir o show ao vivo do _Irmão Coelhão_, parecia meio afastada. Foi quando Henry perguntou o que estava acontecendo, e comentou sobre não ter gostado muito do último episódio do Irmão Coelhão e ela disse que não tinha nada e começou a falar umas coisas bem feias sobre o senhor Williams. E então ele retrucou dizendo que ela parecia diferente, e que ela não lembrava do que o Irmão Coelhão falava sobre amigos coelhinhos, que eles deveriam permanecer juntos e confiar uns nos outros, e que ela estava escondendo algo. Ela então ficou muito brava, e os dois começaram a bater boca, e eu tentei separar os dois, mas aí Henry também falou coisas meio feias para mim, sobre meu pai ter sido um covarde de não ter ido para o Vietnã como o pai dele e o tio dele que morreu, e eu não pude resistir, e gritei com ele, e a gente meio que brigou... E aí a Amanda sumiu... Talvez Henry tivesse certo sobre a Amanda, no fim do dia."

As coisas começam a ficar mais claras: se descobriram o diário de Amanda na Cena 2b, eles sabem que ela estava sob algum efeito hipnótico bizarro, e se ouviram as _Mensagens do Irmão Coelhão_ nas fitas dos pais de Amanda e descobriam as _Mensagens Subliminares_, então devem ter percebido que Amanda e seus pais provavelmente foram manipulados pelo Irmão Coelhão.

+ ***Sabe onde ela poderia ter ido? Parece que ela foi para a Floresta***

> "Tem alguns lugares na Floresta onde a gente costumava se esconder em brincadeiras, ou então quando a gente ia pescar no rio... Mas a realidade é que já passei para a Polícia esses lugares, mas não acharam nada, exceto as roupas delas largadas perto de um local ali... Rasgadas..."

Tudo leva a crer que o Irmão Coelhão está cooptando crianças mesmo. A questão é...

Para que?

Só vão descobrir se entrarem na Floresta, indo para a Cena 3. Mas antes, podem conseguir algumas pistas adicionais na casa do Williams (Cena 2b) ou com o senhor Justin Mitchell (Cena 2c)

## Cena 3 - Na Floresta

Se os personagens ainda não entenderam isso, de alguma forma o Irmão Coelhão está convencendo crianças a irem sozinhas para a Floresta, onde ele provavelmente as encontra e sequestra...

Caso contrário, é sempre possível que alguns ___Homens-coelho de Marshall Hare___ venham à noite para a cidade e tentem sequestrar tanto Toby quanto Henry... Como isso vai se dar, fica a seu critério.

A Floresta da região é muito fechada: apesar de tudo, os _Amish_ locais costumam preservar a natureza, já que ela fornece carne de caça para os mesmos por meio de gambás, coelhos, raposas e outros animais de caça não muito grandes. Entretanto, ao caminhar na mata fechada, os personagens vão notar que algo está errado: o ___cheiro de coelho___ permeia o ar. 

A partir daqui, peça testes de _Percepção_ ou _Intuição_. Como dificuldade, role em segredo pelos ___Homens-coelho de Marshall Hare___. Se os personagens falharem, role novamente pelos ___Homens-coelho___ e use o resultado do rolamento dos personagens como Dificuldade. 

+ Se os personagens passarem, eles verão ___Sombras estranhas___ na mata: elas tem a altura de pessoas adultas, ou até mesmo maior, as maiores beirando em torno de 2,40m. Um sucesso com Estilo vai mostrar que essas sombras ___Não são Exatamente humanas___: é difícil de dizer por causa do movimento, mas as formas parecem erradas: parecem ter leves corcundas e estranhas formas acima da cabeça. Se perseguirem essas formas, irão entrar em uma perseguição contra um grupo de ___Homens-coelho de Marshall Hare___. Como são quatro Homens-coelho, aumente em +1 os rolamentos e coloque duas caixas de estresse (o bônus desaparece tão logo uma caixa seja marcada).
+ Se por um acaso, os ___Homens-coelho___ irão emboscar os personagens, pulando do nada com um Ataque antes de qualquer personagem (mas ao qual os personagens poderão resistir naturalmente). Como dito acima, são quatro ___Homens-coelho___ que atacam em conjunto, procurando o mais forte entre os personagens, portanto dê a eles o mesmo +1 acima, e as caixas de estresse. Eles estão tão alucinados e são leais ao seu líder até a morte, preferindo morrer a ser capturados e entregar seu líder. Entretanto, se os personagens forem derrotados (ou Concederem), o que parece ser o líder desse grupo, que parece mais uma lebre que um coelho e já parece muito mais velho, com uma pelugem bastante acinzentada e dentes que estão muito amarelentos, além das garras por baixo das almofadas das patas dianteiras meio curvadas, irá os levar até _"O Senhor da Lapinidade, que terá o prazer de mostrar-lhes os prazeres leporinos"_

Seja como for, eles serão encaminhados (ou continuarão na direção geral para onde os Homens-coelho iam), e conforme se aproximam o cheiro de coelho fica cada vez mais intenso: o cheiro forte de dejetos de coelho permeia a região até que eles veem uma caverna antiga... E vários Homens-coelho (ao menos 12) ao redor. 

Seja como for, eles irão para dentro da caverna, seja capturados (e então enjaulados), seja ocultos e se escondendo, eles verão mais alguns Homens-coelho parados diante de uma Lebre... Ou melhor... Uma _criatura_ meio-humano, meio-lebre, rindo de maneira maníaca enquanto opera um equipamento estranho: parece uma cadeira de dentista, mas é mais assustadora que uma (o que é algo realmente terrível), cheia de tubos de raios catódicos (vulgo tubos de TV) e fios e eletrodos e amarras de couro, e tudo que você imaginaria de uma cena de tortura de um filme B. 

Mas pior, porque tem uma pessoa... Ou antes... O que _parece uma pessoa_, um vagabundo de estrada preso à mesma... Exceto que ele está sem orelhas, que subiram para o alto de sua cabeça e foi puxada para o alto, e de seus lábios agora puxados para frente na forma de um focinho de coelho pode ser ouvido um guinho terrível em meio aos barulhos infernais que a mesma está fazendo, mudando partes de sua pele para um pelo cinzento de coelho, as mãos tremendo enquanto os dedos ganham garras almofadadas, em um espetáculo grotesco que lembre _Um Lobisomem Americano em Londres_. Até que em certo momento, o homem-lebre desliga a máquina e o vagabundo cai nocauteado, partes do corpo ainda mexendo de maneira bizarra, como se fossem cera quente, liberando novos tufo de felpa de coelho e mudando ossos e órgãos internos para suas versões lapinas.

> "Maravilha, maravilha! Essas informações que coletei desse espécime vai me ajudar com o Super-Lapinador! Logo construirei um lapinador capaz de tomar todo o mundo! As pessoas se tornarão meus *Oryctolagus Sapiens* e eu serei o Rei-Deus de uma Nova Lapinidade! A Humanidade falhou! Ela é como gafanhotos destruindo a natureza! E eu serei o senhor de uma nova era! O Irmão Coelhão é apenas o começo!"

Qualquer personagem que queira saber se ele é o Irmão Coelhão vai ter a surpresa: a começar pela cor de pelos, formato do focinho, das orelhas, o focinho, nada nessa Lebre lembra o Irmão Coelhão... E onde entra o Irmão Coelhão nessa jogada então?

Eles veem dois Homens-coelho guardando uma jaula. Se lembrarem das fotos da casa dos Matthews (Cena 2b), vão reparar que parecem demais com o Sr e a Sra Matthew, exceto pelo olhar desfocado e pelas formas leporíneas e dentro da jaula uma criança cujas orelhas e nariz já estão em forma de coelho.  E a imagem delas vai lembrar das que viram em cartazes de _Desaparecido_ pela cidade de Angola.

Essa é Amanda Matthew, já parcialmente transformada em uma menina-coelho!

E, para piorar, qualquer personagem que esteja sob as ___Mensagens Subliminares do Coelhão___ vai se sentir compelido a ajudar o Lebre do Campo a capturar seus amigos e/ou apresentar-se como voluntário à Lapinação!

Caso eles tentem resgatar Amanda (ou precisem de ajuda para fugir), uma forma aparece na escuridão, vagamente humanoide, mas não distinguível. Ela falará com um sussurro agudo e acelerado.

> "Apenas me acompanhem e não façam muito barulho!"

Os personagens podem tentar resgatar Amanda (e ocasional Toby e Henry). Apesar de tudo, essas ***gaiolas*** não são muito seguras (_Roubo_ ***Regular (+1)*** para abrir as mesmas). Além disso, poderão contar com a ajuda da pessoa na escuridão.

Se for necessário, os personagens poderão tentar fugir, mas aí será novamente uma perseguição como a descrita anteriormente, exceto pela maior quantidade de _Homens-coelho_ perseguindo os personagens (aumente o bônus para +2 e cinco caixas de estresse), além do Sr. e Sra. Matthews agindo _a favor_ da Lebre, caçando sua própria filha!

Assim que saírem da caverna, os personagens descobrirão quem é que está os ajudando.

O Irmão Coelhão!

Mas... Ele não era o sequestrador?

> "Vamos fugir primeiro! Depois conversamos!"

Aqui você pode resolver de várias formas as coisas:

+ Um Conflito seria um problema, mas se um dos personagens se sacrificar os demais poderão fugir com Amanda e o Irmão Coelhão. Esse personagem certamente será transformado em um ___Homem-Coelho do Lebre do Campo___ (como uma Consequência Extrema, substituindo algum Aspecto do personagem), mas não totalmente controlado pelo mesmo: se for caso, dê chance do personagem (ou permita que ele queime PDs) para ele próprio empreender fuga no futuro, ou ficar para aprender sobre o plano de Marshall Hare e tentar o sabotar. 
+ Uma perseguição seria óbvia o suficiente para ser uma solução. Ainda que os personagens tenham dificuldade, se trabalharem direito podem fugir, em especial com a ajuda do Irmão Coelhão.
+ Se tentarem atiçar os instintos lapinos dos Homens-coelho contra eles, pode ser uma boa ideia: usar fogo, imitar barulhos de predadores, gerar sons altos (um tiro para cima é o suficiente) e coisas similares pode ser usado para fazer seus ___Instintos___ lapinos dispararem, fazendo-os correr desembestados por todos os lados e dando tempo de eles fugirem

## Cena 4 - O Irmão Coelhão

Seja como for, esperamos que eles consigam fugir, até que o Irmão Coelhão possa os conduzir até seu esconderijo em uma velha Fazenda _amish_ a umas duas horas de caminhada de onde estavam, bem afastado de Angola, entre a mesma e Shipshewana. A Fazenda está abandonada, e é afastada das principais rodovias o bastante para ser um bom esconderijo, garantindo uma certa privacidade. Ao chegar no celeiro, que o Irmão Coelhão está usando de esconderijo, os personagens poderão ver algumas hortaliças em um canto (curiosamente incluindo poucas cenouras), junto com um balde cheio de água tirada do fosso próximo, limpa e fresca o bastante para ser considerada potável. Um pacote grande de ração para coelho está perto das hortaliças. O cheiro de coelho é presente, mas bem mais suave que na caverna.

> _"Fiquem a vontade para descansar um pouco... Eu tento tomar meus banhos, mas o rio próximo tem muitos coiotes e outros predadores, então espero que não se incomodem com um certo cheiro de coelho."_

A única fonte de luz no celeiro é provida pela luz da Lua no céu. Por alguma razão, enquanto organiza as coisas, ele vai sussurrar a _Canção do Irmão Coelhão_. Se por algum motivo os personagens forem junto, Amanda (que na realidade está apavorada, o que faz com que ela comece a cheirar a coelho) vai se acalmar o bastante para conversar.

O Irmão Coelhão vai lhes oferecer comida e água, enquanto providencia uma pequena fogueira para se aquecerem. _"Infelizmente só tenho hortaliças e comida de coelho."_ ele diz. Por alguma razão, ele transparece ser o ___Irmão Coelhão de cabo a rabo (literalmente)___, não apenas na aparência totalmente homem-coelho ao ponto que é simplesmente IMPOSSÍVEL achar que é fantasia e protéticos, mas no comportamento quase cavalheiresco: calmo, suave, esperto e gentil como eles sabem que é se eles viram o bastante das ***Aventuras do Irmão Coelhão*** (possivelmente com Justin)

Amanda irá então dizer que os pais dela pareciam diferente desde algum tempo, em especial depois que a fizeram ouvir as novas fitas das ***Aventuras do Irmão Coelhão***. Se os personagens não notaram até então, o Irmão Coelhão irá revelar que essas fitas tem as Mensagens Subliminares para fazer tanto os pais quanto Amanda caírem nas garras do Lebre do Campo... Mas um pouco mais abaixo

+ ***OK... Quem é você?***

> "Meu nome, ou ao menos o que posso dar agora para vocês, é Eaton Rabbit, e não tenho como dizer de outra forma, mas eu sou o VERDADEIRO Irmão Coelhão!"

Ele vai contar então sua história, como visto abaixo em sua ficha, mas de maneira resumida. A realidade é que, seja quem Eaton era antes de ser transformado pelo Lebre do Campo em um Homem Coelho por meio do Lapinador, ele deixou de existir, e acabou criando para o Irmão Coelhão como uma nova _persona_ para si mesmo, e teve uma vida muito feliz, até que novamente o Lebre do Campo se envolveu, e ele teve que abrir mão dessa nova vida... _"E eu não vou deixar ele destruir novamente minha vida... Já me matou uma vez, não vou perder novamente minha vida"_

+ ***Então você conhece aquele outro... Homem-Coelho?***

> "O nome dele é Marshall Hare. Ele se autodenomina o _Lebre do Campo_ e _Senhor da Lapinidade_, e de alguma forma ele sempre procura transformar as pessoas e de alguma forma as colocar sob sua vontade, o que ele chama de _Lapinação_. É um processo extremamente doloroso, e só escapei de ser um escravo dele pois meus instintos lapinos apareceram antes que a sugestão hipnótica dele assentasse e me colocasse sob sua vontade."

Se os personagens perguntaram ou lembram sobre a ___Páscoa dos Coelhos de 1911___, vão lembrar das histórias. Se então tentarem lembrar de mais fatores, vão lembrar que um misterioso Ovo Fabergé foi dado como desaparecido da Corte Russa um pouco antes. Com um sucesso com estilo, vão associar isso a certas lendas sobre um _Fabergé Místico_, uma joia dotada de poderes sobrenaturais de transformação. Não é muito difícil pensar que o Lebre ficou louco, mas conseguiu de alguma forma descobrir e desenvolver formas de transformar pessoas em Coelhos sob sua vontade

+ ***E o que é aquela máquina?***

Amanda então, mesmo que nervosa, vai dizer:

> "Aquele bobão falso chama de _Lapinador_, e ele a usa para transformar as pessoas em coelho agora... Ele não pode colocar as pessoas por muito tempo, pois machuca demais. Eu mesma fui colocada naquela forma e doeu... Doeu muito!"

Ela então começa a chorar. Se nenhum dos jogadores tiver a ideia de a consolar (que belo bando de Heróis da Nova Era esses, né?), o Irmão Coelhão vai a abraçar, encaixando-a em meio ao pelo macio de coelho, enquanto sussurra novamente a _Canção do Irmão Coelhão_, procurando a acalmar... E nisso eles podem ter a certeza: ele **É** o Irmão Coelhão como está na cantiga! *"Não precisa ter medo e nem tampouco se assustar! / O Sempre Atento Coelhão com certeza vai lhe salvar!"*

Ela, então mais calma vai continuar

> "Eles te prendem naquela máquina, com aqueles eletrodos, e começam a emitir uns raios e coisas em você, e aqueles tubos começam a falar para você que você é uma pessoa coelho, é parte da... Lápis Idade... E que você vai sempre obedecer o Lebre do Campo... Eu só consegui escapar por ter notado que aquele não era o Irmão Coelho de verdade... E aqueles não eram meus pais! Pais não fazem o que eles fizeram comigo! Eu só pensava na Canção do Irmão Coelhão e isso me... Me ajudou... A me manter eu mesma e não cair nas mentiras daquele falso, bobo e fedido, quando aquela máquina tentava me convencer a obedecer aquele bobão!" 

A realidade é que o Sr e a Sra Williams, de maneira trágica mas certeira, caíram nas patas do Lebre do Campo devido ao seu passado meio _hippie_ e de sua ligação com o Irmão Coelhão no passado, mas justamente essa ligação no presente salvou Amanda de ser dominada mentalmente pelo Lebre do Campo.

+ ***Amanda, tem mais alguma coisa que você ouviu?***

> "Ele disse que... Estava construindo uma coisa chamada... Super Lápis Ador... E que era para trazer as pessoas para a lápis idade... Por todo o mundo! Criar um Exército de Homens-coelho e virar um Rei-Deus do mundo!"

Mesmo com a forma de falar de Amanda, ficará claro que o objetivo do Lebre do campo é um só.

***Dominar o mundo para a Lapinidade com um Exército de Homens-coelho!!!!***

Esse é a ___Grande Vantagem___ do Marco de Crescente de Ação (_Rising Action Milestone_) 

+ ***OK... Irmão Coelhão... Como você deixou ele assumir seu manto.***

> "Não deixei: ele continuou suas experiências lapinas mesmo após a minha fuga, e deve ter descoberto sobre o programa, já que era um programa muito popular ao redor dos Estados Unidos, chegamos a ter quase tantas Tocas do Coelhão quanto Clubes Hu Dunnit ou Clubes Mouseketeer. E aí aconteceram os desaparecimentos e tudo se voltou contra mim. A minha antiga agente, Priscilla Preston, ainda tentou lidar com a PR, mas decidimos por bem acabar com o show antes que tudo piorasse para o lado dela, e ela vendeu os diretos para a _Peek-a-Boo Entertainment_. Agora, a relação entre o Lebre do Campo e _Peek-a-Boo Entertainment_ é difícil de dizer"

Isso tudo é verdade: não dá para afirmar comprovadamente que a ação do Lebre do Campo nesse momento é relacionada com a _Peek-a-Boo Entertainment_. Não há provas nem que sim e nem que não, e isso é o maior problema.

+ ***E a tal Priscilla, onde ela está?***

> "Eu não sei... Depois que o programa foi desfeito, eu passei um tempo por aí, ocasionalmente me apresentando por conta como o Irmão Coelhão, mas sempre dava um jeito de encontrar. Mas recentemente ela desapareceu..."

Fica a critério de você decidir o que pode ter acontecido com Priscilla e o que isso tem a ver com a história, mas algumas ideias:

+ Ela está tentando fazer com que o falso Irmão Coelhão seja desmascarado: na realidade pode ter sido ela quem pediu para Alpha ou James Marconi pegar esse caso, já que ambos conhecem uma grande quantidade de pessoas, algumas inclusive conhecendo seus segredos;
+ Ela pode ter sido capturada pelo Lebre e lapinada. Nesse caso, fica apenas a questão se ela está sob a vontade do Lebre (onde ela se torna ***Alice Hare***) ou não. Nesse último caso, é possível que ela esteja fingindo estar do lado de Marshall Hare, apenas esperando o momento para o trair;
+ Ela pode estar escondida: ela sabe demais, então chamar a atenção é problema. Ela não quer fazer nada que chame a atenção para si;
+ Ela pode estar nas mãos de Filippo Geppeto ou, ainda pior, estar sob efeito de ***Você tem Cordões, sim!***: nesse caso, Priscilla foi manipulada no passado por Filippo para vender para ele os direitos do Irmão Coelhão, e então passou para seu lado, manipulada para trabalhar por ele, talvez até mesmo sendo quem ajudou indiretamente Marshall Hare a assumir a identidade do Irmão Coelhão e preparar o seu plano de dominação

+ ***E como vamos parar o Lebre do Campo? Não temos como enfrentar um Exército de Homens-coelho.***

Quanto mais tempo os personagens gastarem planejado, considere que ele vai adicionando mais Homens-coelho aos seus números.

Se voltarem para Angola, vão descobrir que agora crianças de todas as idades que se aproximam da Floresta estão sendo sequestradas. Se Henry não estiver entre os sequestrado, Justin estará na cadeia, acusado de sequestrar e molestar menores. Se demorarem muito, existe uma bela chance de Justin cometer suicídio, já que sem suas medicações e sob a pressão da acusação falsa as ideações suicidas provocadas pelos Traumas de Guerra vão voltar e poderá ser o suficiente, junto com o estresse, para que ele considere a morte um alívio!

Mas existe um detalhe que o Irmão Coelhão vai lembrar

> "O Lebre do Campo me odeia: por muito tempo, no auge do Irmão Coelhão no passado, as crianças me amavam, não importava se eu estava no meu programa, nos shows, ou mesmo nas participações especiais, como em _Fred Rogers' Neighborhood_. Ele ambiciona mais que tudo isso: ele é um narcisista maluco por ser idolatrado e amado. Não sei os motivos disso, mas ele é Louco como uma Lebre do Campo e deseja muito essa adoração. E como eu tive isso enquanto o Irmão Coelhão, ele faria de tudo para me destruir."

Se os jogadores sugerirem algum plano que envolva usar o Irmão Coelhão como isca, Amanda irá implorar que não, mas o Irmão Coelhão aceitará sem pensar meia vez: por mais que ele ame ser o Irmão Coelhão, ele não tem mais uma vida, e quase não tem mais a outra, deixando ele com quase nada a perder. Se houver uma mínima chance de vencer o Lebre do Campo e o deter, o Irmão Coelhão a aceitará sem pestanejar.

## Cena 5 - _"O Irmão Coelhão está aqui para te ajudar!"_

Então a melhor forma é gatilhar o confronto final, usando A Grande Vantagem para melhorarem seu plano...

Mas como?

Algumas sugestões:

+ Invadir novamente a base do Lebre do Campo para destruir seu Lapinador e o capturar é quase suicida... Exceto que, sabendo que tem muitos coelhos ali, é sempre razoavelmente fácil fazer essa turba explodir em pânico de alguma forma, desde que consigam bastante de coisas que apavorem coelhos, como raposas ou outros predadores (ou seus barulhos) e fazer uso desse mecanismo para os pressionar até que eles explodam em tensão e saiam correndo, deixando apenas Marshall Hare e meia dúzia de coelhos um pouco mais resistentes, incluindo os Sr e Sra William e talvez Priscilla/Alice;
+ Se passarem a situação para Alpha, tudo estará tão complicado que ela pedirá alguns dias, e então informará que James irá ao campo para ajudar... O que eles não sabem é que ***James Marconi na verdade não Existe***: ele é uma identidade falsa de um dos Espíritos do Século que, em 1911, enfrentaram e venceram Marshall Hare, Nicola Castrogiovanni, o Espírito do Otimismo. E ele pode trazer também outros reforços. Provavelmente esse fato será revelado durante o confronto final entre os personagens e o Irmão Coelhão e Marshall Hare com seu Exército da Lapinidade. Por mais que o tempo tenha passado, ele não esqueceria _"O odioso cheiro daqueles que impediram Nova Iorque de descer a Toca do Coelho pelas minhas mãos, em especial aquele palhacinho imbecil Nicola Castrogiovanni!"_ Com um pouco de sorte, pode até ser que os Antigos Espíritos que o ajudaram e que tenham sobrevivido à caçada do Comitê de Atividades Anti-Americanas do Congresso (___House Un-American Activities Committee - HUAC___) podem dar o ar da graça e ajudar, nomes como Sidney Sheeran ou Oregon Cadmus vindo ao socorro de James/Nicola.
+ Como está perto da Páscoa, sempre pode ter a possibilidade dos personagens aproveitarem a temática lapina do feriado para atiçar o Lebre do Campo contra eles, em especial contra o Irmão Coelhão: o ódio do Lebre contra o Irmão Coelhão é grande o bastante para que ele tente destruir o mesmo a todo custo em um verdadeiro embate em Angola, se devidamente provocado. Claro que pode ser um problema se os jogadores não se prepararem corretamente, mas de repente Amanda e os demais membros da Toca do Coelhão podem ajudar a revelar a verdade. Se isso ocorrer de maneira adequada, o Lebre do Campo pode descobrir-se do outro lado de uma ___Turba de País Furiosos___ prontos para fazer o possível para o transformarem o Lebre e seus soldados (INCLUINDO o Sr e Sra Matthews) em guizado de lebre!
+ Se a coisa apertar, sempre pode ser possível revelar que Priscilla estava do lado do Lebre, mas para o trair no momento certo: ela nunca realmente cedeu ao Lebre, mas diferentemente de Eaton que fugiu, ela fingiu estar do lado de Marshall, até conseguir ajuntar provas e outras informações que a ajudassem a desfazer o mal do Lebre, incluindo projetos que ela enviou para um contato na _Faculdade de Engenharia Cross_ e que lhe mandou informações sobre como deslapinar pessoas que tenham sido lapinadas recentemente. Isso não será o suficiente para devolver ela ou Eaton ao normal, mas o será para os Matthew
+ Contatos no FBI, CIA e outras organizações podem ser úteis, mas lembre-se que o Chefão está sempre por detrás dos planos, então pode ser que na verdade seja uma vitória de Pirro que apenas impeça uma vitória atual mas, pior, entregue a inimigos o projeto do Lapinador, que certamente será usado em outros momentos para os interesses do Chefão. Em especial, o _Partido da Nova Ciência_ ou os _Kroll'X_ poderiam crescer o olho para os eventos, como uma forma de acelerar seus planos (e talvez para uma serialização desse filme do Irmão Coelhão como uma série? Ou o início de uma série com a NOVA)
+ O Irmão Coelhão poderia esconder mais um segredo: existe um "grupo de elite" dos Coelhinhos do Irmão Coelhão originais, uma Toca do Coelhão de nível nacional, que na realidade é considerada uma _Semente Dourada_ (_Golden Seeds_, Shadow of the Century página 153), uma das organizações que recebeu parte da fortuna do _Espírito do Comércio_ Mack Silver após sua morte nas mãos do Doutor Matusalém em uma explosão em 1961. Esses Coelhinhos (que poderiam ou não incluírem Justin) seriam um grupo especializado em ações não-violentas, mas ativas, usando como práxis de ação a ingenuidade, inventividade e ajuda ao próximo do Irmão Coelhão. Não são tão grandes como Os Clubes Hu Dunnit, As Raposas Prateadas ou a Faculdade de Engenharia Cross, mas os Coelhinhos do Irmão Coelhão poderiam estar prontos a agir em um momento de urgência... Que seria agora!
+ Deixe os personagens fritarem para ter uma ideia: isso é um filme de Shadow of the Century. Se não for para chutar a porta, melhor voltar para casa. Não tem pontos para o segundo lugar! É difícil ser um homem quando tudo que você tem é uma arma na mão! Mesmo as maiores estrelas descobrem a si mesmas ao olharem-se no Espelho! É a hora de mostrar o que estão dispostos a fazer para impedir os planos do Lebre do Campo! 

Por fim... Esperamos que eles evitem a Ascensão da Lapinidade! Afinal de contas...

*"O Sempre Atento Coelhão com certeza vai lhe salvar!"*

## Epílogo - As Pontas Soltas

Obviamente, algumas pontas vão ficar soltas... A questão é que você ainda pode tentar resolver elas como forma de encerrar esse filme... (ou deixar para _O Irmão Coelhão Parte II - O Retorno da Ameaça Lapina_, quem sabe)

+ Qual a participação de Christian Matteo (ou melhor, de Filippo Geppeto) e da _Peek-a-boo Entertainment_ nisso tudo. O que era para ser uma coisa divertida virou um pesadelo... Quantos eventos similares ainda ocorrerão? E isso foi apenas um acidente? Ou teriam interesses nisso? Interesses ocultos nas Sombras?
+ Essa provavelmente não será a última vez que os personagens e a NOVA ouvirão falar de Marshall Hare. Mesmo que seja capturado, sem o Clube do Século, quem teria a capacidade de cuidar de um homem-coelho insano?
+ O que nos traz a Amanda e sua Família: o que acontecerá com eles? Ela voltará a ser uma menina como era antes, ou tragicamente ficará como uma menina-coelho? Se for isso, quem poderia cuidar dela? Os Sr e Sra Williams seriam confiáveis o bastante? Ou Justin poderia a receber em sua casa, mesmo em segredo? E qual o custo desse segredo?
+ Imaginando que a ideia da elite da Toca do Coelhão original voltar a ajudar o Irmão Coelhão, o que vai afetar isso na vida de Justin?
+ Esse caos todo irá trazer atenção à cidade de Angola, para o bem e para o mal... Como eles vão lidar com toda essas coisas?
+ Por fim, que fim terá o Irmão Coelhão? Será que ele terá que se ocultar novamente? Onde? A cidade de Angola estaria disposta a ajudar, ou ao menos a o deixar em paz na velha Fazendo _Amish_ onde ele estava escondido? Ou será que ele continuará sendo caçado por organizações com interesses escusos, como o Instituto Talia e a CIA?
+ Além disso... Pode ser que agora os personagens conheçam a verdade sobre James Marconi/Nicola Castrogiovanni. E provavelmente, se Marshall Hare está por aí, outros inimigos, como Lemont Fitzlefebvre ou talvez um herdeiro de Anthony Fagin, a _Sombra da Inocência do Século 19_... E isso vai colocar um alvo nas costas de Nicola ou dos personagens... E como eles lidarão com isso?

## **Apêndice 1 - NPCs**

### Extras

+ ***Shelly Mitchell (neé Hamon):***
    + ___Extremamente leal ao marido___ Razoável (+2);
    + ___Justin precisa de mais que o tratamento que recebe das pessoas___ Péssimo (-2);
+ ***Justin Mitchell:***
    + ___Veterano e Líder da Toca do Coelhão___ Bom (+3); 
    + ___A Guerra já me destruiu o Bastante, não precisam terminar o serviço___ Ruim (-1);
+ ***Henry Mitchell:***
    + ___Amigo de Amanda e Toby___ Bom (+3); 
    + ___Se sente culpado pelo desaparecimento de Amanda___ Ruim (-1);
+ ***Wilhelmina "Willa" Michaels:***
    + ___Mãe de Toby e Amiga do passado de Justin___ Razoável (+2); 
    + ___Viúva, Toby foi tudo que sobrou___ Péssimo (-2);
+ ***Toby Michaels:***
    + ___Amigo_ nerd, _forte mas gentil de Amanda e Henry___ Razoável (+2); 
    + ___"Tem algo muito esquisito sobre o Irmão Coelhão"___ Péssimo (-2);
+ **Homens-coelho de Marshall Hare**
    + ***Homem-Coelho alucinado*** Razoável (+2); 
    + ***Instintos Animais muito Aflorados*** Péssimo (-2)
+ **Turma Coelhal do Coelhão**
    - ***Homens (e Crianças) parcialmente transformados em Pessoas-Coelho*** Razoável (+2); 
    - ***Instintos não controlados*** Péssimo (-2)
+ ***Amanda Matthew:***
    + ___Menina desaparecida virando criança-coelho___ Regular (+1); 
    + ___"Esses não são meus pais e nem o Irmão Coelhão"___ Péssimo (-2);
+ ***Sr. e Sra. Matthew:***
    + ___Um casal meio-coelho___ Razoável (+2); 
    + ___O Lebre do Campo é Nosso Senhor___ Péssimo (-2);

### **Marshall “Lebre” Hare, o Homem-Coelho louco como uma Lebre do Campo**

Poucos se lembram da *Páscoa dos Coelhos* de Nova Iorque no início da década de 1910, e de como, por muito pouco, Marshall Hare não conseguiu transformar toda a Nova Iorque em sua Grande Toca, seu pequeno reino no qual mandaria como um rei.

Não fosse o Clube do Século e seus Espíritos, ele teria conseguido. Sobrou para ele fugir, seus instintos gritando para ele fugir daqueles humanos que o caçavam.

Poucos ainda lembram da história do garoto que, de tanto ser chamado de *louco como uma Lebre do Campo*, acabou descobrindo (ou imaginando-se) como alguém saído diretamente do País das Maravilhas, descendente da Lebre Louca do Campo!

Mas ele descobriu que o processo de tornar-se um coelho teve uma vantagem: diferentemente de muitos outros Negativos, vilões que acabaram tornando-se subordinados das Sombras do Século (e morrendo durante de velhice, por não gozarem da vitalidade excepcional que os mesmos gozam junto com os Espíritos do Século), o seu processo de envelhecimento foi modificado e tornado mais lento pelas mutações que sofreu ao usar o Fabergé Secreto, que foi destruído com a queda do Império Russo e a Ascensão da União Soviética.

Mas isso não impediu ele de tentar transformar pessoas em homens coelho, muito pelo contrário: usando estudos da recentemente descoberta área da Genética, ele criou sua própria forma de Eugenia, uma forma onde ele desenvolveu uma série de processos mutagênicos para transformar as pessoas, por meio de uma série de versões de uma máquina conhecida como *Lapinador*, que ele foi aprimorando, e perdendo, e quebrando, e recriando ao longo das décadas, até que a mesma tornou-se o que parece um montão de sucata mas na prática é algo extremamente efetivo. E agora ele acredita ter chegado na versão suprema do *Lapinador* e só precisa de "voluntários" (sejam voluntários de verdade ou não) para ajudar com sua nova invenção.

E então... Ele criará um *Lapinador* de nível mundial! E ele será o senhor do Mundo Leporino!

#### **Aspectos**

|       ***Tipo:*** | ***Aspecto:***                                                              |
|------------------:|:----------------------------------------------------------------------------|
|       ***Gonzo*** | Um homem louco como uma lebre do campo, transformada em uma!                |
| ***Dificuldade*** | Hiperatividade e Egomania, a receita do Desastre                            |
|                   | O *Oryctolagus Sapiens* é o futuro, e eu sou o início e senhor desse futuro |
|                   | Experiências Místicas do passado como um caminho                            |
|                   | Senhor da Lapinidade dos Homens coelhos                                     |

#### **Papéis**

|    ***Papel:*** | ***Nível***        |
|----------------:|:-------------------|
|   **Assassino** | *Razoável (+2)*    |
|  **Autoridade** | *Bom (+3)*         |
|   **Criminoso** | *Bom (+3)*         |
| **Manipulador** | *Ótimo (+4)*       |
|   **Cientista** | *Excepcional (+5)* |
|     **Soldado** | *Razoável (+2)*    |

#### **Façanhas:**

- ***Descendo a toca do Coelho (Gonzo):***
  - LVL1: ***Ciência do Coelho:*** Enciclopédia Perambular (Genética, do Cérebro)
  - LVL2: ***O Futuro Leporino:*** Confia em Mim + Confia em você mesmo, usado para chamar seus *Soldados Coelho*
  - LVL3: ***Lapinador:*** Possível... Em teoria + Basta Fita Adesiva + Ciência Estranha, usado para desenvolver o *Lapinador*

#### **Lapinador**
- *Máquina que transforma pessoas em homens coelho*
- *Funciona a base de silver-tape, mas não é tão confiável quanto silver-tape*
  - ***Hipnose perfeita:*** Ataca o alvo mentalmente pelo nível de *Cientista* de Marshall. Se o alvo for derrotado, ele estará ***sob controle*** de Marshall
  - ***Mudança de Corpo:*** Ataca o alvo fisicamente mudando sua estrutura genética com o nível de *Cientista* de Marshall... Se o alvo for derrotado nesse processo, ele terá a forma de um *Homem Coelho*, como um Aspecto Gonzo, substituindo permanentemente um dos Aspectos do Alvo como se fosse uma Consequência Extrema


### **Alice Hare, a Esposa do Senhor das Lebres**

Ninguém sabe a origem de Alice Hare, a Esposa do Senhor das Lebres... Nem ela: ela vê a realidade e a fantasia como uma coisa só, e ainda que (pouca coisa) mais razoável que o Lebre do Campo, ela nunca deixaria seu Coelhão de lado.

#### **Aspectos**

|       ***Tipo:*** | ***Aspecto:***                                            |
|------------------:|:----------------------------------------------------------|
|       ***Gonzo*** | Uma mulher coelho apaixonada em um relacionamento abusivo |
| ***Dificuldade*** | Levemente menos louca que seu "Coelhão"                   |
|                   | Realidade e Fantasia para ela é uma coisa só!             |

#### **Papéis**

|    ***Papel:*** | ***Nível***     |
|----------------:|:----------------|
|  **Autoridade** | *Bom (+3)*      |
|   **Criminoso** | *Bom (+3)*      |
| **Manipulador** | *Razoável (+2)* |
|   **Cientista** | *Ótimo (+4)*    |

#### **Façanhas**
- ***Senhora Coelho (Gonzo):***
  - ***Gargantilha de Coelho (LVL1):*** Como *Senhor Monteiro*, mas apenas para ocultar sua forma lapina e voltar para sua antiga forma humana
  - ***Orelhas do Coelho (LVL2):*** um dispositivo curioso, essa Orelhas de Coelho podem ser colocadas em qualquer alvo com um Ataque bem sucedido e o pagamento de 1 PD. Além do dano, o alvo é colocado sobre uma *Hipnose Lapina*, onde ele passa a estar sob as ordens de Alice. O alvo pode resistir com *Vontade* normalmente, contra uma dificuldade igual ao Esforço do Ataque.

### **Eaton Rabbit, o Verdadeiro Irmão Coelhão**

#### **Aspectos**

|       ***Tipo:*** | ***Aspecto:***                                                          |
|------------------:|:------------------------------------------------------------------------|
|       ***Gonzo*** | Homem-Coelho de Verdade, não uma fantasia                               |
| ***Dificuldade*** | Agora sou APENAS o Irmão Coelhão, não vou perder minha vida *novamente* |
|                   | Resquícios da Páscoa dos Coelhos de 1911                                |

#### **Papéis**

|             ***Papel:*** | ***Nível***     |
|-------------------------:|:----------------|
| **Homem-Coelho (Gonzo)** | *Bom (+3)*      |
|               **Espião** | *Bom (+3)*      |
|            **Diletante** | *Razoável (+2)* |
|                 **Face** | *Ótimo (+4)*    |

#### **Façanhas**
- **O Irmão Coelhão (Gonzo):**
  - **Nível 1: Conhecimento de Desenho** *Enciclopédia Perambular (Psicologia e Desenhos Animados)*
  - **Nível 2: Lógica de Desenho** *Não Acredito nisso (invertida, para fazer com que coisas sem lógica ou fora do bom-senso se tornem plausíveis. Quanto mais implausível, mais difícil de fazer, mas a dificuldade pode abaixar quanto mais alto o nível do VHS) + Ciência Estranha (Formas de dobrar a realidade: quanto mais ilógico for, mais difícil de ser usado, mas com a dificuldade caindo quanto mais alto for o VHS)*
  - **Nível 3: A Turma do Irmão Coelhão:** *Confia em Mim + Confia em você mesmo + Possível... Em teoria*

#### **Interpretação**

Eaton é um homem (coelho?) que basicamente vive para uma fantasia, assim por dizer: ele é quase patologicamente alegre e positivo quase o tempo todo, mantendo seu pelo lustroso limpo e perfumado tanto quanto possível. Os poucos a quem ele revela sua história, porém, descobrem rapidamente que por baixo da cara boba alegre, existe um coração que sofreu as piores desgraças que alguém pode imaginar, quase como se ele tivesse morrido não apenas uma, mas duas vezes, mas que ainda assim não se deixa cair e faz o melhor para se reerguer.

#### **História**

Eaton era um homem comum, como vários outros: um cara comum, de uma cidade pequena do interior, teve sua carga de aventura (e sofrimento) ao ser alistado para a 2ª Guerra, no *front* da Ásia. Ao voltar, desligou-se do exército e, após uma faculdade de Administração com classes de Teatro em uma faculdade no seu Estado de Nascimento (Oklahoma), voltou à sua cidade e montou um pequeno negócio, a *General Store* da cidade...

Até que acabou indo parar no lugar errado e na hora errada.

Um garoto esquisito mandou um pedido estranho de peças de reposição de TVs, rádios e outros equipamentos eletrônicos. O estranho era a combinação das coisas: teriam que ter pifados todos os eletrônicos da casa ao mesmo tempo.

Sem falar que o endereço também era esquisito: uma parte da cidade na região rural, onde na teoria não deveria ter nada, ainda mais que consumisse energia elétrica.

*"Bem... Um pedido é um pedido"*, ele pensou *"Assim que acabar o dia, fecho a loja e vou efetuar a entrega."*

Aquele foi o último dia em que ele foi visto como era...

Ao ir para o tal local, ele chegou em uma velha fazenda de milho. O curioso é que ele notou que havia uma linha de energia próxima, mas nada oficial, certamente: ele sabia dos improvisos que alguns agricultores mais pobres faziam para obter energia de maneira ilegal... Ele pensou em ver o que estava acontecendo antes de efetuar a entrega... Em especial por ter ouvido rumores esquisitos sobre pessoas abduzidas, desaparecidas em meio à noite nas rodovias e lanchonetes locais. Moços e moças de maneira geral, mas também pessoas de qualquer idade ou classe social.

Ao se aproximar do celeiro, procurando o dono da propriedade, ele começou a notar o cheiro forte de coelho: ele fazia caçadas a coelhos no período certo, sabia o cheiro dos mesmos... Mas começou também a ouvir uns guinchos esquisitos, parecidos com gritos de dor humanos, ou guinchos de coelhos em sofrimento.

Ele se aproximou do celeiro, quando viu a coisa mais esquisita de sua vida!!! O que parecia um cruzamento profano entre um Andarilho das Estradas de Ferro com um Coelho *Jackrabbit*, longas orelhas ao topo da cabeça substituindo as humanas que deveriam estar ao lado, um focinho com um grande par de incisivos saindo pelos lábios leporinos, mãos agarrando com força uma cadeira de dentista com uma combinação bizarra de fios, aparelhos e ganchos. Obviamente, seja o que for que estava sentado na cadeira, estava em grande dor, ele conseguia reconhecer do seu passado em Iwo Jima.

Mas o mais esquisito era a quantidade de... *criaturas*... próximas e suas formas bizarras, que espantariam até mesmo um leitor voraz dos antigos *pulp*: pessoas altas como humanos, mas com corpos similares a coelhos e lebres, ainda que suas proporções gerais e postura ereta fossem suficientemente humanas para colocar a sanidade dele em xeque.

Foi quando ele percebeu que tinha que avisar sobre isso ao xerife. Dane-se que iam falar que ele estava sofrendo da *Fadiga de Combate* que muitos sofriam no *front* e após voltarem: havia uma pessoa em sofrimento e uma situação bizarra o bastante para demandar a atenção do Xerife... Talvez até mesmo do Governo!

E como acontece nessas horas... Um galho seco que ele pisou acidentalmente delatou sua posição.

Ele mal teve tempo de se voltar, quando notou que, de tocas escavadas no chão como os soldados japoneses fariam, saiam mais e mais desses homens coelho... Alguns até com *dogtags* da época da Guerra! Ele era apenas um, e não foi capaz de se evadir dos mesmos.

Ele foi levado ao celeiro... Quando viu o andarilho meio-coelho encerrar sua transformação, e uma criatura similar a um homem-lebre saído de *Alice no País das Maravilhas* estava próximo. A criatura se voltou a ele, e ele podia ver nos olhos o olhar sádico e feliz de ter obtido mais uma presa. Ele voltou-se a algo que parecia um painel de um transmissor de rádio e começou a mexer uma série de potenciômetros, enquanto alguns dos homens coelho que o capturaram começaram a rasgar suas roupas e a o amarrar à cadeira.

Ele imaginava que o Lebre iria o torturar: ele ouviu relatos mais que o suficiente das torturas que o Exército Imperial Japonês aplicava contra os que capturava... E como agir em caso de captura!

Mas nada o preparou para o *Lapinador* do Lebre do Campo.

Quando os primeiros choques e luzes o atingiram, ele começou a gritar seu nome, o que faria pela última vez por muito tempo, talvez para sempre. Enquanto as energias eugênicas do *Lapinador* mudavam sua forma por alguma razão estranha, vozes em sua mente começavam a o seduzir, para servir ao Lebre do Campo, Marshall Hare, como senhor da Lapinidade.

Mas nesse momento, nem Marshall Hare imaginava que ele tinha tanta força de vontade...

E nem o novo Homem-Coelho!

Ainda mais quando essa força de vontade se somou aos instintos apavorados de um Coelho preso sem ter como fugir para sua toca!

Com um guincho terrível, seu novo corpo despertou, mas seus instintos o impulsionaram para lutar! Como um coelho acossado, ele usou toda a sua força para arrebentar as amarras e fios que o prendiam ao lapinador e saiu correndo, em disparada, chutando tudo que tivesse em seu caminho com suas patas poderosas, derrubando o que deveriam ser seus compatriotas com facilidade... 

Ele ainda pode ouvir o Lebre do Campo gritar: *"Peguem-no! Precisamos doutrinar a mente dele!"*

Mas ele não tinha sobrevivido ao Pacífico para ter sua liberdade tomada ali! Sua mente, misturada com seus novos instintos leporinos, o fizeram correr para a mata, procurando um local para se abrigar, evitar os predadores, evitar aquela criatura que o transformou e seus subordinados. Achando uma velha mina abandonada, ele se ocultou lá e esperou, até que a adrenalina baixou e ele notou que, ao menos por aquele momento, estava seguro: aparentemente, os servos desse Lebre do Campo não tinham pego seu cheiro, então não poderiam o localizar.

Nos dias seguintes, ele procurou compreender no que se transformou: seu novo corpo poderosos, capaz de correr mais rápido e pular mais alto que os melhores atletas olímpicos, sua mente desembestada capaz de processar informações de uma maneira quase dolorosa, seus novos sentidos eram muito aprimorados... Dolorosamente aprimorados, de fato: quando ouviu o trem passar perto de um lago onde pode beber água, quase enlouqueceu, seus instintos lapinos gritando *"Predador!!!!"*

Foram dias que se tornaram semanas e que se tornaram meses, comendo nozes, bagas e outras coisas que encontrava na floresta, bebendo da água do rio e deixando que a chuva o lavasse, fazendo suas necessidades na caverna ou em locais para indicar a outros que ali era seu território e que quem tentasse algo poderia ter problemas, ocasionalmente sendo visto por caçadores que levaram lendas urbanas e histórias absurdas sobre homens-coelho gigantes. Apenas dois anos depois, quando conseguiu aprender a controlar e usar suas novas habilidades sem enlouquecer e sem ceder ao coelho dentro dele, se familiarizando e entrando em contato e ficando em paz com ele é que ele decidiu voltar. Depois de todo esse tempo, já mais calmo (ou tanto quanto seus novos instintos permitiam) ele pode voltar até a velha fazenda, e notou que o celeiro da mesma foi destruída...

...e ele foi dado como desaparecido, como veio a descobrir ao caminhar pela noite em sua velha cidade, os cartazes amarelados de _"Desaparecido"_ ao lado dos anúncios do pregoeiro de que sua velha loja seria leiloada, o dinheiro disponibilizado para quitar suas dívidas.

Ele agora estava morto, até onde sabia...

Ele ainda conseguiu obter umas roupas velhas largadas no meio da rua, e seguiu para a estrada...

E ao poucos foi ajudando crianças que encontrou, disfarçado, em entroncamentos de trens e nas beiras de estrada. Então ele criou para si mesmo a história sobre um certo Irmão Coelhão, como um novo alter ego, baseado no velho Tio Wiggly Longears, dos livros que sua mãe lia a ele quando criança. E as histórias sobre o Irmão Coelhão começaram a se espalhar.

Ele então descobrir que as crianças estavam espalhando essa notícia, e conseguiu então uma forma de se manter, quando encontrou alguém que sabia sobre isso: uma pessoa que lembrava da lenda urbana da *Páscoa dos Coelhos* de 1911, quando crianças foram transformadas em Coelhos por meio de Ovos de Páscoa encantados. Essa pessoa ajudou a finalizar essa sua história do um *Irmão Coelhão*, alguém para ajudar e consolar, como o Tio Wiggly Longears, vivendo aventuras e ajudando crianças.

Bozo tava na moda, assim como o Capitão Canguru e *Howdy Doody*, programas infantis com personagens fantásticos e desenhos animados passando mensagens positivas às crianças. Isso poderia ajudar o agora chamado Eaton Rabbit a conseguir como se sustentar e passar-se como uma pessoa em uma roupa de coelho, não como um homem-coelho de verdade.

Esse foi o melhor período de sua vida: Eaton Rabbit viveu dez anos de muita felicidade como o *Irmão Coelhão*, e interagiu com alguns dos maiores, como Bozo e Fred Rogers, e seu programa passava por todo os Estados Unidos, com atores fazendo o papel do Irmão Coelhão quando e onde ele não poderia estar.

Mas obviamente isso chegou ao fim, e de uma maneira pavorosa, quando uma série de boatos sobre crianças desaparecendo e tornando-se aberrações começou a se disseminar por meio de tabloides, o que levou a uma queda rápida de audiência e cancelamento do show.

Eaton sabia o que aconteceu na realidade: o Lebre do Campo o descobriu e usou-se de seu programa para usar o lapinador em crianças e jogar a lama nele! Ele não podia mais continuar assim!

Com o fim do programa, ele apenas desejou boa sorte à pessoa que criou junto com ele o Irmão Coelhão e partiu. Ao se ocultar, começou a pesquisar sobre o Lebre do Campo e sobre a Páscoa dos Coelhos, e foi quando descobriu mais coisas sobre o Lebre, sobre o Fabergé Secreto, os Ovos e os Espíritos do Século...

E então ele descobriu mais coisas: aparentemente, quando ele se evadiu, Marshall Hare capturou a pessoa que o ajudou e assumiu o Irmão Coelhão como personagem! E está usando isso para capturar mais e mais crianças!

E ele não pode deixar as coisas como estão!

Afinal de contas, como dizia a Canção do Irmão Coelhão:

> *“O Irmão Coelhão está aqui para te ajudar!*
>
> *O Irmão Coelhão sempre vai te amparar!*
>
> *Seja na Terra, no céu, na água ou no mar!*
>
> *Seja gigante, bruxa, pirata ou outra pessoa má!*
>
> *Não precisa ter medo e nem tampouco se assustar!*
>
> *O Sempre Atento Coelhão com certeza vai lhe salvar!"*

### **Priscilla Preston, ex-diretora do show *As Aventuras do Irmão Coelhão***

#### **Aspectos**

|       ***Tipo:*** | ***Aspecto:***                                                             |
|------------------:|:---------------------------------------------------------------------------|
|     ***Aspecto*** | Alguém que está vendo os cordões em si                                     |
| ***Dificuldade*** | Zelosa demais para assumir que errou                                       |
|                   | "As Aventuras do Irmão Coelhão *é maravilhoso, alguma coisa está errada!"* |
|                   | Sabe muito mais coisas do que realmente quer admitir (ou deveria)          |

#### **Papéis**

|  ***Papel:*** | ***Nível***     |
|--------------:|:----------------|
|  **Inventor** | *Razoável (+2)* |
|   **Cérebro** | *Bom (+3)*      |
|     **Líder** | *Bom (+3)*      |
| **Diletante** | *Ótimo (+4)*    |

#### **Façanha**

- Formado na Faculdade

#### **Interpretação**

Priscilla ficou popular recentemente como diretora do show *As Aventuras do Irmão Coelhão*, mas sua fama rapidamente desapareceu quando comentários esquisitos sobre o show começaram a circular, em especial envolvendo o desaparecimento de partes do *cast* humano, e alguns comentários apócrifos sobre acontecimentos relacionados a pessoas que se tornavam desenhos (algumas montagens de efeitos especiais, certamente). Ela é orgulhosa de sua obra e zelosa quanto a ela, mas desde que foi afastada pelo dono da *Peek-a-boo Entertainment*, algo está piscando na mente dela, um sinal de alerta de que ela possa estar sendo usada de alguma forma não muito positiva


### Filippo Geppeto, Sombra da Fantasia

#### **Aspectos**

|       ***Tipo:*** | ***Aspecto:***                                                                      |
|------------------:|:------------------------------------------------------------------------------------|
|      ***Sombra*** | Sombra da Fantasia (Gonzo)                                                          |
| ***Dificuldade*** | Crianção demais para seu próprio bem                                                |
|                   | Alucinado em uma existência de Fantasia                                             |
|                   | *"Seu sonho é tão belo... Seria uma pena se ele se tornasse um pesadelo!"*          |
|                   | Controlar a imaginação é controlar a narrativa, a história, as pessoas, a realidade |

#### **Papéis**

|    ***Papel:*** | ***Nível***        |
|----------------:|:-------------------|
|   **Assassino** | *Razoável (+2)*    |
|  **Autoridade** | *Bom (+3)*         |
|   **Criminoso** | *Bom (+3)*         |
| **Manipulador** | *Ótimo (+4)*       |
|   **Cientista** | *Excepcional (+5)* |
|     **Soldado** | *Razoável (+2)*    |

#### **Façanhas:**

- Tudo tem seu preço (Espião)
- Senhor Monteiro: Christian Matteo
  - Dono da Peek-a-boo Entertainment
  - Ocasionalmente as coisas saem do controle... e eu não gosto disso

##### **Façanha Gonzo: O Titeriteiro Supremo**

- **Nível 1: Improvisação suprema** *Formado na Faculdade*
- **Nível 2: O Mago atrás dos panos** *Olho para os detalhes + Charme Acachapante*
- **Nível 3: Você tem cordões, sim!** *Enciclopédia Perambular (Psicologia e Hipnose) + Ciência Estranha (Formas de dobrar a vontade da pessoa) + Não Acredito nisso (invertida, para fazer com que coisas sem lógica ou fora do bom-senso se tornem plausíveis)*

#### **Interpretação**

Filippo Geppeto é uma das Sombras mais terríveis que existem: se por um lado utiliza seu conhecimento sobre fantasia para alienar as pessoas e com isso obter lucros, por outro é um psicopata que vive para destruir os sonhos daqueles que estão em seu caminho, preferencialmente usando eles para levar essas pessoas à ruína. Atualmente se disfarça sob uma alcunha, a de Christian Matteo, dono da famosa companhia e estúdio de animações e programas infantis Peek-a-boo Entertainment. Seu objetivo maior é colocar as pessoas sob seu poder, e a melhor forma para isso é controlar a narrativa da história, por meio da imaginação das pessoas. Parece sempre estar calmo, mas quando se descontrola parece um crianção birrento, mas com poder o bastante para colocar as pessoas umas contra as outras. Dizer que ele enxerga o mundo como um tabuleiro de xadrez não faz justiça a ele: na realidade, ele o vê como seu parquinho particular, e todas as pessoas são parte do seu Forte Apache.

## Apêndice 2 - Sugestão de Playlist para a aventura

+ Quaisquer músicas infantis dos anos 80
+ Martika - Toy Soldiers
+ Torch - Soft Cell
+ Might Wings - Cheap Trick
+ Only When You Leave - Spandau Ballet
+ Footloose - Kenny Rogers
+ Island in the Stream - Dolly Parton & Kenny Rogers
+ Jive Talkin' - Bee Gees
+ Head over Heels - Tears For Fears
+ Walk of Life - Dire Straits
+ Mickey Mouse Club March - The Mickey Mouse Club
+ Sweet Home Alabama - Lyndr Skynr
+ The Goonies' Good Enough - Cindy Lauper
+ I like Chopin - Gazebo
+ Suzie Q - Creedance Clearwater Revival

## Apêndice 3 - As Tocas do Coelhão como _Sementes Douradas_

> *“O Irmão Coelhão está aqui para te ajudar!*
>
> *O Irmão Coelhão sempre vai te amparar!*
>
> *Seja na Terra, no céu, na água ou no mar!*
>
> *Seja gigante, bruxa, pirata ou outra pessoa má!*
>
> *Não precisa ter medo e nem tampouco se assustar!*
>
> *O Sempre Atento Coelhão com certeza vai lhe salvar!"*

A _Canção do Irmão Coelhão_ passou a ser o lema de muitas crianças nos anos 1950 e 1960. ***As Aventuras do Irmão Coelhão*** com seu icônico personagem tornou-se um modelo para milhares de crianças ao redor dos Estados Unidos. Ao defender a ideia da inventividade, ingenuidade, tolerância e colaboração na comunidade como a melhor maneira de resolver problemas, o _Irmão Coelhão_ passou a inspirar a participação comunitária e a atividade cívica em muitos jovens, ainda mais quando começou-se a estimular que os Coelhinhos (como seus fãs eram conhecidos) formassem suas Tocas do Coelhão em sua cidade, formando atividades cívicas, como auxílio a idosos, reforço escolar para crianças com problemas, ajudar veteranos da Guerra e outras.

Esses casos de atividade cívica pululavam ao redor dos Estados Unidos, e mesmo em países onde o show das ***Aventuras do Irmão Coelhão***: com a ajuda de adultos, os Líderes Coelhão, esses Coelhinhos continuavam a realizar atividades cívicas, em paralelo ou em conjunto com outros grupos com atividades similares, como os Escoteiros ou o Rotary Club. 

Originalmente, essas _Tocas do Coelhão_ não eram muito diferente dos Escoteiros, mas conforme os Coelhinhos originais foram crescendo, eles começaram a se estruturar melhor, em especial quando agiam com outras organizações locais. A importância foi reconhecida quando um grupo de Coelhinhos originais resgatou um grupo de escaladores em apuros na região dos Apalaches: por conhecerem bem a região, logo encontraram pistas e indicaram o caminho para os grupos de resgate, garantindo a sobrevivência dos mesmos!

Curiosamente, as _Tocas do Coelhão_ continuam em contato umas com as outras, mas suas ações são muito mais locais do que a de outras organizações similares, como os Clubes de Mistério Hu Dunnit. Entretanto, já houve casos onde os Coelhinhos das Tocas do Coelhão de uma região se uniram em objetivos comuns, como localizar uma pessoa perdida ou angariar recursos para ajudar em desastres naturais.

Mesmo com o cancelamento das ***Aventuras do Irmão Coelhão***, algumas Tocas do Coelhão continuaram sendo ponto de congregação dos fãs do mesmo, os Coelhinhos, e sempre mantiveram os valores do Irmão Coelhão vivos, pela lembrança das ***Aventuras do irmão Coelhão*** e por meio das Atividades Cívicas que o Irmão Coelhão estimulou os Coelhinhos a terem.

+ ***Agenda:*** apoiar as comunidades locais, manter a mensagem do Irmão Coelhão viva
+ ***Recursos:*** muito poucos, já que dependem muito mais dos próprios envolvidos e seus recursos pessoais que de financiadores ou patrocinadores, ainda mais que com o final das ***Aventuras do Irmão Coelhão*** possuem poucos pontos de contato, em geral apenas as _Tocas do Coelhão_ das redondezas.
+ ***Patrão:*** apesar dos poucos recursos, ser parte da _Toca do Coelhão_ local pode significar ter amigos dispostos a se arriscarem para o ajudar, pois sabem que você faria o mesmo se fosse o contrário.
+ ***Aliados:*** os Coelhinhos costumam respeitar qualquer um que esteja ajudando a comunidade, e podem ajudar aqueles que tenham como objetivo ajudar as pessoas da comunidade
+ ***Contatos:*** em geral, os Coelhinhos não se envolvem com pessoas de fora, sempre sendo muito focados em suas comunidades locais. Entretanto, se os personagens comprovarem ser fãs do Irmão Coelhão, poderão conseguir algumas respostas fáceis dos Coelhinhos da _Toca do Coelhão_ local
+ ***Rival:*** Em geral, as _Tocas do Coelhão_ não possuem problemas com pessoas que tentem alcançar o mesmo objetivo que eles... Mas se perceberem que as ações dos personagens podem prejudicar sua comunidade, eles farão de tudo para as atrapalhar. E creia-me: as lições do Irmão Coelhão nisso são muito sérias!
+ ***Inimigos:*** Qualquer tentativa de prejudicar diretamente uma comunidade onde tenha uma _Toca do Coelhão_ irá chamar a atenção não apenas dos Coelhinhos, mas das organizações locais E das _Tocas_ da região, o que tem todo potencial para tornar a vida dos personagens um pesadelo!

## Apêndice 4 - NOVA: uma Semente Dourada para _Shadow of the Century_ 

NOVA (_Network Of Variable Agents_ - **Rede de Operativos Variáveis**) foi fundada pelo estranho e inteligente James Marconi, na época um programador do AI Labs do MIT. Tentando aprimorar o uso das melhores tecnologias atuais, James desenvolveu uma rede de pessoas comuns que enfrentam ameaças incomuns. _"Ordinarius populus adversus minas aliena"_, _pessoas comuns enfrentando ameaças bizarras_ é o lema da NOVA.

A principal estratégia da NOVA é baseada na ideia de _células compostas_: seus agentes na realidade são poucos e espalhados por aí, muitas vezes desconhecidos uns dos outros, mas eles são convocados por ele mesmo ou sua segundo-em-comando conhecida apenas como _Alpha_. Eles são convocados conforme a necessidade, alguns deles podendo até mesmo serem colocados _em espera_ para demanda adicional. Eles recebem bem, para ser honesto: além de dinheiro baseado nas suas competências e participações, eles podem a qualquer momento solicitar a James um favor qualquer e ele fará o máximo possível para o conceder (e normalmente o irá)

Agentes NOVA em geral precisam também seguir determinadas regras quanto a discrição e não-violência, mas mesmo NOVA sabe que ocasionalmente as coisas saem do controle, então ela também tem em seus números uma boa quantidade de operativos com retrospecto militar.

Existem dois segredos sobre NOVA:

1. eles são uma _Semente Dourada_ uma boa quantidade de dinheiro tendo surgido em contas secretas que James mantém em paraísos fiscais como Luxemburgo, Ilhas Jersey e Suíça;
2. Na realidade James Marconi "Não existe": ela é uma ***identidade falsa, mas muito elaborada*** de um dos últimos Espíritos do Século ainda em atividade, ___Nicola Castrogiovanni, o Espírito do Otimismo___

Nicola já estava fora dos EUA quando o Clube foi desfeito, acusado por McCarthy de ser parte do Perigo Vermelho, como previsto por uma velha patrocinadora do clube ligada a ele, Mary Alethea, que ajudou ele a ser localizado pelo antigo _Espírito da Educação do Século 19_, _Frederick Van Der Merwe_, também conhecido como _Don Cagliostro_. Ele já tinha partido para a África do Sul, onde Frederick partiu em paz de volta a sua Cidade Natal de Bloemfontein, Nicola sob outra identidade falsa, tecnicamente um neto de Frederick, _Rasmus Van Der Merwe_, mas logo ele teve que partir da África do Sul com o aumento do autoritarismo imposto pelo Apartheid e por suas ligações (como Rasmus) com o _Congresso Nacional Africano_, o partido Clandestino cujo líder Nelson Mandela foi aprisionado e condenado em 1962 no Julgamento de Rivonia.

Como parte de suas ações, Nicola aprendeu um truque ou dois sobre como se manter na maciota, oculto por várias identidades falsas: tecnicamente, tanto Nicola Castrogiovanni quanto Rasmus Van Der Merwe morreram em suas casa, e James Marconi não possui nenhuma relação com eles, exceto por ter recebido dinheiro de um fundo beneficente criado pelos Van Der Merwe.

Como um Espírito-em-Segredo, ele aprendeu como atuar nos bastidores, e seu otimismo sempre lhe mostrou que o jogo ainda não terminou: claro, o mundo é sombrio, sujo e perigoso, e os Espíritos pereceram ou estão escondidos, ele não sabe qual das opções, mas ele descobriu uma nova forma de lutar o Bom Combate, de mostrar formas das pessoas serem otimistas e continuarem na luta. Mesmo que ele tenha que sujar as mãos, e ele vai o fazer, ele vai manter-se com a moral elevada!

Ele então criou uma nova organização, assim que notou as coisas acontecendo nos bastidores, a batalha secreta pela realidade na qual, desde a queda do Clube do Século, deixou a humanidade sem campeões: os Kroll'X, os Manuscritos de Catargo, todas essas coisas que estavam rasgando a realidade e a tornando horrível para todos, exceto uma meia dúzia. Claro que o próprio Clube foi parte do problema, mas era parte da descrição do serviço quando você lida com coisas que rasgam a realidade.

E foi então que ele recebeu ajuda: ele descobriu em certo momento que uma grande quantidade de dinheiro foi depositada em suas contas secretas em locais como Luxemburgo, Suíça, Hong Kong e nas Ilhas Jersey. Ele investigou e descobriu quem fez isso: era uma _Semente Dourada_ uma herança deixada para ele pelo finado Espírito do Comércio _Mack Silver_, junto com alguns de seus escritos para as _Sementes Douradas_: ele tinha lá seus desentendimentos com Silver, para ser bem honesto, mas ele decidiu que honraria seu nome e os dos Espíritos caídos colocando tais recursos em bom uso.

Ele então criou uma nova identidade falsa, o que foi razoavelmente fácil: ele forjou a identidade de _James Marconi_, de Wamega, Kansas. Ele tinha acabado de entrar no MIT em uma graduação de filosofia, e entrou nos Laboratórios de Inteligência Artificial (AI Labs) ao fazer alguns cursos de programação de computadores e acabou ficando. Ele também ficou conhecido como o nome por trás de uma fanzine apócrifa sobre _"As Lendas do Clube do Século e de Seus Espíritos"_, que o trouxe perigosamente para o radar. Ele apenas foi ignorada pois, por não colocar nenhuma fonte, acabou ficando com ar de Teoria da Conspiração.

E, debaixo de todas essas camada, ele criou a NOVA.

Então ele comprou seus primeiros computadores pessoais, um Commodore PET e um Tandy Radio Shack TRS-80 Model I, além de um potente (para o período) modem de 300 bauds com o qual se conectava à NFSnet (usando uma conexão do AI Labs) e a algumas BBSes, procurando pessoas, ouvindo histórias e separando o joio do trigo, e descobrindo pessoas que passaram por problemas, realmente bizarros: um cara que descobriu que sua futura noiva era uma invasora insetóide; uma garota fugindo de um cyber-caçador viajante do tempo do espaço; aquela jornalista inglesa que viajou (de acordo com suas palavras) pelo tempo e espaço com um cara com dois corações que usava um esquisito e gigantesco cachecol e que mudava de forma quando próximo da morte!

E foi assim que ele encontrou, ou melhor, _foi localizado_ por _Alpha_:

_Alpha_ sempre foi uma hacker inacreditável, parte dos melhores dos melhores dos melhores. Além disso, como investigadora, ela ainda lembrava de todas as vezes que Clube do Século salvou o mundo de inimigos como Gorilla Khan, o Lebre do Campo, Jared Brain, Lemont Fitzlefebvre, e o Doutor Matusalém. Ela então foi espevitada o suficiente que ela conseguiu seguir os rastros que James, ou melhor, _Nicola_, espalhou pelo mundo e pelo tempo. Ela garantiu que isso não foi nada fácil, e que mesmo considerando que a zine apócrifa deu a maior bandeira, ele não deveria se preocupar. Ela conhecia tudo sobre a sua vida, inclusive onde e como ele "morreu", passando de uma identidade para a outra, rastreando todas as informações até o momento em que Don Cagliostro o encontrou e o trouxe ao _McNash & Sullivan Circus and Shows_, onde ele se tornou o palhacinho _Nicolino_ após parar um leão furioso apenas _falando com ele_.

_Alpha_ tem os Espíritos em alta conta: sua mãe foi salva por eles nos eventos conhecidos (na época, agora perdidos no tempo como lendas urbanas) como _Dinocalipse_, e ela estudou na _Escola de Engenharia Cross_, onde ela descobriu (e guardou para si tal segredo) que a Escola era comandada por ninguém menos que o Espírito da Engenhosidade Sally Cross, ou melhor, Sally Slick!

_Alpha_ e James então trocaram alguns emails e deram início ao projeto NOVA, uma organização que visava lidar com o bizarro, mas com pessoas comuns, para ser uma barreira para conter invasões de realidades hostis, como era parte da missão do Clube do Século até seu banimento.

Então ambos começaram a rodar o mundo, até mesmo do outro lado da Cortina de Ferro, procurando pessoas que encaixassem-se em seus planos. Com o tempo e os sucessos, a NOVA ganhou fama: o suficiente para serem ascendidos a uma nova arena, mas ainda não o suficiente para os colocar no radar.

Entretanto, cada vez mais olhos começam a observar a NOVA, e alguns deles são olhos que _Alpha_ e James, ou melhor, _Nicola_ gostariam que não os observasse.

### Rumores

+ Na realidade _Alpha_ é uma Sombra do Século: ninguém sabe se ela está usando Nicola para uma agenda oculta e horrenda, ou está em busca de redenção, mas parece que ela não é alguém que não seja hábil em sujar as mãos;
+ James Marconi NÃO É Nicola Castrogiovanni: o verdadeiro Nicola caiu durante a caçada aos Espíritos do Século, e James é apenas alguém que conhece sua história o bastante para assumir seu manto e identidades;
+ James/Nicola está em uma missão secreta para buscar trazer o Clube do Século de volta à sua antiga gloria: ele está procurando artefatos que foram perdidos durante o banimento do Clube, e qualquer informação sobre Espíritos escondidos. Alguns suspeitam que ele está os procurando para uma última batalha que teria sido profetizada por Mary Alethea
+ Alguns suspeitam que NOVA na realidade visa selecionar os próximos Espíritos do Século
+ Alguns agentes da NOVA, em especial os mais experientes, desenvolveram capacidades similares às dos Espíritos, ou ainda maiores: o contato dos mesmos com o VHS e com os Fragmentos do Matusalém os mudaram de maneiras bizarras;
+ Embora Nicola tenha passado muito tempo nos AI Labs do MIT, atualmente ele está escondido em um circo próximo a Omaha, Nebraska. Outros também dizem que ele tem uma propriedade pequena em Baraboo, Wisconsin, ou em outras cidades onde antigos circos mantinham suas bases de Inverno. Alguns dizem que se verdadeiro _sanctum_ é na antiga base de Inverno do _McNash and Sullivan Circus and Shows_ em Newark, NJ;
+ _Alpha_ na realidade não é uma pessoa, mas sim uma Inteligência Artificial Turing-Realista, programada por Nicola ou que foi "baixada" de fora de nossa realidade: a principal razão de ela procurar Nicola é que ela sabe ele é uma peça chave para um futuro ao qual ela quer direcionar a realidade;
+ Nicola na realidade está em uma _blacklist_: ele cedeu à escuridão e tornou-se uma Sombra, e NOVA agora é apenas uma peça para alcançar seus objetivos. Algo no passado o fez tornar-se cínico e amargo e isso o fez debandar muito antes do Banimento do Clube do Século;

### NOVA como uma Semente Dourada

+ ___Agenda:___ tornar-se uma das barreiras protegendo a humanidade contra perigos e invasores de fora de nossa realidade, e lidar com ameaças bizarras criadas por agentes inimigos
+ ___Recursos:___ não muito grandes e muito espalhados, mas isso pode ser uma vantagem já que certamente, com tempo o bastante, você encontrará um agente da NOVA próximo
+ ___Patrão:___ Ser parte da rede NOVA pode ser uma ótima forma de manter-se na crista da onda, já que na maior parte do tempo a NOVA lida com o bizarro e estranho, e é de certa forma seguro, já que eles preferem lidar com agentes locais que possam ajudar e saibam lidar bem com informação confidencial. E sempre tem O Favor que você pode pedir e a NOVA fará o possível para conceder
+ ___Aliado:___ Como é totalmente espalhada, a NOVA pode não ser a melhor aliada, já que achar um agente da NOVA pode ser complicado, mesmo por aqueles partes da rede, exceto por James e Alpha. Entretanto, uma vez que você tenha um aliado dentro, você acaba conseguindo acesso a grandes quantidades de informação, recursos e equipamentos, desde que tenha paciência
+ ___Contato:___ Como no Aliado, a NOVA não é exatamente o melhor contato se você tiver com pressa, mas caso contrário é uma ótima fonte de informação
+ ___Rival:___ A NOVA não é invejosa em sua missão ao ponto de a tomar como exclusiva deles. Entretanto, ela pode ser bem sensível quanto aqueles que atrapalhem suas missões. Normalmente eles vão colocar as coisas no armário até o problema ser resolvido... E vão cair matando em cima de você caso achem que você está bagunçando tudo!
+ ___Inimigo:___ se você pensa que a NOVA pode ser tratada como um bando de bobos por causa da sua estrutura flexível, pense novamente: os agentes da NOVA são treinados para notar coisas bizarras que demandem sua ação. De fato, se a NOVA te notar, e VOCÊ estiver fazendo coisas ruins, eles vão para cima de você de forma tão pesada que você vai desejar que eles não tivessem o notado!

### **Face: Alpha**

Normalmente as pessoas não conseguem encontrar Alpha se ela não desejar o ser, e mesmo assim ela se passa por uma garota *punk* “completamente normal” com único diferencial o cabelo em um verde elétrico: apenas nas BBSes e na NSFnet (predecessora da Internet) é que as pessoas conhecem sua verdadeira fama

#### **Aspectos**

|       ***Tipo:*** | ***Aspecto:***                                   |
|------------------:|:-------------------------------------------------|
|     ***Aspecto*** | Grll Power Uber-Hacker                           |
| ***Dificuldade*** | Sabe mais do que deveria sobre O Clube Do Século |
|                   | Mais amigos (e inimigos) do que ela imagina ter  |

#### **Papéis**

|  ***Papel:*** | ***Nível***     |
|--------------:|:----------------|
| **Diletante** | *Razoável (+2)* |
|  **Detetive** | *Bom (+3)*      |
| **Sabotador** | *Bom (+3)*      |
|    **Hacker** | *Ótimo (+4)*    |

#### **Façanha**

- Formado na Faculdade
- Computador maneiro

### **Face: "James Marconi" (Nicola Castrogiovanni)**

Ninguém poderia associar o dono de uma loja de computadores e ex-funcionário do AI Labs do MIT gordinho e meio careca James Marconi com *Nicolino,* o Anjinho dos Circos e Vaudeville dos anos 1910s, e conhecido pelo nome verdadeiro de Nicola Castrogiovanni. E ainda menos são aqueles que o ligam à estranha organização NOVA que investiga casos estranhos.

#### **Aspectos**

|       ***Tipo:*** | ***Aspecto:***                                     |
|------------------:|:---------------------------------------------------|
|    ***Espírito*** | Espírito do Otimismo (Gonzo)                       |
| ***Dificuldade*** | *"No final tudo vai dar certo!"* - Inocente demais |
|                   | NOVA dará origem a um novo Clube do Século         |
|                   | Apenas faça o que precisa, não fique lamuriando    |
|                   | Talvez chegou a hora de Voltar à Ativa             |

#### **Papéis**

|          ***Papel:*** | ***Nível***        |
|----------------------:|:-------------------|
|         **Diletante** | *Bom (+3)*         |
|          **Detetive** | *Ótimo (+4)*       |
|            **Hacker** | *Ótimo (+4)*       |
| **Pollyanna (Gonzo)** | *Excepcional (+5)* |

#### **Façanha**

- **Senhor Monteiro:** Sua atual identidade James Marconi;
  - *Cientista da Computação em uma pós-graduação*;
  - *Seu histórico não sobrevive a uma consulta a documentos reais*
- **Senhor Monteiro:** Rasmus Van Der Merwe
  - *Cientista Social de Bloemfontein, África do Sul*
  - *"Sobrinho" de um dos fundadores do Congresso Nacional Africano*

#### **Façanha  Centuriã (Gonzo) – O jogo do Contente**

- Nível 1: Consigo Fazer Isso (Graduado na Faculdade)
- Nível 2: *Por Favorzinho!* (Charme Acachapante + Livro Aberto)
- Nível 3: Vamos conseguir fazer isso, Juntos! (*Plano B* + *Confie em Mim* + *Confie em Você mesmo*)

<!--  LocalWords:  title Coelhão subtitle An Easter Adventure br page
 -->
<!--  LocalWords:  language categories tags Shadow of the Century and
 -->
<!--  LocalWords:  excerpt separator Issue Subplot Wilhelmina NPCs NJ
 -->
<!--  LocalWords:  Agents segundo-em-comando não-violência Mary Tandy
 -->
<!--  LocalWords:  Alethea Frederick Cagliostro Nelson Rivonia Wamega
 -->
<!--  LocalWords:  Espírito-em-Segredo Catargo Commodore Shack bauds
 -->
<!--  LocalWords:  NFSnet insetóide cyber-caçador Gorilla Khan Brain
 -->
<!--  LocalWords:  McNash Sullivan Circus Dinocalpse Dinocalipse
 -->
<!--  LocalWords:  Sally Slick emails Nebraska Baraboo Wisconsin
 -->
<!--  LocalWords:  sanctum Newark Turing-Realista blacklist
 -->
