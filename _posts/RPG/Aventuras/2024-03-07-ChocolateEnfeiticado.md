---
title:  "Chocolate Enfeitiçado"
subtitle: "A _Little Wizards_ Tale adapted as a _Do - Fate of the Flying Temple_ Letter"
language: br
layout: aventuras
categories:
  - Aventuras
tags:
  - Do - Fate of the Flying Temple
---

__Stamps:__ Knot, Pen, Lotus

> _"Caros Monges do Templo Voador:_
>
> Eu sou Maxi, e eu sou um Maguinho do Mundo-Moeda. O Mundo-Moeda é um lugar bem legal, onde a magia existe, portanto brincamos com coisas mágicas como fadas e pixies no lado de Cara e com Lobisomens e Vampiros no Lado de Coroa. Como vocês podem ver, nosso mundo é parecido com uma moeda, mas podemos ir de um dos lados para o outro de maneira razoavelmente simples. Eu sou do Arquipélago dos Calafrios, em Cora, onde você sempre pode sentir calafrios das histórias de horror sobre lobisomens que, mesmo amaldiçoados, provocam bons sustos ao uivar de maneira artística
>
> Como um Maguinho, eu viajo pelo Mundo-Moeda entendendo melhor a magia e as pessoas. Minha melhor amiga é Sora, uma Feiticeirinha (existem algumas diferenças entre Maguinhos e Feiticeirinhos, mas em geral vivemos em paz) que parece uma palhaça o tempo todo, pois ela nasceu desse jeito. Ela nasceu no Arquipélago dos Sorrisos no lado de Cara. Ela é muito energética, ainda que um tanto destrambelhada (mais ou menos como eu). Eu gosto muito de Sora já que, por eu ter nascido no Recanto dos Gambás, as pessoas acham que eu fedo como um gambá por causa da minha cauda, e eu não fedo como um gambá, deixo bem claro, e ela é uma das poucas pessoas que não acham que eu fedo.
>
> Meus outros amigos são o meu familiar e o de Sora. No passado apenas gatos podiam ser familiares de Magos e Feiticeiros, mas essa regra mudou, e agora o animal apenas tem que ser preto (não me perguntem o porquê. *"É a Tradição!"*, meu pai me disse). Meu Familiar é Puffers, um gambá totalmente preto que tem olhos muito sábios e que me ajuda bastante (mesmo que de vez em quando cheire muito mal), e o de Sora é Horace, um coelhinho preto, bastante tímido e assustadiço, que passa a maior parte do tempo debaixo do chapéu de Sora. Sim, Magos e Feiticeiros usam vassouras, varinhas e chapéus: o de Sora é tão colorido e espalhafatoso quanto possível, e o meu é feito do couro de um gambá, incluindo a cauda (que não fede!)
> 
> Mas basta de falar de mim ou de Sora
>
> Meu pai me disse que se algum dia eu encontrasse um problema que mesmo com nossa Magia não conseguíssemos resolver, deveríamos mandar uma carta ao Templo Voador que receberíamos ajuda.
>
> E isso é exatamente o que está acontecendo:
>
> Sora e eu recentemente viemos para Sorrisos visitar a família dela no circo, e vimos que as pessoas estavam agindo de maneira estranha!
> 
> O padeiro abandonou a padaria e estava brincando no chafariz, nadando nele exatamente como estava vestido, com avental, chapéu e tudo!
>
> Uma mãe deixou seu bebê para trás e sentou em seu carrinho e passou a passear por aí pela cidade!
>
> E, o mais curiosissímo (como Sora falou), alguns adolescentes estão fazendo birra, como bebês chorões e birrentos! Até nós somos mais maduros que eles, e olha que temos apenas 7 anos!
> 
> A gente conversou com alguns Doutores e eles disseram que não sabem o que está acontecendo. Nossa única pista é que recentemente uma nova Doceria, chamada _Delícias do Ellys_, abriu e começou a vender chocolates. Não sabemos dizer se tem algo a ver com o que está acontecendo, mas essa é a única coisa diferente que Sora viu de diferente desde que a última vez que ela tinha vindo visitar sua família. Tudo está tão estranho que nem nossos familiares, Puffers e Horace podem nos ajudar...
>
> Por favor, por favor, por favorzinho (Sora pediu para eu escrever assim), venham até o Mundo-Moeda e nos ajude. Prometemos pagar vocês com um passeio por muitos lugares legais aqui, como todas as coisas divertidas no Arquipélago dos Sorrisos e até a minha casa em Calafrios, onde você poderão ver como é divertido sentido um bom susto ouvindo a Orquestra Zumbi tocando as Flautas Arrepiantes.
>
> Obrigado, Monges. Estaremos os esperado aqui em Sorrisos.
>
> _Maxi, Maguinho & Sora, Feiticeirinha_




<!-- Local Variables: -->
<!-- simplenote-key: "agtzaW1wbGUtbm90ZXIqCxIETm90ZSIgODEyNTE0NjgzM2YyMTFlN2JiMjk1Nzc1NWE0ZGFkYzEM" -->
<!-- End: -->
