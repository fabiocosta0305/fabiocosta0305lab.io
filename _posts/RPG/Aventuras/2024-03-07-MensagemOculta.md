---
title:  "A Mensagem Oculta"
#subtitle: "A Christmas letter for _Do - Fate of the Flying Temple_"
language: br
layout: aventuras
categories:
  - Aventuras
tags:
  - Do - Fate of the Flying Temple
mastodon_base: 112197600913226753
---

+ ___Stamps:___ Flag, Pen, Sword

> + _**Para:** admin@flyingtemple.org.mw_
> + _**De:** admin@crystalis.gov.mw_
> + _**Assunto:** Solicitamos Ajuda_
>
> _"Caro Monges e Peregrinos do Templo Voador._
>
> _Esperamos que vocês consigam acessar essa criptomensagem em seu cristal de mensagens. A Criptografei com uma cifra de Nome Verdadeiro usando o Nome Verdadeiro do próprio Templo, de modo que apenas os Monges e Peregrinos do Templo Voador possam ler essa mensagem-eco armazenada aqui. Qualquer outra pessoa veria apenas um bando de baboseiras sem sentido._
>
> _Talvez vocês que estejam vendo essa mensagem não nos conheça, então é melhor nos apresentarmos antes de seguirmos adiante com o nosso pedido de ajuda._
>
> _Nosso mundo, Crystalis, é um pequeno mundo nas fronteiras entre os Céus do Aço, do Pó e do Jade e Especiarias, e é muito escasso em quase todos os recursos naturais: temos poucas terras ferteis onde plantar e criar gado, nossas fontes de água são poucas e afastadas umas das outras, e não temos muitas rochas ou madeira para construções._
>
> _Portanto, em condições normais, seríamos apenas um outro mundo perdido no nada se não fosse nosso principal produto de exportação._
>
> _Os primeiros colonos descobriram que em algumas das montanhas cristalinas daqui podeiam ser encontrados cristais especiais que permitiam nos comunicar remotamente, ao usar eles para mandar mensagem, ou_ ecos _como chamamos. Como tempo e diligência, desenvolvemos a arte de lavar tais cristais e usar seus fragmentos para comunicarmos-nos com outros usuários de fragmentos do mesmo cristal, formando o que chamamos de uma_ shardnet, _uma rede de comunicação entre todos os que portam tais fragmentos do mesmo cristal. Começamos então a exportar tais fragmentos por todos os Muitos Mundos, já que os fragmentos permitem a comunicação remota imediata entre todos que possuam fragmentos do mesmo cristal (ou seja, da mesma_ shardnet) _+independentemente da distância._
>
> _Além disso, com o tempo descobrimos e desenlvemos técnicas para embaralhar os ecos, garantindo comunicação segura e garantida entre dois usuários dos cristais, sem que houvesse interceptação dessa informação, o que chamamos de_ criptomancia._ Existe toda uma gama de técnicas de criptomancia, algumqas muito poderosas, como a usada nesse cristal, visando que apenas aqueles a quem as mensagens sejam destinadas as recebam._
>
> _Porém, esquecemos do poder da ambição humana, ao transformarmos nosso mundo em uma biblioteca, o maior centro de conhecimento dos Muito Mundos à excessão do Templo Voador. Para constar, espero que a_ shardnet _do Templo esteja em bom funcionamento e que seu_ Shardscape, _seu grande cristal que serve como centro de conhecimento cristalizado, também esteja funcional._
>
> _Perdão... Me dispersei._
>
> _Recentemente o Mundo de Showa e o Império de Koivinar começaram a aumentar as hostilidades entre ambos, e ambos demandam a nós acesso à_ shardnet _um do outro. Infelizmente, apenas os fragmentos de um mesmo cristal conseguem acessar os ecos em uma_ shardnet, _Existem algumas técnicas para se rotear as mensagens entre_ shardnets, _ao usar-se em ambas as mãos pragmentos dos cristais de cada_ shardnet. _Entretanto mesmo isso não permite uma forma simples de resolver-se os problemas da criptmancia._
>
> _E após algum tempo dessas demandas, alguns de nossos embaixadores tanto em Showa quanto em Koivinar desapareceram, parando de enviar mensagens em nossas_ shardnets. _Tememos que os mundos guerreiros sequestraram nossos embaixadores como forma de pressionar-nos a oferecer os acessos que desejam, ou ainda pior, tomar para si os cristais de nosso mundo. Como mundos guereiros do Céu do Aço, eles procuram poder para si e apenas para si, e ao monopolizar os fragmentos, eles poderiam facilmente controlar as comunicações envolvendo todos os Muitos Mundos, mesmo o Templo Voador estando sob suas garras._
>
> _Como disse anteriormente, Crystalis é apenas um centro de conhecimento pacífico, tendo quase nenhum tipo de força militar. Nossa pequena população quase que exclusivamente está focada em produzir fragmentos de cristal, nossas safras e gado mal-e-mal nos sustentando, todo o resto sendo obtido por meio de trocas comerciais com outros mundos. Não temos recursos para contratar mercenáriose, e não queremos aceitar trocar nossos centros de conhecimento nas_ Shardscape _e acessos à informação pela "proteção" desses reinos._
>
> _Portanto, nosso melhor e última esperança se voltou ao Templo Voador, que conhecemos de tempos passados. Acreditamos que o Templo será capaz de nos ajudar a encontrar meios de evitarmos a agressão de Showa e Koivinar, visando nos conquistar, ou ainda pior, simplesmente espoliar nossos cristais e nos deixar para morrer aqui._
>
> _Esperamos que vocês nos auxiliem, Monge do Templo Voador._
>
> _**Sarin Adamant**, Cryptoadministrador, Centro das_ Shardnets _de Crystalis_
>
> _XXXX-03-09 1400 +0300"_

### NPCs:

#### Sarin Adamant, Cryptoadministrador

+ ___Minha vida é a de um Cryptoadministrador; Maestria no uso das_ Shardnets__
   + _**Bom (+2) em:**_ Shardnets, Cryptomancia, Análise de Informações;
   + _**Ruim (-2) em:**_ Agir com Violência;

#### Volton Lyander, Representante de Koivinar 

+ ___Showa são escória!; As_ Shardnets _são e serão nossas e APENAS nossas___
   + _**Bom (+2) em:**_ Pavonear-se quanto seus poderes;
   + _**Ruim (-2) em:**_ Coragem;
   
#### Saburo Yashiwa, Representante de Showa

+ ___Honrado mas implacável;_ "Eu não converso com escória de Koivinar!"; _Preocupado de certo modo com possíveis consequências das ações de Showa___
   + _**Bom (+2) em:**_ Ser respeitoso, mas implacável; Hospitalidade e Cortesia;
   + _**Ruim (-2) em:**_ Ir contra as ordens de Showa;

#### Karin Yashiwa, Embaixatriz de Crystalis em Showa

+ ___Não sou mais parte de Showa; Embaixatriz de Crystalis em Showa; Morte antes da Desonra___
   + _**Bom (+2) em:**_ Usar Shardnets;
   + _**Ruim (-2) em:**_ Ser desrespeitoso;

#### Florian Tarson, Embaixador de Crystalis em Koivinar

+ ___Crystalis é minha casa; Embaixador de Crystalis em Showa; Perito em Quebra Criptomântica___
   + _**Bom (+2) em:**_ Engenharia Social, Cryptomancia, Shardnets;
   + _**Ruim (-2) em:**_ Ser comum;

#### Os Devoradores de Riscos, Seres Manipuladores

+ ___O futuro é nosso, assim como a responsabilidade sobre ele; Não temos nenhuma ligação, trabalhamos nas sombras___
   + _**Bom (+2) em:**_ Cryptomancia, Shardnets, Serem esquivos;
   + _**Ruim (-2) em:**_ Lidarem com a Derrota;

#### Lars Talsorian, cryptomante traiçoeiro

+ ___Informação é poder!; Manipula tudo e todos para alcançar seus objetivos; cryptomante autodidata___
   + _**Bom (+2) em:**_ Criptomancia, Ser Enganador, Coletar Informação;
   + _**Ruim (-2) em:**_ Aliados e Contatos
   
