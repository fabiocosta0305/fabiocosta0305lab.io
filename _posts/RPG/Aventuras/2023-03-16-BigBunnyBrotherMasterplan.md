---
title: "The Big Bunny Brother's Masterplan"
#subtitle: "An Young Centurions Easter Adventure"
language: en
layout: page
categories:
  - Adventures
tags:
  - Shadow of the Century
excerpt_separator: <!-- excerpt -->
---

**A *Shadow of the Century presents* movie**

> + ***Big Issue:*** Someone is rising an army of Rabbit-Men
> + ***Subplot Issue:*** Common people solving bizarre problems

This is a movie that works nicely as opening, closing or special on a _Shadow of the Century_ series, or as a One-Shot Movie for cons. It doesn't need or use (at start, at least) any of The Man from the setting, but you can show that they have some interesting on those events, even more if you use also the _Shadow of Fantasy_, _Filippo Geppeto_ on this (in his ***Mrs. Underhill*** fake identity _Christian Matteo_)

Although this movie points to the events of my _Young Centurions_ adventure [***The Mystical Egg and the March Hare***](http://fabiocosta0305.gitlab.io/adventures/TheMysticalEggAndTheMarchHare/) (as the main villain here is the March Hare, Marshall Hare), there's no need to play it before, it would not make any difference.

This adventure is for 2-6 players, that will be part of a _Golden Seed_ organization called NOVA (***Network of Variable Operatives***), but it can be easily for other demands on number of players and organizations.

<!-- excerpt -->

## Introduction - the Big Bunny Brother's Menace

> *“Big Bunny Brother is here, yay, hooray!*
>
> *To help and cheer up for you, we believe, yay!*
>
> *Through sea, air, mount and earth he will came*
>
> *To help us against all kind of mean people, hooray!*
>
> *We will have no fear or sadness, hooray!*
>
> *Because we know Big Bunny Brother will come, yay, hooray!"*

This is the old _Big Bunny Brother song_, the ***Big Bunny Brother Adventures*** anthem, a song that is still in the hearts of many of whose were children in the 1960s and 1970s. In this show, an Actor on the costume of this character, just called ***The Big Bunny Brother*** (_Bunny Brother_ for short) got in all kinds of trouble at the hands of mean animals while helping and protecting his friends. Aside his name, there still some other names that his fans still remember, like the foes Fred Fox and Bruce Bear, and the friendly Bettina Bowwow, Lena Lamb and Suzan the Lil' Shepard.

Sadly, the show was abruptly cancelled in the end of 1970s, thanks some weirdly connected and never confirmed children disappearances, where people started to suspect Big Bunny Brother could be exerting a bad influence.

However, this didn't was in the way of _Peek-a-boo Entertainment_'s, a new but rising children shows' company, plan on bringing _Big Bunny Brother Adventures_ on a new, flashy, style, back to the limelight. The company, owned by a weird guy called Christian Matteo, bought the character's rights and brought it back as an animation that was then licensed and syndicated on other children shows', and that brought a resurgence for the _Bunny Warrens_, the _Bunny Brother_'s fan-clubs.

However, recently some weird news linking the _Big Bunny Brother_ to some abductions of kids and adults fans of the Bunny Brother, raised a kind of _Satanic Panic_ all around US and on other countries where the show was syndicated, like Japan, West Germany, Brazil and South Africa.

No one is safe from those abduction...

... and looks like now we should have fear and sadness of Big Bunny Brother coming for us!

## Act 1 - Alpha

The player's are called for Alpha, their contact on NOVA and its manager, for a really weird mission, but totally under NOVA's banner, _“Ordinarius populus adversus minas aliena”_, ***common people solving bizarre problems***.

> James called me to put you on a situation that is demanding NOVA action: on the Amish region in Indiana, people started to being kidnapped, specially kids. This would be only common police stuff if wasn't some issues:
>
> + First, this started a week after a _Bunny Brother_ live show, from ***The Big Bunny Brother Adventure*** children show
> + Those abductions are happening all around the region
> + All the abducted were part of the local _Bunny Warrens_
> + There was also some weird sightings all around the region, of rabbit-shaped creatures, but big as human adults, and even bigger!
> 
> James has a hunch that there's more here than what they gave for him when came to him... So, he asked me to put you on the game. And also he said me to ask you to look upon the  ***1911 Rabbit's Easter*** for clues, for... reasons. I tried to look for clues on this on all BBSes I could, but all that I've found was weird conspiracy theories.

Here is some questions the characters can ask Alpha:

+ ***What the heck was this 1911 Rabbit's Easter?***

If they want to look if they know this previously, this would be a _Superb (+5)_ ***Knowledge*** check, as this is an urban legend lost in the time. If they ask Alpha, she'll pass them all the intel she have on this:

> _"Well, it's said that there was an very weird event on 1911 Easter: someone started to sell a kind of chocolate egg so good that people were clashing for them, almost literally. And worst... According the tabloids at the time, people were being turned into bizarre half-men half-rabbit creatures by eating those, under the trance of a certain half-rabbit creature called The March Hare, that wanted to turn New York into his own Wonderland. It's said that the ones who stopped him was a group of teenagers linked with an organization called The Century Club, that fell under shame in the future. For me... This is really a big pile of balderdash, even putting The Century Club into account."_

The Century Club's existence fell in the mist of time on this now so cynical time, and even between those who knew the legends about the Spirits of the Century, the versions are so different as contradictory: some say they were the Humanity Last Bastion against Things Humankind Shouldn't Knew About, and also Guardians of Dangerous and Nefarious Knowledge! Others, however, called them weak or, even worse, part of the Red Danger by not passing to government some of those secrets for help on the War Effort at WWII, as pointed by Senator McCarthy! There's even some that believe that this was only a Collective Surge where everyone imagine those heroes from past as a fugue from reality.

This is really what the average guy knows, but there's really bigger secrets, although they doesn't will help on the moment, so let us go forward

+ ***And who is this Big Bunny Brother?***

> _"He's the main character from this old children show, **The Big Bunny Brother Adventures**, that was shown till 1978 when it was cut out on weird circumstances, till he was relaunched as a cartoon and had a resurgence with new fans joined the old ones, enough so there was a demand for Bunny Brother shows all around the country."_

If they investigate on the old show, they'll discover that it was cancelled after some kids had gone into a forest and lost themselves for some days. No one know what happened with the kids (aside they got back alive) and their families never gave interviews on the topic and asked for privacy.

They will also know that there was other actors all around US that played Big Bunny Brother on TV: as like Bozo, Fred Rogers' Neighborhood, Captain Kangaroo and Howdy Doody, although they were national shows, there was no satellite broadcast technology at time to allow national TV broadcasting, so many times the shows were played by local actors, and some pre-recorded inserts were sent to be played with the live show.

Curiously, all Big Bunny Brother actors already died on natural causes, except one, a certain _Eaton Rabbit_, that never gave interviews out of Big Bunny Brother suit, and even on this he gave very few ones: since the cancellation, he gave two interviews (always on Bunny Brother's suit, or so they think), and then he spirited away himself: he vanished and no one found him, a mystery still in the head of some of (now parents) fans. 

+ ***And those Bunny Warrens?***

> _"They were the Bunny Brother's fan-clubs... You know, like the Hu Dunnit Mystery Clubs, and Mickey Mouse's Mouseketeers... In the original show, Big Bunny Brother tried to encourage kids on helping society, either by joining groups like Scouts, or by other ways of local social action. Many of them, then, joined themselves and created the local Bunny Warrens to help people in their cities. There was even some gifts the show sent for the little bunnies, Bunny Brother's fans, that prove that were_ nice little bunnies and improved everyone's life, _things like books, pins and even some rabbit ears."_

The characters will need no checks on the public information on _Hu Dunnit Mystery Clubs_ or [_Mickey Mouse's Mouseketeers_ (M-I-C-K-E-Y M-O-U-S-E)](https://en.wikipedia.org/wiki/The_Mickey_Mouse_Club). However, any information on _Hu Dunnit Mystery Clubs_ as Golden Seeds (_Shadow of the Century, p 156-157_) they'll need some _Knowledge_ rolls, difficult ones.

If you notice something weird on Alpha know all this, she'll confess: she was ***a little bunny part of her home city's* Bunny Warren**, and she still have the Bunny Ears she got when she and their classmates from the _Bunny Warren_ collected toys on a Christmas action for poor kids. She's the only thing she'll say about his time as part of a _Bunny Warren_ as _"it was so long before, and nowadays Bunny Brother looks a little too much_ corny, too much naively idealist and clean and soft, like a kid's dream without a place on our world"_

+ ***And what we know about this new company with Bunny Brother's rights?***

> _"Well... His owner is a certain Christian Matteo: born 1942 at Wichita, Kansas, graduated on NYU at 1962, Arts Major and business minor, worked for some time as producer at_ Broadway, _and even with all the shows closing just some time after he join them, he got very fast to the peaks on Broadway as producer, till he got out and bought this old cheap children show's producer, **Peek-a-boo Entertainment** and made it very profitable and productive very fast. There's some weird gossip about mental problems involving the Peek-a-boo's workers, either actors and producers, but only gossip as far I know, so they still produces shows, and they hitted the jackpot by buying the inventory from old time's producer of **The Big Bunny Brother Adventures**. And they got this one not exactly for peanuts, to be hones... A little too much dough was put on this. James think there's something fishy on all this stuff, but he can't confirm anything, so he took this for himself"_

If the PCs wants to question the founder of NOVA, James Marconi's, inaction, Alpha will make clear that _"if I don't know anything about this, you also would not. If there's something James knows is when he can pass intel forward or not."_

But if they investigate Christian Matteo, they'll discover he's as controversial as successful: there's some gossip in the Broadway, about his almost childish tantrums, weird relationships, and knack to deal with the _glamour_ and tame even the wildest stars under his eyes, no matter what.

If they got really into this (it will take some really difficult checks that will demands, lots of time), they'll discover something really weird: ___There's no Christian Matteo under the cold stone papers!___ no matter what computer systems points off!

There's a reason for this, but it's up the Narrator to allow the players to discover it and go down the Rabbit Hole (pun indeed) on this: in fact, he's _Filippo Geppeto_, _Shadow of Fantasy_, someone linked with events at 30s and 40s where someone tried to manipulate reality with actions involving fairy-tales and artistic activities.

How much this will link him with Big Bunny Brother will be up the Narrator, as (at least here) Filippo Geppeto is not directly linked.

+ ***What is known about the abductions?***

> _"The last ones were four kids into Indiana_ Amish _region. If we put into account the most advanced tech_ Amish _uses is 18th century Horse-pulled carts, it's surprising there was audience enough for a children character show there, but this also explain why the abductions being so geographically spread. The last one, two days later, was from Amanda Williams, 7, at Angola, Indiana. Typical local cupcake girl: girl scout, top student, majorette and ballerina, the only out of stereotype thing is being part of local_ Bunny Warren"

If they ask Alpha about gifts that maybe she got from Big Bunny Brother or something like this, she has no information at all on this stuff: it's the 80s, for Pete's Sake and even Alpha being an _uber-hacker_ for the time, computers were still sparsely used on public sector, even more network connected. Even Internet still was a mishmash of communication protocols and systems linked themselves, like ARPANet (private, low spreading), MILNet (military, heir of the original ARPANet) and NSFNet (public,  used mainly by universities and research centers), aside a mishmash of systems that exchange information asynchronously using telephones and old protocols like Usenet, FTP, UUCP (Unix-to-Unix Copy) and the recently founded FidoNET, with others.

+ ***Any help we can count upon?***

> _"James got some money to provide you the way to go there and stay for a while. To be hones, go straight to Angola, Indiana: maybe there will be some contacts even inside the Amish, if needed."_

In fact, there's no link between the Amish and the history: if you know enough about them and want to have some fun, put some of them to help the characters. Just remember, this is a _Shadow of the Century presents_ Movie: _go big or go home_, there's no points for second best!

Before sent the characters off, Alpha will inform she'll be available under her normal ways. What this means, it's up the Narrator and the Players: normally it would involve public telephones and exclusive BBSes.

Although being almost toys, at the time there was already some "transportable" computers like  [Osbourne 1](https://pt.wikipedia.org/wiki/Osborne_1) and [IBM Portable](https://en.wikipedia.org/wiki/IBM_Portable), and some real, honest-to-gosh, miniaturized computers, almost toys but usable enough for those with the right skills, like the Epsons [HX-20](https://en.wikipedia.org/wiki/Epson_HX-20) and [P-8 Geneva](https://en.wikipedia.org/wiki/Epson_PX-8_Geneva) and the [TRS-80 Model 100](https://en.wikipedia.org/wiki/TRS-80_Model_100)... If you are on the 1986, there's even the _avant-garde_  [IBM PC Convertible](https://en.wikipedia.org/wiki/IBM_PC_Convertible). Made it clear, though, that's not like you just push a cellphone from pocket and ask your baby girl.

Anyway, now it's up the player's on how they'll solve the ___Big Bunny Brother's menace!___

## Act 2 - Amanda

No matter what, if the characters follows Alpha's tip, they'll take two days on the road, hearing cassette or Super 8 tapes or enjoying the greatest _hits_ from the best oldies like Dolly Parton and Elvis Presley... If they're lucky, the dial will fall down somewhere a wacky DJ will be playing the best of crazy European sounds like Kraftwerk, Kate Bush, Mike Oldfield, Spandau Ballet or Neu! 99 Red Balloons probably will be the best thing they'll get.

Anyway, if they try to use radio as a way to cash some information on the kidnappings, you can always put ___televangelists using the radio to spread Satanic Panic against the Big Bunny Brother___, all the balderdash package included, even the _"satanic messages hidden on the so-called Big Bunny Brother music"_. You know, [all the old _play the music backwards for satanic message_](https://en.wikipedia.org/wiki/Backmasking) from the past: they did this with Beatles, Elvis, Hendrix... Even Eagles' _Hotel California_ had this "honor". Use this to reinforce rumors about rabbit-men in the woods, satanic messages, children's corruption, all the package, the best of the worst conspiration theories: time to give a look on that old **GURPS Illuminati** copy on your bookshelf and show them that FNORD is happening because FNORD did FNORD while FNORD FNORD FNORD! FNORD!

When they arrive at Angola, Indiana, they'll see the city is one of those small cities in midwest that is are full package Americana, the ones that would inspire Mark Twain, Ernest Hemingway and Jack Kerouac all the same time. The Bed and Breakfast is small, simple, but clean, comfortable and, yep, there's an _International Gideons_ Bible at the nightstand's drawer. The restaurant is small, the most modern music you can get on the jukebox is Doors' _Light my fire_, but the burger is so meaty and tasty that you almost can hear the moo from the cattle. All the houses have a fluttering US flag, but the gardens are all pretty and well-cared and people are really talkative, even offering some refreshment in the meanwhile.

When asked about, below are some information they can provide:

+ ***What you know about that kidnapped girl?***

> _"Little Amanda Williams? But oh, she's an angel! Always so helpful, better girl scout in region, church chorus singer, a cupcake! Only that Bunny Brother stuff is weird: she brought that so called Bunny Warren with little Toby Michaels and Henry Mitchell, and Henry's father, Justin helped them, but... God bless him, but he didn't got back alright from 'Nam, you know, totally bonkers! And we don't know if those Reds didn't did some bad stuff with him"_

This is truth: Amanda, with some friends, brought a _Bunny Warren_ at the city, with that Mr Justin Mitchell's, Henry's father, help

+ ***Why an adult would join a children's shows' fan-club?***

> _"Well... As far we understood, there's a requirement for a responsible adult be part of the Bunny Warren for all the Warren's activities being accepted, at least was what Mr Mitchell said us, but... You know... He lost it at war, you know. Even before the Bunny Brother got back on TV, soon and another he got out the city and back again with some weird Bunny Brother memorabilia! And last year, when the new animation got some steam, he and the Bunny Warren joined our Thanksgiving parade! Well... It was weird, but harmless, we thought at time, but know... It looks so bonkers..."_

If they looks upon some information about the Warren's participation on the Thanksgiving parade, they'll see that everything was OK, and the Warren was a little bigger then, with 10 children and three adults. One of the adults, in fact, is shown in the newspapers pictures on a very top notch complete Big Bunny Brother costume, with a big Top Hat with holes for the big ears, catgut cowl prothetics and big fake rabbit feet. If they ask about this pics, people will confirm the guy on the costume is that Justin Mitchell, as the newspaper only says that _"the local Big Bunny Brother Warren from Angola joined the Thanksgiving parade"_, as they did with other local organizations, like Freemasons, Shriners, Rotary Club, local church chorus, Civil War Reenactment Club, and the Clown Alley.

+ ***What you can say about this Justin guy?***

> _"He was at 'Nam and got back after losing his brother there and he got... Bonkers, ya know? Almost don't get out home, don't go to the bar, you almost can't see him aside when he go for the church Service and for grocery store... And he got to Army's Hospital at Michigan once and awhile to get some of those new meds for cuckoo people. But, to be honest, this stuff don't see to work if someone likes to dress himself like a big bunny, whatcha think?"_

It's _really truth_ that Justin uses psychiatric meds, and _really truth_ he dressed himself as the Big Bunny Brother and so, but there's a _real serious reason_ for all this stuff, that the characters will discover if they go for him at Act 2a.

If they want to go for the Williams family and look for Amanda's kidnapping clues, in Act 2b they'll discover the things are _even more weird_!

And if, give a look for the other Amanda friend Toby, go for Act 2c, and they'll discover he's a little too quiet, for his mother worry, Miss Wilhelmina "Willa" Michaels.

## Act 2a - Justin Mitchell

Justin's house is like many of them around: a typical Americana two-story house, a typical US flag fluttering supported by a hanger on the doorstep, a typical old Plymouth Voyager in front of the garage, working but that had saw better days. As soon characters knocks the door, it will not open totally. A woman will open it partially with the chain on it and says:

> _"I'll make it really clear: if you are another goddamn paparazzi, go for that Sheriff McMahon, he is the one who loves the spotlight! My husband had enough!"_

This is Mrs Justin Mitchell (_nee_ Shelly Hamon). Mrs. Shelly is ___loyal to her husband to a fault___ (_Fair (+2)_ for any rolls) and knows ___he deserves more than Army and this city gave for him___ (_Terrible (-2)_). If they can show her they are there to help Justin, she'll (grudgingly) open the door and let them in.

The windows are closed with light colored, but thick, curtains, what turn the main room a little darker and cooler, and ensure some well-needed privacy now the Justin is on the limelight by all the wrong reasons. There's few furniture, but everything very tidy and of good taste. An old Gala Army suit is on an mannequin in a corner. Over the hearth, two set of medals are exposed: bravery medals for those who were Vietnam veterans, specially after Khe Sahn, when everything was becoming a meat grinder. Between those, a picture of a 20-something years man, average but healthy, over a folded US flag. Any simple test will say that this is a flag folded after being taken from a fallen soldier's coffin and given to the family. All around the main room, some other pictures, with the same two guys on all ages, from the childhood with weird homemade top hats with rabbit ears through them, with other kids that looks like their friends, and to them older, on military fatigues and dogtags around their necks.

Over the father's chair in the room, watching the TV, there's a man that looks too much for one of those guys, but obviously older. Looks stronger for the age, but with a pot belly, and a very down look.

> _"He's this way since Amanda disappeared... The Sheriff was really cruel with Justin. What a pig! My husband didn't lost his brother and almost his sanity in Vietnam to be bullied by those idiots here! To be honest... Maybe it would be nice for him to talk a little with someone more supportive."_

Justin will give a look for this wife, that will get some drinks. _"No alcohol: it is totally verboten for Justin thanks his treatment."_

With the correct approach, they will obtain from Justin the following answers for some questions.

+ ***People said you are a Big Bunny Brother fan... Even helped to build the local Warren.***

> _"Yep, I've always been a Bunny Brother fan, since childhood! Seeing **Big Bunny Brother Adventure** when I was a kid made me look upon for becoming a teacher, you know... But... Well... Vietnam got into a quagmire just when we were going to Indianapolis for college, and then me and my brother were drafted. Worse is, unlike some of the other guys here, we were in the_ real real _front, to the meat grinder, not on office stuff like some of the playboys and preacher's boys here... Only_ The Big Bunny Brother Song _let us hold ourselves into our minds, you know."_

He will, on a somewhat childish voice, sing _The Big Bunny Brother Song_. It's clear that, no matter how much harsh the war was with him (looks like too much), he isn't like axe murderer or something like that: he's just someone that, to survive the meat grinder at 'Nam, just found a way to do this without going totally bananas.

+ ***So, you REALLY like the Bunny Brother... I say, from when you were a kid?***

> _"For sure! Me and my Brother Terry founded the original_ Bunny Warren _here at Angola. I still have lot of the gifts we've got that time: our Warren was always quoted by the Bunny Brother, he always said at the show about_ 'the always active little bunnies from the Bunny Warren at Angola, Indiana'"

He will bring the players to the basement, where he build what looks like an ___unofficial Bunny Brother Museum___: an old Super 8 projector aside some film rolls with old episodes from ___Big Bunny Brother Adventures___, including some with participation of the Mitchell brothers; books, vinyls, mugs, banners, comic books, the Top Hats with the Rabbit Ears, old _Little Friend Bunnies_ uniforms with his names and his brother's, _Little Friendly Bunny_ certificates for social actions... And on a corner, on a mannequin, a top notch, almost honest to gosh, Big Bunny Brother costume, complete with tails and waistcoat and cowl and Hat, full package, big enough for an adult: it would be obviously someone on it would not look like _real real_ Big Bunny Brother, as by the pictures on the old _Amazing Adventures from the Big Bunny Brother_, but definitively it is top notch stuff. If they saw the pics from newspaper, they will recognize it's the same one, and Justin will proudly confirm he was the one inside the suit. They'll understand he was really happy on bringing some smiles that time.

+ ***Well... So, how you got involved with the kids?***

> _"Since the first run of Big Bunny Brother, every Warren needs a Bunny Leader, a responsible adult to help and coordinate the Warren activities, it's like the Scout Leader, you see. When I was a kid, my father was the Leader, and I always thought my elder brother would be the leader, but... They took the program out and he died at 'Nam... And then, when the program got back, my son started to see it, and he remembered some of the comic books from my collection, and then he wanted to build a new Warren at Angola, with some of his friend at school that liked the Bunny Brother, like Amanda and Toby, the one that were more active with Henry, and others... But..."_

+ ***But?***

> _Need to say: I don't like how they are doing Big Bunny Brother nowadays. Call me old-fashioned, but in my time the Bunny Brother were somewhat naive but really clever: he didn't got into a fight even against his enemies, like Fred Fox or Bruce Bear. He always got out their traps with cleverness and with help of friends they did in the way. But now.... Honestly, he is not too much unlikely Tom & Jerry, you know. All this violence is not Big Bunny Brother-ish"_

Above everything, Justin is a worried parent (perhaps a tad bit too much) with family, as after his brother death is everything he has at life. 

He will show some of the classic Big Bunny Brother episodes: describe that he never got into fights, but used his cleverness and the help of friends to save himself. On a time a skunk spray making them run away, on another use the water from the rain on some leaf to mess with the enemies sight and make they roll from a small mound straight into some thornbushes, or other things like that, always using cleverness and friends help.

The new ones, however, although Big Bunny Brother still get help, he is more "on your face" with kicks and punches, not too much away from Tom & Jerry, they'll notice by seeing some episodes with the Mitchells.

+ ***And about your brother?***

> _"We were in the front, and even there we used what we learned with the Big Bunny Brother to help the Vietnamese kids there: it wasn't their fault, they were also innocent on the war. We hated all that_ napalm _stuff, those_ carpet bombing, _we never used them on our battalion, even against Pentagon orders. This was until when we were on a battle so intense the grenades and obuses and mortars were flying all around, we couldn't see which fire is which.... Then a mortar fell nearby us: I had my shoulder broken, hit by debris. My brother hadn't been that lucky: they found only his head and torso... I will never forget when I saw his brother... As I was wounded, I was sent back, nut I was so bad in my head, they discharged me, throwing me away. I had some... Dreams, lucid dreams, like visions... With my dead brother, in pieces... Even when at grocery store, like one of those_ Night of the Living Dead _Zombies... It is hard, and it would be even worse without Shelly here. She was part of our time Warren, and she lost a brother at 'Nam too, and she helps me to cope with all this. She is a teacher at school, but she was given absence time since the Amanda issue to care for us."_

If they ask Shelly about this, she'll gave the cliche, but true, answer: they knew themselves from childhood from the Bunny Warren, she liked him and vice-versa, but they were both too shy to concede and ask the other, till he was drafted to Vietnam and got back broken. She helped him with stuff, they chose to assume the passion, marry and they had Henry. Justin do some small work as a fix-it-all, with tractors, some pumbling, and so, to help with money, but the main cash comes via Shelly, and Justin helps with home-care stuff, which helps him to avoid too much people.

+ ***And your son, Henry, how he's dealing with Amanda's disappearing?***

> _"He's sad... And feeling a little guilty: he thought something was wrong on Amanda, and they had one of those childish clashes the day before Amanda spirited away, and Toby got to support Amanda. The fact is: Henry agrees with me about the excessive violence on the new Big Bunny Brother cartoons, that encourages some nasty behavior on kids, like cheat and lie and fight, instead what happened on my time, when he encouraged ingenuity and creativity"_

If they ask Toby, he'll confirm Justin:

> _"We had a fight, you know: Toby and Amanda liked that new episode where I thought the Big Bunny Brother was really mean, even being against Fred Fox, by cheating him and shoving that bee hive over Fred' head! I feel really annoyed with them and sweared bad names against them both... And then, next day, Amanda disappeared and Toby stayed at home since!"_

In the end, wasn't Alice disappearance, it would be only a childish fight.

+ ***How Amanda disappeared?***

> _I don't know: I was woken up by the Sheriff almost putting my door down, interrogating me as a criminal, just because I'm the Bunny Leader of the Bunny Warren. Like a criminal, goddamnit! They searched all around my home, they even messed up all around my Bunny Brother collection, almost destroying it... Like I was a criminal! They said that Amanda was saw at the Forest, and that I would be the one that kidnapped her, but I was all the night at home after the Church service! I just was here in home, playing with Henry, all the night. I didn't got to bar or anything like that, I can't because my treatment."_

The fact is, Justin knows nothing about Amanda's disappearance. He really stayed at home all that night, but this Alibi is not that good, as the only ones that could confirm the Alibi was his wife and kid, and this is not too much, even _no one else_ saying he was outside. And Justin was always read as someone that got back Vietnam with some loose bolts, specially with all the Big Bunny Brother thing, so people in the city _already_ had prejudice against him.

If the PCs wants to investigate some more, they can either go talk with Toby (Act 2c), go for Amanda's home (Act 2b), or try to look into the Forest (Act 3)

### Act 2b - The Williams

The Williams family lives in a bigger, nicer home than the Mitchells, and their prosperity can be saw on the two cars in the garage: a brand-new Dodge Caravan and a very well conserved, almost brand-new, classic '76 Chevrolet Camaro. However, when the characters arrives, although the cars are in front of the garage, clean and neat, looks like there's no one at home.

The characters can get inside house by breaking in (***Fair (+2)*** _Burglary_ check), or trying the trick of finding a spare key below the backdoor's mat (1 PD or a ***Fair (+2)*** _Insight_ check). If they got failures, there's a chance police can get and find them soon!

Anyway, when they get in, they'll see that the house is clean and tastefully decorated, even including a brand new IBM PC at a corner! However, an _Awareness_ ***Fair (+2)*** roll will let them feel something weird, a ***Faint rabbit smell*** all around. Also, they'll notice lots of things that ***show the Mitchells has a past as* hippies**, like some photos on *tie-dye* shirts and *hippie* necklaces on rallies against the Vietnam War, and some counterculture books from the time on a rack. And they'll also see some pictures from their time as kids, with the Mitchell brothers Justin and Terry. The same one can be saw on the Mitchells house and on Michaels home at Act 2c.

If they search for clues on Amanda's bedroom, they'll notice it's the default little princess bedroom from 80s: canopy bed, a dollhouse, lots of teddies, a vanity table, lots of pictures with birds and cats, a big wardrobe full of pretty clothes, including her majorette suit and ballet leotard... The only odd thing was some Big Bunny Brother stuff, including some story books, a Hat with Rabbit Ears, and some cassette tapes from Bunny Brother that can be heard on a player over the table.

_Insight_ or _Awareness_ ***Good (+3)*** checks will let them find hidden somewhere _Amanda's Diary_. If they give a look, they'll see that he was already having weird dreams with the Big Bunny Brother calling her to the woods ***before Big Bunny Brother's show***, but after the show she was convinced she was being ***called to be part of the Special Big Bunny Brother Family*** (Justin, if asked about this, will say this doesn't exist). She also says she was hearing the Big Bunny Brother History tapes before sleep to help here, but there was something weird on this all, saying her to leave behind family and friends, what she didn't want to.

However, the last entry on the diary, from the day before she disappeared, is saying that she made her mind and would go for the woods: she knew that (or she believed), as Big Bunny Brother stated on the tapes, his parents and real friends, like Toby, would understand, and that Henry Mitchell was _"a silly and stinky meanie boy, not a little friendly bunny"_. Toby would understand as he also heard the Big Bunny Brother tapes (Justin will say that both Toby and Henry didn't want to hear anymore the tapes after hearing one she lent on a Warren's meeting and that Toby said about weird dreams)

The bed is untidy, and there's some dust all around... Like no one got inside for a while. This can raise some flags on the idea that their parents weren't worried with her disappearance...

Which is somewhat true.

The parent's bedroom is a little cleaner, but there's two weird details: the untidy bed and the rabbit smell all around.

If they give a look, they'll find a pair of brand-new _Walkman_ on the nightstands aside the bed some _bootleg_-looking tapes with the title _Big Bunny Brother Messages_ on the drawers!

If the characters try to hear the tapes, ask for _Will_ checks, starting difficult _Mediocre (+0)_. On _Fails_, they'll get a hidden Aspect on them for the _Big Bunny Brother Subliminal Messages_, with a number of Invokes equals the number of failures. While the character stay hearing the tapes, he'll need to do _Will_ checks with a +1 per fail, and need to do an extra check when decide to stop. Each time they succeed, ask for _Knowledge_ or _Insight_ ***Fair (+2)*** checks, or instead pay 1PD to notice what is wrong! They'll notice the _Subliminal Messages_, a _Success with Style_ or another roll (incurring on the chance of being put under the _Subliminal Messages_) will show something about _Rabbit-kind's future_, and asking them to _shed thar nasty human form behind, going down the Rabbit Hole of the March Hare's Wonderland_.

If, to finish, want to give a look on the attic, they'll notice the rabbit smell on the place is really strong, almost a stink, with lots of Big Bunny Brother stuff all around the mess, and there was also some clues that points rabbits were there, like felps and even droppings, but with a weird thing on it: they are a lot bigger than you thought a rabbit could drop! Something weird was there: in fact, probably there would be difficult for the characters to confirm it, but the Williams were hiding (without Amanda knowing) some ***March Hare's Rabbit-men*** in the attic for his master.

If you think they had already took too long there and/or they failed when breaking in the house, the Police will get there, with some armed officers and Sheriff McMahon. Enough to give a scare on the players and _"make those hoodlums go away"_, but not enough to make them suffer.

They now can go to Justin (if they were not yet or want some extra information - Act 2a), go for Toby's house (Act 2c), or go for the forest (Act 3)

### Act 2c - Toby and Wilhelmina Michaels

Unlike the Williams and Mitchells, the Michaels lives on one of the apartments over the local _General Store_ at the Main Street. Big nice apartments, but a lot smaller than the other families houses. If they say they want to talk with Toby, Wilhelmina (Toby's Mother) be very reticent on let them talk, after everything that happened, but if they convince they will try to avoid painful topics (_Persuade_ rolls), she'll allow them to talk with Toby.

Toby looks a little bigger, stronger and older than Henry and Amanda, but he's surprisingly shy, and there's a reason: albeit all this, he's their gang's _nerd_, his room full on memorabilia (specially Big Bunny Brother related), fantasy and fairy-tale books, dictionaries and school books.

Toby will be on living room, the TV showing of the Big Bunny Brother cartoons, and then they'll understand Justin and Henry's criticism: although it tries to look something else, it's not that different from Tom & Jerry or Woody Woodpecker stuff, which is very different of those more cuddly cartoons like _Get Along Gang_, _Kissifur_, _Popples_ and _The Care Bears_.

If they look around, they'll see a photo from Justin and Terry when kids, with the Williams and Willa, and on another photo, a handsome old man. If they ask about the latter, Willa will say it is her deceased husband, died on a cardiac arrest more or less an year before. She still suffers, but is dealing with this the best she can the best she can.

If they talk with Toby, Willa will be always nearby, but will let Toby at ease to answer everything he can under his shyness. If they can convince Toby that they are trying to help Amanda and Justin, he'll open himself for them:

+ ***Looks like you were the big kid for your little rabbit friends, right?***

> _"Yep. The other kids in the school always bullied me, because I like to study, and they got harsher after my father's death. But Amanda and Henry helped me in the Bunny Warren, and it is fun. It always was, but after my father died, things become easier after joining the Bunny Warrens and got more to Henry's house... Until some days before... When Amanda disappeared."_

Toby makes clear he focused on the Bunny Warren as a way to cope with his father's decease. Nothing that special, but it was the way to deal with death, which is not that easy for a 8 years old kid.

+ ***What do you think about Mr Mitchell?***

> _I know him, and he would never do that! He always loved us, he's an incredible Bunny Leader, the best one, following everything the Big Bunny Brother teaches us. I know what the people in the city thinks about Mr Mitchell, but he is not a bad person and he would never do something bad with Amanda!"_

Toby believes on Justin innocence so much that any suggestion otherwise can make him get really hostile against the characters.

+ ***And what about the parade last year? Isn't a little too much an adult on a Big Bunny Brother Suit?***

> _"No more than the clowns, and the Indian dressed people, and the Mickey guy and... People can say a lot of bad things on Mr Mitchell, and I know they do: they say he's bonkers and so... But he was one of the few that got for my father at Hospital before he died, and allowed Henry to bring me their home for some sleepover on that day!"_

In fact, it _just looks like_ Justin had good friends before goes to Vietnam: when he got back, broken by the war, Big Bunny Brother being his obsession to make him somewhat functional after the war trauma, people took him as a bad influence. If the characters didn't talked yet with Justin, she'll say them all about Justin and his brother at war, and will confirm something that the characters maybe got on alone: she, her husband, the Williams and the Mitchells where some of the main Little Friendly Bunnies from the old Bunny Warren on Angola in the past. Willa, as a nurse, now discharged for a while to take care on Toby, will say that Justin reopened the Warren when Big Bunny Brother got back to help the other parents, all of them too busy (Mr and Mrs Williams were construction engineers)

> _"Wasn't for Justin being a Bunny Leader for the new Bunny Warren, I don't know what would be for Toby: he was always a shy boy, and as he's bigger and stronger than the other boys, they always looked if he was really strong, but as strong Toby is, he's also kind, and nowadays I know people take kindness as weakness."_

She almost spat this, and everyone can read that Angola is not exactly this Americana paradise it shows. If they ask a little more, they'll see Justin wasn't as cherished by people as they try to show, even before going to 'Nam, they only never said that openly.... And now that he got back messed, people only took the niceness away.

> _"Justin and his brother were always kind guys... Justin still is, but with all the meds for the PTSD, he's not the same. He didn't get too much out his house, becoming a recluse. Shelly is so sweet caring for him, and he's also a sweet to try and make the possible to make their life as easy as possible, caring for home while she goes work, doing laundry and food and all, but... The fact is that his war-given Post-Traumatic Stress Disorder messed with him, made him isolate himself, and people here doesn't like this. His only social meeting, aside going to Church Services, is, or was, the Bunny Warren. And I know very well what adults takes for another adult that, without any payment or other benefit, care by himself some kids."_

The city of Angola never took Justin's isolation well, and as the Bunny Leader for the Bunny Warren, the only adult with kids around him for no reason aside reasons, he was "obviously" read as a child molester in potential. But Willa knows better, as she's part of his treatment so he doesn't need to go too much for Army's Hospital all time, and so she can see how much the city do ill for him.

+ ***What do you think about Henry and Amanda, Toby?***

> _"By far they are my best friends! Amanda is so cute, all prim and proper and a nice student, and Henry is so creative and courageous, more than myself... However..."_

+ ***However?***

> _"The day before she disappeared, we had a very nasty discussion: to be honest, Amanda looked weird since we got to the_ Big Bunny Brother live show, _looked a little aloof. It was when Henry asked what was happening, and commented he didn't liked the last Big Bunny Brother cartoon, and Amanda said it was silly and said some really bad words about Mr. Williams. And he talked back saying she was different, and that she didn't remembered Big Bunny Brother always said the Friendly Bunnies always stayed together and trust each other, and that she was hiding something. She then got really mad, and they shouted to each other, and I tried to just pull them away for a while and then Henry said some nasty things about my father, calling him a coward by not going to Vietnam, like his father and his deceased uncle, and I've snapped and shouted back to him... And we fought... And then Amanda disappeared.... Maybe Henry was somewhat right about Amanda, in the end."_

Now things can be more cleaner: if they read Amanda's Diary at Act 2c, they'll know that he was under some bizarre hypnotic effect, and if they heard the _Big Bunny Brother's Messages_ on Amanda's parents tapes and discovered about the _Subliminal Messages_, they will notice Amanda and her parents were being manipulated by the Big Bunny Brother since a while.

+ ***Do you know here she could had gone? We heard about her had gone to the Forest***

> _"There is some places in the Forest where we go to play hide'n'seek or when we wanted to fish in the river... But I've already passed all this information for the Police and they didn't found her, only her clothes... Torned away..."_

Looks like Big Bunny Brother is really taking kids away... Question is...

Why?

They'll only find answers in the Forest, at Act 3. However, they can go look for some extra clues on the Williams (Act 2b) or at the Mitchells (Act 2a)

## Act 3 - Into the Woods

If the characters didn't got into that yet, someway the Big Bunny Brother was convincing kids (and some adults too) to go to the woods alone, where he find and abduct them....

If they need some extra incentive, there's always the chance some ___Marshall Hare's Rabbit-men___ got to the city in the night to try and abduct either Toby and Henry... If and how this will happens it's up to the Narrator.

The Forest nearby is really dense: beside anything, the local _amish_ community take care and preserve nature, as it provides them game meat, as by skunks, rabbits, foxes and other somewhat small game. However, as they walk in the woods, they'll notice something wrong in the way of a ___very pervasive rabbit smell___ all around the woods.

Now, ask some _Insight_ or _Awareness_ checks for the Characters, that will be opposed in secret by the ___Marshall Hare's Rabbit-men___. If the characters fail, roll again for the Rabbit-men and use the characters' previous effort as Difficult.

> + If the characters are succeeded on their roll, they'll notice some ___Weirdly shaped shadows___ in the woods: they look adult-sized, or even bigger, the biggest ones going 2.40m (7ft10). A Success with Style will show the shadows are ___Not exactly human___: there's no way to pinpoint thanks their movement but the shadows looks _wrong_,w ith hunchbacks and weird appendages in the top of the head. If they pursue or shadow them, they get into a ___Contests___ against the ___Marshall Hare's Rabbit-men___. As they are four, they get a +1 on checks and put on them two stress boxes (the bonus disappears as soon one of the boxes is checked).
> + If, instead, the ___Rabbit-men___ are successful on their check, they'll ambush the characters, jumping over the characters out of the woods and Attacking them, but they can still Defend themselves, it's not a complete ambush. As said before, it's four ___Rabbit-men___ that will act together, team up against the most strong-looking character, so give them the same +1 and stress boxes as described above. They are really crazy loyal to his master to death, preferring this fate than being held hostage and betray his master. However, if the characters are _taken out_ (or Concede), one of them that looks like the leader of this team, looking more like a hare than a rabbit and really old, with grayed fur and yellow bucky front teeth and curved claws bellow the finger pads, will held them hostage and bring them to _"The Rabbit-kind Master, that will show you the rabbity pleasures."_

No matter how, they'll be brought to (or follow the rabbits) through a way, and as they go forward, the rabbit smell grows over and over: there's rabbit dropping all around till get to an old mine... Guarded by lots of Rabbit-men (12 at least). They then will get inside the cavern, either held hostage or shadowing and hiding inside behind some crates all around, and they'll see some Rabbit-men in front of a Hare... Or better... A half-man half-hare _creature_, with a maniacal glee while looking on what's happening on that weird device, a terrifying look dentist-like chair with lots of CRTs and cables and electrodes and leather binds all around, and all stuff you could imagine of a slash movie torture chamber... 

And on bonded to it a person... Or better... What _looks like a person_, a hobo guy... Except his ears are no more the side of his head, but _over_ it, pulled up, his lips now pulled forward and fused with his nose as a muzzle, from where a painfully high-pitched rabbity squeal can be heard, his hands trembling while fingers became padded claws in a grotesque, _An American Werewolf in London_-like show. Till at a moment, the hare guy shuts the machine down and the hobo got knocked out, parts of his body still looking like made as hot wax, releasing new tufts of fur while bursting his skin and changing bones and internal organs to their rabbity versions.

> _"Yes! Yes! What a show! All this new information I've got from this specimen will help a lot for the Super-Lapinator! Soon I'll build a Lapinator big and powerful enough to transform all the world into my_ Oryctolagus Sapiens _that will treat me as the new Rabbit-kind God-King! Humankind had failed, locusts that spoils and destroys everything! I'll be the lord of a new age, the Rabbit-kind Age! And the Big Bunny Brother is just the beginning!"_

If the question if he's the Big Bunny Brother, they'll discover, surprise, he's totally _different_ of the Bunny Brother, from the fur color, muzzle and ears and tail, not an ounce of him looks alike Bunny Brother... 

And what about Big Bunny Brother on all this?

In meanwhile, they'll see two Rabbit-people caring for a cage. If they got into the Matthews' home and looks their pictures (Act 2b), they'll notice they are too much looking like Mr and Mrs Matthew, except by their absent-looking eyes and rabbity shapes. Inside the cage, there's a small girl, already with the ears and muzzle of a rabbit. And her look will trigger a memory from the _Missing_ posters all around Angola.

She's Amanda Matthew, now partially turned into a rabbit-girl!

And, to make things worst, any character under ___Big Bunny Brother Subliminal Messages___ will be Compelled to help the Hare-man, The March Hare, to capture his friends and/or volunteer themselves as volunteer for Lapination!

If they try to rescue Amanda (or need some help to get away), a form shows himself in the dark, vaguely humanoid, but not discernible at all. He will talk under a acute, small and fast squeak:

> _"Just follow me and don't make any noise!_

The characters can help to rescue Amanda (and occasionally Toby and Henry, if they got captured). After all, those ***Cages*** are not that secure (***Average (+1)*** _Burglary_ to break them). Also, they'll have the help from the person in the dark.

If they need, they can try to run away, but them will be another _Contest_, except by the bigger number of ___Rabbit-men___ chasing the characters (+2 bonus, five stress boxes), aside Mr and Mrs Williams _helping_ the Hare to chase their own daughter!

As they got out the cavern, they'll discover who's the one helping them.

The Big Bunny Brother!

But... Wasn't him the kidnapper?

> _"Run first, talk later!"_

Now... Some ways to solve this actions:

> + A Conflict would be a bad idea, but if one of the characters choose he can sacrifice himself while the others can run away with Amanda and Big Bunny Brother. This character surely will be turned into a ___Rabbit-man___ via the Lapinator (as a Extreme Consequence, changing one of his Aspects, maybe his _Team-Up_ or _War Story_ ones), but will not be under Marshall Hare's control, at least not totally: give the character (by checks or FP uses) the chance to run away some time after, or stay there to learn more about Marshall Hare's plan and work on ways to sabotage it.
> + A Contest by chasing is the obvious situation. It will not be easy, but if they do it right, they'll be able to run away, even more with The Big Bunny Brother's help
> + They can try to lure the rabbity instincts from the Rabbit-men against them, and it would be a good idea: fire, predators sounds, high sounds (a rifle shot to the air would be enough) and things like them can be use to lure their ___Instincts___ making them run around without control, giving them time to run away

## Act 4 - The Big Bunny Brother

No matter what, hope they can run away. If so, they'll go through a way till The Big Bunny Brother bring them to his hiding place into an abandoned _amish_ farm somewhat two hours of walks where they were, between Angola and Shipshewana, away enough from roads to be a nice hiding spot, allowing him some privacy. In the stable, Big Bunny Brother's hiding place, the characters could see some vegetables on a corner (curiously very few carrots), with a big bucket of water took from the well nearby, clean and fresh enough to be drank. A big package of rabbit pellet is also around, and some blankets all around, some really dirty but uselfu enough. The rabbit smell is strong, but a lot less than in the cave.

> _"Please, be my guest and take some rest... I try to take my baths, but the river nearby is a point for coyotes, foxes and other predator, so I hope you don't matter the rabbit smell."_

The Big Bunny Brother will offer some food and water while lighting a small fire to heat them in the night. _"Unfortunately I only have some veggies and rabbit pellets."_ he says. For some reason, he'll be always ___The Big Bunny Brother inside out___, from the totally rabbit-man-ish appearance that makes totally IMPOSSIBLE think on this as a costume, makeup and prothetics, but also and mainly on his gentleman-ish _behaviour_: calm, suave, kind and clever as they know if they saw enough ***The Big Bunny Brother Adventures*** episodes (probably with Justin)

The only light all around is provided by the moonlight and perhaps by the fire the Big Bunny Brother lits. While he tidies everywhere for them, he'll whisper _The Big Bunny Brother Song_. if they join the The Big Bunny Brother in the song, Amanda (that is really frightened on all this, which make her release the smell like a fearful rabbit) will calm down enough to talk.

Amanda will say her parents looked weird since a while, specially after making her hearing the new ***The Big Bunny Brother Adventures*** tapes. If they didn't noticed since, the Big Bunny Brother will say that those tapes have subliminal messages to make anyone hearing them, in this case both Amanda and her parents, to fall under the March Hare's grasp... But more about this later...

+ ***Alright, who are you?***

> _"My name, or the one I can give you, is Eaton Rabbit, and there's no other way to say, but I'm the_ real real _Big Bunny Brother."_

He'll give them his history, as below, but summarized. The fact is that, no matter who Eaton was before being turned into this Rabbit-man by the March Hare via The Lapinator, he ceased to exist, now only The Big Bunny Brother existing as a new _persona_ created for and by himself. He had a nice happy life for a while till the March Hare crossed his steps again, and he had to give away his new life... _"And I will not let him destroy my life again... He did it before, and it was enough!"_

+ ***So you know that other... Rabbit-man?***

> _"He is Marshall Hare, self-proclaimed_ March Hare _and_ Rabbit-kind Sire. _For some reason, he looks to turn people on Rabbit-men and put them under his will, which he calls_ Lapination. _It's really painful, and I only didn't fell under his grasp as my rabbity instincts kicked before his post-hypnotic trance fixed itself, putting me under his grasp."_

If they questioned or remembered the _1911 Rabbit's Easter_, they'll remember all the histories, and if they investigate a little more the memories, they can remember that a weird jewel, part of the _Fabergé Eggs_, had gone missing some time before. With a Success with Style, they'll also remember some legends about a _Mystical Fabergé Egg_, a jewel imbued with transformation-related mystical powers. It's not difficult to notice that the Hare was crazy, but clever enough to find and develop ways to turn people into rabbit-men under his will.

+ ***And what was that machine?***

Amanda, even weakly, will say

> _"That stinky meanie called it_ The Lapinator, _and he uses it to turn people into rabbits... But he can put people under it only for too long, people would be really hurt, a lot... Like he did with me... I was put it on and it... It hurts! A lot!"_

Then she'll start to cry, a high pitched rabbity squeal also on it. If no character has the idea to get nearby to calm her down (what a great bunch of New Age Heroes we have here, nah? NOT!), the Big Bunny Brother will hug her, pulling her to his soft furried pot belly, while whispering again the _The Big Bunny Brother Song_... And then they can be certain that he ***IS*** The Big Bunny Brother as by the song! *"We will have no fear or sadness, hooray! / Because we know Big Bunny Brother will come, yay hooray!"*

Then, after calming a little down, she'll continue:

> _"He put you and bind you under that machine with all that electrodes, and it starts to shower you with those rays and lights and stuff, and while sending you some message on your ear that you are and always had been a rabbit-person, part of that... Rabbity kind... And that you should always behave and obey the March Hare... I only didn't fell under it because I saw that meanie  was not The Big Bunny Brother... And that those weren't also my parents! Parents don't do what they did with me! I could only remember_ The Big Bunny Brother Song _and this... Helped me... To hold me as myself and doesn't get into that stinky meanie's lies, as that machine tried to make me obey him!"_

Fact is: Mr and Mrs Williams, on a tragic but real way fell under March Hare's wrap thanks their _hippie_ past and the link with _The Big Bunny Brother_ in the past, but this same link in the present saved Amanda to get into March Hare's mindwrap.

+ ***Amanda... Had you heard something else? Anything?***

> _"He said about... Something like a... Super Lap inator... That would bring people to rabbity kind... All the world! Creating an army of a Rabbit-men and... Becoming a kind of... God... King... Of this world!"_

Even with Amanda's child-speak, it will be clear the March Hare's objective is one and only one.

> ***Bring Humankind under his wrap as a Rabbit-kind as a Rabbit-men Army!***

This is the _big advantage_ for the _Rising Action Milestone_

+ ***But... Big Bunny Brother... How you let him take your mantle... Your show...***

> _"Believe me, not my fault: he continued his rabbity experiences even after I ran away, and somehow he discovered about **The Big Bunny Brother Adventures** show. Well, I should think he would do it, we became a very popular show all around US, we had once as much Warrens as there was Hu Dunnit Clubs or Mouseketeers clubs. And then those desappearances happened and every fingers got pointed to us. My old agent, Priscilla Preston, even tried to save what she could with PR, but we chose to end the show before the murky stuff splashed on everyone else, and then the IP rights and assets from the show were sold for_ Peek-a-Boo Entertainment. _But I can't see how the March Hare and_ Peek-a-Boo Entertainment _would be linked, if so."_

This is truth: there's no way to confirm _for sure_ any link between March Hare's actions and _Peek-a-Boo Entertainment_. There's no proof for yay or nay, and this is the problem.

+ ***This Priscilla girl, where she is?***

> _"I don't know... After the show was cancelled, I took some time around, doing occasional The Big Bunny Brother shows to make a living, but always got into contact with her, but recently didn't found her..."_

It's up the Narrator on what happened with Priscilla, but some ideas:

+ She's doing her best to reveal all about the fake Big Bunny Brother: in fact she was the one to ask Alpha or James Marconi about this, as they knew a lot of people, many of them owe them some and vice-versa;
+ She had been captured by the Hare and lapinated. Now, there's only a question: had she fell under the Hare's will (becoming ***Alice Hare***) or not. On the latter, she can be acting like she's at Hare's side, only waiting the best time to backstab him;
+ She's on hide: she doesn't want to have anything with this for some reason, probably she know too much and want to be low profile;
+ She fell under Filippo Geppeto's ***There's really strings on you!***: if so, Priscilla was manipulated by Geppeto to sell him the assets and right for The Big Bunny Brother to him, and then got to his side, working for him, even being the one to help or lure Marshall Hare getting into The Big Bunny Brother identity and working his plan.

+ ***And how we will stop the March Hare? No way we can fight a Rabbit-man Army!***

The longer the characters plans, the bigger March Hare's numbers grows.

If they get back to Angola, they'll find that now people from all ages are going to the woods and being kidnapped and probably lapinated. If Henry are not on those, Justin will be now in jail, arrested as kidnapper and child molester. If they take too long there's a very nice chance for Justin kill himself, as without his meds and under the pressure of false accusations, the War Traumas will get back full blow, enough to make him think on death as a bless!

There's something The Big Bunny Brother will remember, though:

> _"Marshall Hare hates me: for too long, during the time of **The Big Bunny Brother Adventures** show, children loved me, many of them still love me, they got to me no matter in my own show, or even when I guest started on other shows, like_ Fred Roger's Neighborhood, _and this is the thing he craves the most: he's a crazy narcissistic, for love and devotion. I don't know why, but fact is he's mad as a March Hare and wants this the most. And as I had all this being The Big Bunny Brother, he would do the best to destroy me and replace me for it."_

If they suggest any plan that involves using The Big Bunny Brother as bait, Amanda will squeal no, but The Big Bunny Brother will accept without any though: as much he loves being The Big Bunny Brother, he has no more his first life, and is almost losing his one as The Big Bunny Brother, leaving him as a man with nothing to lose. If there's any chance, no small it is, to stop Marshall Hare by those plans, he'll accept this without thinking.

## Act 5 - _"Because we know Big Bunny Brother will come, yay hooray!"_

Now, it's time to trigger Final Confrontation by cashing in the Big Advantage.

But how?

Some ideas:

+ Go and invade the March Hare's base to bring his Lapinator down and capture him is almost suicidal... _Except_ because, as there's lots of rabbit-men there, it's perfectly reasonable to make all the rabbits imploding in panic in some way, by gather enough things that could make them go crazy thanks their rabbity instincts, like foxes and other predators (or their sounds), and using this to press them mentally till they go banana and flew away in instinctive panic, leaving behind Marshall Hare and only a handful of those most faithful and resistant servants, like Mr and Mrs William and perhaps Priscilla/Alice;
+ if they report to Alpha, everything will get so messy that she'll ask some days while James ready things to go to the field... What they don't know is that ***James Marconi doesn't exist in fact***: he's a fake identity for one of the Spirits that, in 1911, fought and defeated  Marshall Hare, ***Nicola Castrogiovanni, Spirit of Optimism***, that maybe can bring reinforcements from other Spirits or not. Probably this fact will be revealed during the Final Confrontation between the characters and Big Bunny Brother vs Marshall Hare and his Rabbit-kind Army. Even long time flew since his defeat, he would never forgot _"that awful smell of one whose frustrated his plan to turn New York into my own Wonderland, that pesky little clownie Nicola Castrogiovanni!"_ Maybe he also found some of the old-time Spirits of the Century that got away from the ___House Un-American Activities Committee___'s persecution, names like Sidney Sheeran, Oregon Cadmus and Mairead Mag Raith coming to help James/Nicola.
+ If things happens nearby Easter, they can always take a good use on the rabbity time to lure March Hare against them, specially against we know Big Bunny Brother: March Hare's hate against The Big Brother is big enough to make him go all-in against him on Angola, if rightly provoked. If put in the right way with the right proofs, Amanda and all the Bunny Warren members can reveal the truth about March Hare, and put him on the other side of an ___Horde of Furious Parents___, ready to turn him and his servant (INCLUDING Mr and Mrs Matthew) into rabbit stew!
+ If the characters got into trouble, there's always the chance Priscilla just backstab the Hare and reveal she never got into the Hare's lure, but she faked being at her side, gathering intel and proofs on things to turn her back as a human, undoing the Lapination, that she send for a friend on _Cross School of Engeneering_. He sent her back some information on how unlapin people that were recently lapinated. Not enough for her or Eaton, but enough for the Matthew (or at least Amanda)
+ The PCs can bring help on contacts from FBI, CIA and other organizations, but remember: the Man can always be behind the curtains, and there's a big chance of this becomes a Phyrric win that in fact give to the enemies all information about the Lapinator, that can always be used to advance The Man's agenda, Specially _New Science Party_ and the _Kroll'X_ (Shadow of the Century, p. 23 and 26) can find this really interesting, as a way to push forward their Agenda (and as a chance of a new Series, either working with Big Bunny Brother or with NOVA... or with both!)
+ Big Bunny Brother has another big secret: in secret, he built (or knew about) a "elite team" of Little Friendly Bunnies from the original Bunny Warrens, a kind of national level Bunny Warren, in fact a _Golden Seed_ (Shadow of the Century p. 153), an organization that got some of the money left behind by the deceased _Spirit of Trade_ Mack Silver after his fall under Doctor Methuselah on an explosion in 1961. Those Friendly Bunnies (Justin on it or not, is up to you) are specialized on nonviolent actions to fight crime with ingenuity, inventiveness and helping each other, as teached by the Big Bunny Brother. They are not as big as the Hu Dunnit Mystery Clubs, Silver Foxes or the Cross School of Engineering, but the Big Bunny Brother's Friendly Bunnies are always ready to act in crisis situations like this one.
+ Let them freak it on to find a way: it's a _Shadow of the Century presents_ movies, for Pete's sake. Go big or go home! There's no points for the second best! It's hard to be a man when there's a gun in your hand! Even the greatest stars find themselves in the Looking Glass! It's time for them to show that they are all in to bring March Hare down!

In the end... Everything okay... They avoided the Rabbit-kind Uprising! 

After all in the end of the day...

*"We know Big Bunny Brother will come, yay hooray!"*

## Epilogue - Loose Threads

Obviously there will still some Loose Threads... Question is if they want to solve them to finish the movie or leave them behind... (Maybe for _Big Bunny Brother II -  The New Rabbity Menace_, who know)

+ Is Christian Matteo (or better, Filippo Geppeto) and _Peek-a-boo Entertainment_ has any link with all this mess? What should be a fun show became into a nightmare... How many events like this one will still happen? And it was this a real conicidence accident? Which interest are on this all? Including those hidden in the Shadows...
+ Looks like this will be not the last time NOVA and the characters will hear about Marshall Hare. Even if acquired, without the Century Club, who would be able to hold a rabbit-man mad as a March Hare?
+ Which bring us to Amanda and her family: what will happen with them? She will be unlapinated, or unfortunately her fate is still being a rabbit-girl? If this happen, who could care for her? Mr and Mrs Williams will be trustful enough after all this? Or maybe Justin could raise her, even in secret? And what would cost him?
+ If there was that Friendly Bunnies elite and they got back to help the Big Bunny Brother, will Justin be part of this?
+ All this mess put Angola under radar, for good and ill... How they will deal with this?
+ In the end, which will be Big Bunny Brother's fate? He'll hide himself again? Where? Angola town would help him, or at least leave him alone on that _amish_ farm he uses as hideaway? Or he'll be hunted by organizations with ulterior interests, like CIA and Talia Institute?
+ And... Perhaps now the PCs knew the truth about James Marconi/Nicola Castrogiovanni. And as Marshal Hare was still there, active, hidden in shadows, maybe other bad guys could be there too, like Lemont Fitzlefebvre or an Shadow trained by Anthony Fagin, the now-deceased (at least they think so) _19th Century Shadow of Innocence_. And this will bring a target pointed to the PCs and to NOVA... 

## **Appendix 1 - NPCs**

### Extras

+ ***Shelly Mitchell (nee Hamon):***
    + ___Loyal to her husband to a fault___ Fair (+2);
    + ___Justin deserves more than Army and this city gave him___ Terrible (-2);
+ ***Justin Mitchell:***
    + ___Local Bunny Warren Leader, Vietnam Veteran___ Good (+3); 
    + ___War had already broken me enough, they don't need to finish me___ Poor (-1);
+ ***Henry Mitchell:***
    + ___Amanda and Toby's Friend, part of the Bunny Warren___ Good (+3); 
    + ___Feel guilty of Amanda's kidnapping___ Poor (-1);
+ ***Wilhelmina "Willa" Michaels:***
    + ___Toby's mom and Justin's friend___ Fair (+2); 
    + ___A widow, Toby is all that she still have___ Terrible (-2);
+ ***Toby Michaels:***
    + ___Strong and kind nerd friend of Amanda and Henry___ Fair (+2); 
    + ___"There's something weird now in Big Bunny Brother"___ Terrible (-2);
+ **Marshall Hare's Rabbit-man**
    + ***Crazy Rabbit-man*** Fair (+2); 
    + ***Very touchy instincts*** Terrible (-2)
+ **Big Bunny Brother's gang**
    - ***Partially transformed rabbit-man (or kid)*** Fair (+2); 
    - ***Uncontrolled instincts*** Terrible (-2)
+ ***Amanda Matthew:***
    + ___Kidnapped girl being transformed into a Rabbit-girl___ Average (+1); 
    + ___"Those are not my parents and neither is this the Big Bunny Brother"___ Terrible (-2);
+ ***Mr and Mrs Matthew:***
    + ___A rabbit-person couple___ Fair (+2); 
    + ___The March Hare is our Master___ Terrible (-2);

### Marshall Hare, a Crazy as a March Hare Rabbit-man

Few are those that remember *1911 New York Rabbit's Easter*, and how near Marshall Hare was to turn New York on his own Wonderland, his fief where he would be king.

This only didn't happened thanks The Century Club and his Spirits. He had to flee, his instincts demanding him to run away from that humans that were hunting him.

Even fewer are those who remember about the kid that, after being called being *mad as a March Hare*, discovered (or imagined) himself as someone straight from Wonderland, a heir of the Mad March Hare!

But he discovered the process that made him half-rabbit had an advantage: unlike other Negatives (villains that were subordinated to the Shadows of the Century) that already died, as they weren't blessed with the exceptional longevity Shadows and Spirits received, his aging was slowed down thanks the mutations he got after using on himself the _Mystical Fabergé_, that was destroyed when Russian Empire fell and Soviet Union rose.

This didn't, however, for look upon on ways to bring people to his rabbit-kind, quite the contrary: by reading and testing some genetic theories, he developed his own Eugenic methods to activate mutagenic processes to change people, and did this by building lots of versions of a machine he calls *The Lapinator*, that he improved, and lost, and broke, and rebuild during the decades, until it looks like a big pile of junk that was, in fact, frightly effective on what it was built for. And now he believes it's time to build the supreme *Lapinator* and he just need some "volunteers" (being them volunteers or not) to help him. And he is using the Big Bunny Brother as a way to gather those!

And soon... He'll get a world level *Lapinator*! And this will make him Lord of Rabbit-kind!

#### **Aspects**

|   ***Kind:*** | ***Aspect:***                                                          |
|--------------:|:-----------------------------------------------------------------------|
|   ***Gonzo*** | A rabbit-man crazy as a March Hare                                     |
| ***Trouble*** | Hyperactivity and Egomania, disaster recipe                            |
|               | The future is the *Oryctolagus Sapiens* and I'm his beginning and sire |
|               | The Mystical Experiences from the past as a roadmap                    |
|               | Lord of the Rabbit-kind                                                |


#### **Roles**

|    ***Roles:*** | ***Level***   |
|----------------:|:--------------|
|    **Assassin** | *Fair (+2)*   |
|   **Authority** | *Good (+3)*   |
|    **Criminal** | *Good (+3)*   |
| **Manipulator** | *Great (+4)*  |
|   **Scientist** | *Superb (+5)* |
|     **Soldier** | *Fair (+2)*   |

#### **Stunts**

- ***Down the Rabbit Role (Gonzo):***
  - LVL1: ***Rabbit-kind Science:*** Encyclopedia Perambula (Genetics, focused on Eugenics, p 37)
  - LVL2: ***Rabbit-kind Future:*** Trust Me + Trust Yourself (p 45, to call his ***March Hare's Rabbit-men***) 
  - LVL3: ***Rabbit-kind Tech:*** Practiced—in Theory! + And Duct Tape + Weird Science (p 44 to build the *Lapinator*)

#### **Lapinator**
- *A rabbit-man transformation machine*
- *Built on duct tape but not as reliable*
  - ***Perfect Mind-washing:*** Attacks the target mindly using Marshall's *Scientist* level. If the target is _taken down_, he'll be now a ***Marshall Hare's Rabbit-man***
  - ***Body changing:*** Attacks the target physically using Marshall's *Scientist* level. If the target is _taken down_, he'll be now suffer a Extreme Consequence ***Body of a Rabbit-man*** as a Gonzo Aspect replacing some of his own

### **Alice Hare, Lord of Rabbit-kind's Wife**

No one knows Alice Hare's backstory... Neither her: she see reality and fantasy as the same thing, and even he being (a tad bit) more sensible than the March Hare, she would never left her Bunny Hunny behind.

#### **Aspects**

|   ***Kind:*** | ***Aspect:***                                     |
|--------------:|:--------------------------------------------------|
|   ***Gonzo*** | A rabbit-woman locked into a abusive relationship |
| ***Trouble*** | A little less crazy than her Bunny Hunny          |
|               | Reality and Fantasy are the same                  |

#### **Roles**

|    ***Roles:*** | ***Level***  |
|----------------:|:-------------|
|   **Authority** | *Good (+3)*  |
|    **Criminal** | *Good (+3)*  |
| **Manipulator** | *Fair (+2)*  |
|   **Scientist** | *Great (+4)* |

#### **Stunts**
- ***Mrs Rabbit (Gonzo):***
  - ***Rabbit necklace (LVL1):*** Like *Mr Underhill* (p. 49), but only to hide his rabbity form and back again
  - ***Rabbit Ears (LVL2):*** a very weird device, those Rabbit Ears can be placed on any target on a successful Attack and a PD. Summed with the stress, the target will be put into a *Rabbity Hypnosis*, under Alice's orders. The target can Defend against orders as normal by *Will*, against a difficult based on the Attack Effort

### Eaton Rabbit, the *Real* Big Bunny Brother

#### **Aspects**

|   ***Kind:*** | ***Aspect:***                                                      |
|--------------:|:-------------------------------------------------------------------|
|   ***Gonzo*** | The real Big Bunny Brother, not a guy on a costume                 |
| ***Trouble*** | Now I'm ONLY the Big Bunny Brother, I'll not lose my life  *again* |
|               | 1911 Rabbit's Easter reminiscence                                  |

#### **Roles**

|                  ***Roles:*** | ***Level***  |
|------------------------------:|:-------------|
| **Big Bunny Brother (Gonzo)** | *Good (+3)*  |
|                       **Spy** | *Good (+3)*  |
|                **Dilettante** | *Fair (+2)*  |
|                      **Face** | *Great (+4)* |

#### **Stunts**

- **The Big Bunny Brother (Gonzo):**
  - **LVL 1: Cartoon knowledge** *Encyclopedia Perambula (Psychology and Cartoons, p 37)*
  - **LVL 2: Cartoon Logic** *I Disbelieve (p 37 - reversed) + Weird Science (p 44, to make any illogical, silly or nonsensical thins to happen. The most nonsensical the action is, the bigger the difficult, but the difficult can go down the bigger VHS is)*
  - **LVL 3: The Big Bunny Brother Friendly Little Bunnies:** *Trust Me + Trust Yourself (p 45) + Practiced - In theory (to call his **Friendly Little Bunnies**)*

#### **Interpretation**

Eaton is a man (rabbit-man?) that basically now lives for a self-build fantasy, so to speak: he's pathologically friendly and happy and positive almost all the time, with clean fluffy blueish-gray fur as glossy and perfumed as possible. Those who know his story, however, understand very fast this silly, naively happy face is a façade to hide a heart that suffered some of the biggest disgraces you can imagine. It was like he died not once, but twice, and anyway try his best to not fall in sadness and get over again.

#### **History**

Eaton was a common guy, like many others, from a small city, with his fair share of adventure (and suffering) on WWII Asia front. When he got back, he was discharged from Army, got into a Business university with Theater minors at his birth-state (Oklahoma), got back his city and built a local General Store... And it would be that...

If he wasn't on the wrong place and wrong time.

It was when a weird kid left him a weird list for spare components for TVs, radios and other electronics. The weird thing was the combination: it was like ALL their appliances had crashed the same time. Aside the address was weird: a farm in the rural zone the city, where in fact should have _nothing_, even more that used _electricity_

*""Well... Business is business."* he said *"As soon the day ends, I'll close the shop and go deliver everything."*

That was the last time people saw him as he was...

Going to the place, he saw it was an old corn farm. The curios thing it was the energy line that got nearby, nothing official, for sure: he knew some of the poorer farmers jury-rigged some energy connections as bootleg... He thought on give a look on everything before does the delivery... Even more because he heard some nasty gossip of abductions and kidnappings on local railroads and bars. Boys and girls normally, but people from all ages and social classes.

As he got nearby the barn, looking for the owner, he started to notice the strong rabbit smell: he had gone for rabbit hunt on rabbit time, he knew how a rabbit smell... But he started to rear weird squeals, that looked as a merge of a rabbit squeal and a human scream of pain.

When got looking around through a window, he saw the most weird thing all his life: what looks like a railroad hobo crossed with a Jackrabbit, big ears over his head replacing the ones that should be aside him, a muzzle with big bucky teeth getting over his rabbity lips, clawed hands holding to what looks like a dentistry chair with a bizarre mishmash of cables, electrodes and weird apparatus. Obviously, whatever was on that chair, was in big pain, he could see remembering his time at Iwo Jima.

But the weirdest thing is there was lots of... *creatures*... nearby, with bizarre forms, that would shock even a fanatic reader from *pulp*: people big as human, but with rabbit-shaped bodies, although risen on hind legs and with general proportions, human enough to put the sanity of those who saw them in check.

It was when he decided... He need to call the sheriff. To the hell if people took this for *combat fatigue* he could got from the *front*: there was someone in suffering and a very weird thing happening... This would bring attention for the Sheriff... Maybe from the Government!

And as always happens this time... A twig broke under his feet, denouncing him.

He almost had no time to get back, when he noticed that, from small holes in the ground like the Japanese soldiers did, lots of those rabbit-men jumped... Some of them still with the wartime *dogtags*! He was only one, and could not evade himself from them.

He was brought to inside the barn... Where he saw the hobo finishing his transformation, with a human-hare hybrid straight *Alice in Wonderland* nearby. The human-hare hybrid give a look for him and there was sadistic happiness on his look by taking another prey. He turned to some of the apparel and started to turn  knobs, while some rabbit-men stripped him for his clothes and bound him to the chair.

He though the hybrid hare would torture him: he heard more than enough about the tortures the Japanese Army applied on those he got as hostage... And how to deal with it!

But nothing would ready him to the March Hare's *Lapinator*

As the first electric bolts and lights started to hit him, he started to scream his name, what he did for the last time, forever. While the Lapinator's eugenic energies started to change himself for some weird reason, voices got inside his mind, luring him to serve Marshal Hare, the March Hare and Lord of Rabbit-kind.

But neither Marshall Hare put into account his willpower...

Nor the new Rabbit-man!

Even more when that willpower grow even stronger thanks the new panicked instincts from an enclosed Rabbit that had no way to find his warren!

With a terrible squeal, his new body woke up, instinct pushing him to fight for life or death! Like a cornered rabbit, he used all his new strength to destroy the bindings and cables that locked him on the Lapinator, running outside, kicking anyone and anything on the way with powerful hind paws, taken those that would be his fellows down with ease...

He still could hear the March Hare scream: *"Get him, you fools! He needs to be indoctrinated!"*

But he didn't survived Pacific front at War to lose his freedom there! His mind, now mingled with rabbity instincts, made him run to the woods, looking for some place to hide himself, avoiding predators, including that weird creature that made him this way and his servants. He found a deactivated mine where he could hide himself and wait, when adrenaline rush ended and he felt that, at least for a while, he was safe: looks like the servants of that March Hare had not got his scent, so they could not find him.

In the next days, he tried to understand what he had become: a powerful both, faster and with a bigger jump than the best Olympic athletes, a speed up mind able to process information on an almost painfully way, and enhanced senses... Painfully enhanced, in fact: when he heard a train passing nearby a small lake he got for some water, he almost gone crazy, the rabbity instincts screaming *PREDATOR!!!!*

Days became weeks became months, eating berries and nuts and wild roots in the forest, drinking water from a river nearby, letting the rain cleaning him, doing his necessities on the mine, or in the woods, marking his territory and showing he would be business against anyone trying to capture him, occasionally being seen by hunters, which became urban legends about giant rabbit-men in the woods. It took two years for him to control and use his new skills without going insane or conceding to the rabbit within, taming and contacting him, so he could get back home. After all this time, now calmer (as long as he new instincts allow them) he got to the farm. He noticed the barn was destroyed...

...and he was took as missing, when he got through the night on his old city, his yellowed _"Missing"_ posters cornered by the announces for a auction for his old shop to pay for his debts...

As far he and everyone he knows knew, he was dead...

He still was lucky enough to find some old rags to dress himself on the streets, before hitting the road...

In meanwhile, he looked around and helped children that he found, while on disguise, on train crossings and roads. It was when he build for himself a history about that Big Bunny Brother, a new _alter-ego_ based on the old _Uncle Wiggly Longears_ books his mom read him when he was a kid. And the legend of the Big Bunny Brother spreaded itself.

He then discovered that the children were talking his stories, and this ended being a nice way to win his bread (his carrot?), when he found someone that knew about this: someone that remembered about the urban legend about *1911 Rabbit's Easter* when children were transformed into rabbit with enchanted Chocolate Eggs. This person helped him to polish his history as *The Big Bunny Brother*, someone to help and cheer for you, like Uncle Wiggly Longears, living adventures and helping kids.

It was time of Bozo, Captain Kangaroo and Howdy Doody, children's show with fantastic characters and cartoons passing positive lessons to kids. This could help the no self-called Eaton Rabbit to have some way to win his bread (carrot?) and looks like someone into a rabbit suit, not like a rabbit-man for real.

It was the time of his life: Eaton Rabbit lived somewhat ten years of lots of happiness as *Big Bunny Brother*, interacting with the bests, like Bozo and Fred Rogers, his show being syndicated all around US, with some other actor doing for the role where he couldn't be.

But obviously this came to an end, on a very pavorous way, when nasty gossip about kids disappearing and becoming aberrant creatures spreaded away on tabloids, which made his show audience fall down like a brick and making the show being cancelled.

Eaton knew the real facts: the March Hare discovered about him and used his fame and his show as a staple to kidnap and lapinate kids, smearing his name in mud! He could allow this to be!

With the end of the show, he only wished good look for the nice lady that helped him with Big Bunny Brother and got away. While hidden again, he started to look upon for the March Hare and the 1911 Rabbit's Easter, and it was when he found lots of things about the Hare, the Mystical Fabergé, the Enchanted Eggs... And the Spirits of the Century...

And things got really worse: looks like that, when he evaded, Marshall Hare captured the lady that helped him and took Big Bunny Brother's character for himself! And used this to capture more and more kids (and nostalgic adults)!

He would not let this be, he would not disappointing people, as by the _Big Bunny Brother Song_:

> *“Big Bunny Brother is here, yay, hooray!*
>
> *To help and cheer up for you, we believe, yay!*
>
> *Through sea, air, mount and earth he will came*
>
> *To help us against all kind of mean people, hooray!*
>
> *We will have no fear or sadness, hooray!*
>
> *Because we know Big Bunny Brother will come, yay, hooray!"*

### **Priscilla Preston, former director of *The Big Bunny Brother Adventures***

#### **Aspects**

|   ***Type:*** | ***Aspect:***                                                                     |
|--------------:|:----------------------------------------------------------------------------------|
|  ***Aspect*** | Someone that could see the strings on her                                         |
| ***Trouble*** | Too much zealous to admit she did wrong things                                    |
|               | "The Big Bunny Brother Adventures  *is really cozy and fun, something is wrong!"* |
|               | Knew too much more that she should (or would) admit                               |

#### **Roles**

|   ***Roles:*** | ***Level*** |
|---------------:|:------------|
|   **Inventor** | *Fair (+2)* |
|      **Brain** | *Good (+3)* |
|     **Leader** | *Good (+3)* |
| **Dilettante** | *Fair (+4)* |

#### **Stunts**

- Majored in College

#### **Interpretation**

Priscilla got really popular as director of ***The Big Bunny Brother Adventures*** show, but her fame got away really fast after some nasty gossip about the show started to spread, specially involving the abduction of part of the human *cast*, and some very shady comments on things related of people that became cartoons (very powerful special effects, for sure). She's really zealous  and proud of her job, but since she was pushed away from it by the new owner, *Peek-a-boo Entertainment*, some red flags were risen on his mind as an alert sign that showed him she was being used on a not nice way!

### Filippo Geppeto, Shadow of Fantasy

#### **Aspects**

|          ***Type:*** | ***Aspect:***                                                               |
|---------------------:|:----------------------------------------------------------------------------|
| ***Shadow (Gonzo)*** | Shadow of Fantasy                                                           |
|        ***Trouble*** | Childish to a fault                                                         |
|                      | Living on a Fantasy World                                                   |
|                      | *"What a beautiful dream... Shame if it became a nightmare!"*               |
|                      | Controlling imagination is controlling narrative, history, people, reality! |

#### **Roles**

|     ***Role:*** | ***Level***   |
|----------------:|:--------------|
|    **Assassin** | *Fair (+2)*   |
|   **Authority** | *Good (+3)*   |
|    **Criminal** | *Good (+3)*   |
| **Manipulator** | *Great (+4)*  |
|   **Scientist** | *Superb (+5)* |
|     **Soldier** | *Fair (+2)*   |

#### **Stunts:**

- Upper Hand
- Mr Underhill: Christian Matteo
  - Peek-a-boo Entertainment owner
  - I don't like when things goes wrong... I don't really like

##### **Gonzo Stunt: Supreme Puppet-master**

- **LVL 1: Supreme improvisation** Majored at College (p 41)
- **LVL 2: The wizard behind the curtains** Eye for Detail (p 40) + Smooth Operator (p42) for trying to find ways to manipulate people
- **LVL 3: There's really strings on you!** *Encyclopedia Perambula (Psychology and Hypnosis, p 37) + Weird Science (Ways to put people under his will) + I Disbelieve (p 33 - reversed, to force other to see nonsensical thing as plausible*

#### **Interpretation**

Filippo Geppeto is one of the most terrible Shadows: for one side he uses his knowledge and knack for fantasy to alienate people and profit on this, on the other side he's a psychopath that lives for destroy other peoples' dreams and lives, ruining them. Nowadays he disguised himself as Christian Matteo, owner of the now famous animation studied and children's shows company *Peek-a-boo Entertainment*. His biggest objective is put people under his power, and the best way he does this is controlling the narrative and gaslight them, playing with his imagination. He will sounds like someone very calm and sensible, but if things goes out of his control, he became a big spoiled brat man-child, with power enough to manipulate people ones against the others. It's wrong say he looks world like a chess table and everyone as his pieces: in fact he looks the world like a particular Fort Apache and everyone as his soldiers.

## Appendix 3 - The Bunny Warrens: A _Shadow of the Century_ Golden Seed

> *“Big Bunny Brother is here, yay, hooray!*
>
> *To help and cheer up for you, we believe, yay!*
>
> *Through sea, air, mount and earth he will came*
>
> *To help us against all kind of mean people, hooray!*
>
> *We will have no fear or sadness, hooray!*
>
> *Because we know Big Bunny Brother will come, yay, hooray!"*

The _Big Bunny Brother Song_, the ***Big Bunny Brother Adventures*** anthem, became a kind of hymn for a generation of US kids in the 1950s and 1960s, as they love the Big Bunny Brother main character: his ideas of inventiveness, ingenuity,  tolerance, respect and collaboration with the community and friends as the best way to solve problems inspired lots of children to joining civic participation and activities, even more when he stimulated the Friendly Little Bunnies (as his fans was known) to build their Big Bunny Brother Warrens (shortened to Bunny Warrens) on their cities and joining civic activities, like helping elders, giving extra school classes for the kids behind, helping the War veterans and so.

Those civic activities examples spreaded all around US, and even on other countries that syndicated ***Big Bunny Brother Adventures***: with the help of responsible adults, the Bunny Leaders, those Friendly Little Bunnies upheld his civic activism, either in parallel or joining other similar groups, like the Scouts or the Rotary Club.

Originally, those _Bunny Warrens_ were not that different from Boy Scouts, but as the original Friendly Little Bunnies grew up, they started to put a better structure, specially helping other local organizations. Their importance was recognized when a group of those Bunnies rescued a group of hikers that lost themselves at the Appalachians: as they knew very well the region, they soon found clues and shown the way for rescue teams, ensuring the hikers got out of trouble safely!

Curiously, although the _Bunny Warren_ stay in contact with themselves, their actions normally are more local than what similar organizations does, like the Hu Dunnit Mystery Clubs. However, there lots of cases of Friendly Bunnies from all the Bunny Warrens of a region joining together for some common objectives, like find a lost person or gather resources for disaster recovery.

Even after the ***Big Bunny Brother Adventures*** cancellation, some Bunny Warren were still meeting points for the Friendly Bunnies, that made their best to uphold the Big Bunny Brother values, by the remembrance of the ***Big Bunny Brother Adventure*** and by the Civic Activities that Big Bunny Brother always stimulated the Little Bunnies to do.

+ ***Agenda:*** support their local communities, pass forward Big Bunny Brother message
+ ***Resources:*** very few, as normally they depends more on the Friendly Bunnies involved resources and occasional patrons, even more that with the ***Big Bunny Brother Adventures*** cancellation they have few contact points, normally only the nearby Bunny Warrens
+ ***Patron:*** although few resources, being a Friendly Bunny can provide friends to risk themselves to help you, as they know you would do the same for them
+ ***Ally:*** Friendly Bunnies normally respect anyone that helps their community, and they will help those trying to help the community
+ ***Contacts:*** normally Warrens doesn't deal with outsiders, more focused on their local communities. However, if the characters show they are Friendly Bunnies (even former ones), they can gather some easy answers from the Friendly Bunnies from the local Warren
+ ***Foil:*** normally Warrens will not mess people that are trying to go for the same objective they try... But if they see character's actions can bring trouble to community, they'll do their best to foil their actions. Believe me: Big Bunny Brother's lessons on foil enemies' actions are really serious!
+ ***Enemies:*** Anyone trying to go against a community with a Bunny Warren will certainly bring not only the Bunnies eyes to them, but other local organizations **AND ALL** Warrens nearby, with the potential to turn the character's life a nightmare!

## Appendix 4 - NOVA Agency: A _Shadow of the Century_ Golden Seed

NOVA (_Network Of Variable Agents_) was founded by the weird and intelligent guy James Marconi, a computer programmer from the MIT AI Labs. Trying to improve and use the best on the best of the new technologies, James had developed a network of people that works fighting weird menaces. _"Ordinarius populus adversus minas aliena"_, _common people fighting weird menaces_, is the NOVA motto.

One of the strategies of NOVA is based on the idea of _composing cells_, their agents are few and apart and sometimes even unaware about each other: they had been enlisted by James itself or his best agent, called _Alpha_. All agents are notified on-demand about their enlistment for any action, some of them even being put _on hold_ for extra demands. They are paid dearly, obviously: all of them, aside their wage, can ask at anytime a favor that James do his best to achieve (and normally he achieve). 

NOVA Agents comply some rules about discretion and non-violence, but even NOVA knows that sometimes shit goes to the fan, so they have some good military operatives as their roster.

There's two big secrets about NOVA:

1. they are a _Golden Seed_, some money appearing into some James' secret accounts on Luxembourg, Jersey Shore and Switzerland.
2. James Marconi, in fact, "doesn't exist": it's a ___very elaborate, but fake, identity___ of one of the last standing Spirits of the Century, ___Nicola Castrogiovanni, Spirit of Optimism___

Nicola was away from US when the Club disbanded, accused of being part of The Red Danger by McCarthy: it was a prophecy made by an old and now deceased acquaintance of him, Mary Alethea, the Club Retainer that helped him to be found by the 19th Century Spirit of Literacy, _Frederick Van Der Merwe, AKA Don Cagliostro_. He was in South Africa, where Frederick had died in peace on his birth-city Bloemfontein, under another fake identity, technically a great-son of Frederick, _Rasmus Van Der Merwe_, but had to evade South Africa thanks the rising danger of Apartheid and his links (as Rasmus) with the _African National Congress_, the underground party that lost his leader in '62 when Mandela was imprisoned and condemned on the Rivonia Trial.

As part of his actions, Nicola learned a trick or two to stay low profile with lots of fake identities: technically, both Nicola Castrogiovanni and Rasmus Van Der Merwe had died on their homes. James Marconi has no link with none of them, except receiving a good money from a fund created by the Van Der Merwes.

As a Spirit-In-Hiding, he learned how to act in the background, and his optimism showed him that things didn't finished yet: the world is dark and dire, for sure, and the Spirits were dead or in hiding, he was not sure which case, but now he found a new way to fight the fight, to provide ways to make people optimist of staying in the good fight. He will dirty his hand, for sure, but he has the moral high ground for him.

He started by creating a new organization, as soon he noticed the lots of things that was happening in the background, the hidden fight for reality that the fall of the Century Club left humanity without champions: the Kroll'X, the Carthage Manuscripts, all those things that were ripping the reality were bad for everyone. For sure, the Century Club was always part of the problem, but that's the perks on deal with reality shattering things, it was in the job description.

And he received a help: he discovered that a scrumptious value in cash was deposited on his secret accounts in Luxembourg, Switzerland, Hong Kong and Jersey Shore. He discovered, investigating, who did it. It was a _Golden Seed_, an inheritance left behind for him by the deceased Spirit of Trade _Mack Silver_, with some of his _Golden Seed_ texts: he had some disagreements with Silver, to be fair, but he chose to live that inheritance in a good way, to honor his and the other Spirits' sacrifices.

He decided to created a new fake identity, and it was somewhat easy to do this: he undergone a new identity as _James Marconi_, from Wamega, Kansas. He just joined the MIT for some philosophy grad and joined the AI Labs as part of a minor on computer programming, and stayed. He also is known as the name behind an apocryphal zine about _"The Century Club, His Legend and Spirits"_, that put him dangerously on radar. He was ignored just because it was said that it was totally without sources, so a kind of conspiracy theory.

And, under all those skins, he started NOVA.

He bought some years after his first personal computers, a Commodore PET and a Tandy Radio Shack TRS-80 Model I. Also he bought a 300-baud modem and used it to join the NFSnet (via MIT AI Labs) and some BBSes, contacting people, separating wheat from the chaff, and discovering people that had passed by problems, weird ones: a guy that discovered his soon-to-be bride was an insectoid invader; a girl that fought her way against a space and time travelling cyber-hunter; that English reporter that traveled in time and space with a two-hearted guy with a garish big scarf that changed himself when facing death (her words). 

And she found, or better, was found by _Alpha_:

_Alpha_ was a incredible hacker, the _creme-de-la-creme_. Also, was an investigator, and remembered the times the Century Club saved the world from enemies like Gorilla Khan, The March Hare, Jared Brain, Lemont Fitzlefebvre, and Doctor Methuselah. And she was sassy enough to reveal she followed James', in fact, _Nicola_'s breadcrumbs all around the world and time. She assured him it was not that easy, and that even with his apocryphal zine being a somewhat big blunder he should not be worried, she discovered all his timeline, including where and how he "died" from an identity from another, backtracking things to the first time he found Don Cagliostro in the _McNash & Sullivan Circus and Shows_, when he became the little clown _Lil'Nico_ and stopped a rampant lion just _by talking_.

_Alpha_ had a big consideration for the Spirits: her mother was saved by them in the (now washed aside, forgotten and part of legends) _Dinocalypse_ event, and she had studied in the _Cross Engineering School_, where she discovered (and hidden for herself that secret) that the school was commanded by the one and only Spirit of Ingenuity Sally Cross _nee_ Slick.

_Alpha_ then exchanged some emails and they came with the project called NOVA, a possible organization for dealing with the weird, but with common people, to be one of the barriers to contain outside invasion from hostile realities that was part of the Century Club job until they were disbanded.

Then, both _Alpha_ and James ran around the world, even behind the Iron Curtain, trying to find people that could help. With time and successes, NOVA made a name: good enough to be risen to a bigger arena, but still low enough to make them stay low profile.

However, more and more eyes are looking for NOVA, and some of them either _Alpha_ and James, or better, _Nicola_ wanted to push away from them.

### Rumors

+ _Alpha_ is in fact a Shadow of the Century: no one knows if she's using Nicola for a hidden and nasty agenda or she's looking for redemption, but looks like she's not someone that is not accomplished to dirty her own hands;
+ James Marconi is NOT Nicola Castrogiovanni: the real Nicola had fell in the hunting for the Centurions, and James is someone that knows his story enough to take his identities.
+ James/Nicola is on an undercover mission to recover the lost glory of the Century Club: he is looking for some of the artifacts that were lost during the Club disbanding, and on all information about the Centurions-In-Ride. Some even suspect he's looking them for a last hurrah that was prophesied by Mary Alethea
+ Some says that NOVA is a kind of selection for the next Centurions
+ NOVA agents, the most experienced ones, had developed by themselves Centurion-like capabilities, and even bigger: their contact with VHS and Methuselah Fragments had changed them in weird ways
+ Although Nicola had been in MIT AI Labs, nowadays he can be found hidden on a circus nearby Omaha, Nebraska. Some says that he can be found also on a small property in Baraboo, Wisconsin, or on any city were the old circuses made their Winter HQs. Some says that his real _sanctum_ is on the old _McNash and Sullivan Circus and Shows_' Winter HQ at Newark, NJ
+ _Alpha_ isn't a human being at all, but one Turing Realistic Artificial Intelligence, programmed by Nicola or downloaded from outside our reality: the main reason she found Nicola is that she knows he's a key part on a future she wants to guide people to
+ Nicola is now on a blacklist: he was seduced by the darkness and became a Shadow, and NOVA is just a pawn he uses to achieve his plans. Something in the past made he grew bitter and cynical and this made him go rogue even before the Century Club banishment;

### NOVA as a Golden Seed

+ ___Agenda:___ to be one of the new barriers protecting humankind against extra-dimensional hazards and invaders, and to deal with weird menaces created by rogue agents
+ ___Resources:___ not too big and very scattered, but this can be seen as an advantage: you can almost certainly discover a NOVA agent and resources somewhere, given time
+ ___Patron:___ Being part of NOVA network can be a great way for those who like to be on the edge, because lots of time NOVA deals with the Gonzo and the weird, and is somewhat safe, as they like to have agents on places where they can help and are very sensible with classified intel. And there's The Favor you can ask and NOVA will do its best to get it solved
+ ___Ally:___ As totally scattered, maybe NOVA is not a good ally, as finding a NOVA agent is really complicated, even by those inside the Agency, beside James and _Alpha_. However, once you get an ally inside, you have access on, given time, lots of intel, resources and equipment
+ ___Contact:___ Same as Ally, NOVA is not exactly the best contact for those in a hurry, although if you can wait it's an incredible information source
+ ___Foil:___ NOVA isn't that jealous on their mission to think it as exclusive. However, they can be very touchy if you just try to mess on their missions. Normally, they put things on the shelf until the main problem is solved... And then they go as full as they can to hit you hard if they think you messed things
+ ___Enemy:___ if you think NOVA can be taken as a bunch of morons based on their loose structure, think it again: NOVA agents are trained to notice weird things that can demand their attention. In fact, if a NOVA agent notice YOU, and you are doing naughty things, NOVA will hit you so hard that you'll want that you'll want they didn't notice you.

### Face: Alpha

Normally people can't see Alpha if she doesn't want, and even so she pass as a very common punk girl with electrical green hair: just in the BBS and NSFnet (the predecessor of Internet) people knows about her. 

+ ___Grll Power Uber-Hacker; She knows too much about the Century Club; More friends (and enemies) that she knows___
  + ___Great (+4)___ Hacker; ___Good (+3)___ Saboteur, Detective; ___Fair (+2)___ Dilettante
  + Nice Computer

### Face: "James Marconi" (Nicola Castrogiovanni)

No one could associate the somewhat chubby and bald headed computer store owner and former MIT AI Labs employee James Marconi with _Lil'Nico_, the Vaudeville Angel from 1910s Circus and Vaudeville called Nicola Castrogiovanni.

+ _**Spirit of Optimism** (Gonzo, Spirit)_; **"Everything will be fine in the end!" - _Too much naive_**; _**NOVA will be the New Century Club; Just do what you can, don't whim around; Maybe it's the time to get back**_
  + _**Superb (+5)** Pollyanna; **Great (+4)** Brain, Leader; **Good (+3)** Hacker, Dilettante; **Fair (+2)** Detective_
  + **Mr Underhill:** his actual identity James Marconi; 
	+ _Computer Scientist on a post-Grad at MIT AI Labs_; 
	+ _A backstory that doesn't survive a DMV query_
  + **Mr Underhill:** Rasmus Van Der Merwe
	+ _Social Scientist from Bloemfontein, South Africa_
	+ _"Nephew" of one of African National Congress founders_
  + _**Good Enough Game (Gonzo):**_
    + LVL 1: I can do this (_Majored at College_ from Dilettante) 
    + LVL 2: _Pretty Please!_ (_Disarming Charm_ from Dilettante + _Open Book_ from Face)
    + LVL 3: We can do this, Together (_Plan B_ + _Trust Me_ + _Trust Yourself_)
	
## Appendix 5 - Adventure suggested playlist

+ 80s Children's music
+ Martika - Toy Soldiers
+ Torch - Soft Cell
+ Might Wings - Cheap Trick
+ Only When You Leave - Spandau Ballet
+ Footloose - Kenny Rogers
+ Island in the Stream - Dolly Parton & Kenny Rogers
+ Jive Talkin' - Bee Gees
+ Head over Heels - Tears For Fears
+ Walk of Life - Dire Straits
+ Mickey Mouse Club March - The Mickey Mouse Club
+ Sweet Home Alabama - Lyndr Skynr
+ The Goonies' Good Enough - Cindy Lauper
+ I like Chopin - Gazebo
+ Suzie Q - Creedance Clearwater Revival

<!--  LocalWords:  Filippo Geppeto Underhill Matteo yay Bettina Suzan
 -->
<!--  LocalWords:  Lil NOVA's Ordinarius populus adversus minas intel
 -->
<!--  LocalWords:  aliena HUAC Doody pre Hu Dunnit Mouseketeers NYU
 -->
<!--  LocalWords:  bulleye's th uber ARPANet MILNet NSFNet UUCP HX FP
 -->
<!--  LocalWords:  FidoNET Osbourne Epsons TRS avant garde Parton Neu
 -->
<!--  LocalWords:  Kraftwerk Spandau conspiration GURPS FNORD Gideons
 -->
<!--  LocalWords:  Michaels prothetics Shriners meds whatcha Hamon Un
 -->
<!--  LocalWords:  Khe Sahn dogtags ish thornbushes Mitchells obuses
 -->
<!--  LocalWords:  pumbling sweared Camaro backdoor's felps Kissifur
 -->
<!--  LocalWords:  Popples PTSD hide'n'seek Torned amish rabbity NPCs
 -->
<!--  LocalWords:  gleeing Lapinator Oryctolagus Lapination Fabergé
 -->
<!--  LocalWords:  Shipshewana furried Eator lapinated backstab Talia
 -->
<!--  LocalWords:  Geppeto's Castrogiovanni clownie Sheeran Cadmus de
 -->
<!--  LocalWords:  Mairead MacRaigt Engeneering unlapin Phyrric LVL
 -->
<!--  LocalWords:  Kroll'X teached unlapinated Lemont Fitzlefebvre
 -->
<!--  LocalWords:  roadmap Perambula mindly Hunny Iwo Jima Longears
 -->
<!--  LocalWords:  Lapinator's spreaded pavorous lapinate Alethea Der
 -->
<!--  LocalWords:  Merwe Cagliostro Rasmus Rivonia Merwes Wamega HQs
 -->
<!--  LocalWords:  Tandy NFSnet insectoid cyber hearted McNash NSFnet
 -->
<!--  LocalWords:  Lil'Nico Dinocalypse Baraboo Grll DMV Martika
 -->
<!--  LocalWords:  Talkin Lyndr Skynr Goonies Lauper Suzie Creedance
 -->
<!--  LocalWords:  Clearwater
 -->
