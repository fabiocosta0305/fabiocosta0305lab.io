# Façanhas dos Papéis em Shadow of the Century

## O Brigão

+Perícias: Atletismo, Lutar, Provocar, Vontade

- **Repete se for homem\!** - Quando se defender contra um Ataque focado em medo ou intimidação e for bem-sucedido com estilo, você pode provocar um dano mental direto no alvo de 2 tensões em vez de obter o impulso
- **Manda mais que tá pouco\!** - Hordas que ataquem você não recebem bônus de Trabalho em Equipe, mas ainda o recebem quando se defenderem de seus ataques
- **Quebre a perna dele** – Quando atacando alguém que já tenha Consequências, você recebe +1 no Ataque para cada Consequência que o alvo tiver
- **Fale para eles que eu vou os achar:** Uma vez por sessão, quando um capanga ou tenente conceder ou for tirado de jogo, você pode escolher deixar um deles ir. Na próxima cena que o incluir ou a um dos seus associados, você ignora o primeiro Ataque Físico deles que provocar dano
- **Desempate:** Quando defendendo-se de um Ataque e empatar, o atacante não recebe impulsos
- **Força Irresistível:** +2 ao remover Aspectos usando sua força física

## O Cérebro

+ **Perícias:** Computadores, Engenhocas, Intuição, Conhecimentos

- **Diagnóstico Clínico:** Quando você conhecer ao menos um dos Aspectos do NPC, você pode usar *Intuição* no lugar de *Persuasão* para obter informações sobre ele, mas você parecerá rude ou forçoso, e não será bem-visto por ele no futuro
- **Enciclopédia Perambular:** no *início do episódio*, role *Conhecimentos*, dificuldade *Medíocre (+0)*. Cada duas tensões permitem que você coloque uma Invocação Gratuita em uma Área de Conhecimento que você nomeie. Quando essa área de conhecimento puder ser usada, use a Invocação para conseguir um Sucesso Automático em
 um teste relacionado
- **Eu não acredito nisso\!** - quando apresentado a algo ou alguém que desafia a lógica ou bom-senso, role *Conhecimentos* no lugar de *Vontade* para resistir aos ataques mentais dele, com +1 no rolamento
- **Empatia com Máquinas:** se ficar por alguns instantes estudando alguma coisa tecnológica que não conheça, recebe uma Invocação Gratuita em um Aspecto que ela tenha ou em um Aspecto novo (se ela não tiver nenhum)
- **Milagreiro:** sempre que bem-sucedido com estilo em um rolamento de *Conhecimento* para recuperar-se de uma Consequência Suave física, você pode optar por limpar automaticamente a Consequência no lugar de obter um impulso no processo
- **Tradutor Universal:** você não precisa rolar para falar e ler qualquer idioma que seja razoável de conhecer. Caso contrário, você rola contra dificuldade *Ótima (+4)*

## O Detetive

+ Perícias: Percepção, Contatos, Intuição, Persuasão

- **Um palpite correto:** Você conhece as ruas e seus pés-rapados. Durante uma montagem, você ganha uma invocação extra em qualquer Aspecto gerado quando usar Contatos para recorrer a tais contatos. Quando Montando uma Disputa, você pode sacrificar essa invocação extra para somar uma vitória adicional
- **Olho para os detalhes:** quando examinar a cena de um crime para Superar por pistas ou evidências, você é automaticamente bem-sucedido se seu nível de *Percepção* for melhor que o da sua oposição
- **Na mente deles:** Pode usar *Intuição* para tentar prever os próximos passos de alguém que tenha encontrado previamente
- **Observador Sagaz:** Ao ver uma pessoa pela primeira vez você pode usar *Percepção* ou *Intuição* para tentar descobrir algum Aspecto relacionado a maneirismos, vestimentas, postura ou outros sinais visíveis do alvo sem gastar nenhuma ação
- **Vamos fazer um trato:** recebe +2 ao obter informação por *Persuasão*, se puder genuinamente oferecer alguma coisa que o outro lado deseja
- **O pessoal do laboratório:** Você tem um amigo no laboratório forense que pode fazer seus pedidos passarem na frente na fila, portanto você pode utilizar *Contatos* no lugar de *Conhecimentos* para obter pistas forenses. Nomeie tal contato quando escolher essa façanha.

## O Diletante

+ Perícias: Computadores, Direção, Persuasão, Recursos

- **Charme Acachapante:** *Uma vez por episódio*, você pode impedir que o Narrador invoque um Aspecto de um NPC que seja contrário a você
- **Tudo tem um preço:** Caso falhe em um teste de *Persuasão* contra um NPC, você pode usar *Recursos* no lugar de *Persuasão* contra o mesmo NPC e dificuldade (se for ativa, mesmo resultado do rolamento anterior). Entretanto, mesmo que você tenha um sucesso com estilo, essa ação vai ter um custo. Os demais PCs podem usar essa Façanha também.
- **Molhando as mãos certas:** Durante uma Montagem, quando usando *Recursos* para subornar um alvo e for bem-sucedido, você obtêm uma invocação adicional em qualquer Aspecto gerado. Se Montando uma Disputa, você pode sacrificar a invocação adicional para obter uma vitória adicional.
- **Astolfo, para casa:** você tem um chofer atencioso, competente e leal, que conta como um *Capanga* Bom (+3) com um Conceito, uma Dificuldade, além de um sedan de luxo (seu, claro) e, como sempre, uma sagacidade exacerbada. Uma vez por episódio, gaste 1PD para invocar esse chofer para pegar você e seus companheiros, desde que seja algo explicável ter ele por perto.
- **Formado na Faculdade:** *Uma vez por episódio*, você pode escolher uma perícia que não esteja associada aos seus papéis. Essa perícia passa a ser considerada *Razoável (+2)* até o final da cena
- **Computador Sofisticado:** Qualquer um que use seu *Computador* ao realizar testes de *Computadores* soma +2 ao rolamento se você perguntar o que ele está fazendo.

## O Espião

+ Perícias: Roubo, Contatos, Recursos, Furtividade

- **Milhões de Faces:** você pode deixar uma cena desde que você consiga descrever de maneira plausível a fuga e você não esteja sendo ativamente observado. Em uma cena no futuro ou mais adiante na mesma cena, você pode gastar um Ponto de Destino para substituir um NPC, como um cultista ou cientista;
- **Amigos do Carteado:** +2 ao Criar Vantagens com *Contatos* para lidar com os altos níveis da sociedade
- **Método de Atuação:** Quando passando-se por outra pessoa, substitua seu *Conceito* por um apropriado referindo-se à pessoa substituída. Quando realizando ações que possam ser justificadas por esse Conceito, ao invés de usar suas perícias você pode rolar usando um nível *Bom (+3)*
- **Senhor Monteiro:** Você possui uma identidade alternativa que criou após anos de trabalho. Ao comprar essa Façanha, defina um nome, um *Conceito* e uma *Dificuldade* para essa identidade. Uma vez por episódio você pode invocar um desses Aspectos sem pagar Pontos de Destino, enquanto estiver passando-se por essa identidade. Seus inimigos nada sabem sobre ela, ao menos de início;
- **Mexido, não batido:** *Uma vez por episódio*, você pode usar seu Estresse mental para absorver um Ataque físico;
- **Estou com a Vantagem:** *Uma vez por episódio*, enquanto estiver envolvido em apostas, você pode trocar seu resultado com o de um outro participante da mesma.

## O Guerreiro

+ Perícias: Lutar, Provocar, Atirar, Vontade

- **Sempre traga um reserva:** uma vez por episódio, você pode revelar que tinha uma arma escondida com você, mesmo que pareça impossível para você a ter no momento
- **Endurecido pela batalha:** você recebe *Armadura:2* contra ataques contundentes, como socos e bastões
- **Venha Brincar:** *Uma vez por episódio*, ao desafiar um NPC para uma luta mano-a-mano, ele é obrigado a aceitar e você recebe uma Invocação Gratuíta em cima dele no Conflito
- **Sem misericórdia:** Em uma luta mano-a-mano, quando você está armado e o oponente não, aumente seu nível de *Arma* em 2 para tais ataques
- **Esgrima Espalhafatosa:** quando usando uma lâmina leve, como uma Rapieira ou sabre, você recebe +1 para Criar Vantagens com *Lutar*. Você pode invocar os Aspectos gerados por essa Façanha para receber +3, ao invés do +2 normal
- **Caminho do Guerreiro:** *uma vez por episódio*, quando você tiver um pouco de paz e quietude, você pode tentar meditar, focar seu *chi* ou coisa assim. Role sua *Vontade* contra uma oposição igual ao valor da sua caixa de Estresse maior. Se você for bem sucedido, você recebe uma segunda trilha de Estresse físico, que tem caixas igual à tensão obtida. Ao fm da próxima cena, essa trilha é limpa e desaparece.

## O *Hacker*

+ Perícias: Roubo, Computadores, Contatos, Engenhocas

- **Acesso obtido:** quando usando uma ação para tentar invadir um sistema, você é automaticamente bem-sucedido se o seu nível de *Computadores* for melhor que a oposição;
- **Chapa nas BBSes:** uma vez por episódio, você pode declarar que tem um contato online que pode oferecer informações úteis. Nomeie esse contato por seu “apelido” na BBS, como um Aspecto de Situação, com uma Invocação Gratuita, que permanece em jogo até o final do episódio;
- **Contabilidade Criativa:** Pode usar *Computadores* no lugar de *Recursos* para Criar Vantagens relacionadas a finanças;
- **Fantasma da Máquina:** Quando você usa *Computadores* para invadir um computador e for bem-sucedido, suas atividades não podem ser rastreadas; em caso de sucesso com estilo, você pode sacrificar o impulso para deixar pistas que apontem a outro alvo;
- **Sei o que precisamos:** +2 ao Superar com *Roubo* quando tentar roubar computadores ou componentes para um
- **Luz Verde e Vermelha:** Você é versado na invasão de sistemas de controle público, como o sistema de trânsito. Sempre que você tiver invadido sistemas públicos e usar *Computadores* para Criar Vantagens, você pode invocar os Aspectos resultantes para forçar um alvo dentro da cidade a falhar automaticamente em qualquer tentativa
 de deslocar-se na cidade.

## O Inventor

+ Perícias: Computadores, Direção, Engenhocas, Conhecimentos

- **Basta Fita Adesiva:** você tem um condão para canibalizar coisas mundanas para criar coisas fantásticas. Quando fazendo gambiarras fora de um laboratório ou oficina, você recebe +2 no rolamento de *Engenhocas* se você puder mencionar ao menos três coisas tecnológicas que possam ser vagamente relevantes ao que você quer fazer – como usar um Pense Bem, um Walkman e um Teddy Ruxpin para construir um dispositivo de rastreamento.
- **Agora ele voa\!:** se você tiver tempo e ferramentas, você pode modificar um veículo e adicionar uma nova forma de locomoção ao mesmo, tornando um carro em um carro voador, ou um avião em um submarino. De ao veículo um *Conceito* que você pode invocar para mudar o modo de locomoção do veículo. O narrador definirá uma *Dificuldade*
- **Possível… Em teoria…:** Quando comprar essa Façanhas, escolha um dos seus Aspectos. Se você falhar em um teste de *Computadores*, *Engenhocas* *ou Conhecimento*, e você usar um desses Aspectos para um novo rolamento, você recebe +2 ao seu resultado final
- **Ciências Sociais:** quando falando com alguém que é muito conhecido em uma determinada científica, você pode usar *Conhecimentos* no lugar de *Intuição* para descobrir algum Aspecto do mesmo
- **Teoria na Prática:** Você não teme sujar suas mãos. Durante uma Montagem, quando usando *Engenhocas* para criar ou melhorar algo e for bem-sucedido, você obtém uma invocação adicional em qualquer Aspecto gerado. Se Montando uma Disputa, você pode sacrificar a invocação adicional para obter uma vitória adicional.
- **Ciência Estranha:** *Uma vez por episódio*, pode usar *Engenhocas* para criar uma invenção. Ela terá *Conceito* (você define), *Dificuldade* (o Narrador Define) e algumas Façanhas. A dificuldade é o dobro de Façanhas colocadas. Em caso de Falha ou Empate, você ainda cria o que você queria, mas o Narrador recebe a Invocação Gratuita

## O Ladrão

+ Perícias: Percepção, Roubo, Recursos, Furtividade

- **Bater carteira:** No seu turno, você pode usar 1 Ponto de Destino para rolar Furtividade para roubar um pequeno objeto ou qualquer outra forma de prestidigitação sem precisar gastar sua ação;
- **Itens Essenciais:** você mantêm uma mochila com uma série de coisas essenciais para caso precise “dar um perdido”. Uma vez por episódio, diga ao Narrador *“Vou pegar meus Itens Essenciais”*, para obter um aspecto *Itens Essenciais* com três invocações gratuítas, desde que seja plausível o fazer;
- **Mercadoria Quente:** você conhece alguém que sabe comprar e repassar mercadorias roubadas. Vocês não são amigos, mas você confia nele o bastante e vice-versa. +2 em *Recursos* para repassar ou adquirir coisas roubadas, desde que aceite um custo menor adicional e independente do resultado do rolamento;
- **Algum problema, oficial?:** Quando confrontado contra um agente da lei, você pode gastar 1 Ponto de Destino para declarar que ele é corrupto. Você pode então fazer com que ele saia do seu caminho, com um rolamento de *Superar* por *Recursos* com dificuldade igual ao nível de *Roubo* ou *Vontade* do mesmo, o que for maior;
 - **“Não” passar… Sério?:** Isso na prática é uma sugestão, né? Durante uma montagem, você ganha uma invocação extra em qualquer Aspecto gerado quando usar *Furtividade* ou *Roubo* para entrar em um local onde você não deveria estar. Quando Montando uma Disputa, você pode sacrificar essa invocação extra para somar uma vitória adicional
 - A trilha ainda tá fresca: Quando perseguir alguém a pé, você já começa a Disputa com uma Vitória

## O Líder

+ Perícias: Intuição, Conhecimentos, Persuasão, Recursos

 - **O plano vai se consolidando:** se todos os personagens forem bem-sucedido durante uma montagem, os Aspectos gerados dão bônus de +3 no lugar de +2 quando invocados para bônus;
 - **Líder pelo Exemplo:** Escolha um Aliado ao fim da cena: se ele usar a mesma perícia para fazer a mesma coisa na próxima rodada, ele recebe +2 no rolamento
 - **Plano B:** Após uma montagem que gere qualquer número de Aspectos, você pode reescrever alguns deles, mantendo suas Invocações Gratuitas. Você pode fazer isso com um número de Aspectos igual seu nível de *Intuição* por episódio
 - **Pela Grana:** Você não teme sujar suas mãos. Durante uma Montagem, quando usando *Engenhocas* para melhorar algo e for bem-sucedido, você obtém uma invocação adicional em qualquer Aspecto gerado. Se Montando uma Disputa, você pode sacrificar a invocação adicional para obter uma vitória adicional.
 - **Confia em mim:** Quando usar sua ação para convencer outros PCs a fazer algo perigoso e eles fizerem, eles recebem +2 nos seus rolamentos;
 - **Confie em você mesmo:** uma vez por episódio, se você puder chamar um PC a parte e lhe dar um discurso motivacional, ele recebe uma invocação gratuita em um dos seus Aspectos, a sua escolha;

## O *Ninja*

+ Perícias: Atletismo, Percepção, Lutar, Furtividade

- **Arma Ancestral:** Você possui uma arma de artes marciais (katana, ninja-to, nunchaku…) que é extremamente bem-feita e parte vital de sua identidade. Dê um Aspecto para tal arma. No se turno você pode invocar esse Aspecto para provocar um dano físico direto em um alvo igual ao seu nível em *Lutar*. Isso ainda vai contar como uma ação,
 mas você não precisa rolar os dados.
- **Luta às Cegas:** quando lutando no mano a mano com alguém, seu adversário não pode invocar ou forçar aspectos relacionados à ausência de luz contra você;
- **Vindo das Sombras:** Uma vez por rodada, quando atacando um extra ou horda de inimigos que não está ciente de sua presença, você pode os derrotar sem precisar rolar dados
 - **Armas Improvisadas: **quando usando armas improvisadas, você recebe +2 ao *Criar Vantagens *ao *Lutar*
 - **Como um gato:** Você sempre cai de pé, sempre sendo bem-sucedido em qualquer ação de *Atletismo* para cair em segurança cuja a oposição for menor que seu nível de *Atletismo*
- De repente um *ninja*: uma vez por cena, você pode desaparecer da mesma, desde que você consiga descrever de maneira plausível a fuga e você não esteja sendo ativamente observado. Para evitar riscos com isso, você deve pagar 1 Ponto de Destino. Você pode reaparecer em cena em um local completamente diferente em seguida. Se isso lhe der vantagem tática, você deve pagar 1 Ponto de Destino também

## O Piloto

+ Perícias: Percepção, Direção, Provocar, Atirar

- **Viciado em Velocidade:** quando reparando ou modificando um veículo, você pode usar *Direção* no lugar de *Engenhocas*
- **Apenas uma mão no volante:** quabdo operando um veículo e atirando ao mesmo tempo, você pode gastar um Ponto de Destino para rolar Atirar sem gastar sua ação
- **Pagar de Gostoso:** Quando pilotando um veículo, você recebe +2 para *Provocar* o piloto de outro veículo
- **Modificações Especiais:** Durante uma Montagem, se você estiver usando *Engenhocas* para melhorar seu carro, você obtem uma invocação adicional em qualquer Aspecto gerado. Se Montando uma Disputa, você pode sacrificar a invocação adicional para obter uma vitória adicional.
- **Carango Maneiro:** Você possui um carro especiai. Dê a ele um *Conceito* e uma *Dificuldade*. Você e qualquer outra pessoa recebe +1 em todos os testes para o operar. *Uma Vez Por Episódio*, pague 1 Ponto de Destino para adicionar um terceiro Aspecto no mesmo com uma Invocação Gratuíta, representando alguma capacidade do mesmo até então escondida. Esse Aspecto desaparece ao final da Cena. 
- **Por pouco:** *Uma vez por episódio*, quando pilotando um veículo por *Direção*, você pode marcar uma caixa de Estresse Mental para ganhar um bônus em Direção igual ao valor daquela caixa de estresse. Essa caixa de estresse não é limpa até o final do episódio

## O Policial

+ Perícias: Atletismo, Direção, Atirar, Vontade

- **Mãos na cabeça\!:** Quando colocar alguém na mira, você pode o intimidar usando *Atirar* ao invés de *Provocar*
- **Giroflex:** Quando dirigindo um carro com sirenes e giroflex, você recebe +2 para *Superar* usando *Direção* em ambientes urbanos;
- **Oficial em Perseguição:** Quando perseguir alguém a pé, você já começa a Disputa com uma Vitória
- **Fala isso pro juiz:** Quando alguém tentar o intimidar e você for bem-sucedido com estilo na sua Defesa, você pode provocar um dano mental direto de 2 tensões no Atacante ao invés de obter o impulso;
- **Informante:** Uma vez por episódio, você pode declarar que conhece alguém (normalmente um bandido pé-de-chinelo) que pode ter informação útil. Defina o apelido desse informante: ele entra como um Aspecto em jogo com uma Invocação Gratuíta e permanece em jogo até o fim do episódio;
- **Servir e Proteger:** Quando um aliado próximo sofrer um Ataque físico, você pode utilizar seu Estresse para auxiliar a absorver o dano;

## O Sabotador

+ Perícias: Roubo, Engenhocas, Conhecimentos, Furtividade

- **Sempre é o fio vermelho:** pode desarmar qualquer explosivo sem precisar rolar dados desde que seu nível em *Engenhocas* seja menor que a oposição
- **Sabotador experiente:** você é familiar com todo tipo de bombas, explosivos, armadilhas e outras coisas que Anarquistas usam contra O Chefe. Você pode usar *Conhecimentos* como uma ação de Superar para descobrir mais sobre o alvo, com dificuldade dependendo do quão conhecido é o criador do dispositivo explosivo
- **Cortando o tubo do fluído de freio:** Quando você usa *Engenhocas* para colocar um Aspecto em um Veículo, você pode invocar aquele Aspecto para forçar o operador a falhar em um teste de *Direção*, mesmo que ele esteja em algum outro local;
- **Trilha de Evidências:** sempre que você se envolver em uma ação de sabotagem, você pode gastar 1 Ponto de Destino para criar uma Trilha de Evidências apontando para alguma outra pessoa;
- **Já lidei com isso:** uma vez por episódio, você pode agir contra um NPC enquanto não estiver em cena. Entretanto, ele tem que estar perto de alguma estrutura, veículo ou máquina que você tenha tido acesso prévio durante o episódio. Se você descreveu que preparou a situação previamente em uma montagem (como colocando um carro-bomba
 ou sabotando um detector de metais), você recebe +2 no rolamento;
- **Nivelar:** Pode usar *Conhecimentos* no lugar de outras perícias para dobrar alguém da alta sociedade. Funciona apenas uma vez contra uma pessoa específica e você fica marcado como alguém rude

## O Soldado

+ Perícias: Atletismo, Lutar, Atirar, Vontade

- **Botas no chão:** quando correr, pular, ou escalar, você é bem-sucedido sem precisar rolar, desde que seu nível em *Atletismo* seja maior que o da oposição;
- **Nunca deixe ninguém para trás:** se um aliado seu que esteja próximo a você receber uma consequência, você pode reduzir a severidade da mesma recebendo você mesmo uma consequência. O número de passos que a consequência será reduzida dependerá da consequência sofrida por você: uma para suave, duas para moderada, três para severa. Se a consequência for reduzida para abaixo de Suave, ela não é sofrida;
- **Abaixem-se:** ao usar uma arma totalmente automática, você pode usar *Atirar* para defender a você ou a seus aliados, mesmo que não haja aspectos de situação que permitam tal manobra. Em caso de falha, o Estresse é resolvido normalmente;
- **A missão vem primeiro:** você pode invocar suas Consequências como se fossem Aspectos seus
- **Essa é minha arma:** Você possui uma arma especial que valoriza acima das demais. Dê-lhe um *Conceito* e uma *Dificuldade.* Uma vez por cena, você pode invocar um desses Aspectos sem gastar Pontos de Destino;
- **Olha fulminante:** Quando sofrer Ataques mentais baseados em medo ou intimidação, você pode usar *Lutar* ao invés de *Vontade* para se defender;

## O Rosto

+ Perícias: Contatos, Intuição, Persuasão, Provocar

- **Uma história passável:** quando um NPC tentar descobrir algum Aspecto seu, você pode se defender usando *Persuasão* no lugar de *Vontade*. Se bem-sucedido com estilo, você pode, no lugar do impulso, criar um Aspecto para ele “descobrir” sobre você, com uma Invocação Gratuita
- **Livro Aberto:** Quando falando com alguém, você pode aprender um dos aspectos dele sem precisar de rolamentos, desde que revele um Aspecto seu
- **Parece verdade:** quando tentar Superar alguém em uma tentativa de passar a lábia, você é automaticamente bem-sucedido se sua *Persuasão* for melhor que a oposição
- **Na Maciota:** Durante uma Montagem, quando usando *Persuasão* para convencer os outros e for bem-sucedido, você obtém uma invocação adicional em qualquer Aspecto gerado. Se Montando uma Disputa, você pode sacrificar a invocação adicional para obter uma vitória adicional.
 - **Mexendo no Vespeiro:** quando usar *Intuição* para descobrir um Aspecto pertencente a alguém, você pode sacrificar uma Invocação Gratuita para descobrir um segundo Aspecto dessa pessoa.
 - **Vamos conversar, gente:** *uma vez por episódio*, quando alguém tentar lhe atacar fisicamente, você pode rolar *Persuasão* como sua defesa, se for capaz de dizer algo que as façam parar e as pessoas ao seu redor estiverem a ouvindo. Caso seja bem-sucedido, na prática você encerra aquele conflito, mas isso não impede o oponente de iniciar outro conflito, ainda que você ganhe tempo para seu grupo tentar algo

