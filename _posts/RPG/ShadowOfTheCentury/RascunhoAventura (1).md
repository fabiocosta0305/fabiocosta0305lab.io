Hey, remember when I posted a video from a bizarre group called _Dschinghis Khan_ as something that made me think on _Shadow of the Century_?

Okay, I thought on the adventure premise :

1984. Eurovision Music Festival try an expansion overseas thanks by a new sponsor called Musiflow. Aside the east Europe nations, there will be a place for each into Americas (Latin and North, one for each), Eastern Europe, Middle Eaast (excluding Israel, that is already part of Eurovision), Asia and Oceania.

This is the official version

The "Real version" is : Musiflow is owned by a Methuselah Fragment and his #1 band is leaded by another Methuselah Fragment. Both had developed a brainwashing song that, via mathemagics, goes even deeper into the mind of people. This music was already played to some people, including some Spirits of the Century in hiding and great musicians like Vangelis, Jean Michel Jarre and the Kraftwerk crew, all them recognizing their insidious potential.

The PCs are one of the bands in the international selections that are contacted by someone that knows about the song. They'll be asked to go into Eurovision and play a music that could act as a "counter-spell" against that music. However, they'll have to take the song from their heart, while putting all the techniques they could to win, but learning from some of older Eurovision winners, like ABBA and Dschinghis Khan, and also working with some of those and with the best musicians.

Montage scenes could be with any music from ABBA, Dschinghis Khan, Eurythmics, Treats for Fears, Bangles, you name it, involving them practing, composing and creating the choreography of their music.

They'll need to avoid or fight some mooks from Musiflow and so, till they get into the Eurovision finals, at Zurich, just some blocks from the Musiflow HQs. Musiflow band will play just before the players, and they'll have to fight the 
mathemagical waves via their music, or Methuselah will return to his full power, to take power and creating an immortal theocracy, where he'll be God.

## Musiflow Music and Shows

+ Investiment: Music, Entertainment, Sound Systems
+ Owner: Marcus M. Townsend
+ HQs: Zurich (Switzerland), Luxembourg
+ Issues:
    + Musiflow is trying to getting into The Board
    + Marcus is a Methuselah Fragment
    + They have a quarry with Radio Luxembourg
        + Maintained by _Amelia Stone_ and _Professor Khan_ in the backstage

## DuoLogic

+ Actual members
    + Lead Singer, Guitar, Founder: Wayne Darsius (UK)
    + Back vocal, Bass: Lawrence Willians (Ireland)
    + Back Vocal, Keytar, Electronic Music: Sabine "Dizzy" Pfeiffer (West Germany)
    + Drums: Olivier Narval (France)
    + Violin, other Chords, Ethnic vocals: Shamal Abulafia (Morroco)
    + Sax, Woods: Samuel Valente (Portugal)
+ Style: Synthwave, somewhat new age, some pop (_Tears for Fears_ meets _Clannad_ meets _ABBA_)
+ Nominated to Eurovision via: Luxembourg
+ Secrets:
    + Wayne is a Methuselah Fragment, working with Marcus to bring Methuselah back
    + Sabine, Shamal and Samuel were all from the Hu Dunnit Club, although none of them knows Wayne's and Marcus' secret
    + Sabine has a music PhD, focused on Electronic Music and Ethnical Music
    + Samuel was a Vienna Symphony musician coopted by Musiflow, but has lots of contacts
    + Except by Marcus, no one knows the reason Duologic is getting into Eurovision
    + The band has almost totally changed in the last year: the older members had suffered weird accidents or chosed to get out the band
    
### People

### Marcus M. Townsend

### Wayne Darsius

### Kraftwerk

Comming from the recent success _Computer World_, they are making something more like the past, but they are also working and listening to music all around the world. 

#### Secrets

+ Florian was one of the Doctors in Sabine's PhD board, and they maintain contact since it;
+ Ralf is linked with some institutions and people, specially in the Berlinen Universität;

#### Ralf Hütter


#### Florian Schneider

### Dschinghis Khan

Formed to compete the national nomitations for West Germany in the 1979 Eurovision, Dschinghis Khan had achieved some success in Europe after with songs like the band-namer "Dschinghis Khan" and "Moskau"

#### Secrets

+ Louis Potgieter is now with AIDS, without explanation. Some people say he was innoculated by Musiflow's goons as a way to be blackmailed into the company. He, however, refused.
+ "Moskau" was written, in fact, by a ghost-writer, not by those who receive the credits. Some believe that this ghost-writer was _Rocket Red_ and the music has a secret message for her _tovarisch_ (friends), urging them to thrawn the Andropov/Chernenko government, as the only way to open USSR diplomatic relations with the West (the motives are unknown). This could be the explanation on why this music is forbidden at USSR (not the official version about parody).

### ABBA

Disbanded after the collapse of the band member's marriages (Björn Ulvaeus and Agnetha Faltskog, Benny Andersson and Anni-Frid "Frida" Lyngstad), they still are involved with music, Ulvaeus and Andersson composing music, Faltskog and Lyngstad singing solo

#### Secrets

+ Ulvaeus and Andersson are working together, but they had get away from the marriages because they felt the pressure of music industry over relationships
+ Faltskong and Lyngstad were approached by Musiflow, but they declined it invite. No one beside them knows the terms involved on the negotiation

### Jean-Michel Jarre

After the auction of the only copy of  _Musique pour Supermarché_, and his playing via _Radio Luxembourg_, as a protest by the "silly industrialisation of music", he's working on a new album and big shows around the world. He turned very famous via his _Oxygéne_ album and to be the first eastern musician to play into the post-_Cultural Revolution_ China.

#### Secrets

+ Jarre knew he was somewhat sabotaged by some other music company to hurt _Dreyfuss Musique_, the label he's linked. He suspects of _Musiflow_
+ He had access for the music _DuoLogic_ played to win the Luxembourg nomination, and he felt dizzy and queasy after that. He has the hunch that the music is more than they shown.
+ He knows that _Musiflow_ tried to enter in China before him but was unsuccessful.

### Mike Oldfield

After a controversal therapy, the musician came back to recording lots of music, including _Blue Peter_'s theme and a music for Prince Charles and Pricess Diana wedding. He's going into mainstream music after a start into more experimental music.

#### Secrets

+ Into the controversal therapy Exegesis he passed through, he knew Wayne and feel that he was not exactly what he said. Wayne had undergone Exegesis with everything paid by Musiflow

### Evangelos Odysseas Papathanassiou - Vangelis

After putting two biggest scores in the films _Blade Runner_ and _Chariots of Fire_, having some music at the _Cosmos_ soundtrack and having a great partnership with former _Yes_ player _John Anderson_, Vangelis is working on some new soundtracks and on his own authoral music.

#### Secrets

+ As one of the Eurovision judges, he received DuoLogic music and he found himself having nightmares for some days after hearing the music


### Possible Nominations

+ Latin America: 14 Bis - Mel do Amor
+ Oceania: Dire Straits - Money for Nothing
+ Africa: Tears for Fears - Shout
+ América: Culture Club - Karma Chameleon
+ Oriente Médio: Eurythmics - There must be an Angel (Playing With My Heart)
