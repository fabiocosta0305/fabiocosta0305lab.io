---
#subheadline: The Blueblood lady that can deal everything for the needed love from _Dystopian Universe_
title: Colton "Colt" Derringer
layout: personagens
categories:
     - personagens
tags:
     - Shadow of the Century
language: br
header: no
---


## Aspectos

|              ***Tipo*** | ***Aspectos***                                      |
|------------------------:|:----------------------------------------------------|
|            **Conceito** | Antigo Adido Militar, antigo _Top Gun_              |
|         **Dificuldade** | Não tem a mente muito aberta                        |
|    **Chamado às Armas** | Vida civil não é para mim                           |
| **No Campo de Batalha** | Respeito ao inimigo é sempre uma boa ideia          |
|   **Formando a Equipe** | Já vi pessoas melhores morrerem no campo de batalha |


## Papéis

| ***Papel*** | ***Bizarro?*** | ***Perícias***                                    |
|------------:|:--------------:|---------------------------------------------------|
|     Cérebro | N              | Computadores, Engenhocas, Intuição, Conhecimentos |
|     Soldado | N              | Atletismo, Lutar, Atirar, Vontade                 |
|      Piloto | N              | Atirar, Direção, Percepção, Provocar              |

## Perícias

|     ***Perícia*** | ***Nível***   |
|------------------:|---------------|
|        **Atirar** | Razoável (+2) |
|     **Atletismo** | Regular (+1)  |
| **Conhecimentos** | Razoável (+2) |
|          Contatos |               |
|  **Computadores** | Bom (+3)      |
|       **Direção** | Ótimo (+4)    |
|    **Engenhocas** | Razoável (+2) |
|       Furtividade | Regular (+1)  |
|      **Intuição** | Regular (+1)  |
|         **Lutar** | Regular (+1)  |
|     **Percepção** | Regular (+1)  |
|         Persuasão | Regular (+1)  |
|      **Provocar** | Regular (+1)  |
|             Roubo |               |
|          Recursos |               |
|       **Vontade** | Regular (+1)  |

## Façanhas [Recarga: 2]

+ ***Nunca deixe ninguém para trás:*** se um aliado seu que esteja próximo a você receber uma consequência, você pode reduzir a severidade da mesma recebendo você mesmo uma consequência. O número de passos que a consequência será reduzida dependerá da consequência sofrida por você: uma para suave, duas para moderada, três para severa. Se a consequência for reduzida para abaixo de Suave, ela não é sofrida;
+ ***Modificações Especiais:*** Durante uma Montagem, se você estiver usando *Engenhocas* para melhorar seu carro, você obtém uma invocação adicional em qualquer Aspecto gerado. Se Montando uma Disputa, você pode sacrificar a invocação adicional para obter uma vitória adicional.
+ ***Tradutor Universal:*** você não precisa rolar para falar e ler qualquer idioma que seja razoável de conhecer. Caso contrário, você rola contra dificuldade *Ótima (+4)*
