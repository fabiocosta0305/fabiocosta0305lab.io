---
#subheadline: The Blueblood lady that can deal everything for the needed love from _Dystopian Universe_
title: Jenny Williams
layout: personagens
categories:
     - personagens
tags:
     - Shadow of the Century
language: br
header: no
---


## Aspectos

|              ***Tipo*** | ***Aspectos***                                |
|------------------------:|:----------------------------------------------|
|            **Conceito** | Uma estudiosa da cultura futurista            |
|         **Dificuldade** | Síndrome de Peter Pan                         |
|    **Chamado às Armas** | Uma área da faculdade que não banca as contas |
| **No Campo de Batalha** | Se não é comum, deixa comigo                  |
|   **Formando a Equipe** | Eles precisam enxergar além do que existe     |

## Papéis

| ***Papel*** | ***Bizarro?*** | ***Perícias***                                    |
|------------:|:--------------:|---------------------------------------------------|
|     Cérebro | N              | Computadores, Engenhocas, Intuição, Conhecimentos |
|   Diletante | N              | Computadores, Direção, Persuasão, Recursos        |
|    Inventor | N              | Computadores, Direção, Engenhocas, Conhecimentos  |


## Perícias

|     ***Perícia*** | ***Nível***      |
|------------------:|------------------|
|            Atirar |                  |
|         Atletismo |                  |
| **Conhecimentos** | Excepcional (+5) |
|          Contatos |                  |
|  **Computadores** | Bom (+3)         |
|       **Direção** | Razoável (+2)    |
|    **Engenhocas** | Ótimo (+4)       |
|       Furtividade |                  |
|      **Intuição** | Regular (+1)     |
|             Lutar |                  |
|         Percepção | Regular (+1)     |
|     **Persuasão** | Regular (+1)     |
|          Provocar | Regular (+1)     |
|             Roubo |                  |
|      **Recursos** | Regular (+1)     |
|           Vontade | Regular (+1)     |

## Façanhas [Recarga: 2]

+ ___Enciclopédia Perambular:___ no início do _episódio_, role _Conhecimentos_, dificuldade _Medíocre (+0)_. Cada duas tensões permitem que você coloque uma Invocação Gratuíta em uma Área de Conhecimento que você nomeie. Quando essa área de conhecimento puder ser usada, use a Invocação para conseguir um Sucesso Automático em um teste relacionado
+ ___Formado na Faculdade:___ _Uma vez por episódio_, você pode escolher uma perícia que não esteja associada aos seus papéis. Essa perícia passa a ser considerada _Razoável (+2)_ até o final da cena
+ ___Ciências Sociais:___ quando falando com alguém que é muito conhecido em uma determinada científica, você pode usar _Conhecimentos_ no lugar de _Intuição_ para descobrir algum Aspecto do mesmo

