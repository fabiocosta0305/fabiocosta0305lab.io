---
#subheadline: The Blueblood lady that can deal everything for the needed love from _Dystopian Universe_
title: Timothy Sanders
layout: personagens
categories:
     - personagens
tags:
     - Shadow of the Century
language: br
header: no
---


## Aspectos

|              ***Tipo*** | ***Aspectos***                                       |
|------------------------:|:-----------------------------------------------------|
|            **Conceito** | Anarquista Panfletário "Profissional"                |
|         **Dificuldade** | Passado "obscuro" - Uma família de carolas           |
|    **Chamado às Armas** | Vi o que não deveria ver                             |
| **No Campo de Batalha** | Pensar fora da caixa é bom                           |
|   **Formando a Equipe** | Esses caras são caretas, precisam ver algo diferente |

## Papéis

| ***Papel*** | ***Bizarro?*** | ***Perícias***                               |
|------------:|:--------------:|----------------------------------------------|
|       Rosto | N              | Contatos, Intuição, Persuasão, Provocar      |
|   Diletante | N              | Computadores, Direção, Persuasão, Recursos   |
|       Líder | N              | Intuição, Conhecimentos, Persuasão, Recursos |


## Perícias

|     ***Perícia*** | ***Nível***      |
|------------------:|------------------|
|            Atirar |                  |
|         Atletismo |                  |
| **Conhecimentos** | Bom (+3)         |
|      **Contatos** | Regular (+1)     |
|  **Computadores** | Regular (+1)     |
|       **Direção** | Regular (+1)     |
|        Engenhocas |                  |
|       Furtividade |                  |
|      **Intuição** | Bom (+3)         |
|             Lutar |                  |
|         Percepção | Razoável (+2)    |
|     **Persuasão** | Excepcional (+5) |
|      **Provocar** | Razoável (+2)    |
|             Roubo |                  |
|      **Recursos** | Razoável (+2)    |
|           Vontade |                  |

## Façanhas [Recarga: 3]

+ ___Confia em mim:__ Quando usar sua ação para convencer outros PCs a fazer algo perigoso e eles fizerem, eles recebem +2 nos seus rolamentos;
+ ___Plano B:___ Após uma montagem que gere qualquer número de Aspectos, vcê pode reescrever alguns deles, mantendo suas Invocações Gratuitas. Você pode fazer isso com um número de Aspectos igual seu nível de Intuição por episódio
+ ___O plano vai se consolidando:___ se todos os personagens forem bem-sucedido durante uma montagem, os Aspectos gerados dão bônus de +3 no lugar de +2 quando invocados para bônus;

