---
title: Happiest Armageddon on Earth
#subtitle: An Adventure for the _20th Century Young Centurions_
layout: aventuras
#date: 2016-05-27 23:15:00 -0300
#date: 2016-08-23 23:15:00 -0300
categories:
 - Adventures
comments: true
tags:
  - Shadow of the Century
header: no
excerpt_separator: <!-- excerpt -->
---

## A _Shadow of the Century presents_ Movie!

This movie is for 2-6 PCs. Although he has few moments of super-driven action, they are precious, so a group of differently talented and focused PCs would be a good way to go through things. No character should, at least at start, have any Gonzo Aspect/Stunt: the main idea here is common people fighting a very weird menace.


## Chapter 1 - The Fairest of Them All

The PCs are all agents of NOVA (_Network of Variable Agents_), a special operations organization specialized on work with the uncommon and the weird, as by their motto _"Ordinarius populus adversus minas aliena"_, _common people fighting weird menaces_, that in fact works on what will be called in the future as _Smart Mobs_: by knowing and listing lots of specialized operatives, and know their whereabouts, they call the best team they can to join based on time and capabilities.

<!-- excerpt -->

All of them found themselves in front of Orlando International Airport, where they encounter one of the local NOVA Operative, _Sargent Sandra Williams_ from Police Department Police of Orange, a city on Orlando Metropolitan Area known by being the real city from _Walt Disney World_.

After greeting everyone, Sargent Williams will explain what happened:

> _"We had a very weird spree of attacks against people related with Disney World, and Disney PR doesn't want to show off things by putting police on it. However, we had a last case that made shit hits the fan. So, I asked my superiors and Disney World big guys and they understood that putting NOVA on the case would be a thing. Let's go, I'll show you what is happening."_

She place the PCs into some cool cars and go for a place into a somewhat Middle Class neighborhood in Orange, where there is another guy, a little nervous, on a dark blue suit, the only thing that shows his importance being a Mickey Ears tie clip.

> _"This is Mister Timothy Andalasia, Disney World HR Chief. He's here because the case affects them."_

The house is clean and beautiful. The decoration is all Disney: lots and lots of photos from Disney Worlds, Mickey Wall Clock, a Mad Hatter Tea Cup set, even a small Pluto-tag shaped pillow. Some of the living room furniture, however, is broken and there was some markings showing where a body was found.

> _"We couldn't leave the body here, as the Florida heat would accelerate decomposition, and the victim was killed 3 days ago, it would make here stink. But we already have some intel about her."_

If people wants to investigate all around, an _Awareness_ ***Fair (+2)*** will show that the main Disney addiction of the deceased was Snow White: there was lots and lots of Snow White stuff, and they could even find some Snow White Halo-ween costumes on the closet. Even their clothes are on Snow White colors and so.

Before question about the deceased, Mr. Andalasia came to them

> _"Before we start, I want to ask you to hold this information as classified for a time. We have a reason for this related with Disney World."_

If they look the photos local CSI provided to Sargent Williams, they'll see that there was also a very unusual thing nearby the body... And with the body.

The body of the deceased woman was dressed into one of the Snow White costumes, but without a wig, as the auburn short red hair showed. She also has an apple in the hand.

> _"She's Leanna McLane, and she was a Cast Member as Snow White at Disney World."_

People will find it weird, and even weirder about how Mr. Andalasia looks touchy about revealing this information. If they try to question Mr. Andalasia, here's some questions, the answers and if he's being truthfully and extra intel they could juice from him.

1. ___"Cast Member? What the fark?"___

> + _"Cast Member is just some Disney Speak for_ employee. _From the Janitor to The Mouse itself, everyone on Disney World are part of a Cast, even me."_
>
> This is is true: Disney is trying to be a place without as many prejudices as possible. This is very weird from a 80s company, but this is paramount for a company that works with a cultural symbol like Mickey Mouse, that goes all around. And even more new that they are struggling to recover from a very harsh hostile takeover attempt by Saul Steinberg, which made Roy Edward Disney (Walt's nephew) to remove Walt's son-in-law Ronald Miller to bring Micheal Eisner as CEO. All of this made a big punch on Walt Disney Company, and they are still licking the wounds. Considering that the second favorite of everyone, Donald Duck, is turning 50, it's not a good time for scandals.

2. ___"And why all the secrecy about her being Snow White?"___

> + _"Think on a kid: they doesn't see our Cast Member, specially the Characters, as people. They see them as the Characters. If a Character Cast Member is on suit, he's onstage. Think if people discovers, for just a little, that Snow White killed herself with an poisoned apple?"_
>
> The PCs can make fun, because this is somewhat **EXACTLY** what happens in _Snow White and the Seven Dwarves_, but this will not be see on good eyes by Andalasia: this secrecy on the Character real identities helps also to allow people to shift from Character to Character, he'll explain. Also, he'll explain, if questioned, about the differences between Face Characters, that doesn't uses full body costumes, and can, in fact ___MUST___, talk and act like the character, like Alice, the Princess or Peter Pan; and _Fur Characters_, that uses full body costumes, so can't talk, communicating only through _animations_, default gestures and moves.

3. ___"So, Disney World's panic on all this is if people could see Snow White killed herself. There's anything wrong with her?"___

> + _"No... In fact, she was one of our best Face Characters: she was passionate with Snow White, but she also covered the bases on other characters. Alice, Cinderella, the Tremaine... Even Mickey."_
>
> If the PCs asks why she could be Mickey being a woman, Andalasia will non-chalantly that, for Fur Characters, as far the Cast Member has the right measures and body shape, he can be assigned for any character, as far he knows the right animations. In fact, all Face Characters passes at least one year as Fur Characters at least.
>
> About the victim: she was an healthy life girl, as an _Awareness_ or _Insight_ ***Average (+1)*** roll will show: she has lots of granola and fruits, almost no meat and a card from the local gym. Some pics could even be seen on her doing some running on local run competitions and so.
>
> For personal life, she was single, but there was some pics with her family: the only one that could associate her as Snow White with her family is one from a Halloween where she was dressed as a too much perfect (for a common costume) Snow White suit, her mother as Cinderella's Fairy Godmother, and two other women as the Tremaine Sisters Drizella and Anastasia. One of them was holding hands with two kids dressed as Jaq and Gus, the mice that helps Cinderella. 
>
> If they question Andalasia about this pic, he'll say they are their family: Mother and Sisters, one of them a mother of twins. He knew them some time before, when Leanna bring them to Disney World as part of a benefit as Cast Member. He also knows his father was killed on 'Nam.

4. ___"There was other cases?"___

> + _"We had some assaults against other Cast Members, but this is the first time things had gone to be against a Character, and to go full kill."_
>
> Sargent Williams will pass them a list of assaults that had happened to other people that worked on Disney World. Nothing big, and no one that would be that important: the others were all the common ground guys, like Janitors, selling people, nothing so big.

5. ___"How do you know wasn't a suicide?"___

> + _"It would be too simple... And the messy furniture shows that someone had there before..."_
>
> Things are a little fishy. If the PCs pushes things a little (___Persuade___ or ___Provoke___ checks Against Williams), Sargent Williams will show a picture that wasn't in the profile. Looks like a weird thing that CSI gave as a random factor, but kicked in for Williams as NOVA Agent: in the back of Leanna's neck, there was a small sequence of puncture wounds, very small. Someone could take this as drug uses, beside the fact is a place not to good for an addicted make a drug shot. And they were somewhat ___Neatly Disposed___ each other, and looked more like it was ___made by a microchip, but octagonal___.
>
> + _"This is totally weird: I send this one for our Forensics and they saw the only thing could this those furrows were really a microchip contact, like those from a Commodore or Radio Shack, but the number, size and disposition were like nothing I know. The nerdy guy from Forensics even though as a joke that this could be some kind of controlling device from the Future... However, there was no forced entrance we could see here."_

6. ___"If she was targeted, why they should do this? How they knew about her being Snow White? Some kind of Ax Murder stalker?"___

> + _"We on Disney World takes very seriously the information about who are our Characters, to avoid to break Disney Magic, especially the Face Characters that can't hide under a full body costume. To be fair, time and another some hardcore Disney fans discover about this or that Cast Member that did this or that Character, but is very rare. And this is good, because we need to uphold Disney Magic!"_
>
> By using all kind of theatrical tricks and do a very diligent work, the Disney World makeup people can turn anyone with the right bodyframe into the character they need to be. It would be needed a ___Fantastic (+6)___ _Awareness_ roll to distinguish from, for example, an Alice and another Alice. As there isn't two equal Characters at any time (at least at the same park), it would need also a very big feat of memory (or lots of good quality pics) to do this. 
>
> To even more track them to a specific people it almost impossible (___Beyond Legendary (+14)___ _Awareness_ Contest): Disney Company (from the very start, when Walt Disney was still alive) takes lots of measures to make people to get into the so-called _Disney Magic_: one of the open secrets that cleverly help this is that there's under Disney World a maze of tunnels where around the Cast Members goes from a place to another, specially the Characters. Some of the entrances and routes are well-known, but nothing that could help someone to track someone. Also, normally the Cast Members get together to a place in the Disney World to where they go dress up and get onstage.
>
> With all those precautions, there would one explanation to someone assaults people knowingly against Disney Cast Members: ___Inside Job___

7. ___Maybe you have a mole into your people. Do you know anyone that could have a so big grudge against Disney?___

> + _"Well... Maybe some commies, but nothing else... But I can't see them getting as a mole into Disney World."_
>
> Some communists parties and countries are 110% against Disney because they say on it the vanguard of (sic) USA imperialism and capitalist ideary. However, the hiring process on Disney is really hard.

To end, Andalasia propose that the PCs goes undercover as Cast Members, allocated on various parts of the park, preferably as people that could commonly pass undetected: janitors, maintenance, etc...

So, they are now ___Disney World Cast Members___.

## Chapter 2 - Here you leave today...

Next day, the PCs arrives at Main Street Entrance, where Mr. Andalasia waits them, below the famous plaque with the park premise: _"Here you leave today and enter the world of yesterday, tomorrow, and fantasy"_. They are given special Mickey Ears and some papers for positions on Disney: they are also explained on some basic terms, like being _"onstage"_ (dealing with public on any way) and being "Backstage" (working on support). Also, they are given some ___Basic Orientation about how Disney World works___ by some Cast Members on their places: nothing big, but allow the PCs to turn this into an Create AdMontage action if they want to fill this Aspect with as many Aspects they want. This will be very useful in the future.

They start their work on the park: let them taste the waters and take the heat. Remember they are ___Disney World Cast Members___ by now: maybe they are Janitors that will need to deal with a ___very piggy visitor___, that throw away they cola can or (oh no) cigar dregs or even (HEAVEN FORBID) chew gum. Or they are selling staff that needs to deal with a ___spoiled brat___ that wants EVERYTHING in the store, even against their parents approve. They can have problems even in backstage: if they are in the dressing sector, maybe they would need to deal with a ___Snow White dress starting to loose their stitch in the hem of the skirt___! Of course Snow White would be not fair on this: it's just lagging the work.

Let them suffer a little the pressure on working in ___The Happiest Place in the World___, before put the clues on for them to look upon...

### Chapter 2.A - Sabotaged Joyride

One of the most classic rides in the Disney World, _Peter Pan's Flight_ is one of the few ones that stays from Disney World opening. So, he's cherished as one of the most iconic (and fun) of Disney World rides.

The PCs (at least those that are on the maintenance) are called upon a problem on the ride: for some reason, the diagnostics software showed a problem nearby the animatronic of Hook being almost eaten by Tic Toc Crock. 

The weirdest thing is that, when they arrive in there, there's _already_ a small group of maintenance guys (1 per PC + 1). They'll say that they found a weird thing on the control unit for that part of the ride, and will go away with a small box.

If the PCs asks about the box, the NPCs will say that they already had exchanged it for a fresh unit. This can lift a red alert on a clever PC: an ___Awareness___ roll will reveal them as ___Sleeper Agent___ that for some reason were sabotaging the ride. In fact, either the unit they have in hands and the one that they replaced with are ___Sabotaged Control Units too much advanced for their time!___ If they notice this, they'll run away.

GM, those Control Units are VHS 2 devices: they could not work okay on almost anywhere, but Disney World has the potential to suffer from ___Variable Hyperdimentional Simultaneity___! Remember, this is a place where _"you leave today and enter the world of yesterday, tomorrow, and fantasy"_. And those guys wants to beef something on this.

Some options:

+ ___The PCs tries the showoff option, fighting the guys:___ Bad idea. Those guys are ___Real Cast Members___ as far they know, and the security team could be called for to control the mood;
+ ___The PCs allow the guys to run away:___ they'll have a good clue on the ___Control Unit___. However, they'll also to need to exchange the ___Sabotaged Control Unit___ by a clean, real one. Good news is: it's not too far via the secret corridors the warehouse with the replacement units. Bad news is: unless they call for a _refurbishment_ (close the ride), they'll need to exchange this with everything online. This would be a ___Good (+3)___ _Gadgetry_ roll to replace the units, as far they have the real one. To do this before the ride gets there (and maybe crash, derail or otherwise lock down), they'll need an ___Fair (+2)___ _Athletics_ roll
+ ___The PCs will try to defuse the sabotage using the Sabotaged Control Unit:___ here the things goes big; as a VHS 2 device, it's not like it would be active all the time. The device is activate as soon the local VHS goes to 2. The bad news is: VHS goes up there a time and another, as soon people are happy. ___Happiness makes VHS___ grows on Disney (unknowingly for everyone). The options to avoid the VHS trigger are:
  + ___a. Reduce the speed to make the ride slower (and a little bore):___ a _Gadgetry_ or _Computers_ ___Good (+3)___ roll will be enough to avoid the trap;
  + ___b. go for the backup control unit while defusing:___ worse news: unless the PCs pay an 1FP, the ___Backup Unit is also sabotaged!___. If they pay so, the Backup unit is a common one, and it's just turning a switch on a control panel to exchange from the main control unit to the backup one. If they do this, they can remove and replace the main control unit without rolls;
  + ___c. go all out and defuse the main control unit online:___ not too easy: we can treat this as a _Contest_ between the PCs _Gadgetry_ or _Computers_ and the device's _Fantastic (+6)_ ___Security Measures___ (YIKES!!!). If the PCs fails, a locking system will engage and the device engage as soon the ride passes there, no need of VHS 2. If the PCs are successful, the control unit will shutdown and the ride will stop as soon getting there (ride's security measure). People will be a ___little scared___, but not the PCs could not deal with;

### Chapter 2.B - The Stressed Popcorn Seller

If there's a thing that people eats a lot on amusement parks is popcorn, even more on Disney! The PCs are working on Main Street (the big street that goes to Cinderella's Castle) and they notice a guy that is not too much "onstage" so to speak: he looks a little too much grouchy and grumpy. He's giving the popcorn with a little too harsh moves, and even pointing places with just index finger, while Disney standard is using index and middle fingers together, as in some cultures pointing places with just a finger is a very rude move.

If they get nearby and talk with the guy, he'll give a excuse, like being _"on a bad day"_, a _"Headache about my sister_ and so on. However, if the guy is pressed, he'll try to excuse himself from the work. 

If the PCs tries anything hard, like trying to capture him, the Disney World security will be there, and they'll have some problems.

If they try to convince the guy to get out, they could juice some good information about him, like his name: if they cross things after, they'll see that he's part of a group of ___Sleeper Agents___, that ___Doesn't exists on hard, cold paper___, even their ___Social Security Cards being fake, albeit a really good one___: ___they had injected their informations in the System___, maybe using some super hacker. 

If they took the popcorn, they saw that it was purposely made to make the mood bad, and on a very clever and simple way: salting the popcorn too much, and putting some bad pepper.

### Chapter 2.C - The Mole

If the PCs are really smart, they had put someone in HR: ___very boring backstage work___, but occasionally seeing Mickey without his head is somewhat fun. 

The Mole in the place is an ___Sleeper Agent___ called Diana McIntosh, or at least is the name she passed when hired and it is on the tag on her nice blouse. She was hired some months ago, some weeks before the first attack, as a common clerk, and one of their functions was to set the characters shifts. 

So, she could be the one that knew about Leanna McLane.

If the PCs there do their job alright, they'll sooner or latter get into with Diana. If confronted with the fact, she'll give a somewhat decoying excuse: she will say that she was really sorry and did a bad move by not shredding the recent character shifts, so someone maybe had dumpster diving and took it for some reason.

This is totally false.

Diana had a job: trying to make people somewhat fearful and sad. This would make, by emotional fluctuations make the reality borders nearby Disney World thin, and so changing VHS. And this is their plan.

If confronted, she'll try to run away. But, if the PCs are clever enough, she's the best way to discover what is happening

No matter what, the should understand that the best thing now is to shadow one of those people to discover what is happening.

## Chapter 3 - The Bastion Cult

IF the PCs want to get and find about those people, they can easily (somewhat) shadow them: just a simple contest between them and the ___Sleeper Agents___. They go for a small house nearby Disney World, in Orange. So nearby that they can see the Cinderella's Castle tip in the horizon.

If the PCs see, there's apparently nothing wrong in the place: just some people drinking and seeing NFL/NBA/NHL/NASCAR/whatever. However, a ___Good (+3)___ _Awareness_ or _Insight_ roll will show that some of the noise are a ruse: they are debating something.

If they go and take a peek, another roll will show them that there's ___lots of super-tech things___, things that turned a Commodore 64 into an abacus! They also will see a big America-like banner, but with a Earth globe forming what looks like...

... Mickey ears.

If they try some dumpster diving or something like else, they'll discover some threshed leaflets, but not enough to be reconstructed with some glue and/or tape. A ___Good (+3)___ _Insight_ or _Knowledge_ roll will allow them to remake the leaflets, that says something about ___The Bastion of United World Government___, with some orientations on the _Fourth Dimentional Expansion Project_ a.k.a. _Disney Trail_.

A PC with the correct devices/contacts can do a ___Fantastic (+6)___ roll against adequate skills (_Computers_, _Knowledge_, _Contacts_) they can discover that ___The Bastion Cult___ is a kind of uber-right-wing people that makes KKK looks like ACLU: they are fighting to establish a _"new world for the Free and the Bold, a World which We Would be The Bastion of the Future"_. 

A lot of uber-right-wing balderdash.

However, this will not explain how they have that _super-tech_. Leave to the PCs to go full conspirational theorists to try to imagine what is happening. If they try to follow the traces about those guys, they'll discover that, although they are in the System, their information are totally fake in hard paper: the schools, parents, nothing on this is real.

However, they still doesn't know what are their plans...

... except by a ___threshed old Magic Kingdom map, with some markings on them___.

This is the _Rising Action Milestone_ - ___"A Totalitarian Regimen is Rising!"___

## Chapter 4 - Yen Sid

Now, the PCs needs to understand (a) what those Bastion guys wants and (b) what that Leanna McLane girl has with all this.

While they think on this, they'll see a weird thing (secret _Awareness_ roll, the best that pass a ___Average (+1)___ roll, or someone with an adequate Aspect that accepts a Compel or pay an FP for an Invoke) at Main Street's Firehouse: a light is on in the living quarter's window. But (a) there's nothing on the quarters at Disneyworld and (b) this is a tradition on Disneyland, at San Francisco. 

A weird thing... Maybe a prank, or maybe something else.

If they go for the something else, as soon he got nearby the firehouse, a very old man, with a crooked, severe look, get nearby them and say:

> _"If you saw that light, maybe you're special. Find me here in the After Hours. I believe I know a little this and that about what you are searching for, and about_ YOU."

He has a very profound and serious voice, although not (totally at least) hostile. Their blue eyes are really really old and his crooked face is severe, although not mean. An ___Fantastic (+6)___ roll _Awareness_, _Insight_ or _Knowledge_ roll (worst of them) will reveal who is this guy, but they can't believe: ___he should be just a cartoon!!!___

If (or when) they go After Hours, to the Firehouse, they'll find the doors opened: which is weird, as they, as part of Main Street stores and so, should be locked After Hours. They could hear some music coming upstairs (where, in fact, there should be NOTHING), and they could see a stair that it wasn't there before.

If they goes, they'll see a very simple but elegant flat, the lights on while an old fashioned Victrola is playing ___The Sorcerer Apprentice___. And sat on a couch, there is that man, but dressed into a blue robe, with a weird hat over his head.

An _Insight_, _Knowledge_, _Awareness_ ___Fair (+2)___ roll give the same previous information, confirming with those who where successful previously:

> _"Hello, my friends. I'm Yen Sid, The last Master and Herald of Disney Magic and Imagineering. Or, at least, it was what I was in my reality."_

So, the PCs will go certainly bonkers (if not, they are bonkers for sure!). He will give some answers for them. Remember that Yen Sid is severe, somewhat harsh, but never hostile. There's no notes because all answers are true

+ ___"Okay, this is a joke, right?"___

> _"No... I had came to this reality when it was December 15th, 1975, exact nine years after Walt Disney's demise. I used lots of Disney Magic and Imagineering to run away from my reality to this one."_

+ ___"Why you ran away?___

> _"Some years after Disney's assassination, in my reality, a group of people with nationalist intentions somewhat robbed Disney culture to form a kind of ultra-nationalist, right-wing, cult that grows and took America government. Some years after, Moscow was nuked on a fast, pseudo-preemptive attack. In few years, what was knows as United States of America became the United World Government, forcing all governments in the world, by war or financial blockage or so, to become part of this. Lots of people were killed on a thing only compared with Nazi. And they hunted all the Disney Magic and Imagineering practitioners, to put them under their service if possible, or to kill if not. I did a last Disney Magic ritual to escape that reality, but never knew they would get here."_

+ _**"Wait, what the** bleep **is this Disney Magic and Imagineering thing?"**_

> _"I think it was Asimov that said that_ All sufficiently advanced technology is indistinguishable of magic. _Disney Magic is just what I call magic in general, but a more focused one, on bringing the fantastic and wonderful. And Imagineering is using this understanding on some kind of energies your reality's scientists still didn't identified and understood, as a way to build some super-technology."_
>
> If someone think this as somewhat like _Methuselah's Fragments_ (if they had already some contact with them), it not the same thing, although he somewhat operate into a similar frame.

+ ___"We found this weird thing on a sabotaged ride. Could be this Disney thing?"___

This can only be asked if they have or had contact with the ___Sabotaged Control Unit___ at Peter Pan's Flight in Chapter 2A. If they show it to Yen Sid, he'll look a little, open, do some kind of weird magic moves and says:

> _"Yes... This is Imagineering, for sure! This device could not be built with the actual technology form this reality. This is bad news."_

+ ___"So, looks like those guys are from your reality... And what about the Bastions?"___

> _"The Bastion is the group that commands my reality. They twisted all Disney defended of good and nice, making it a grim, evil parody of all Disney defended."_

+ ___"And about Disney? You said he was assassinated, but he died by smoking."___

> _"In my_ original _reality, he was assassinated. Obviously there was an investigation, that concluded he was killed by communist agents in our country. However, there was always some suspicious on internal job against Disney. Even I could not investigate enough to understand what happened for sure. Some key events in my original reality happened as in yours, like_ The Fall of the Century Club, _but some didn't happened like on yours, like Disney's assassination."_

+ ___"And why they are coming here?"___

> _"I'm not sure, but I have some tips: the climate in my reality, when I ran away, was changing: extreme weather events, like typhoons, floods, droughts and earthquakes. This, and Bastion's ambitions, were provoking resource shortage: they explored the natural resources, and for this they needed people. The population growth, stimulated by the Bastion, provoked a food shortage, even more that lots of productive soil was spoiled during the natural resource exploitation. And with the ambition and the super-scientific, for your time, Imagineering, they are trying to expand themselves to another dimensions. And looks like your dimension is the next target of their expansionist campaign is your reality."_

+ ___"Can we stop them?"___

> _"We need to know their plans: how they are trying to get into your reality is still something I'm trying to understand. The energies involved are big and not easy to harvest and to manipulate enough to make a rift between the realities."_

Exactly this moment, there came the ninjas:

A group of ___Super-Armed Totalitarian Soldiers from other reality___ came to fight the PCs: they are 2 per PC + 4 for Yen Sid. And the conflict starts.

Remember that they are at Disney, and next day the park will open. It would be really complicated to explain that Main Street Firehouse was bullet-ridden. So, they are into a situation were their opposition has the higher ground.

Their objective is simple: kill the PCs. Take Yen Sid if possible, kill if not. They want Yen Sid to help them.

But if PCs are smart, they can capture one of them. And this will bring us to the next Chapter. If not, just jump to the Chapter 5... It will be fun. Anyway, this will be a ___Confrontation Milestone___.

## Chapter 4.B - The Hostage

This will happen if the PCs are clever enough to take one of the Soldiers from the conflict before. 

+ ___"Okay, why the attack?"___

> _"We wanted to capture the traitor Yen Sid, to make him help us into our plan."_

+ ___"Which plan?"___

> _"We'll start the_ Disney Trail, _by opening a big enough portal to some troops get into this reality. Soon, our mighty troops will bring power enough to dominate this reality."_

+ ___"And what about Leanna McLane?"___

> _"She was uneasy and talked about us with some local enforcement agent, so we needed to neutralize her before she could discover any extra information."_

So, Leanna was seen in fact a ___NOVA Operative, albeit unintentional___, when she talked (probably with Sargent Williams).

+ ___"And when this will happen?"___

He will smile and says, before kill himself:

> _"Tomorrow, and there's nothing you can do."_

Yen Sid will look for the PCs and says

> _"We'll need to discover from where they'll open the portal. Looks sensible it will be at Disney, but we need to know exactly where, as there would be some Imagineering device that we can try to defuse. Also, there's something I had prepared using all my Disney Magic and Imagineering knowledge to help people here."_

He'll go for a safe behind one of the Walt Disney photos on the wall. After opening it, Yen Sid will show some weird devices into a box: they looked like clocks, but bigger, and with some small silhouettes engraved on them, easily recognized as some of the most famous Disney characters, like Mickey, Donald, Snow White and Alice.

> _"This is called Disney Casting Device. It's a thing of my own invention I did still when in my original reality. You see: imagination is power, this is one of the basis of Disney Magic. By using this, you can bring the sheer power of imagination and, there's no other way to say,_ become _the Disney Characters. This could gave you some power to fight the Bastion... But need to say, this is very dangerous: you'll need to let your imagination get loose and allow the Disney Magic to flow into you, making_ you _becoming Disney Characters. This is not easy and could be lots of side-effects. I'll understand if you refuse my offer. However, if you take this, maybe Disney Magic will help you."_

Let the PCs choose the _Disney Casting Device_ they want. It will be obvious that, when engaged, they'll make them the Disney Character on it. There's some samples in _Disney Casting_ Gonzo Stunt listed under _Yen Sid_'s character. The PCs will have access to the _Disney Casting_ Gonzo Stunt while they have the device.

So...

Next day will be the day. 

And, as said before, this is a ___Confrontation Milestone___

## Chapter 5 - Imagination has no age, Dreams are Forever

Now, the PCs will came to Disney, trying to locate the portal opening device.

This will be easy: a ___Good (+3)___ _Awareness_ or _Insight_ roll will show there's some weird movement nearby Cinderella's Castle, on an access only for maintenance people. They are going for the suite Walt Disney planned to be his when Disney World were completed and he never built: he died some time before and the "suite" is empty...

... aside some ___Agents___ with a ___Technician___ that is manipulating a device.

This things starts to escalate as shown by the Countdown and Distraction below (_Fate Adversary Toolkit, p. 23-25_):

> ### The Portal Device
> 
> + ___Choice:___ Fight the Agents or try to engage the Technicians?
> + ___Repercussion (Fight the Agents):___ maybe there'll be no time to defuse the portal device
> + ___Repercussion (try to engage the Technician):___ there's a chance some of them being _taken out_
>
> ### Portal Activation Device ( )( )( )( ) + 1 ( ) per PC
> + ___Obstacle (Defuse):___ ___Fantastic (+6)___ Opposition
> + ___Obstacle (Gain time):___ ___Good (+3)___ Opposition
> + ___Trigger:___ once per turn
> + ___Trigger:___ once the PC take down a group of Agents
> + ___Trigger:___ once the PC take down the Technician
> + ___Outcome:___ the ___Happiest Armageddon on Earth___ starts

Good news, the ___Agents___ here are not too much powerful. Sometimes they attack like a group, but event his will not make them too much powerful to be fought by the PCs. And they are not too much people: one per PC + the Technician.

Bad news, each of them are plugged to the device: each time the PCs take down one of the Agents or the Technician, mark one box in the ___Portal Activation Device___ countdown. Also, mark one per turn the Conflict pass. 

If the PCs take down all the Agents and the Technician, the clock for the Device will still ticks. They can try to either ___Defuse___ the Portal Device (good luck) _and/or_ manipulate the Device to gain time for defuse.

If the PCs tries to _Gain Time_, a successful _Gadgetry_ roll against ___Good (+3)___ allows the PCs to clean one box on the ___Portal Tracking Device___ countdown (two with a ___Success with a Style___). This counts as a _Create Advantage_ action for Stunts use, as they can be done at the same time the try to ___Defuse___ the device

If the PCs tries to _Defuse_, this will be treated as a Contest between them and the ___Defuse___ Obstacle (___Fantastic (+6)___) ___and___ the Countdown will still ticks. 

The Portal Device will activate if:

a. The Countdown track is full
b. The PCs are defeated on the ___Defuse___ Contest;
c. At any time they fail on the _Gain Time_ roll;

If the PCs has the success on defuse, they'll discover that they only ___Reduced their Numbers___ (an Aspect with a Free Invoke): there were similar devices all around Disney, hidden on any ride or attraction you think is good. 

If the PCs wasn't successful (probable result), they'll just have the time to see a portal start to grow in front at Cinderella's Castle. Destroying the device now is no good: a ___Gadgetry___ roll will show that the portal will grow enough to allow the invasion.

The ___Happiest Armageddon on Earth___ just started!

## Chapter 6 - The Happiest Armageddon on Earth

Somewhat no matter they were successful on Defuse the Portal device or not, the invader troops will come: people into a weird cross of Stormtrooper suit and Mickey Mouse. Their numbers are not so great, but they need to be pushed away before the big numbers came.

For emulate this: the first round, there'll be just a Mob of three ___Super-Armed Totalitarian Soldiers from other reality___ for each 2 PCs (so add a (1) stress box for the mob)

However, this is the good news.

Bad news: each round came another group of this. For each round they didn't drop down a Mob, two mobs join each other into a bigger mob, as by rules under _Shadow of the Century_, p. 76

Worst news: their boss came with them.

They don't _Theresa Scrooge_, but she is a _Shadow of the Century_ from Yen Sid's reality, one where the Shadows won big. She is part of the _Bastions_, so is one of the most powerful persons into their reality.

And she's even more powerful: a ___Good (+3)___ _Awareness_ roll will show that she's using a Disney Casting Device, like the ones Yen Sid offered to the team. She looks to everyone and say, into a so powerful voice that there's like Disney speakers had been hacked by her sheer will (somewhat she did it, as now Disneyworld is under _VHS 3_):

> _"People from this reality, I'm Teresa Scrooge, part of the_ Bastion of the United World Government. _You have only one choice: to submit yourself to us. There's no way you can fight our superior technology and power!"_

___Chaos become norm!___

Okay... If the PCs think fast, they'll notice that a battle against the Bastion and National Guard at Disneyworld would be a bloodbath, and one that would not be good.

So it's up to the PCs.

If they are smart enough (or had some good rolls), they can notice that they can use _Disney Casting Device_ for their benefits: by becoming _Disney Characters_, they can (a) calm people down (Overcome the ___Chaos become norm!___) and (b) fight back.

If they use the _Disney Casting Device_, let they play with it, and even let them use ___Supercalifragilisticexpialidocious!___ as a way to power up themselves and to put some mobs on their own by using that power. 

If they doesn't have the idea...

After three or four rounds (time enough there's at least 10  ___Super-Armed Totalitarian Soldiers from other reality___) Theresa will engage her own _Disney Casting Device_, becoming _Maleficent_ into her Dragon form! And she'll turns part of their troops into _Chernabog_ and some of their minions from _A Night on Bald Mountain_ on Fantasia, demons from _Walpurgisnacht_, Witch's Night. This can show the PCs what the can do with the _Disney Casting Device_. 

If they are still without ideas (and, if they get this far without ideas they are really lucky), make Yen Sid and a bald man came there. They'll recognize both of them: Yen Sid and James Marconi. The latter will scream:

> _"She's a Shadow! She's a Shadow from another reality!"_

Theresa will look and say, with a small smile:

> _"So... I recognize you: I killed you on another reality, James Marconi... Or should I say, Rasmus Van Der Merwe... Or even... Nicola Castrogiovanni!"_

If the PCs rolls for an ___Superb (+5)___ roll on _Knowledge_, they'll see: Nicola Castrogiovanni was the _Spirit of Optimism_, but he was given as deceased, and was in the infamous list of hunted Centurions after McCarthy declared The3 Century Club as part of the Red Menace!

But he'll assume his part: he take a swordcane and go for the fight.

And we hope they will have all the luck they need. Faith, Trust and some bit of luck.

## Chapter 7 - Epilogue

We hope the PCs were successful on defeat Teresa Scrooge: if she's to be defeated, she'll retreat and close down the portal. Otherwise, the invaders are now dead or ran away, either back to their reality or on our now.

And the PCs will be cheered as heroes that defeated ___The Happiest Armageddon on Earth___

Some things can happen:

+ If James/Nicola had revealed himself, the PCs could ask him about The Century Club, and he'll say his history and that _"maybe is the time to reopen the Century Club, even that is for the Fight for the Century"_
+ Unintentionally, the events here put NOVA, Yen Sid and the PC on the radar from the biggest groups in the setting: The Board, John Faraday (Methuselah), the Shadows and Kroll'X are all now looking for the PCs. Maybe lurking in the shadows to catch them. Maybe looking for ways to manipulate them for their own profits. Maybe  both.
+ Teresa Scrooge, if survives, will start to prepare a revenge... And there's lots of options for this one, like the plans for Disney parks all around the world, any and all of them potential places for a next try. She's angry, but she's patient, as she won the Century War in her reality, and she has lots of experiences;
+ Albeit not related with Methuselah, maybe Yen Sid is somewhat related. Could Disney Magic and Imagineering be some kind of Methuselah Fragments?

## Appendix 1 - NPCs

### Extras

+ ___Timothy Andalasia, WDW Human Resources Chief___ _Fair (+2)_, ___On a PR hotbed___ _Terrible (-2)_
+ ___Sleeper Agents from Outside our reality___ Average (+1), ___Magic is for morons___ _Terrible (-2)_
+ ___Sleeper Agent Technician___ Good (+3), ___Can't multitask___ _Poor (-1)_
+ ___Super-Armed Totalitarian Soldiers from other reality___ Fair (+2), ___Not too much creative___ _Terrible (-2)_
+ ___Diana McIntosh, Sleeper Agent___ _Fair (+2)_, ___Sometimes shows being too cold___ _Terrible (-2)_

### Sargent Sandra Williams

+ ___Orange PD Detective, Disney Maniac on hiding, NOVA Agent___
    + _**Great (+4)** Detective; **Good (+3)** Hacker, Inventor; **Fair (+2)** Soldier_
    + _Keen Observer_

### "James Marconi" (Nicola Castrogiovanni)

+ _**Spirit of Optimism** (Gonzo, Spirit)_; **"Everything will be fine in the end!" - _Too much naïve_**; _**NOVA will be the New Century Club; Just do what you can, don't whim around; Maybe it's the time to get back**_
  + _**Superb (+5)** Pollyanna; **Great (+4)** Brain, Leader; **Good (+3)** Hacker, Dilettante; **Fair (+2)** Detective_
  + _**Good Enough Game (Gonzo):**_
    + LVL 1: I can do this (_Majored at College_ from Dilettante) 
    + LVL 2: _Pretty Please!_ (_Disarming Charm_ from Dilettante + _Open Book_ from Face)
    + LVL 3: We can do this, Together (_Plan B_ + _Trust Me_ + _Trust Yourself_)
    
### ___Theresa Scrooge, Shadow of Magic from another reality's 20th Century___
+ __*The Shadow Of Magic from Another Reality (Gonzo, Shadow); One of the* Bastions; *Power-mongering; Has lots of Distorted Disney Magic (Gonzo)*; "Imagination?! Imagine *THIS*"__
  + _**Superb (+5)** Authority; **Great (+4)** Mastermind, Scientist; **Good (+3)** Assassin, Soldier; **Fair (+2)** Criminal_
    + Disney Magic;
    + Disney Casting (any Disney Villain for the time - she has a fondness for Maleficent);

## Appendix 2 - NOVA Agency: A _Shadow of the Century_ Organization

NOVA (_Network Of Variable Agents_) was founded by the weird and intelligent guy James Marconi, a computer programmer from the MIT AI Labs. Trying to improve and use the best on the best of the new technologies, James had developed a network of people that works fighting weird menaces. _"Ordinarius populus adversus minas aliena"_, _common people fighting weird menaces_, is the NOVA motto.

One of the strategies of NOVA is based on the idea of _composing cells_, their agents are few and apart and sometimes even unaware about each other: they had been enlisted by James itself or his best agent, called _Alpha_. All agents are notified on-demand about their enlistment for any action, some of them even being put _on hold_ for extra demands. They are paid dearly, obviously: all of them, aside their wage, can ask at anytime a favor that James do his best to achieve (and normally he achieve). 

NOVA Agents comply some rules about discretion and non-violence, but even NOVA knows that sometimes shit goes to the fan, so they have some good military operatives as their roster.

There's two big secrets about NOVA:

1. they are a _Golden Seed_, some money appearing into some James' secret accounts on Luxembourg, Jersey Shore and Switzerland.
2. James Marconi, in fact, "doesn't exist": it's a ___very elaborate, but fake, identity___ of one of the last standing Spirits of the Century, ___Nicola Castrogiovanni, Spirit of Optimism___

Nicola was away from US when the Club disbanded, accused of being part of The Red Danger by McCarthy: it was a prophecy made by an old and now deceased acquaintance of him, Mary Alethea, the Club Retainer that helped him to be found by the 19th Century Spirit of Literacy, _Frederick Van Der Merwe, AKA Don Cagliostro_. He was in South Africa, where Frederick had died in peace on his birth-city Bloemfontein, under another fake identity, technically a great-son of Frederick, _Rasmus Van Der Merwe_, but had to evade South Africa thanks the rising danger of Apartheid and his links (as Rasmus) with the _African National Congress_, the underground party that lost his leader in '62 when Mandela was imprisoned and condemned on the Rivonia Trial.

As part of his actions, Nicola learned a trick or two to stay low profile with lots of fake identities: technically, both Nicola Castrogiovanni and Rasmus Van Der Merwe had died on their homes. James Marconi has no link with none of them, except receiving a good money from a fund created by the Van Der Merwes.

As a Spirit-In-Hiding, he learned how to act in the background, and his optimism showed him that things didn't finished yet: the world is dark and dire, for sure, and the Spirits were dead or in hiding, he was not sure which case, but now he found a new way to fight the fight, to provide ways to make people optimist of staying in the good fight. He will dirty his hand, for sure, but he has the moral high ground for him.

He started by creating a new organization, as soon he noticed the lots of things that was happening in the background, the hidden fight for reality that the fall of the Century Club left humanity without champions: the Kroll'X, the Carthage Manuscripts, all those things that were ripping the reality were bad for everyone. For sure, the Century Club was always part of the problem, but that's the perks on deal with reality shattering things, it was in the job description.

And he received a help: he discovered that a scrumptious value in cash was deposited on his secret accounts in Luxembourg, Switzerland, Hong Kong and Jersey Shore. He discovered, investigating, who did it. It was a _Golden Seed_, an inheritance left behind for him by the deceased Spirit of Trade _Mack Silver_, with some of his _Golden Seed_ texts: he had some disagreements with Silver, to be fair, but he chose to live that inheritance in a good way, to honor his and the other Spirits' sacrifices.

He decided to created a new fake identity, and it was somewhat easy to do this: he undergone a new identity as _James Marconi_, from Wamega, Kansas. He just joined the MIT for some philosophy grad and joined the AI Labs as part of a minor on computer programming, and stayed. He also is known as the name behind an apocryphal zine about _"The Century Club, His Legend and Spirits"_, that put him dangerously on radar. He was ignored just because it was said that it was totally without sources, so a kind of conspiracy theory.

And, under all those skins, he started NOVA.

He bought some years after his first personal computers, a Commodore PET and a Tandy Radio Shack TRS-80 Model I. Also he bought a 300-baud modem and used it to join the NFSnet (via MIT AI Labs) and some BBSes, contacting people, separating wheat from the chaff, and discovering people that had passed by problems, weird ones: a guy that discovered his soon-to-be bride was an insectoid invader; a girl that fought her way against a space and time travelling cyber-hunter; that English reporter that traveled in time and space with a two-hearted guy with a garish big scarf that changed himself when facing death (her words). 

And she found, or better, was found by _Alpha_:

_Alpha_ was a incredible hacker, the _creme-de-la-creme_. Also, was an investigator, and remembered the times the Century Club saved the world from enemies like Gorilla Khan, The March Hare, Jared Brain, Lemont Fitzlefebvre, and Doctor Methuselah. And she was sassy enough to reveal she followed James', in fact, _Nicola_'s breadcrumbs all around the world and time. She assured him it was not that easy, and that even with his apocryphal zine being a somewhat big blunder he should not be worried, she discovered all his timeline, including where and how he "died" from an identity from another, backtracking things to the first time he found Don Cagliostro in the _McNash & Sullivan Circus and Shows_, when he became the little clown _Lil'Nico_ and stopped a rampant lion just _by talking_.

_Alpha_ had a big consideration for the Spirits: her mother was saved by them in the (now washed aside, forgotten and part of legends) _Dinocalypse_ event, and she had studied in the _Cross Engineering School_, where she discovered (and hidden for herself that secret) that the school was commanded by the one and only Spirit of Ingenuity Sally Cross _née_ Slick.

_Alpha_ then exchanged some emails and they came with the project called NOVA, a possible organization for dealing with the weird, but with common people, to be one of the barriers to contain outside invasion from hostile realities that was part of the Century Club job until they were disbanded.

Then, both _Alpha_ and James ran around the world, even behind the Iron Curtain, trying to find people that could help. With time and successes, NOVA made a name: good enough to be risen to a bigger arena, but still low enough to make them stay low profile.

However, more and more eyes are looking for NOVA, and some of them either _Alpha_ and James, or better, _Nicola_ wanted to push away from them.

### Rumors

+ _Alpha_ is in fact a Shadow of the Century: no one knows if she's using Nicola for a hidden and nasty agenda or she's looking for redemption, but looks like she's not someone that is not accomplished to dirty her own hands;
+ James Marconi is NOT Nicola Castrogiovanni: the real Nicola had fell in the hunting for the Centurions, and James is someone that knows his story enough to take his identities.
+ James/Nicola is on an undercover mission to recover the lost glory of the Century Club: he is looking for some of the artifacts that were lost during the Club disbanding, and on all information about the Centurions-In-Ride. Some even suspect he's looking them for a last hurrah that was prophesied by Mary Alethea
+ Some says that NOVA is a kind of selection for the next Centurions
+ NOVA agents, the most experienced ones, had developed by themselves Centurion-like capabilities, and even bigger: their contact with VHS and Methuselah Fragments had changed them in weird ways
+ Although Nicola had been in MIT AI Labs, nowadays he can be found hidden on a circus nearby Omaha, Nebraska. Some says that he can be found also on a small property in Baraboo, Wisconsin, or on any city were the old circuses made their Winter HQs. Some says that his real _sanctum_ is on the old _McNash and Sullivan Circus and Shows_' Winter HQ at Newark, NJ
+ _Alpha_ isn't a human being at all, but one Turing Realistic Artificial Intelligence, programmed by Nicola or downloaded from outside our reality: the main reason she found Nicola is that she knows he's a key part on a future she wants to guide people to
+ Nicola is now on a blacklist: he was seduced by the darkness and became a Shadow, and NOVA is just a pawn he uses to achieve his plans. Something in the past made he grew bitter and cynical and this made him go rogue even before the Century Club banishment;

### NOVA as a Golden Seed

+ ___Agenda:___ to be one of the new barriers protecting humankind against extra-dimensional hazards and invaders, and to deal with weird menaces created by rogue agents
+ ___Resources:___ not too big and very scattered, but this can be seen as an advantage: you can almost certainly discover a NOVA agent and resources somewhere, given time
+ ___Patron:___ Being part of NOVA network can be a great way for those who like to be on the edge, because lots of time NOVA deals with the Gonzo and the weird, and is somewhat safe, as they like to have agents on places where they can help and are very sensible with classified intel. And there's The Favor you can ask and NOVA will do its best to get it solved
+ ___Ally:___ As totally scattered, maybe NOVA is not a good ally, as finding a NOVA agent is really complicated, even by those inside the Agency, beside James and _Alpha_. However, once you get an ally inside, you have access on, given time, lots of intel, resources and equipment
+ ___Contact:___ Same as Ally, NOVA is not exactly the best contact for those in a hurry, although if you can wait it's an incredible information source
+ ___Foil:___ NOVA isn't that jealous on their mission to think it as exclusive. However, they can be very touchy if you just try to mess on their missions. Normally, they put things on the shelf until the main problem is solved... And then they go as full as they can to hit you hard if they think you messed things
+ ___Enemy:___ if you think NOVA can be taken as a bunch of morons based on their loose structure, think it again: NOVA agents are trained to notice weird things that can demand their attention. In fact, if a NOVA agent notice YOU, and you are doing naughty things, NOVA will hit you so hard that you'll want that you'll want they didn't notice you.

### Face: Alpha

Normally people can't see Alpha if she doesn't want, and even so she pass as a very common punk girl with electrical green hair: just in the BBS and NSFnet (the predecessor of Internet) people knows about her. 

+ ___Grll Power Uber-Hacker; She knows too much about the Century Club; More friends (and enemies) that she knows___
  + ___Great (+4)___ Hacker; ___Good (+3)___ Saboteur, Detective; ___Fair (+2)___ Dilettante
  + Nice Computer

### Face: "James Marconi" (Nicola Castrogiovanni)

No one could associate the somewhat chubby and bald headed computer store owner and former MIT AI Labs employee James Marconi with _Lil'Nico_, the Vaudeville Angel from 1910s Circus and Vaudeville called Nicola Castrogiovanni.

+ _**Spirit of Optimism** (Gonzo, Spirit)_; **"Everything will be fine in the end!" - _Too much naïve_**; _**NOVA will be the New Century Club; Just do what you can, don't whim around; Maybe it's the time to get back**_
  + _**Superb (+5)** Pollyanna; **Great (+4)** Brain, Leader; **Good (+3)** Hacker, Dilettante; **Fair (+2)** Detective_
  + **Mr Underhill:** his actual identity James Marconi; 
	+ _Computer Scientist on a post-Grad at MIT AI Labs_; 
	+ _A backstory that doesn't survive a DMV query_
  + **Mr Underhill:** Rasmus Van Der Merwe
	+ _Social Scientist from Bloenfontein, South Africa_
	+ _"Nephew" of one of African National Congress founders_
  + _**Good Enough Game (Gonzo):**_
    + LVL 1: I can do this (_Majored at College_ from Dilettante) 
    + LVL 2: _Pretty Please!_ (_Disarming Charm_ from Dilettante + _Open Book_ from Face)
    + LVL 3: We can do this, Together (_Plan B_ + _Trust Me_ + _Trust Yourself_)

## Appendix 3 - Yen Sid, Herald of Disney Magic

![](https://vignette.wikia.nocookie.net/disney-magical-world/images/9/9c/DMW2_-_Master_Yen_Sid.jpg/revision/latest?cb=20151106154019)


Since it's opening, there's a small apartment hidden at Disneyland, on the Firehouse at Main Street. This apartment was used by Walt Disney, to see his dreams growing and growing. With time and Walt Disney passing, a lamp was placed to show that Disney spirit is still there, looking upon to maintain and raise Disney Magic.

Some special few people knows that this is even truther than people could think:

At December 15th, 1976, some people had gone to look for Walt's apartment periodic refurbish, when they saw a man that... There was no other way to describe... looked like jumped straight from _Sorcerer's Apprentice_ sequence from _Fantasia_: a strong, non-nonsense face with big white beard and hair, using blue robes and a certain, very iconic hat.

It was when he revealed himself for some of the managers as _Yen Sid_, Master Magician and Herald of _Disney Magic_, that came from another reality where the Disney Magic was suppressed by an evil, dystopian version of US government, that suppressed all kind of magic and happiness beside an ultra-patriotism that forced US into a perpetual war by heating the Cold War until there was only _War!_ against everyone else, the pacifist and dissident voices crushed like a pulp.

_Yen Sid_ had then used his magics to avoid being catched by some of his enemies from another dimension, coming till that one. Where he discovered that many things that resulted on his dystopian reality already happened, like the Century Club disbanding, although other had not happened.

Like Disney being so well-succeeded and made some inspiring place like Walt Disney World and Disneyland.

He decided to stay here: he believe that his magics could not bring him back his home dimension, so he only can believe that it was reduced to oblivion. And this would not happen on this place.

And to do this, he would help to grow Disney dream of peace and laughter.

Although his strong, serious, non-nonsense face, he has a so big heart that he run everywhere, looking for people in need of help. He try to do his best, even knowing that not all places accept his _Disney Magic_ (he calls it this way). He can, however, be fierce and even furious on the right (Wrong?) place and time, and those rare people that saw him on this could see he is fire and fury when people mess with his mission.

He can be found at Disney Parks, where he can just Teleport from one another using some magics and gateways unknown for everyone except a very rare few (Eisner included), and he works by improving the Disney Magic, either by helping training new Cast Members to hearing and supporting the _Imagineers_ to using his magics (that he uses only in the direst circumstances).

### Aspects

+ *__High Concept:__ Disney Magic Personification* (Gonzo - As a _Spirit_)
+ *__Trouble:__ Strong Face, Big Heart*
    + _Walt Disney Alter-Ego?_
    + _"You can Dream it, you can do it"_
    + _"Laugh is timeless, dreams are forever"_

### Roles

+ *__Disney Magic (Gonzo)__ Superb (+5); __Inventor, Leader__ Great (+4); __Brain, Face__ Good (+3); __Dilettante__ Fair (+2)*

### Stunts

+ _**Imagineer (LVL 1):**_ as by _Weird Magic_ but uses _Knowledge_ instead _Gadgetry_ (Yen Sid uses _Disney Magic_ role for this);
+ _**You Can Fly! (LVL 2):** +2 on all **Imagineer** rolls. However, any use of the generated gadget can raise local VHS to 2. Failure gives two boosts on any Aspect for the GM **and** allow him to put an extra **Trouble** Aspect. Also, he can do some kind of "special effects", by changing the overall appearance of someone else as a_ Create Advantage _by Knowledge (difficulty based on the changes or on target_ Will _if a non-voluntary target)_
+ _**Bibbity Bobbity Boo! (LVL 3):** +2 on all **You Can Fly!** rolls, cumulative with **Imagineer** (total +4 for **Imagineer**). However, any use of the generated gadget can raise local VHS to 3. Failure gives two boosts on any Aspect for the GM **and** allow him to put an extra **Trouble** Aspect, cumulative with **You Can Fly!** Also, his **You can Fly!** abilities grow to change almost anything on a target, even imbuing them with part of Disney Magic, the so-called_ Casting. _This, however, can't be used against a non-voluntary target_ at all _and that the changes can't be **permanent**, although they can be **indefinite**. This last case, he is obliged to put some **escape clause** that is plausible to be undergone so the target can get away from it_.

### Notes

As the _Spirits_ and _Shadows_, _Yen Sid_ can raise the VHS level of a region. However, this is limited by some geographic bounds (specially those related with Disney parks)

### Disney Magic (Gonzo) Role

_Disney Magic_. Since the past this is the term used by some on how use reality to bring fantasy to life. Walt Disney was one of the best on this art. Not all shows were commercial success, at least at opening: _Alice in Wonderland_ and _Fantasia_ are the main examples of those. However, Disney Magic became more than an idea.

Those who uses _Disney Magic_, normally called _Imagineers_, dance in the blade of VHS to do impossible things with the possible. They look forward and advance technique and technology to the skies to bring magic to life.

+ *__Skills:__ Knowledge, Gadgetry, Awareness, Insight*

### Disney Magic Gonzo Stunt

+ _**Imagineer (LVL 1):**_ as by _Weird Magic_ but uses _Knowledge_ instead _Gadgetry_ (Yen Sid uses _Disney Magic_ role for this);
+ _**You Can Fly! (LVL 2):** +2 on all **Imagineer** rolls. However, any use of the generated gadget can raise local VHS to 2. Failure gives two boosts on any Aspect for the GM **and** allow him to put an extra **Trouble** Aspect. Also, he can do some kind of "special effects", by changing the overall appearance of someone else as a_ Create Advantage _by Knowledge (difficulty based on the changes or on target_ Will _if a non-voluntary target)_ (Yen Sid uses _Disney Magic_ role for this);
+ _**Bibbity Bobbity Boo! (LVL 3):** +2 on all **You Can Fly!** rolls, cumulative with **Imagineer** (total +4 for **Imagineer**). However, any use of the generated gadget can raise local VHS to 3. Failure gives two boosts on any Aspect for the GM **and** allow him to put an extra **Trouble** Aspect, cumulative with **You Can Fly!** Also, his **You can Fly!** abilities grow to change almost anything on a target, even imbuing them with part of Disney Magic by putting into them the_ Disney Casting _Gonzo Stunt pack. This, however, can't be used against a non-voluntary target_ at all _and that the changes can't be **permanent**, although they can be **indefinite**. This last case, he is obliged to put some **escape clause** that is plausible to be undergone so the target can get away from it_. (Yen Sid uses _Disney Magic_ role for this)

### Disney Cast Role (Gonzo)

+ _**Skills:** Knowledge, Awareness, Persuade, Insight_

#### Disney Casting Gonzo Stunt

+ _**It's a Jolly Holiday (LVL 1):** When buying this, choose one of those skills, as far there's part of your Roles Skill: Knowledge, Awareness, Persuade. You can use them instead of Insight to Create Advantages by discovering Aspects related on what would make people happy_
+ _**Hot Dogs! (LVL 2):** You can immerse yourself into the character you are associated for: it combos the Stunts_ A Likely Story _and_ Seems Legit. _Good News: you roll only if Persuade is less than opposition+2. However, as soon you "reveal" an Aspect as by_ A Likely Story, _it can be Invoked or Forced against you for narrative, as it was one of the Character Aspects (for example, a White Rabbit can be Forced into his **I'm Late for a very Important Date** to be confused on what to do). This stays during the scene it was generated or until removed with a Will roll, difficulty being the Persuade level of the character_
+ _**Supercalifragilisticexpialidocious! (LVL 3):** This is a really overwhelming Stunt: to activate it, aside any other costs, the character needs to suffer a Consequence_ "Under the Character", _the level indicating how far this Stunt still be held on: Mild for a Scene, Moderate for a Episode, Severe for a Movie (you can't make this goes during a full Season or during a Series). But the power can compensate: first by doing this, the PC sends the local VHS around him to 3 automatically, and any try to reduce VHS by Shadows could be done only if they roll against the character Will as a Contest. Second: during the power use, the PC choose two Setting Aspects representing new truths in the setting, that can override almost any other Aspects beside those from Shadows or Methuselah Fragments. The GM can choose a_ Trouble _Aspect also. Third: any Aspect generated on montages that can be linked to the Character the PC is associated for can be invoked for +3 instead for +2 per boost/FP. This can't be used again while the character is "healing" the_ "Under the Character" _Consequence, and he can only start to heal the Consequence as soon this use expires. To finish, the character can make people getting into his "fantasy", treating them as Mooks with a **Good (+3)** level Aspect related to the character (example: a Peter Pan character can turn people around into **Lost Boys**). The Effects passes as far the PC is took out from a Conflict or is knock out or rendered unconscious on any way_

<!--  LocalWords:  Ordinarius populus adversus minas aliena Andalasia
 -->
<!--  LocalWords:  CSI McLane fark Steinberg Dwarves Tremaine Jaq Toc
 -->
<!--  LocalWords:  chalantly Drizella bodyframe ideary AdMontage FP
 -->
<!--  LocalWords:  animatronic Hyperdimentional informations uber Der
 -->
<!--  LocalWords:  Dimentional conspirational Disneyworld Chernabog
 -->
<!--  LocalWords:  Imagineering Stormtrooper Rasmus Kroll'X WDW LVL
 -->
<!--  LocalWords:  Supercalifragilisticexpialidocious Alethea Rivonia
 -->
<!--  LocalWords:  Merwes Wamega Tandy TRS NFSnet insectoid cyber de
 -->
<!--  LocalWords:  hearted Lil'Nico née Baraboo HQs NSFnet Grll
 -->
<!--  LocalWords:  dystopian Imagineers Imagineer Bibbity Bobbity
 -->
