---
#subheadline: The Blueblood lady that can deal everything for the needed love from _Dystopian Universe_
title: Thomaśz "Thomas" Kozlowski
layout: personagens
categories:
     - personagens
tags:
     - Shadow of the Century
language: br
header: no
---


## Aspectos

|              ***Tipo*** | ***Aspectos***                              |
|------------------------:|:--------------------------------------------|
|            **Conceito** | Ex-espião do outro lado da Cortina de Ferro |
|         **Dificuldade** | Sabe demais para seu próprio bem            |
|    **Chamado às Armas** | Caçado por acusações falsas                 |
| **No Campo de Batalha** | Pessoas sempre podem se corromper           |
|   **Formando a Equipe** | Talvez eles sejam minha última opção        |

## Papéis

| ***Papel*** | ***Bizarro?*** | ***Perícias***                                    |
|------------:|:--------------:|---------------------------------------------------|
|   Sabotador | N              | Roubo, Engenhocas, Conhecimentos, Furtividade     |
|     Cérebro | N              | Computadores, Engenhocas, Intuição, Conhecimentos |
|      Espião | N              | Roubo, Contatos, Recursos, Furtividade            |

## Perícias

|     ***Perícia*** | ***Nível***   |
|------------------:|---------------|
|            Atirar |               |
|         Atletismo |               |
| **Conhecimentos** | Bom (+3)      |
|      **Contatos** | Regular (+1)  |
|  **Computadores** | Regular (+1)  |
|           Direção |               |
|    **Engenhocas** | Ótimo (+4)    |
|   **Furtividade** | Razoável (+2) |
|      **Intuição** | Razoável (+2) |
|             Lutar |               |
|         Percepção | Razoável (+2) |
|     **Persuasão** | Razoável (+2) |
|          Provocar |               |
|         **Roubo** | Ótimo (+4)    |
|      **Recursos** | Regular (+1)  |
|           Vontade |               |

## Façanhas [Recarga: 2]

+ ***Sempre é o fio vermelho:*** pode desarmar qualquer explosivo sem precisar rolar dados desde que seu nível em *Engenhocas* seja menor que a oposição
+ ***Sabotador experiente:*** você é familiar com todo tipo de bombas, explosivos, armadilhas e outras coisas que Anarquistas usam contra O Chefe. Você pode usar *Conhecimentos* como uma ação de *Superar* para descobrir mais sobre o alvo, com dificuldade dependendo do quão conhecido é o criador do dispositivo explosivo
+ ***Empatia com Máquinas:*** se ficar por alguns instantes estudando alguma coisa tecnológica que não conheça, recebe uma Invocação Gratuita em um Aspecto que ela tenha ou em um Aspecto novo (se ela não tiver nenhum)
