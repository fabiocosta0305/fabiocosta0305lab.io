---
#subheadline: The Blueblood lady that can deal everything for the needed love from _Dystopian Universe_
title: José "Dash" Marques
layout: personagens
categories:
     - personagens
tags:
     - Shadow of the Century
language: br
header: no
---


## Aspectos

|              ***Tipo*** | ***Aspectos***                                                |
|------------------------:|:--------------------------------------------------------------|
|            **Conceito** | Piloto extremamente capaz                                     |
|         **Dificuldade** | Não sei quando parar                                          |
|    **Chamado às Armas** | Uma carreira nas pistas interrompida                          |
| **No Campo de Batalha** | Se tem motor, eu piloto. Se não tem, _pode ser_ que eu pilote |
|   **Formando a Equipe** | O cara da fuga                                                |


## Papéis

| ***Papel*** | ***Bizarro?*** | ***Perícias***                             |
|------------:|:--------------:|--------------------------------------------|
|   Diletante | N              | Computadores, Direção, Persuasão, Recursos |
|      Ladrão | N              | Percepção, Roubo, Recursos, Furtividade    |
|      Piloto | N              | Atirar, Direção, Percepção, Provocar       |

## Perícias

|    ***Perícia*** | ***Nível***      |
|-----------------:|------------------|
|       **Atirar** | Regular (+1)     |
|        Atletismo |                  |
|    Conhecimentos | Regular (+1)     |
|         Contatos |                  |
| **Computadores** | Regular (+1)     |
|      **Direção** | Excepcional (+5) |
|       Engenhocas |                  |
|  **Furtividade** | Regular (+1)     |
|         Intuição | Bom (+3)         |
|            Lutar |                  |
|    **Percepção** | Bom (+3)         |
|    **Persuasão** | Razoável (+2)    |
|         Provocar |                  |
|        **Roubo** | Regular (+1)     |
|     **Recursos** | Razoável (+2)    |
|          Vontade |                  |

## Façanhas [Recarga: 2]

+ ***Tudo tem um preço:*** Caso falhe em um teste de *Persuasão* contra um NPC, você pode usar *Recursos* no lugar de *Persuasão* contra o mesmo NPC e dificuldade (se for ativa, mesmo resultado do rolamento anterior). Entretanto, mesmo que você tenha um sucesso com estilo, essa ação vai ter um custo. Os demais PCs podem usar essa Façanha também.
+ ***Itens Essenciais:*** você mantêm uma mochila com uma série de coisas essenciais para caso precise “dar um perdido”. Uma vez por episódio, diga ao Narrador *“Vou pegar meus Itens Essenciais”*, para obter um aspecto ***Itens Essenciais*** com três invocações gratuitas, desde que seja plausível o fazer;
+ ***Carango Maneiro:*** Você possui um carro especial. Dê a ele um ***Conceito*** e uma ***Dificuldade***. Você e qualquer outra pessoa recebe +1 em todos os testes para o operar. *Uma Vez Por Episódio*, pague 1 Ponto de Destino para adicionar um terceiro Aspecto no mesmo com uma Invocação Gratuita, representando alguma capacidade do mesmo até então escondida. Esse Aspecto desaparece ao final da Cena.
