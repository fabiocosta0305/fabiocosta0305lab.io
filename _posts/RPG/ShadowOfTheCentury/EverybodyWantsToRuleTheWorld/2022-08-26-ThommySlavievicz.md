---
#subheadline: The Blueblood lady that can deal everything for the needed love from _Dystopian Universe_
title: Thommy “Tom” Slavievicz
layout: personagens
categories:
     - personagens
tags:
     - Shadow of the Century
language: br
header: no
---


## Aspectos

|              ***Tipo*** | ***Aspectos***                                                |
|------------------------:|:--------------------------------------------------------------|
|            **Conceito** | Ex-militar, baterista durão                                   |
|         **Dificuldade** | Nascido nos Estados Unidos, Forjado no Afeganistão            |
|    **Chamado às Armas** | Estou cansado de tiros e bombas e sangue                      |
| **No Campo de Batalha** | _Ba-dum-tsi_ contra o _Ratatata_ na minha mente               |
|   **Formando a Equipe** | Alguém precisa proteger essa turma, eles são bonzinhos demais |

## Papéis

| ***Papel*** | ***Bizarro?*** | ***Perícias***                                |
|------------:|:--------------:|-----------------------------------------------|
|   Guerreiro | N              | Lutar, Provocar, Atirar, Vontade              |
|      Brigão | N              | Atletismo, Lutar, Provocar, Vontade           |
|   Sabotador | N              | Roubo, Engenhocas, Conhecimentos, Furtividade |
	
## Perícias

|     ***Perícia*** | ***Nível***   |
|------------------:|---------------|
|        **Atirar** | Bom (+3)      |
|     **Atletismo** | Regular (+1)  |
| **Conhecimentos** | Regular (+1)  |
|          Contatos |               |
|      Computadores |               |
|           Direção |               |
|    **Engenhocas** | Regular (+1)  |
|   **Furtividade** | Regular (+1)  |
|          Intuição | Razoável (+2) |
|         **Lutar** | Ótimo (+4)    |
|         Percepção |               |
|         Persuasão | Regular (+1)  |
|      **Provocar** | Bom (+3)      |
|         **Roubo** | Regular (+1)  |
|          Recursos |               |
|           Vontade | Razoável (+2) |

## Façanhas [Recarga: 3]

+ ***Força Irresistível:*** +2 ao remover Aspectos usando sua força física
+ ***Venha Brincar:*** *Uma vez por episódio*, ao desafiar um NPC para uma luta mano-a-mano, ele é obrigado a aceitar e você recebe uma Invocação Gratuita em cima dele no Conflito 
+ ***Nivelar:*** Pode usar *Conhecimentos* no lugar de outras perícias para dobrar alguém da alta sociedade. Funciona apenas uma vez contra uma pessoa específica e você fica marcado como alguém rude
