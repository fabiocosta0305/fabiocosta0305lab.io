---
#subheadline: The Blueblood lady that can deal everything for the needed love from _Dystopian Universe_
title: Priscilla “Prizz” McKinnon
layout: personagens
categories:
     - personagens
tags:
     - Shadow of the Century
language: br
header: no
---


## Aspectos

|              ***Tipo*** | ***Aspectos***                                                              |
|------------------------:|:----------------------------------------------------------------------------|
|            **Conceito** | Policial da SWAT e Saxofonista Amadora                                      |
|         **Dificuldade** | Tapando buraco                                                              |
|    **Chamado às Armas** | Sempre faz aquilo que é necessário                                          |
| **No Campo de Batalha** | Saxofones são ótimas armas brancas e suas caixas, esconderijos para objetos |
|   **Formando a Equipe** | Conheço essa turma de outros carnavais                                      |


## Papéis

| ***Papel*** | ***Bizarro?*** | ***Perícias***                           |
|------------:|:--------------:|------------------------------------------|
|    Policial | N              | Atletismo, Direção, Atirar, Vontade      |
|    Detetive | N              | Percepção, Contatos, Intuição, Persuasão |
|     Soldado | N              | Atletismo, Lutar, Atirar, Vontade        |
	
## Perícias

|    ***Perícia*** | ***Nível***   |
|-----------------:|---------------|
|       **Atirar** | Razoável (+2) |
|    **Atletismo** | Razoável (+2) |
|    Conhecimentos | Razoável (+2) |
|     **Contatos** | Razoável (+2) |
| **Computadores** | Regular (+1)  |
|          Direção | Regular (+1)  |
|       Engenhocas |               |
|      Furtividade |               |
|     **Intuição** | Ótimo (+4)    |
|        **Lutar** | Regular (+1)  |
|    **Percepção** | Bom (+3)      |
|    **Persuasão** | Razoável (+2) |
|         Provocar |               |
|            Roubo |               |
|         Recursos |               |
|      **Vontade** | Razoável (+2) |

## Façanhas [Recarga: 3]

+ ***Oficial em Perseguição:*** Quando perseguir alguém a pé, você já começa a Disputa com uma Vitória
+ ***Informante:*** Uma vez por episódio, você pode declarar que conhece alguém (normalmente um bandido pé de chinelo) que pode ter informação útil. Defina o apelido desse informante: ele entra como um Aspecto em jogo com uma Invocação Gratuita e permanece em jogo até o fim do episódio;
+ ***Observador Sagaz:*** Ao ver uma pessoa pela primeira vez você pode usar *Percepção* ou *Intuição* para tentar descobrir algum Aspecto relacionado a maneirismos, vestimentas, postura ou outros sinais visíveis do alvo sem gastar nenhuma ação
