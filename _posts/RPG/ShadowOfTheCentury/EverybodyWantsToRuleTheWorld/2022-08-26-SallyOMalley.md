---
#subheadline: The Blueblood lady that can deal everything for the needed love from _Dystopian Universe_
title: Sally “Boss” O'Malley
layout: personagens
categories:
     - personagens
tags:
     - Shadow of the Century
language: br
header: no
---


## Aspectos

|              ***Tipo*** | ***Aspectos***                                                     |
|------------------------:|:-------------------------------------------------------------------|
|            **Conceito** | A empresária que garante todos os shows                            |
|         **Dificuldade** | Deixou uma bela cota de ressentimentos (e inimigos) por aí         |
|    **Chamado às Armas** | Conheço um cara que Conhece um Cara... E vou usar isso a meu favor |
| **No Campo de Batalha** | A Melhor Fita Demo de 1983                                         |
|   **Formando a Equipe** | Vamos chegar todos juntos ao Topo da MTV                           |


## Papéis

| ***Papel*** | ***Bizarro?*** | ***Perícias***                                    |
|------------:|:--------------:|---------------------------------------------------|
|     Cérebro | N              | Computadores, Engenhocas, Intuição, Conhecimentos |
|    Detetive | N              | Percepção, Contatos, Intuição, Persuasão          |
|   Diletante | N              | Computadores, Direção, Persuasão, Recursos        |

## Perícias

|     ***Perícia*** | ***Nível***      |
|------------------:|------------------|
|            Atirar |                  |
|         Atletismo |                  |
| **Conhecimentos** | Regular (+1)     |
|      **Contatos** | Excepcional (+5) |
|  **Computadores** | Ótimo (+4)       |
|       **Direção** | Regular (+1)     |
|    **Engenhocas** | Regular (+1)     |
|       Furtividade |                  |
|      **Intuição** | Razoável (+2)    |
|             Lutar |                  |
|     **Percepção** | Razoável (+2)    |
|     **Persuasão** | Bom (+3)         |
|          Provocar |                  |
|             Roubo |                  |
|      **Recursos** | Regular (+1)     |
|           Vontade |                  |

## Façanhas [Recarga: 3]

+ ***Na mente deles:*** Pode usar *Intuição* para tentar prever os Próximos passos de alguém que tenha encontrado previamente
+ ***Tradutor Universal:*** você não precisa rolar para falar e ler qualquer idioma que seja razoável de conhecer. Caso contrário, você rola contra dificuldade *Ótima (+4)*
+ ***Computador Sofisticado:*** Qualquer um que use seu *Computador* ao realizar testes de *Computador* soma +2 ao rolamento se você perguntar o que ele está fazendo
