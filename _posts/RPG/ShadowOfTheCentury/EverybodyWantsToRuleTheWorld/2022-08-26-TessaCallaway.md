---
#subheadline: The Blueblood lady that can deal everything for the needed love from _Dystopian Universe_
title: Tessa “Tess” Callaway
layout: personagens
categories:
     - personagens
tags:
     - Shadow of the Century
language: br
header: no
---


## Aspectos

|              ***Tipo*** | ***Aspectos***                                                 |
|------------------------:|:---------------------------------------------------------------|
|            **Conceito** | A Vocalista Líder da Banda                                     |
|         **Dificuldade** | Convencida demais para seu próprio bem                         |
|    **Chamado às Armas** | Acredito no poder da música                                    |
| **No Campo de Batalha** | A Faculdade foi legal, mas não nasci para ser atendente de bar |
|   **Formando a Equipe** | Nos conhecemos desde sempre                                    |

## Papéis

| ***Papel*** | ***Bizarro?*** | ***Perícias***                               |
|------------:|:--------------:|----------------------------------------------|
|       Rosto | N              | Contatos, Intuição, Persuasão, Provocar      |
|   Diletante | N              | Computadores, Direção, Persuasão, Recursos   |
|       Líder | N              | Intuição, Conhecimentos, Persuasão, Recursos |


## Perícias

|     ***Perícia*** | ***Nível***      |
|------------------:|------------------|
|            Atirar |                  |
|         Atletismo |                  |
| **Conhecimentos** | Regular (+1)     |
|      **Contatos** | Regular (+1)     |
|  **Computadores** | Regular (+1)     |
|       **Direção** | Regular (+1)     |
|        Engenhocas | Regular (+1)     |
|       Furtividade |                  |
|      **Intuição** | Excepcional (+5) |
|             Lutar |                  |
|         Percepção | Razoável (+2)    |
|     **Persuasão** | Bom (+3)         |
|      **Provocar** | Regular (+1)     |
|             Roubo |                  |
|      **Recursos** | Razoável (+2)    |
|           Vontade | Bom (+3)         |

## Façanhas [Recarga: 3]

+ ***Na Maciota:*** Ao usar *Persuasão* ao convencer os outros durante uma Montagem, recebe uma Invocação Gratuita/Vitória Adicional
+ ***Líder pelo Exemplo:*** Escolha um Aliado ao fim da cena: se ele usar a mesma perícia para fazer a mesma coisa na próxima rodada, ele recebe +2 no rolamento
+ ***Charme Acachapante:*** *Uma vez por episódio*, você pode impedir que o Narrador invoque um Aspecto de um NPC que seja contrário a você
