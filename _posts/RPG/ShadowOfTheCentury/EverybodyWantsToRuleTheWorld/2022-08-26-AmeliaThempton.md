---
#subheadline: The Blueblood lady that can deal everything for the needed love from _Dystopian Universe_
title: Amelia “Amy” Thempton
layout: personagens
categories:
     - personagens
tags:
     - Shadow of the Century
language: br
header: no
---


## Aspectos

|              ***Tipo*** | ***Aspectos***                                                   |
|------------------------:|:-----------------------------------------------------------------|
|            **Conceito** | O pau-para-toda-obra do grupo                                    |
|         **Dificuldade** | Um passado em Quantico – temporariamente afastada do FBI         |
|    **Chamado às Armas** | Preciso de um tempo depois da catástrofe da última missão        |
| **No Campo de Batalha** | Seis idiomas e um belo conhecimento da _realpolitik_             |
|   **Formando a Equipe** | Ficar na maciota, sem precisar salvar o mundo, é bom para variar |


## Papéis

| ***Papel*** | ***Bizarro?*** | ***Perícias***                             |
|------------:|:--------------:|--------------------------------------------|
|   Diletante | N              | Computadores, Direção, Persuasão, Recursos |
|      Ladrão | N              | Percepção, Roubo, Recursos, Furtividade    |
|      Espião | N              | Roubo, Contatos, Recursos, Furtividade     |
	
## Perícias

|    ***Perícia*** | ***Nível***   |
|-----------------:|---------------|
|           Atirar |               |
|        Atletismo |               |
|    Conhecimentos | Bom (+3)      |
|     **Contatos** | Razoável (+2) |
| **Computadores** | Bom (+3)      |
|      **Direção** | Regular (+1)  |
|       Engenhocas |               |
|  **Furtividade** | Razoável (+2) |
|         Intuição |               |
|            Lutar |               |
|    **Percepção** | Regular (+1)  |
|    **Persuasão** | Regular (+1)  |
|         Provocar |               |
|        **Roubo** | Bom (+3)      |
|     **Recursos** | Ótimo (+4)    |
|          Vontade |               |

## Façanhas [Recarga: 3]

+ ***Método de Atuação:*** Quando passando-se por outra pessoa, substitua seu *Conceito* por um apropriado referindo-se à pessoa substituída. Quando realizando ações que possam ser justificadas por esse *Conceito*, ao invés de usar suas perícias você pode rolar usando um nível *Bom (+3)*
+ ***Molhando as mãos certas:*** Durante uma Montagem, quando usando Recursos para subornar um alvo e for bem-sucedido, você obtêm uma invocação adicional em qualquer Aspecto gerado. Se Montando uma Disputa, você pode sacrificar a invocação adicional para obter uma vitória adicional.
+ ***Itens Essenciais:*** você mantêm uma mochila com uma série de coisas essenciais para caso precise “dar um perdido”. Uma vez por episódio, diga ao Narrador *“Vou pegar meus Itens Essenciais”*, para obter um aspecto ***Itens Essenciais*** com três invocações gratuitas, desde que seja plausível o fazer
