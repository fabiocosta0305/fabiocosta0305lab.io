---
title: "Lendas para #iHunt"
subheadline: "O Folclore Brasileiro procura provocar sua Luta Justa em #iHunt"
layout: post
categories:
 - Fate
 - Fate Básico
 - iHunt
 - Clados
 - Subclados
 - Lendas
comments: true
tags:
 - Fate
 - Fate Básico
 - iHunt
 - Clados
 - Subclados
 - Lendas
 - cenarios
header: no
excerpt_separator: <!-- excerpt -->
---

O Brasil é cheio de lendas sobre criaturas estranhas e bizarras, algumas delas contadas como "lendas fofas", outras como monstros psicopatas, sendo que na realidade elas não são nem uma coisa e nem outra... (ao menos não normalmente).

Capelobos, Anhangás, Bradadores, Corpos Secos, Iaras e Botos, Caiporas e Curupiras...

Todas essas lendas existem por aí

E costumam ser um grande problema para \#iHunters, tanto gringos (por esperarem os tradicionais vampiros e afins) quanto para Brasileiros (devido aos regionalismos específicos relacionados a essas lendas)

<!-- excerpt -->

## Fatos

+ ***Tabus:*** em geral, toda Lenda possui algum tabu sobre coisas que ela deve fazer ou não pode fazer. Em geral envolvem restrições a alvos ou sacrifícios que possam os apaziguar, mas em geral são bem aderentes a tais tabus
+ ***Variedade:*** quase toda Lenda é única de uma região, mesmo existam variantes por todo o país com capacidades similares

## Mitos

+ ***Símbolos Sagrados e Ritos Religiosos:*** Exceto em casos muito específicos, quando os tabus de uma Lenda entra nesse tópico, Lendas não são mais ou menos incomodados por símbolos sagrados ou por fé demais que uma pessoa comum. Claro, um pastor gritando vai os incomodar, mas quem nunca teve a vontade de arremessar um sapato em um pastor, como se faria com um gato enchendo o saco à noite?
+ ***Naturalmente malignos:*** Isso é propaganda colonialista contra as Lendas: algumas podem até mesmo frequentar igrejas calmamente! Enfia seus tabus e preconceitos religiosos do século 19 no cu! Além disso, que culpa eles têm se caçadores e escravistas costumavam ser cuzões?
+ ***Restrições Geográficas:*** No passado isso se aplicava, mas atualmente são comuns Lendas de certas regiões irem para outras, seja apenas pela proliferação de suas histórias como Lendas Urbanas, seja fisicamente falando. Afinal de contas, a não ser em certos casos específicos, como os dos Botos, o transporte público tá ai para todo mundo, e mesmo Negrinhos podem atualmente deixar de lado seus cavalos e "dar grau" em todo o país.

Lendas não possuem um pacote básico graças à sua variedade, sendo então condensados por seus Sub-Clados

## Subclados

### Botos (Custo: 5)

Os "Comedores de Casadas" do Folclore Brasileiro, acredita-se que saiam dos rios em noites quentes de luar para invadir festas e, com isso, deitar-se com jovens virgens (ou mulheres em geral) e desse modo proverem filhos. 

A realidade é que eles são metamorfos aquáticos de maneira geral, mas apenas conseguem se reproduzir em sua forma humana. Podem sair dos rios quantas vezes necessário, mas a cada 24 horas precisam mudar novamente às suas formas aquáticas, passando ao menos 8 horas em tais formas. Só podem permanecer em corpos de águas limpos: águas poluídas lhes são letais. Entretanto, não precisam de água corrente: mesmo que desconfortável, uma banheira grande cheia de água limpa é o suficiente para que eles possam ficar. 

Nem todos os botos são _boys_ lixo por padrão, mas definitivamente todos são bonitos. A única forma de os detectar é ver o furo de boto no alto de suas cabeças: isso explica porque todos eles sempre cobrem a cabeça com algum chapéu ou outros acessórios.

+ ***Características:*** Metamorfose: Forma Animal - Boto (2); Tentador (2);
+ ***Dons:*** Alucinações: Falha na Matrix (1), Influenciar Emoções: A Sós (1)
+ ***Fraquezas:*** Preso a um local: precisa permanecer por ao menos 8 horas em um período de 24 horas em grandes corpos de água (Tamanho mínimo de uma piscinha grande desmontável ou uma banheira residencial grande - U0, P1)

---

### Flor do Mato (Custo: 10)

Protetoras de certas matas e bosques do agreste, são voluntariosas e muito travessas, assustando os animais ou fazendo com que atraiam caçadores ou assuste aqueles que entrem em suas matas, perturbando-os com o barulho de animais que ela imita muito bem. Entrentanto, se lhe for oferecidas oferendas em fumo, mel, leite fresco ou aveia pode ajudar aqueles que fazem tais oferendas, orientando animais a facilitarem sua captura ou guiando perdidos pelos caminhos.

+ ***Características:*** Mente Bizarra (2), Rápido pra Caralho (2), Controle Animal (3), Contato de Essência: recupera 1 de Essência a cada dia em seu território (1), Sacrifícios: Fumos e Alimentos específicos (1), Reserva Mágica (5)
+ ***Dons:*** Alucinações: Falha na Matrix, Alucinação em Massa, Aprisionar a Mente (3)
+ ***Fraquezas:*** Poder Limitado: funcionam apenas nos territórios que protegem (U1, P2)

---

### Velho do Saco/Papa Figo (Custo: 1)

Lenda popularizada após a chegada do Homem Branco, o Velho do Saco ou Papa Figo sequestra crianças para roubar-se o fígado, que devora ... Alguns acreditam que essa lenda surgiu pela crença de que o fígado humano curava doenças como a febre amarela e a lepra. Alguns imaginam que ele seja como o Krampus das lendas natalinas, que leva as crianças más que desobedecem seus pais.

+ ***Características:*** Mente Bizarra (2), Comedor de Carne: Seletivo (Fígados Humanos, Mais Humano que Humano - Fígado: Serviço Social) (3)
+ ***Dons:*** Feitos Incríveis: Demolir a Porra Toda (1), Roubo de Autonomia: Influência, Indução, Titeriteiro (3), Influenciar Emoções: A Sós, Dinâmica de Grupo (2), Puf: Filho da Puta Fodão, Um Mundo dentro do Bolso (3), Aterrorizar: Reação, Aura de Medo (2)
+ ***Fraquezas:*** Fome: Fígados Humanos (U2, P1), Poder Limitado: seus poderes só funcionam contra pessoas "más" (seja lá o que isso significa - U2, P3), Fraqueza Mortal: caso devore o fígado de uma pessoa boa (U0, P5)

---

### Matinta Pereira (Custo: 10)

Uma velha (ou mulher em geral) que pode assumir a forma de aves, é a personificação do Mau Agouro e do Azar. Essa lenda sempre procura algum tipo de oferenda (normalmente fumo na forma de ave, e um pouco de comida também enquanto velha) de casas onde as pessoas saem quando notam seu assobio agoureiro: caso o que foi pedido não seja entregue, ela rouba alguma coisa ou algo muito ruim acontece na casa! Segundo os povos originários, ela também pode espanhar doenças enquanto voa sobre as casa.

+ ***Características:*** Mente Bizarra (2), Troca de Essência (2), Festival de Fé (5), Voador (2), Metamorfose: Forma Animal (2), Controle Animal (3)
+ ***Dons Mágicos:*** Maldição (4)
+ ***Fraquezas:*** Poder Limitado: só pode ser usado contra aqueles que lhe negam oferecer fumo e comida (U2, P3)

---

### Mula sem Cabeça (Custo: -6)

Uma mulher que seduziu um Padre e com ele mantem um relacionamento amoroso, ela se transforma em uma Mula que tem labaredas de fogo no lugar da cabeça e uma cauda de fogo. Após transformar-se sai em disparada pelos campos, destruindo tudo no caminho, tentando cruzar sete cidades até o amanhecer. Para ser parada, deve ser golpeada de tal forma a extrair sangue da mesma, mesmo que uma gota, ou deve ter os freios que voam pelas chamas e são presos em sua boca invisível removidos. Seus relinchos são assustadores e muito mais altos que os de um cavalo comum, além de ocasionalmente ouvirem-se os soluções de uma pessoa. A maldição é interrompida por tanto tempo quanto a pessoa que removeu os freios ou derramou seus sangue permanecer na mesma cidade que ela, mas pode ser desfeita permanentemente se o sacerdote a repudiar sete vezes antes de uma missa.

+ ***Características:*** Metamorfose: Forma Animal (Égua), Transformação Condicional (Noites de quinta e período da quaresma), Forma Monstruosa (6); Chamado (-2); 
+ ***Fraquezas:*** Fraqueza: precisa ser golpeada por algo que derrame seu sangue (U2, P3); Poder Limitado: Apenas Transformado (U2, P3); 

--- 

### Negrinhos do Pastoreio (Custo: 13)

Negros escravos que foram muito judiados em vida, e acabaram mortos de maneira cruel por judiação dos seus donos ou filhos dos mesmos, eles encontraram consolo no pós-morte junto do manto de Nossa Senhora Aparecida. São conhecidos por ajudarem aqueles que estão em dificuldade, iluminando caminhos e localizando objetos perdidos. Entretanto, por três dias todos os anos ele corre pelas matas montadas no cavalo baio que era o favorito de seu patrão, e depois desses três dias, todos os cavalos da região disparam por aí sem motivo aparente.

+ ***Características:*** Rápido pra Caralho (2), Festival da Fé (5), Voador (2), Sentidos Especiais: Leitura de Almas (2), Imortal: Imortalidade Verdadeira, Juventude Eterna (7)
+ ***Dons:***  Controle Animal: Influência (1), Feitos Incríveis: Velocidade Ofuscante, Fodido Saltitante (2)
+ ***Dons Mágicos:*** Benção, Cura
+ ***Fraquezas:*** Poder Limitado: só pode usar para fazer o bem (U2,P3)

---

### Alma de Gato (Custo: 14)

Uma criatura que pode assumir tanto a forma de um Gato quanto a de um Pássaro, alguns dizem trazer mal-agouro, já outros dizem que consegue fazer crianças ficarem em silêncio... Também dizem que aquele que come da folha nascida de onde um Alma-de-Gato foi enterrado pode ficar invisível, e isso os torna muito caçados por feiticeiros

+ ***Características:*** Festival da Fé (5); Metamorfose: Forma Animal (2); Roubo de Autonomia: Influência (1); Invisibilidade: Imobilidade, Invisibilidade Parcial, Invisibilidade Perfeita (5); Influenciar Emoções: A Sós, Dinâmica de Grupo (2); Puf: Filho da Puta Fodão (1); Telepatia: Ouvir Pensamento (1)
+ ***Dons:*** Maldição (4)
+ ***Fraquezas:*** Poder Limitado: só consegue usar seus poderes contra crianças mal-criadas (U2, P2)

---

### Anhangá (Custo 47 - !!!!)

Uma das mais perigosas Lendas, o Anhangá é uma das variadas forças defensoras das Matas e Rios. Aparece em uma forma de um cervo branco com chifres com pelo, uma cruz na testa e olhos em chama. Sua presença é tão perigosa que sempre foi lida como perigosa pelos índios e maligna pelos brancos, sendo os jesuítas quem associou Anhangá ao demônio. Ocasionalmente assume a forma de um velho índio, e em outros momentos apenas assume uma presença tóxica ao ser humano, o que se sabe é que encontrar um Anhangá pela frente é garantia de uma Luta Justa!

+ ***Características:*** Mente Bizarra (2); Troca de Essência (5); Etéreo: Deslocamento de Fase (2); Rápido Pra Caralho (2); Infeccioso: Veneno Debilitante (2); Potência Desumana: Maromba (5); Invencibilidade: Realmente Invencível (4); Máscara: Mímico (3); Arma Natural: 
+ ***Dons:*** Letal (6); Sentidos Especiais: Leitura de Almas (3); Metamorfose: Forma Animal, Forma Monstruosa (7); Controle Animal: Influência, Indução, Titeriteiro (3); Invasão de Área: Consciência, Gremlins, Animação (3); Aura Mortífera: Ar Poluído, Vento Mortífero (2); Alucinações: Falha Na Matrix, Alucinações em Massa, Aprisione a Mente (3); Persiguição Implacável (3); Aterrorizar: Reação, Aura de Medo (2);
+ ***Fraquezas:*** Preso a um Local: apenas nas matas que protege (U2, P3); Poder Limitado: apenas contra os que fazem mal à mata (U2, P3)

---

### Bradador (Custo 14)

Um morto-vivo que não encontrou conforto nem entre os vivos e nem entre os mortos, seu grito é enlouquecedor àquele que o ouve. Diferentemente dos mortos-vivos, entretanto, ele é mais similar ao que se conhece como uma múmia. Acredita-se que tenha sido amaldiçoado por não cumprir com os deveres religiosos. Alguns dizem serem capaz de mudar de forma, mas todas elas horrendas e decadentes: raposa, cabra preta ou urubú. Muitos dizem que tudo que ele faz é gritar e só, em dor, provocando pânico e terror

+ ***Características:*** Mente Bizarra (2); Contato de Essência (1); Potência Desumana (3); Arma Natural (Berro): Letal (6); 
+ ***Dons:*** Metamorfose: Forma Animal, Forma Monstruosa (7)
+ ***Fraquezas:*** Poder Limitado: apenas durante as noites (U2,P3)

---

### Cabra Cabriola (Custo 6)

Lenda similar ao Velho do Saco ou Papa Figo, ela é muito mais esperta, ao ponto de conseguir enganar suas vítimas, normalmente crianças que estão aos seus próprios cuidados. Parece com uma criatura com boca com dentes afiados, muitos dos quais soltam também fogo e fuligem, uma lingua grossa que ocasionalmente marreta até deixar fina o bastante para ser capaz de imitar a voz humana, e os chifres similares ao de cabras. Se move com movimentos de rebolada, a cabriolagem que lhe dá o nome.

+ ***Características:***  Comedor de Carne (3), Máscara: Mímico (2 - Especial: só consegue emular a voz do alvo)
+ ***Dons:***  Feitos Incríveis: Demolir a Porra Toda, Massacre, Velocidade Ofuscante (3), Aterrorizar: Reação, Aura de Medo (2)
+ ***Fraquezas:*** Sujeito a Regras: repelida por rezas (U2,P2)

---

### Caipora (Custo 20)

Chamada ocasionalmente também de Caapora, e ocasionalmente confundida com outra Lenda, o Curupira, é mais uma entidade protetora de matas que se volta contra os que caçam por esporte ou que não aproveitam totalmente a criatura morta, demonstrando desprezo pela Natureza. Suas descrições são variadas: pode ser homem ou mulher, ser caolho ter um pé só ou ser gigantesco. Em geral é visto acompanhado por um ou mais porcos-do-mato ou queixadas, ao qual(is) chama "Jaum" (João). Quando mulher, pode aprentar uma bela cabloca que pune cruelmente traição com uma surra de cipó. É dito que aquele que cruzar a Caipora pode ter azar, mas também é dito que a Caipora troca conhecimentos sobre as histórias, mitos e lendas da mata, além sobre remédios. Pode ser apaziguada com alimentos como cachaça ou mingual e fumo. Entretanto, nunca consome sal, açúcar ou pimenta, e fica enfurecida se isso lhe é oferecido. Costuma assustar caçadores que não respeitem suas regras e, para aqueles mais reticentes e hostis, comandam animais para atacar o caçador.

+ ***Características:*** Mente Bizarra (2); Troca de Essência (3); Cara de Bravo (contra caçadores que abusam da mata) (3); Potência Desumana (3); 
+ ***Dons:*** Feitos Incríveis: Velocidade Ofuscante, Fodido Saltitante, Massacre, Demolir a Porra toda (4); Controle Animal: Inflência, Indução, Titeriteiro (3); Invasão de Área: Consciência, Gremlins, Animação (3); Perseguição Implacável (3); Telepatia: Ouvir Pensamentos (3); Aterrorizar: Reação, Aura de Medo (2)
+ ***Dons Mágiico:*** Ressurreição
+ ***Fraquezas:*** Preso a um Local: apenas nas matas que protege (U2, P3); Poder Limitado: apenas contra caçadores que não seguem as regras (U2, P3) 

---

### Saci (Custo 13)

O mais comum e famoso entre as Lendas Brasileiras, é dito que é uma combinação de mitos dos vários povos que vieram ao Brasil, com elementos de mitos originários, africanos, portugueses e de outros povos europeus. Um garoto negro, de uma perna só (deficiência que pode ocultar magicamente, mas sempre por outra forma sem uso), em geral usando um capuz e calças ou bermuda vermelha, na realidade ele é uma força do caos: em geral, quando passa por perto, derruba chapéus, dá nó em crinas de cavalos, confunde os viajantes fazendo eles se perderem, faz leite azedar e incomoda os animais durante a noite, fazendo com que eles fiquem assustados e barulhentos à noite toda, sem permitir que os fazendeiros durmam. Em geral suas peças são muito mais irritantes que perigosas, mas um Saci violento pode ser mortal.

Também é dito que conheçe bem as matas e pode assumir a forma de um redemoinho de vento, se deslocando de maneira impossivelmente rápida nessa forma. Nessa forma, entretanto, pode ser capturado se no meio do redemoinho for arremessado um rosário, terço ou uma peneira. Pode desse modo ser aprisionado em uma garrafa desde que a rolha da mesma seja marcada por uma cruz. Se alguém conseguir roubar-lhe o gorro que usa à cabeça, passa a possuir poder ante ao mesmo e ele deve obedecer o seu captor até que o mesmo decida o libertar, ainda que possa agir de maneira travessa mesmo enquanto "aprisionado" desse modo. É dito que pode ser afastado ou parado por meio de rezas, como o Credo e o Pai Nosso, e que Alho o mantem afastado.

Conhece muito das matas, e gosta de pregar peças nos caçadores ao imitar os barulhos dos animais e os remover dos caminhos e trilhas conhecidos e os colocar em enrascadas. Entretanto, é dito que pode ser convencido a colaborar se receber pagamento na forma de cachaça ou fumo, como muitas outras Lendas. Nesse caso, pode ser um útil batedor e falar sobre conhecimentos da mata. Um Saci que decida ajudar é alguém muito confiável, ainda que travesso

Existem vários tipos de saci, sendo o mais conhecido o Saci Pererê, o Saci Trique (que costuma fazer barulhos de galhos estalando à distância), e o Saci-Saçura, que tem ambas as pernas e olhos avermelhados.

+ ***Características:*** Mente Bizarra (2); Troca de Essência (2); Festival da Fé (5); Máscara: Especial - apenas para ocultar a deficiência da perna faltante, mas sempre de uma forma inútil: mais curta, deformada, o que for (1)
+ ***Dons:*** Feitos Incríveis: Velocidade Ofuscante, Fodido Saltitante (2); Controle Animal: Influência, Indução (2); Forma Elemental: Intangível, Força Elemental, Vôo (3); Alucinações: Falha na Matrix, Alucinação em Massa (2); Puf: Filho da Puta Fodão (2); Aterrorizar: Reação, Aura de Medo (2)
+ ***Fraquezas:*** Fobia: Alho (U2, P1); Fobia: Símbolos Sagrados (U2, P1); Sujeito às Regras: se capturado de acordo com as lendas, deve obediência ao captor (U1, P3)


## Lendas Exemplo

### Vic Menezes, Boto da Música (3 Estrelas)

Um dos mais talentosos compositores da atual música popular do Recife, compositor até para o Galo da Madrugada, Vic foi introduzido na música como parte de uma orquestra de frevo, até que logo criou um nome como musicista e compositor. Acabou abandonando a parte de musicista pois era considerado "difícil de lidar" por causa dos horários estritos que impunha às suas participações em cordões, orquestras e outros grupos de frevo e de Axé.

Existe uma explicação fundamental para esse companhamento de Vic, ao menos para quem o conhece realmente bem: ele é um Boto.

Nascido de um Boto que engravidou sua mãe durante o Carnaval, Vic foi ensinado pelo pai sobre esse fato ainda quando criança, já que sua mãe tinha um certo problema para criar o mesmo (imagina criar uma criança que, a cada 8 horas, precisa virar um boto e ser colocada em uma piscina), além de se envolver na cultura do Carnaval recifense rapidamente. Sua habilidade como compositor e musicista logo apareceu, e isso tem o ajudado tanto a financiar-se quanto a conseguir acesso às mulheres que todo Boto deseja. E também a pagar por todos os botinhos que ele anda espalhando pelo Brasil graças a seus amores de Carnaval!

+ ***Clado:*** Lenda (Boto)
+ ***Aspectos:*** 
  + Boto Lenda do Amor e da Música; 
  + Por incrível que pareça, pai responsável; 
  + Poligâmico e Poliamoroso por natureza; 
  + Uma piscina dentro do Trio Elétrico
+ ***Habilidades:*** Acadêmico 4, Atleta 1, Influencer 4, Criador 4, Médico 1, Ocultista 1, Assistente Social 1, Socialite 4
+ ***Manobras:***
  + ___Disciplina Específica (Música):___ Sempre que usa _Acadêmico_ em qualquer teste envolvendo música, Vic ganha +2 no rolamento
  + ___Vence pelo Cansaço:___ Sempre que Vic ganhar de outro personagem em uma perseguição, como superação ou oposição passiva, você dá a essa pessoa a condição situacional de ___Exausta___.
  + ___Participe da Minha Live:___ se você permitir, outros jogadores podem usar a sua habilidade de _Influencer_ em vez de suas próprias habilidades. Mas se eles falharem, você também sofre as consequências.
  + ___Meia-Volta:___ _uma vez por sessão_, quando enfrentar um Chamado de um dos seus Aspectos, em vez de escolher aceitar ou recusar a complicação, você pode escolher mudar seu Aspecto. Se fizer isso, você invalida o chamado, como se ele nunca tivesse acontecido, e quaisquer pontos de destino são devolvidos à pessoa que fez o chamado. Se escolher fazer isso, seu aspecto nunca mais voltará ao que era antes.
  + ___O Maior Sucesso da Última semana:___ quando estiver lidando com tendências conhecidas na música, você pode usar sua habilidade _Criador_ no lugar de qualquer outra habilidade
+ ***Características:*** 
  + Metamorfose: Forma Animal - Boto (2); 
  + Tentador (2);
+ ***Dons:*** 
  + Alucinações: Falha na Matrix (1);
  + Influenciar Emoções: A Sós (1)
+ ***Fraquezas:*** 
  + Preso a um local: precisa permanecer por ao menos 8 horas em um período de 24 em grandes corpos de água (Tamanho mínimo de uma piscinha grande desmontável ou uma banheira residencial grande - U0, P1)

### Bento "Ligeirinho" Mariano, Negrinho do Pastoreio chapa do pessoal (4 Estrelas)

Ligeirinho é o cara mais maneiro que qualquer um pode conhecer, sem falar que como guia turístico fatura uma grana que dá para viver bem no dia a dia.

Mas poucas pessoas sabem que ele é um _Negrinho do Pastoreio_: filho de um Negrinho com uma mulher humana, sob a benção de Nossa Senhora Aparecida, ele descobriu sua Herança como Negrinho quando, ainda quando vivia no Rio de Janeiro, foi colocado no Micro-Ondas por alguns milicianos que não gostaram de suas ações sociais na comunidade.

Qual não foi a surpresa dos milicianos em verem ele, montado em uma moto, mandando um grau e se mandando do Rio após verem as cinzas do mesmo no chão.

Bento (ninguém sabe se esse é o seu verdadeiro nome) ainda imagina que esses milicanos possam estar atrás dele: ele já "morreu" ao menos mais duas vezes até conseguir se enturmar com um pessoal no Recife, inclusive com um parça, Vic, um Boto que ocasionalmente precisa de... Uma _certa_ ajuda... com _certas coisas_.

Bento sabe que Vic tem ao menos uns 10 filhos, quase todos Botos, espalhados pelo Nordeste (fora os que tem até mesmo no Japão e na Dinamarca!), mas sabe que Vic é um cara que ainda faz o que é certo tentando botar tudo limpo e certinho dentro da lei. Sem falar que a música dele é muito boa: Bento ainda tenta convencer Vic a escrever uns funks para os pancadões do Rio...

+ ***Clado:*** Lenda (Negrinho do Pastoreio)
+ ***Aspectos:*** 
  + Alguém que já morreu algumas vezes pelo certo
  + _"Vic é um parça legal... Nunca vi ele fazer nada ruim DE VERDADE com as mulheres!"_ 
  + Milicianos querem (ainda) minha cabeça (Novamente); 
  + Conhece os principais pontos turísticos do Recife
  + Sempre sob a luz de Nossa Senhora Aparecida e de Oxum 
  + Meu Cavalo é uma CB300
+ ***Perícias:*** Acadêmico 2; Atleta 2; Lutador 2; Guerrilheiro 1; Influencer 3; Investigador 1; Ocultista 1; Organizador 3; Profissional (Guia Turístico) 3; Socialite 1; Assistente Social 2; Sobrevivente 4
+ ***Manobras:***
  + ***Conhecimento Inútil:*** Desde que você consiga justificar com um factoide bizarramente relevante, Bento pode gastar um ponto de destino para usar _Acadêmico_ no lugar de qualquer outra habilidade
  + ***Raciocínio Frio:*** Bento entende como o mundo funciona, e consegue usar a lógica frente a uma influência sobrenatural. Ele pode usar _Acadêmico_ para se Defender contra influências mentais sobrenaturais
  + ***Segue O Fluxo:*** Bento é um mestre de parkour. Depois ele você começa, nada pode te impedir. Depois da primeira ação de _Atleta_ baseada em parkour ou movimento de uma cena, todas as outras ganham +2.
  + ***Finta:*** Bento dá um drible nos seus oponentes antes de dar o golpe final. Quando ele _criar uma vantagem_ dando um ataque em falso para deixar seu oponente desprotegido, ganhe uma _invocação grátis_ adicional para o Aspecto que você criar.
  + ***Amigos Do Trabalho:*** Bento conhece pessoas na sua área e pode pedir tal ajuda. Quando gastar um ponto de destino para adicionar um detalhe da história, ele pode criar um personagem do zero que seja diretamente ligado à sua história de trabalho e te deva algo.
  + ***Agenda Telefônica:*** Bento já ajudou muita gente, e essas pessoas são agradecidas. Sempre que ele gastar um ponto de destino para acrescentar um detalhe à história, você pode criar (ou modificar) um personagem que você tenha ajudado no passado e que deve seu sucesso ou estabilidade atuais a você.
  + ***Sorriso Nos Lábios:*** _uma vez por sessão_, Bento pode gastar um ponto de destino para deixar uma consequência mental para lá. Ele pode reduzir uma consequência mental para leve se ele tiver um espaço leve disponível, ou  pode simplesmente remover uma consequência leve.
+ ***Características:*** 
  + Rápido pra Caralho (2)
  + Festival da Fé (5)
  + Voador (2)
  + Sentidos Especiais: Leitura de Almas (2)
  + Imortal: Imortalidade Verdadeira
  + Juventude Eterna (7)
+ ***Dons:***  
  + Controle Animal: Influência (1)
  + Feitos Incríveis: Velocidade Ofuscante, Fodido Saltitante (2)
+ ***Dons Mágicos:*** 
  + Benção
  + Cura
+ ***Fraquezas:*** Poder Limitado: só pode usar para fazer o bem (U2,P3)

### Figueira, Papa-Figo Matador (4 Estrelas)

Se Bento é o apoio "bonito" para Vic, Figueira é a Mão Esquerda do Demônio nesse caso. Poucos sabem do passado de Figueira, ou como ele descobriu/se atinou que era um Papa-Figo, e dizem que aqueles que descobriram essa verdade não estão mais entre nós, incluindo \#iHunters que tentaram o apagar. Alguns dizem que ele tá vivo desde a época do Cangaço, outros que ele era parte do DOPS, outros que ele sobreviveu no Araguaia comendo os Fígados dos companheiros caídos (aos quais traiu, alguns acrescentam). A verdade é que, seja qual for a verdade, torça para não a conhecer. Ele tá ao mesmo tempo na lista dos mais procurados e na agenda dos mais poderosos.

Figueira é muito feio, e fica ainda mais feio quando a fome de Fígados toma conta dele. Sempre é visto de gibão e garrucha, não importa onde. Apenas se veste melhor se o serviço pede assim. Ele é mais forte que o Papa-Figo comum, e dizem que fez pactos com o Cramunhão por uma Capa que permita que ele se oculte

+ ***Clado:*** Lendas (Papa-Figo)
+ ***Aspectos:*** 
  + Mão Esquerda do Demônio
  + Feio feito a Morte
  + Uma história que ninguém conhece (e quem conhece não está mais entre nós)
  + Na lista dos Mais Procurados (em todos os sentidos)
  + Uma Lenda entre Lendas
+ ***Habilidades:*** Assassino 6; Atleta 2; Lutador 4; Guerrilheiro 4; Médico 2; Criador 2; Assistente Social 1; Sobrevivente 4
+ ***Manobras (5):***
  + ***Tiro Incapacitante:*** às vezes só um tiro não é suficiente, você precisa tirar um membro, um olho, um tentáculo, o que for. Gaste um ponto de destino quando Figueira tiver sucesso em um ataque para aplicar um aspecto situacional ao alvo além dos efeitos normais do acerto.
  + ***Tiro Para Matar:*** contra um alvo completamente indefeso ou desatento, adicione +2 às suas ações de ataque de Assassino.
  + ***Choque E Pavor:*** você tem uma invocação grátis adicional para qualquer consequência causada com a sua habilidade Assassino, desde que você a use para aterrorizar, intimidar ou distrair.
+ ***Características:*** 
  + Mente Bizarra (2)
  + Comedor de Carne: Seletivo (Fígados Humanos, Mais Humano que Humano - Fígado: Assistente Social) (3)
  + Etéreo: Deslocamento de Fase (2)
+ ***Dons:*** 
  + Feitos Incríveis: Demolir a Porra Toda (1)
  + Roubo de Autonomia: Influência, Indução, Titeriteiro (3)
  + Influenciar Emoções: A Sós, Dinâmica de Grupo (2)
  + Puf: Filho da Puta Fodão, Um Mundo dentro do Bolso (3)
  + Aterrorizar: Reação, Aura de Medo (2)
  + Perseguição Implacável (2)
+ ***Fraquezas:*** Fome: Fígados Humanos (U2, P1)

### Brigitta, A Flor do Mato (4 Estrela)

A "herança holandesa" de Brigitta se resume ao nome e ao fato de ser uma mulher morena de olhos verdes. De resto, sua herança cabloca é mais viva, ainda mais por ser uma Flor do Mato, "voluntariosa protetora" de algumas das poucas matas que ainda existem no Recife. Normalmente é vista nos parques, "trabalhando" como protetora, e quando convencida conforme a tradição com fumo, leite e mel, como guia para fotógrafos e afins. Afinal de contas, seu Instagram mostra como ela ama leite e mel com Sucrilhos, o que come enquanto caminha na mata e mostra os melhores pontos para fotografia! Aliás, as capas de disco do Vic (que mostra toda a sedução dele quase nu em cachoeiras) são trabalho dela!

+ ***Clado:*** Lendas (Flor do Mato)
+ ***Aspectos:*** 
  + Flor do Mato com 30k seguidores no Instagram
  + Fotógrafa Profissional, sente-se pelada sem uma DSLR
  + Sente um ciumizinho de leve das mulheres que Vic seduz
  + Gosta especialmente de irritar outros fotógrafos que entrem em suas matas
  + Holandesa é o kct!
+ ***Habilidades:*** Acadêmica 1; Trambiqueira 1; Hacker 3; Influencer 4; Investigadora 4; Criadora 1; Organizadora 3; Ocultista 3; Socialite 4; Espiã 1
+ ***Manobras:***
  + ***Anonimidade:*** mesmo que hackear não seja algo inerentemente defensivo, seus hábitos tendem a favorecer a anonimidade. Brigitta pode usar Hacker para se defender contra ataques eletrônicos ou baseados na sua identidade, e em ações de criar vantagem. Além disso, adicione +2 à oposição ativa ou passiva em qualquer desses ataques contra ela.
  + ***Vestida Para O Sucesso:*** quando criar uma vantagem baseada em montar seu look ou sua apresentação e imagem pública como um todo antes de um evento, Brigitta ganha uma invocação grátis adicional para esse aspecto.
  + ***Participe Da Minha Live:*** se Brigitta permitir, outros jogadores podem usar a sua habilidade de Influencer em vez de suas próprias habilidades. Mas se eles falharem, ela também sofre as consequências.
  + ***Elementar:*** Brigitta é muito boa em anunciar o que parecem ser detalhes nada a ver sobre um mistério, que vão se tornar realidade. Sempre que ela gastar um ponto de destino para declarar um detalhe da história, você também pode criar um aspecto com invocação grátis que só pode ser usado em ações de Investigador.
  + ***Amuletos De Defesa:*** se Brigitta souber contra o que está lutando e tiver pelo menos dez minutos para se preparar antes de um encontro, ela pode se defender contra habilidades sobrenaturais com a sua habilidade Ocultista.
  + ***Caçadora De Conhecimento:*** o conhecimento de Brigitta sobre tradições sobrenaturais é profundo e implacável. Uma vez por sessão, quando criar uma vantagem com base em um factoide que aprendeu sobre o sobrenatural, ela ganha uma invocação grátis adicional.
  + ***Janela Indiscreta:*** devido à seu olhar quase sobrenatural (apenas quase), ao Criar uma Vantagem usando Influencer para fotografar alguém Brigitta pode, caso bem-sucedida com estilo, trazer para o jogo um Segundo Aspecto com um detalhe picante, constrangedor, ou potencialmente complicador. Esse Aspecto compartilha as Invocações do Aspecto original, sendo que pode ela poderá ser usada em um ou em outro.
+ ***Características:*** 
  + Mente Bizarra (2)
  + Rápido pra Caralho (2)
  + Controle Animal (3)
  + Contato de Essência: recupera 1 de Essência a cada dia em seu território (1)
  + Sacrifícios: Fumos e Alimentos específicos (1)
  + Reserva Mágica (5)
+ ***Dons:*** 
  + Alucinações: Falha na Matrix, Alucinação em Massa, Aprisionar a Mente (3)


