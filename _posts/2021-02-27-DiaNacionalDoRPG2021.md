---
title: Os Materiais do Dia Nacional do RPG 2021
subheadline: Incluindo o Sparks para o Cenário _Espadas pelo Perdido - Conflitos em Agresh_
date: 2021-02-27 16:00:00 +0300
layout: post
categories:
 - RPG
tags:
 - Fate
 - Fate Core
 - Fate Acelerado
 - Fate-Core
 - Artigos
 - Cenarios
header: no
---


Aqui vão ser colocados os materiais e _links_ relacionados ao Dia Nacional do RPG 2021.

+ [Os Materiais da Oficina, incluindo _Dry Fate_ e _Sparks_ em Fate](/assets/Dia_Nacional_do_RPG-Materiais.zip)
+ [Link para a Planilha de Google Docs com o _Sparks_ de _Espadas pelo Pedido - Conflitos em Agresh_](https://docs.google.com/spreadsheets/d/1fgG6UFgbsXS7ewx3xPwvd1ZgdmhN4Vnvz1svPjHPs0o/edit?usp=sharing)

