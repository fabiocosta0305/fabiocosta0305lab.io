---
layout: reviews
#date: 2020-08-19 15:00:00 -0300
title: Resenha - Fate Condensed
language: br
subheadline: Um novo modo de ver o Fate
categories:
  - reviews
tags:  
  - Fate Condensed
  - Fate
  - Evil Hat
  - RPG
book_info:
  cover_img: /images/FQXKZd.jpg
  authors:
   - PK Sullivan
   - Edward Turner
   - Fred Hicks
   - Richard Bellingham
   - Sophie Lagacé
   - Robert Hanz
  publisher:
    name: Evil Hat
    link: https://www.evilhat.com/home/fate-condensed/
  year: 2019
  isbn: 978-1-61317-196-7
  pages: 60
  formats:
    - PDF
  buy_at:
#    - site: Amazon.com (Capa Dura)
#      link: https://www.amazon.com/Evil-Hat-Productions-Shadow-Century/dp/1613171706
    - site: DriveThruRPG
      link: https://www.drivethrurpg.com/product/302571/Fate-Condensed
    - site: itch.io
      link: https://evilhat.itch.io/fate-condensed
---

> Publicado originalmente no [_site_ da Dungeon Geek](https://www.dungeongeek21.com/resenha-fate-condensed/)

O Fate Básico e seu "irmão menor" Fate Acelerado são uma ótima evolução ao Fate, em especial à sua 3ª Edição (que veio para o Brasil na forma do _Espírito do Século_, produto da Retropunk), trazendo um sistema de regras extremamente enxuto, customizável e rápido.

Entretanto, isso não quer dizer que ele funcionasse como um sistema completo para qualquer um: ele partia da premissa que o Narrador entendia ele enquanto um sistema customizável e estivesse disposto a mexer um tanto debaixo do capô para ajeitar o sistema as suas necessidades.

Isso para alguns (como esse que está escrevendo) é muito bom. Para outros, traz uma complexidade que não agrada, além de aumentar a quantidade de coisas para ler.

E como parte do financiamento de _Fate of Cthulhu_ (a ser revisado) a Evil Hat disponibilizou recentemente o  [_Fate Condensed_](https://evilhat.itch.io/fate-condensed), um novo manual básico para Fate, que também agora é parte do SRD do Fate.

## Espremendo o suco

Qual o objetivo desse jogo?

Primeiro, é ser um pacote de regras que pode ser facilmente agrupado ao seu jogo, já que está no SRD, evitando com isso o problema do Fate 3 (_"um jogo simples que exige que eu adicione 300 páginas em meu jogo, que diabo é isso?"_), mas ao mesmo tempo permite que você carregue um jogo mais complexo que o que você consegue no Fate Acelerado. 

Segundo: é um jogo um pouco mais simples que o Fate Básico, mas mais complexo que o Fate Acelerado. Na prática, O FCon (a sigla adotada pelo pessoal da comunidade internacional) é o mesmo _Fate Básico_, mas com um texto totalmente enxuto e cortando uma grande quantidade da "gordura"que vem no mesmo.

Embora os autores vejam _Fate Condensed_ como um _Fate 4.5_, particularmente o visualizo como um _Fate Realmente Básico_. O maior problema que via no Fate Básico é que ele carrega um tanto de discussões que "não caberiam" no mesmo: FCon permite ser um segundo passo após você jogar o Fate Acelerado, antes de chegar no Fate Básico e se envolver com mais complexidades. 

Dito isso...

## O que realmente mudou?

Vamos começar com uma coisa: o FCon é basicamente o mesmo conteúdo do Fate Básico, mas com algumas alterações de regras que chamam a atenção daqueles mais acostumados com o Fate Básico.

Isso se deve, segundo o próprio livro diz, a ser resultado não apenas do financiamento do Fate of Cthulhu, mas de 8 anos de jogos Fate saindo no mercado, alguns com regras muito complexas(_Mecha vs Kaiju, MindJammer, Atomic Robo_) outros com pouquíssimas customizações (_Do: Fate of Pilgrim Temple, Young Centurions, Strays_).

Vamos falar um pouco das mudanças mais visíveis e diretas no jogo, e como isso pode mudar a dinâmica do jogo.

### Criação de Jogos

Na criação de jogo, basicamente se eliminou todo o processo de geração de Faces e Lugares, além da questão dos níveis de campanha e coisas do gênero (basicamente o capítulo 2 do Fate Básico e toda uma gama de materiais dos capítulos mais avançados). Parte-se da premissa que a pessoa está pegando o FCon para tocar uma aventura ou campanha mais direta, e que todos concordam com as premissas a serem estipuladas.

Isso não é ruim: de fato, isso pode ser uma forma de gerar o efeito da _Narrativa Emergente_ que os fãs do Old School amam. Entretanto, pode-se sentir falta de mecanismos de reforço dos temas do jogo que normalmente viriam por meio dos Aspectos de Problemas e coisas do gênero.

Além disso, fica meio subentendido que o jogo é para ser usado como está, sem muito espaço para criação de Extras, ainda que alguns deles possam aparecer.

### Criação dos personagens

A primeira diferença um pouco maior no jogo aparece aqui: ao criar os personagens, não se faz mais necessário as _três fases_ para ligar-se os jogadores entre si (embora seja algo sugerido). Isso é reduzido ao aspecto de _Relacionamento_. Basicamente, você diz algo sobre como seu personagem interagem com outro, como em ___"Sou leal a Sarek... Por enquanto."___ ou ___"Lynn sabe que tem um lugar para ficar lá em casa"___

Aproveitando o tópico, é reforçado a ideia de que ___Aspectos são verdades em jogo___: se o seu personagem está ___Apavorado___, espera-se que o jogador siga esse fato e aja como se o seu personagem tivesse realmente apavorado. Se ele tem um Aspecto de ___Inimigo Jurado do Clã do Pé___, se eles aparecerem, não existe papo, sópapo (desculpem o trocadilho infame)

### Perícias e Escada

A Escada ficou _"um pouco maior"_ para baixo, mostrando valores para -3 e -4. Não que isso não pudesse ser feito previamente, mas às vezes fica aquela noção de que ter colocado ajuda ao narrador refletir melhor.

Em termo da lista de perícias, a única modificação notável foi na adição de _Academics_ (Acadêmico em tradução livre), em contraponto a _Lore_ (que no nosso Fate Básico em português está como _Conhecimentos_, mas que para o FCon pode ser traduzida como Tradições). Isso representa a dicotomia entre conhecimentos formais (e portanto seguros) contra os conhecimentos tradicionais e vindos de fontes (em teoria) não-científicas. Entretanto, nada impede que você customize suas listas como você desejar, seguindo as orientações do mesmo.

Além disso, não existe aquela lista que associava as ações às perícias. Na prática isso fica mais baseado no bom-senso dos jogadores e do narrador do que em qualquer restrição mecânica. Particularmente prefiro desse modo, embora isso reduza um pouco a importância de certas perícias.

Isso faz com que a lista de perícias seja menor e com descrições mais genéricas. Isso pode deixar o jogo mais "óbvio", mas com isso se perde certas sutilezas das descrições das perícias.

### Façanhas e Recargas

A primeira coisa que é enfatizada no FCon é que a Recarga funciona como uma medida geral da _Agência_ do personagem no mundo. Uma visão que não era muito clara antes, mas que pode ser explicado com o exemplo de Batman vs Super-homem: na teoria, o Super-homem tem poucas "coisas legais"  quando comparados com o Batman. Entretanto, ele tem mais agência que o Batman, podendo fazer mais "coisas diferentes" e menos travado aos seus Aspectos que o Batman.

Como uma forma de reduzir o tamanho do livro, todas as façanhas do Fate Básico foram removidas e substituídas por _"moldes de perícias"_, como no Fate Acelerado. Isso faz com que a criação de Façanhas pareça mais intuitiva e fácil que no caso do Fate Básico. Entretanto, para os fãs de tabela isso pode ser um problema maior.

### Estresse e Consequências

Aqui está uma das maiores diferenças do FCon para o Fate Básico e para o Fate Acelerado. 

No FCon se adota o sistema de Caixas de Estresse de 1 Ponto (como listado no Fate Ferramenta de Sistemas). Você começa com três caixas em cada trilha de estresse (ainda as mesmas Física e Mental) e recebe uma caixa adicional caso tenha as perícias adequadas em _Regular (+1)_ e _Razoável (+2)_, e mais duas se tiver em _Bom (+3)_ e _Ótimo (+4)_. Cada caixa vale 1 de Estresse, mas você pode marcar tantas quanto necessárias (e possíveis) para absorver ataques

Para as consequências, nada mudou: as mesmas consequências Suave, Moderada e Severa, com uma Suave adicional se tiver a perícia apropriada em _Excepcional (+5)_ ou melhor.

O uso das caixas de 1 ponto tem potencial para prolongar os combates, mas ao mesmo tempo pode tornar ele mais interessante com a possibilidade de ocorrer mais concessões tão logo os personagens estejam com suas trilhas perigosamente cheias. Como no FCon existe uma tratativa do Estresse enquanto _'plot armor"_, isso faz muito sentido. Isso até é citado e sugerido que dependendo do jogo se adicione uma ou duas caixas adicionais gratuitas!

E isso é o que muda na parte de criação de personagem.

## Rolando os dados

Como de costume, e enfatizado no FCon, no Fate sempre pense ___Primeiro na Narrativa___: descreva o que você quer fazer e depois se verifica a ação e perícia a serem usadas.

Em caso de dúvidas, sugere-se que você se pergunte:

+ O que pode impedir isso de ocorrer?
+ O que pode dar errado?
+ O que pode acontecer de errado que seja interessante?

Se nenhuma dessas perguntas puder ser respondida de maneira interessante, melhor não rolar dados. De resto, o sistema de rolamentos e oposição é o mesmo. Existem algumas sugestões sobre custos, como aplicar dano no Estresse em caso de empates e por aí afora, mas de resto continua o mesmo sistema de rolamentos.

E justamente na seção de Ataque é que se menciona algumas guidelines de que perícias podem executar ataques ou defesas. De modo geral, são as mesmas perícias que no Básico.

## Aspectos (e Sem Aspectos de Campanha)

Como dito anteriormente, até pela própria definição de como o FCon está construído, não existe o conceito de Aspecto de Campanha... Você pode colocá-los como parte dos Aspectos de Situação, mas ainda assim não é a mesma coisa.

Nos Aspectos, existe um reforço na ideia da Invocação Hostil, onde os personagens podem invocar Aspectos de inimigos para aumentar testes de Defesa em +2.

Isso também é feito como forma de enfatizar a diferença entre uma _Invocação_ e uma _Forçada_. Uma Invocação Hostil visa frear mecanicamente o inimigo, por meio do aumento de dificuldade: se você é um ___Especialista em Combate Um a Um___ quando a região está cheia de pessoas isso complica você. 

Por sua vez uma forçada é uma dificuldade que é  colocada de maneira narrativa: se você é um ___Guerreiro da Corporação SEED___, quando ___Revolucionários da Folha___ aparecerem, perceber!ao você e farão de tudo para deitar você morto, mesmo que isso signifique em algumas centenas de baixas adicionais em ambos os lados.

### A Regra do _"Fala Sério!"_

Para evitar que os jogadores abusem dos Aspectos, o FCon introduz uma "regra de dedo" chamada de _bogus rule_ ou, como traduzo, regra do _"Fala Sério!"_. Sempre que qualquer intrusão narrativa aparecer em jogo, veja a reação dos jogadores e a sua. Se, a qualquer momento, paracer algo forçado, algo que você diria _"Fala Sério!"_, você pode negar o uso, ou negociar alguma saída, como um bônus menor, uma vantagem um pouco menor e por aí afora. 

Essa regra serve como orientação para evitar abusos nos Aspectos, em especial aqueles obviamente feitos com o único objetivo de ser vantajosos ao jogador.

## As ações complexas continuam as mesmas...

... mas possuem alguns detalhes enfatizados.

Para começar, as explicações sobre Zonas e Trabalho em Equipe são colocadas de forma mais geral, com uma maior enfâse das mesmas para todas as ações complexas ao invés de apenas para conflitos. 

Nas Disputas, foi incluído uma regra para disputas perigosas, como, por exemplo, Lutas de MMA ou _Iaijutsu_. Nesse caso, quando estiver nessa condição, o lado que for derrotado na altercação recebe um ataque equivalente à diferença entre os rolamentos, direto no Estresse. Isso torna disputas algo interessantemente perigoso, bem climático para _Iajutsu_ e justas

E falando de dano: à exceção da mudança citada no Estresse, como citado anteriormente, nada mudou na regra de absorção de dano. Entretanto, para realizar a recuperação de consequências, agora é ___obrigatório___ um rolamento de uma perícia adequada contra a severidade do dano, +2 se o personagem estiver tentando tratar a si próprio. Apenas após isso é que começa a contar o tempo para recuperação da consequência

### A Iniciativa Pipoca

Também chamada de _Inciativa Balsera_ ou _Iniciativa Cinemática_, ela funciona de uma maneira bem diferente ao que normalmente se vê nos RPGs em geral, e mesmo no Fate.

De início, ela funciona normalmente, definindo-se a iniciativa como normal (Atletismo para Físico e Percepção para Mental, com desempates por Vigor e Vontade, respectivamente).

Entretanto, a partir do momento que o primeiro personagem agir, será ele a decidir quem irá agir. Só podem ser escolhidos personagens que não agindo nesse turno. Uma vez que todos os persoangens (incluindo NPCs) já tiverem agido, um novo turno começa com o último personagem que agiu escolhendo quem é o primeiro a agir no novo turno (inclusive a si mesmo).

Essa forma de iniciativa gera um efeito legal que valoriza o trabalho em equipe onde cada personagem prepara o terreno para um último realizar ataques muito poderosos.

## Evoluindo

Na evolução está outro impacto forte do FCon: agora existem apenas os Marcos Menores (ao fim da sessão de jogo) e Maiores (ao concluir um Arco de História).

O Marco Menor continua com os mesmos benefícios. Já o Marco Maior engloba parte dos benefícios previamente incluídos no Marco Significativo:

+ Renomear o Conceito
+ Iniciar recuperação de Consequências Moderadas ou Severa que ainda não tenham começado
+ +1 em uma perícia qualquer, desde que respeitada as colunas

Com a opção de o Narrador adicionar os seguintes benefícios se o Narrador considerar que os eventos da aventura impactaram o mundo o suficiente:

+ +1 na recarga
+ +1 adicional em uma outra perícia qualquer

## Sendo o Narrador e Criando a oposição

Ainda que sumarizado, para aquele que conhece bem o Fate, a sessão sobre ser o Narrador não inclui maiores novidades. Em especial existe uma descrição sucinta e interessante sobre como definir dificuldade/oposição.

Para NPCs menores, fica em aberto usar-se as Perícias ou algo similar ao Fate Acelerado, então você poderia tanto ter:

> + ___Sergio Leroy, l33t h4x0r___
>     + _**Acadêmicos** Bom (+3)_
>     + _**Empatia** Bom (+3)_

Ou 

> + ___Sergio Leroy, l33t h4x0r___
>     + ___Bom (+3) em:___ invadir sistemas, engenharia social
     
Para quem já leu, isso também acontece em Nest

A principal diferença é que não existe nenhuma guideline sobre como criar NPCs maiores, sendo que eles podem ser tão poderosos quanto sua necessidade narrativa (ou sadismo) desejar.

Por fim, ainda que rapidamente, são citadas as _Ferramentas de Segurança_ já conhecidas: como o FCon veio de Fate of Cthulhu, se faz necessário isso para que não ocorra problemas. 

## Regras opicionais (ou a alegria de muitos)

O FCon importa para si algumas regras que foram vistas em outros lugares e que são muito pedidas por jogadores e narradores. Isso definitivamente não exime uma bela consulta aos Ferramentas de Sistemas, _Adversary Toolkit_ e assim por diante, mas ele traz algumas que podem ser bem inteligentes quando bem usadas.

### Condições

A primeira é a sugestão do uso de ___Condições___ no lugar das Consequências. No caso, as Consequências são substituídas por Condições cujas caixas tem valor igual a metade da Consequência original, sendo uma de cunho físico e uma de cunho mental:

| ***Nível*** | ***Condição Física*** | ***Condição Mental*** |
|-:|:-:|:-:|
| Suave (1) | Arranhão | Assustado |
| Moderado (2)| Ferimento | Aterrorizado |
| Severo (3) | Ferimento Grave | Desmoralizado |

De resto, funciona como visto anteriormente no ___Ferramentas de Sistema___

### Criação de personagem em jogo

É uma sugestão que foi sumarizada mas já existia no próprio Fate Básico. Todos os personagens devem definir seu nome, Conceito e Perícia Pico, e podem ir alocando as demais conforme os testes forem demandados e se tiverem caixas de perícias livres para tal. Da mesma forma, Aspectos e Façanhas são definidos conforme a necessidade.

### Contadores

Vindos diretamente do ___Fate Adversary Toolkit___, basicamente são Aspectos com uma ___trilha de Estresse___ atrelados a ele, indicando que um ___Resultado___ terrível irá acontecer assim que essa trilha se encher, conforme ___Gatilhos vão ocorrendo___ e marcando essas caixas de Estresse. Tais gatilhos podem ser dos mais diversos tipos e complexidades, desde ___A Cada Turno___ até para ___cada vez que um personagem entregar-se aos Grandes Antigos___.

### Consequências Extremas

Igual ao Fate Básico: uma consequência Extrema permite absorver 8 Pontos de Estresse, mas ao custo de um Aspecto do Personagem que não o Conceito. Você pode fazer isso apenas uma vez a cada Marco Maior, e você só pode renomear o Aspecto ao final do Marco Maior seguinte ao que o personagem sofreu a Consequência Extrema

### Defesa Total e Disputas Rápidas

Defesa Total é uma conhecida de quem leu o Fate Básico detalhadamente. Ao realizar uma Defesa Total, você recebe +2 em todos os rolamentos de Defesa, mas não pode realizar nenhuma outra ação, já que você agindo conscientemente para se defender. 

Se por um Acaso você não precisar rolar defesa até sua próxima ação, você recebe um _Impulso_ por ter se preparado para agir.

Já nas Disputas Rápidas, ao invés de permitir que todos os personagens rolem contra uma oposição em uma disputa, os personagens devem escolher explicitamente o que desejam fazer: rolar contra a oposição, criar alguma vantagem para quem for rolar contra a oposição ou usar _Trabalho em Equipe_ e oferecer +1 automaticamente a quem for rolar sem precisar rolar.

## Aumentando o Poder 

FCon vem com muitas sugestões interessantes de como tornar seus adversários incrivelmente poderosos e difíceis de serem derrotados

### Escala

FCon traz o sistema de escala de _Dresden Files Accelerated_, uma versão de algo visto previamente em _The Secrets of the Cats_

No caso, define-se uma escala de menos poderoso para mais poderoso e, para cada nível de diferença, o personagem mais poderoso recebe um dos benefícios abaixo, escolhido no momento de agir:

+ +1 na perícia envolvida _antes do rolamento_
+ +2 no resultado final do rolamento, se for bem sucedido
+ +1 Invocação Gratuita no caso de ações de Criar Vantagem

Sugere-se, entretanto, oferecer aos jogadores mecanismos de contornar ou inutilizar a Escala se necessário, como descobrir vulnerabilidade do adversário mais poderoso ou mudar os objetivos de modo a não permitir o mesmo de usar sua vantagem de escala.

## Inimigos poderosos e capangas descartáveis

Para criar inimigos ainda mais poderosos, uma coisa que pode ser interessante sugerido como regra opcional em FCon são as Imunidades e os Capangas Descartáveis.

Capangas Descartáveis é exatamente isso: o adversário possui capangas que literalmente morrerão por ele (ou algo similar) que o adversário pode usar como _Sucesso a Custo_ em suas Defesas.

Já as Imunidades impedem que o inimigo seja alvejado diretamente por Ataques até que os personagens vençam algum tipo de desafio (Disputa) ou tenha seus pontos fracos revelados (Desafios). Isso lembra um tanto alguns jogos onde você primeiro tem que acessar algum tipo de ponto fraco do inimigo antes de provocar-lhe dano.

### _"Essa não é minha forma final!"_ - Forma Final e _O Mapa é uma Ameaça_

Usando ___Forma Final___, você também pode criar um _"irmão gêmeo"_ que os personagens devem enfrentar para destruir o inimigo, com novas perícias, façanhas, poderes e até mesmo Estresse e Consequência limpas se você for suficientemente sádico. 

Ao usar ___O Mapa é uma Ameaça___, você cria um inimigo que, na verdade, é composto por múltiplas partes que atacam ao mesmo tempo, sendo consideradas zonas diferentes. Isso reflete _Kaijus_ e Naves Espaciais Gigantescas, onde para derrotar-se o inimigo é necessário que derrote-se todas as sessões. Ao mesmo tempo, você pode combinar esse elemento com ___Capangas Descartáveis___ para criar coisas como naves cujas sessões auxiliares explodem ao receberem dano, ou ainda um _Kaiju_ que pode perder partes do seu corpo mais ainda lutar.

### _"Venham na maciota, tá bom?"_ - Bônus solo

Alguns vilões muito poderosos são ainda mais poderosos quando enfrentando grupos. Esses vilões possuem ___Bônus Solo___ quando lutarem sozinhos contra hordas de PCs.

Um personagem com isso pode:

+ Receber um bônus nos rolamentos igual ao número de PCs -1
+ Reduzir o Estresse de um ataque pela metade do número de PCs envolvidos, arredondado para cima. Caso tenha medo de que o combate se alongue demais, faça com que todo ataque bem-sucedido provoque ao menos 1 ponto de Estresse direto no Inimigo;
+ Ao invocar Aspectos usando Pontos de Destino, o inimigo aumenta seu bônus no rolamento no número de PCs atacando (___YIKES!!!!!___);
+ O inimigo pode fazer com que invocações de Aspectos só ofereçam +1 no rolamento, ou apenas permitam re-rolagens, ou ainda impedir que os personagens usem múltiplas Invocações Gratuitas;

Tome cuidado ao aplicar essas opções: ainda que possam ser usadas em conjunto, elas podem tornar o desafio praticamente impossível de ser resolvido.

## Arma e Armadura

Por fim, FCon cita a tradicional regra de Arma e Armadura para Fate, como no Fate Básico. Níveis de Arma adicionam Estresse a um Ataque bem-sucedido igual ao seu nível (incluindo no caso de _Empates_) e Armaduras reduzem estresse em caso de você sofrer um Ataque Bem-sucedido (dando um _Impulso_ ao adversário em caso de zerar o Estresse) igual ao seu nível.

## O Livro

FCon não é um livro "bonito". Na prática, não possui nenhuma ilustração em suas 52 páginas (54 considerando capa no PDF). Entretanto, seu conteúdo rico e sucinto compensa para quem está procurando uma alternativa mais simples que o Fate Básico para começar, mas não quer ir ao Fate Acelerado por achar ele muito simplório. 

As regras novas tornam o jogo mais quente e sumarizam opções que certamente eram "regras da casa" usadas em muitas mesas de Fate. Posso dizer com certeza que, por exemplo, sou um fiel defensor da Iniciativa Popcorn: com todos os seus defeitos, torna o jogo mais ágil e divertido.

Os exemplos ainda são do Fate of Cthulhu, de onde ele basicamente foi recortado _ipsis literis_, mas muitas das regras opcionais estavam nos mais variados módulos: _Ferramentas de Sistema_, _Adversary Toolkit_, _Dresden Files Accelerated_, _Secrets of the Cats_...

Realmente, o FCon é um bom resultado de 8 anos de Fate.

Entretanto, o FCon sofre com as consequências de sua proposta, ao não explorar possibilidades de criação de campanha e outros detalhes do Fate Básico deixados para trás. Além disso, a total falta de citação ao Fate Fractal me parece um problema potencialmente sério, já que isso tira da mão do narrador uma das ferramentas mais poderosas (ainda que menos compreendidas) do Fate. Porém, visto que a função dele é ser um meio termo entre o Fate Acelerado e o Fate Básico, isso não me parece ruim.

Fate Condensed (FCon) é um produto 4/5 na minha opinião: ele é uma nova visão do Fate que surge como uma boa alternativa para aqueles que estão começando agora no mesmo, mas que ainda assim desejam um jogo um pouco mais complexo que o oferecido pelo Fate Acelerado. Sua sucintez era algo desejado e esperado há muito tempo por jogadores e narradores do Fate. E o fato de ser SRD permite que produtores de conteúdo insiram as regras com as devidas alterações em maiores problemas.

Se pode dizer que existe um problema um pouco mais sério é que FCon é que ele ainda está em inglês, mas não duvido que logo teremos novidades sobre ele em português.

E agora você tem ainda mais opções para ser desafiado pelo Destino.

E aí, está preparado?

<!--  LocalWords:  PK Sullivan Edward Fred Hicks Richard Bellingham
 -->
<!--  LocalWords:  Sophie Lagacé Robert Hanz year Condensed Retropunk
 -->
<!--  LocalWords:  of Cthulhu Evil Hat SRD FCon vs Kaiju MindJammer
 -->
<!--  LocalWords:  Atomic Robo Pilgrim Young Centurions Strays Old br
 -->
<!--  LocalWords:  School Sarek Lynn sópapo Academics Lore Batman MMA
 -->
<!--  LocalWords:  não-científicas plot armor guidelines SEED bogus
 -->
<!--  LocalWords:  paracer enfâse Iaijutsu Iajutsu Balsera RPGs NPCs
 -->
<!--  LocalWords:  persoangens Sergio Leroy Nest guideline opicionais
 -->
<!--  LocalWords:  Adversary Toolkit Dresden Accelerated The Secrets
 -->
<!--  LocalWords:  the Cats Kaijus PCs YIKES re-rolagens Popcorn tags
 -->
<!--  LocalWords:  ipsis literis reviews title language subheadline
 -->
<!--  LocalWords:  categories book info cover img authors publisher
 -->
<!--  LocalWords:  name isbn pages formats buy at Amazon DriveThruRPG
 -->
<!--  LocalWords:  itch io Dungeon Geek
 -->
