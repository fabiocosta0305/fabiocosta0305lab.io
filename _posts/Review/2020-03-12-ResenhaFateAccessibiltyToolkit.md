---
layout: reviews
#date: 2018-09-28 10:00:00 -0300
title: Resenha - Fate Accessibility Toolkit
language: br
subheadline: Pessoas com necessidade e competencias especiais!
categories:
  - reviews
tags:  
  - Toolkit
  - Fate Accessibity Toolkit
  - Accessibilidade
  - Fate
  - Evil Hat
  - RPG
book_info:
  cover_img: /assets/img/CapaFAcT.png
  authors: 
    - Brian Engard
    - Sophie Lagacé
    - Lauren Bell
    - C.D "Casey" Casas
    - Lilian Cohen-Moore
    - Philippe-Antoine Ménard
    - Aser & Megan Tolentino
    - Clark Valentine
    - Mysty Vander
    - Zeph Wibby
  publisher:
    name: Evil Hat Productions, LCC
    link: https://www.evilhat.com/home/shadow-of-the-century/
  year: 2019
#  isbn: 978-1-61317-170-7
  pages: 212
  formats:
    - PDF
  buy_at:
#    - site: Amazon.com (Capa Dura)
#      link: https://www.amazon.com/Evil-Hat-Productions-Shadow-Century/dp/1613171706
    - site: DriveThruRPG
      link: https://www.drivethrurpg.com/product/283738/Fate-Accessibility-Toolkit-o-Prototype-Edition
    - site: itch.io
      link: https://evilhat.itch.io/fate-accessibility-toolkit-prototype-edition
---

> Publicado previamente no [site da _Dungeon Geek_](https://www.dungeongeek21.com/resenha-fate-accessibility-toolkit/)

> ___Nota:___ Essa resenha está sendo feita baseada no _Prototype Edition_, a versão "apenas texto" do livro, portanto não serão considerados detalhes de diagramação.

## O Início

Uma verdade inconveniente sobre o RPG: os jogos e o ambiente dos jogos tendem a ser extremamente capacitistas. Devido à sua origem em _jogos de guerra_, a maior parte do tempo quase todo tipo de (na falta de termo melhor) deficiência é tratada de maneira negativa. Desde a perda de CA por cegueira ao famoso "Kit do Psicopata Feliz" de GURPS (comprar as desvantagens _Fúria, Sadismo_ e _Sanguinolência_), situações de neuro-atipicidade ou condições diferenciadas em termos físicos são tratadas invariavelmente como desvantagens.

Isso, mais o fato de serem jogos que recorrem muito ao visual (fichas e tudo o mais) e à fala, tornam o RPG infelizmente um jogo ainda muito hostil a pessoas que tenham qualquer divergência em relação ao "padrão" social, seja em termos físicos (quantas lojas de RPG possuem acesso facilitado a cadeirantes?) seja em termos mentais (conseguimos lidar bem com jogadores que tenham hipersensibilidades, como Autistas?).

Existem algumas boas iniciativas recentes para tentar melhorar esse cenário: coisas como o [DOTS Project](https://www.dotsrpg.org/) que procura criar dados para RPG em braille ou com cores que os tornem mais acessíveis a daltônicos; o jogo [Amigo Dragão](https://www.catarse.me/amigodragao) (em financiamento durante o período em que esse artigo está sendo escrito), desenvolvido pelo Tiago Junges para ajudar a sua filha que é autista a interagir por meio do jogo, são algumas entre várias iniciativas, mas ainda assim esparsas e individuais.

Foi quando, Fred Hick e Rob Donoghue foram apresentados por Shoshana Kessock a Elsa Sjunneson-Henry, uma jogadora de RPGs cega, surda e com Estresse Pós-Traumático (como dito no próprio livro). Sua experiência com os jogos de RPG era a de desbravar um ambiente hostil: não apenas para personagens, mas também para _jogadores_, fora do padrão social. E ela veio com a ideia de como criar um jogo mais acolhedor a jogadores e personagens com algum tipo de diferença física ou mental em relação ao padrão. E ele deu o aval.

Entretanto, devido a questões econômicas, a _Evil Hat_ se viu em dificuldades: o recente cancelamento da linha dos _World of Adventure_, além de uma série de atrasos em seus produtos, foram provocados por essas dificuldades. 

A aposta da Evil Hat foi em começar a publicar os materiais de maneira um pouco mais crua: os _Prototype Edition_, edições com o conteúdo de texto, mas sem as imagens. Essas vendas iniciais são usadas para financiar as artes dos mesmos, com o compromisso de, tão logo a diagramação final estar pronta, a pessoa receberá o documento final.

A iniciativa deu muito certo com o _Space Toolkit_ (o guia Espacial), e isso ajudou eles a colocar esse módulo adiante.

## As barreiras

Na introdução ao _Fate Accessibility Toolkit_ (vamos usar a sigla sugerida FAcT para facilitar), Elsa explica que começou a se envolver nos jogos por meio de LARPs. Como ela menciona no livro:

> _"It didn’t matter that I was Sherlock and I was blind. It didn’t matter that I was a hardcore dragon fighting princess and deaf. No one cared if I had a heart condition and wanted to be an epic hero."_
>
> ___"Não importava se eu era cega, eu era Sherlock. Não importava se eu era surda, eu era uma Princesa Dragão Lutadora durona. Ninguém ligava se eu tinha uma condição coronária e queria ser um herói épico."___
 
Foi quando ela encontrou os RPGs... E começaram as restrições: como explorar as condições que ela tinha de uma maneira interessante em jogos cujas regras não previam tais situações, ou pior, as previam como um redutor na capacidade? Como criar personagens como ela em cenários e jogos que não previam isso?

E ela começou a tentar resolver o problema, criando personagens. O problema é que na maioria dos jogos, isso não é intuitivo, as ferramentas para traduzir as coisas não estão lá: no máximo, são tratadas como desvantagens e formas mais rápidas de perder-se o personagem. Afinal, como lidar com cadeiras de rodas quando as regras não as inclui? No final, elas são veículos e deveriam ser tratadas como tal.

E nesse ponto Fate chamou a atenção dela, já que é um jogo onde se _"deve tratar os personagens como pessoas competentes, dignas dos riscos e desafios lançados a eles."_ (Fate Básico, página 16) Você pode fazer coisas sem que ninguém diga que você não pode. Personagens não são estatísticas em um jogo que é ___muito mais gramático que matemático___ (enfase do autor do artigo). Eles são competentes e sabem como fazer seu caminho no mundo, seja como o _Demolidor_ ou a _Oráculo_. Por que então os jogos tratam condições como essas como desvantagens?

A ideia por trás do FAcT é vencer essas barreiras: é tratar coisas como depressão, bipolaridade ou Estresse Pós-Traumático como mais que uma mecânica para horror lovecraftiano. É um livro para incluir aqueles que antes eram apenas estatística negativa. 

E por que simplesmente não jogar com um personagem _capaz_, por assim dizer?

Por que, na prática, uma pessoa com o que uma pessoa _capaz_, por assim dizer (como o autor desse), diria ser um problema na verdade não se vê (ou não deveria) como alguém com um problema. Aquilo não é um problema, é parte dela. E é curiosamente uma minoria à qual uma pessoa pode entrar a qualquer momento: pense, por exemplo, em Marcelo Rubens Paiva. Ou melhor, leia _Feliz Ano Velho_ e _Ainda Estou Aqui_.

Pode acontecer de um belo dia algo acontecer e você se ver, por exemplo, tendo que reaprender a usar um computador ou celular com recursos de acessibilidade como o _Talkback_, ou algo similar, tendo que se adaptar a uma nova realidade, a um novo corpo, por assim dizer. Isso passa a ser parte de você. E por isso, em alguns casos, pessoas em tal situação não querem ser _"curadas"_: alguns simplesmente não podem, outros não se preocupam com o fato de não serem conformes ao padrões sociais. Sem falar que muitas vezes isso foca-se na realidade não em melhorar a qualidade de vida de tal pessoa, mas na das pessoas ao redor dela.

## O Começo do Caminho

Uma coisa importante: FAcT, assim como todas as Caixas de Ferramentas do Fate (as Ferramentas de Sistemas, Adversary, Horror, Space), não é algo pronto para ser usado direto da caixa. É um conjunto de recursos a serem usados pelo Narrador, e portanto demandam tempo e discrição em serem estudados e compreendidos antes de serem usados.

Além disso, ele nem chega a ser uma explicação profunda: embora existam muitos detalhes interessantes (não sabia que existem 200 formas diferentes de Nanismo conhecidas), elas não chegam sequer a arranhar a coisa toda. Portanto, você, seja como narrador ou jogador, precisará fazer sua lição de casa e, ao criar um personagem com alguma situação, entender de maneira mais profunda. Nem todo cego é o _Demolidor_, nem todo depressivo é a _Tristeza_ de _Divertida Mente_ (para usar expressões mais "comuns").

Outra coisa importante: esse livro foi escrito por quem vive todos os dias com isso. Elsa, Fred, Rob e a equipe da Evil Hat tomou o cuidado de procurar pessoas com todos os tipos de situações neuro-atípicas e de outra forma não-conformes para falar de seus pontos de vista sobre tais condições. 

Infelizmente isso quer dizer que algumas situações não são mencionadas, como algumas formas de condições mentais, epilepsia, problemas de desenvolvimento intelectual, etc... Importante, ao ler o FAcT, entender que essas são vozes de pessoas que passam por esse tipo de condição e que participam do hobby do RPG. Vozes que merecem ser ouvidas e que muitas vezes não o são.

## Sobre dizer _Não_

A primeira coisa que vemos é um compromisso que a Evil Hat tem a muito tempo: _não lidar com nada que não possa ser lidado de maneira respeitosa, e ao o fazer não o fazer com o objetivo de prejudicar algo ou alguém._

Para quem leu minhas resenhas de outros materiais da Evil Hat, é o mesmo compromisso que é feito no _Horror Toolkit_, sobre _Horror não dar direito a ser horrível_, ou em _Shadow of the Century_, onde você não precisa trazer para o jogo tudo dos anos 80: você pode deixar o racismo, machismo, xenofobia e homofobia lá, trazendo apenas as partes divertidas, como os ternos de ombreira, _keytars_, De Loreans, _boomboxes_, cabelos extravagantes e leques (_Marcha, Ibiza, Locomia_).

A ideia é: se alguém não está se divertindo, está errado. Se você cria um elemento de jogo que incomoda alguém por causa de uma condição, ao invés de dizer pura e simplesmente Não para a pessoa ou para o elemento, trate isso como uma oportunidade de crescimento. Ela não está normalmente pura e simplesmente impedindo aquilo: ela apenas quer ser ouvida. Ouça-a e ajuste conforme a necessidade.

O objetivo é a diversão: se algo que você trouxe está impedindo todos de se divertirem, você está atrapalhando a diversão e a mesa tem o direito de dizer não para aquilo. Não é errado fazer ajustes finos para chegar em um ponto onde todos se divirtam. Não se mantenha atado a clichês tóxicos: deixe o capacitismo para trás. Mesmo que isso seja não fazer as coisas como antes.

## O que não te contaram sobre Acessibilidade

Essa lista é importante de rever e vou copiar praticamente _as is_ do FAcT, por ser necessário e primordial para entender o que ele traz (lembrando que uso o termo _deficiência_ como um termo comumente usado):

+ Nem toda pessoa com alguma deficiência deseja ser curada
+ Nem sempre dá para se curar uma deficiência
+ Binarismo em deficiências é uma ilusão: a maior parte das deficiências não são oito ou oitenta, vindo em escalas.
+ Dispositivos de Acessibilidade são ferramentas, não brinquedos
+ A deficiência não é toda a sua identidade, ainda que possa ser algo importante. Você pode ser um cego judeu, ou uma pessoa de cor cadeirante, ou uma mulher com bipolaridade, ou um homem trans autista... ou qualquer outra coisa. Pessoas com deficiência são pessoas complexas, como qualquer outra.
+ Dor é um conceito relativo: certas dores podem ser simplesmente ignoradas se você as sente o tempo todo
+ Deficiências não são caixinhas: elas variam de pessoa para pessoa. Mesmo pessoas que tenham o mesmo tipo de deficiência podem lidar com ela de formas diferentes.
+ Nem toda deficiência é visível: algumas podem não ser vistas, mas ainda poderem influenciar a vida da pessoa. Pense em pessoas diagnosticadas como autistas.

Até o cuidado ao falar sobre tais condições deve ser considerado: alguns preferem a ideia de "pessoa primeiro, condição depois", como em _pessoas cadeirantes_. Outras pensam em uma ideia de que aquela condição é definidora delas, como em _cego_. Isso não é um detalhe: algumas preferem a primeira por terem tornado-se cadeirantes durante sua vida. Outras podem preferir se referir como cadeirante de maneira geral pois (em teoria) nunca conheceram outra forma de se enxergarem: elas sempre se viram assim por alguma razão.

## Aspectos não são positivos ou negativos, eles SÃO!

Essa é uma frase que sempre costumo dizer ao mencionar Aspectos, e aqui isso é mais de oito mil vezes mais importante ao mencionarmos situações de deficiência.

Lembre-se sempre que bons Aspectos dizem mais do que uma coisa só. Dizer, por exemplo, que um personagem é ___Cego___ é um tanto genérico, enquanto dizer sobre um ___Combatente contra o Crime Cego___ é algo muito mais interessante (que o diga o Demolidor). Dizer que o personagem tem ___Transtorno de Múltipla Personalidade___ não é tão legal, mas uma ___Agente contra o Mal com Transtorno de Múltipla Personalidade___ como a Crazy Jane de _Patrulha do Destino_ é muito mais interessante.

Ao usar Aspectos desse modo, em especial ao os Forçar, não o faça de modo a tornar a restrição algo que simplesmente vai remover o personagem do caminho: não Force o Aspecto do  ___Hacker Cadeirante___ para impedí-lo de subir pela Escada de Incêndio e tirar-lhe a chance de participar da ação. Faça isso para que ele suba pelo elevador e chame atenção desnecessária para o grupo, mas de modo que ele chegue aonde o resto do grupo vai estar (talvez depois de espancar alguns capangas com o Taser embutido na cadeira de rodas). Use tais Forçadas para abrir novos caminhos de história que podem se tornar interessantes: e se o ___Hacker Cadeirante___ usar uma estação de trabalho da contabilidade para acessar os Arquivos Secretos da Megacorp? Será que não chamaria a atenção? De repente...

E lembre-se: personagens de Fate com Deficiência são pessoas antes de serem deficientes (me perdoem novamente por esse termo). Elas são parte da ação por resolverem problemas, não apenas por superarem deficiências. Evite a ___Narrativa de Superação___, isso não é nada divertido (exceto se, e ___MUITO SE___, a premissa do jogo, combinada entre todos, envolver ___TAMBÉM___ essa superação).

## Faça a lição de casa

Em seguida FAcT lista algumas linhas gerais sobre como lidar com todo tipo de condições fora do padrão. Isso se deve, como dito anteriormente, que FAcT não tem como cobrir todas as bases, até porque existem todo um universo de condições fora do socialmente convencionado (lembre-se: 200 formas diferentes de Nanismo!).

O resumo dessa sessão, não importa se é sobre criar personagens ou trazer jogadores com condições assim é simples:

> ___FAÇA SUA LIÇÃO DE CASA!!!!___
 
Não trate tais condições como piada ou estereótipo: não precisamos de mais ___Palhaços do Crime___, ou dos ___Gênios Cadeirantes___, já os temos em suficiente. 

Isso é especialmente relevante com a questão das condições mentais: estereótipos como o do super-gênio autista ou com Transtorno Obsessivo Compulsivo já foram requentados à exaustão. O mesmo vale para os sanatórios mentais abandonados, com quartos com paredes cheias de escritos perturbadores feitos com excrementos e coisas do gênero. 

Outra coisa, e algo que pode acontecer com facilidade se você não se cuidar: ___não diagnostique seus jogadores___. Não importa quanta pesquisa você faça na Internet, você não é gabaritado para isso (a não ser que você seja ___REALMENTE___ gabaritado para isso). Você pode, legitimamente, pedir para um amigo de mesa que você suspeite que tenha alguma condição procurar ajuda especializada (na verdade, você _deveria_) mas você não deve tentar ___Diagnosticar___. 

Você só é um Narrador de RPG, deixe isso para profissionais especializados.

Como uma sugestão para, por assim dizer, tirar a galhofa do horror e dos jogos que lidam com insanidade, uma sugestão é adotar-se o sistema de ___Ganchos___: ao criar o personagem, ao invés de usar o Trio de Fases, faça cada jogador definir 1 a 3 Ganchos, Aspectos relacionados a coisas que mantém o personagem ligado a coisas que o mantém longe do horror. Quando o personagem se vir em uma situação em que deseja melhorar suas chances mas não tem como (por falta de PDs ou não possuir Aspectos relevantes), ele pode ___Arriscar um Gancho___: ele recebe o mesmo benefícios de Invocar um Aspecto, mas se ele for bem-sucedido, ele deve _reescrever tal Gancho_ ao fim da cena, em algo relativo ao contexto do jogo. Caso falhe, nada acontece. Um personagem só pode Arriscar um mesmo gancho uma vez por rolamento.

> _Emma possui um Gancho_ **Seguidora de Nossa Senhora de Fátima** _em um jogo de Horror. Ela vê diante de si um cultista que está tentando sequestrar crianças que ela está protegendo. Ela não possui Aspectos relevantes para melhorar suas chances de matar o cultista com o rifle que Jack, o policial, deu para ela. Ela decide então Arriscar esse Gancho para melhorar seu Ataque em +2. Isso é o suficiente para acabar com o cultista, mas... Ao preço de reescrever esse Aspecto. Quem sabe agora Emma, coberta de sangue, não se veja como_ **A Mão Esquerda de Deus**, _ou imagine que_ **O Mal Vence quando os bons nada fazem**.
 
## Criando personagens

Uma coisa também importante é que personagens com deficiência não estão impedidos de pegarem perícias. Um cadeirante pode ter _Atletismo_, representando sua habilidade em Cadeira de Rodas (quem sabe ele não é o ___Cadeirante Mais Radical dos X-Games___?). Alguém com, por exemplo, Autismo, pode até mesmo ter ___Percepção___ muito alta: sua hipersensibilidade a tudo pode o tornar mais sensível a notar coisas que saiam do padrão, como aquele _sniper_ no alto do prédio.

Ao elaborar Façanhas, pense também mais no que o personagem PODE fazer do que no que ele NÃO PODE fazer: o ___Cadeirante Mais Radical dos X-Games___ pode ter um ___Momentos Déjà-vu da Cadeira de Roda___ que, sempre que ele precisar _Criar Vantagens_ por Atletismo demonstrando suas habilidades, ele recebe +2 se puder descrever como ele desce de uma maneira praticamente impossível uma ladeira ou coisa do gênero. Já nosso Autista poderia ter uma Façanha ___Falso! Falso!___ que permite a ele somar +2 para Superar usando _Conhecimentos_ ao perceber detalhes sutis mas precisos que permitam a ele identificar obras falsas.

Pense também em Extras que envolvam os elementos que o personagem tem que usar cotidianamente, como dispositivos de acessibilidade. Se você já é o ___Cadeirante Mais Radical dos X-Games___, por que não incluir um _Dispositivo de Tirolesa_ à sua Cadeira de Rodas que permita você deslocar-se de um ponto a outro que você normalmente não poderia, desde que consiga antes realizar um teste de _Conhecimento_ para preparar o Dispositivo? Isso pode ser até bem legal não apenas para você, mas para seus amigos personagens, quando os ninjas estiverem correndo atrás deles!

Por fim, é interessante que personagens assim tenham certas _Condições_ especiais que eles possam marcar em momentos específico. Se você é o ___Cadeirante Mais Radical dos X-Games___, parece meio óbvio que ocasionalmente você fique _Sem Seu Dispositivo_, ou que possa ficar _Cansado_ de empurrar sua cadeira de rodas... Falaremos um pouco mais sobre isso adiante.

## Narrando para pessoas com deficiências

Uma sessão meio curta, até porque o narrador pode usar muitas das informações das demais sessões para ajudar, ela enfatiza a necessidade de fazer seu dever de casa: se você tem um jogador que tem deficiências visuais, talvez compense gastar um tempo enviando a ficha dele via Internet para que ele use um leitor de tela (de preferência em um formato compatível) ou que você desenvolva uma ficha em tamanho apropriado para que ele consiga ler as informações na mesma.

Além disso, procure tornar acessível o jogo: se você tem um jogador que é Surdo, talvez seja legal que você se posicione de tal modo que ele consiga fazer leitura labial e/ou consiga se comunicar em Libras com ele, ou colocar ele mais perto de você na mesa de modo que o dispositivo coclear dele fique mais próximo e capte melhor a sua voz e assim por diante. 

Uma parte interessante mas que infelizmente não é muito útil no Brasil é que no livro ele trás uma série de gestos básicos para narrar-se Fate com jogadores que compreendam a ASL (_American Signal Language_). Infelizmente, existem algumas diferenças entre ela e a Libras (_Linguagem Brasileira de Sinais_) que torna isso infelizmente um pouco inútil.

Acima de tudo, a ideia é que você deve fazer sua lição de casa e tornar sua mesa acessível.

Além disso, verifique como jogadores que você tenha em mesa com tais condições preferem ser lidos: existem, como dito, aqueles que se enxergam como _pessoa primeiro, condição depois_, e existem aqueles que se definem _primeiro pela condição_. Além disso, tome muito cuidado com as palavras ao se referir a tais condições: eu mesmo estou me preocupando e evitando usar termos como _deficiência_ ou _necessidade especial_, pois sei que tais termos podem ser agressivos a certa pessoas. Com certeza, enquanto alguém razoavelmente comum e "normal" (apenas tenho um grau meio alto de óculos), vou errar, e espero que qualquer pessoa que acredite que tenha algo a me corrigir nesse quesito me procure para que eu possa melhorar.

Por fim, como envolver condições como as tratadas em FAcT pode ter potencial sério para gatilhos, em especial em jogadores que possuem tais condições, o FAcT volta ao tema da Mesa Segura, mencionando os velhos conhecidos _Controle Remoto_ e _Carta X_. Além disso, pode ser interessante utilizar mecanismos de _Trigger Warning_ prévios para deixar claro que pode acontecer certas temáticas e debater com os jogadores até que ponto (se for o caso) pode-se ir.

FAcT trás também o conceito de _“all access gaming”_, jogo para todos. Envolve mais uma filosofia e princípio que regras: é a ideia de tentar tornar mais acessível PARA TODOS a experiência. Várias condições possuem problemas em espaços onde jogos ocorrem: cadeirantes podem ter problemas para entrar, cegos e pessoas com baixa visão podem não conseguir ler as fichas, e uma que não pensamos muito: pessoas surdas podem não conseguir participar devido ao fato de não conseguirem ouvir ou falar! 

Para isso, FAcT faz uma série de sugestões de como lidar com isso, como criar um espaço em um evento onde pessoas surdez possam ser atendidas por narradores e _staff_ preparados para tal, acesso facilitado a cadeirantes, etc... Claramente é bastante provável que você vai errar, mas a preocupação e o desejo e esforço de melhorar devem estar sempre presente.

## Entrando no mundo das Condições

OK...

Dito tudo isso, chegamos às condições _per se_ e seu uso em Fate (e em outros jogos).

Primeiramente, esqueça a ideia do _"Kit do Psicopata Feliz!"_ A ideia aqui é que você faça personagens interessantes, não os trate como bônus ou regras. 

Primeiramente, ele dá uma ideia do que pensar ao incluir uma condição como essas a seu personagem, sendo tal modelo expansível a todo tipo de situação ou condição fora do padrão social que não tenha sido atendida no FAcT: em especial, as questões de gênero foram uma baixa complicada, mas isso deve-se ao princípio do FAcT de colocar pessoas com tais condições falando sobre elas. Mesmo assim, tal gabarito é bem versátil e pode ser usado para quem queira de repente expandir.

Algumas das questões são:

+ Como o personagem obteve tal condição?
+ Que tipo de dispositivos de Acessibilidade ele usa? O que ele tentou e não funcionou?
+ Ele é totalmente daquela condição (cegueira, surdez) ou ele tem uma gradação da mesma (baixa visão, não ouve volumes baixos)
+ Como é a relação do personagem com tal condição? Como ele lida com as pessoas que o questiona sobre a mesma?

Lembre-se que nem toda condição é adquirida de maneira trágica, ao estilo _Alessandro Zanardi_[^1]: algumas são de nascença (cegueira costuma ser uma delas) e outras podem vir da idade (surdez é bem comum em pessoas idosas).

Cada uma das sessões citadas em FAcT representa um espectro de condições e dicas de como lidar com as mesmas, que seguem um molde específico:

+ Uma introdução à condição com comentários sobre detalhes normalmente desconhecidos, como a já citada questão das 200 formas diferentes de nanismo e da diferença entre cegueira total e baixa visão (que pode ser considerada _Cegueira Legal_, como no Brasil)
+ Como trabalhar aquela Condição em Forma de ___Aspectos___: a sugestão geral é refletir em duas vertentes, pelo como a condição restringe o personagem (__*Que diabo é isso de visão periférica?*__) ou no que a pessoa é capaz de fazer (___Precisão rítmica total___)
+ A relação entre Perícias e Condição. Por exemplo, um personagem totalmente cego pode ter _Percepção_, mas ele usa algumas formas de ecolocalização no lugar da visão;
+ A relação entre as Condições e Façanhas. Lembre-se que Façanhas são _sempre positivas_ em circunstancias especiais. Pense, por exemplo, em um personagem cego que tenha uma ___Bengala com Taser___ que, além de dar +2 no Ataque, em caso de Sucesso com Estilo possa reduzir o Estresse em 1 para provocar o Estresse direto nas Consequências... Esse não seria um "ceguinho comum"
+ Condições para as suas condições. Essa regra especial é usada para indicar situações que podem complicar a vida dele por causa de sua condição. Um Autista, por exemplo, pode ___Colapsar___ diante de uma sobrecarga de estímulos visuais, ou um cadeirante pode se ver ___Sem o meu dispositivo___ depois de ser atacado por alguns ladrões que roubem sua cadeira de rodas.
+ Conselhos para o Narradores e Jogadores criarem personagens com tais condições e sobre como pensar tais condições em jogo. Para os Narradores também é incluído algumas dicas para criar-se Antagonistas com tais condições

As condições citadas no FAcT são:

+ ___Cegueira:___ incluindo também coisas como baixa visão, perda de campo, perda de visão de profundidade e sobre o uso de óculos, em especial para graus muito pesados, além de Daltonismo e outras faltas de sensibilidade de cores, como a acromatopsia
+ ___Surdez:___ inclui aí a surdez (em minuscula) para aqueles que recorrem ao uso de dispositivos cocleares, leitura labial e afins, além de falar), a Surdez (em maiúscula), para aqueles que apenas utilizam linguagem de sinais e as pessoas _Duro de Ouvido_ (que tem problemas para ouvir volumes ou frequências específicas). Além disso, fala sobre a "cultura ouvinte" (ou seja, a ideia da normatividade na capacidade de ouvir), que acaba se aplicando a outras condições, e sobre porque você não deve fingir usar uma "linguagem de sinais" estilo _Pig Latin_ ou _Engrish_, além da questão sobre as diferenças culturais envolvendo as múltiplas linguagens de sinais
+ ___Problemas de Mobilidade:___ as questões envolvendo cadeiras de rodas, muletas e similares, e como é legal transformar em sua muleta normal em uma _Muleta de Estoque_, embutindo uma lâmina na mesma!
+ ___Nanismo:___ aqui fui surpreendido, pois além da já citada questão das mais de 200 formas de nanismo, tem também detalhes sobre ___Nanismo Proporcional___ vs ___Nanismo Desproporcional___, e coisas como a reposição hormonal quando necessário e as consequências de sua supressão. Ah, e evite os tropos do Anão/Fadinha/Elfo/Hobbit... Tyrion Lannister tá aí para mostrar que pessoas ananas podem também serem duronas e significativas.
+ ___Doenças Crônicas:___ aqui, assim como nas _Questões de Mobilidade_ inclui-se uma gama extremamente grande de condições, indo de coisas como Diabetes e Anemia Falciforme a Doença de Crohm e Osteogenese Imperfeita (uma forma de _Doença dos Ossos de Vidro_) ou Analgesia Congênita (não sentir dor de forma nenhuma). 
+ ___Espectro Autista:___ uma das mais comumente estereotipadas condições, essa sessão é especialmente interessante pois mostra que uma das coisas normalmente não apresentadas, a dificuldade de processamento sensorial de um personagem, pode não necessariamente ser algo ruim: pense no exemplo que citamos da pessoa que seja hipersensível a detalhes em imagens. Isso pode ser uma _Façanha_ onde ele recebe +2 para detectar, por exemplo, mudanças em ondas sonoras. Uma das sessões mais complexas e interessantes, ela carrega toda uma série de sugestões para pensar-se personagens autistas, como que tipo de estímulos sensoriais o acalma ou o perturba (pense em um personagem que seja acalmado por textos em uma tela mas perturbado por sons de alta frequência), ou interesses específicos (como coletar compulsivamente chaves ou dados ou vidros de perfume), e como tudo isso pode se transformar em boas ideias do personagem.
+ ___Depressão e Ansiedade:___ o famoso _"mal do século"_ e como foi vulgarizado e como às vezes uma pessoa em um dos estados pode enganar dizendo que _"Está tudo bem!"_
+ ___Esquizofrenia:___ difícil de ser diagnosticada e quase um "balaio de gato" para qualquer situação neuro-atípica que não caia em outras definições, cita também como curiosamente os efeitos e diagnóstico da mesma variam conforme a cultura: enquanto no ocidente existe o conceito das "vozes na mente que mandam matar", na Índia tendem a ser mais "calmas". É tão complicado descrever aqui quanto na leitura na sessão no FAcT, tanto quanto  _Depressão e Ansiedade_ e _Autismo_
+ ___Bipolaridade:___ aqui uma questão interessante é que tenta-se "desromantizar" a Bipolaridade como _"doença de Artista"_, já que existem dois tipos básicos de bipolaridade, onde a variação é baseada na potência dos momentos de mania, onde um tipo fica ainda sociável o bastante para tentar "extravasar" essa mania em algo "produtivo", enquanto no outro a pessoa se torna perigosa e paranoica o bastante para demandar atendimento especializado.
+ ___Transtorno de Estresse Pós-Traumático:___ outro clássico deturpado com o _shell-shock_ da época da Guerra do Vietnã, ela pode aparecer por vários motivos e de várias formas. Percepções de cheiros onde não deveriam existir, visões, terrores noturnos, _flashbacks_. Tudo isso pode acontecer com alguém com a PTSD

## Suas Ferramentas e Condições

Por fim, depois das 52 páginas descrevendo as condições exploradas (e que nem cobrem todas as possibilidades), entramos em uma sessão mais relacionadas a regras de Fate.

Tais novas regras visam refletir as necessidades e consequências específicas relacionadas às condições mostradas, como a questão de surtos de Autistas que tenham sobrecargas sensoriais, cadeiras de rodas para cadeirantes ou o clássico óculos fundo de garrafa.

A primeira parte é sobre _Condições_. Novamente, como ocorre em _Horror Toolkit_ e _Dresden Files Accelerated_, essas condições refletem potenciais eventos que podem ocorrer quando elas são ativadas, além das situações que podem levar a tais gatilhos. Também são usadas para estipular regras para que tais condições sejam limpas e a dificuldade de recuperar-se de tais condições. 

De maneira geral, as Condições seguem as mesmas regras do _Ferramentas de Sistemas_, páginas 12 e 13, com a adição de condições _Permanente_, condições que só podem ser desmarcadas após um _Marco Maior_. 

Por exemplo: um personagem Autista pode entrar em um _Surto_, por exemplo, quando em uma situação de hiper-exposição sensorial (lembre de discutir isso com o Narrador). Quando você marca ela quando, por exemplo, o seu personagem Autista com problema de hiper-exposição auditiva acaba no meio de uma ___Multidão Barulhenta___, você deve optar por algumas coisas que vão acontecer com o personagem: ele pode ficar sem conseguir se expressar, ou expressar-se emocionalmente de maneiras explosivas; se ver paralisado, tendo que ser conduzido por alguém de confiança e por aí afora. 

Cada condição trás as regras para marcar a caixa e para limpar tal condição. No caso acima, por exemplo, ele pode precisar ir para um local silencioso, ou com barulhos suaves (talvez com alguns sons de natureza e por aí afora) e permanecer por um bom tempo lá, recuperando seu foco e deixar de ter problemas com a sobre-exposição à qual ele sofreu.

Já na parte dos _Dispositivos de Acessibilidade_, eles podem ser delineados como _Aspectos_ (como em ___Caronte, meu fiel cão guia___) e _Façanhas_, como a _Bengala Branca_ que ajuda o cego a se guiar pela rua, não podendo ter Aspectos de localização forçados contra ele.

Devido até a tal característica, FAcT sugere que se tratem tais _Dispositivos_ como Extras, a serem compostos da mesma maneira, definindo Permissões e Custos e os Benefícios adicionais deles. Além disso, é interessante que existe uma lista de condições listadas que se beneficiam de certos _Dispositivos_: não apenas cegos se beneficiam de _Cães-Guias_, mas Autistas, pessoas com PTSD ou Ansiedade, por exemplo.

Mantendo o exemplo do Cão-Guia, a Permissão para ter um, segundo o FAcT (página 83) seria um Aspecto relacionado à condição específica, e um custo seria uma perícia _Cão-Guia_ que poderia ser usada em momentos onde o Cão-Guia ajuda a pessoa (por exemplo, _Percepção_ para notar locais perigosos e _Atletismo_ para conduzir a pessoa por caminhos adequados). Além disso, sempre tem Façanhas que podem ser compradas, como o fato de seu Cão-Guia ser um ___Defensor Fiel___, o que permite que você use sua perícia _Cão-Guia_ para Defender-se de ataques físicos.

Além disso, ele mostra algumas variações para cada _Dispositivo_: imagina que maneiro seria os seus personagens acharem que o mago maligno cego é indefeso até que ele mostre seu ___Dracolich Guia___? E o piloto de nave cego que tem um ___Metroid Guia___?

## Faça sua lição de casa... E a faça novamente!

Para finalizar, a partir da 101, o FAcT traz uma lista de recursos úteis (em inglês, infelizmente), incluindo coisas como as ferramentas de segurança de mesa como a _Carta X_ e o _Controle Remoto_, e descrições de alguns gestos para usar-se em narrativa de RPG (e Fate em Particular) em ASL. Fica o pedido: algum leitor que se comunique em Libras e puder ajudar, entre em contato com o autor para podermos montar tal lista para Libras. I

Essa última parte, no _Prototype Edition_ (onde estamos fazendo a resenha) ficou infelizmente fraca, já que as descrições não são suficientes: com as artes certamente ficará melhor.

E como "surpresa final", ao fim do FAcT é incluído um conjunto de três páginas para uma ficha de Fate voltada a jogadores com baixa visão, onde a primeira página inclui os Aspectos, a segunda Perícias, Estresse e Consequências e na última Façanhas.

## Abra as Portas

Esse livro vem em uma ótima hora, ainda que possa parecer insuficiente. Um material 4.5/5, ele tem muitos e muitos elementos interessantes não apenas para o jogador do Fate, mas para qualquer RPGista que tenha como intenção sincera tratar questões relacionadas a acessibilidade.

Uma coisa curiosa é que eu tive a experiência de narrar para pessoas sem visão e aprendi técnicas válidas para ajudar elas (como DIZER TODOS OS ASPECTOS DO PERSONAGEM), algumas inclusive que não foram citadas no FAcT. 

Talvez o ponto fraco seja que ele não tem como cobrir tudo: faltou no meu ponto de vista muita coisa. Mas isso infelizmente é uma questão da própria mídia: seria desagradável (para dizer o mínimo) tentar criar um manual completo e acabar com um livro de 1000, 2000 páginas. 

Mas o ponto importante é começar um debate sério e inclusivo sobre como lidar com essas condições, além de prover bases de onde você possa partir para, como citamos muitas vezes aqui e é citado no FAcT, fazer sua lição de casa e pesquisar mais sobre esses assuntos: a Internet está aí, para que ao menos você não comece do zero. Também as pessoas estão aí: se aproxime delas, de seus parentes, pergunte suas necessidades, as dificuldades, as partes que podem ajudar você a colocar coisas interessantes ao lidar com personagens e, acima de tudo, ___jogadores___ que tenham essas condições.

O RPG é um ambiente que se vê como inclusivo, mas na prática é extremamente hostil. Vemos deficiências (aqui é importante dar nomes aos bois) como circunstâncias ruins ou como estereótipos móveis.

É hora de parar de pensar assim, e pensar em pessoas com tais condições como ___PESSOAS___. 

Capazes de serem _Dramáticas, Proativas_ e, acima de tudo, ___COMPETENTES___.

Como todo personagem de Fate deve ser.

[^1]: Alessandro Zanardi foi um piloto de automobilismo que teve ambas as pernas amputadas após uma batida extremamente violenta em uma prova da Fórmula Indy (à época CART Racing - Fórmula Mundial), no autódromo de Lausitzring, na Alemanha. Para mais sobre o acidente, [leia o artigo do _site_ FlatOut](https://www.grandepremio.com.br/indy/noticias/na-garagem-zanardi-sofre-amputacao-das-duas-pernas-apos-acidente-na-alemanha) (TW: gráfico)

<!--  LocalWords:  Reviews book info authors Lauren Bell Casey Brian
 -->
<!--  LocalWords:  Cohen-Moore Engard Sophie Lagacé Philippe-Antoine
 -->
<!--  LocalWords:  Ménard Aser Clark Mysty Vander Zeph Wibby year Rob
 -->
<!--  LocalWords:  Accessibility Toolkit Prototype Edition GURPS DOTS
 -->
<!--  LocalWords:  capacitistas Fred Hick Donoghue Elsa Shoshana FAcT
 -->
<!--  LocalWords:  Sjunneson-Henry neuro-atipicidade Kessock World It
 -->
<!--  LocalWords:  Space Accessibity LARPs didn’t matter that was and
 -->
<!--  LocalWords:  Sherlock blind hardcore dragon fighting princess
 -->
<!--  LocalWords:  deaf one cared if had heart condition wanted be an
 -->
<!--  LocalWords:  epic hero lovecraftiano Talkback Adversary Shadow
 -->
<!--  LocalWords:  não-conformes the Century keytars Loreans Ibiza
 -->
<!--  LocalWords:  boomboxes Locomia capacitismo Crazy impedí-lo Jack
 -->
<!--  LocalWords:  Taser Megacorp X-Games sniper ASL Amerincan Signal
 -->
<!--  LocalWords:  American Language Trigger Warning all access staff
 -->
<!--  LocalWords:  gaming Pig Latin Engrish Hobbit Tyrion Lannister
 -->
<!--  LocalWords:  Crohm Osteogenese shell-shock flashbacks PTSD Indy
 -->
<!--  LocalWords:  Dresden Accelerated Cães-Guias Cão-Guia Dracolich
 -->
<!--  LocalWords:  Metroid RPGista Zanardi CART Racing Lausitzring
 -->
