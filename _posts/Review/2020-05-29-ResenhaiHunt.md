---
layout: reviews
#date: 2018-09-28 10:00:00 -0300
title: Resenha - \#iHunt
language: br
subheadline: Seja um fudido, matando monstros na economia colaborativa!
categories:
  - reviews
tags:  
  - iHunt
  - Fate
  - Evil Hat
  - RPG
book_info:
  cover_img: /assets/img/CapaiHunt.png
  authors: 
     - Olivia Hill
     - Filamena Young
     - Francita Soto
  publisher:
    name: Machine Age Prodcution
    link: http://machineage.tokyo/
  year: 2019
#  isbn: 978-1-61317-170-7
  pages: 328
  formats:
    - PDF
  buy_at:
#    - site: Amazon.com (Capa Dura)
#      link: https://www.amazon.com/Evil-Hat-Productions-Shadow-Century/dp/1613171706
    - site: DriveThruRPG
      link: https://www.drivethrurpg.com/product/298255/iHunt-The-RPG
    - site: itch.io
      link: https://machineage.itch.io/ihunt-the-rpg
---

> Publicado previamente no [site da _Dungeon Geek_](https://www.dungeongeek21.com/resenha-ihunt/)


> ___NA:___ \#iHunt é um jogo mais maduro, portanto sua resenha também será. Contem altos níveis de sarcasmo e baixo calão.

No século 21, tudo parece muito fácil: o capitalismo tardio das _dotcom_ mostrou o caminho, apesar da bolha. Virou quase uma regra 34 da vida: se pode ser feito, alguém já fez uma _app_ de celular para gerenciar quem faz aquilo.

A maior rede de hotéis não tem um quarto de hotel.

A maior rede de entregas não possui um carro de entrega.

A maior rede de taxis não tem um taxi.

A maior rede de caçadores de monstros não tem um caçador contratado.

Sim... Monstros existem, e não apenas os burgueses do caralho que ficam tomando café cagado por passarinhos e pagando milhares de dólares no copo.

Desde o início da existência humanos, os monstros existem, em todas as suas formas e tipos. Claro, nem todos são criaturas assombrosas destruidoras de mentes. E nem todas são material de _fanfic_ de Crepúsculo. Mas todos precisam saciar sua fome, não importa sua origem: mesmo aqueles que surgem a partir de desastres corporativos (sim, eles acontecem) precisam se alimentar.

E claro que tem gente que quer se livrar dessas encrencas.

E com o menor custo possível.

E por que não trabalhar na _"economia do compartilhamento"_ (que piada) e colocar em uma _app_ o pedido de _"ajuda contra o vampiro que quer chupar minha irmã"_?

Azar do bando de fodidos que vão se tretar com o vampiro. Ninguém mandou não estudar em boas faculdades: afinal, é só se esforçar o suficiente e tudo dá certo, né?

_Meu pau de óculos!_

## Preparando a caçada

[_\#iHunt - Hunting Monsters on the Gig Economy_](https://machineage.itch.io/ihunt-the-rpg) (Caçando Monstros na Economia do Compartilhamento) é um dos mais interessantes jogos que saíram recentemente. Baseado em uma série de livros da mesma autora [disponíveis também em seu itch.io](https://machineage.itch.io/), basicamente ele é um jogo sobre _millenials_.

Não... Não estamos falando de _millenials_ que tomam café cagado por pássaros em copos de papel reciclável ou feitos de juta colhida de maneira ambientalmente sustentável por ribeirinhos da Amazônia.

Esses com certeza tem grana o bastante para não se envolverem com \#iHunting, a não ser para encomendar serviços.

Os _millenials_ aqui são aqueles que o liberalismo econômico, somado com o conservadorismo social, literalmente tem fodido: aqueles que não se qualificam como parte da classe média alta. 

\#iHunting não é para Faria Limers.

Normalmente, o \#iHunter típico é alguém que precisa desesperadamente de dinheiro, ao ponto de considerar arriscar o próprio pescoço caçando monstros. Uma pessoa pode ter tido contato com monstros e nem por isso tornar-se um \#iHunter: é necessário um certo grau de desespero para colocar-se diante de uma criatura que pode arrancar sua cabeça e manter você vivo o suficiente para ver seu corpo esviscerado cair no chão. 

Nada que alguns anos de empréstimo estudantil e cartões de crédito atrasados não façam. Ou ser expulso de casa por causa do babaca do pai abusivo e/ou misógino e/ou homofóbico e/ou ___"Cidadão de Bem®"___ e precisar ratear o aluguel de uma casa com alguns amigos.

Claro... Na teoria, seu emprego diário deveria bancar isso e mais, mas a verdade é que os empregos atuais, com sua precarização cada vez maior e salários cada vez menores, estão empurrando as pessoas para o abismo social. O neoliberalismo acabou com o colchão social, a rede de proteção que fazia com que a pessoa pudesse, de repente, passar alguns meses sem emprego se especializando para tentar algo novo: agora é cobra comendo cobra. E o dinheiro do trabalho das 7 às 5 permite apenas sobreviver e olhe lá.

Claro, existem agrupamentos de \#iHunters, e até mesmo algumas categorizações dos mesmos baseados em suas estratégicas de caçada, mas mesmo isso não é algo formal, mas uma linguagem própria surgida da troca de informações importantes para a sobrevivência. Seres humanos são sociais e se agrupam conforme a necessidade. Tudo depende de saber qual é o seu Lance.

Apenas torça para que você não tenha sua "luta justa" contra um vampiro/lobisomem/demônio/coisa-bizonha-que-quer-seu-cérebro-como-comida-e-corpo-como-roupa. Isso pode ser o seu fim.

## Instalando o App

\#iHunt é um jogo baseado em Fate Básico, mas com algumas diferenças, em especial na criação dos Monstros.

Inicialmente, todo \#iHunter possui 6 Aspectos: além de Dois livres a serem usados no final da criação de personagem, e do tradicional _Conceito_, temos o _Drama_, que substitui a Dificuldade com o mesmo objetivo: ser aquela pedra no sapato que ferra você vira e mexe, como _"Vivendo em uma ocupação por vontade própria"_ ou _"Me apaixono fácil"_.

Além disso, dois Aspectos importantes são o seu _Objetivo Real_ (_Vision Board_) e seu _Trabalho Diurno_ (_Daily Job_)

O ___Objetivo Real___ é algo que você mira com muita intensidade: de certa forma, é algo que você deseja tanto, que é quando você acha que vai "vencer na vida". Obviamente, você ainda não tem isso, mas é o símbolo máximo de sucesso que você pode imaginar, ao menos no momento. Pode ser qualquer coisa, como ___Uma Casa no Campo onde possa plantar meus amigos, meus discos e livros e nada mais___, ___Uma Família de Verdade___ ou mesmo ___Que meu corpo reflita minha alma___. O importante é que tem que ser algo que simbolize um desejo tão profundo que possa ser em muitos momentos o que vai fazer você se enfiar em \#iHunting para tentar conseguir.

Já o ___Trabalho Diurno___ é o que você faz "de verdade". Na moral, \#iHunt não é exatamente uma forma segura de sobrevivência... E não apenas por causa dos monstros. Os clientes podem dar uma avaliação ruim e com isso o valor da caçada cair. O seu banco pode achar que o \#iHunt é uma forma de lavagem de dinheiro e não permitir a grana entrar na tua conta. E mesmo com tudo isso, pode passar semanas sem que você encontre uma caçada que compense, considerando sua habilidade e valores. Você ___vai precisar___ de uma forma de se sustentar nesse meio tempo, e essa é o seu ___Trabalho Diurno___. Na prática pode ser de tudo, de ___Reparar Celulares na 25 de Março___ a ___Youtuber em vias de se tornar famosa (\#sqn)___ até mesmo a ___Princesa Disney de Festa Infantil___ ou ___VASP - Vagabundo Anônimo Sustentado pelo Pai___. Não importa o que, essa é a forma que você tem de ganhar algum dinheiro para se virar, enquanto faz seus corres.

Além disso, nesse momento ou antes dele, você deve definir qual é o seu _Lance_ (_Kink_). O _Lance_ é como você caça monstros: os \#iHunters se "dividem" em grupos informais por meio dos Lances, ainda que exista muito cruzamento entre os \#iHunters ao ponto de que os _Lances_ não sejam fixos o bastante para serem grupos formais, mas mais maneiras gerais de dizer sobre o estilo e método de cada \#iHunter.

Ao escolher um _Lance_, você recebe uma _Façanha_ exclusiva daquele Lance e destranca a possibilidade de comprar outras conforme evoluir como \#iHunter. Os _Lances_ são:

+ __*Cavaleiros* (Knights):__ o básico caçador de monstros. Se tem uma forma de matar o monstro na porrada, é assim que o _Cavaleiro_ vai fazer. Senão, ele descobre como abrir uma brecha e aí então sentar a porrada no monstro. Pense em Buffy ou nos irmãos Winchester de Supernatural;
+ ___Evileenas:___ chamados assim por causa de uma personagem de um filme _trash_ do cenário, são os que buscam mais o conhecimento sobre os monstros, e algumas vezes até mesmo descobrem como operar magia ou coisas similares para enfrentar os monstros em seu próprio território. Pense em Giles de _Buffy_ ou em John Constantine;
+ ___Phooeys:___ os _phooeys_ são um fenômeno da nossa era hiperconectada. Teóricos de Conspiração, _hackers_, _cyberativistas_ que se envolveram com monstros e assim por diante, os _phooey_ montaram toda uma rede de servidores _mesh_ conectados na _deep web_ usando serviços de redes sociais descentralizadas ao exemplo do _Mastodon_ ou do _Scuttlebutt_ para facilitar a troca de informações sobre monstros e de formas de tecnologias para lidar com eles. Provavelmente é a ele que você vai recorrer quando precisar de uma cópia PDF do _Malleus Malleficarum_ ou para criar um dispositivo para capturar demônios. Pense nos Caça-Fantasmas com um pouco da Penelope Garcia de _Criminal Minds_;
+ ___Os 66:___ formaram-se após uma série de eventos bizarros que sempre envolveram de uma forma ou de outra o número 66, como assassinatos cuja vítima recebeu exatos 66 golpes de faca, ou 66 igrejas queimadas ao mesmo tempo em diversos estados, os 66 lutam de maneira comunitária, agrupando-se e formando resistências para quebrar o sistema. Anarquistas, revolucionários, eles conseguem vencer por meio da destruição e reconstrução e são especialistas em achar brechas de algo e aproveitar contra elas. Monstro, governos, países, redes... Se existe uma porta, eles arrombam. Se não existe, eles criam uma e a arrombam. Pense nos _Pistoleiros Solitários_ de _Arquivo X_;

Perceba que o fato de você estar em um Lance específico não quer dizer que você não possa aprender habilidades fora do que seria normal... Como diria o famoso xamã cobra Ess El-El de Shadowrun, _"aprenda o que puder, porque na rua burrice mata mais rápido que bola de fogo"_. A mesma regra vale para o \#iHunt: mesmo que você seja um _Evileena_, é sempre bom aprender como lutar, afinal o monstro não vai cair sozinho, e aquele ritual que você pesquisou de nada vai adiantar se suas tripas estiverem para fora. Por outro lado, se você for um _Cavaleiro_, ficar batendo que nem um imbecil em um inimigo sobre o qual você nada conhece pode ser uma passagem só de ida para o Além, portanto conhecer um truque aqui ou ali de um Evileena ou 66 ajuda bastante.

E aqui temos outra mudança em \#iHunt. Ao invés de vir com perícias, em \#iHunt você tem _pacotes de perícias_ que de certa forma permeiam ideias envolvendo certos tipos de profissões, como _Ativista, Guerreiro, Trapaceiro, Influenciador_ ou até mesmo _Serviço Social_. Ao realizar um rolamento, você irá ver qual profissão é mais condizente com isso, ainda que existam referências.

## A Vantagem

Uma regra importante surge na questão dos rolamentos de dados: uma luta entre \#iHunters e monstros NUNCA É JUSTA. Dizer que _"fulano teve uma luta justa"_ dentro do linguajar dos \#iHunters é dizer que ele morreu. E em termos de regras, o \#iHunt traz uma mecânica para simular isso que é _A Vantagem_ (_Edge_).

A Vantagem representa a mão superior que um lado tem sobre o outro em conflitos. Normalmente os monstros terão a Vantagem de partida (isso fica a critério do Narrador), mas os \#iHunters possuem todo tipo de mecanismo para tomar para si _A Vantagem_: pesquisar fraquezas, preparar armadilhas, colocar-se em locais onde podem atacar de maneira segura, tudo isso pode fazer com que os \#iHunters tenham a vantagem sobre os monstros.

Enquanto tiver com a vantagem, qualquer personagem daquele lado pode utilizar uma primeira vez a mesma sem precisar pagar PDs para a usar (demais usos são ao custo de 1 PD). Sempre que usar a Vantagem, o personagem que o fizer substitui 1 dado Fate por 1 dado comum de 6 lados (d6), portanto rolando 3dF+1d6 ao invés de 4dF. 

Isso gera uma grande vantagem mecânica, já que o [valor médio do rolamento com vantagem](https://anydice.com/program/19fae) estará quase no [vale positivo do rolamento comum](https://anydice.com/program/1035b). Perceba que nessa situação da vantagem, existe menos de 20% de chance do rolamento sair com menos de +2, enquanto em um rolamento tradicional existe menos de 20% de sair +2 ou melhor (consulte os _links_ se não acredita).

Essa variação faz com que monstros se tornem muito poderosos e ter-se a vantagem seja algo importantíssimo para um caçador caçar um monstro, já que às vezes é a diferença entre a vida e a morte do seu caçador. Para pegar a Vantagem para si, existem três formas. 

A primeira é por meio de rolamentos de _Superar_ contra um rolamento de defesa do monstro ou seu "nível de ameaça" (_Star rating_, mais sobre isso adiante), o que for maior. 

A segunda é que certos Poderes e Características dos Monstros demandam que eles passem a Vantagem para a oposição ao utilizar tais poderes.

A terceira é caso você consiga uma ___defesa avassaladora___ (_overwhelming defense_) contra quem está com a Vantagem. Isso ocorre quando você é ___bem-sucedido com estilo___ na Defesa contra um ataque onde o alvo detém a Vantagem (não necessariamente a usou). Ao conseguir, você toma a Vantagem para si.

Uma coisa importante é que a Vantagem nunca é tomada para um indivíduo, mas sim para um lado: se tiverem quatro \#iHunters contra um monstro e um dos \#iHunters conseguir tomar a Vantagem, qualquer dos \#iHunters pode recorrer à Vantagem, seguindo a restrição já citada de apenas um uso gratuito, os demais demandando PDs. Esse uso gratuito é "resetado" quando a Vantagem muda de lado.

Para determinar qual lado terá a vantagem, o narrador tem que analisar se:

+ Nenhum dos lados possui monstros ou ambos possuem e o nível de ameaça dos mais poderosos é igual e;
+ Não existe nenhuma vantagem que um lado tenha preparado para usar de maneira objetiva contra o outro.

> ___Exemplo:___ Selena e Kyle estão caçando um vampiro de nível de ameaça 2. Como ambos são humanos e o alvo é um vampiro, na teoria um confronto entre ambos poderia gerar uma situação onde o Vampiro teria a Vantagem. Entretanto, Selena e Kyle são \#iHunters experientes, e já prepararam algumas coisinhas para ajudar os mesmos contra o Vampiro. Como eles tem isso como Vantagens em mãos como Aspectos, por saberem que __*Esse Vampiro* REALMENTE *morre à luz do Sol*__ e ___Ele é vaidoso demais para seu próprio bem___. Com essas vantagens, o narrador pode dizer que ninguém tem vantagem ou mesmo que são os \#iHunters que estão com a Vantagem devido a tal preparação.

Caso aconteça de não haver um lado que comece a cena com a Vantagem, ainda assim a Vantagem pode surgir pelos meios acima.

# Os Monstros de _\#iHunt_

\#iHunt é sobre caçar monstros e sobre os questionamentos sobre isso. Claro que é muito divertido você mandar um FDP de um vampiro traficante de drogas e de prostitutas para a vala, alguém para quem o alemão tem até uma palavra ([_Backpfeifengesicht_](https://lmgtfy.com/?q=backpfeifengesicht) - procurem no Google.). Mas a questão é que \#iHunt não é sobre isso exatamente: claro que existem caras que são _Backpfeifengesicht_ de montão no cenário de _San Genaro_ onde se passa o jogo, mas isso não quer dizer que todos os monstros sejam assim. Na verdade, muito provavelmente eles são uma minoria, ainda que numerosa. Em \#iHunt monstros não são alegorias: monstros são criaturas como quaisquer outras. Pense que _Santa Clarita Diet_ é uma opção muito possível (e provável) em \#iHunt. E às vezes você enquanto \#iHunter vai se descobrir trabalhando para o FDP da vez (isso é que dá deslizar para direita em qualquer coisa).

De qualquer modo...

Monstros... Sem ele não existe caçada de monstros e sem caçada de monstros nada de \#iHunt, e nada de grana.

Portanto, vamos a eles

Na realidade, o livro trás alguns moldes para os monstros (_clades_) que permitem que você não precise construir o seu monstros do zero. Na prática, todos os monstros pagam por seus _Poderes_ (_Gifts_) e _Habilidades_(_Features_) com Recarga, recebendo Recarga adicional ao comprar _Falhas_ (_Banes_).

E aqui entra uma coisa importante...

### _Nível de Ameaça_ (_Star System_)

Ao criar o seu monstro, você irá usar o Nível de Ameaça (de 1 a 5) para definir seu poder inicial. Ele diz quantos pontos o monstro tem para distribuir em seus _pacotes de perícias_, o maior nível que ele pode ter no mesmo e total de recarga que ele pode ter. A partir daí, distribua as coisas normalmente.

Quando houver a necessidade de usar o _Nível de Ameaça_ em testes, como no caso de tentar-se obter a Vantagem, se for uma oposição passiva, utilize o dobro do _Nível de Ameaça_ como dificuldade. Portanto, ao tomar-se a Vantagem de um monstro de nível 3, você deve no mínimo conseguir um rolamento acima de _Fantástico (+6)_ (considerando que o rolamento oposto dele foi menor).

E isso puxa uma coisa:

É possível jogar-se com um monstro caçador de monstros (sim, estamos falando de Blade e Castiel aqui, senhores)?

Na prática sim. Se isso ocorrer, dê ao jogador 20 níveis para distribuir em seus _Pacotes de Perícias_ e 5 Pontos de Recarga para gastar em seus poderes de monstro e coisas do gênero. Isso vai tornar o personagem muito poderoso, mas lembre-se que monstros possuem fraquezas bem sérias. Além disso, é necessário que o personagem tenha sido contaminado por um monstro previamente e isso pode não ser simples (muitas vezes é mais fácil morrer no processo, lembrando _Um Lobisomem Americano em Londres_)

### Os tipos de Monstros e sua variedade

O sistema de _Poderes_, _Características_ e _Fraquezas_ torna criar monstros únicos algo razoavelmente simples se você tiver planejamento prévio. Entretanto, você pode não ter tempo de o fazer. Para isso, dentro de \#iHunt existem modos de criar monstros tradicionais, mas com seu próprio _twist_. Além disso, existem as _subclades_, onde você compra algumas habilidades adicionais para definir subtipos específicos com fraquezas e poderes diferentes.

Dito isso, alguns deles são comuns... Mas com diferenças específicas:

+ ___Vampiros:___ nascidos da sede de sangue, são mais Bram Stoker e menos Crepúsculo, embora ainda possam andar à luz do sol sem brilhar ou derreter. Também não tem problemas com símbolos sagrados e convites (é rude, mas ainda assim podem entrar onde quiser quando quiser);
+ ___Feiticeiros:___ com a popularização do ocultismo e a troca de PDFs via Internet, de maneira geral os feiticeiros tem aumentado em números, vindos de todos os tipos e _backgrounds_. Entretanto, sempre são complicados, em especial quando arrumam uma cópia em PDF do _Necronomicon_ (quem achou que isso era uma boa ideia?)
+ ___Lobisomens:___ a maioria dos lobisomens são nascidos assim e em geral conseguem lidar bem com "aquelas noites". E na prática muitos não precisam de carne humana (jogue umas coxas de galinha cruas para eles e está de boa). O maior problema são os poucos que surgem de transmissões virais e os que piram na batatinha no processo. Algumas vezes ambos em um pacote só. Além disso, são muito territoriais, e se eles perceberem você aprontando em seus territórios, que Deus tenha piedade;
+ ___Reptoides:___ invasores alienígenas Illuminati que desejam dominar o mundo e devorar a humanidade... Ok, tire a parte do Illuminati: os reptoides em \#iHunt não são exatamente espertos, mas são muito violentos. Não muito mais que o típico _redneck_ eleitor do Trump, mas o suficiente para serem encrenca. Alguns se reproduzem ao estilo _Alien o Oitavo Passageiro_, o que adiciona injúria à calúnia;
+ ___Demônios:___ Ok... A coisa aqui é que nem todo Demônio vem do Inferno e nem todo é mal. Ok... A maioria é, mas ainda assim o que você precisa saber é que eles são poderosos e se alimentam de algum tipo de força vital específica. A Duqueza Infernal de San Genaro mantem _raves_ especiais onde ela se alimenta da adoração dos participantes por sua música eletrônica, que escutam e dançam até caírem extenuados. Porém, ela tem uma dieta meio restrita... Nada de balas e doces, se você me entende... Muitos \#iHunters gostam dela, e os que não gostam e tentaram a caçar não estão mais entre nós;
+ ___Os Mortos Famintos:___ _Walking Dead, Resident Evil, iZombie, Santa Clarita Diet_... Todos eles tem um pouco da verdade sobre os desmortos, aqueles que por alguma razão morreram mas _não permaneceram mortos!_ Há até mesmo aqueles que são altamente funcionais: mantenha os mesmos alimentados e eles podem até ajudar a caçar seus irmãos não tão favorecidos e os crimes que eles cometem (já vimos isso em algum lugar?)

### A Essência

Por fim, vários dos monstros possuem algum tipo de medida de poder que eles tem para usar suas habilidades, por meio da Essência. Ela normalmente se carrega de alguma forma e é usada de maneiras diferentes conforme seus poderes: vampiros bebem sangue, Mortos Famintos devoram cérebros (ou outras partes), Feiticeiros realizam sacrifícios ou rituais para obter energia da natureza... Você entendeu.

Em certos monstros, a Essência também serve para determinar o quão desesperado ele está para obter a mesma. A Essência é dividida em cinco níveis (_Faminto, Sem Alimento, Saciado, Cheio_ e _Glutão_) onde cada vez que um poder que demande Essência for usado e demandar o mesmo, a Essência cai em um nível.

A Essência também é um Aspecto no Monstro que pode ser usado, mas com uma diferença: cada nível de Essência fornece um bônus diferente para usos de Pontos de Destino ao invocar-se a mesma (chamado de _Potência_). Sempre que a Essência de um monstro aumentar, ele recebe uma invocação gratuita, e sempre que ela abaixar, a oposição recebe uma invocação gratuita. Isso torna um monstro muito perigoso quanto _Faminto_, mas \#iHunters espertos (que chamamos de _sobreviventes_) costumam usar bem isso para atacar um Monstro quando ele está faminto e a Potência então voltar-se contra o mesmo.

A lista de _Habilidades_, _Poderes_ e _Fraquezas_ engloba muitas das situações tradicionais e explicam muito bem como as usar, assim como seus custos.

## Jogando Seguro (ou _"Fascistas não são bem-vindos"_)

Antes de seguirmos adiante, ao falarmos dos monstros temos que falar sobre _Jogo Seguro_. Muitos poderes dos Monstros vão contra a agência do personagem. E isso pode ser extremamente chato para os jogadores. Além disso, muitas situações propostas em \#iHunt são potencialmente gatilhantes: lembre-se que os personagens são gente fodida, que só se enfia em \#iHunts porque a opção é o crime. Gente que já é vítima de esculacho o tempo todo por qualquer razão (incluindo nenhuma). Além disso, como você vai explicar para o policial (e lembre-se: em \#iHunt, ___All Cops Are Bastards___, todo policial é um babaca) que o CEO que você acabou de apagar na verdade era um Demônio que se alimenta do desespero provocado pelas demissões em massa de empresas falidas ou adquiridas por outras?

Para evitar isso, além de velhos conhecidos como o _X-Card_ e o _Controle Remoto_, o \#iHunt propõe um questionário similar ao visto em [Consent in Gaming](https://www.montecookgames.com/consent-in-gaming/) da Monte Cook Games, onde você detalha o tipo de coisas que você aceita ou não em jogo, e se você só topa se não te envolver. Além disso, existe o conceito do _Intervalo Comercial_, que é uma variação do _X-Card_: qualquer um pode pedir um _Intervalo Comercial_ para respirar um pouco se as coisas saírem do controles (sempre um ótimo momento para a pizza com o refri). Dê um tempo, despressurize um pouco e depois volte para a ação

Pode parecer estranho isso, mas ao saber qual o espaço de manobra que você tem, fica mais interessante para você definir limites e posicionamentos dentro do cenário. Afinal de contas, se ninguém tá no clima para homofobia, por que colocar isso? Para que chamar o personagem de "negão fedido" se o grupo não quer racismo na mesa?

Aproveitando... Se você é um fascista (enrustido ou não), \#iHunt não é para você. Você não é o Constantine, no máximo você é um mal-amado (para não utilizar termo mais chulo) que precisa terapia (mesmo que isso signifique ter uma pia cheia de roupa suja para lavar).

Para deixar bem claro, como escrito no livro... 

> ### NO FASCISTS
>
> If you’re a fascist, you’re not welcome to play this game. It’s against the rules. If you’re reading this and thinking, _“You  just call everyone you disagree with a fascist,”_ then you’re probably a fascist, or incapable of drawing inferences from context and acknowledging a dangerous political climate that causes the oppressed to be hyperbolic. Don’t play this game. Heal yourself. Grow. Learn. Watch some _Mr. Rogers’ Neighborhood_ or something. 

Ou no velho português:

> ### NADA DE FASCISTAS
>
> Se você é um fascista, você não é bem vindo nesse jogo. É contra as regras. Se você está lendo isso e pensando, _"Porra, agora todo mundo que não concorda com você é fascista?"_, então provavelmente você é um fascista, ou ao menos é incapaz de inferir coisas do contexto e reconhecer um ambiente político perigoso que tornou aqueles que são oprimidos mais raivosos. Não jogo esse jogo. Vá se cuidar. Cresça. Aprenda. Vá assistir _"Um lindo dia na vizinhança"_ ou algo similar.

## \#sendoumfodido

Você deve ter reparado que vivo usando o termo "fodido" para os \#iHunters. E não tenho a menor vontade de me justificar, pois é verdade.

Pense bem: que tipo de louco se enfiaria em uma pancadaria contra monstros sobrenaturais por grana? 

\#iHunters não estão nessa por que querem, mas sim porque, de certa forma, o capitalismo falhou com eles, ou eles basicamente são onde o capitalismo externalizou tudo: afinal de contas, para que pagar um tratamento para AIDS para todos? Na hora de virar os olhinhos foi bom, né? Se fode aí.

Dito isso, todo mundo dá seus pulos para sobreviver:

Existe um capítulo inteiro chamado \#THINKINGPOOR mostrando como é você ser um fodido o bastante para ir em \#iHunt. Como é não saber se você vai ter almoço, conseguir roupas apenas em doações do Exército da Salvação e usar a conectividade da biblioteca local para baixar um PDF que você precisa para estudar uma fraqueza de um monstro da caçada atual com aquele tablet que você comprou com parte da grana que ganhou na última caçada e que já tá com a tela quebrada e você não conseguiu trocar a tela pois você usou o dinheiro para comprar a insulina para sua diabetes (350 dólares na farmácia local), sendo que você ainda tem que pagar a luz!

Existe toda uma série de reflexões sobre o que gera o fenômeno de tantas pessoas recorrerem a economia de compartilhamento (_spoiler:_ não é porque elas andam viciadas em cafés de grãos cagados por passarinhos), por que isso passa a sensação de incompetência (que é mítica) e sobre como pessoas de uma renda menor desenvolvem um estilo de vida onde ou elas se empanturram ou morrem de fome. 

Além disso, uma coisa importante: no capitalismo a riqueza é fetichizada. Ser rico é mais do que ter dinheiro: o dinheiro serve como forma de dignidade. O Dinheiro é melhor que Deus, por que garante poder. E alguém fodido não tem isso. Isso leva a necessidade que uma pessoa fodida teria para ter o carro do ano, o celular top... Te faz se sentir importante.

E existe todas as explicações de como o sistema fode você: como o fato de você precisar de um comprovante de residência te fode para abrir uma conta no banco, o que te fode na dificuldade de receber seu salário. Como cada uma dessas pequenas merdas te fode a tal ponto que você fica fodido de verde e amarelo. 

E não é só nisso. São 26 páginas onde se comenta todas as formas como o sistema pode te foder e tudo que você pode fazer (legalmente ou não) para dar seus pulos e sobreviver para seguir adiante e, talvez, conseguir o que você deseja. Em especial, a sessão de \#LIFEHACKS é muito divertida, com tudo o que você pode fazer para se virar, de tentar escavar lixo corporativo por peças para vender no eBay como relíquia a coisas bem menos bacanas. 

Mas é assim que é a vida de um fodido, e \#iHunters em geral são fodidos. 

## \#iHunt - agora com novos recursos

Em uma outra sessão, é falado sobre a parte do programa \#iHunt, e sobre como o próprio app te fode (sim... Lembre-se, você é um fodido. De outra forma, não estaria caçando monstros para complementar renda)

A primeira coisa: no mundo de \#iHunt, monstros sempre existiram. As pessoas apenas são mais educadas para ignorar coisas estranhas. Afinal de contas, se tem um cara meio acabado em uma cracolândia, o que é mais fácil acreditar?

+ Ele é um cracudo que escolheu isso?
+ Uma súcubo o drenou sexualmente e o viciou a ponto de ele ir parar nas ruas?

O \#iHunt é um típico fruto da tecnologia da Web 3.0: _mobile first_ (ou seja, depende de celular como o WhatsApp ou o Uber), viu um problema e resolveu-o por meio de um aplicativo. Mas quem está por trás da empresa \#iHunt? Monstros tentando eliminar a escumalha sem sujar suas mãos? Caçadores utilizando o mesmo como prospecção? Ambos? Nenhum? Apenas alguém tentando fazer dinheiro em cima de um problema? Um Elon Musk da caçada dos monstros? Qual é a verdade? Elas são excludentes?

Basicamente, o \#iHunt é tipo um Tinder ou Uber da caçada de monstros e funciona assim:

+ Alguém posta um pedido de caçada: não necessariamente matar, mas em geral é eliminar um problema. Ele define um preço pelo serviço e os algoritmos do \#iHunt (sempre eles) definem qual é a dificuldade geral do alvo
+ Um \#iHunter pode aceitar ou não o serviço, basta arrastar para a direita para aceitar ou para a esquerda para negar. Uma vez aceito, o \#iHunter pode interagir com o cliente para tentar obter maiores detalhes e dar parecer do que está acontecendo. Se o negar, aquela caçada não aparece mais na sua lista (imaginando que o algoritmo não endoide);
+ Uma vez que o \#iHunter cumpra o serviço, uma _selfie_ é enviada comprovando ao cliente que o serviço foi feito
+ O cliente avalia o serviço e dá uma nota ao \#iHunter. Notas baixas pode resultar em descontos oferecidos pela plataforma (e descontados do valor do \#iHunter) e o pagamento é efetuado.
+ O \#iHunter tem seu valor final creditado pela plataforma, após cobrada a taxa do aplicativo

E só por aí você pode perceber o tanto de merda que pode acontecer: alvos mal-avaliados, clientes insatisfeitos, clientes pegajosos, algoritmos falhando e dando o mesmo serviço para vários \#iHunters, bancos que não aceitam os depósitos...

Tudo para te foder, óbvio.

Para ajudar, o \#iHunt não aceita que \#iHunters negociem valores por fora: isso pode levar ao encerramento da sua conta no serviço por violação dos termos de uso do aplicativo: sim, as porras das letras miúdas da EULA que ninguém lê. 

E eles não se envolvem com grupos caçadores de monstros que já existam. Sim, eles existem e, sim, eles não são tão fodidos quanto você, tendo mais equipamento e conhecimento e experiência e treinamento de séculos caçando monstros. Algumas vezes eles podem te contratar via \#iHunt para dar uma aliviada nos mais chinfrim e deixar o grande _boss_ (que provavelmente vale muito dinheiro e tem uma caçada só para ele no \#iHunt) para eles. Eles podem ver você e decidir te trazer para dentro dos seus números, com a vantagem de ter alguns benefícios melhores, até mesmo incluindo planos de saúde. Ou eles podem ficar muito putos porque você, um fodido qualquer, tá caçando a caça deles...

E claro que existem _apps_ para resolver o problema do \#iHunt (afinal, _there's an app for that_, sempre tem uma app para resolver tudo), desde um esquema atrelado ao \#iHunt para caso um lobisomem te estripe e você precise que alguém faça seus órgão internos voltarem a ser internos sem muitas perguntas, até mesmo um sistema de armazenamento em nuvem onde você vai postar aquela cópia de _O Rei em Amarelo_ em PDF que você pegou com seu colega Phooey: nunca se sabe quando precisará desenhar o Símbolo Antigo. Sempre sem muitas perguntas, desde que a grana flua.

## O Trampo

A cidade de San Genaro é a típica cidade do Vale do Sílicio fictícia com monstros. É como se a Beauchamps de True Blood fosse teletransportada para o sul da California e ao invés de hamburgeres e comida _cajun_ você tivesse comida vegana e café de grãos cagados por passarinhos (eu sei, está ficando chata essa piada... Vai acabar logo). Se você jogou GTA, deve estar entendendo o clima, mas troque o crime por caçada de monstros. O cenário é bem detalhado, em sua opulência decadente e seu realismo com tapa na cara... De repente, o cracudo na rua era um \#iHunter foda até ter seu cérebro transformado em geleia por vício em mordidas de vampiro, que ele tenta enganar com crack.

No caso, uma coisa interessante que \#iHunt oferece é um gerador automático de Trampos (_gigs_), onde você pode gerar aleatoriamente todas as coisas que podem acontecer no trampo e as merdas que podem dar disso. Afinal, de repente você foi mandado para matar um vampiro, mas na verdade ele é bacana e o apenas quer proteger uma galera em uma região que é alvo de uma tentativa de gentrificação por parte do cliente... E aí?

E para aumentar ainda mais as incertezas, o \#iHunt adiciona um novo mecanismo aos Aspectos que é ___Arriscar os Aspectos___ (_imperish_). Ao arrisca um Aspecto, além de oferecer 1 PD, o Narrador (ou quem o fizer, já que jogadores podem Arriscar Aspectos de NPCs) oferece duas escolhas potencialmente ruins. Tipo: você tá inundado de gasolina e o Monstro quer te matar, bebendo seu sangue. E aí? Você pode Arriscas um Aspecto ___Vivo por tempo demais para morrer___ do mesmo para impor um dilema: vai tentar a morder e com isso  virar cinzas, ou vai fugir e deixar ela fugir. A parte importante é que ambas as escolhas são potencialmente ruins. Além disso, existem limites para usar isso: o Narrador pode usar apenas uma vez durante uma cena e contra apenas um PC. PCs só podem o fazer se, durante a cena, sofrerem uma consequência moderada ou pior.

### _Selfies_ - as cicatrizes de batalha dos \#iHunters

Lembra-se que a caçada só é paga quando você envia uma _selfie_ para o servidor e o cliente aprova? Em \#iHunt sugere-se que isso se replique nas fichas de personagem. Cada marco do personagem gera uma _selfie_ que, além de prover os efeitos tradicionais dos marcos, geram pseudo-Aspectos chamados _callbacks_. Cada _callback_ permite que você tenha um determinado nível de benefício sempre que invocar eles for relevantes:

+ _Marcos menores_ geram _Clima maneiro_ que você pode usar, uma vez por episódio (sessão), aumentar em +1 um rolamento.

> Stella possui o _Clima Maneiro_ ***"Uma vez mais celebramos mandar um sanguessuga para a vala"***. Ela está caçando outro vampiro e lembra todas as discussões que ela e Helena, sua parceira (em todos os sentidos) tiveram enquanto estavam comemorando a caçada bem-sucedida em uma _rave_ ao som de _Daft Punk_. Ela recebe +1 no rolamento para tentar localizar o vampiro

+ _Marcos significativos_ geram _Grandes Negócios_, que você pode invocar como um Aspecto uma vez por sessão

> Mikaela está pesquisando uns livros de ocultismo para encontrar informações sobre um demônio enquanto ouvindo ___"Bibbity-Bobbity Boo"___, que é o nome de um Grande negócio onde, loucura loucura, ela descobriu em uma de suas primeiras caçadas que poderia usar magia graças a um demônio que na prática era apenas uma fada madrinha estabelecida em um _barrio_ em San Genaro. Ela lembra que louco foi sentar o _"Bibbity-Bobbity Boo"_ na cara do babaca do cliente, e ainda ganhar uma grana com isso, já que o lazarento era ele próprio um demônio. Agora ela é uma aprendiz de fada madrinha e tem seu próprio negócio de personagens vivos! Isso a ajuda a se focar na investigação: afinal de contas, aquela roupa da Cinderela não está barata.

+ _Marcos Maiores_ geram _Eventos Que Mudam Vidas_, que você pode usar quando quiser, como se fosse um Aspecto seu, incluindo para ser Forçado ou ter o Mesmo Arriscado. Além disso, ele tem uma invocação gratuita, além dos benefícios normais, você pode usar o dado de Vantagem como se você estivesse com ele... Na realidade você não a tem, mas é como se a tivesse

> Sergio possui um _Evento que Mudou Sua Vida_ chamado ___O Incêndio da Escola St. Clerance___, quando ele ao tentar expulsar um demônio acabou provocando um incêndio (chamas vindas do Inferno, cacete!) que resultou na morte de uma garota. Ele ainda se culpa um pouco, quando ele está caçando um lazarento de um fodido de um _Faminto_ que além de tudo é um estuprador de crianças que por acaso trabalha para alguns vampiros da cidade, "amaciando" as crianças e quebrando sua resistência para que os mesmos possam satisfazer com elas sua sede de sangue, as "pegando de volta" quando não servem mais para saciar sua própria fome. Ele descobre tudo e olha, com sangue nos olhos para ele e fala: _"Já mandei gente para o inferno por bem menos. Mandar um corno que nem você vai ser um prazer. A grana do \#iHunt é só um bônus."_ ele diz, lembrando daquela criança cuja vida ele meio que vive por tabela

No \#iHunt é sugerido que você mantenha um álbum de fotos com coisas relacionadas aos _callbacks_, literalmente _selfies_ representando tais Marcos. Até mesmo você pode ir mais longe e estabelecer uma conta em rede social sobre seu personagem, ligada a de outros personagens. O céu é o limite.

## Conclusão

\#iHunt é sobre gente fodida com opções de menos e com problemas demais. Pode parecer depressivo, mas pode ser bem mais interessante do que aparenta. É um material extremamente bom, uma das surpresas do final de 2019.

\#iHunt tem um texto caustico e muito agridoce em muitos momentos. Ele serve para lembrar algumas coisas amargas das maravilhas que a tecnologia e o capitalismo tardio "trouxeram", sobre como nossos cafés de grãos cagados por passarinhos (juro, é a última vez que vão ouvir essa) acabam resultando em toda uma economia que basicamente é um grande moedor de carne humana, onde certas carnes são mais baratas que outras. 

Claro, você pode usar \#iHunt para fazer seu tipo jogo de "vamos matar o monstro da semana", mas isso faz com que muito do tempero que torna \#iHunt realmente divertido se perca. Existe muito pouco em regras que te obrigue a reforçar essa situação de ser um fodido, mas por outro lado o jogo perde um pouco do seu tom e fica meio comum, além de ingênuo.

A ideia de que os monstros não são o maior problema não é exatamente nova, sendo um tropo da Fantasia Urbana. Mas aqui a coisa fica interessante por você não conseguir dizer que exista alguém bom ou mal. Às vezes, você vai precisar matar aquela fada que protegia o parque onde uns sem-teto moravam em caixotes. Foda-se que as construtoras vão destruir tudo depois para construir mais um prédio para Faria Limers. Você precisa comprar o remédio para fibromialgia agora, não daqui a seis meses.

Não é um jogo para fracos de coração: você é um fodido e sabe disso. Você tem escolhas pesadas a fazer. Às vezes o que você precisa para espantar alguns diabretes faz várias vezes mais dinheiro no eBay. Algumas vezes, eliminar o monstro só quer dizer que outro ainda pior entra no lugar. Às vezes, nem quem ganhar ou perder vai ganhar ou perder, vai todo mundo perder.

O texto de \#iHunt, como dito é caustico e agridoce, mas ao mesmo tempo cheio de um humor memético típico dos _millenials_, com seus rolamentos resultando em valores de 10-30 pontos de tensão (procurem pela meme 10-30), com uma fluidez sólida e em vários momentos angustiante. O capítulo \#THINKINGPOOR é um dos maiores tapas na cara que alguém pode receber ao ler um jogo de RPG. 

A diagramação é linda: definitivamente é um dos livros mais belos para Fate que já vi, senão o mais belo. Ao invés de muitas artes, os autores utilizaram imagens de arquivo de imagens em Creative Commons ou com custo baixo e _royalty-free_ para gerar uma diagramação bacana e enfatizada na ideia que que você é alguém que está vivendo em um tempo em que, ao mesmo tempo que se tem tudo, não se tem nada.

Esse é um livro que não consigo pensar em uma falha. É um jogo redondo, bem planejado, onde mesmo opções que não me seriam queridas (como o uso de 1d6 para a Vantagem) se mostram efetivas em game design. Talvez o ponto mais fraco seja o fato de que Monstros não são facilmente criados: não é difícil, mas precisaria daquele momento do comercial onde a galera come uma pizza e dá uma despressurizada de uma cena mais tensa (que pode potencialmente ter gatilhado um jogador) para você preparar o bicho. Os monstros de exemplo, porém, podem ser facilmente adaptados para o que você precisa. 

Até a descrição do aplicativo do \#iHunt e das falhas que o algoritmo do mesmo pode provocar e toda a questão envolvendo você receber (ou não) pela caçada é envolvente. Ele tem detalhes o bastante para deixar o cenário crível e realista, e buracos o bastante para se explorar todo o tipo de temática.

Eu dou um 5/5 sem muito medo para \#iHunt, por sua capacidade de ser um produto bem acabado e bonito, cuja talvez sua única desvantagem seja estar em inglês.

Para quem quiser matar uns monstros e ficar um pouco menos fodido com um dinheiro extra...

É só deslizar para a direita em \#iHunt!

<!--  LocalWords:  tags Reviews RPG book info authors Hill Filamena
 -->
<!--  LocalWords:  Young Francita publisher Productions year iHunt né
 -->
<!--  LocalWords:  dotcom fanfic app tretar Hunting Monsters on the
 -->
<!--  LocalWords:  Gig Economy itch io millenials iHunting Limers Job
 -->
<!--  LocalWords:  iHunter McEmpregos McEmprego iHunters Vision Board
 -->
<!--  LocalWords:  Daily Youtuber sqn Disney VASP Kink Buffy trash PD
 -->
<!--  LocalWords:  Winchester Evileenas Giles John Constantine phooey
 -->
<!--  LocalWords:  Phooeys phooeys hiperconectada hackers mesh deep
 -->
<!--  LocalWords:  cyberativistas Mastodon Scuttlebutt Malleus Minds
 -->
<!--  LocalWords:  Malleficarum Caça-Fantasmas Penelope Ess El-El PDs
 -->
<!--  LocalWords:  Shadowrun Evileena Edge dF links Star rating Kyle
 -->
<!--  LocalWords:  overwhelming resetado FDP Backpfeifengesicht San
 -->
<!--  LocalWords:  Google Gifts Features System Blade Castiel twist
 -->
<!--  LocalWords:  subclades Bram Stoker PDFs backgrounds Reptóides
 -->
<!--  LocalWords:  Necronomicon Illuminati Ok Reptoides reptoides All
 -->
<!--  LocalWords:  redneck Trump Alien Duqueza Genaro Walking Dead If
 -->
<!--  LocalWords:  Resident Evil iZombie desmortos gatilhantes iHunts
 -->
<!--  LocalWords:  Cops Bastards CEO X-Card Consent Gaming Cook refri
 -->
<!--  LocalWords:  FASCISTS you’re fascist not welcome play this It’s
 -->
<!--  LocalWords:  against reading and thinking You just call you or
 -->
<!--  LocalWords:  everyone disagree with then probably incapable of
 -->
<!--  LocalWords:  drawing inferences from context acknowledging that
 -->
<!--  LocalWords:  dangerous political climate oppressed be Don’t Mr
 -->
<!--  LocalWords:  hyperbolic Heal yourself Grow Learn Watch Rogers
 -->
<!--  LocalWords:  Neighborhood something sendoumfodido externalizou
 -->
<!--  LocalWords:  THINKPOOR tablet spoiler LIFEHACKS eBay cracudo an
 -->
<!--  LocalWords:  cracolândia first WhatsApp Uber Elon Musk Tinder
 -->
<!--  LocalWords:  selfie mal-avaliados there's Sílicio Beauchamps St
 -->
<!--  LocalWords:  True Blood California hamburgeres cajun vegana GTA
 -->
<!--  LocalWords:  gigs gentrificação imperish NPCs PCs Selfies Daft
 -->
<!--  LocalWords:  pseudo-Aspectos callbacks callback Stella Punk br
 -->
<!--  LocalWords:  Sergio Clerance sem-teto memético THINKINGPOOR Hat
 -->
<!--  LocalWords:  Creative Commons royalty-free gatilhado layout img
 -->
<!--  LocalWords:  reviews title Accessibility ToolkitResenha fudido
 -->
<!--  LocalWords:  language subheadline categories cover name link at
 -->
<!--  LocalWords:  Prodcution isbn pages formats buy Amazon Dungeon
 -->
<!--  LocalWords:  DriveThruRPG Geek Knights EULA boss apps Mikaela
 -->
<!--  LocalWords:  Bibbity-Bobbity barrio selfies
 -->
