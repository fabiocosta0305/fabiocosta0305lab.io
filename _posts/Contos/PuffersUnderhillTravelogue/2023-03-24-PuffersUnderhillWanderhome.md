---
subheadline: "A skunk Exile for Wanderhome"
title: "Puffers Underhill (he/him)"
layout: personagens
language: en
categories:
  - npcs
tags:
  - Wanderhome
header: no
---

+ Pragmatic and Sharp, not Explosive or Careless

### Look:

+ Cloak Big Enough To Hide In
+ A Sacred Text From Your People
+ Ornate Ceremonial Scepter

### Place that gone now:

+ True Reason: You fled to escape familial obligations. It is a palace or a labyrinth.
+ What I say: You fled because you were declared an omen of death. It is a farm or a wilderness.
+ Worry it's truth: You fled out of shame, to help the land heal. It is a garden or a glen.

Puffers ran from the family obligations, as he was chosen to succeed his father as his region's Scholar and Seneschal. He normally says that all the small black spots on his tail white stripes are taken as bad omen, and when it appeared since he was a small kit, people took him as a portent of bad luck, even of destruction of his community. He ran away after so much humiliation not only for him, but for his family.

### Play:

+ Still: An ancient violin that you once spent hours practicing as a kid while waiting for your mother to come back.
+ No long understands: The lullaby your father would sing to you as you fell asleep

### Things I always do:
+ Say an expression in your traditional language.
+ Keep an eye on the exits.
+ Push something out of sight or out of mind.
+ Play a tune that reminds you of home.
+ Say: “You look familiar.”
+ Ask: “Can I tell you a story about my home?” They get a token if they say yes.

## Left and right:

+ Do you miss our home as much as I do?

Miss my home, my mothers almond porridge and my father playing the violin with the traditional songs while me and my other brothers and sisters dancing aroung

+ How did you help me when I felt like no one else would?

I felt you were accused without proofs, because _reasons_, and this hitted my like a ton of bricks when I remember my own solitude.

### Traits:

+ Wise, Pensive, Chill 
  + Propose a path quite unlike those that others have suggested.
  + Ask: “What else can we do?”
  + Remind everyone to take a step back.

