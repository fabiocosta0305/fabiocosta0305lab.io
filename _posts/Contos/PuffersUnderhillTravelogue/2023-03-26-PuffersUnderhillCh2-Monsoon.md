---
layout: contos
title: "Puffers Underhill - Chapter 2 - Monsoon/Ablution: Univercittà Verde"
subtitle: A Wanderhome Story
category: stories
excerpt_separator: <!-- excerpt -->
---

> + Monsoon
>   + Torrential Rain
>   + Brief Moments Of Clear Skies
>   + Beautifully Green Plant-life
>   + Distinctive Rain Jackets
>   + Lots Of Tea
>   + The Clean Smell Of Soil After The Rain
>   + Jumping In Giant Puddles
>   + Enveloping Mists
> + Ablution
>   + The Ritual Retelling Of An Old Story
>   + A Whole Day Of Cleaning And Reorganizing
>   + Songs Of Freedom And Hope
>   + Special Roles For The Youngest Children
>   + A Scavenger Hunt
> + University
>   + Quiet, Stern Halls
>   + Sounds of Feruling
>   + The _Superior Maestro_ rules (Folklore)
> + Carnival
>   + Strange Actors
>   + Masquerade Accoutrements
>   + The _Marcia Dei Mascherati_ (Folklore)
> + Wilderness
>   + Trees Older Than Any Empire
>   + Something Bigger Than You’ve Ever Imagined
>   + The Old Tea-House out of nowhere (Folklore)

<!-- excerpt -->

"Varst!" I say, after puking again

_"It was a bad idea bring this guy, Biggia! He's weak like a twig!"_ said a big raccoon seaman

_"He was clever enough to take us out the way that really bad storm yesterday, Tanis. I can deal with a guy that do this and still puke out because not accomplished with the river!"_ replied the rabbit, on sailor clothes.

_"This is my ship, you should had asked me before bring him!"_ said Tanis

_"I asked, and you agreed, and he helped even after Livorno recovered himself!"_ replied Biggia

> + ___Biggia Saltare___
>    + Rabbit Sailor (He/him)
>    + _La Dulce Conigglio_ sailor
>    + Positive, but sometimes inquisitive, do what he needs to do
>    + _Crafty, Adventurous_
>         + Reveal that a plan they’ve had in motion has paid off.
>         + Have circumstances improbably work out for you.

> + ___Tanis dell'Aqua___
>    + Raccoon Sailor (He/him)
>    + _La Dulce Conigglio_ captain
>    + Demands respect, but it's open for ideas. Non-nonsense
>    + _Cunning, Proper, Sturdy_
>         + Get somewhere you’re not supposed to be.
>         + Judge something for its inappropriateness.
>         + Support something in danger of collapse.

_"You're right, you're right!"_ conceded Tanis _"I just don't like to have newcomers here, even more green on ships like this skunk. Lucky looks he has no stink for a while."_

_"Sorry being inconvenient."_ I whisper _"I'll get inside and look the maps if there's any nice route."_

_"Don't worry,_ gamberetto, _we are almost at Velina, the first port at Talmina. Nice place, a bit small, but have some good places nearby, like the_ Univercittà _and the Old Fair. Think I'll give you all a day or two."_ he says

_"I think I'll just go there, thanks."_ I say _"Think had enough for a while on rivers..."_ 

_"We'll miss you, chap... But anytime you see_ La Dolce Conigglio, _you'll be welcomed."_ he says

_"Thanks..."_ I say, while going to my bunk bed...

There was three days since we parted from South Glens to the country of Talmina. I thought it would be nice to get even more south to there, and the ride as a spare navigator for _La Dolce Conigglio_ sounded a good idea.

What I had in mind to get to a boat on a river in Monsoon? 

We had, at most, some few hours of calm river while getting down, and almost every day I was puking out everything I've ate! Even being able to do my work as navigator, I feel really sick since I've got out in South Glens port.

At least it is ending. Although I'll miss those guys, non-nonsense and in-your-face, but happy and emotional.

When I got to my bunk bed, Biggia came and offered me again his seasickness remedy. It tastes horrible, but need to say that does miracles. Maybe I should ask him the recipe before parting ways, who knows when I'll need it again.

_"Just stay laid down... It's better for a while as you're really sick. Drink a little water: it will ease the sickness too and remove the vomit taste."_ he says, offering a earthenware cup with water he took from the barrel nearby.

_"Thanks."_ I say, drinking, when noticing the river got calmer _"Need to say, I don't think I'll feel any sorry leaving the river behind. My time at Furrow Heighs didn't prepared me for this."_

_"No one would be prepared: I'm a sailor since I was a small kit, my father put me on it to go and fish, and also I've learned about the seamanship with him. Now I just do this small route with_ La Dulce Conigglio _but at past I got around the oceans sometimes. And even me got on some really nasty times, like the time I've almost drown myself on the ocean nearby Sirenna"_ he says

_"Wow... Now I can't blame myself on what happened with me here..."_ I say

_"Now, go sleep a little. I'll see for a light soup for you later. Tomorrow, we'll arrive at Velina."_ he says, leaving me to sleep, thinking I'll miss them...

---

It was five days since I've arrived at Velina...

Velina isn't exactly a city, more like a Duchy composed of lots of small villages spreads around the _Foresta Verde_, the green Forest, a big forest that cover some days of walk. It's a dense one, but people build some roads of stone. Monsoon's rains had did some mess on it however, and the rains doesn't help to find some shelter and so my way isn't that fast...

Although, when the rains stops and the skies opens and the sun shines, like now, it's pretty, really pretty, as small rainbows shows all around the road, and the mist bring a nice, nostalgic feeling all around, like I was walking into a fairytale, a fable that old nannies will talk for kitten in the future. (+1 Token: 1)

I find a crossroad and a stone cottage on it. _La casa da tè di Martina_ is written on a wooden table over the entry. I just get in: 

_"Hello..."_ I say, when a small hamster lady, matronly looking on a big dress full of frills.

"Ciao, _my dear skunk."_ welcomed the lady _"I'm Martina Pergola, owner of this teahouse. Looks like you're a tired, and soaked, wanderer... Please, came and take a hot bath and put some clean clothes before taking some tea. And don't worry with money: I like stories, and this can be a nice way to pay me."_ she says, pulling me to the upper floor. She takes some clothes and a small bag for my old ones, while I clean myself into a hot bathtub, with some rose smell on the water and a good smelling soap bar. I feel so relaxed, after hours below the rain without a raincoat, smiling nicely

> + Martina Pergola (she/her)
>    + Hamster Tea-shop owner
>    + Gossip like a hen, but knows how to held secrets
>    + _Witchy, Venerable, Wise, Watchful, Intertwined_
>         + Mix assorted components to create something new.
>         + Offer something that hasn’t been seen in a very long time.
>         + Reflect on what someone else has said.
>         + Point out something people missed.
>         + Show how two things are connected in an unexpected way.

_"Looks like you had a harsh time... Monsoon is a very difficult time, and there's few times were people should walk like you, without a raincoat. I'll leave a spare one with your clothes when your clothes dry off... Now, get out the bathtub and dry yourself... I'll wait you below, for some tea."_ she says, and need to say that I feel how long I've had a bath like this. Even on South Glens, I didn't bathed like this... So sweet and nice, like I could lose hours and hours here... The smell of roses, the nice warm water caressing my uncared fur and tried legs and tail, the time I can just be... There... Just calm, emptying my mind and relieve from what I've thinking before, and appreciate time and the happiness of being alive and there. Even the small rain sound I could hear and leave me feeling like I'm just glad by living.(+1 Token: 2)

However, I need to get away: I don't want to abuse this lady hospitality, that is being so kind. Now, it's time to repay this hospitality by being a nice guest and talk good histories.

---

The mint tea is delicious, as it was the small bumble sausage snacks:

_"So, you came here getting down the Green River after a time at South Glens... And coming from that far away north like Furrow Heighs? Quite an adventure, my dear."_ she says

_"Yes, but it wasn't like I wanted... But now I'm learning a lot outside my home..."_ I say

_"Maybe, my dear... You are not exactly being truthful on this. Maybe it's not the fact of being a Death Omen, or about your place... But maybe, you still think you aren't good enough to become a Seneschal and Scholar."_ she says, lightly

This never occurred to me... or better... Occurred, but I tried not think on this. And she said this on a so straight way, but at the same time on such a sweet way... I could not left this without an answer, but at the same time I couldn't be aggressive, even she going on a touchy topic.

_"Maybe you're right, lady: my parents were suffering a lot because all the Death Omen stuff. But I'm... Well... I feared that I couldn't be good enough to follow my family traditions and be a shame for them. At the same time... I feel... I don't know how to say this..."_

_"A wanderlust, to know the world?"_ she smiled, drinking her tea _"For me, you're just a child, and I've saw this history repeating itself a lot: someone trying to prove himself and prove he can live upon their family, just got away to learn for the world. But, to be honest, it was a lot of time since one like you came to my humble shop. Maybe the gods sent the Monsoon rains this year so you need to take a time here, for you and for me. Now... Before you ask: I don't know how your history will finish, as few came back once here, and fewer let me know what they would be. Some just stayed wandering around the world as far I know. Others got back to their family, humbled enough to understand what was expected from them. Some even joined either_ La Univercittà Verde _nearby. There was even this mouse kid that left everything behind and got to the_ Carnevalle _down the road after the_ Univercittà. _But there's something I know: those are THEIR stories, don't take them as yours. Your story is YOURS truly, no matter how much you believe you can learn from others. That you can, in fact, but you'll learn, even more if you understand there always things that will need to learn by yourself, and many time those are the most important things."_ she says, finishing her tea, while I took the pot _"Oh, sorry, I'm such a chatterbox sometimes"_

_"Don't worry."_ I say, while serving her some more tea as a token of respect, as soon she nods accordingly for more _"And maybe you're right... I... In fact... I ran from my obligations as a Seneschal._ Varst! Zletvair Varst!" I say before I can contain myself.

_"Language, mister! I still have lice soap to soap your mouth if you still say those bad words!"_ she says

_"Ops... Sorry... Wait a minute, how do you now Old Furrowian!"_ I say, curious

_"I'm an old lady, and I serve this tea shop since I was a kit, and in past this road was always full of people, even on Monsoon or Frostbite, and so since I was a small kit I learned a lot, from common, Old Furrowian, Karash, and other languages all around the world. Aside,_ Univercittà _is a center of knowledge since centuries before, and so there's people there that speaks old Furrowian, and other languages, and they also practice with each other and some travelers, and this happens lots of time here on this tea-shop."_ she says, taking a sip of tea _"But now... Continue from where you were before going pottymouth."_

_"I feared that I was not good enough as a Seneschal, even being in the Top Tier of my Classes, my preceptors all liking on how much I worked hard... But... But... It was all about those spots on my tail: they are being a bane since I was born and my tail started to sprout. All this made people pushed me and bullied me... And I..."_ I said

_"And you thought it would be easy to just run away."_ she says _"I'll not be the one that can say what is up for you. But..."_ she takes a sip _"Everything in Haeth has a place. There's order, even for Chaos, and Chaos also is part of the order, but even Chaos has a function at Order. You'll find it: maybe tomorrow, maybe in some weeks, maybe when you die... But certainly you will. By now... Don't worry about you past. Enjoy your travel. Like we are doing with this tea!"_

I smile... When it was the last time I had this kind of talk while taking tea?

_"Thank you, Mrs Pergola..."_ I say

_"Call me Martina."_ she says _"Now... Looks the rain will stay all the day. And so, no customers. But you can pay for a night helping me readying everything for tomorrow: maybe tomorrow there'll be some customers. If you do your job right, I can provide you some food for the road. Think you are going for the_ Univercittà, _or at least you should. It would be good for you, to see more people like you. It's still two days, but you can always find some huts and shacks all around where you can sleep at night or hide from rain."_

_"I'll be... Now... Let me wash this ware for you."_ I say, getting the ware and bringing it to the sink

_"In meanwhile, I'll get some materials for a travel honey cakes and gingerbread for you and for the shop."_ she said, going for the cupboard

---

I play a little while back in the road. It was somewhat five hours after I got out Martina's Tea-Shop, and the rain got strong enough to form some puddles, that I playfully jumps over. Now, Mrs Pergola provided me some nice clothes, old but sturdy and well-conserved. Aside the new trousers and shirt (my old stuff, now dry, at my bag), I have a nice yellow raincoat, a red newsboy hat and a pair of yellow Wellies. I still walk, but I can see the rain is growing stronger.

_"Hope I'm nearby the Univercittà. The rain is getting stronger, and even being fun to enjoy the rain and water puddings, the rain can get dangerous."_ I think, while taking a small honey cake from the satchel and hastily eat. It was really sweet and gave me strength to go. I can see at the distance the towers for the _Univercittà_. I start to push myself forward, till I get to the big main doors. I knock the door.

_"Hello! I'm a traveler needing a refuge from the rain!"_ I say. Soon a Labrador dog open a window.

_"Just a minute!"_ says the dog, on a very formal voice. Soon, he opens the door.

_"Please, come in. I can't bring you inside the inner building, but at least I can provide you safe haven from the rain."_ he affirms _"Now, I'm Gennaro, and I'm a servant here. We are almost at the meal time. Please, come with me, and just put your coat and Wellington to dry."_

I follow the Labrador, and everything looks so monastic, even more than on my time at Furrow Heighs. I could hear the noise of chalk against boards, and even some feruling noises that made me shudder... I remember how many times I was feruling thanks some of the other apprentices that pushed me and made me got by the preceptors. Even being a good student, they needed to ensure the discipline at all, and they feruled me... 

> + ***Gennaro "Nasone" Tinacci (He/him)***
>    + Labrador Keeper of the _Univercittà_
>    + Loyal to the _Univercittà_, only wanted to prove he can be more than a keeper
>    + _Confident, Resolute, Inquisitive, Wise_
>        + Charge into a situation without understanding the risks.
>        + Keep at something that others would give up at.
>        + Hold something up to the light.
>        + Propose a path quite unlike those that others have suggested.

_"Looks like you are a scholar too... And a skunk one, that's not that common here."_ he says _"Came from somewhere else?"_

_"I was at South Glens till some days ago, but I got from Furrow Heighs"_ I say

"Istlevar! _Furrow Heighs? Are you a Seneschal from there?"_ he says, speaking on Old Furrowian. I didn't noticed at first and then just changed for it.

_"Yeah... I came from there... So you're a student here? Like Martina from the Tea-House said, looks like you also speaks Old Furrowian. Need to work on accent, but is good enough for being understood..."_ I say, using Old Furrowian again for a long time, aside my imprecations.

_"Oh, no! I'm not a student. My father was previous warren for_ Univercittà, _and I've inherited his station. It's a great honor!"_ replied Gennaro

_"Never thought on be a student? Just noticed how you spoke Old Furrowian. Very few can speak that so good outside the North."_ I wow

_"You flatten me: I got so many times to Martina Tea-House, as I need to do some errands for the_ Maestros, _while they uphold their studies and researches. And so, I've found some people from North, and practiced my Old Furrowian that I've learned with the_ Maestros _for doing my job."_ he says

_"To be honest, you speak like a Furrowian Scholar! I know this, as I'm a former Scholar student!"_ I say, when I hear someone shouting

"Nasone, _you fool! You are talking for too long with this man!"_ says a Mole guy, haughtily

_"Sorry_, Superior Maestro _sir. But he is a wanderer from North."_ he says, back to common

_"I'm Puffers Underhill, from Furrow Heighs. I'm a wanderer former Scholar and Seneschal in training."_ I reply, on my Old Furrowian, curving in respect _"He was really kind to provide a wanderer like me with a shelter with this time"_

_"I see."_ he says, but I can see how atrocious is his Old Furrowian accent, even more with all the embedded arrogance, when measured me like an old smelly bundle of clothes _"I'm Leone Talpatone. Sorry about this fool: I just uphold him as our warren by the tradition of the post his family that always served_ Univercittà _since his building."_

> + ***Leone Talpatone,* Univercittà'*s* Superior Maestro *(High Master) (he/him)***
>     + Mole Scientist and Scholar 
>     + Haughty and traditionalist
>     + _Intertwined, Wise, Pensive, Royal_
>        + Take your time and move very carefully.
>        + Reflect on what someone else has said.
>        + Rain on someone’s parade.
>        + Inflict your will on the world around you.

_"Don't be. In fact, he was quite a nice company for the moments: you have a very smart and loyal associate on him."_ I state

_"For loyal, you're right. About being smart... I don't think so. Serfs like him tend to be so obnoxious and single-minded that should be on their own station. Try to stimulate them to go for higher stations will only bring them and everyone's around pain."_ he says, on a condescending voice that makes me grew angry... I needed to use all my Seneschal training to not say some words about his arrogance, starting by his lousy Old Furrowian

_"Well, mister. Believe me, you have a very good servant on this warren. Perhaps... better than you deserves. Oh, sorry about this."_ I say on a pinch, and I can see him being hit by my words (+1 Token: 3)

_"You flatten us..."_ he states, and I know he was not happy with my comments before, and so would not provide me full hospitality. _" Now... It's meal time. Unfortunately I can't receive you on our Meal Room, but you are welcomed to eat on the kitchens."_

_"I'm more than glad,_ Superior Maestro". I reply, nodding for him

---

_"You was a little picky with_ Maestro, _mister Underhill!"_ said Gennaro

_"Sorry, but... I just remembered what pushed me out home."_ I reply (-1 Token: 2) _"I can't stand for injustice. Reverence is good and important, but sometimes too much permanence on the old ways makes even the best fruit rot! I was saw as a Death Omen, unfit for my then-future role as a Scholar and a Seneschal, just because the spots on my tail! I couldn't stand this, I just couldn't..."_

_"I know, mister... And I'm really glad you had put yourself under my favor..."_ (-1 Token: 1) _"But... Think on it: you are a scholar, even a former one. Maybe, by defending myself, you lost access to all knowledge from the_ Univercittà Verde. _Hundreds and Hundreds of years of knowledge, accumulated and stored and analyzed, from even before the Old Times. We are one of the most important places of knowledge. And..."_

_"And I would not stand this..."_ I state, while eating some soup and bread, like the other servants _"Gennaro, you're smart, you're cunning. I'm not suggesting you to leave your station as warden if this is what make you happy. But... You should not just accept what they say about you, treating you like the fool you're not. I felt his arrogance, like he spoke a better Old Furrowian then myself, that was born at Furrow Heighs."_

_"I know."_ says Gennaro _"But, he's_ Superior Maestro, _chosen to the place by the other_ Maestri _on the Conclave after the decease of the previous one."_

_"Being chosen by their own doesn't mean he's the best, the most smart or even the correct one. Had saw this before."_ I say, when Gennaro gasps _"Sorry... Think it's better for me to go. If I stay, I can put yourself and everyone under toil."_

_"No."_ says Gennaro _"To be honest, you are the first one to talk with me with kind words, aside the other servants. Even the students here are..."_ he stops, worried

_"Unkind? Unruly? Bad behavioured with you? Treating you like filth?"_ I say

_"Don't listen him, Gennaro! He will put bad thoughts on you!"_ says a mouse lady 

_"Sorry lady, but treat people with kindness is not a bad thought, quite the contrary. Real sages treat people kindly no matter what. It's civil, it's right, and it's better for everyone."_ I say

_"And..."_ she tries to reply, but after I finished my soup I broung my bowl to the sink and start to clean _"Please, no, mister! This is our job!"_

_"I know... And you already have enough with the ware from the_ Univercittà. _It's not fair to demand you to clean mine too. And would be unkind too."_ I state, smiling

She looks to me, meekly smiling _"You are a nice_ Maestro _stuff. Maybe you should stay."_

_"Nope... I would be seen as a ragamuffin or an arrivist by the_ Superior Maestro." I say, smiling weakly. _"Better to go now. Hope we find ourselves again. on a better time."_ I finish

---

The mouse lady was really kind to offer me some small travel cakes she got from the _Univercittà_ dispenses. My next stop would be at _Carnevalle_, the old Carnival nearby. I could hear the laugh and the raucous from half way the road till there. The rains were a little less intense, and there was more places for a wanderer to hide himself on this part of Velina.

_"Going for the_ Carnevalle?" says a sly looking fox, looking for the place were I made my pause _"You should take care._ Carnevalle _is a place with lots of travelers, that came from all around Velina. And sometimes there's some highwaymen there."_

_"Like you?"_ I say, already taking the fox as a thief

He smiles.

_"Not the first to take me as a thief as I'm a fox. I'm Lucca Volpinare,_ Carnevalle'_s_ Brighella!" he says, happily.

> + ***Lucca Volpinare*, Carnevalle'*s* Brighella *(He/Him)***
>     + Fox _Commedia Del'la Arte_ actor
>     + Funny, somewhat sly, and an anarchist to the core, has a passion on humiliate powerful people
>     + _Passionate, Confident, Oracular_
>         + Say exactly what’s on your mind right now.
>         + Jump headfirst into action.
>         +  Tell someone the bad news about what their future holds. If they want to defy you, they’ll need to spend a token.

_"Oh!"_ I say, abashed on my own prejudice, after all I said on _Univercittá_, when the rain started to intensifies _"Please, come here! No need for being there, wet under the rain, there's space enough for both of us!"_ I say, asking him to shelter on the shrine for the gods I'm on. It's small, but would allow us both to stay comfortable, and looks like there wasn't other travelers. I already left a small tithe for the small god of this shrine, asking him for protection against the rain

_"Thanks... Had to go for_ Martina's _for some new stuff for_ Carnevalle, _including our new_ Commedia _stuff. Hey, you look like a Scholar. Why are you on the road with this rain? You could had got some shelter on_ Univercittà" said Lucca, while putting in the ground a big bag he had at his back

_"Chose to not abuse them..."_ I say, and then talked about what happened involving Gennaro and the _Superior Maestro_

"Cáspita! _That new_ Superior Maestro _is the worst: since he came to the job, replacing the old one that died five years ago, life nearby is being hell!"_ says Lucca _"He stated that we could not do our parades nearby, and couldn't go on costume through the road! We couldn't even got to the port and announce our shows and_ Carnevalle! _Our resources is growing short thanks this, and maybe we'll need to close the_ Carnevalle, _we are running out of funds! I think he wants to suffocate us, feeling on us a menace to his own fief."_

_"Why?"_ I asks

_"In past some of the Scholars from_ Univercittà _were more open to us, even allowing us to present ourself on_ Ablution. _We were mischievous, for sure, but_ Ablution _is all about this in the end of the day, and we restrict our interaction with the_ Univercittà _only in the festival times. But some of the_ Maestri _thought we were an inconvenience, even more when some of the_ alunni _defected them for the_ Carnevalle, _feeling their life call with us!"_ says Lucca

It's like something was clicking on me

_"And he thought this would break down their Ivory's Tower."_ I start to see

_"Yeah! But we are, in fact, not with this: we at_ Carnevalle _are only worried on all the merriment we can provide for people, we don't care if people also go study on the_ Univercittà, _quite the contrary. Sometimes people needs to let their hair down no matter how much you can focus on studies, you know. All that reverence can be sometimes oppressive..."_ says Lucca, when something clicked on me

_"Say me, Lucca... How do you do Ablution here? We had Ablution on Furrows Heighs, but I know some places has changes on the traditions."_ (+1 Token: 2, asking about Ablution)

_"We had lots and lots of our traditions, specially about masking yourself and free yourself to experience new things, like_ Danza mascherata, _the dance of the masked, and, the most special one:_ La Marcia dei Mascherati, _the Parade of the Masked. On this special parade, we do some raucous sounds and walks around the road, from dawn to dusk. We dress totally outrageous costumes, hiding our face behind masks, and sometimes we do some mischievous things, like throwing rotten fruits and so, using this to greet people and the small gods with new, fresh ideas, throwing the rotten stuff away. This is the idea on Ablution, after all: to show what needs to be thrown away and what need to be uphold. To renew things!"_ says Lucca

I smile...

_"I think I can help you... I heard a little this and that about_ Commedia _before, when training as Seneschal at Furrows Heighs, and now I can see why the_ Superior Maestro _is so dull!"_ I state _"Do you have any spot open for_ Commedia?"

_"Well... We are without an_ Arlecchino... _But..."_ says Lucca

_"Don't worry, I think we'll solve everything soon!"_ I say

---

Some days passed, and all around Velina they heard _La Marcia Dei Mascherati_ would parade around again, from _Carnevalle_ through the way to the port for the first time in five years. Liked the _Univercittà_ or not! And would make a stop at _Univercittà_!

I readied myself: Lucca, and his Director, _Monsignor_ Paollo Dalla Piva, a nice and big _Canne Corso_, helped me on getting on my Character, _Arlecchino_, the somewhat dumb but witfull, mischievous althought lighthearted, servant, nicer than his mean and sometimes even brutish elder brother _Brighella_, Lucca's character. We played and I studied all I could to be the best _Arlecchino_ in so few time, readyng myself for _La Marcia_. I was ready to do what I need to do, when I put down my _Arlecchino_ mask.

> + **Monsignor *Paollo Dalla Piva,* Carnevalle'*s* Commedia *Director* (he/him)**
>     + Former _Alunno_ from _Univercittà_, now *Commedia* director
>     + Has a grudge with _Superior Maestro_, that branded him _Persona Non Grata_
>     + *Resolute, Passionate, Hurt, Imaginative, Honest*
>         + Keep at something that others would give up at.
>         + Say exactly what’s on your mind right now.
>         + Articulate a step on the path towards healing.
>         + Explain a way things could be better.
>         + Point out the truth everyone else has been ignoring.
>         + Lay out the facts, as you see it.

We started as soon the Sun risen: the rain had stopped and the smell of the petrichor started to grew around. I smiled and smelled the rain, getting inspiration. Looks like all the old gods wanted the Ablution back like in the old days!

_"Now!"_ commanded Paollo, or better, _Il Dottore_, the old-fashioned and sometimes decadent doctor. This was chosen on purpose: looks like him and the _Superior Maestro_ has some old grudge, and perhaps it's time to solve this!

We started to march, doing raucous sounds, tambourines and flutes doing very noisy music, calling everyone to the Parade

As we get nearby the Univercittà, we start to go even louder on music, singing loudly and purposefully bad! The tambourines gets somewhat out of time, making a cacophony!

_"STOP, you rascals!"_ screamed someone on the biggest tower, the _campalina_ at _Univercittà_

It was the _Superior Maestro_

_"Are you mad? This is a place of knowledge, not a ragamuffin's barn! You are disrupting our studies!"_ he screams and scoffs

I look to him under my mask and says, like _Arlecchino_!

_"Yuck!"_ I say, faking feeling a bad smell and then doing a mocking curtsy _"Oh, greatest_ Maestro, _as big on knowledge as on the stink that got from your mouth! You say balderdash! Your knowledge is useless, being put inside those towers and walls, to rot like old bananas!"_ I taunt

_"You!"_ he shouts, seeing my tail when I did my curtsy _"Are you envious of our knowledge!"_

_"No, you big fool! This fool here is only being impertinent like those small ranked like him!"_ says _Il Dottore_, mocking a kick on my butt, that I mockingly feel but doing some acrobatics that makes everyone laugh. This pushed curiosity all around _Univercittà_, _Maestri_ and _Alunni_ putting their heads out the windows curiosly with the rauscous Bedlam hapenning below

_"You, otherwise..."_ he continues, looking for the _Superior Maestro  "Your time is being hell for everyone here in Talmina! Your knowledge took the happiness from the small people around. You didn't put this on use for Talmina's people, quite the contrary. You reinforce your structures and resources, but also pushes out those that had more wisdom than you no matter the topic, what looks otherwise only because you speak flaunty..."_

_"Like a big flatulence!"_ I say, in Old Furrowian, and Lucca does very loud flatulence sounds and then in common I finish _"Like the big flatulence vapors into your mind!"_ This makes lots of the _Alunni_ to laugh heartily

_"YOU! OUT! I'll ask the constables for you!"_ menaced the _Superior Maestro_

_"Not today, old fart! You did enough ill to Talmina!"_ replied Lucca, or better, _Brighella_ _"It's Ablution today! We conclaim the Sacred Rights of this day. The right to mock the powerful. The right to spat on their face. The right to sneer from his balderdash. The right for new ways and new chances. It's time to threw away the old, and renew everything! Your time as_ Superior Maestro _is over!"_ he took his _Batocio_ like a sword.

_"Sons of Talmina, I speak to thee! It's time to fill yourself with joy, and hate those that oppresses you! It's time to renew! No fear, no regrets! As we did in the past, from the time of the_ Signori _to now! It's time to get our wisdom back! The wisdom taken from us!"_ he says, pointing the _Batocio_ to the _Maestro_, and then...

...

...

The _Marcia_ invaded the _Univercittà_! They forced the doors, pushing like a sweaty wave, and got inside, even with help of some of the _alunni_, even Gennaro and some of the workers helping!

"Viva la Abluzionne!" screamed the crowd happily, even some of the _Maestri_ discreetly joined us!

_"No!"_ screamed the _Supremo Maestro_ as Lucca and me started to go up to the high tower, where he screamed orders, trying to bring order to the chaos that instated in the _Univercittà Verde_!

The order in _Univercittà_ got upside down, _alunni_ and workers throwing rotten tomatoes and bumble dung to the most devious, hateful, _Maestri_, and some of the _Maestri_ also joined by doing some mischief against the most stupid but mellifluous _alunni_, protected by the worst _Maestri_ and by the _Superior Maestro_ for political reasons!

We got to the _campanile_ top, where we saw the _Superior Maestro_ really angry with us: _Arlecchino_, _Brighella_ and _Il Dottore_.

_"Satisfied, you oaf? You just destroyed the_ Univercittà! _You brought it down to the mud."_ he says

_"No,_ Maestro. You _were the one that had_ this _honor."_ says Paollo, removing the mask of _Il Dottore_ _"Remember when you expelled me, still in the time of the previous_  Superior Maestro? _I wasn't throw out because I wasn't capable, as you made people think of, but because I challenge you as an_ Alunno, _because I never conceded you as a the only source of knowledge, and didn't saw being a_ Maestro _as social ladder, like_ Il Dottore _did in the_ Commedia! _I didn't accepted your teachings as written in the stone you always loved. And you raised against me, and honeyed the_ Superior Maestro _against me! But he didn't fell on you, and only the_ Collegiale dei Maestri _chose to expel me, branding me with hot iron as a_ Persona Non Grata!" he showed the branding on his arm, and I could see it: a Sigil of Shame! 

I couldn't believe. I then looked to my old Scholar Tome and remembered also all the punishments just about all the Death Omen stuff.

_"You're right_ Il Dottore, Signore! _Knowledge without wisdom is rot!"_ I say, when I look to my old Scholar Tome. 

I then opened it and started to rip my Tome apart, page by page, and throw them away in the air, falling to the ground! (-1 Token: 1)

_"NO!"_ screamed the _Superior Maestro_ _"You are getting down the populace's level, discarding that knowledge!"_

_"I don't need this anymore, not for a while  at least!_ I say _"I don't need this, from the people that branded me as a Death Omen!"_ I say _"Maybe I'll someday get back there, like people is getting back the_ Univercittà. _Until then, I don't need this tome! Those that wants that gather those pages!"_

_"Fool!"_ he ran to me, but then I do my trick: dodge him and place my tail over his face, rolling it and exploding my stink straight to his snout!

_"You stink more than my tail,_ Maestro." I mock him, when he fell coughing _"Now! You'll renew_ Univercittà, _as part of Ablution! You are the one with this power! You only need to abdicate from your position. Let the_ Alunni _and_ Maestri _to choose the new_ Superior Maestro. _They'll be able to choose someone wise enough to know that kindness is also part of wisdom and that people deserves the chance to try and learn, even not being_ Alunni."

_"I will not!"_ he says, screamed

_"Oh, yes, you will!"_ says Lucca, smiling gleefully _"Or you'll have some spanking from my_ Batocio!" he says, hitting him hard in the butt, making him yelp

_"I have an idea!"_ says Paollo, getting the Maestro and pushing him to the corner of the _campanile_ and exposing him to everyone, taking his trousers and underwear down, making him falling from the _campanile_ to the ground, calling everyone attention, and lowing his mask as _Il Dottore_, supporting the _Superior Maestro_ on the corner, butt naked

_"NO! NO!"_ he says, desperately and in shame and in fear on what will happen with him.

_"Once you said,_ Signore _Leone Talpatone, that shame and pain are the best teachers. You said this when a certain student was being branded with hot iron as_ Persona Non Grata... _Now, tame came to_ you _learn this lesson too!"_ he says, when Lucca started, as _Brighella_, to publicly spanking Leone's bottom with the _Batocio_, until he started to cry like a baby, making all the _Alunni_ feruled by him and his fellows, and all the _Maestri_ forced to ferule their _Alunni_, laugh to the heart on rightful revenge and mockery!

_"Please! Please! Please!"_ he begged, humiliated. _"I just..."_

_"You wanted to preserve_ Univercittà'_s Letter as a Place of Knowledge and Learning. But you almost made it lose it spirit, by freezing your ways to rot, to enclose your knowledge under an Ivory Tower."_ I say, while Lucca gleeful spanks his butt _"Now, it's time to end this! To renew_ Univercittà. _To allow knowledge being used and replicated by everyone. And_ you and only you _can do it."_

_"So... How it will be, old fart?"_ says Lucca, somewhat mischievous. _"Don't be so hasty to end the show. Everyone below is loving it, and I'm having the time of my life spanking and humiliating an old smelly fart like you! Take your time, enjoy your lesson, we can wait!"_

_"I will do it! I will do it!"_ he said, in despair and shame _"Please, just stop!"_. 

_"Alright Lucca!"_ I say _"I think he had enough."_ 

Lucca stopped, somewhat happy by the time he enjoyed, and Paolo put him over his feet again and straight him

_"You need to be presentable for what we are asking you to do."_ said _Il Dottore_, making the _Superior Maestro_ mad in rage and shame. 

_"Now... The words."_ I say, doing a mocking curtsy for him while he looked down.

_"I... I FORFEIT AS_ SUPERIOR MAESTRO!!!!" he screams, in shame and pain

Everyone cheers and plays music, although some mourn the loss of the now former _Superior Maestro_'s sect, by the power they'll soon lose. But those mourning were suffocated by the music that started to play in the _Univercittà_, a so crazy cacophony that, as by magic, became like a _camerata_ concert.

I... By myself, I look for the _Campanella_, the bell. And feeling the urge of the dumb and happy _Arlecchino_, now with right being done, and run to, on a silly way, jump and held on the big rope of it, making it bells as my weight makes the bell tolls, announcing all around the _Univercittà_ soon would have a newer, better _Superior Maestro_

---

The day next tomorrow, I'm on Martina's Tea-House, that now has lots of people talking about the news.

_"So, you toppled the old_ Maestro?" she wowed, serving tea to me and some friends in the table: Gennaro, Lucca and Paollo.

_"We just asked for the Ablution, for renewing everything. Just after the forfeiting, they elected a new_ Superior Maestro." I say

_"Yeah... Old_ Martinno Aguila. _He's a nice guy, and already allowed us workers to take some classes on our free time, and removed feruling as punishment to the_ Alunni. _I just want to practice my Old Furrowian. He'll ready_ Univercittà _to a new time, a better one!"_ says Gennaro

_"So, enjoy and just give yourself a chance, Gennaro. Maybe you can be a_ Maestro, _maybe not, but at least give a try. At least, this will make you a better warren."_ I reccomend

_"I know, mister. And I'm glad you did it."_ he says

_"And looks like how much people is there. Looks like the old_ Superior Maestro _liked the_ Univercittà'_s isolation..."_ says Martina

_"This is not wise..."_ I say _"I believe this is why I left my Seneschalhood, in the end... I believe this will soon make them fall."_ I say, really sad (-1 Token: 0 - Ablution: learning about the problems at home)

_"That's not your worry anymore..."_ says Lucca _"You also forfeited this position, right?"_

_"Maybe... but someday I'll need to get back home, to look the truth about me. Even not being too soon... Someday I'll need to get back there."_ I say

_"But by now, you're a wanderer."_ says Martina

_"Yeah. You're right... Maybe it's time to hit the road again!"_ I say

_"And we want you to have this."_ says Lucca, offering him the _Arlecchino_'s costume and mask, on a package. _"We just got a new_ Arlecchino _from one of the_ Alunni _that was most flogged by that old_ Superior Maestro, _and now he's joining us at the_ Carnevalle _as_ Arlecchino. _But this costume is now yours,_ Arlecchino" he finishes, cheeky.

_"I'm be glad to know."_ I say, thanking the package _"And I'm thankful for this gift. Maybe I'll have the chance to put this on good use... And... Need to say, I don't want to go back to port and goes via river."_ I say

_"Bright is starting! Now is First of Bloommeadow!"_ says Martina _"The river's flow is more suave now... Maybe you'll have a great time."_

_"Yeah!"_ says Lucca _"Came from the south there! Was a great ride!"_

_"Thanks... Sounds like a nice tip... Give another try."_ I say, while looking _"Well... Time to go."_

_"Thank you, Puffers Underhill!"_ says Paollo _"Believe me, you made the difference here."_

"Ciao!" I say for them, turning to the door and leaving it behind.

<!--  LocalWords:  Underhill Tillsoil Asfriteyhi Varst Ack Geroff pre
<!--  LocalWords:  spoilt Seneschal's farmworker Duchacy Heighs Heyia
<!--  LocalWords:  Hardworker Maths teached Nearsight Yay Cep mr Gack
<!--  LocalWords:  Conclaim conclaim conclaimed C'mon sensorial 'The
<!--  LocalWords:  Biggia Saltare Talmina Conigglio Whatcha Talvish
<!--  LocalWords:  Wind' berrywine Univercittà Wanderhome Feruling da
<!--  LocalWords:  dei Mascherati Tanis Dulce dell'Aqua gamberetto tè
<!--  LocalWords:  Velina Sirenna Foresta uncared Carnevalle Zletvair
<!--  LocalWords:  Furrowian Karash pottymouth Haeth there'll Gennaro
<!--  LocalWords:  feruling Nasone Tinacci Istlevar Talpatone Maestri
<!--  LocalWords:  Univercittà' behavioured arrivist Volpinare Del'la
<!--  LocalWords:  Brighella Commedia Arte Cáspita alunni Danza Dalla
<!--  LocalWords:  mascherata Arlecchino Paollo Piva Canne Corso Il
<!--  LocalWords:  witfull Lucca's Alunno Grata petrichor Dottore 's
<!--  LocalWords:  campalina Talmina's Batocio Abluzionne Collegiale
<!--  LocalWords:  Leone's Paolo camerata Martinno Aguila Bloommeadow
<!--  LocalWords:  Seneschalhood
 -->
