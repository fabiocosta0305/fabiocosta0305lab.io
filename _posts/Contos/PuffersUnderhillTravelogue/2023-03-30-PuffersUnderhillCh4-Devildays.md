---
layout: contos
title: "Puffers Underhill - Chapter 4 - Devildays/Callaleah: Preccia"
subtitle: A Wanderhome Story
category: stories
excerpt_separator: <!-- excerpt -->
---

> + Devildays
>     + Dry Grass
>     + A Calming Breeze
>     + The Cruel Bright Sun
>     + Lazy Teens
>     + A Festival Remembering A Local Folk Hero And Their Adventures
>     + A Lonely Lute-Player
>     + Sweat-Soaked Clothes
> + Calleleah
>   + Carnival Games
>   + A Costume Contest
>   + A Farcical Play With A Raucous Audience
>   + Children With Noisemakers
>   + Lots Of Pranks
> + Farm
>     + Crops As Far As The Eye Can See
>     + Rusty Overgrown Weapons Of War
>     + The Scarecrow That Walked Away (folklore)
> + Lake
>     + Barnacle-Covered Rocks
>     + Sandy Coastline
>     + The Hubris Of The Warthog Captain (folklore)
> + Metropolis
>     + Tall Ramshackle Apartments
>     + Laundry Hanging From Windows
>     + The Truce Of The Three Gods (folklore)

<!-- excerpt -->

_"Heyia,_ gamberetto!" I heard someone says. It was Sabrinna Scoiattolo, from _La Rosa Rossa_. And I could see in the port, _La Dolce Conigglio_, the boat that brought me to Talmina almost two months before.

_"Sabrinna!"_ I say, smiling

_"Looks like you had a nice time after all. Heard what happened about_ Castello Porcinni? _Hope you've had your answers before that happened."_

_"Yeah, you can say this way! Ah, and this is Otello Lontrini, he gave me a ride to here. We had just arrived."_ I say

_"Yeah, that last farm was weird, that story about a_ buonnandanti di piagli, _a walking scarecrow!"_ he says

_"Heard about this one! Want the full story,_ gamberetto? _Have some fresh_ polenta _here, and the bumble_ pepperoni _is fantastic!"_ she says, while his man was in aside the boat, playing cards or raucously playing songs

I just get inside, the boat locked to the port that connect Preccia to the river and the Sea. The _La Rosa Rossa_ cook served us with a variation of _polenta_, fried and with some sauce, and some _pepperoni_, the spicy sausage so popular there. And there was some berry wine and grape juice too.

_"So, it was a long time ago, in the time of the_ Signori, _when the duchies_ principi _and_ principesse _fought each other for survival and dominance, before the_ Conigglia _lineage risen to the Throne. Preccia wasn't exactly one duchy, but a confederation of three duchies, or better, two duchies and_ La Chiesa, _the Church, an old institution here in Talmina that has  a belief that there's only One God, they call_ Sacríssimo, _the Most Sacred, and all the other gods are in fact entities, some goods, some evil, but all under_ Sacríssimo'_s authority. But, it was when, in one of the_ Giorni del pasticcio _at that time, some saw that strawman, walking all around. People thought it was a_ Commedia _flick, but then another one came from the other duchy... And then, a third one, this one on a_ Fratello'_s cassock, the suit of a_ Fratello, _a_ Maestro _from the_ Chiesa. _And then, they looked to each other, like challenge themselves, and they sacked silver blades, and started to stab each other! And, in fact, no blood was drawn, except the one that was impregnated on the straw itself,_ paglia insanguinata, _the Bloody Straw! This happened time and another for some years, always at the_ Pasticcio, _and populace avoided it as much as its possible, fearing either the blades and_ pagila insanguinata, _that was took as cursed since some people took them and got poisoned and died."_

_"Wow!"_ I say

_"It was when, the Last War started by the Porcinni against Talmina, and that happened more or less at the time of_ Pasticcio, _and they feared that the Porcinni, or another warmongering_ Signori, _sent assassins. But then, the three Strawmen came again, like in the past... But, that time, they got themselves in the_ Tripla Strada, _the Triple Way, the point where Preccia city was split between Preccia,_ Old Leprione, _and_ Chiesa _territories. They stood each on one corner, looking each other, like before. But, this time, they did something weird: they took their silver blades, lifted than on a  salute to each other, like calling old comrades and screamed_ 'VIVA TALMINA! VIVA MARINA CONIGGLIA!'" she continues

_"You see, Marina Conniglia was a_ Matronna, _one of the most powerful kith in city, man or woman. And she was unaligned with all them: she was a_ Chiesina, _a believer on_ Chiesa'_s dogmas, but she wasn't as strict as others, quite the contrary: she did lots of charity work and good work for everyone, no matter the God or gods they believed:_ "Il Sacríssimo _gave us a Commandment:_ Il tuo vicino è importante quanto te, _your neighbor is as important than thou. Me and my family will live under this tradition till The End of the Days!"_

_"But she wasn't well cherished for the_ Leprioni, _or by Preccian authorities, and some believe that they were co-conspiring with each other to take her life and blame the_ Chiesa" she says

_"Talk about a lousy plan!"_ I say

_"That it was, because, just when the_ Leprioni _and the Preccian rose their forces against Marina, it was when the Porcinni came to invade Preccia, the Albionneri banners risen against us. And in the chaos of a three-team battle, the_ Buonnandanti di Paglia, _as the Strawmen were called, rose the populace against either the Porcinni, the_ Leprioni _and Preccian, and even against some_ Chiesini _that was aside any of them! And they fought hard, the blood created the_ righe insanguinate, _the Blood Lines, that split Preccia as it's now. But the populace pushed them, even with this so big cost. The_ Principi _and_ Maestri _from_ Leprioni _and Preccians were hanged, aside some that Martina explicitly pardoned and brought under her wings, almost literally as she was a big Matron hen. But, she wasn't interested in the crown or in the throne, but on charity and_ Una Talmina Unita: _she had saw enough pain all around during the Old Wars, and she wanted to unite Talmina, preferable under_ Chiesa'_s banner, but otherwise if needed."_

_"And it was when, the_ Supremo Fratelli, _Supreme Brother, the biggest on_ Chiesa _hierarchy, called Martina for a talk, and after some days confabulating, and calling some of the Leprioni and Precciani that survived all this, they instated a new lineage, with her young daughter, also called_ Marina, _this one a real rabbit, instated as Queen with a Consort from the Leprioni and a_ Supremo Maestro _from the Precciani. Since them, after the fall of the Porcinni, the_ Conniglia _lineage is the one that commands Talmina. The crest on our banners shows this, the three colours from the Three Duchies: Orange, for the Leprioni, Yellow for the Preccian, and Red for the Chiesa, and for the Conniglia."_ she says

_"Well, sound like an incredible story... But do you think that the strawman we heard about could be a_ Buonnandanti?" I ask

_"The legend says that, in times of crisis for Talmina in general and for Preccia in particular, the_ Buonnandanti _would rise again, their silver blades ready to protect Talmina."_ she says _"Unfortunately, some 50 years ago, a group reinstated themselves on a corruption of this tradition, calling themselves_ Il Inquizittori, _and they started to push away from Talmina everyone that, on their words_ 'would not look upon the tradition of the _Buonnandanti_' Spavalderia, _I say: there wasn't an unified Talmina from before the Conniglia! You can't use this as an argument!"_ she says _"And recently I've heard loads of weird rumors, about how some people wanted to break the peace from the_ Buonnandanti." she says.

_"Sounds like a problem."_ says Otello _"I mean, even for us, from_ Briatoli _or_ Mugenna, _Preccia's stability means Talmina's stability. And there's still some old ladies that remember the_ Erano i Signori, _the Age of the_ Signori."

_"And even more that people saw that_ Castello Porcinni _had fell, people got this as an_ algurio, _a portent from the gods. For good or ill, no one knows, but nothing happened to that part of Albionne since the Age of the_ Signori". says Sabrinna

I felt a small shiver, and couldn't be about cold: Devildays are the hottest time of the year, and I've never got it on a so hot place like here in the South at Talmina.

_"Tii hot for you,_ gamberetto?" smiled Sabrinna, noticing my uneasiness on this hot place, serving me another glass of juice _"Drink this one: we call this_ Acidittà. _It's made with all kinds of tar fruits, more or less like an non-alcoholic punch. It's a little bitter, but with bumble honey and correctly iced, it's incredible."_

I take the yellow-white-ish colored juice on the glass cup and take a gulp. Really, she was right: it was bitter on the start, but some seconds after it grew very refreshing!

_"This is perfect! Need to take the recipe!"_ I say

_"Are you becoming a gourmet?"_ she smile

_"Nah... I'm just... Well, Scholars from the Furrow Heighs normally have an old Tome that you copy by hand from your Preceptor page by page, just when we start. But recently I lost my one and decided to do a new one, but with things I've learned from my journey"_ I say, but without saying her that as _Arlecchino_ I destroyed it by myself when we toppled the old _Superior Maestro_ from the _Univercittà_.

_"Now... Let us find some place: it's time of festivities here. Aside the_ Pasticcio _it's time of_ Cellebrazionne: _it's anniversary of Melina Conigglia's Queendom. She's daughter of Old King Salvio, son of Old King Rastelini, son of Old Queen Maressa, daughter of Old King Apolonio, son of the Oldest Queen Marina Conigglia... Normally is a_ mascherata, _a costume ball. It's not that expansive, and maybe you'll learn a lot,_ gamberetto!" she says

_"Hope this year I can go: it's not that cheap though. Even the most cheap admission ticket is not that cheap. But think the crop was right enough to allow me some fun before getting back my farm."_ says Otello

_"I'll try to find some money for this... Maybe selling some old stuff..."_ I say to myself _"Anyway..."_ I say, when someone comes to us, under a very official clothes.

_"Would any of you be_ Maestro _Puffers Underhill?"_ says the kid, a mouse one, with a very innocent look, but on formal vests. The Royal Conniglia Crest was there, and he was using the symbol of the _Chiesa_, a caret simbol.

_"It's me... But I'm not a Maestro..."_ I say

_"I have an_ Convocazione Ufficiale _from The Queen itself,_ Vossa Maestà _Melina Conigglia. She Demands your presence to her Quarters at the time of the afternoon meal."_ he says, producing a roll of parchment, and we see.

"Porca Misera! _It has the Official Seal of the Queen!"_ says Otello _"Who are you?"_

_"In fact, you're not just a wanderer as you show us, Puffers."_ replied Sabrinna

_"Indeed, I'm not!"_ I think to myself, knowing that calling me _Maestro_ implied know, or at least suspice, about what happened at _Castello Porcinni_.

But, there was no way to run: an official convocation from a high protectorate, no matter the place, shouldn't be avoided without dire consequences. And I thought on Otello and Sabrinna, that could suffer retaliations.

_"Alright... I'll go. However, you need to understand I'm just a wanderer, and don't have appropriated clothes to go in front of_ Vostra Altessa." I say

_"No worries. The Court can provide something suitable for your station..."_ says the kid _"In four hours you'll be expected at Your Majesty Quarters."_ he finishes, doing a greeting and turning away

_"On the Majesty Quarters?"_ says Sabrinna _"Sounds like Serious business!"_

_"What had you done? Who are you, Puffers?"_ asks Otello

I just shake my head, trying to understand what is happening.

---

As soon I could, I greeted Otello and Sabrinna and gone for the _Castello Precciana_, Precian Castle. I showed the Royal Convocation, and a local Seneschal ushered me inside to a room.

_"We are already ready for you,_ Maestro. _I hope you understand that_ Regina Conigglia _shouldn't be in front of any foul smells, like... You know."_ said the Seneschal, a very mellifluous badger _"Like my smell!"_ I would like to reply straight, but I'm well versed on Courting to know it would be a big blunder. Better to play the game until I understand what is happening.

He presented me with a small visitor room.

_"You can leave all your effects here and collect them after your audience with_ La Regina. _You are to clean yourself and make yourself presentable as the_ Maestro _you are. You can use anything on the wardrobes that fits, and that will be you after all, as a demonstration of our respect for the time you provided for_ La Regina. _Hope you are acquainted with the Court behavior."_ he said

"Varst! _What an old fart!"_ I thought _"If you really took me for a_ Maestro _perhaps you shouldn't be so condescending!"_

_"I'll be back in an hour, and a maid will came to help you."_ said the Seneschal, before doing a greet and going.

I go straight for the bathtub, ready for me: it was like the one at Martina's place, but so... Royal... On a bad way: everything so Regal, and so... Cerimonially artificial!

_"I got too much time away from this kind of stuff, for good and ill. Got a little unaccustomed will all this protocol."_ I say to myself, undressing myself and cleaning myself.

_"Maybe, I should not let the_ Albionneri _stuff here. So weird they calling me a_ Maestro. _They should knew I was instated as_ Ultimo Albionnere Maestro _if so!"_ I think, while cleaning myself. I'm not so comfortable to take time, so I bath and dry myself fast, and opened the wardrobe: 

It was a festival of rich clothes, things that even my old Scholar stuff would be rags compared to them: colorful clothes, sober and nice looking cassocks, overcoats with bouffant shoulder pads, even the underwear was on rich fabrics and embroidements.

Anyway, I though on what a _Maestro_ used based on my experiences with _Univercittà_, and chose, aside underwear, a nice maroon cassock, that I closed with a belt with a steel buckle. For the feet I got a pair of nice leather boots with buckles, a pair of long johns under the boots, over my feet and till my waist. I chose a darker maroon overcoat with not so bouffant shoulder pads, and took a beautiful dark red beret hat to cover my head.

_"Oh,_ Signore Maestro." I hear a squeaky behind me, and look a somewhat pretty mouse girl, almost a kid, that just got inside on maid clothes: a big heavy black dress, a frilly white bonnet with small holes for their ears, and a white pinafore. Her fur was white and brown, with some light brown snout and whiskers. _"Sorry being so late to help you with your dressing. I'm Elena Ratiratta, one of the_ Regina'_s Chambermaid."_ she finishes with a curtsy.

> + ***Elena Ratiratta, Mouse Chambermaid (She/Her)***
>    + Still lacks experience on her job, but very loyal to the Queen as a chambermaid. _Chiesina_
>    + _Crafty, Honest, Watchful_
>        + Point out the truth everyone else has been ignoring.
>        + Propose an alternate approach.
>        + Point out something people missed

_"My pleasure. Puffer Underhill, at your service!"_ I reply

_"Oh, no! I'm on your service,_ Maestro. _In_ Nome del Sacríssimo, _I need to learn to respect my superiors."_ she says

_"And you're doing pretty well. But, from where I came, those that are superiors on ranking also needs to be kind with those from below stations."_ I say

_"You're as kind as wise,_ Maestro _Underhill. But now, we need to go!_ La Regina Melina _has very few time on their Quarters, and even fewer that she can speaks with those outside the Castle._ Il Preceptore _don't like this kind of things."_ she whispers, while ushering me gently outside the room.

_"Just a minute."_ I say, getting a small leather satchel and putting inside the proofs about what happened on _Castello Porcinni_: I don't want this to be too far away from me, the journal, the sigil and the cloak. _"Now we can go... And... About this_ Il Preceptore, _it's the old badger that brought me inside?"_

_"Yep..._ Maestro Antonnio Tassini _is the old preceptor and Seneschal here at_ Castello Precciana, _and also works a steward together with the Housekeeper's chief_ Porpinella Porcospina..." she giggles and then put the hands over his mouth _"Sorry,_ Maestro, _I should not laugh on my superiors."_ she says, looking to me, that was almost to laugh too

_"Don't worry: you still have lots to learn, and no one should blame you otherwise. Just learn that is her name, and there's nothing that can done about this."_ I say

_"You're so kind,_ Maestro, _like_ Signora Porcospina. Maestro Tassini, _otherwise, is so strict. He commands everyone and everything here like he was the_ Il Re! _And..._ she shudders _He doesn't believe in the_ Sacríssimo _like the Queen and me. Do you believe on Him?"_ she asks

> + ***Antonnio Tassini,* Castello Precianna'*s Seneschal (He/Him)***
>    + Mellifluous Badger with those superiors them him, it's not nice with those below
>    + _Royal, Cunning, Learned, Venerable, Proper_
>        + Inflict your will on the world around you.
>        + Get somewhere you’re not supposed to be.
>        + Reference a text no one else here has read.
>        + Tell someone how they will repeat the mistakes of the past. If they want to prove you wrong, they’re going to need to spend a token.
>        + Judge something for its inappropriateness.

_"Honestly, I don't know how to reply: my people is a little faithless, so we normally doesn't care too much for god or gods, as the North is so unforgiven that looks like we were forsworn by any god that could exists. However, I think there's things we can't learn by books or mind, but only by our experience. And I believe above everything on the goodness on kith, even some of them being the worst."_ I say, trying to not offend her feelings and believes

_"You're kind."_ she says _"Perhaps_ Il Sacríssimo _has something for you_ Maestro. _Hope you have the answers that_ La Regina _wants: you see, some says that she can see the future in dreams, but sometimes even her can't understand them, when she consults_ Maestro _Tassini about them. But, for some reason, this time she demanded_ you, _saying she heard your name on a dream, something about_ Castello Porcinni." she says _"Had you saw the_ Castello? _We heard recently it got in shambles, and that the old_ puzza di morte _is no more."_

_"I saw it, coming here, but it was already in flames. Looked like the land itself was being cleansed."_ I say, putting a small lie on the bigger truth.

_"Wow... Looks like something important, as Albionne was really accursed in the past because the warmongering of_ Principe Porcinni, _everyone knows that."_ she says, when we get to a lateral door, with the royal crest on it. She gave what looks like a special sequence of knocks, and then someone looks from a small trapdoor

_"It's me, Elena. With the_ Maestro." she says

Without no speaking, the trapdoor closes and the door opens.

_"Welcome, my dear_ Maestro!" says a nice singsong voice. A pretty rabbit, totally white, except by the insides of ears, button of nose and eyes that all were pink. She is dressed on a beautiful peachy colored dress, and uses a crown over her head. She looks royal and proper, but on a good way, whe I put it on comparison with _Principe_ Porcinni.

_"Your Majesty. I'm Puffers Underhill."_ I say, presenting myself

_"I know who you are,_ Maestro." she states on a light voice, and then she looks for some soldiers and gives a small hand flick, demanding them to wait outside. When Elena was to got out, she says _"Not you, Elena. In fact, I demand thee as my tea-maid for today. Hope you have no other services by now, but if so, I'll let they know I excuse you from them."_

_"As your command, Your Majesty."_ says Elena, with a deep curtsy, going for the tea service, while me and the Queen got to the chairs and small table nearby her bed. It looked like everything was setup for security and privacy: afar from the windows, all of them closed with big heavy curtains.

_"Now... Mr. Underhill: I heard some stories about you and what happened at_ Univercittà Verde, _in the Ablution... But this is not our topic today. In fact, I called you as_ Maestro _suspecting you would understand..."_ she says

_"I... I don't know how to say..."_ he said

_"I know what happened._ Il Sacríssimo _conceded me the gift of true dreaming. I can see thing in my dreams that happened, happens or will happen. I can't see them totally, but what I see is as truth as truth can be. And I saw you on a dream, with the clothes and sigil of an_ Albionneri Maestro, _even knowing that Albionne was destroyed so long ago. And I could see you had some... Chains on you, and that those chains linked you with Castello Porcinni."_ she says

_"I can say, Your Majesty,"_ I speak, in my not too good Talminare _"that your vision is true. I can't, however, talk you all the details, under a Superior Command, one beyond Life and Death. I'm sorry I can't provide more information about this. And hope you excuse me for my not so good Talminare."_ I say

_"I excuse you, as in fact it's pretty good for a foreigner. And I'll not force you to forsworn a superior command. In fact, this is not the topic I made me ask your presence you here today. This year,_ Celebrazzione _will specially be upheld together with other two festivals: first one is_ Giorno del Pasticcio, _that you from North call Callaleah. And the second is_ Buonnandantara, _or the Strawmen's day, when we remember the Three Strawmen that, this is what legends said, helped in our unification, avoiding a big sort of plans to topple us that could bring war on Preccia, and would bring decease to my lineage before even it started with my ancestor_ La Vecchia Regina Martina Conigglia, _daughter of_ Matrona Martina Conigglia. _There's some differences of view between us, from_ La Chiesa, _and others, but the fact is, the Three Strawmen represented_ Il Sacríssimo'_s plans, even that got some entities to help it."_

_"Well... Pardon me, Your Highness, but I can't see how this could imply myself."_ I say

_"I'll speak now on a way that even Elena will not know. As you can see, I'm studied and can speak and understand Old Furrowian. Hope the accent isn't too atrocious for you."_ she says, and I can see Elena not understanding. I give an affirming look to her and she understands that it's something that shouldn't be understood for her

_"Not at all, Your Highness. It's pretty good for someone that could have no Furrowians to train with. Better than my Talminare, this I can say."_ I reply

_"You flatten me, Mr Underhill. Another advantage is, I don't need to follow protocols speaking this way, as the only ones that can totally and truly understand what is said are you and me. So, I can open myself true and genuinely, as so rarely I can do, violating protocols if needed. You see... I need to say, I saw you on another dream... As one of the Strawmen!"_ she says

_"One of the Strawmen?"_ I ask, raising an eyebrow

_"I saw the Strawmen coming to the city. However, at the same dream, I saw people looking for me, trying to get me and to imprison me, even to kill me. You know, even with everything that happened, the_ Conigglia _lineage isn't 100% recognized by some poeple, even inside the_ Chiesa. _Some say we are too much soft with the_ heretici, _the ones that wants to follow the old gods and old costumes. But, and this is our motto since The Old Queen and The Matron,_ your neighbor is as important than thou, _we could not do otherwise."_

_"Understandable your worry and some insatisfaction. Some people just want to subjugate everything and everyone, by power, and needs excuses to do this. Unfortunately, for some_ Chiesa _and it's Commandments is the excuse. Sorry being so blunt, Your Highness."_ I say

_"No worries... In fact, this is exactly what I expected from_ Il Ultimo Albionneri Maestro." she says, using the title on Talminare, making Elena gasp _"I've saw you where titled this way by the Albionneri fallen, in my dreams. You don't worry about commands, because this is from my dreams, you didn't forsworn your command."_

_"I'm glad, Your Majesty, but..."_ I was talking when she did a small gesture asking me to stop talking

_"You see... There is a prophecy, an old one, that explains why none of the old Monarchs, Queens, Kings,_ Principi _and_ Principesse _got to take_ Castello Porcinni _or even try to purify that place. It was said that the day_ Castello Porcinni _was cleansed and_ Il Ultimo Albionneri Maestro _got in Talmina, it would be the day Talmina could got back for the_ Erano di Signori" she says

_"What? Are you saying..."_ I say, and then she did another gesture of silence

_"Now, my dear Scholar: I don't believe on that one. Many of those that believes on this balderdash are from either the populace or that wants an excuse to conspire against me, as you said yourself. However, on my dream, I saw the_ Buonnandanti _back to Preccia just when I was to be attacked, and I saw you as one of them, no straw hiding you and your marks of someone that Walked Between The Dead."_ she says

_"So... You believe that, in the end of the day, I'm here to save you from conspirators?"_ I asks

_"Perhaps, it was a small tithe Albionne sent for their freedom to Go Forward to be judged by_ Il Sacríssimo. _Perhaps, it's something you'll need to help us. I don't know. What I know is, in my dream, I saw you as a_ Buonnandanti, _in fact the one that protected_ Chiesa _in the past!"_ she states

_"Well... I'm not exactly an example on believe on anything, aside what I look..."_ I say

_"But, in fact, you had done this cleasing of Albionne someway."_ she states _"I can say you'll help me."_

_"I can do this... However, I want to make clear I'm not exactly a fighter. Had some fights, of course, but it was by sheer luck and some omens of the gods I've survived."_ I say

"Il Sacríssimo _put you in my dreams as a_ buonnandanti. _Perhaps there's ways to help me."_ she says

_"Had you got any menace recently? Something that could sounds like organized?"_ I ask

_"There's a group, called_ Il Vecchi Fratelli, _The old brothers. It's a very old conspiracy against us from_ Conigglia _lineage, and they are basically from_ heretici _that thinks we should shun and forsworn_ Il Sacríssimo." she states

_"And on the Higher stations on society... Anything you know?"_ I asks

_"Nope, to be honest."_ she says.

_"And... In your dream, you saw if people were dressed like the hoi-polloi or like the noblesse?"_ I ask

_"The problem is that I saw them dressed like it was for the_ Pasticcio" she states

_"So, in costumes, probably in masks, like in a_ mascherata." I say.

_"Do you think you can help me,_ Maestro" she says

_"To be honest, I can't promise anything, except do my best. But I'll need help. I have some affiliates here that can help with on how Preccia works... But I wanted someone that could help me and is linked with Your Highness, if I need to get into contact with Your Highness fast. Preferably someone that could pass unscathed through your own guards and could report straight to you."_ I say

_"Elena!"_ she says and then, back on Talminare she says _"Elena, I Command Thee to help_ Maestro _Underhill on His Demand, no matter what. His Word will be my Word, his Command will be my Command. And you'll obey him as you would obey me."_ she says

_"Yes,_ Vostra Altessa." she says, curtsying

_"This is enough,_ Maestro _Underhill?"_ she says

_"Yes, it is..."_ I say, accepting the demand (+1 Token: 2)

---

_"I don't understand why_ Regina Melina _would allow you to have one of her chambermaid as a companion on this wild goose hunt for conspirators! I myself think she's too much haughty on thinking on those dreams._ Castello Porcinni'_s falling wasn't about if, but about_ when. _It was something from more than 200 years ago."_ said the Seneschal Tassini

_"Neither me. However, I can't feel that I should ignore her uneasiness... I heard weird rumors about Strawmen walking when coming to Preccia..."_

_"And you think this would be the_ Buonnandanti? _Kid, you are like Melina, and you see too much outside the real world."_ he states

_"That can be, but there's only a way to discover if she's on fantasy or she is truth."_ I state (-1 Token: 1) I say, back on my common costumes, and now with Elena on common costumes too. I have the satchel I've got to hold my _Albionneri_ stuff now with the Court costume.  I do a last nodding for the Seneschal and go.

Sabrinna and Otello were nearby, on a _ristoranti,_ eating some bread and coffee.

_"What happened?"_ she says _"I thought they would at least allow you take the costume you've used as memento..."_ 

_"They let. But... I need your help, and is something very important. I can't say all the details here, I need a safe place to talk."_ I could say _"The_ Pasticcio _is coming fast this year with all the other holidays getting together with it."_ I say

_"We can go to my boat..."_ Sabrinna says, while we walk there and I present them to Elena.

In the boat, Sabrinna brings us to her room as captain of the ship. I do a very fast summary on what happened and talk about me being _Il Ultimo Albionneri Maestro_.

_"So,_ Regina _saw you saving her from conspirators? But, who they are."_ she says.

_"I think we'll need to go for some place where the riffraff stands. If nobles are conspiring against her, they'll use the most ignomous and disposable people they can find. And only in a place were riffraff gather together they can find those kind of people."_ I say

_"There's a place like that nearby!"_ says Elena _"I only know that because I got there to take my father from there lots of times, till he died and I was accepted under_ Regina'_s service_. Il puzzore puzzolente, _The smelly Skunk. It's a terrible_ taverna _nearby the sewers exit. Some people says that they are there because they can throw away the bodies of those killed there straight in the sewers."_ she says

_"Well, I can go there with you. I don't fear a nice fight in a_ taverna." says Otello

_"And I'll let my men and some friends know you can need help if things goes south."_ says Sabrinna

Sometime later, I go with the others to the _Il puzzore puzzolente_. It's a very smelly place, in fact, and very bad lighted. I got some old, dirty clothes and gave a small skunking on them, let them even filthier and smelly. Elena then used some soot to dirty us and I shagged my fur myself. 

The bar itself is what I've expected: foul-smelling, dark, full of all the worst seamen and hoi-polloi you could imagine, drinking some kind of old wine, smoking a smelly tobacco and otherwise talking rowdy jokes, playing cards and other rowdy games and being ill-behavioured in general.

_"Newer guy in the block?"_ said a ferret guy, with a eye-patch. _"Never saw you, and skunks are not common this south"_ he speaks on a very ill accented common, mixed with words on other languages.

_"Name's Starn"_ I say, trying my best dirty impersonation of a riffraff _"Otello here said I could find some work here. This is my woman, Callisteni."_

_"Furrow Heighs?"_ says a cat guy _"Looks like, there's many skunks there, training on with the Seneschals"_

_"Nah, I loathe'em, those sissies."_ I spat in the ground in the most horrible way I could _"Carnverash is from where I came. Callisteni here is from Altalus, and we found ourselves at South Glens."_ I say

_"Hey, guys!"_ said a skunk guy, bigger and really mean looking _"Chap here looks like a nice fellow, nah? Let me test him on a nice skunky game of Stankart."_

Stankart: played it only as a kid, when my stink glands came with when I got teens, as many skunks does. Basically two skunks wraps their tails on each other's snout and blast their stink successfully in rounds, first turn at the same time, until one of them gasps, pukes or passes out. Like a skunk version of slap fighting. Not my kind of stuff normally.

_"Whatcha say me, huh? Worried to see if you're not smelly enough?"_ he says, with a mean, brutal, but honest and truthful smirk. He knew that, no matter what, a skunk stink is his pride.

_"Dunno, but I'm up to try!"_ I say, and them people open the circle, Otello and Elena waiting aside.

"Alright!", a weasel came _"Ya both know da rules: first to gasp, puke or fall out, loses! Winna win da prize! Now, up your tails, boy-os!"_

I feel his old, rash tail running around my face, and now I feel a little worried: I bathed myself some hours ago at the _Castello_, and maybe he'll notice it!

But... On this preparation... I feel his smell, and he's not that stinky. Looks like he's also not that hard guy. Maybe I can find a good friend here, after all (+1 Token: 3)

I feel his tail grasp over my snout, and I finish my own grasp over his snout... We are skunk locked as its said... It's time for Stankart!

_"Nah! Uno, Dos, Tres... Go!"_ says the weasel.

A thing that few people know about Stankart: it's not just about how smelly _you are_.

It's about how smelly _you can feel the other skunk think you are!_

And there's lots of techniques for this in the end of the day.

As soon the weasel opened the account, I concentrated my stink to bombard it, but I just let a _tad bit_ goes before he get the go. So it would open that guy snout somewhat.

And then we released our stinks.

And I noticed it worked, otherwise I would be puking out because he was really smelly.

He tried the forceful way, by blast his stink on a single note. But, when I did my trick, he lost some of the concentration and could not skunk me enough, while made to feel my stink full round.

And me, I just finished the service by skunking him hard.

_"Yuck, that was a shitshow!"_ commented a monkey nearby enough to be the unfortunate one to feel two smelly skunks together, on our remaining stink getting into his nose!

The guy released his tail and, even feeling ill, I released my one. But I was not gasping as he was.

_"Yeah, new guy is da winna!"_ says the weasel and some people cheered, other booed, but I could hear some coins exchanging hands. In some moments, the tavern got back his normal activity

_"You're pretty good on Stankart, and looks you is stinkier than old Fredal here!"_ says the old seaman, sitting on his table, feeling a little sick, but smiling hearty. _"Take a drink! Looks like you wanna to know something!"_ He fills some eartheware cup with the contents of a recipient made from bumble hide. I take a sip: it was not the best wine I've ever drank, but it was at least drinkable. Hope Otello and Elena could hide any disgust.

_"Yeah... Heard about some guys wanna to mess with_ Chiesa." I say openly 

_"Who wouldn't?_ Chiesa _make difficult our job selling stuff here. Butcha know, there's some_ Chiesini _on big guys pockets and vice-versa. Overpious guys need some stuff, I heard. Big, nasty, stuff. They want to do big mess during_ Pasticcio. _What their problem? Just have fun, I say!"_ says Fredal

_"Don't know... Heard a weird gossip. Says_ Chiesini _want to topple Regina"_ I say

_"Only in their dreams. She's though, I say, but she's fair. She knows how shit works. She doesn't mess with us, we don't mess with her... Except..."_ says Fredal

_"Except?"_ I ask

_"Heard that gossip about her dreams. She's cuckoo, I say! But some guys said_ Castello Porcinni _was an omen. Old dead fart was freed, they said, to help them topple the Queen. Balderdash I say. Old dead fart is dead, I say. Lady cuckoo is cuckoo, but let us in peace."_ she says

_"Anyone you can say that was on this idea with the old fart?"_ I asked, when I see someone throw a dagger against the guy. I put myself in the way, being hit in the shoulder! (+1 Token: 4) I scream in pain, the blade getting all the way through my shoulder

_"Shaddap, smelly fart. The Old Prophecy happened! Time come for_ Chiesa _falls! For Porcinni!"_ screamed a hog looking pirate, and some guys started to throw things trying to hit us and going for us. But this only made start a bar brawl!

And then, I see some guys I knew from the past: the _La Dolce Conigglio_'s crew!

_"Take the girl, Biggia! Sabrinna said to bring her there! I take Puffers and the other guy! Stolas, open the way for us. Falstaff, your darts!"_ said the raccoon captain, while an eagle guy started throwing darts, probably with some narcotics as people fall in ground asleep, and a big brown bear, just pushed people away, throwing them against the others.

_"Tanis!"_ I screamed _"Take Fredal, we need to help him!"_ (-1 Token: 3)

_"Right!"_ he said, and pushed the big skunk pirate with him, some of his tripulation helping us!

_"To the ships!"_ screamed Tanis _"Biggia, the signal!"._ 

Biggia pushed what looked like an old baton and, after using a flintlock to lit, threw in the air, making it explode in colors!

We splitted into three ships: _La Dolce Conigglio_, _La Rosa Rossa_ and one that I could read like _Savaltare_.

_"On the way, everyone!"_ screamed Fredal, on _Savaltare_ _"To the sea!"_

The three ships sailed away, before anyone could follow them.

---

_"I'm glad you saved my life, chap!"_ says Fredal  _"Need to say normally I don't give a damn for anyone messing or not with Queen, but those fuckers are conspiring to topple them. Looks like some big kahuna has a nice cut on graft money for allowing some goods to get into Preccia, under_ Chiesa's _nose. Big stuff, as far I know. Me? I'm more on 'common' pillaging. Easier to deal, sell fast and don't mess more than I can deal with."_ he finishes

_"Do you know who could it be?"_ I say, from the _La Rosa Rossa_, while Sabrinna's physician was patching me. The wound was big and profound for a dagger, but thankfully looks like there was no nasty stuff on that blade.

_"Dunno... Only saw the fucker one time: on cloak, could not distinguish the kith. Honey-mouth, those are the worst fuckers: they do some saalam, you fell on their stuff and then, poof, it's you that have a noose around your neck over the scaffold."_ he says

_"But he deal with_ heretici?" I say

_"Not only. Didn't you noticed? There was_ Chieserini _there. When you ass is ready to be bitten by sharks, you pray for anyone."_ he states

_"I saw what is happening in the cards, Puffers!"_ she says, opening her Tarot and sacking three cards. "L'Imperatore, Il Tredici, La Giusticia. _The Emperor, The Death, The Justice. Looks like, whatever happened at_ Castello Porcinni _triggered some kind of... Reckoning... For Preccia, and maybe_ Regina'_s life is really at danger. And so the_ Buonnandanti _risen again for helping Talmina like in the past."_

_"I don't know about you, but I'm not up to redo the Old Captain Warthog Folly, thank you! He was stupid enough to join the Precciani at that time, and only think he got was to put he and his family on the noose! I like me neck exactly where it is, thank you."_ says Fredal. _"Maybe it's time getting back sea for a while, before shit start to flies like stinky pies all around."_ he says from the height of his pirate ship, Savaltare, when he points to a direction.

A ship has just arrived. Sabrinna got a Pirate Lens and pass some other for us. I put it over my eye and look to the boat, even with some pain still on my recently patched shoulder.

_"It was that Hog!"_ I say _"The one that tried to kill you, Fredal, and left me with this gift."_ I show him the dagger Sabrinna's physician extracted from my arm

_"Yeah, that fucker Hans. Heard he was making big dough with stuff for the Preccia_ Noblesse, _under_ Regina'_s nose Pretty pushy posh, I say he is cuckoo, but he has balls to do this..."_ he says

_"...or have help."_ says Sabrinna, pointing to the other side, a small boat rafting with some guys, coming from Preccia _"Look they are coming from the_ Noblesse Porta, _the Noble Port."_

_"But_ Noblesse Porta _can be used only by Royal boats, or by the Nobles!"_ gasped Elena

_"Maybe we have part of our answers now about them!"_ I say... _"Looks like really some pirates have support from the Nobles that wants to topple_ Regina Melina."

_"Look! There it is! Someone got from the boat!"_ says Otello

I could say. It was a big guy, with a slow noble walk, and a hunchback... A so familiar hunchback.

_"This guy sounds familiar..."_ I say, looking 

_"I can do some lipreading, even that distant. I'm really good on this."_ says Fredal

We can say the men talking. The Hog guy looks very angry, while the cloaked kith looked wise.

_"He was saying about... Discovery... And... Angry... And... Queen... And about_ Castello Porcinni." says Fredal _"Looks like this fucker is really on this and... They are talking hard!"_ he states, while we say they where bickering each other, till the Hog took the cloak!

"Sacríssimo mio!" sweared Elena _"It's..._ Maestro _Tassini!"_

_"Hide themselves! Turn off all the lights!"_ says Fredal, and we hid ourselves behind the ship's gunports.

_"I can't... I can't believe it!"_  states Elena _"The_ Maestro? _Why? Why he's doing this? He's_ Chiesini! _And the_ Chiesa _is with_ Conigglia _lineage!"_

_"I can think: he's from another side of_ Chiesa, _a stricter one, one that is against the old gods and wants to shun them away from Talmina. Or, at least, put people that has belief on the old gods under their wraps, and this can be the way for this... Perhaps..."_ something grew on my mind _"Elena, do you know if_ Supremo Fratello _will came for the_ mascherata?" I ask, worried

_"Of course he will:_ mascherata _is the biggest social service at Preccian Court. And is always did via_ Convocazione Ufficiale." says Elena

_"And so, undeniable unless under the dire straits!"_ I say.

_"What are you thinking on?"_ asks Otello

_"Looks like that the Dreams from_ Regina _isn't only for Preccia, but also for_ Chiesa!" I say _"Maybe he intents to topple not only_ Regina Melina _but the_ Superior Fratello!"

"Il Sacríssimo! _But, why?"_ asked Elena

_"It's what happens when faith become excessive."_ said Sabrinna _"He believes in_ Una Chiesa Unita, _One Only Church. But this Only means_ One Faith, _no other faith accepted at Talmina, unlike stated by_ Matrona Conniglia."

_"Those fuckers!"_ says Fredal _"He wants to turn Talmina on a Church State. This is really bad news! Last time this happened, a country was turned into a carrion's feast."_

_"Could this be... The reason of ressurgence of_ Buonnandanti?" asked Elena

Then I think...

_"You know... They want the_ Buonnandanti? _We'll give them the_ Buonnandanti!"

_"But how? Some magic? Or praying and calling_ Il Sacríssimo _for a miracle?"_ she asks

_"We don't need to get so far... Sabrinna, do you have any straw?"_ I say

---

Some days after, the last week of Devildays came, and with it, _Giorni dei Pasticcio, Cellebrazionne_ and _Buonnandantara_. Carnival Games, Costume Contest, Drinks For Everyone, Children With Noisemakers, all the merriment and raucous rowdy anyone could imagine, even some of the _Noblesse_ involved.

But, on the last day, was the finish of _Giorni dei Pasticcio_, with _Cellebrazionne_, a _mascherata_ with all the _Noblesse_ of Preccia... 

Including those that wanted to topple _Regina_ Melina.

But our plan was ready... (-1 Token: 2)

We contacted _Regina_ in secret, and Elena brought Otello and Sabrinna inside, disguised into the _mascherata_ but always nearby Regina. Also, some loyal soldiers helped me get inside. 

_"Hope your plan works,_ Maestro Gamberetto." one of them says while I ready myself

_"If anything goes south, you can always says it was a_ Commedia _and spanks me with a_ Batocio." I say, while dressing myself on my costume, the surprise I was holding to myself, even _Regina_ not knowing, only me, Elena, Sabrinna and Otello knowing about this.

Outside the castle there was raucous and noise and mischief, while inside things were royally well-tasted.

It was when the clock strikes the midnight, Devildays finishing and giving start for Swarming, Bright giving way Breath, and _Regina_ rises to proclaim the Last Dance, some men got from outside in black costumes, sacking blades.

_"You're, Queen Melina, and all the Conigglia lineage was, is and always will be corrupted by evil and lost the service under_ Il Sacríssimo. _The same came for_ Supremo Fratello _Tiesto. Time come to bring you both down! We'll bring a new regime to Telmina, under_ Il Sacríssimo'_s Commandments!"_ someone said, coming from forward, while the other black clothed conspirators pushed the _Noblesse_ aside, except some that already knew about the conspiration. And some guards came to try and fight them!

_"So, you dropped your own mask,_ Maestro?" she said, when Otello and Sabrinna could avoid being put under wraps.

_"So it will: today, the Conigglia lineage and_ Supremo Fratello _Tiesto will fell thanks their cowardice. On the Noose with them! The portent came! It's time for us to expurge Talmina from the Old Gods, like what happened with the fall of_ Castello Porcinni. _Their stench will disappear, and_ Il Sacríssimo Livorno _will restart from Talmina to all Haeth!"_ he says

_"Not that fast!"_ I screamed from the heighs of a balcony, isolated by the guards. On my costume! Regina looks and see on the other balconies, other two person on the same costume.

"Il Buonnandanti!" praised Regina Milena.

"Il Sacríssimo _sent their soldiers,_ Il Buonnandanti!" replied Supremo Fratello

_"You are against me?_ Buonnandanti, _you should serve the_ Sacríssimo Livorno, _the Most Sacred Work!"_ he screams

_"You call about_ Il Sacríssimo, _but your mouth smell like a skunk tail! You are the one that violates the work of_ Il Sacríssimo. _When me and my old fellows fought, we fought to understand each of us. But when the Folly came, we raised our blades for_ Conniglia _lineage, loyal to them while they were loyal to the Commandments of_ Il Sacríssimo, _what they did since then. But_ you, _you humiliate the poor, you explore the weak, you flatten the rich, you take the side of the powerful, you scoff the suffering of those below you. No more! The weak and poor risen their prayers to_ Sacríssimo, _and He Commanded  us to rise, and he sent a_ Gamberetto _to_ Castello Porcinni, _to undo the Curse of the Folly, to cleanse that land, to free_ Principe Porcinni _to go forward... And now, this_ Gamberetto _became one of the Buonnandanti!"_ I say, getting down though the drapes and going forward.

"Il Ultimo Albionnere Maestro!" said _Maestro_ Tassini, seeing the crest and cloak below my _Buonnandanti_ costume. A makeshift Silver Blade I unleashed and, weirdly inspired, maybe by the old gods, maybe by _Il Sacríssimo_, maybe by _both_, I started to fight some of the ruffians and conspirators, while Otello and Sabrinna started to do the same.

We were no more 3 kiths, in fact. At least that Moment, we were the _Buonnandanti_! (-1 Token: 1)

_"Yeah! I'm the one that got into_ Castello Porcinni. _I was commanded by_ Principe'_s spirit to find her Daughters spirits and reunify his family, split since his Folly and the Fall of Albionne. I was Commanded this way as_ Il Ultimo Albionnere Maestro, _the Last Albionne_ Maestro, _stated by him as His Last Command. And now, I'm here to protect_ Regina Milena Conigglia _from conspirators!"_ I say and then I speak _"Precciani! I'm Puffers Underhill, I'm a Foreigner, a Wanderer,_ Il Ultimo Albionnere Maestro, _one of the_ Buonnandanti. _And I Conclaim thee, no matter your social status, to defend your_ Regina. _You know what those conspirators wants in the end of day: the end of good things, the end of respect to each other, the end of a civilized life, based on freedom and respect, to one based on norms and fear. Don't be afraid: remember the_ Buonnandanti, _remember when they risen against Albionne Folly! You are Talminare or not?"_ I say, a force growing. I wasn't Puffers Underhill there. I was _Il Ultimo Albionnere Maestro_. I was one of the _Buonnandanti_!

And I'm there to defend _Regina_!

_"Elena, go with the Queen for the Chambers!"_ I screamed, while some guards were fighting some of their brothers in arms that were to betray them and kill _Regina_!

I ran to _Regina Melina_ and, before a conspirator came to kill her, I cut his Achille's Tendons and this made him fall in the ground in pain. And then did the same for another that came for _Elena_!

_"Not that time."_ said a conspirator, but then he saw someone getting inside with a big group!

_"A pirate! A goddamn pirate get to the Castle!"_ screamed a guard.

_"Don't!"_ I say _"He's with us!"_

_"Aye,_ Buonnandanti." he says and them he claim _"C'mon, you fool that want to topple Regina Melina! I'm Fredal Oldsmell, son of Thabald Oldsmell, from Icy Winds! Come and play! Let us dance the_ danse macabre!" He says, taking a big _claymore_ sword and going for them, bloodlust  and honor in his eyes and smile!

> + ***Fredal Oldsmell, Skunk Pirate (He/His)***
>    + Bloodlusted, but a sense of honor, a good scoundrel, but a scoundrel nevertheless
>    + _Furious, Heroic, Honest_
>        + Express your rage in a constructive manner.
>        + Declare someone fundamentally good or irredeemably evil. If anyone wants to prove you wrong, they’re going to need to spend a token.
>        + Point out the truth everyone else has been ignoring.

There was a very intense battle, chaos spreading around, the city split in two basically: _partisani_, those who supported _Conigglia_ Lineage, and _conspiratori_, the ones that wanted to topple it.

_"He's trying to run, Puffers!"_ screamed Elena, that was now running with Regina and some soldiers! And I could see the _Maestro_ Tassini running!

_"Fool, I'll never be get..."_ he said for someone. When he saw something that brought fear for him...

And to me...

And to everyone around, and everyone in the city.

Making the fight stop on a silence that screamed!

The _REAL REAL_ BUONNANDANTI!

> + **Il Buonnandanti, *Spirits of Honor dressed into Straw (They/Them)***
>    + It's said _Il Buonnandanti_ come to Talmina when things are wrong or at danger. Always on three.
>    + _Dead, Heroic, Honest, Mighty, Oracular_
>        + Send a chill down someone’s spine.
>        + Show someone something they truly don’t want to grapple with. If they want to avoid thinking about it, they’re going to need to spend a token
>        + Know what’s best for everyone else.
 >       + Present a perfect persona to the world.
>        + Point out the truth everyone else has been ignoring
>        + Move the unmovable
>        + Tell someone the bad news about what their future holds. If they want to defy you, they’ll need to spend a token.

_"We are_ Il Buonnandanti." one of them said, looking the leader, on an spectral voice like I've hear at _Castello Porcinni_, like a big yell coming to the deeps of a big cave. 

_"O,_ Buonnandanti." _Maestro_ Tassini was saying _"You know what is happening and how_ Regina Melina _betrayed_ Il Sacríssimo..."

_"Silence, fool_ Maestro. _We are not here for thou. We are looking for_ Il Ultimo Albionnere Maestro, _that was dreamed as a_ protettore _for Regina Melina Conigglia."_ said the smaller one

_"I'm the one."_ I say, getting on my knees _"Sorry to use this ruse and your name, O_ Buonnandanti!"

_"Rise_ Protettore del Regno, _protector of the Land. Since Ablution we heard about you and your sense of Justice. You are fallible as any man, but you tried your best."_ says the middle heighed of the _Buonnandanti_, getting nearby him. He just got his Silver blade and sacked it and pointed to me. _"Look to_ La Spada! _Let us see you are really the one."_ 

I felt fear... But, at the same time, I recognized the wisdom on _Il Buonnandanti_: I wasn't being judge by my courage, but _by my heart._ I look humbly and the most courageous I can, even knowing that he could go off with my head with a blink of eye. And, if got too much fear, my stink would reveal it.

_"You are the one!"_ says the _Buonnandanti_, satisfied _"You are really_ Il Ultimo Albionnere Maestro. _And that"_ he said, pointing _Maestro_ Tassini _"is the one was conspiring against_ Regina Melina Conniglia!" he says, ready to kill him

_"Please, no!"_ says _Regina_ Melina, when getting again inside the courtroom _"Please: don't draw his blood on this courtroom. It's said this would bring desolation for Talmina!"_

_"Indeed. There's no need of this,_ Buonnandanti." I say, nodding for them _"If you accept a suggestion of this one he should be branded traitor and expelled from Talmina. And he couldn't get nearby Talmina for at least 10 years."_

_"No! This means..."_ he says

_"You'll die alone, outside Talmina."_ said the _Buonnandanti "This suits you nicely, as you had grown into folly, losing your wisdom to a Folly of unifying Talmina and_ La Sacra Chiesa. _Your folly would bring Haeth to Doom! It's well suited for you to be a proscribe!"_ he says, looking for _Maestro_ Tassini _"Now, you'll be Proscribed! With my Sword I bring justice! With my Sword, You are Punished! With my Sword, You are a Proscribe! No Talminare will help you! No Talminare will assure you! No house in Talmina, no matter how low, will provide you shelter! No one that recognize Talmina as friend will provide shelter!"_ he says, touching Tassini's shoulder with his blade, with made him yell in pain, a terrible wail that could make anyone beg for pardon, if he wasn't a conspirator.

_"Now... Off you go! And beg you are not in Talmina as the sun rises, or even the Bugs of Swarming will peck you and feed on your blood!"_ said the Buonnandanti, and some of the insects and bumble that were nearby started to peck him, making him run, yelping in pain.

_"Now, for the other conspirators, O_ Ultimo Maestro, _what you suggest?"_ they say

_"They should be stripped from his stations and wealth."_ came Regina _"Their folly should not pass unscathed. However, they could stay here. But they'll lose stations and wealth, the latter splitted between Talmina and_ La Chiesa _to help the poor according_ Matrona Conigglia'_s teachings. And they should be branded conspirators. Otherwise, they should not be expelled Talmina. Their folly will be a painful teaching for Talmina, but one I believe it will be healed"_ she said

_"We hear, and we obey, O Regina_ Melina Conigglia!" says both the Buonnandanti, flicking their hands and markings showing on at least 50 nobles and other 100 people as conspirators _"Now go! And be glad Regina is so lenient with your lot! We had come to spill blood like in the past, but that wasn't needed on this day!"_ says the Buonnandanti, and they ran away, some Guards following them. _"Our service here is done, O_ Regina. _Now, we need to go back for peace!"_

_"So you can go, O_ Buonnandanti. _Your deed was done, your peace was ensured and deserved. Go and leave in peace back to your rest, and hope Talmina doesn't demand your service for a long time."_ she says, and the _Buonnandanti_ departs, leaving our reality like dreams and straw.

---

_"Rise!"_ says Regina Melina Conigglia. 

Elena was totally pretty on her new _Signora_ Dress, now _Signora Elena Ratiratta,_ his family now a noble one, she now a _Gentildonna,_ a Friend of The Queen. Otello also was reward, claimed as a _Scudiero_ for his region. Even Fredal received some pardon on his previous crimes. Sabrinna also receive a commerce ship to be Captain, _Il Ratto delle vendite_, the Seller Mouse.

_"And about you,_ Ultimo Maestro?" she said

_"I don't know. All my time here was so incredible, but also remembered my all the problems I've left behind at my country,_ Vossa Altessa. _I still needs to wander, to learn, but now I'm thinking... I had already my reward. You see, I exiled myself from Furrow Heighs, and I'm not yet ready for go back, but I'm ready to go forward, healing myself. I learned about Death and Life, and about Follies, and about Freedom and Happiness. Think I'm ready to heal myself. Maybe, I'll come back here, maybe not, but I'll proudly say I'm_ Il Ultimo Albionneri Maestro!" I say

_"Elena, the box!"_ says Regina, and Elena gives her a small wooden box. Inside it, a very nice crest:

_"This, O Wanderer, is a_ Maestro _sigil from Talmina. Whatever a Talminare is and you need help, you can claim help using this sigil. It also will provide safe haven and free transportation by any Talminare, even beyond the Hospitality traditions. But, above everything, this is a way to say we recognize you as a Talminare. Talmina will be your home, no matter what._ Il Sacríssimo _put you on my dreams,_ Gamberetto, _but not as a Sigil of Death, but as a Sigil of protection and change. Yeah, I know there's lots to improve here, and this is the bitter lesson that came from the_ conspiratori. _But now, I believe we can build a better Talmina. Rise, O_ Maestro _Puffers Underhill,_ Il Ultimo Albionneri Maestro"

I hear the claps, and I understood:

I did run from Scholarhood because I thought I was not enough, or because I was a death omen.

But not because what I've feared: putting my father and my family in shame.

Now, I proved myself to myself. I now trust the me that trusts me.

I'm ready to start healing, and becoming, in the end, a real _Maestro_.

---

_"So, are you really going?"_ said the rabbit lady in rags, speaking in common, following the mouse Chamber-lady and the otter with a _Scudiero_ suit, while looking me embarking the big ship _Il Ratto delle vendite_, with its Captain Sabrinna Scoiattolo.

_"I need to."_ I say _"I really thought on take some more time in Talmina, but heard about other places in the Northwest that sounds good. And Sabrinna is going there, it will be nice to go with a friend."_

_"You are always welcomed here, O Wanderer!"_ says the rabbit, with her sooted face hiding the fair completion of _Regina Melina Conigglia_ now under the disguise of a small servant of Her Chambermaid Elena Ratiratta.

_"Be quiet, servant!"_ says Elena, playfully, hitting the Queen lightly between her ears.

_"O, sorry,_ Signora!" she replies playing the role

_"It's needed to know the social rules, so appearances be upheld, even in front of friends. But it's tardy, and we need to go! We wish you a great luck and that_ Il Sacríssimo _helps you on finding the answers you are looking upon, O_ Maestro Gamberetto!" says Elena, curtsying, and _Regina Melina_, or better, _Celina_, doing the same, while they go away.

_"Yeah... It was a short but fun time! And what a history I'll have back_ Calabrona. _Even in Briatoli and Mugenna they'll not believe me, but a_ Lettera di Abilitazione, _a Letter of Entitlement from the Queen herself will be accepted for sure."_ says Otello, when a mink page come to him.

"Signori, _it's time to go. The guards only are going with us because they need to go to_ Forte Nortissa _and this is under our way."_ he says

_"Alright, Alberto, we are going. My wife will think it's a fairytale, but... In some way it was: Pirates, Dreams,_ Castello Porcinni, Il Buonnandanti... _Well at least there will be nice news!"_ says Otello, getting over a nice _chevrette_ with top snails and gone away too.

_"Are you ready to leave behind Talmina,_ Maestro? _We are going for some diplomatic business at Herringland, at Northwest. Nice place, educated people, but sometimes too much boring and posh."_ says Sabrinna in common.

_"That I am... Looks we'll take at least 10 days at the sea, right?"_ I ask

_"If everything is right. But this ship is fast."_ she says

_"So... let us go!"_ I say, using in my new travel clothe the sigil of a _Maestro_. _"I'm as ready as ready I can be!"_

_"And... Maybe... Some day I get back."_ I think to myself 

<!--  LocalWords:  Underhill Tillsoil Asfriteyhi Varst Ack Geroff pre
<!--  LocalWords:  spoilt Seneschal's farmworker Duchacy Heighs Heyia
<!--  LocalWords:  Hardworker Maths teached Nearsight Yay Cep mr Gack
<!--  LocalWords:  Conclaim conclaim conclaimed C'mon sensorial 'The
<!--  LocalWords:  Biggia Saltare Talmina Conigglio Whatcha Talvish è
<!--  LocalWords:  Wind' berrywine Univercittà Wanderhome Feruling da
<!--  LocalWords:  dei Mascherati Tanis Dulce dell'Aqua gamberetto tè
<!--  LocalWords:  Velina Sirenna Foresta uncared Carnevalle Zletvair
<!--  LocalWords:  Furrowian Karash pottymouth Haeth there'll Gennaro
<!--  LocalWords:  feruling Nasone Tinacci Istlevar Talpatone Maestri
<!--  LocalWords:  Univercittà' behavioured arrivist Volpinare Del'la
<!--  LocalWords:  Brighella Commedia Arte Cáspita alunni Danza Dalla
<!--  LocalWords:  mascherata Arlecchino Paollo Piva Canne Corso Il '
<!--  LocalWords:  witfull Lucca's Alunno Grata petrichor Dottore 's
<!--  LocalWords:  campalina Talmina's Batocio Abluzionne Collegiale
<!--  LocalWords:  Leone's Paolo camerata Martinno Aguila Bloommeadow
<!--  LocalWords:  Seneschalhood Castello Porcinni 's Sabrinna Rossa
<!--  LocalWords:  Scoiattolo Signori' Preccia Albionne's bloodlusted
<!--  LocalWords:  Tredici Callaleah Giorno Vulpone splitted Strada
<!--  LocalWords:  Reale Devildays Albionne cattlers Preccian Porcini
<!--  LocalWords:  principi Giacobbe Daughters' putrification Hovkar
<!--  LocalWords:  carrions imponent devoided Thristvnar Oglash Varsh
<!--  LocalWords:  Hiev Talminare Principessa Tagrash Porcinni' Vossa
<!--  LocalWords:  Studioso Maestà principe Principes Principesas tuo
<!--  LocalWords:  stablekith Albionnere Capitano geasa suceed Morte
<!--  LocalWords:  Maestro' Maledicta gettoni Albionneri follyfull te
<!--  LocalWords:  prophetised fetore Maestritura Grazie Otello righe
<!--  LocalWords:  Lontrini Preccia's Preccian's Calleleah piagli ish
<!--  LocalWords:  buonnandanti principesse Conniglia Conigglia Unita
<!--  LocalWords:  descendancy stabilished Chiesa Sacríssimo Giorni
<!--  LocalWords:  Sacríssimo' strawman Fratello' paglia insanguinata
<!--  LocalWords:  pagila Strawmen Tripla Leprione Matronna Chiesina
<!--  LocalWords:  Chiesas vicino importante quanto Leprioni Chiesini
<!--  LocalWords:  insanguinate Preccians Chiesa' Fratelli Precciani
<!--  LocalWords:  colours Inquizittori 'would Spavalderia Briatoli
<!--  LocalWords:  Mugenna Erano algurio Acidittà Cellebrazionne hoi
<!--  LocalWords:  Melina Conigglia's Salvio Rastelini Maressa simbol
<!--  LocalWords:  Apolonio Convocazione Ufficiale Porca Misera puzza
<!--  LocalWords:  suspice Vostra Altessa Precciana Precian Ratiratta
<!--  LocalWords:  Cerimonially embroidements Regina' Preceptore Uno
<!--  LocalWords:  Antonnio Tassini Porpinella Porcospina Precianna'
<!--  LocalWords:  morte Celebrazzione Buonnandantara Strawmen's 's
<!--  LocalWords:  Vecchia Matrona Furrowians heretici cleasing Starn
<!--  LocalWords:  Vecchi polloi puzzore puzzolente Callisteni skunky
<!--  LocalWords:  loathe'em Carverash Altalus Stankart shitshow mio
<!--  LocalWords:  Fredal Butcha Overpious Shaddap Stolas tripulation
<!--  LocalWords:  Savaltare Chiesa's 'common' saalan Chieserini
<!--  LocalWords:  L'Imperatore Giusticia Porta sweared gunports
<!--  LocalWords:  Fratello ressurgence Tiesto conspiration expurge
<!--  LocalWords:  heighs Milena kiths Principe' Achille's Oldsmell
<!--  LocalWords:  Thabald danse bloodlust partisani conspiratori
<!--  LocalWords:  dreamt protettore Regno heighed Spada Tassini's
<!--  LocalWords:  Gentildonna Scudiero Ratto delle vendite sooted
<!--  LocalWords:  Scholarhood Calabrona Lettera Abilitazione
<!--  LocalWords:  Nortissa Herringland
 -->
