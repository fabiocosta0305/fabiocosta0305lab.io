---
layout: contos
title: "Puffers Underhill - Chapter 3 - Bloommeadow: Castello Porcinni"
subtitle: A Wanderhome Story
category: stories
excerpt_separator: <!-- excerpt -->
---

> + Bloommeadow
>   + Time To Relax
>   + Sunny Skies
>   + Good Cheer
>   + All Kinds of Flowers
>   + Waves Of Pollen
>   + Baskets Of Ripe Fruit
>   + The Best _Polenta_ all around
> + Ruin
>   + Monuments To Arrogance
>   + The Foundations Of Something Massive
>   + The Old Folly (Folklore)
> + Cave
>   + Darkness Deeper Than You Expected
>   + Old Icons for Saints, Heroes and Martyrs
>   + The Old Praying Cave (Folklore)
> + Castle
>   + Long Shadows
>   + The Crest Of A Distant Monarch
>   + The Treason of the Old _Maestro_

<!-- excerpt -->

The river is really nice this time: but maybe this is because I'm going even more to south and we are now at Bloommeadow, no more at Monsoon.

_"Were are you going, wanderer?"_ says the captain, a big chipmunk that just accepted me for the help.

_"I don't know, to be honest..."_ I say

It passed five days since I've left Velina. Even with the happiness I've felt after the _Univercittà_'s event, they were so bittersweet, because it remembered me so much my own exile and my own desires to my old teachers.

Even without my book, and scepter from my time as Scholar, I'm still one in the heart. Maybe not totally, and totally not the same that left Furrow Heighs. But I'm still a Scholar, sometimes worried on with the ways of the world.

_"Looks like you are a wanderer without a North, right?"_ smiled the chipmunk

_"Don't talk me about North..."_ I say, when I notice on how ill-behaved I was in my gloom _"Sorry... It was..."_

_"Lots had happened, right,_ gamberetto?" says the Captain Sabrinna Scoiattolo, from _La Rosa Rossa_, the boat I'm in, getting even South.

_"Yeah... And I'm feeling that things are growing worse North, but I'm not ready to get back and see..."_ 

_"The truth? About you? Or about your family?"_ she says _"Don't worry for this. Everyone needs to find those truths, but do this on your own time, don't press yourself. Be like those flowers..."_ she says, while we look the flowers on the river's margin. _"They bloom on their time, and this and the release of the pollen shows us it's Bloommeadow time."_ he says.

It was when...

I notice at the distance what's looks like the ruins of an old Castle. Like it's calling me.

_"What can you say about that castle?"_ I say, pointing.

_"Oh!_ Castello Porcinni? _The Old Porcinni Castle? You see: people talks its cursed, since the_ Signori'_s age, when all the duchies and counties from Talmina fought each other. Porcinni is exactly under the limits of Preccia and Albionne's duchies... Not that this is so much relevant nowadays, we're a country with a nice Prime-Minister and King since Unification. But... It's said that the Old Prince Porcinni was a bloodlusted chap: some says that the stench of Death is still there, lots of people avoid it as far they can."_ says the Captain

> + ***Captain Sabrinna Scoiattolo*** (She/her)
>     + Chipmunk Captain from *La Rosa Rossa*
>     + Knows lots of stories about Talmina
>     + Adventurous, Cheerful, Wise, Oracular
>         + Declare where you’re going next.
>         + Look on the bright side.
>         + Ask: _“What are your feelings on the matter?”_
>         + Make a vague and unclear reference to something that hasn’t happened yet.

_"The Death stench..."_ I whisper _"Like a Death Omen..."_

_"Looks like you are being called there, right?"_ she says

_"Oh, no! I..."_ I try to exchange the topics

_"I can see your eyes. Had seen this before: people that goes there, to find themselves."_ she says, taking a _Tarot_ deck and, after shuffling a little, taking a card. "Il Tredici, _The Death. People fears future, because the future only bring for us Death, to be honest. But, those like me that understand Death is a friend, never fear her: even Life needs Death. Look at that Lotus there."_ she says, pointing a pretty pink lotus, that risen from what looks like a big pile of wild bumble droppings _"From Death came Life, from Life came Death. I saw your spots and I can see the shame you feel from them... Maybe you should see Death in the eyes, and for this, maybe_ Castello Porcinni, _can help you. There's nothing there anymore, not that can kill you_ physically... _But maybe there'll be some turning point for you: some change you need to do to yourself."_

_"Should I go there, so?"_ I ask

_"Maybe... Or maybe not... Let us put this way: Preccia has one of the best_ Callaleah _all around,_ Il Giorno del pasticcio, _where people can be free from their burden and be raucous. Some wanderers like to go to Preccia by foot, looking upon on a pilgrimage to prepare themselves for_ Il Pasticcio. _Maybe you can do this way. I'll leave you in our next port, Vulpone, and you go to Preccia by foot. And, if you feel you need to get to_ Castello Porcinni, _then it's up to you. But, no matter what, I believe you'll have some answers there, for good and ill."_ she says.

_"Alright... Maybe you're right."_ I say, taking some mop and a bucket of water _"So let me clear here to pay my journey."_

---

We really splitted ways at Vulpone port.

She was very kind by giving me some food and an old map of the Talmina roads: _La Strada Reale_, the King's Road, is  my way from Vulpone to Preccia. A really long way: I'll certainly take all Bloommeadow and even some Devildays in the road. With the few money I still have, I just bought a new canteen and got to the road. I just crossed some markers that shows that Vulpone finishes and I got into Albionne.

I could see big fields of bumble cattlers and wheat crops. People works the crops with care, sometimes waving hands for me, which I replied. Never needed to sleep at wild: every night at least there was a nice tree or a spot on stable to sleep that a kind farmer could provide me and, no matter how poor they are, they already had _polenta_, a kind of creamy cornmeal with cheese and butter, slices of spicy bumble sausage, wheat bread and sweet grapes juice, to share with a traveler that could pay back on news and stories.

And boy, what a delicious food it was. I even took my new travelogue and annoted this recipe to bring with me this taste from Talmina. Even got some travel pots and corn flour for doing my own _polenta_ from a very very kind old lady, the last stop for me before getting nearby Porcinni Castle (+1 Token: 1)

_"If you are going the_ Castello Porcinni, _avoid for all you can go into the old_ Maestro _room: people says it's cursed by the gods, from the arrogance of old_ Principe Porcinni. _He was a slob, glutton fool that, in his arrogance, declared war and tried to unify Talmina under his rule. His intent was in fact somewhat succeeded, but not like we wanted: when Preccian forces, with the support from the other_ principi _from Talmina came, they decimated the old Duchy, salted his land, and finished all the Porcinni, their blood flowed to the ground and even nowadays stenches the air with death. Only her daughters, Amelia and Jocasta, got to a cave and disappeared. Some says the old gods spirited them away as a punishment for Old_ Principe _Giacobbe Porcini, so he wouldn't find them in the afterlife. Some says that they protected the girls from the wrath of the prince, after their betrayal. Some even say that, in fact, they just ran away with some of the_ principi _that destroyed Porcinni to live Happily Ever After."_ said the woman _"Oh, sorry, I'm a big chatterbox."_

_"Oh, no!"_ I say _"I love this kind of stories, even note them: since I've left my home and my station as Seneschal, this kind of story became a great source of information. People lost on how much you can learn from old stories."_

_"You're kind,_ gamberetto." she replies _"It's said that no one had broken the_ Castello Porcinni _curse and freed the Porcinni Daughters' souls. On full moon nights, people says you can hear, and sometimes even see, the old battles, the banners and swords, the spears and crowns, and even yell Porcinni enraged voice!"_

_"Hum... Sounds like something interesting."_ I say

Then I got to the bifurcation on _La Strada Reale_: forward, a neat road to Preccia, but turning left, an old road, full on yellow and pink _Inamorata_ flowers, beautiful flowers that rises only in Talmina as far I know.

But even the flowers can't ride, with all the smell and pollen that made me mask my snout with a mask, the stench of death: a stench of putrefication that even me, as a skunk, so accomplished with bad smells, can deal without cough.

"Hovkar! _What a terrible stench! Even the flowers can't rid it! I didn't believe this would be possible! This can't be natural, for sure!"_ I say to myself and, without thinking to not falter on my intention, got left, to the _Castello Porcinni_

The walk is pushy, but uneventful, except by the stench: lots of dead bumbles and other insects brings the scavenger insects and bugs, like carrions and flies, which only turn the smell and vision worse. There's a river, but the water doesn't look that good to be drank upon: it's barren and with a weird, sinister crimson look. And even it smelt bad from dead fish!

_"Yuck... Better go forward as soon I can!"_ I say _"Lucky I still have some provisions if needed."_

I look to the distance, and see the ruins of _Castello Porcinni_, and now I can see their folly: the _Castello_ is really big, but right in the middle of that meadow. The open skies only make the imponent decadence look of the Castle even more oppressive, some Deep Purple and Soft Pink flowers all around, a very creepy look, like they lived by drinking the dead's blood. And looking around, I can see some of the skeletons of the fallen: finely dressed, neatly armed, but so hollow in their now clean bones, devoided from everything that could be said as life!

"Thristvnar!" I say, sad, looking on how many fell thanks a folly, no matter their side. _"And even the flowers can't hide the sadness and tragedy..."_

I say when I notice the sun had fell and Moon risen, full and bright in the sky!

And I started to hear some sounds in the wind, the pollen looking like forming waves that formed shapes, vaguely looking like people armed and fighting each other

"Varst!" I think _"It's the curse!"_

I can see the Battle! And hear orders: whispers of yells from the past. And a very arrogant one, when I looked to the biggest tower in the _Castello_.

The shape of a neatly dressed and armored pig, but screaming order with an arrogance and folly I've never saw before

_"Fools! We are_ Il Porcinni! _We'll be rulers of Talmina! Go, fools! Kill them all: even the wounded! Even the cowards! Their bodies will fertilize our lands!"_ he screamed, on a kind of yell that came to me so many many years, like a whisper that wind wouldn't dust away, to remember everyone that folly!

I chose what looks like a folly itself, but I couldn't think on something better.

I got to the _Castello_ to find a spot for the night.

---

The _Castello_ has a big sturdy construction, and this is the only reason it didn't got into dust now. I can see all around the draping of old banners, eaten by termites. The old kitchens, full of putrefied food and stuff that wasn't sacked out. The old dance halls are destroyed and almost everything useful, sacked.

_"Looks like there was really lots of rage there..."_ I say _"Even the flowers can't hide it. But why? I know the_ Porcinni _engaged into war, but here... Looks that what happened wasn't just war: it was a massacre, a generalized manslaughter!"_ I gasp, surprised. And then I remembered what the old lady said...

_"Maybe... Even with the curse, the_ Maestro _room could help understand what happened here."_ I say, and I thought heard a small voice, like an old nymph, maybe an old god of remembrance.

_"Looks like something wants this mystery to be solved!"_ I say to myself. And then I found it.

_"Looks that, for some reason, people didn't wanted to get there."_ I say _"It's sealed, by... something like bumble wax: people could release this seal if they wanted. But they didin't wanted... For some reason."_ I think _"Should I?"_

I chose to take some rest, and eat a little, in front of the door... Curiously that the door, even being of wood, hadn't the same fate of almost everything else, being ramsacked and destroyed, quite the contrary, looking as pretty as was in the past, even with the _Porcinni_ heraldry crest engraved left intact, aside the painting that lost some of its glory. But this sounds also really sad... (+1 Token: 2)

_"Looks like it was an old bad omen... Something terrible happened behind those doors."_ I think to myself. I look around, trying to see if there's anything that could help into my decision.

It was when, from the door itself, some of the paint and wood crust fell, with the name of Porcinni family... Like it was a portent of small gods that needed this to be revealed. Like a secret needed to be revealed now. (-1 Token: 1)

_"Looks that, whatever secret was hidden behind those doors, it's time to be revealed, or at least is what is looks like the gods want. Like... Lifting the curse?"_ I say, lifting and, running into my bag, finding a salve of wax remover.

The door is not that big, so I can, with some effort, pass the remover on the wax at the door, and in some instants it's dissolves into a weirdly, poop-colored, goo. I then push the door and open it.

The smell of putrefication hits me straight in the nose like I do when I stink someone with my tail.

"Oglash! _What an awful smell! Even my stink doesn't smell that bad, and I'm quite a stinky skunk when needed!"_ I state to myself, somewhat proud on my stench.

I look around the room: everything in the racks is well preserved, as long Time can preserves. But there was signs of fight, from the stripped and ripped banners, to the mummified bodies in the ground, one of them with a crown over it's head and a spear through it's chest, the tattered clothes looking like splendorous in the past, the snout showing a surprise that someone that was betrayed! On a table, another body, from a fellow skunk, has his head over something, a silver goblet in the ground like felt after being drank from its contents, his hand still holding a pen, even after Death's grasps getting them! On his tattered clothes, a small crest in the side closing his cape

"Varsh!" I say _"Looks like this is where_ Principe Porcinni _died... And... That crest... Looks like from some order of scholars or sages, the pen and the book on it can show this."_ I say. I do a small pray, to placate his soul and asking for a nice sleep in his death, and after some protective charms, I put some cloth on my hands and gently pull the old guy body aside to the ground. And then I found what looks like an old journal, surprisingly on a pretty good, almost mint, state.

"Hiev! _That's incredible!"_ I say, gently cleaning the diary _"Looks like he protected this with his own life... But what happened here? Looks like_ Principe Porcinni _was killed by one of his own. And what happened with the Preceptor? Killed himself? Maybe there's some answers on the diary"_ I say, taking the diary and looking for a corner were I could read, but only after opening some windows, the dead miasma making me feel uneasy _"So this is how people feels when I skunk them?"_ I say to myself.

On a corner, I start to read the diary, pushing straight to the last half of it. Need to say my Talminare isn't exactly sharp, and the flourished letter and some old idioms make me get some headache trying to understand it, but with two hours of reading, I can understand enough.

_"So... After the Old_ Principessa Porcinni _was killed during an Attack, Prince Porcinni, in rage and sadness, ordered an all-out attack, even counting the populace as troops! This would be an all-out pointless genocide, conclaimed by his own leader!_ Tagrash Varsh! _And then..."_ I read some more _"Some soldiers and the_ Maestro _brought him here to the_ Maestro _room, after the_ Maestro _saying the Prince's daughters Amelia and Jocasta to hid into the Praying Cave. And some men, loyal to the country, not exactly to the Prince, betrayed the Prince and a battle was instated, and the Prince was killed by them. But all the men were killed, and, knowing the price of his treason, the old_ Maestro _finished to register this, just a little after taking a slow poison that would poison himself to dead."_ I think loudly, closing the journal

_"So, this was the_ Principe Porcinni'_s folly, doesn't understanding the responsibilities of his station..."_ I say to myself _"He grew so haughty of yourself... So arrogantly full of himself... He put everyone and everything else on danger, on the brink of destruction... By a folly!"_ I state for myself

_"You're right,_ gamberetto!" I heard a whispered yell

And then... I looked, and saw a image... Over the Old _Principe_ Porcini's body!

It was him! 

_Principe_ Giacobbe Porcinni! 

His ghost was... There!

> + **Principe *Giacobbe Porcinni*, *Ghost* Principe *for* Castello Porcinni** (He/Him)
>     + Pig Ghost 
>     + Former _Prince_ of _Castello Porcinni_ and Albionne Duchy, now only a ghost from the past (literally)
>     + _Dead, Empty, Royal, Proper_
>         + Send a chill down someone’s spine.
>         + Sigh and gaze blankly.
>         + Make a sweeping proclamation. If someone wants to openly defy you, they’re going to need to spend a token.
>         + Explain how things have been handled in the past.

_"So... You're a_ Gamberetto Studioso. _Not that this matter anymore."_ I hear him say

_"Are you... Really..._ Vossa Maestà, _Giacobbe Porcinni?"_ I say, doing the best curtsy I could under the pressure, astonished and fearfully: had heard from the old woman in Furrow Heighs about the ghosts and banshees and how they could curse you.

He smiled _"Even under fear, you demonstrate education... This is admirable. But yeah, I'm what I am. Giacobbe Porcinni. In the past a powerful_ principe _on the Talmina. Now... I'm just a ghost!"_ he says

_"Because of your folly?"_ I say, before I could control myself

_"KNELL! Kneel in front of your superiors,_ gamberetto!" he demanded, and I got myself shivering in fear, my tail almost losing his control and spraying all around, when I knelt in front of the royal ghost, like commanded by a powerful master

_"You were the first one that had the nerve to undo the treason of my_ Maestro. _This can be a sign that the curse the_ Principes _and_ Principesses _placed under Albionne is to end. My duchy was cursed to be always a desolation, until my Royal Daughters would be found! Me, my spouse, even that traitor of_ Maestro _would stay here, splitted apart, but visible for each other, only saw for the living in the First Full Moon of Bloommeadow, unable to Go Forward. The flowers would always remember the blood drawn in soil, and the rivers would have the stench and taste of kith carrion."_ He states _"Why did you that_ gamberetto? _Didn't know the legends?"_

_"Someone said me I would learn about Life and Death here."_ I say, in fear enough to raise my head to look him. _"Even as a ghost he is as arrogant as in life!"_ I think to myself

_"Rise!"_ he commanded, on a regal voice.

_"I have a Command for thee. A Command from a_ Principe, _even a deceased one. Do you understand how important it is? You can be accursed as forsworn if negate my command!"_ he says, on an arrogant but hopeful smile.

_"Yes, Your Highness!"_ I confirm, without willpower to avoid it!

_"First of all, strip that traitor from his cloak and effects and tokens as_ Maestro. _Those I solemnly grant for thee! May they never fail you on service, may they never let you without assistance, may they bring you your answers."_ he says, as an order... Something on his supernatural presence making me do his will like a puppet!!! 

But... At the same time... I notice that his arrogance was not totally part of a folly: he was cunning enough, regal enough, proper enough, to be obeyed! Even in Death, even accursed, he still was regal enough to make me, an almost confirmed Seneschal from Furrow Heighs, obey him like a stablekith! _"Why would him go on this folly, if he looks powerful and royal enough to be obeyed even after death!"_ I think while I go for the _Maestro_ body and strip him from his cloak and tokens as a _Maestro_, basically ransacking his body! (+1 Token: 2)

He smiled _"Now, as the Last Albionnere_ Maestro, _I Command Thee on a Last Order from me as King. Perhaps you know about my Daughters, the light of my eyes! My Folly was so big that, in my rage, lost them. Their betrayal, by not being aside me on death... It was even painful than the one_ Maestro _did with_ Capitano, _and that brought me to death. I'm still here thanks the betrayal of my_ Maestro _and from my Daughters. Now... I Command thee to find my Daughters. I want them to pardon this old father's Folly! This is the only way for me to Go Forward. And this is the only way for_ you _now to go away from Albionne! Fail, and you die once you put a tad finger beyond Albionne's frontier! Fail, and your name will be forfeit away! Fail, and you'll share our Doom!"_ 

He Commanded... 

And I felt! 

I was cursed now! 

A _geasa_!

A supernatural command, not only to try, but to _succeed!_

_"Now, go! You have till the Sunrise to suceed! Otherwise, you'll die, and I will forever be locked here, with my spouse, and daughters, and_ you!" he states, on a voice tone... That made me run!

---

Now, I have only some hours to find where the Daughters got, otherwise I would die and stay forever as a ghost here!

Maybe... Thinking right, he was arrogant, alright, but sounds like he was a good father after all. Because, if I remember right the legend, they got into a cave nearby... And the _Maestro_ journal said they got into the Praying Cave.

So, all I need to do is to find this Praying Cave...

... and their ghosts? If he died, perhaps them too...

... or they were spirited away by the old gods from Albionne as a protection?

No matter what, I had to succeed, otherwise I would die!

I got nearby looking, and found some rotten plaques showing a way for the _Cave of Prayers_!

I followed it, and noticed that the moon was already starting to fall. I need to go fast...

But... For some reason, I felt the gods of Albionne are looking for me: perhaps it was all the nice fireflies showing me the way, or the moon light showing a nice road, the only kind of beauty here that wasn't to oppress. 

Whatever... I need to give a look so I follow the fireflies.

The Praying Cave was in shambles as almost everything around. However, it wasn't sacked: everything, aside the deterioration brought by Time, was right where it should be as it should be, all the icons from heroes and saints and martyrs and gods. 

Doing a small reverence I get into the cave. I just look around and decide to pray for the local gods, kneeling in front of the Altar, with an Icon from, probably, the Old Albionne God.

_"I'm here in the name of the Last Albionnere_ Principe, _as his Last_ Maestro, _to Correct a Fault, and to Provide Peace for this people that suffered so much. I was Commanded to look upon for the Lost Daughters. I need to find them, so they can pardon Their Father. I know: by what I've heard, he was, pardon my language, a pain in the ass. But, even arrogant, I can't see him as a terrible father. And he accepted the consequences and pain of his folly. And now, he Commanded me to find her Daughters, so they can Go Forward with all the Albionnere people. O, Old Albionneri Gods, Saints, Martyrs and Heroes! I pray for thee, asking a Last Favor, to bring peace to your people and cleanse the curse they suffered, so even you can go and sleep in peace! Hear this pray from this Wanderer, that wants to help!"_ I say

I feel the silence for a while, and thought I've busted my chance, when I feel the lights growing, filling the Cave and I saw the spirits of two really pretty girls, a pig and a macaw.

"Papa _had really Demanded on thou, O, Wanderer?"_ said the pig one, that looked the elder, although looked no older than eighteen. The macaw one looked even younger, almost ten.

_"Yes, that he did, after I opened the_ Maestro'_s room."_ I say

_"You broke the seal of the_ Maestro?" wowed the Macaw _"The_ Maestro _sealed the room this to avoid my father's spirit to become a monster that went around, destroying and killing everyone in death as he did in life!"_ 

_"I..."_ I dawned myself, on the folly _I've_ did _"I'm sorry... maybe I was the one under a folly, looking for answers about Life and Death."_

_"You have a_ Memento de Morte _on your tail. Those are a symbol of renewing, as Death itself, as_ Il Tredici. _Maybe it came time for Albionne to cease to exist forever. No curse, no blessing, only peace."_ says the elder _"So, came and speak, O Wanderer. I'm Amelia Porcinni, daughter and_ Principessa _of Giacobbe Porcinni and this is my sister Jocasta!"_ Commanded me like his father did previously

> + ***Amelia and Jocasta Porcinni*, *Ghost Albionnere* Principesse** (both She/Her)
>     + Pig and Macaw Ghosts
>     + Former _Principesse_ of Albionne Duchy, now only ghosts from the past (literally)
>     + _Dead, Royal, Proper_
>         + Send a chill down someone’s spine.
>         + Make a sweeping proclamation. If someone wants to openly defy you, they’re going to need to spend a token.
>         + Explain how things have been handled in the past.

I go and tell everything to them, since the boat at river, to when his father Commanded me.

_"You had a pretty Adventure, O Wanderer!"_ said the macaw one, that I know is Jocasta _"Maybe, the_ Fortuna _threw their dice to put you on our way. And perhaps not a moment too soon. Now... It's time to lift the_ Maledicta di Albionne, _this curse that fell on Albionne. However, we both Command thee: as soon everything finish, You will set_ Castello Porcinni _into fire! You shall not speak about what happened here till the Bright Season ends. You shall not take anything from_ Castello _aside the journal and the all_ gettoni dei Maestri _our Royal Father gave you! You Are the Last_ Albionneri Maestro, _this is your reward and Honor!"_ she Commanded me.

_"So... Are you coming with me?"_ I say, somewhat happy

_"The time came for us to pardon our father. He was arrogant. He made a folly that resulted on all this destruction and death, but time came to cleanse all this, and let people get into Albionne once again, and use his soil to grow something new. Now, let us go!"_ the Elder One demanded. The Small Gods of Albionne, and their Saints and Martyrs and Heroes agreed.

---

The sun was rising when I got back to the _Castello_, and I could see in the _Campanile_ the ghost of _Principe_ Porcinni. He saw us and got down as fast as he could. He basically ignored me and hugged their daughters, and spoke in Talminare:

_"Sorry, my dear Daughters, my precious ones, my dear ones, being a so terrible father! The death and suffering you and all Albionneri had was my and my only fault and folly! Only my hand are bloodstained under the Gods!"_ he cried ghostly pearly cries while that.

_"We also had our folly, O Royal Father, to command the_ Maestro _to conspire and topple you. It was our folly: we wanted a very good father, but we never understood that love can also be a source and reason of ill and pain and folly. We had the best father, even a failable one. We should had stopped you before, and not like we've done!"_ they cried

And then I started to see ghosts risen all around, on the many miles nearby _Castello Porcinni_: the serfs, the fighters, the enemies, everyone and anyone ones that died on this cursed battleground. 

And the Queen, a pretty swan lady.

_"My Heart and Joy! My Dear One! I'm so ashamed on the Folly I've did!"_ he said

_"You understood, at last, my Dear One, and not a moment too soon. Now is the prophetised day, when a_ gamberetto _smelled something worst than his_ fetore. _The stench of dead will be soon disappear from this place."_

And, then, came the _Maestro_, kneeling in front of him

_"I'm sorry for all this,_ Signore, _but it was the despair of seeing the ruin coming that made me betray you. It was my own folly, to betray you, and just now, as everyone, I'm free to Go Forward after all those follies are cleaning. Think_ Il Ultimo Maestro _can Command us to Go Forward."_

They gave me this responsibility! (-1 Token:0) To free them from they struggle and sins! But ...

I was never a preacher or something like that, but now... Looking around, I could see.

I'm _Albionnere Ultimo Maestro!_ The Last Albionne Sage!

I looked forward, putted myself on my best posture and, while the sun starts to rise, I proclaimed on my best Seneschal voice and tone

_"I, Puffers Underhill_ Il Albionnere Ultimo Maestro, _Command Ye! You are free now! Your follies are no more! This land is free now! The_ Principe'_s Folly, and all the others, are now cleansed! Albionne are now free to leave existence and be forgot and be now only land for a new time, for new kith! Albionne will fall into oblivion today, here and now, and not a moment too soon! You can depart! You have now Peace and can Sleep! I'll destroy_ Castello Porcinni, _and nothing will be spoken on what happened here till this Season of Bright changes! Albionne will only exists in the legends and in my_ Maestritura!" I claim to the skies and to the ghosts

And then... 

While the sun risen, I could see the ghosts, smiling, starting to dissolve, like fireflies... Like pollen, like dandelion's seeds spread through the air....

And I can't feel sadness, quite the contrary.

When Prince Porcinni, and his Daughters, and the Queen, and the _Maestro_ were finishing to disappear, they said, on an almost inaudible voice, a whisper that came from time and death.

"Grazie, gamberetto! Grazie, Il Ultimo Albionneri Maestro. _Now you is free to go your own way too."_ they said

I know that I could, but there was a last command to be done before I could go...

---

It was six days since then, and I'm far enough from _Castello Porcinni_ to see him in details. 

But the flames I still can see.

I found there was lots of inflammable stuff on the deposits of _Castello_. There was some stuff also, but nothing usable. And even if I found, I would not take anything there. I just made a makeshift wick and wait some until the Castle starts to burn, explode, implode and collapse. Looks like the material would reduce the castle to ashes.

I looked around and felt a weird thing: there was no more the stench of death, that terrible stink that even I as a skunk had some trouble to cope with. I could drink the water from the river, and could get some berries from bushes, as all the stench and signals of death just vanished away, a final act of cleansing by Albionne's gods. (+1 Token: 1)

I looked to my bags and noticed I, even after all the berries I've collected, was now really short on provisions.

_"Hey, guy!"_ heard someone coming from the other way, from East. An otter farmer on a wagon pushed by some big snails full of wheat bundles and sacks to be sold.

_"Oh, hello!"_ I say, and I then notice that I didn't took away the tokens of a _Maestro_ from _Albionne_

_"Had you got into_ Castello Porcinni? _Looks like something weird happened there: after centuries the castle just... Got fire and got destroyed! Just like... That... People can see this all around Talmina! It's a commotion and no one know what happened to make this happen!"_ said the goose guy. 

_"I got nearby the Castello, but don't know anything about this!"_ I lied

_"I see... Better take this_ Maestro _stuff off. People could think finding those Albionne stuff an ill charm. I don't know how you got this, and this is not my problem, but better safe than sorry."_ he says, while I take away the cloak and sigil as _Ultimo Albionnere Maestro_  _"Where are you going to?"_

_"I don't know... Think Preccia is nearby, right?"_ I say

_"Somewhat. We are at the duchy frontier, but there's still five days at least till city! And Bloommeadow got down fast this year, right? Devildays just started."_ he says _"Want a ride? Better two than one, as with_ Pasticcio _getting nearby, highwaymen also came to the roads to steal and ransack all they can!"_

_"Yeah, this sounds good. But I have no money or stuff, and perhaps my stories are boring."_ I say

_"Any history, no matter how boring, is better than a silent road, believe me! Just hop in and let us go to Preccia!"_ he says, and I got the ride

_"Alright... I'm a Wanderer, Puffers Underhill."_ I say, hopping to the wagon

_"I'm Otello Lontrini! I'm a farmer looking for some money on Preccia's market."_ he says, conducting the snails down to Preccian's Valley

And the _Castello_ still burns even when I can't see the flames anymore, leaving Albionne into past.

<!--  LocalWords:  Underhill Tillsoil Asfriteyhi Varst Ack Geroff pre
<!--  LocalWords:  spoilt Seneschal's farmworker Duchacy Heighs Heyia
<!--  LocalWords:  Hardworker Maths teached Nearsight Yay Cep mr Gack
<!--  LocalWords:  Conclaim conclaim conclaimed C'mon sensorial 'The
<!--  LocalWords:  Biggia Saltare Talmina Conigglio Whatcha Talvish
<!--  LocalWords:  Wind' berrywine Univercittà Wanderhome Feruling da
<!--  LocalWords:  dei Mascherati Tanis Dulce dell'Aqua gamberetto tè
<!--  LocalWords:  Velina Sirenna Foresta uncared Carnevalle Zletvair
<!--  LocalWords:  Furrowian Karash pottymouth Haeth there'll Gennaro
<!--  LocalWords:  feruling Nasone Tinacci Istlevar Talpatone Maestri
<!--  LocalWords:  Univercittà' behavioured arrivist Volpinare Del'la
<!--  LocalWords:  Brighella Commedia Arte Cáspita alunni Danza Dalla
<!--  LocalWords:  mascherata Arlecchino Paollo Piva Canne Corso Il
<!--  LocalWords:  witfull Lucca's Alunno Grata petrichor Dottore 's
<!--  LocalWords:  campalina Talmina's Batocio Abluzionne Collegiale
<!--  LocalWords:  Leone's Paolo camerata Martinno Aguila Bloommeadow
<!--  LocalWords:  Seneschalhood Castello Porcinni 's Sabrinna Rossa
<!--  LocalWords:  Scoiattolo Signori' Preccia Albionne's bloodlusted
<!--  LocalWords:  Tredici Callaleah Giorno Vulpone splitted Strada
<!--  LocalWords:  Reale Devildays Albionne cattlers Preccian Porcini
<!--  LocalWords:  principi Giacobbe Daughters' putrification Hovkar
<!--  LocalWords:  carrions imponent devoided Thristvnar Oglash Varsh
<!--  LocalWords:  Hiev Talminare Principessa Tagrash Porcinni' Vossa
<!--  LocalWords:  Studioso Maestà principe Principes Principesas
<!--  LocalWords:  stablekith Albionnere Capitano geasa suceed Morte
<!--  LocalWords:  Maestro' Maledicta gettoni Albionneri follyfull
<!--  LocalWords:  prophetised fetore Maestritura Grazie Otello
<!--  LocalWords:  Lontrini Preccia's Preccian's
 -->
