---
layout: contos
title: "Puffers Underhill - Chapter 7 - Firetop: Althusia"
subtitle: A Wanderhome Story
category: stories
excerpt_separator: <!-- excerpt -->
---

+ Metropolis, Workshop, University

> + Firetop
>     + Brilliantly Colorful Leaves
>     + Crisp Evening Air
>     + Hearty Food
>     + A Festival Celebrating The Harvest
>     + A Jolly Good Time
>     + Musicians Practicing
>     + Mulled Cider
> + Garden (Fruits and Veggies)
>     + Describe the diversity and variety of people.
>     +  Show conflict between differing needs and worldviews.
>     +  Tell someone they’re lost, and give them a token.
>     +  Tall Ramshackle Apartments
>     +  The Enclave Of A Community You’re Proud To Consider Yourself Part Of
>     + The Giant Scorpions In The Sewer (Folklore)
> + Workshop
>     + Describe the process of creation.
>     + Show conflict between expectations and demand.
>     + Give someone a token if they work hard and sweat over their creation.
>     + An Important Supply Line
>     + A _Cunning_ Kid Too Young To Be Working
>     + The Crafter Who Made Herself A Bride (Folklore)
> + Univercity
>     + Describe the vast amount of knowledge still left to learn.
>     +  Show conflict between differing philosophies or viewpoints.
>     +  Ask: “Do you want to learn a difficult or painful truth?” Give them a token if they listen.
>     + Statues Of The Founders In All Their Glory
>     + A _Witchy_ Professor Whom You Once Called A Mentor
>     + That One Prank That Got Far Too Out Of Hand (Folklore)


*"Look out!"* someone says in the ground!

*"I need some help!"* I scream from my Balloon, *The Smelly Friend*

People started to come for help: my first solo travel can easily become my last one!

I start to fall fast: looks like I opened too much fast the top. I look and see... The parachute didn't opened right!

I hold to myself for the impact, and it came... Strong, rough... But I'm on a single piece and still not stinking

"VARST!!!" I scream. 

"Senõr, *first time flying on a balloon? Interesting name...* Amigo Apestoso." says a small calf kid helping me out of the balloon. _"You were either really skilled or lucky. Probably both. Better balooners had died this way. Anyway... Welcome to Althalus: you arrived at Althusia, on Levante"_ he says, helping me out of the Balloon *"I'm Toni Berrotin"*

> + ***Antonio* "Toni" *Berrotin (He/Him)***
>     + Calf Stablekith on Althusian Balloonport
>     + Young and confidant Stablekith
>     + _Cunning, Inquisitive, Raucous_
>         + Get somewhere you’re not supposed to be
>         + Focus on an irrelevant detail
>         + Ask: “What’s this?”
>         + Find the fun in a dull task.
>         + Get lost in the excitement.

*"Puffers Underhill."* I say, then I notice *"Althusia! The Althalus' center of knowledge!*

*"Yes"* he says looking for the big institutions nearby. *"We are a big metropolis. Lots of nice stuff came from here. Not like the places you got along... Talmina, Harringland, Vaer... What a travel you did"* he says

*"I bought this from a friend. Now I'm just a traveler alone again."* I say

*"Now you can have lots of friends. Althusia is a center of knowledge. So sages from all around Haeth came here, even from the other side of the Sea, on Tahotlan and Estela"* says the boy, while some people get the basket right and take my stuff from inside. *"Pretty rich stuff..."* He says

*"Only the wage from some adventures I had.""* I say *"By now... No money aside histories."* I say.

*"!TONI! I've already said you to not get into the Balloonport!"* says a severe but likeable voice

*"But,* Papá, *This skunk would be in problem! His Balloon parachute didn't opened right."* says Toni

*"Alright,* hijo, pero *you should be studying on the* Escuela, *not doing jobs on the balloonport. Sorry my son, foreigner. He likes the new stories from oher places. I'm Ernesto Berrotin."*

*"You have a great son,* Señor *Berrotin. I'm just a wanderer from North looking around stuff on the world. And you have a very good kid. Inquisitive and sensitive, this is a very good combination"*

*"I know... But I want him to have a better life than me. Balloonport stuff is very hard for a* becerro *like him. Please, come with me, looks you had a very harsh flight."*

*"Indeed I had... After some good start fly, I got some strong bouts of wind on the Channel, and lost my balloon control."* I say. *"Hope it's not destroyed. Paid big money for him and have some story on it"*

*"Looks like it's not. He got a big damage for sure, but it will survive: old sturdy stuff, not those new balloons that could be destroyed by a slingshoted rock."* stated Ernesto... *"Now, better we go inside. The guys will get and repair it. We have some* paella *ready in the fire. We can eat while you talk about your story."*

*"It will be an honor."* I say

> + ***Ernesto Berrotin (He/Him)***
>     + Experienced Stablekith on Althusian Balloonport
>     + Old and Wise Stablekith, wants for his kid what he didn't had: a nice chance.
>     + _Honest, Wise, Sturdy_
>         + Exert yourself to protect someone else
>         + Lay out the facts, as you see it.
>         + Ask: _“Do you want my opinion?”_
>         + Reflect on what someone else has said.

While we walk, I can see my balloon being sent to a repair shop nearby on the port. I can see lots of balloons with empty, full and in-between envelopes.

*"This port is really busy: as we are the last one in the continent for those going to Ferretington, there always lots of people around. Even more now that Harringland and Vaer relations had gone better after the Trisquel recovery, lots of Seneschals, Wise men and so are looking to go there and give a look on the Trisquel."* he says and he notice my clothes, including the sigils of my *Magestritura* 

*"!Dio Mio!"* he says *"But... Are you the one that helped with the Trisquel? The* sabio *that crossed Preccia and then helped on Ottershire? Everyone is gossiping about you all around the* citad *and in the University they were arguing if this is all true!"*

*"Indeed I am..."* I whisper *"But can we uphold this for awhile? I don't want all those lights over me."* I say while hiding my sigils. I want and need some privacy with all those gossip around 

*"For sure!"* says Ernesto *"I can give you some clothes to hide you. In meanwhile, I want to hear all those histories: the* Marcia dei Mascheratti, *the fall of* Castelo Porcinni, *the* Buonnandanti, *and now the Trisquel! So many things from before the time of war being healed!"*

I go then with him

---

I take the next day looking for some emergency stuff: first of all, simpler clothes that can help me mingle on the Althusian people. Not that was that easy... Not too many skunks there.

*Althusian population is very cosmopolitan, thanks the University and the Balloonport: our port is the best balloon builder and repairer all around Haeth, although the Harringlanders and the ones from* Brisante, *on the  continent at the other side of the Blue Sea, can say otherwise. Also, there's lots of pieces of knowledge we salvaged during the wars with* Tejo *and* Maquis, *in the ages of wars. You should give a look otherwise.*

*Perhaps I should...* I say, when I see someone...

A Seneschal from Furrow Heighs! And... I recognized that face...

"Meister *Durkhan!"* I shout to him

The man looks to me... His eyes are pretty different from what I remembered. He was severe, but fair and intense. Now... They are... 

Sad.

> + ***Haulph Durkhan (He/Him)***
>     + Previously Furrow Heighs *Meister*, now lost in the world
>     + One broken spirit now around the world
>     + _Wise, Venerable, Lost, Hurt, Empty, Witchy_
>         + Express the disconnect between yourself and the world around you
>         + Re-open an old wound
>         + Ask: _“Does it matter?”_
>         + Cackle
>         + Reflect on what someone else has said
>         + Tell someone how they will repeat the mistakes of the past. If they want to prove you wrong, they’re going to need to spend a token.

*"You... The Death Omen!""* he says, seeing me *"The accursed kith Varg Underhill should left behind in the ice!*

*"Yeah,* Meister. *Nice to see you again."* I spat. *"What are you doing so afar Furrow Heighs?"*

*"Heard about the codswallop about a skunk helping to find the Vaerian Trisquel at Ottershire."* He says *"Looks like you had a good time gallivating around, Death Omen."*

*"Well... I was to be expelled by you. But I can show you that my* Maestry *is now confirmed, thank you and those people from north for nothing."* I say, taking and show him the symbols I've hidden for too long *"Albionne, Talmina, Harringland, Vaer... All of them recognized the value Furrow Heighs Seneschal never recognized."*

*"Balderdash: a kith like you? A stupid and a Death Omen? Saving Preccia? Recovering the Trisquel? This is one of the biggest Balderdashs I've ever heard, bigger than the* Srón an Bhanda!" he cackle

I lost my temper

*"Old fart: I had been saw on* Regina Coniggla's *dreams, and had found* The Winter Winds *at the* Srón an Bhanda. *Now, you saw the symbols. I got them from those of right:* Principe Porcinni's *ghost, Regina Connigla, Queen Minerva Rabbiton II, Queen Ewe Mac Donaghal from Vaer. If you can't believe your eyes, it's sad but looks like Furrow Heighs lost its wisdom!* I spat, before I can contain myself (+1 Token - Speak your true feelings on a subject - Tokens: 1)

He looks to me with a kind of cold rage and I see that I crossed some lines.

*"Like your father, cursed be him, you are impudent and arrogant! Anyone can get nice trinkets after helping people, but real wisdom is really earned by sacrifice. Like we do in Furrow Heighs. Thankfully your father was full of folly and paid for it. And you'll follow his steps and soon will repent being so full of yourself only because was lucky!"* He states 

"Gristager Varst *I'll, Old fart!"* (-1 Token: paying to prove he's wrong - Tokens: 0) *"I had made my Ablution and destroyed my book from my time at Furrow Heigh! I found the problem: you are so full of yourselves that forgot there was a world outside! Thankfully, I noticed the folly before I became like you, an old sorrow, sad, bitter and full of himself kith"* I spat again, when some soldiers came to us

*"Any problem,* Maestros?" say a big bison to us.

*"Nothing, sir."* I say, while looking to the old *Meister*, that now looks pitiful for me. *"It was just some kind words I got for* Meister *Durkhan! I'm going now, if you excuse me"*


---

I got into the Berrotin's house, a small, sturdy rock house from before the war times at Haeth. They serve me the *paella* with rice and some bumble meat and seafood and it's really delicious, but I feel a little bitter and then I talk with *Señor* Berrotin with it

*"So... This* Meister *from Furrow Heighs was a mentor of you when you were a Seneschal?*

*"Right..."* I say *"Lost the count of the times he flogged me. And about the Death Omen, they said because some of the spots of my tail."* 

*"I can see."* said Señora Berrotin

> + ***Magdalena Berrotin (She/Her)***
>     + Ernesto's Wife, Toni's Mother
>     + With gypsy blood
>     + _Witchy, Oracular, Caring_
>         + Mix assorted components to create something new.
>         + Tell someone the bad news about what their future holds. If they want to defy you, they’ll need to spend a token.
>         + Ask: _“What do you need right now?”_

*"What?"* I ask

*"The Death Omen, the spots on your tail, are real! The death follows you, and the death is your best friend, or should be. Death is needed by life, as much Death needs life."* she says

*"My* esposa *is from an old lineage of* gitanos, *old sages and magicians."* says Ernesto *"She still knows a little about the old time wisdom"*

*"What I can say for you: don't look for your past. Death is all what you'll find there."* she says, when she take a Tarot deck and after suffling she take and draw some cards: "El Arcano Sin Nombre, El Diablo, La Torre, El Judicio, La Justicia. *Death, Demon, The Tower, Judgment, Justice... You suffered a lot because this Omen, but the Omen was not for you, but for others. Don't worry about them, they had their payment..."* she says

*"But, my parents were there!"* I say *"There's people I care for there!"*

*"I can see you heard some message about them... They are safe... But you need to care about you."* she says, drawing other cards "Lo Loco, El Hierofante. *The Fool and The Hierophant. They are okay... Somewhere else. Now, you are the Fool... The one walking only on faith and sheer luck."*

*"So, you think I'm still not a* Maestro?" I say

*"What do you think?"* she says

I say... Pretty full on all that is happening... *"Honestly, everyone took me like a small kith all around, but I have the battle scars of my claims: the fall of Castello Porcinni, the* Buonnandanti, *the Trisquel, the Winter Winds."*

*"That I can see..."* she says, while serving the *paella* and putting some earthenware mugs full of wine for us *"But my question would be: do you think you can solve the problems you are seeing, and I'm asking it thinking those are problems."*

I look inside myself and then I say

*"Honestly... No! Those Seneschals from Furrow Heigh are so high on themselves that they would not believe my claims and still deny my position as Seneschal. They treated me like trash because someone in the centuries behind put that my spot profile is a Death Omen..."* (+1 Token: 1 - Speak your true feelings on a subject.)

*"And that it is..."* she says *"But you, like almost everyone else, see Death as a monster, the end of everything, the last and only enemy totally unbeatable. No matter how much you run, how much you hide... Death will get you, will get us all. Death is the only equalizer, as no matter it's a king or a dung bumble, it came for it all. But us* gitanos *understand that Death can be also a friend. We even had one small god,* La Ballerina Calavera *that helps us to understand that life is to be enjoyed... For sure you can enjoy on books and studies, but doing those only by themselves is a way to just throw your life away."*

I think now... What happened with me? I had fun while on doing the Ablution with _Danze dei Maschieratti_ on Velina... And I can't say it wasn't fun to go look for the Trisquel and so... But since them...

*"You look like feeling you lost something."* says Ernesto

*"That old Maestro... He felt like a husk... Since I saw him, I could feel there's something showing me that I need to got back and solve this once and for all."* I say *"Especially because he cursed my Father's Name."*

*"Perhaps."* says Magdalena *"But this can take time. You are on your own road. Death will follow you always, that I can see... It's up to you to understand if you want her as a friend or as an enemy, to fear her or accept that, in the end, she'll be the last friend you'll ever find in life."*

*"Looks you have a fascination on Death."* I say when I notice my comment wasn't that good *"Sorry..."*

*"We can say,* Maestro", she started *"that we* gitanos *understand Death is unavoidable, no matter the potions and stuff... But at the same time isn't the end. You had contact with the supernatural before, I can smell it"* she says

*"Hope isn't my tail losing my control."* I chuckle hoping my corny joke turns things a little less tense...

Ant it works as she give a small smile

*"No... You are not stinking. But there's something on you, the Death Omen, that makes people fears... You bring death, but not the omnious, useless death from battle. But the death that bring renovation. The* Iglesia *believes on the afterlife that was brought as a promise by the one that call The Son."* she says *"But we* gitanos *always saw there would be an afterlife, but one that no one knows about, neither the* Iglecistas" she states

*"And what have with me?"* I say, while eating

*"Those linked with the death and the supernatural are naturally feared"* she says *"Like us* gitanos *that were hunted by* Iglesia. *Because change provokes fear. Fear on what will happen, fear on what we'll lose, even fear that what we'll win would not be enough."* she states

*"And you think that the Seneschals and* Meisters *from Furrow Heighs feared that? That my spots was a symbol of change"*

*"Probably..."* she says, when I hear the huff of *Señor* Bellotin

*"Sorry, I didn't want to spoil the humor of the food, or abuse your hospitality."* I say

*"It's not your,* Maestro" says Ernesto... *"It's Toni... He didn't came for food again! What a boy! I believe he skipped the* Escuela *again to go for the balloonport."*

*"I can help to find him... I've finished your excellent* paella, Señorita *Madgalena, I can help your* marido" I say, using all Althusian I have

*"Thank you,* Maestro. *Let us find Toni first, then we'll decide what kind of punishment I'll apply!"* he says, when I notice he's not exactly calm while we go outside (+1 Token: 2 - Inconvenience yourself to help someone else.)

---

"Señor... *I believe you have some worry on Toni. Is there something wrong?"* I ask 

*"I don't know... We heard some weird gossip... Some students on one of our schools did some stuff trying to reproduce some old stuff of alchemy and by accident some scorpions get it. This was some years ago... But recently happened some weird deaths of tick hunters on the sewers: they had some wounds on their legs, like scorpion stings, but bigger than a common one, and they took days feeling dellirium and madness... People talks about* El Scórpion Diablo, *The Devil Scorpion, the one to bring death..."* Ernest says, while we looked for Toni all around the balloonport.

We start to talk with everyone about him, when a boy came to us and said *"We started to play hide-and-seek nearby the sewers exit, and then we tried to find him... But it was sometime ago, and we didn't found him..."* says the sheep boy

*"Alright"* says *Señor* Berrotin... *"Call the* Guardia *and let they know we are in the sewers... I'll look for my son."*

I can see *Señor* Berrotin looks tense, even scared

*"Fear it can be the Devil Scorpion?"* I ask

*"To be honest... Hope not."* he says, while he lits a torch and gave me another one.

We start to walk on the sewer catacombs, the horrible stench and dirty only being bearable for me because I'm pretty used to horrible stenchs as a skunk. *"Toni!"* we yell and the echo of our voices spreads through the tubes till we hear a cry of a calf and a voice.

"¡Papá!" and we see Toni... He looks dirty, stinking and scary.

"¡Hijo! *How many times I said you to not get into the sewers..."* was saying *Señor* Berrotin when he shout

*"I saw it!* !El Escorpión Diablo! *The Devil's Scorpion! He's nearby! I ran from it!"* he says

When we hear the clicks of pincers

*"Run!"* I shout *"Those clicks is not for a common scorpion or even for a group of them."*

We start to run in the opposite direction, trying to remake our way inside the sewers and run from the menacing clicks of the scorpion...

When on one corner, he cut our way outside and I see that are cornered. We all gulp and then I say for them.

*"I'll try to gain time. Run and bring help."* I say

*"Are you crazy? You'll die!"* shouts *Señor* Berrotin

*"You have family,* Señor. *By all I heard, I'm as good as alone on Haeth!"* I say, pushing them aside when the monster scorpion raises his tail and I can see the big sting... (-1 Token: 1 - Ease someone’s pain, if only for a moment.)

It's when I feel it: the sting hits my arm and I can feel something being pumped inside myself and burning... *"I'm poisoned!"* I think, while they run... I can see in horror the monster's pincers he have as a guise for a mouth going to chomp myself.

I can only think on The One In The Winds... 

> *“Straight, easy, simple answers are normally wrong, think you knew about this.”*

*"Varst... All the time here I was thinking on Furrow Heighs..."* I say *"And I'll die alone on this sewer"*

And then I hear the noise of a crossbow bolt hitting its target... I then do my best to hold the sting on my arm... He'll need to let it be split to run... I can hear the beast noise of pain and I can feel the venom bath I'm bathed on... And some members of the *Guarda* helping me, pushing me outside. (-1 Token: 0 - Ease someone’s pain, if only for a moment.)

But the sounds, the feelings... It's like a dream, like a haze... My body is like cooking on something, stewed on something... The numb feeling is horrifying conforting... When I pass out...

---

I can't say how much I felt useless...

I could hear voices and feel smells and see things...

But on the haze of the dellirium, I could not really understand what was happening to me.

When I saw an image and heard a voice...

*"Need to say, Puffers Underhill... You give me a lot of work."* says the voice of a lady.

I looked and could see her: a lady skunk, pretty, totally naked... She remembered my mother. At the same time, something on her gave me a kind of fear that I could not understand, only feel.

*"Who are you?"* I ask

She then looks to me and say...

*"I'm the last friend."* she says, and I could see...

Then I see her tail... The same patterns from mine!

She was Death

> + ***Death, the Last Friend (They/Them)***
>     + The Unavoidable and Unconquerable Enemy 
>     + The Last Friend also
>     + Appears when it's needed, on the form she needs to, but only on life-threatening situation
>     + _Dead, Empathetic, Venerable, Oracular, Wise, Friendly_
>         + Show someone something they truly don’t want to grapple with. If they want to avoid thinking about it, they’re going to need to spend a token.
>         + Express a concept in a way everyone understands
>         + Show what things were like in more grim times.
>         + Make a vague and unclear reference to something that hasn’t happened yet.
>         + Propose a path quite unlike those that others have suggested
>         + Introduce someone to an old friend of yours.

*"Am I dead?"* I say, raising from whatever I was

*"This... Is a question for you to say about."* she(?!) responds. *"It will all depends on the lessons you've learned till now. The Old Wind is not happy with you, that old fart."* she says, when I see her eyes... They have a kind of fascinating depthness, like all the jewels from the past.

*"The Old Wind? The North Wind"* I ask

*"Yeah... Pussy old fart. He's not happy on how you look like jump to my grasps."* she giggles. With a friendly undertone that I would never think Death would have

*"Now..."* she continues, a little serious. *"You thought on what getting that sting? That venom is burning you on the inside, and everyone can see you are stinking horribly, even for skunk standards."*

*"There was a kid there and his father. I'm alone in the world as far I understand."* I say

She laugh *"What a doofus you are. Are you taking conclusions on an old fart's gossip, that knows where you could be hit worst? For someone that encountered the* Bonnandantti *and danced the* Danze dei Mascheratti *on Ablution, you are pretty dim."*

I look and see her laugh

*"Understand, Puffers Underhill, I'm not your enemy, quite the contrary. I'm the one that will follow you all your time till you die."* she states

*"So, I'm not dead?"* I say *"This is a dream?"*

*"You aren't yet."* she says, smiling *"And here is as much a dream as reality is."*

*"Please... I'm not up for philosophical ideas."*

*"Oh, poo!"* she pouts *"You are dull. Didn't you understood yet? I'm not for you now."*

*"But..."* I try to state, when she looks serious to me

*"There's something you need to understand: there's not a fate, at least not as you kith understand. There's birth, there's death, there's events, but the middle is filled by the events as they happen."* she states *"And the links between events and symbols are illusions that kiths put to try and control stuff. But, at the same time, there's truth on it"*

*"What?"* I ask

*"Your Death Omen... They are right, the* Meisters *from Furrow Heighs... But at the same time they are wrong."* she says

*"What?"* I ask

*"Your Death Omen was right... It was a real one. But not for you, not for your family. And not one that would happen. One that was a Portent for something that was happening."* she says

*"And what would be it?"* I ask

*"The Fall of the Furrow Heighs* Meisters *."* she states, which hits me like a brick

*"What? So..."* I was to ask

*"Doesn't ask me for more. The event was a lesson for them. But all Furrowians will look for you as the cause, not the diagnosis".* she says

*"This is why the old* Meister *looked so angry to me?"* I ask

*"He's spiteful. He could see on you the strength to cope and raise from the fall."* she says

*"But I've failed with my family."* I say

*"If so, I would already got you... You are not a failure. Your family don't think you this way. You don't need believe me. You'll see them on the right time. Don't worry about them because you'll see them when time come."* she says

*"And... What will be of me?"* I ask

*"That is up to you... Embrace me and we'll go for the next journey. The one that curiously even I don't know how will be. Or... Just let me go by now. But how do you see me?"* she asks

I think for some instants and says *"The Last Friend, the one which I'll walk last.*

She smiles.

*"The* Buonnandantti *where right to declare you a* Maestro" she says, while I feel the haze going away, replaced by a darkness and by some pain...

---

I wake up on a bed, sweat and with a stench that, even remembering my natural stink, was really bad.

"¡Gracias las Passiones! *Blessed be the Passions, the fate-weavers!" I can hear a voice behind some protection for my stink.

*"Ugh! Do I smell that bad?"* I say weak and playfull. 

She opens the windows and then I can see the sun entering and removing the sickly green shade that confirms the skunk smell on a place.

I see *Señora* Magdalena and some other people... Even scoffing I could see *Meister* Durkhan. A physician on the clothes of the local university can be seen.

*"You were pretty lucky. The fragment you got from the Devil Scorpion's tail was enough to prepare a medicine for you."* he states *"By now, lay down and recover... This is one of the nastiest venons we had ever seen. One that is strong enough to make the stink of a skunk go even worst, like announcing death."* 

*"Humpt, what a waste, to cure a Death Omen. The one that brought our Honorable* Meistersheimat, *Our House of Wisdon, down just by being born. Be accursed his mother and father!"* he states

*"Shut up, old fart."* I say weakly

"Meister... *I'll need to ask you to go away."* says Ernesto.

*"Mark my words, kid. Those trinkets you have will not make you a real* Meister." he states before going out

*"So... It is true."* I say *"Death was right."*

*"Had you talked with it?"* Magdalena asks

*"Yeah!"* I say *"The last friend I'll walk aside."*

She smile

*"So... You did some decisions"* she says

*"Yeah... I'll find the truth about my family and I'll get back Furrow Heighs, but on the long way"* I say

*"And about being a* Maestro?" *says Toni, between his relieved sobs*

*"Somewhat, he was right. There is nothing that sustains my claims, aside my own words. Even those from Regina Conigglia from Preccia, Queen Minerva Rabbiton II from Herringland and Queen Ewe Mac Donaghal from Vaer will be seen for some as a condecending thing for a kith that helped them somehow, or worst, as a crazy whim of a patronizing rich powerful person.''* I say *"And there's nothing I can do against that... Only thing I can do is to go forward no matter what. And when time comes, go back Furrow Heigh and discover all that happened there and how my family is involved on this all..."*

*"But there's nothing you'll do for awhile..."* says Magdalena *"The news spread about you, how you fought and survived the* Escorpión Diablo *and how you were involved on all those stuff"*

Then I think on something and ask *Señorita* Magdalena...

*"When I was there, with Death, talking with her, she said... About my family. And that Furrow Heighs had fell"* I say

*"By what I saw, they were falling way before you was born.* ¡Portento, *you are! A message from all gods, big and small, of warning, showing them where their hubris would bring them."* says *Señor* Berrotin *"I've saw those guys before, all haughty and mighty, like above everyone else. That should not be: they should work with others for a better world."*

I think a little on this...

*"Perhaps I need to go as soon as possible. I feel and fear my family is suffering hard because of my Death Omen..."* I say

*"Before, you need to rest, and your Balloon need to be repaired. Your* Amigo Apestoso *needed a lot of repairing thanks your fall."* says *Señor* Berrotin *"And now you are in no condition to do anything else for a while. Take your time and rest, please."*

I nod, while pushing my tail to my belly, like a little skunk kith would do.

---

Some days passes, and I'm recovered. While drinking some mulled mead, I can see the leaves starting to falling and some chilly winds calling Grasping, I think on the recents experiences:

On Vaer, I found the Old North Winds, that asked *"Who are you?"*. Do I *really* know who I am? Or perhaps I was molded by the *Meisters* and can't just throw away this burden, no matter my bravado as *Arlecchino* at Velina's *Unnivercità* during the *Marcia Dei Mascheratti*?

Here, I find a kind of wisdom I never thought I would find: I found Death, but she'll be my Last Friend, the Last Friend that, time coming, will go for my future journey. But not by now... First, at least, I need to understand about the Fall of Furrow Heighs Meisters and about my family.

And for this, perhaps I need to stop to just throw myself in danger... I tried my best to be not so explosive, but looks like things casted me to be this way. Even risks being calculated, I throw myself into them...

*"What are you thinking,* Maestro?" says Toni

*"I'm thinking on my life and my ways nowadays... I was running from Furrow Heighs for so long... Now it's time to run to Furrow Heighs!"* I say *"On my own time, on my own way, but sooner or later I'll need to see this on my eyes."*

*"It's about your* Papá *and* Mamá?" he asks

*"Yes... I need to find their whereabouts: I run from them feeling I was a failure, a shame, a humiliation source for my family. But now, I understand I need to do this. Now I need to get back them, to try and show what I've learned: the folly of the Ivory Tower."* I say

*"Well... Your* Amigo Apestoso *is almost totally repaired. Lots of pieces and stuff to be repaired but the sturdy old balloon surely resisted. Great Harringlander work, for sure."* says Toni *"I'll miss you: you saved me."*

*"I'll too: I learned to miss everyone in my way. Lilly and Butterscotch from South Glens, Biggia, Nasone and Mrs Pergola from Velina,* Regina *Melina Connigla and Elena from Talmina, Amelia that teached me to balloon flight, Lipton Baron of Ottershire, Mr Rian McLuchóg... Everyone I found since I started for sure on this way."* I think *"But I know they are well, and they remember me... As you'll be well and remember me. Perhaps we'll find each other someday."* I say

*"Now, I'll study hard.* Papá *is right, being on the balloonport is not for me. I need to be better, to provide better ways for my parents and for Althusia and Haeth itself."* he says

*"I know..."* I say *"But never forget to give a time and go to the balloonport to never forget from where you came and to see what people get on their lives. Don't let knowledge rots in the Ivory Tower of the* Escuela" I say *This is the sin of Furrow Heighs... Don't make the same mistake. Never forget that you are a kith before anything."*

---

Some days passed and I'm good enough to resume my voyage. Grasping is come more and more nearby, the bolts of cold coming more and more...

*"And where is your next step,* Maestro?" asked Mr Berrotin, while he and Toni helped me with the last preparations for my departure with *The Smelly Friend*.

*I'll try to get back Furrow Heighs... But I'll need to do this the long way. There's a chance that snow will make me stopped on some balloonport. Anywhere you could recommend me?* I ask, when Mr Berrotin gives me a map of the continent.

*If you are going to Furrow Heighs, perhaps the best way will be going through the Kilner Way. It was a good airway pretty known by people, and there's the advantage to go into* Champaigne *and people there is pretty good with balloonists.* he says, when I see *Meister* Durkham getting into a big balloon with other sages, looking like going to Harringland.

*Hope I see you no more, Death Omen.* says *Meister* Durkham

*If you go to Ottershire, talk with Baron Lipton Harrington, and says him Puffers Underhill sent you. I think he'll love to share everything he knows about the Trisquel with you.* I say

*And why should I accept your council, the council of a haughty, impudent, and arrogant kith?* he spats

*Because we can only give people what our heart is full of, this is what I've learned while on the Seneschal training at Furrow Heighs, from someone you know pretty well.* I say, getting inside *The Smelly Friend*. *But you can take it or not, it's not my problem. I'm going back Furrow Heighs.*

*You'll not find what you want there,* priklosh. *You father is as good as dead for me, and your family too, and for good I need to say. Remember this when looking to your tail, Death Omen.* he says, while I take the ropes Mr Berrotin untied.

I pointed to the direction of Champaigne. I feel curiosly calm after all the offenses *Meister* Durkham said as a farewell. I then look to my tail, the spots of the Death Omen being so clear and pervasive.

*Perhaps... No... Death will be always my last friend, the last one I'll see. That old fart can rot alive as long it concerns me, I don't care. Enough to run from Furrow Heighs. Now it's time to run to Furrow Heighs, on the right time.* I think to myself, while Althusia is left behind...

I see the clouds and feel the cold... Knowing my fate is not linked with the old *Meisters* after all

<!--  LocalWords:  Underhill Tillsoil Asfriteyhi Varst Ack Geroff pre
<!--  LocalWords:  spoilt Seneschals farmworker Duchacy Heighs Heyia
<!--  LocalWords:  Hardworker Maths teached Nearsight Yay Cep mr Gack
<!--  LocalWords:  Conclaim conclaim conclaimed C'mon sensorial The
<!--  LocalWords:  Biggia Saltare Talmina Conigglio Whatcha Talvish è
<!--  LocalWords:  Wind' berrywine Univercittà Wanderhome Feruling da
<!--  LocalWords:  dei Mascherati Tanis Dulce dellAqua gamberetto tè
<!--  LocalWords:  Velina Sirenna Foresta uncared Carnevalle Zletvair
<!--  LocalWords:  Furrowian Karash pottymouth Haeth there'll Gennaro
<!--  LocalWords:  feruling Nasone Tinacci Istlevar Talpatone Maestri
<!--  LocalWords:  Univercittà' behavioured arrivist Volpinare Del'la
<!--  LocalWords:  Brighella Commedia Arte Cáspita alunni Danza Dalla
<!--  LocalWords:  mascherata Arlecchino Paollo Piva Canne Corso Il 
<!--  LocalWords:  witfull Lucca's Alunno Grata petrichor Dottore 
<!--  LocalWords:  campalina Talmina's Batocio Abluzionne Collegiale
<!--  LocalWords:  Leone Paolo camerata Martinno Aguila Bloommeadow
<!--  LocalWords:  Seneschalhood Castello Porcinni  Sabrinna Rossa
<!--  LocalWords:  Scoiattolo Signori Preccia Albionne's bloodlusted
<!--  LocalWords:  Tredici Callaleah Giorno Vulpone splitted Strada
<!--  LocalWords:  Reale Devildays Albionne cattlers Preccian Porcini
<!--  LocalWords:  principi Giacobbe Daughters putrification Hovkar
<!--  LocalWords:  carrions imponent devoided Thristvnar Oglash Varsh
<!--  LocalWords:  Hiev Talminare Principessa Tagrash Porcinni Vossa
<!--  LocalWords:  Studioso Maestà principe Principes Principesas tuo
<!--  LocalWords:  stablekith Albionnere Capitano geasa suceed Morte
<!--  LocalWords:  Maestro Maledicta gettoni Albionneri follyfull te
<!--  LocalWords:  prophetised fetore Maestritura Grazie Otello righe
<!--  LocalWords:  Lontrini Preccia's Preccian's Calleleah piagli ish
<!--  LocalWords:  buonnandanti principesse Conniglia Conigglia Unita
<!--  LocalWords:  descendancy stabilished Chiesa Sacríssimo Giorni
<!--  LocalWords:  Sacríssimo strawman Fratello paglia insanguinata
<!--  LocalWords:  pagila Strawmen Tripla Leprione Matronna Chiesina
<!--  LocalWords:  Chiesas vicino importante quanto Leprioni Chiesini
<!--  LocalWords:  insanguinate Preccians Chiesa Fratelli Precciani
<!--  LocalWords:  colours Inquizittori would Spavalderia Briatoli
<!--  LocalWords:  Mugenna Erano algurio Acidittà Cellebrazionne hoi
<!--  LocalWords:  Melina Conigglia's Salvio Rastelini Maressa simbol
<!--  LocalWords:  Apolonio Convocazione Ufficiale Porca Misera puzza
<!--  LocalWords:  suspice Vostra Altessa Precciana Precian Ratiratta
<!--  LocalWords:  Cerimonially embroidements Regina Preceptore Uno
<!--  LocalWords:  Antonnio Tassini Porpinella Porcospina Precianna
<!--  LocalWords:  morte Celebrazzione Buonnandantara Strawmen 
<!--  LocalWords:  Vecchia Matrona Furrowians heretici cleasing Starn
<!--  LocalWords:  Vecchi polloi puzzore puzzolente Callisteni skunky
<!--  LocalWords:  loathe'em Carverash Altalus Stankart shitshow mio
<!--  LocalWords:  Fredal Butcha Overpious Shaddap Stolas tripulation
<!--  LocalWords:  Savaltare Chiesa common saalan Chieserini dane
<!--  LocalWords:  L'Imperatore Giusticia Porta sweared gunports Cair
<!--  LocalWords:  Fratello ressurgence Tiesto conspiration expurge
<!--  LocalWords:  heighs Milena kiths Principe Achille Oldsmell
<!--  LocalWords:  Thabald danse bloodlust partisani conspiratori di
<!--  LocalWords:  dreamt protettore Regno heighed Spada Tassini's ot
<!--  LocalWords:  Gentildonna Scudiero Ratto delle vendite sooted Ní
<!--  LocalWords:  Scholarhood Calabrona Lettera Abilitazione sigil
<!--  LocalWords:  Nortissa Herringland Ottershire Ferretington cmon
 -->
<!--  LocalWords:  Harringland Badgerton Badgerton's Asvreth Laughton
 -->
<!--  LocalWords:  Harringlander Haatheestan criss stablekiths Vaer
 -->
<!--  LocalWords:  cattling Ottershire's Auberon Rabbiton Jeoffrey ár
 -->
<!--  LocalWords:  Vaerian Ballyford Auberon's Lapidation Belltower
 -->
<!--  LocalWords:  Trisquel Vaerian's farmlady Venisonton Heigh Rían
 -->
<!--  LocalWords:  lapidated Rabbiton's heaten Kingmaking mortem Rián
 -->
<!--  LocalWords:  cypher Laparast McLuchóg Longeyes Fussyears Milord
 -->
<!--  LocalWords:  Donaghal coleen Lavast Vaer's Noblesse Hallowberry
 -->
<!--  LocalWords:  sigils Harringland's Gateling unfill stravaganza
 -->
<!--  LocalWords:  Althalus Sliabh Thuaidh Srón Bhanda Gobnait muid
 -->
<!--  LocalWords:  Kennacht éiríonn tuirseach obair Chun seo onóir Tá
 -->
<!--  LocalWords:  gcuid tallainne agus cinniúint anseo gcás gach rud
 -->
<!--  LocalWords:  féidir linn thógáil Scian Tábla Pláta Peann Tuagh
 -->
<!--  LocalWords:  Claíomh Ciseán Ionstraim Níl aon dhéanamh feabhas
 -->
<!--  LocalWords:  dtiocfaidh ar saol luthiers Sorcha ni Luchóg meds
 -->
<!--  LocalWords:  Seneschal Fearghus Luthier mastership neverending
 -->
<!--  LocalWords:  Thinkerer resignate verde Althusia
 -->
