---
layout: contos
title: "Puffers Underhill - Chapter 6 - Gateling/Pyre: Vaer"
subtitle: A Wanderhome Story
category: stories
excerpt_separator: <!-- excerpt -->
---

+ Garden, Mountain, Workshop

> + Gateling
>     + Clear Starry Skies
>     + Cold Cold Nights
>     + The Smell You Imagine Home Would Smell Like
>     + Bonfires
>     + The First Few Orange Leaves
>     + Sweet Treats
>     + Kids Camping Out In Tents
> + Pyre
>     + Tossing Worries Into The Fire
>     + Roasted Sweets On Sticks
>     + Spooky Masks And Costumes
>     + Learning How To Light Fires
>     + Buckets Of Candy
> + Garden (Fruits and Veggies)
>     + Describe the abundance all around us.
>     + Threaten the health and vitality of the abundance.
>     + Ask: “What do you need most right now?” Give them a token if they can’t find it here.
>     + Trees Overburdened With Fruit
>     + Prismatic Butterflies
>     + The Peach Tree’s Prophetic Blossom (Folklore)
> + Mountain
>     + Describe what can be seen from way up here.
>     + Show someone pushed to desperate extremes.
>     + Isolate someone from everyone else, and give them a token.
>     + A Sign Someone Was Here Once
>     + A _Cautious_ Creature Hiding Just Out Of Sight
>     + The North Wind God And What He Taught Us (Folklore)
> + Workshop
>     + Describe the process of creation.
>     + Show conflict between expectations and demand.
>     + Give someone a token if they work hard and sweat over their creation.
>     + A Work Song
>     + An _Imaginative_ Craftsperson Who Made One of Your Treasures
>     + The Neverending Tapestry (Folklore)

<!-- excerpt -->

_"I'm happy I'm here with you!"_ says Willa, the Harringlander's balloonist, when we just got out _Cair Lavast_, Vaer's castle, after give them back the Trisquel. Now, we are going for our inn, to take some rest. 

_"What you'll do now, lad, lass?"_ asks Rían McLuchóg, our chaperone on Vaer. Vaer's Ambassador at Harringland, he came with us as Royal Guardian of the Trisquel, till it get to its right place at Vaer.

_"I don't know."_ I say _"Gateling is a very short time... And look this year will be really short!"_ I say

It was already four days since we've crossed Jumper-behind-Cloister and got into Vaer. Nice place: big valleys full of small farms, big mountains where cattlers and miners can be seen working, and lots of roads from all kinds criss-crossing the country and linking all the small villages with the towns. There was only a thing that we couldn't see a lot here: industry. There a few workshops here and there, but fewer and far apart than what we could see at Harringland and Talmina. Although all villages and towns, except the smaller ones, had nice shops for all they needed. (+1 Token: 1)

But Gateling was the shortest month at the year: at most 21 days...

_"Time flies when we have fun, nah, lad?"_ says McLuchóg. _"Say you: I heard this year the Royal Park will open at the festival time!"_

_"I heard about it! They say there's a nice garden, and The Royal Workshop where lots of things are done, gifts so precious the Queen Ewe Mac Donagal only give for those she believes deserves that honor. And I heard about the Tapestry and I would love to look it!"_ says Amelia

_"Tapestry?"_ I lift an eyebrow

_"This is an old legend, from the times where Harringland and Vaer had better relations in the past, before Auberon Mastiff's folly and the Trisquel Disappearance: it's said that a Goddess of Knowledge, Lady Cluckton, gave for old Mart Mac Mahon a project from a Machine, a Loom that would provide a Neverending Tapestry that would reveal what happened, happens and will happen. Some says that, if at anytime the loom stops to weave the tapestry, it will be the end of Haeth. So, the Royal Thinkerers were instated with the order to maintain the Loom working. It's a great honor to get into the shop, and a bigger one to work with the Thinkerers."_ tell us McLuchóg

_"In Harringland people says the Tapestry has prophetic powers: if someone looks to the Tapestry shuttles, it shows the most needed thing they should do, waving that on images. Some even says that it is in fact a Fate Weaver, it's how we call it at Harringland."_ comments Amelia

_"That's so interesting! I would love it!"_ I say

_"That's easy!"_ states Rián _"It's the Bonfire time! The only time in the year where Royal Park is opened for everyone. The Royal Park is on Ballystairs, two days road from here, and there's a balloon port on Lickley Pool, a small village nearby, one and a half day balloon from here."_ 

_"So... With the_ Vision of Friendship, _it will be only one day! Looks like it will be a nice travel: strong but gently winds this time of the year, it will be piece of cake."_ smiles Willa

_"Let us prepare ourselves today."_ I say _"Tomorrow we go for Ballystairs."_

_"I'll give a look and see how_ View of Friendship'_s canisters need refilling and any maintenance is needed. Puffers, you, and Mr. McLuchóg if he wants to join us, could buy some provisions: you know, food, drinks, stuff."_ says Willa _"We are way provided by now with the tithes we were granted by Queens Minerva II and Ewe Mac Donagal."_

I nod: my money purse is really full of Herrings and Gold Clovers, the local money.

_"Aye, lass. I have no business for a while here: should go for my fief at Hallowberry, but I know they can spare me for a little while."_ he smile, while we split ourselves

_"You are a great chap, Mr McLuchóg, to care by us!"_ I say

_"Nah, lad: you did a great service for both countries, Harringland and Vaer. And people here just recognized you for your service. Old Auberon Mastiff was a rotten scamp, and people spat on his name, but Falstaff's honor was now restored and he's back as a Friend of Vaer, as you and Baron Lipton Harrington, and Willa..."_ he says

_"Hey, don't forget me!"_ says the beagle guy with us, only his sword showing he's on duty

_"Oh, sorry Roland!"_ I say

_"Don't worry, Mr. Underhill. Normally I just want to do my duty good."_ he says _"But we, me and Roland wanted to help and do our duty!"_

_"And that you are, lass!"_ says Rián _"Now, take a breath! No riffraff will try anything. And if so"_ he says, snapping his fingers _"I will have some fun do some Vaerian brawling for a change. Fists against fists!"_

_"No need for this: ah, and Roland, you can relieve yourself a little. We'll take some time at North, on the Royal Parks, and you are coming with us, but not as a guardian or servant, but as a friend!"_ I state

_"It's an honor, sir!"_ he salutes

_"And, for first, it's not sir. Call me Puffers!"_ I say, while we laugh and start to shop for the festivals at Royal Parks

Sounds like will be a great time!

---

The travel was really nice: Vaer has incredible views, and I even got me a stravaganza. I saw an old badger photographer demonstrating one of those new portable daguerreotype cameras, that promised to work and replicate any image we want to, that came from New Badgerton on the Other Side of the Sea. I bought one he had as a spare for a small fortune.

And it was beautiful... I could registry on a picture the _View of Friendship_ with me, Willa, Rián and Roland, when we started the flight to Ballystairs. We could see some other balloons come the same route, many of them with the Vaerian crest, but some from Harringland and, I gasped, even some of Talmina!

_"Looks like_ Univercittà Verde _heard about the Trisquel and they came to give a look. Easier to come via balloon, except the Channel crossing that is a physical test: 12 hours flight from Althalus port of Esperanza, so you need to hold your potty till get Ferretington or the other balloon port of Lilly-under-pathways. Never did this before, sounds like something really big."_ says Willa

_"And what is that mountain?"_ I ask, pointing the mountain in front of us, that crosses the skies even higher than we are.

"Sliabh Thuaidh, _the North Mountain. Biggest one here at Vaer, and one of the few one real mountains here, even with all the other mountains and plates all around. Some says that was risen by Liar God, when did a prank with Earth Goddess, pulling her nose until it became long and thin like a pencil. This is why some call it_ Srón an Bhanda, _the Nose of the Goddess."_ says Rián (+1 Token: 2)

_"Looks like a very lonely place"_ states Willa _"Like... No one lives there."_

_"That it is: very few people got there, very fewer climbed it, even fewer came back alive, and even fewer got to it's peak. It's a dangerous climbing, very few got to it's peak..."_ says Rián

I look to it, transfixed...

_"What are you thinking, Mr. Underhill?"_ says Roland

_"Mr. McLuchóg, do you know anyone that got to the peak?"_ I ask

_"Are you thinking on... Look, lad, heard about even more skilled climbers than you would ever became that didn't get back! It's dangerous, a folly to you to climb this."_ he says

_"I know... But... Forget it, you're right. Sounds like a folly!"_ I say, but I'm still half baked with the idea to climb _Srón an Bhanda_. I just push it to the back of the mind while we start to ready for arrive at Lickley Pool.

---

We are fortunate that the news came to Ballystairs very fast and they recognize us as Friends of Vaer, and so our access to Royal Thinkerers Shop was pretty easy. We are received at it by a mink girl, on leather clothes and special glasses like the one Willa gave me 

_"I'm Lady Gobnait Mac Kennacht. I'm the Chief ot Royal Thinkerers Shop. Here we build and create the best items, some of them travel around the world!"_ she says proudly while we get inside after changing on some security stuff _"This is mandatory: lots of places here deal with white hot tin and steel, and I've already saw some stupid kith that didn't took care got some really nasty wound that took him his working capability!"_.

> + ***Lady Gobnait Mac Kennacht (She/Her)***
>     + Mink Chief of Vaerian Royal Thinkerers Shop
>     + Elegant but Non-nonsense and very strict
>     + _Proper, Crafty, Watchful_
>         + Reveal that a plan they’ve had in motion has paid off.
>         + Point out something people missed.
>         + Explain how things have been handled in the past.

It's a mishmash, but an _organized one_: all kind of workbenches, doing all kind of stuff, producing materials and finished goods from all kind, and the same music song all around:

> Ní éiríonn muid tuirseach den obair
>
> Chun é seo ár n-onóir
>
> Tá ár gcuid tallainne agus cinniúint anseo
>
> I gcás gach rud is féidir linn a thógáil
> 
> Scian, Tábla, Pláta, Peann
>
> Claíomh, Tuagh, Ciseán, Ionstraim
>
> Níl aon rud is féidir linn a dhéanamh
>
> Le go dtiocfaidh feabhas ar an saol i Vaer

_"This is our traditional song from the Royal Thinkerers of Vaer"_, says Lady Gobnait:

> We don't fear the toil
>
> Because our honor is to work here
>
> All our talent and destiny is here focused
>
> On building everything we need and can
> 
> Knife, Table, Plate, Pen
>
> Sword, Axe, Basket, Plaything
>
> Nothing exists that we can't build
>
> To better Vaer's Life

_"Nice life principle."_ says Roland

_"Thank you, mister. All those workers are the best of the best, joining our rank on very challenging skill tests that happens every year. At least a thousand try this every year, just a hundred got into the final round, only 10 becoming Royal Thinkerers commissioned by Queen Ewe Mac Donagal herself, under the Donagal House."_ she says

_"Wow!"_ I say, looking for a violin on a box over a workbench a mouse girl just finished

_"Do you like it, sir? This is done, like almost everything here, under a hundred years tradition of luthiers, teached from Master to Apprentice."_ says the mouse girl _"I'm Sorcha ni Luchóg. Nice to meet you sir."_

_"Puffers Underhill."_ I say _"That is nice... Can I play a little bit?"_

_"Oh, please! It's the first one I'm finishing. If this is really good, I'll became a new Violin Master, and this is so important, we love so much violins here at Vaer."_ she says

I check the tone and I can't resist and let a small tear.

_"Sir?"_ says the lady mouse

_"Nothing... Just remembering... I'll play it a little. If you excuse me."_ I say, taking the arc and starting to play... _The Winter Wind_

It's so different now, after all that happened... Sounds like... Calling me home, making me feeling homesickness for the first time since I crossed old Furrow Heighs limits.

_"Master..."_ says the lady, seeing my crying

_"Puffers..."_ asks Willa

_"Sorry: it's just... I had one almost exactly this one in past."_ I say

_"Wow... A Vaerian Royal Violin? Where you got that?"_ asks Lady Gobnait

_"Furrow Heighs... From father: he was a Seneschal like I was to be, and gave me this one, and I trained this music day and night to wait mother come back. And then I exiled myself thanks all this death omen balderdash and lost it, destroyed by some highwaymen at South Glens and now... I feel... Sad! Really Sad!"_ I start to cry like a little kith

_"Oh, dear!"_ says Lady Gobnait, when Willa come to me 

_"Calm down, Puffers, calm down. You're with friends now. You are a_ Talminare Maestro, _a Harringland Hero and a Friend of Vaer..."_ she says

_"I know, but... Sorry, it's the first time I feel that homesickness."_ I say, putting myself together.

_"I see... Well, if this violin is good enough to make you have those feelings, means little Sorcha did really a top notch violin, don't you agree, Mr. Underhill."_ asked Lady Gobnait, and then I nod, which make Sorcha smile.

_"Sorcha, go for Fearghus and give him this."_ Lady Gobnait give her a sigil _"You are now a Master on Musical Instruments. Ask him what to do next."_ 

Sorcha was to collect the violin when she thought something _"Mr Underhill... Do you want this? As a personal gift?"_ she asks, and looks for Lady Gobnait, as I myself

_"It's a great honor: normally the Apprentice stuff is not exactly good, but receive the first one from a recently minted Master is a great honor, as normally those are personal effects from him or her."_ she says

_"It would be an honor... If you wouldn't mind, dear lady."_ I say

_"No problem... Your music touched me. I had heard_ The Winter Wind _before, but I could feel the unforgiving wind and the claims of the kith, and the lessons from nature."_ she says

_"I'm glad then, thank you. I'll prize this as an honor, Master Luthier Sorcha ni Luchóg."_ I say, taking the case and closing it inside, nodding to her very respectful.

_"She is a promising one: she has some nice hearing and do the best as Luthier, and see her getting mastership fill me with honor."_ says lady Gobnait

_"That I can see."_ said Rián _"Lady Mac Kennacht, can I ask you a favor? My friends here heard about the Neverending Tapestry and Lady Cluckton Loom that Mart Mac Mahon built and want to give a look on it."_

_"Oh, that they can."_ says she smiling _"Come with me."_

She conduct us through a way, where looks like there's less heat, but lots of noise. A big Loom that weave by itself is there. And, out of it, a big Tapestry with all kind of things drawn and stitched on it.

_"This is the Lady Cluckton Loom, that weave the Knowledge Tapestry for Donagal House, since its build by Eternal Master Mart Mac Mahon built it from the design he received from Lady Cluckton herself on a dream. We saw lots of designs that represented events in the past, present and futures. It's neverending, although every year, on the last day of Chill, before Sunrise, we ceremonially cut the tapestry for the events from the previous year don't ilk those from this one..."_ she says, when she looked to it, astonished.

We looked, and we got astonished

On the patterns, one that looked like us bringing back the Trisquel, and then in the Royal Thinkerer Shop, and one of a skunk climbing a mountain... Till it detached by itself from the Tapestry and, falling in the ground, and someone ran and got it from the ground and gave it to Lady Gobnait

_"It's a Revelation Weave!"_ she gasps, looking to and take it

_"What?"_ asked Willa

_"Sometimes the Tapestry reveal some prophecy: we know because they detach themselves from the Tapestry itself, becoming those Revelation Weaves. It's said they are oracles from past, present and future."_ she says, looking to me and giving me the Revelation Weave _"Looks like you are to climb some place... and by what I can see, it's_ Srón an Bhanda." she finishes

_"Balderdash I say!"_ shouted Rián _"It's suicide to climb The Nose of the Goddess out of nothing! Better climbers became ice cones on this folly!"_

_"I don't know about you, Mr. McLuchóg, but I've saw very bad things happening for those who ignores Revelation Weaves. Legend says it was one of did this was an Old Otter that sent the Trisquel to Harringland and we all know what happened."_ she says

_"Codswallop! I don't want to see someone do this out of nothing, even more a Skunk Seneschal!"_ he replied

_"Mister McLuchóg, Lady Mac Kennacht..."_ I say when observe something

A small peach tree from the windows, and a rainbow colored peach flower blossoming

_"This!"_ Lady Mac Kennacht said _"It's the prophetical rainbow peach flower!"_

Even Mr McLuchóg can't say anything.

_"I'll go."_ I say _"I'll climb_ Srón an Bhanda"

_"What?!"_ exasperates Rián _"But it's..."_

_"I know..."_ I resignate _"But Mr McLuchóg, I don't want to sound preposterous with this, but I'm a grown up kith. I know that is risky, even a folly... But something, since we start to came to Royal Garden at Ballystairs, I felt like I should go there. There's something calling me there... Like a judge."_

_"And you are going to go on a Wild Goose Hunt there? Lad, you lost your marbles."_ he puffed and then he smiled meekly _"But looks like at least you have balls and heart enough."_

_"Thank you... But I'll need some supplies."_ I say _"And, if this is right, I'll need to do this alone"_

_"We'll provide you with some stuff... We need some test for some special climbing stuff we are developing."_ she says.

_"And all I can say, lad, is that the Gods bless you and make you grew into sense before your folly kills you."_ says Rián

_"And we'll be waiting you here."_ says Willa with Roland nodding.

I think on them... And on the climbing and I know it will be difficult!

---

Three days had passed. The first one was easy: I left everything aside my new climbing stuff on Ballystairs and got a ride till the base at _Srón an Bhanda_. A small ox boy offered to guide me to the first stage camping.

_"The first stage is the easiest to get, on half a day we'll be there. The second and third one are more difficult, and after you got out the third is to the peak or bust."_ he says

_"Thank you."_ I say

On the first stage, they registered my name and give me a patch for my bag and a map

_"There's two routes to stage two: one longer, but easier and safer, other way shorter, but difficult and dangerous. I don't counsel a first-time climber to go the difficult route though: some tight passageways and other kind of obstacles that can leave you injured pretty bad if you fail. Here, take those too"_ she gave me some flash lights. _"If you are injured, light one of them and throw to the skies, help you'll came."_

_"Thank you: how many time I'll take on each way?"_ I ask

_"The longer one is two day, and the shorter one takes half a day."_ she says while I take a bumble cheese hot soup as lunch

_"Thank you, think I did my mind."_ I say, with a sense of urgency

_"You'll go through the fast one, right? I can see this urgency on your eyes. What you want too much on this desolation?"_ she asks and I tell her my reasons.

_"You are on a Wild Goose Hunt, so."_ she says. _"How you know you find what you needed there if you don't know?"_

_"To be honest... I don't... I just have this feeling, this hunch, that once I get there I'll have my answers."_ I say

_"I can't forbid you on being stupid, but remember that it will be dangerous and_ Srón an Bhanda _doesn't forgive those that are stupid. That lots of care and, if for any reason you are on a big danger, forget this folly and run back safety here. No one will blame you for get sensible and get back."_ she says, while I ready myself

I follow the map and it's really difficult: my bag is heavy and I need my best to push myself on some places. I even got some cuts from some nasty stones that I clean as soon I can.

"Varst! _Maybe Mr. McLuchóg and that lady were right, this is a wild goose hunt! But need to go even a little more! At least to the Stage 3!"_ I push myself

It's when I got nearby the Stage 3 I see something in the middle of the snow: looks like a snail, but made of crystal, really cautious.

_"C'mon, dear... I don't want to hurt you."_ I say, when I start to follow it... And...

I start to hear a rumble, and feel the ground below me starting to break 

_"AVALANCHE!!!!"_ my instincts screams! I run and follow the small creature, and I can see her get inside a small cavern entry and...

I can get inside just before some tons of snow and ice close the cavern! I see that I even cut myself again thanks some ice shards nearby

"VARST! Zletvair Varst!" I scream _"I'm closed here!"_ I say to myself.

_"First things first. Care for yourself now, freak later."_ I think to myself and started to clean the wound with the special tools provided by Royal Thinkerer Workshop, including some good (but painful) injections of special meds to avoid all necrosis and infections. I finish clean and cloth my wounds, get a torch and eat a small climbing snack for energy

_"Now... Let us see if I can walk"_ I say, noticing I'm with some pain in my hind left paw, but it is not badly swelled or something like that.

_"Okay... I can walk by now. If I get out of this, it's down back safety for me. What a folly it was!"_ I regret myself

The small cautions crystal salamander are still there, and I can hear they squeaking to me, like saying I should go with they

_"Just wait and don't run, I can't run anymore!"_ I say and, putting the backpack and adjusting the weight to let my left paw with less weight, I got with them.

We walk for a while and I lose some of the time, till I got on what looks like a nice big cavern. Spots of light all around and nice crystal shards all around. And curious is that it is so... Warm.

But I look around and looks like there was no way out there...

_"It's here... My coffin?"_ I think to myself

I take the violin Sorcha gave me, the only effect I've brought with me, and started to play. Even the melancholy of _Winter Wind_ would be better than that desolation and desperation I was feeling inside.

While I've played, I could feel a icy wind crossing me, chilling all my pelt and puffing all my fur.

_"Perhaps, there's a way out. Otherwise, it would not have the wind there."_ I think to myself, walking a little more. And then I can see there is a cavern that goes to another side of the mountain, on an even worse walk. (-1 Token: 1)

I'm really high and below I could see all the way down south. I think I could see even Ottershire and Ferretington. I could see the balloons like small points coming to Royal Gardens at Ballystairs. I could see the smog going out the Royal Thinkerers Workshop's furnaces. It was a pretty vision, even considering my despair. (+1 Token: 2)

And I could hear my heart pounding... 

Curiously I couldn't feel fear: or perhaps I'm on a so big despair that I couldn't fear for my destiny

_"You came!"_ I hear a voice in the sky

_"Who's there!"_ I whisper, and then a small wind come, and get some ice and snow and forms a form in the sky

My form

_"I know you would came, Puffers Underhill."_ says the voice

_"Who are you?"_ I asks again

_"I'm The One In The Winds. There's any names for me, but I believe you know me from..._ The Winter Winds." says the voice

> + ***The One In The Winds (Them/They)***
>     + Mysterious North Wind God
>     + _Oracular, Inquisitive, Honest_
>         + Hold something up to the light
>         + Make a vague and unclear reference to something that hasn’t happened yet.
>         + Point out the truth everyone else has been ignoring.

_"You are the... North Wind God?"_ I ask

_"I'm this too... But in fact, what you should ask is..._ Who are you?" they ask

_"I'm Puffers Underhill, from Furrow Heighs"_ I say as a matter of fact

_"So, you are not_ Arlecchino? Il Ultimo Albionneri Maestro? Il Buonnandanti? _Friend of Harringland and Vaer?"_ they say

_"Why are you saying this? What this have to do with being from Furrow Heighs? What this matter?"_ I ask

_"Why had you cried when saw the violin?"_ they ask

I hadn't an answer

_"Good... Sometimes not have a answer is the best answer"_ they say

_"What you mean?"_ I ask

_"Sometimes, not knowing is the best wisdom, even more if this only brings only pain for you and others."_ they say

_"But if we can improve Haeth, even under the cost of your pain?"_ I say

_"What do you say? Feeling pain is worthy of the best for Haeth?"_ they ask

Again I was almost without answer, but...

_"I don't know... What I know is that the inaction could bring even more pain. It's like the Trisquel, it's like the_ conspiratori, _it's like the Porcinni folly and the_ Superior Maestro _at the_ Univercittà, _and like the Highwaymen from South Glens!"_ I shout

_"So, you prefer to be full of folly than a coward?"_ they say

_"I don't want to be neither one or other!"_ I shout

_"But everyone, sooner or later, become one or another."_ they explain

I understand

_"I... I prefer folly. The action sin, not the inaction one."_ I say

_"Good. This is good. You know thyself. Or at least more than many, more than before. From when you got from Furrow Heighs."_ they say

_"About Furrow Heighs, what can you say for me?"_ I say

_"I can't say nothing. Their follies and cowardice are not your concern since you turned your back from them."_ they reply

_"I can't not think on my father, my mother, my brothers. Their future, their dangers!"_ I say

_"Good. You don't forgot your origins. But their lives are they."_ them say

_"I'll... See them again?"_ I ask

_"That... I can't say... But I can see you learned to teach, and you teached to learn."_ them say

_"Now!"_ I exacerbate _"Can't you give me at least once a single, straight, easy, simple answer?"_

_"Straight, easy, simple answers are normally wrong, think you knew about this."_ they say, on a little disappointed tone

I fell in shame. This is the first thing you learn as a Seneschal and Scholar: ***Straight, easy, simple answers are normally wrong***.

_"You fell in shame on yourself... This is good. Is good to feel shame when you fall from wisdom. It's good to understand when you fail."_ they say

_"Should I still go up to the peak? Or even to Stage 3?"_ I ask

_"Should you?"_ they asked back

I understand...

I understand my folly but the wisdom I would learn here after that folly.

_"But how I could I get back?"_ I ask

_"How should you?"_ they asked back

I looked around and saw the small critter.

_"I think I know how to go."_ I say

_"Then go. You have no more here."_ they say, disappearing in the air.

---

I got back to the Royal Gardens, and I could see I've got back just in time for Pyre.

_"We fell you would come back sooner or later. That was a too big of a folly for a smart fellow like you."_ says McLuchóg, on a costume from a Highwayman

_"Yeah, that was in fact."_ I say, looking for my half healed cuts, my feet just out of a cast, still with a small swelling

_"You were lucky! People thought you were under all that snow from the Avalanche!"_ exasperated Willa on, curiously, a cat lady-thief costume

_"Yeah, I know... But..."_ I say, while checking my wounds and cleaning then

_"And you didn't got to the peak?"_ asked Roland, on a costume of a big mamma lady _"You had all this effort, all that danger, and you didn't even get Stage 3, nearby the peak?"_

_"No... But I don't feel bad, think I've found the answers I wanted there."_ I say

_"Oh, you found? And what were those?"_ plays Willa, while I start to put my _Arlecchino_ costume, the remembrance I have from _Univercittà verde_ and _Carnevalle_.

_"That it was a big folly, like you all said from the start."_ I say

_"Nice you learned that, lad!"_ smiled Rián _"You could had done this without left half of you there, but we have this saying in Vaer:_ little kiths that burns their paws learns fast"

_"That's barbaric!"_ shocks Willa

_"But I understand this."_ I say _"In fact, looks like I needed to burn my paws, for a change."_

_"Now... Let us enjoy the Pyre Bonfire. Had you done your Worries?"_ says Rián

_"Yeah!"_ we stated and got for the Bonfire

It was big, bigger than almost anything I've saw before. And so... Hot! The special kith that uphold the bonfire got naked, and people brought them refreshment all the time and they got away for some fresher places, and Roland teached us to light a fire. He was a great fellow.

_"We need to get back Harringland soon."_ says Willa

_"Why not do a Rabbiton - Ballyford route? Believe me, you'll have lots of guests and could make good money from this. I for a start would be one! Better and faster than by ship!"_ says McLuchóg

_"That's sounds swell."_ she says

_"For me, adventures end here."_ says Roland, while we start to throw the papers with our Worries on the fire _"Old Oswald needs me and so that jumpy Lipton... Or better, Baron Harrington of Ottershire. Otherwise the fief will be plagued by highwaymen."_ he says, trying to be more courageous then he was.

_"And what about you, Mr Underhill."_ she says

I think a little, and throwing my Worry Paper on the Bonfire I say

_"I know this sounds like another folly, but... Do you think you can give me a crash course on balloon flying... And sell me your old balloon?"_ I say to Willa's astonishment, while we laugh and share some more nice fruits and marshmallow roasted on the stick

---

Five days after, I'm back to Ferretington... All those adventures started here. I've just arrived here with old Willa's balloon, which I called _"The Smelly Friend"_ (a joke name that Willa gave but sounds so good and right). With some help, I've got the crests of Talmina, Vaer and Harringland on it

_"Looks like you really learn fast, Mr Underhill."_ she smiled, a little after we finished to ready our balloons to go back to the sky. _"Now, to where you'll go?"_ she says

_"I think I've learn a lot at this travel. Now I can also teach some. And learn to teach and teach to learn. Sorry if I look too obscure."_ I say

_"Nah, you're just being the same Mr Underhill that almost throw himself from a balloon basket on his first travel."_ she says, and then open a map _"If you want some tip, go for_ Althusia, _at Althalus. Great center of scholars, if what I've heard is right. And a nice route from here... You'll need to avoid go potty, but a chamber pot can help, as far you throw their contents on the sea. Heard the balloon port there is also incredible"_

_"Sounds good..."_ I say _"I want to enjoy this... This folly."_ I state

_"Ah, so you believe this is a folly?"_ she asked, playfully

_"Yeah... But, I've learned that, between folly and cowardice, I'll go for folly always."_ I say

_"And... Between us: what had you put on your Worry at Ballystairs?"_ she asks

_"I don't need to talk about. Remember: it's a Worry every kith needs to free himself from, at least for a while. And you don't need to talk about it."_ I say, and Willa looks to me, and we smile with each other.

_"This is what makes you a cute skunk!"_ she says, kissing my cheek, which make them blush hard _"You don't have fear to go forward, even knowing that could be a folly. Hope someday I see you back,_ Smelly Friend!" she says and then give me something _"I want you to have this. This as a gift, not bought like you did with this old, sturdy balloon that was my father and mine."_ passing me a small thing

An old brass compass

_"This compass was from my family since many years, from the time the Amelia-Danes crossed the sea. Some says that there was even Corsairs and Pirates on my family. My father gave it to me for a luck, but I think I don't need anymore. Hope this luck charm help you with your folly. And, the wind gods helps you, Mr Underhill."_ she says, while I put the rest of my stuff inside _The Smelly Friend_. She then helped to undo the ropes that held it to the ground, and started to go for _The View of Friendship_. I pulled the burners and felt the balloon rising, while I could see _The View of Friendship_ going on the other way: Ferretington-Rabbiton-Ottershire-Ballyford

_"Now... Let us go: teach and learn!"_ I say to myself, now _The Smelly Friend_ in the skies. 

While I make _The Smelly Friend_ goes over the Channel, I think on the response for Willa

_"Furrow Heighs"_ I say

That was the Worry I throw into Pyre bonfire.

<!--  LocalWords:  Underhill Tillsoil Asfriteyhi Varst Ack Geroff pre
<!--  LocalWords:  spoilt Seneschals farmworker Duchacy Heighs Heyia
<!--  LocalWords:  Hardworker Maths teached Nearsight Yay Cep mr Gack
<!--  LocalWords:  Conclaim conclaim conclaimed C'mon sensorial The
<!--  LocalWords:  Biggia Saltare Talmina Conigglio Whatcha Talvish è
<!--  LocalWords:  Wind' berrywine Univercittà Wanderhome Feruling da
<!--  LocalWords:  dei Mascherati Tanis Dulce dellAqua gamberetto tè
<!--  LocalWords:  Velina Sirenna Foresta uncared Carnevalle Zletvair
<!--  LocalWords:  Furrowian Karash pottymouth Haeth there'll Gennaro
<!--  LocalWords:  feruling Nasone Tinacci Istlevar Talpatone Maestri
<!--  LocalWords:  Univercittà' behavioured arrivist Volpinare Del'la
<!--  LocalWords:  Brighella Commedia Arte Cáspita alunni Danza Dalla
<!--  LocalWords:  mascherata Arlecchino Paollo Piva Canne Corso Il 
<!--  LocalWords:  witfull Lucca's Alunno Grata petrichor Dottore 
<!--  LocalWords:  campalina Talmina's Batocio Abluzionne Collegiale
<!--  LocalWords:  Leone Paolo camerata Martinno Aguila Bloommeadow
<!--  LocalWords:  Seneschalhood Castello Porcinni  Sabrinna Rossa
<!--  LocalWords:  Scoiattolo Signori Preccia Albionne's bloodlusted
<!--  LocalWords:  Tredici Callaleah Giorno Vulpone splitted Strada
<!--  LocalWords:  Reale Devildays Albionne cattlers Preccian Porcini
<!--  LocalWords:  principi Giacobbe Daughters putrification Hovkar
<!--  LocalWords:  carrions imponent devoided Thristvnar Oglash Varsh
<!--  LocalWords:  Hiev Talminare Principessa Tagrash Porcinni Vossa
<!--  LocalWords:  Studioso Maestà principe Principes Principesas tuo
<!--  LocalWords:  stablekith Albionnere Capitano geasa suceed Morte
<!--  LocalWords:  Maestro Maledicta gettoni Albionneri follyfull te
<!--  LocalWords:  prophetised fetore Maestritura Grazie Otello righe
<!--  LocalWords:  Lontrini Preccia's Preccian's Calleleah piagli ish
<!--  LocalWords:  buonnandanti principesse Conniglia Conigglia Unita
<!--  LocalWords:  descendancy stabilished Chiesa Sacríssimo Giorni
<!--  LocalWords:  Sacríssimo strawman Fratello paglia insanguinata
<!--  LocalWords:  pagila Strawmen Tripla Leprione Matronna Chiesina
<!--  LocalWords:  Chiesas vicino importante quanto Leprioni Chiesini
<!--  LocalWords:  insanguinate Preccians Chiesa Fratelli Precciani
<!--  LocalWords:  colours Inquizittori would Spavalderia Briatoli
<!--  LocalWords:  Mugenna Erano algurio Acidittà Cellebrazionne hoi
<!--  LocalWords:  Melina Conigglia's Salvio Rastelini Maressa simbol
<!--  LocalWords:  Apolonio Convocazione Ufficiale Porca Misera puzza
<!--  LocalWords:  suspice Vostra Altessa Precciana Precian Ratiratta
<!--  LocalWords:  Cerimonially embroidements Regina Preceptore Uno
<!--  LocalWords:  Antonnio Tassini Porpinella Porcospina Precianna
<!--  LocalWords:  morte Celebrazzione Buonnandantara Strawmen 
<!--  LocalWords:  Vecchia Matrona Furrowians heretici cleasing Starn
<!--  LocalWords:  Vecchi polloi puzzore puzzolente Callisteni skunky
<!--  LocalWords:  loathe'em Carverash Altalus Stankart shitshow mio
<!--  LocalWords:  Fredal Butcha Overpious Shaddap Stolas tripulation
<!--  LocalWords:  Savaltare Chiesa common saalan Chieserini dane
<!--  LocalWords:  L'Imperatore Giusticia Porta sweared gunports Cair
<!--  LocalWords:  Fratello ressurgence Tiesto conspiration expurge
<!--  LocalWords:  heighs Milena kiths Principe Achille Oldsmell
<!--  LocalWords:  Thabald danse bloodlust partisani conspiratori di
<!--  LocalWords:  dreamt protettore Regno heighed Spada Tassini's ot
<!--  LocalWords:  Gentildonna Scudiero Ratto delle vendite sooted Ní
<!--  LocalWords:  Scholarhood Calabrona Lettera Abilitazione sigil
<!--  LocalWords:  Nortissa Herringland Ottershire Ferretington cmon
 -->
<!--  LocalWords:  Harringland Badgerton Badgerton's Asvreth Laughton
 -->
<!--  LocalWords:  Harringlander Haatheestan criss stablekiths Vaer
 -->
<!--  LocalWords:  cattling Ottershire's Auberon Rabbiton Jeoffrey ár
 -->
<!--  LocalWords:  Vaerian Ballyford Auberon's Lapidation Belltower
 -->
<!--  LocalWords:  Trisquel Vaerian's farmlady Venisonton Heigh Rían
 -->
<!--  LocalWords:  lapidated Rabbiton's heaten Kingmaking mortem Rián
 -->
<!--  LocalWords:  cypher Laparast McLuchóg Longeyes Fussyears Milord
 -->
<!--  LocalWords:  Donaghal coleen Lavast Vaer's Noblesse Hallowberry
 -->
<!--  LocalWords:  sigils Harringland's Gateling unfill stravaganza
 -->
<!--  LocalWords:  Althalus Sliabh Thuaidh Srón Bhanda Gobnait muid
 -->
<!--  LocalWords:  Kennacht éiríonn tuirseach obair Chun seo onóir Tá
 -->
<!--  LocalWords:  gcuid tallainne agus cinniúint anseo gcás gach rud
 -->
<!--  LocalWords:  féidir linn thógáil Scian Tábla Pláta Peann Tuagh
 -->
<!--  LocalWords:  Claíomh Ciseán Ionstraim Níl aon dhéanamh feabhas
 -->
<!--  LocalWords:  dtiocfaidh ar saol luthiers Sorcha ni Luchóg meds
 -->
<!--  LocalWords:  Seneschal Fearghus Luthier mastership neverending
 -->
<!--  LocalWords:  Thinkerer resignate verde Althusia
 -->
