---
layout: contos
title: "Puffers Underhill - Chapter 1 - Tillsoil: South Glens"
subtitle: A Wanderhome Story
category: stories
excerpt_separator: <!-- excerpt -->
---

> + Tillsoil
>    + Fertile Soil 
>    + Constant And Challenging Work
>    + Gentle Rain
>    + A Rhythmic Work Song
>    + Wide-Brimmed Hats
>    + Fluffy Clouds
>    + Bugs Running Through The Fields
> + Market 
>   + Foreign Wares
>   + Distant Smells
>   + The Golden King And How He Lost His Heart (Folklore)
> + Lagoon
>    + A Glorious Waterfall
>    + A Well-Worn Path
>    + The protective small god of wanderers (Folklore)
> + Road
>    + A Lively Waterway
>    + Grooves In The Ground
>    + The Old Myrmidon that Leads Highwaymen (Folklore)

<!-- excerpt -->

I came from some days of walk to South, from the distant North through the Great North Road. 

"Asfriteyhi! _Maybe I can find some food there. My supplies are going short..."_ I say while let my cloak down from my head. I'm smelling like any skunk, or even worse thanks the time without a bath _"Maybe it's better to go for that lagoon"_ I think, smelling myself _"I'm smelling really bad and this shouldn't be."_  Normally I would have no problems on being stinky, but his past as Scholar-in-training still have its marks on myself, and I know how much people have issues with bad smells. _"Lucky I still have some lice soap with me. This will be enough by now."_ I say, going for the big lagoon. 

I know people normally go for fish and picnic on those places, so I look around for a more isolated spot where I can go naked. I feel a spot, but I notice small carvings.

_"Looks like this is an old homage spot... Maybe for one of the old gods?"_ I raise my eyebrow. _"Whatever... Better not cross them, they're really fickly... I'll give them my last honey bread. Maybe I'll get hungry before getting in the city, but better not cross the gods."_ I say, putting the honey bread over a spot (1+ Token)

Then I undresses myself, relieving on letting his tail flow away, completely naked and get inside the lagoon spot: it's not that deep, but I can't walk more than a half dozen steps before feeling the lagoon will go really deep soon, and I was never exactly a good swimmer, quite the contrary. I start to scrub itself with the lice soap and appreciates the lagoon, its deep blue colors, some birds flying over, even some small boats and rafts getting to the small town, getting there through the river the lagoon goes over. The sky is lighter blue, but as beautiful as the lagoon, it remembers me home, with all the bittersweet remembrances from the old lakes and hills where I'd played, alone or with the fewer that doesn't looked to him as a omen of death. _"Looks like wanderers like me used this spot before... The carvings for that makeshift shrine looks like something done on purpose, although it doesn't looks like any I know before."_ I say, while rinsing myself, trying to not spoil the place with the filthy came from my tired body. (1+ Token)

When I got out the lagoon and back to the forest to dry itself, I could hear a small squeal! Like a small girl on a problem! 

"Varst! _Highwaymen? Let us see!"_ I look to the squeal direction and see a small bunny girl on a cute dress, holding dearly a small basket while a pair of vile looking weasels tries to take it 

_"Give this to us!"_ they say

_"No! This is my daddy lunch!"_ she says.

_"Hey, you! Why are on this?"_ I say, even knowing I'm now naked!

_"Get off, old fart! Or you'll have problems!"_ says one of the weasels

_"Little lady, let them to me, just go for the woods for a while!"_ I replied to the bunny girl, that I ran on a shame _"I should had at least put my trousers!"_

_"Now, old fart, get off... Unless you want us to deal with you."_ gloated the other weasels

_"Well... Looks like you are not gentle enough to not do bad things with a little girl... And doing this on a gang. What a shame... Maybe I should put some manners on you both."_ I say, turning around and giving a big skunk spray on them, that stinks afoul! (-1 Token)

_"Ack! What a stink! Geroff, Don, this guy is business!"_ says the first weasel, while they run, letting the basket fall in the ground.

Puffers get nearby and smells the basket and it's contents. The basket is really stinky now, but their contents are clean and have no smell...

_"Well... This will be a problem..."_ I whisper, while saying to the air _"Hey, little girl, are you still there?"_

_"Yep... But papa says I shouldn't get nearby strangers, even more naked..."_ she says, hidden on some woods

_"Okay... Give me some moments and I'll dress myself. Don't get to your basket by now, it's stinky. Let the air clean it enough. Fortunately, the food is not spoilt!"_ I assure, while I can see the girl nodding.

Hastily, I dress himself, with my travel cloak and take my Seneschal's Staff, the only thing I still holds from my time as Skunk Seneschal in training. I got back and take the basket and... Looking around, he see small flowers. _"This can help with the smell."_ I think and, after doing a small pass asking for the nature's spirits for permission, I took some of the flowers and put them in the basket while the little girl came nearby. I can see she's dressed on a peach colored dress with a white frilled pinafore and bloomers with a hole for her brown tail: she has a white fur all around, except for some caramel colored blotches spread all around her fur, including over one of her eyes, and for the caramel muzzle. Her eyes are brown and big as all rabbits have.

_"Thank you sir... Who are you?"_ she says

_"I'm Puffers, a wanderer for a long time. I was going for South Glens: I've walked a lot at the north last year, and though on getting south on Leap month to start fresh somewhere else."_ I reply

_"I'm Lilly. My papa works on that farm there, mama on a small store at the city."_ says Lilly curtsying, while I nod on reply. _"This is a Seneschal's Staff? You're a Seneschal from some kingdom of the north?"_

> + ***Lilly Bluegrass:***
>    + Rabbit little girl (she/her)
>    + Daughter of Butterscotch Bluegrass, a local farmworker
>    + Likes to study, very helpful
>    + _Cunning, Cheerful_
>         + Get somewhere you’re not supposed to be.
>         + Gallivant into an awkward situation.

_"I was, in the past, training for it... But let us talk about this later. Better bringing your papa's lunch, perhaps he's hungry after working... I'll go with you: better be with you if those weasels try something else. And don't worry about stink: the basket is stinking no more."_ I say and she whiffs a little the basket

_"Yeah, it is not."_ she says smiling

_"So, now, let us go."_ I say, and we walk a little to the farm, where a big caramel colored rabbit was working some vegetables.

_"Papa!"_ she squeals and runs to him, hugging him _"This kind skunk had helped me against those meanie weasels that tried to take your lunch!"_

_"Oh, Lilly... I always says you to not came here alone. You should ask someone else, but I'm glad you're good. And nice to meet you sir! Come here, and we'll share some food."_ says the rabbit farmer

_"It would be an honor. I don't want to be inconvenient."_ I say

The rabbit man usher us to a hut where some other people is eating lunch. An old mongoose lady looks to Puffers and his Staff

_"So... You're a Wanderer Seneschal? A bit far away home, right?"_ she says, with a smirk

_"Sorry, lady... I run away: my father is a Seneschal, but people took me as a omen of death because my cursed spots..."_ I say, shamefully showing them the black spots on his white stripes on his tail

_"Forget it, chap! If you helped little Lilly, you're welcome. We don't have too much, but I think I can give you some wheat soup and bread."_ says the mongoose

_"It would be nice, thank you."_ I reply

I feel good, but a little weird with all those people around

_"Looks like you came from too far, right?"_ says the mongoose lady

_"I think... You are familiar... You came from Furrow Heights at north?"_ I ask

_"Not me... My parents. They came after the Duchacy formation from there. I'm Petunia."_ says the Mongoose _"And looks you are a Scholar from Furrow Heighs... How you came just here in South Glens. Here is a little too hot for a skunk, you know..."_

_"It's not that important..."_ I say, trying to change topics

_"Oh, by it is... You see, people in South Glens can be cosmopolitans, but he have some healthy doses of worries, with guys like those weasel brothers that tried to mess with Lilly."_ says Petunia the Mongoose _"Normally Scholars doesn't go around, unless on Pilgrimage or..."_

_"Alright: I was took as a Death Omen and need to go away to avoid problems for my family..."_ I say, and Petunia put her paws on his mouth

_"Oh... But... I'm sorry, really sorry..."_ she says

_"Don't worry... It's not like I wasn't used to be frown upon because those spots on my tail My parents did their best to help me but... You know how people on Furrow Heighs are: they were shunning my parents, and this was giving problems. My father tried to convince me otherwise, but as soon I've got old enough I just ran... Last year was my first one outside Furrow Heighs, never got so far away to south. I just know about South Glens because my studies"_ I say (+1 Token)

_"Oh... Sorry. I didn't want to push you."_ says Petunia

_"Don't worry."_ I say, smiling, but I notice the caramel looking rabbit farmer looking to me

_"I'm Butterscotch Bluegrass. Lilly is clever and, need to say, would not trust someone that could do her ill."_ he says, assuring _"Think you had walked for a lot. What do you think: you work here for today and get some coins. Then I bring you home and we can lend some place for you."_

> + ***Butterscotch Bluegrass:***
>    + Rabbit farmworker (he/him)
>    + Father of Lilly Bluegrass
>    + Hardworker, supportive for his friends
>    + _Sturdy, Friendly_
>         + Push something concerning aside.
>         + Start up a conversation with someone else.

_"Are you sure? Think you know about skunks..."_ I say

_"I know a good guy like you know how to control stink, even sleeping."_ he says, smiling, his rabbitry lips opening and showing really beautiful white buck teeth

_"I would be glad."_ I reply

_"Heyia, Butterscotch, let him here caring for Lilly. Think she has some homework, right?"_ says Petunia _"What about a Scholar as a Preceptor-for-a-day?"_

_"It would be really fun!"_ squealed delightfully the rabbit girl

_"Looks like she's a nice student... It would be good. Have you your books and notebook?"_ I ask

_"Here!"_ she says, taking them from a small backpack, showing them to me

_"Let me see... Looks like stuff nice for her age... Maths, Language... Never know you had a school here."_ I say

_"We are not a bunch of ragamuffins, you know"_ say a guinea pig guy _"We are one of the main places people came during the holidays. And so we educate our kids so everyone can be at ease here."_

_"Sorry. I didn't want to do harm."_ I say _"As I've said, I just came from North... My last time with people was last month in Billowborough."_

_"You came at foot all the way from Billowborough?"_ wowed a hen _"You had quite a travel to be done at foot. And during the Change of Year? What a sad time to be at road by your own."_

_"Heyia, everyone!"_ says Petunia _"I know it's nice to talk with newcomers, but now we have work to do: that wheat will not crops itself. Now, Lilly, call Donovan and Parsnip: think Mr. Scholar will help you with homework today!"_ she snickers _"And Mr Scholar, I have some clothes here that can work for you: those ones are really filthy-looking and certainly smelly. I can clean them for you and tomorrow morning they'll be clean for you."_

_"Thanks, miss Petunia... But..."_ I say

_"No buts. I don't want the kids feeling ill because stinky clothes of a haughty skunky scholar!"_ cutted Petunia, making fun on Puffers, while taking a small bundle of clothes from below the table she is

_"Alright... Alright..."_ I assure, taking the bundle of farm like clothes and exchanging for my old stuff on a bathroom nearby

It was a nice afternoon: _"those kiddos are really clever"_ I thought, while teaching them some about the five stations, the months, math, and other contents. _"They remember me when I was young._ Varst! _I changed that much since I've got out home?"_ I say, when I look my violin.

_"Now... Looks like you already had did your homework... Who want to hear some music from north?"_ I say and they giggle and cheers: aside the bunny Lilly, Donovan looks like a nice bulldog and Parsnip is a small mouse, with a light brown colored fur and big eyes. I go for my violin and just check it. And then... I start to play the traditional music my mother always loved: _"The Winter Wind"_. A little melancholic, but so beautiful, its said it was composed by one of the first Seneschals from the North while enjoying seeing the Snowy Plateau and feeling the wind singing...

_"Woah... This music is pretty... But it's sad..."_ says Lilly 

_"The North , specially from where I came, is cold almost all year. My people is melancholic and reverent to the old golds. This remembers me home: my mom loved this music because all the histories about them"_ I teach them about those

_"Woah!"_ says Parsnip _"What a history!"_

_"But now, kids, it's time for your parents get back home... And we need to take care: if those weasel brothers are there, their gang is there too. Those highwaymen are a problem: normally they don't get so much, but Lilly attack was the fourth just this week"_ says the Mongoose woman. _"Think I'll need to let some extra security here, even more we are on harvest."_

_"And I can help the others to care for kids in the road, while going to the city."_ I say, while the guys came and starts to serve some water from an earthenware jug. 

_"Take some, Puffers. The road can be a little tiring, better to get some energy before."_ says Butterscotch pushing me a earthenware tankard full of water

_"Thanks!"_ I say, while drinking the water: cool, fresh, with a hint of earth on flavor, very refreshing. _"Think I'm ready to go."_

_"Let Petunia here. She lives here and some guys will be with her."_ says Butterscotch. _"Some constables came now and again to help her."_

We got to the road, and I can feel the tension around: the smelling sense of skunks is heightened than many other animals, perhaps only rabbits and dogs being better. And I could feel some of them smelling of fear...

_"Those guys are those tough?"_ I ask _"They looked like big bullies for me, when I've saw them against Lilly."_

_"They are fierce and mean, but they aren't the worst. Their leader is a guy that was a captain in the past, but lost the post after accused of graft and stealing. No one, however, was able to stop him..."_ says Butterscotch, when some guys circled us. The weasel brothers was with them, but there was all kind of animals

_"So, you are the Skunk Seneschal that stunk my subordinates!"_ says the guy, who looked like a White Bear

_"Yeah, I am... And I can see now who you are: you were from Frostwall. Probably a myrmidon... Am I right?"_ I say (-1 Token)

He smirks, showing me his battle ax _"A Scholar from Furrow Heighs... And a Death Omen one! This explains a lot!"_ he smirks

I give a good look for both Butterscotch and the others. 

_"Not that easy!"_ says the bear, when notice our plan _"You'll not be able to run before give us the tithe!"_

I just turn back him and present my tail, spraying on his direction _"You! RUN! This will not be enough against them!"_ I say. The bandits spread a little away, giving time to them to run away to the city, but soon they are circling me! (+1 Token)

> + ***Jorg Stormbringer:***
>    + White Bear bandolier and highwayman (he/him)
>    + A Myrmidon that fell from grace thanks Furrow Heighs Seneschals
>    + Fierce, Mean
>    + _Furious, Feral, Resolute_
>         + Lash out without meaning to.
>         + Show all your teeth and bite. If someone doesn’t want to get bit, they’re going to need to spend a token.
>         + Keep at something that others would give up at.

_"Now, I had enough! I hate you Furrow Height Scholars. It was one of you that made me go banished when discovered my small side hustle on food. Now... I want your pelt, and I want to carve Jorg Stormbringer name on it!"_ he says, brandishing his battle ax against me. I can dodge a little, but I'm not a proficient fighter, and they are overwhelming me. 

They ganged up against me and two black rat guys hold me, and the weasels punch me in my stomach for a while, when the bear hits me straight on my head with me Seneschal's Staff, breaking it to smithereens. I then choose to a desperate case: forcing myself, I go and release all the stink charges I still had, making the air so foul smelling that even I cough. I start to run when one of the weasels take my bag and I hear it torn itself. I just run away, when I notice something is missing: my old father's violin! (+1 Token)

_"Now, run, you coward! And thanks for this stuff!"_ I can hear the weasel cheer. I feel some tears falling: my last links with my past at Furrow Heighs just got taken from me. I run until I get into a place where I can hide and then, the pain on me make me fell knocked out.

---

_"Mister Scholar... Mister Scholar..."_ I hear someone says and touching me slightly. I wake up, and the pain from the punches with me.

_"Ouch..."_ Is all I can say at the time

_"Come here, Lilly"_ I hear a whisper _"Let us take him home. Lucky we had time to bring some constables to help."_

I can see it's Butterscotch.

_"How... Ouch... You found me..."_ I say in pain

_"Well... It's somewhat easy find where a skunk in fear passed around, even after you stench a region for lots of time. Now, don't speak too much, I'll bring you to Doc as soon we get in the city... Looks like we don't need to fear the bandits, at least by now"_ says Butterscotch

_"Yeah... Yeah..."_ I whisper, while Butterscotch and another bear, a Honey Bear one, with brown fur and dressed on tweed helped me get over my feet the best I could on my pain.

They are secured by some constables until we cross the town walls, that closes after us

_"It's already that late?"_ I asks in pain

_"Nope... Just a little after curfew, but when they saw us running, they closed the doors fearing the bandits decided to crash against us. Now that they saw what you did, they allowed us to go and rescue you. Looks like the Old Nayads from the lagoon helped you: it was difficult to find you, like you were made invisible, and we found you nearby an old holy spot with a half-eaten honey bread on it"_ said Butterscotch.

_"Better we leave you straight on your home, Butterscotch... I'll look for Doc as soon we leave him there. Your wife is good on first aid, she can patch him up enough before Doc give a look on him."_ says the bear

_"Thanks..."_

_"Call me Oakhoney... It's like everyone here calls me. I'm leader of the local constables. And an old chap from Butterscotch"_

> + ***Oakhoney Treehugger:***
>    + Bear constable (he/him)
>    + Old-time friend of Butterscotch
>    + Loyal, confident
>    + _Honest, Sturdy, Proper_
>         + Point out the truth everyone else has been ignoring.
>         + Push something concerning aside.
>         + Struggle to get something new.

_"Thank you..."_ I say weakly 

_"Spare yourself a little. You were really lynched by those guys. I believe you blasted them full round."_ says Oakhoney

_"Yeah... No stink for me at least for some days..."_ I says, moaning in pain.

I see a small house with lights on, and then someone opening it: _"Oh, dear!"_ Someone says, and I see a white rabbit lady, on a simple house dress and apron. She looks too much like an adult Lilly, except her color blotches are light gray, not caramel like her daughter.

_"Bring him inside! The sofa is ready for him for a while! I'll take him some porridge if he can eat."_ says the lady

_"Thanks, Dandelion."_ says Oakhoney _"I'll run for Doc. Close the doors and take all the lights you can off... Better those bandits not know he's here..."_ he finishes and the rabbits nod accordingly while ushering me inside their simple but cozy home.

> + ***Dandelion Bluegrass:***
>    + Rabbit clerk on a small store (she/her)
>    + Butterscotch's wife
>    + Worries a little too much for their family and friends
>    + _Caring, Friendly, Empathetic_
>         + Protect someone else from the world.
>         + Inconvenience yourself to help someone else.
>         + Start up a conversation with someone else.
>         + Express a concept in a way everyone understands.

I was laid down over a sturdy sofa, a pillow helping to hold me up over a corner. I can feel the smell of porridge, and then I see my undone bag: the only thing that was saved was my Seneschal Lore tome, the tome every Seneschal-In-Training copies from the tomes from their masters first thing on his training.

_"They took your violin, right? It was that important?"_ asks Lilly

_"Yes, they did, and yes, it was, but not more important than your safety."_ I say, trying to smile, but really sad

_"Something you papa or mama gave you?"_ she asks

_"My father: he teached me all traditional songs from our family and kind while I waited mom came back from the Temple..."_ I reply, when the rabbit woman came from the kitchen

_"Now, Mrs Scholar: hope you don't feel bored, but Lilly is a chatterbox sometimes."_ says her _"Now, can you hold the bowl and the spoon?"_ she says

I feel some pain, but I can hold both and eat a little. _"It's good, thanks"._

_"It's just common night porridge. Nothing so fancy."_ she says

_"Anyway... It's good."_ I state, eating the most I can in my pain. When I almost finished, the door was knocked, and I saw Mrs Bluegrass getting a pasta roll, ready to use it if needed.

_"It's me, Doc!"_ said an old voice and, relieved, she opened the door for an old mole Doctor, with a big brown overcoat and a carpetbag on a hand. _"So this is the Skunk Scholar the bandits got? By the gods, they mauled you hard!"_ says the mole, touching me lightly and pressing, lightly, but hard enough to make me feel some pain 

_"Good news is, you don't have any internal wounds. Bad news is you'll need sometime to recovery. I'll leave you some recipes for a ointment for your chest and a general painkiller. Let me look your tail."_

I lift myself the best I could and, after Mrs Bluegrass ushered herself and Lilly out of the room, he fast checked my tail.

_"And another bad news... Your tail is neutered for a time: think you did a stink blast, right?"_ I nod _"Well... Your tailbone are a little weak by that, so you'll have some difficult with it for a while too. For this there's no recipe, only recovering with time and rest."_ 

_"Thank you doc..."_ I say _"People need to push those guys away."_

_"I know, but they do for others exactly what they did to you: push hard, maul and steal."_ he says _"The gods sometimes looks like they gone away, if they allow those kind of people nearby. It's not like there's no work here, there's a plenty for everyone if they wanted. But they are good-for-nothing, you know. They all are rotten fruits."_ he says

_"Maybe there's a way to deal with them Mr. ..."_ I say 

_"Call me Hodgepodge Nearsight..."_

> + ***Hodgepodge Nearsight:***
>    + Mole Doctor (he/him)
>    + South Glens doctor & sage
>    + Sometimes the moral authority of the town
>    + _Cautious,Frantic,Learned_
>         + Point out a danger, real or imagined.
>         + Agree to something dangerous or risky.
>         + Know something useful that applies to the situation.

_"Looks like you are the local sage, also."_ I say

_"People respect me because my past and my knowledge, but I'm too much old to help them otherwise like I'm doing you. Now, go take some rest, you'll need plenty of."_ he says

_"If you know there's any way for me to help you all, just let me know."_ I say

_"He was a great pre captor!"_ says Lilly

_"Preceptor?"_ asked Hodgepodge _"Are you really a Scholar and Seneschal?"_

_"I didn't finished my training, but think I can help teaching the kids while recovering myself. If this doesn't bring trouble for you, Mr and Mrs Bluegrass."_ I say

_"Think Lilly needs to improve her grades, and it will be better if she don't go for the farm for a while."_ says Dandelion

_"Yay! That will be fun, he being our pre captor!"_ says Lilly

_"I'll be proud of... And Lilly, it is Pre-Cep-Tor... Preceptor. Is an old word for teacher."_ I say

_"Thanks!"_ says Dandelion... _"Now, let us go sleep, it's getting late. I'll help you your room, Mr Scholar..."_

_"Thanks... I'm Puffers Underhill."_

_"Alright, mr. Underhill... Not to your bed, everyone, you included Doc. Pip pip."_ says Dandelion, on a motherly severe tone, which make us smile and obey

---

Since then, some days passed, enough I've even forgot to get my travel clothes at Petunia. She sent them back me   via Butterscotch.

While recovering myself, I used the Bluegrass house as a makeshift school for helping their kids, like Lilly, Parsnip, Donovan and some other friends, like the little hamster boy Freckles. My Scholar and Seneschal training helped here: I could teach them based on my old time knowledge, using their books and occasionally my Seneschal Lore tome for passing them what I could.

My pain eased very fast, and some nice herb baths Mrs. Bluegrass ready for me let me feel better. In two days I was ready for some short walk around the two, including a pass on the local library shop, where a nice badger had books that could help...

But I'm still sad with the lost of my father's violin and, aside helping the kids, I felt useless. Until I've thought a little

_"Mrs. Bluegrass, do you know where they normally came to attack? They need to be nearby the road, otherwise a walk from and to too far would be risky for them. Their lair need to be nearby."_ I say

_"As far we know, they attack nearby the road, more or less where you were attacked that night."_ she says, while bringing the stew pot 

_"Well..."_ I say _"Think we'll need to deal with those... I have an idea, but maybe it is riskier... Could you call Oakhoney? I want to see if the city constables can help me."_

---

I pray at the night: I heard about this before, on how small and forgotten gods sometimes provide help. And I remembered some training I had as a Seneschal: to not run, to only fight back if needed, to help the weak. I could feel that I can do it. It would be riskier without my stink, my natural way of protection... But it's needed, and I need to do this.

---

The message I've sent for Jorg Stormbringer:

> _"Jorg Stormbringer:_
>
> _I know you and a bunch of thieves under your command fear me. Fear that I can spread the Word of your Falling from Grace to your previous Brothers at North. You would then be hunted for them and pass through their Judgment._
>
> _So I, Puffers Underhill, Proposes, under the wisdom of the Gods Big and Small, a way to Solve your problem, and probably the town's:_
> 
> _You and your men are abusing people, hurting them. Enough is enough_
> 
> _So, I Conclaim the Right of the Judgment Against You by Duel, under the Old Sacred Rites._
> 
> _You, by Winning, can free yourself from the Burden of your Brothers, and I would not talk about This to anyone. I would also be Proscribed from the Town, and your Tithe demanding would be no more my problem._
> 
> _However, if I Win, You Should put Yourself under a Perpetual Sacred Oath of Fiefdom with this Town, becoming Their Champion, and Protecting Them against Ill. You also will restore the Ill you and those under Your Command had done._
> 
> _I, Myself, Sacredly Challenge You, under My Name, My Believes, and My Stations! The Duel will be To Submission, and the Weapons will be decided by You, as by the Sacred Rites. This will be done on the glen nearby the city, nearby the Old Shrine for the Gods_
> 
> _If you don't reply in Two Days, you'll show Cowardice, and this Will Became your Shame!_
> 
> _Puffers Underhill, previously Seneschal-in-Training_
> 
> _South Glens, 18th Day of Tillsoil, Year of the Berries_"

---

_"Do you think he will came?"_ says Butterscotch

_"He will."_ I say, holding the improvised staff I did for myself _"He's proud and he'll duel with me... My main worry is that there's a nice chance he, or at least his associates, to betray the Duel if there's a chance for a defeat. So, I want you all ready to fight them. Otherwise, don't do anything: the Trial by Duel idea is to conclaim the Sacred to decide who is right, under a way to avoid all-out wars."_ I say, remembering my Seneschal training

_"Let us see... They are coming."_ says Oakhoney, when seeing their gang

_"So..."_ spat Jorg _"Are you thinking you can win one on one against me, weakling."_

_"There's one way to see... You conclaimed the Right of an All-Out duel, with Any Weapons we can use against each other."_ I ask

His look is murderous

_"Yeah!"_ he says, taking a big double-bladed Battle Ax _"My Old Ax need some blood, you know..."_

_"Alright... So, we can use all the weapons and techniques we can, right? As long there's no killings and it's one-on-one."_ I confirm

_"Sure... C'mon, let us dance!"_ says Jorg, smiling very wickedly while we open the circle.

_"Remember this is a Sacred One..."_ I say, when I hear the voices nearby, from the wind, from the Tillsoil small rains, from the wind bringing the words from the old and ancient... (-1 Token)

_"Do it fast, before he can hit you, or he'll betray the Duel!"_ I hear the voice, when Petunia came as the Duel Judge.

_"The Duel will Start Here and Now, and it will be to Submission, with all weapons and techniques both can uses. You had Agreed with All Weapons and Techniques against each other, while two Conditions Would be Upheld: First, no Killings; Second: it will be an One-On-One Duel to Submission. Once one of you is Knocked-Out, Pinned-Out or Forfeit Voluntary, this Duel is Over, and this Matter will be considered Solved Once and For All. Are you agreed with those Terms?"_ she says

We both nod.

_"Now, May the Gods Shows the Truth, May Your Hearts be Truthful with Your Demand, and May Your Fate be Shown Now. Begin!"_ says Petunia, getting out

Jorg gave a big roar, making everyone flinch on my side, and his side cheered. I felt myself right: he was in fact a beast. He started to run against me, brandishing his Battle Ax. 

But I have my own resources: I'm not a big fighter, but I can defend and getting out attacks. I just give some passes before, his big and clumsy ax running around and avoiding him to get and push the blade against its momentum and allowing me to get some safe seconds to run away and dodge the attack, while trying to ready his attacks.

_"C'mon, Coward! You have to attack me out too!"_ he roared in frustration, when I push his ax out of my way with my staff

_"Be my guest... I'm only testing the waters! There's more than one way to win."_ I say

He roared in rage. I know I'm playing with fire: Myrmidons from North sometimes goes berserk. I was counting on this, but needed to do this on the exact moment...

And it was when I saw a small bird flying away with what looks like some remnants of my honey bread.

_"A Message from gods? Perhaps!"_ I said, and notice he was really furious!

_"Now!"_ I said to myself, taking from my pockets a small concoction vial, who looked like an egg and throwing it in the air _"Protect your eyes!"_ I said, hitting it with my staff, and breaking it, releasing a special concoction that, in contact with air, produced a bang of lights that would blast him into a sensorial overload! I could hear his scream in rage, bringing his hand to his eyes, and then I knew I needed to be fast: I used my staff to drop him down the ground and used my feet to kick the battle ax away, then got to the ground and locked his throat on with my tail and released all the few stink I've developed since the previous fight, praying it would be enough to knock him out.

And it was: a massive stink spray straight on your nostrils is enough to make almost anyone go out cold! And after my light bomb, it was enough!

Petunia got nearby:

_"Looks like Jorg has fallen! It's a submission! The Duel ends and Puffers Underhill is victorious!"_ she says

_"Not that fast! We'll not accept this! A stink chimney like him would not won our leader!"_ said one of the weasels...

...holding Lilly!

_"Now, chap. It was fair and square!"_ said Petunia _"They could use all weapons and techniques..."_

_"... this has nothing with us!"_ said the weasel brother _"Now... If you want the girl to be on one piece... We want the skunk tail!"_

I give a look for Lilly, and she looks resolute!

_"Don't give them what they want!"_ she says with her eye.

_"Sorry..."_ I give a look to her _"But I need to do this!"_

And I notice something...

_"You want my tail? Come here: but release her before."_ I say

_"Not that easy, old fart! We are smarter than you think!"_ they say, when someone got behind them

_"If you like your pelt, GET. OUT. HERE!"_ It's Oakhoney, really angry!

They just released Lilly and got to me in fear. 

_"Do you want so much my tail? Why don't you said before?"_ I say and, snaking my tail below their noses, I let them feel the remnants of my stink!

_"Gack! It stinkier than that last time! Let us go away!"_, says the younger brother and the bandoliers ran away, chased by the constables, and leaving his master behind.

_"And him?"_ said Butterscotch, looking for the knocked-out bear _"Can we trust him?"_

_"That you can... You see, he's now bound by the Traditions: as soon he wakes up, he'll be obliged to Swear fiefdom to South Glens Perpetually, and this way he'll be obliged to defend it. As far I know, he'll defend you forever."_ I say, while he wakes up. (-1 Token)

_"You were clever, and won me, fair and square... So, I'll undo the ill I did to South Glens, and I swear I'll always defend it from bandoliers, thief and other scum, to repent my sins from the past."_ he says, getting into his knees and supporting his head over the ax hilt.

_"I'm satisfied... And you?"_ I ask for South Glens inhabitants, that nodded, then I turn myself to Jorg _"As long you uphold this fiefdom, nothing will lack for you. You'll have food, a place to live and anything you need to uphold your fiefdom and be their Champion. And the constable soon will bring your old commanded to justice too."_ 

_"I'm... Glad... Some of your kind put me on this shameful situation, but you are releasing me from this shame. I accept gladly this."_ he says, and old Doc came and says

_"You are now our Champion, to care for us in the defense."_ he says, putting a golden collar on him _"This will always remember you your position, the honor and responsibility of this. Now, raise, Jorg Stormbringer. I, Hodgepodge Nearsight, command thee, by authority grant me by Sough Glens township"_

He raises and we shakes hands...

---

Looks like, in the end, I still can do what I was trained to do, although I'm not ready to call somewhere else home. There's still things to learn, people to look and maybe goes back Furrow Heighs.

_"So... Are you really going?"_ says Dandelion, when we are just having a kind of parting party

_"Yeah... Time to hit the roads again. As Jorg, I'm also on a quest to get in peace with myself, and with the world. And so, I need to learn where I'll be at ease."_ I say

_"You can stay here, Mr. Scholar."_ says Lilly _"We would learn more with you..."_ she says

_"No, dear... I'll always remember you all. Till here, I was just a wanderer. Now... Think I know what I have to do. But before it, I want to know a little more world. Maybe getting even more to south."_

_"Heyia! You're the Scholar chap some people was talking about? I'm Biggia Saltare, from Talmina, at South. Our boat,_ La Dolce Conigglio, _is going down the river today, but we got unlucky our Navigator had fell sick and we can't wait: Monsoon is starting tomorrow and the rains will start soon, we are trying to avoid to be on a bad situation in the Green River, and getting a storm will certainly be one... Whatcha you think? What a ride by replacing our navigator a day or two?"_

_"I would be glad..."_ I say, and see that Lilly looks a little sad _"Believe me, I'll never forgot you."_

_"This for certain."_ came Hodgepodge _"Lilly talked a thing or two about how nice you play the violin. We know those riffraff destroyed it, and we did one for you. As a gift."_ he shows me a violin case, pretty, on redwood and some gilded details. I open and see a very beautiful violin, on redwood and oak, and a nice arc. _"Go on... Take it and play us."_ he smiles

"Talvish, _it's really pretty..._" I whisper, trying to contain my tears, but the violin, as long it's simple, it's pretty on his simplicity. It will never be like my father's one... But I'm really happy to see how this simply violin could be so pretty, coming from those happy and hardworking people. I check the tune and is perfect to a fault! 

_"Hope you don't be too sad, but I'll play_ 'The Winter Wind'." I say, and I start to play... 

It's... Soothing, like never I've felt. I can understand now how my mother loved this too much. It's not about what we miss, but also what we can still dream with, what we know belong to somehow. I still belong to Furrow Heighs somehow, I'm still somehow a Scholar and Seneschal. Not having my staff is just a detail.

I finish to play, and can see they understand the music too: they'll lose me, but we'll not lose each other, not forever, not at a whole. I'll still be somehow connected to South Glens and they will be connected to me.

They applaud and cheer, and before the end of the party, they offered me the sweet berrywine as a parting glass. And then I get my stuff and go for the small port of the town and get to the _La Dolce Conigglio_, and next day we start our voyage, enjoying the small window of clean skies and nice winds...

<!--  LocalWords:  Underhill Tillsoil Asfriteyhi Varst Ack Geroff pre
<!--  LocalWords:  spoilt Seneschal's farmworker Duchacy Heighs Heyia
<!--  LocalWords:  Hardworker Maths teached Nearsight Yay Cep mr Gack
<!--  LocalWords:  Conclaim conclaim conclaimed C'mon sensorial 'The
<!--  LocalWords:  Biggia Saltare Talmina Conigglio Whatcha Talvish
<!--  LocalWords:  Wind' berrywine
 -->
