---
layout: contos
title: "Puffers Underhill - Chapter 5 - Swarming: Ottershire"
subtitle: A Wanderhome Story
category: stories
excerpt_separator: <!-- excerpt -->
---

> + Swarming
>     + Dewdrops On Leaf Tips
>     + Constant Distant Buzzing
>     + Ornate And Elaborate Displays
>     + A Whistled Tune
>     + Shepherds And Their Flocks
>     + The Building Of Cairns
>     + A Festival Honoring The Local Herds And The Sustenance They Provide
> + Port
>     + Hot Air Balloons
>     + Smug Captains
>     + The Eastern Mist And The Trouble It Brings (Folklore)
> + Castle
>     + Trophies From Long-Forgotten Wars
>     + Watchtowers
>     + The Murdered Brother (Folklore)
> + Hillock
>     + A Sheltered Valley
>     + Scattered Fragments Of Civilization
>     + The Fallen Star And The Stories She Told (Folklore)

<!-- excerpt -->
    
_"Thank you, Sabrinna!"_ I hug my old fellow when she was getting back to Preccia, through the Old Channel Sea

_"I'll miss you,_ Gamberetto. _But... I believe here is where we part ways, at Ferretington. I want to go fast now Talmina's businesses that I should look upon had ended... and the Eastern Mist is rising, and all the barnacles and problems comes really fast with it. Better avoid it."_ she says

_"Good voyage, Sabrinna!"_ I say, while she gets back to the _Il Ratto delle vendite_ and shaking her hands, announces the departure.

_"Had some great adventures at Talmina... Hope that things are a little calmer here at Harringland."_ I say, looking around.

The Ferretington port is full of activity: commercial ships can be saw aside diplomatic ones and even one or another that looks really fishy, looking like pirates. Traders are selling their stuff and the smell of fish was prevalent all around, either raw and fried with a side of fried potatoes and sauce, fried on the fish's fat. I choose to take some: I still have some money _Regina Conigglia_ provided me and I've exchanged first thing at Harringland. I walk on the streets, bumbles being seen all around, buzzing at the rhythm of the flutes and weird big tambourines and drums, dancing with street jesters and clowns, as Spawning is a very important time for those important providers.

I decide to put the money _Regina Melina Conigglia_ gave to me on good use and reinforce my provisions and stuff. I got some new clothes, more appropriated to the place, a Harringland map, some travel food, a new, bigger, set of backpack and satchel and a new walking stick. Even liking the _Maestro_ sigil, I chose to put it back to my backpack. _"Better walk again as a simple wanderer for a while"_, I say.

Then, after ready myself again, I walk a little, when in the park, I see someone, inside a balloon basket, made from wicker, announcing _"The Greatest Adventure of your time. A travel faster than via Snails or Bumble-ox! Ferretington to Ottershire in a day!"_. The kith is a great dane girl on crazy clothes: leather trousers, shirt hold to her body with belts, big boots and a leather apron, and a big weird set of glasses over her forehead. holding her big ears a little. Behind her, I could see a very pretty and colorful balloon being filled. 

_"Heyia, chap!"_ she says, looking to me _"Maybe you're looking for a travel, right? There's lot of things to be seen at North: Ottershire is a nice place, you know, very tranquil for some Wanderers like you!"_

_"Hum... How much it will cost for a ride?"_ I ask, curious on fly a balloon

_"Ten Herrings, and I'll throw some air-clothes for you for 5 more."_ she says.

It's a little expansive, but not anything I could not deal with, so I take from my pouch some coins and gave the great dane lady.

_"Now, c'mon. We can depart! Ah, I'm Willa Dane."_ she greets me, while I got to the balloon basket.

> + ***Wilhelmina "Willa" Amelia-Dane, balloonist (she/her)***
>     + Great Dane Balloonist, flies a route Ferretington/Ottershire
>     + A free spirit, likes to live from a place to another on her balloon
>     + _Adventurous, Crafty, Watchful, Passionate_
>         + Declare where you’re going next.
>         + Have circumstances improbably work out for you.
>         + Reveal that a plan they’ve had in motion has paid off.
>         + Point out something people missed.
>         + Say exactly what’s on your mind right now.

_"Puffers Underhill. I'm from Furrow Heighs"_ I say

_"Whoa! You walked a lot. If I'm right, you came from... Talmina? Aside this it would be a big of travel."_ she says, passing me a leather jacket and glasses like the ones she's using. _"If you want, I can provide you a hat too"_ she says, showing me a bumble-stalker hat. I take it

_"Yeah... Just got down the ship. I just took some fish and chips as food. Nice stuff."_ I say

_"Take care then... Hope you aren't easy to go sick. And if you want to puke, puke outside the basket."_ she says. The basket is not exactly big, a 4 meter sided basket with a 1.5 meter wall. I could see the air burner, now really active, fixed by the basket with steel structure. Below, in the basket corners, some canisters on what looks like gas

_"Swamp gas. Best to be used, almost pure methane. Not that cheap, though. This voyage is my last one before refill the canisters at Ottershire, one of the only places where you can find natural swamp gas harvested, so really cheap and abundant."_ she says. _"And better we go. The Eastern Mist brings lots of difficult even in the sky, it's easy to lose yourself, even in the sky. And with some of the worst big wasps, better safe them sorry. Had saw some nasty balloon accidents cause by envelopes ripped apart"_

_"Ready when you are!"_ I say, putting the jacket, the bumble-stalker hat and the glasses over my forehead. 

Then she says _"Here we go!"_ She smile and asks the kith nearby to release the ropes while she bring them back, with my help. I could feel the basket starts to raise, just a centimeter, till she pulled one of the burners levers and this release more of the gas, what makes the fire intensifies, filling even more the envelope as she says.

_"Now the envelope are really full!"_ she says, when I notice the balloon raising fast thanks the hot air: one, two, ten... In some minutes, I can see almost all area like a small tapestry high in the sky, almost a hundred meter over.

_"Now... It will be 24 hours, but we'll do some stops: I don't know about you, but never found a kith that could go 24 hours without go potty. We'll get down on some balloon ports in the way, at Cherry-Over-Cricket and Badgerton before getting at Ottershire, right? We'll sleep today on Badgerton's balloon port. Nice place there, everything right we'll arrive there before midnight."_ she says, while lowing the glasses _"Better low the glasses, the cold air can be really hurtful for eyes."_

_"Alright..."_ I say, looking below, and could see the swarms of bumbles and moths over all around while we got over the buildings and trees. I look admired.

_"Never saw this before!_ Asvreth! _So beautiful!"_ I say

_"And you're lucky: looks like this year will be the cicada season. It's really lucky. Some people thinks cicadas are small gods themselves, gods of luck, or at least emissaries of the God of Luck,_ Laughton." says Willa

_"How you become a balloonist?"_ I say, curious _"As far I know, people needs to be..."_

_"Crazy to fly? There's people that think only moths and other critters would fly. But we kith can fly, with balloons, this is know for lots of time but only 100 years before people started to do it. In the end of the day, it's like in the sea and rivers. Had you any time into a boat at Monsoon?"_ she asks

_"Don't say me..."_ I almost got sick only by remembering my time at _La Dulce Conigglio_ after South Glens

_"Looks like you had a rough rodeo before, right?"_ she asks, and I say here my experience

_"Yeah... Not what I could say was a good voyage"_ I say

_"So, as in sea and river, in the air there's danger and demands care and caution. No place is safe for those stupid and careless. Being an adventurous one is one thing, but even Lemmings knows the difference between being adventurous and being suicidal."_ she says

_"But you didn't talked about you yet."_ I say

_"Well, you see: Harringlander society is so much..._ Strict. _A woman like me only had space on a common, good,_ eligible _society if you are ready to accept becoming a kitten factory."_ she spat _"Thankfully, my father had other ideas, and teached us, me and my other brothers and sisters, the same way."_ Then I notice she has a rapier _"Yeah, I fence, and ride horses, and my brothers know how to sew and cook. It was father that teached us how to fly a balloon and I fell in love by this"_ she says _"Now my father is in Haatheestan, working some business for Your Grace Minerva II and I'm here, travelling with skunk wanderers"_ she giggles

I look around and I can see there's loads of other balloons all around.

_"Whoa! Looks like people here like balloons."_ I say

_"It's the season. Some people, specially nobles, don't like the Swarming because all the critters, especially because all the smell and buzzing of the bumbles. Even more this year, with all the cicadas. The Nobles shun this as commoner stuff, I know because my father dealt with them for a while, and I would deal with them too if I didn't ran away with one of his balloons."_ she says

_"I understand what the Nobles think... Although don't liking it."_ I say

_"And so, they have their big balloons, with their own balloonists, and big baskets with servants and stuff, and even some chamber pots so they can relieve themselves and just shove their contents in the sky to fall down without caring if get under others heads... And I saw this!"_ she says.

_"What a_ Varst!" I say

_"Yeah... But, otherwise this is time some of the most poor also take some coins to just have an hour fly, to see what you can see now below. Sometimes, when I'm with full canisters to give some rides for kitten. Believe me, they never forget the view, like you'll never forget."_ she says

I can see below lots and lots and lots small yellow points, like pollen, and the small cicadas swarms looks like a criss-cross of gold over a green back, with a small fog, and just the beauty, like a multitude of small gods just got to dance around the fields and glens and meadows all around. (+1 Token: 2)

_"Yes... This is so pretty... I now can understand those who wants to fly. I never saw this kind of thing in the ground... And I saw a lot of things since I've got from Furrow Heighs."_ I say, appreciating the view for some moments, when Willa said

_"Heyia, I know it's beautiful, but you don't want to fall out the basket, right? Get back!"_ she smiled, pulling me gently back from my unsafe position and I notice that I was almost throwing myself out the basket! I get back, and, after getting worried, I smile and we laugh ourselves heartily

It will be a great voyage.

---

Some of the local stablekith are readying everything for us.

_"This is our last stop till get Ottershire."_ says Willa. _"Old_ Cair Mastiff _is a pretty place to give a look, but, to be honest, I'll never get inside there for my dear life!"_ she says, installing the envelope parachute that would help during landing, while I helped opening the balloon envelope with stablekith.

We positioned the burners in the ground so the stablekiths could inflate the envelope to go up, and in the meanwhile we could enjoy the nice breakfast the badger lady from the balloon port provided us as part of our bread and breakfast stay: tomatoes, beans, fried cricket eggs, bumble sausage, bread and a strong coffee sweetened with honey.

_"Why?"_ I ask

_"Had you heard about The Two Paws War?"_ asked Willa

_"Sounds like Harringlander specific stuff... Sorry, I'm a Scholar, even a_ Maestro di Talmina, _but even I has limits."_ I say

_"No problem, and you're right, but it was during the National Unification Times, I thought it was at least mentioned outside"_ she says

_"Perhaps I lost this class."_ I said, while cutting the cricket eggs and ate a little

_"Ottershire was always a... Not exactly rich... Place. Big green fields, nice view, but with lots of hills and other places that demands a lot of care if you want to grow stuff. Better for Bumble cattling than for cropping."_ she says _"But it was important, as it's half way to_ Vaer, _the kingdom of the Old Cults, that were shunned by Harringland, so a good post for nation's Army, which helped in Ottershire's treasure with all the noble kith that came and could spend some money on taverns and entertainment, approved or not. It was when the brothers Auberon and Falstaff Mastiff had an argument against each other: Auberon was totally pro-Harringland, or so it was though, accepting all commands of Rabbiton almost as a second nature. His brother Falstaff, otherwise, thought they should at least trade with_ Vaer _that could provide them some imports they needed, faster and easier than Rabbiton could, like some copper, steel, tin and hardwoods."_ 

_"It was when, after some diplomatic blunders from part to part, the King at time, Jeoffrey Rabbiton, passed a Law of The Land, commanded that all Vaerian business and contracts in Harringland territory were rendered void and null. Falstaff wasn't exactly against that, but he wanted at least to preserve the family honor and finish the deals still on hold before close the doors and tried to at least finish those deals correctly. The Vaerian understood, as it was expected and... Anyway... They would receive for the goods sold till then. Fair and Square."_ she continued _"But Auberon was greedier and saw on this an opportunity: he closed the limits of Ottershire, holding the Vaerian merchants here officially as Hostage of the Court. Then he pushed them away only on their own clothes, upholding their goods and effects as Claims of the Crown!"_

_"Talk about an insult!"_ I say, smirking, while smearing a small portion of toast on the eggs yoke

_"It was way worse, because the Vaerian then demanded satisfaction from_ Cair Mastiff. _Falstaff begged Rabbiton to go for and negotiate_ Cair Mastiff _out of the mess his brother put they on."_ she says _"And he was allowed... Or this is what was said."_

_"What happened?"_ he said

_"You see: even after Vaer breaking relations with Harringland and Ottershire, Falstaff himself was still under a good name with both Vaer and Rabbiton, and as the tensions were cooling, rumors said he was chosen as diplomat to Vaer to cut a deal between both sides for renew relations."_ she took a small piece of sausage and then continued 

_"But it wasn't what happened."_ she say _"After just a day on travel, he was attacked by highwaymen and killed still on our side: he wasn't expecting that, he was only with his steward, that had run in despair and, after some days surviving on berries in the forest, got safely through Vaerian frontier and was sent to their capital, where he could give testimony."_

_"In meanwhile, Auberon sent message to Rabbiton court, calling about 'Vaerian savages' slaughtering his brother. The rabbit pawn, symbol and crest of Harringland, flown around. War came soon, the badger pawn symbol of Vaerian flew coming, and swords clashed for some weeks."_ she says

_"Meanwhile, the steward arrived at Ballyford, Vaerian capital, with news: it was_ Auberon'_s forces that killed his brother, not the ones from Vaer. Secret messages where exchanged using some spies and scouts and Vaer, until second order, upheld the steward as noble hostage, to avoid problems. Think you know about this tradition."_ she asks

_"Yeah: when two countries got into peace treaties after a long time of war, normally part of their lesser nobility are drafted and exchanged for some time on the other side as an insurance of one side will not go into an all-out war with the other. In general are well-cared and respect, but aren't allowed to go from the places where they were assigned, usually castles from local noble houses"_ I reply

_"Exactly this one. And so the messages from Harringland spies about this came to Rabbiton. The King, furious with Auberon's folly, called him to Rabbiton. In meanwhile, some Vaerian scouts, working with the Harringlander spies  brought secretly the steward to Harringland and so he could go for Rabbiton. To be honest, no one aside Auberon wanted that war, it was decimating both countries resources and lots of lives were lost from part to part. It's said that the fear of Auberon when he saw the Steward and discovered his folly was discovered became so strong that his fear scent became a miasma on_ Rabbiton Castle, _King's palace. And the steward gave his report. The King saw the truth under the steward's words, even more when he brought back the King Seal that He himself gave to Falstaff under secrecy as his emmisary, and his wrath against Auberon was terrible! Accused on High Treason and Conspiracy, the King condemned him for Capital Punishment, by Lapidation. You see, behind the Belltower of_ Cair Mastiff, _there's a big, old, limestone deposit, and with time and the needs, it became sharp after all mining there. The King itself pushed Auberon through the tower, and with the impact to the limestone and with all the sharp pieces all around and with rolling around, Auberon was killed, and the blowflies feasted on his body, his body denied the funeral rites by Old Traditions and by the Laws of the Land... And Auberon's folly made the Mastiff lineage be finished."_ she states, while taking a sip of coffee

_"However, this didn't finished the old ruse between Vaer and Harringland, only quenched it a bit. You see, one of the items that Auberon held on his folly was never found:  a very rare_ Trisquel, _made with cicada hide and emerald. It's said it was being brought for being exposed at the main Harringlander cities as a toke of peace. Some says that this powerful amulet was taken to remove the Vaerian protection from Old Gods. Some says that Auberon was just childish on getting a so important item of Vaer, like a provocation, on his prejudices against Vaer. But... No one had never found it. Some at Harringland even says that this Trisquel never existed, no matter how much Vaer says about his till today. We even says here at Harringland that something is impossible to be done when we say that it will be finished_ 'when Vaerian's Trisquel is found'" she says, finishing his food

_"Interesting... I can say that I have my fair share on this stuff, some experiences with follies and mystic myself, and this sounds... Interesting. Maybe I'll give a look at_ Castle Mastiff. _I don't think that it will have any ill on this"_ I say, finishing my food and going to the potty to relieve my bladder before getting the last balloon leg

---

After we arrived on an Ottershire farm, Willa chose to undergone an Arriving Ceremony.

_"This is a way to thank the gods you got to the skies and back safely. It's done on the end of someone's first travel."_ she says, while an old mole farmlady got a new elderberry wine's bladder and opened it for us, to drink with some bumble cheese, small scones, finger food, and dark wheat bread. Her small farm, with some crickets and bumble and a small vegetable crop make shift as a balloon port for Willa, as she said me.

The old farmlady fills three glasses with the wind and then put the glasses down and passes me a glass full of the elderberry wine, and another to Willa. We drank it and she says _"Repeat with me!"_

> _"The winds have welcomed me with softness._
> _The sun has blessed me with its warm hands._
> _We have flown so high and so well that the gods_
> _has joined me in laughter and set me gently_
> _back into the loving arms of mother Haeth._
> _Hear Hear!”_

I do as she said and repeated the prayer, feeling happy on the new experience.

_"So... You're a Scholar from too far?"_ says the old woman, while bringing us back and some stablekith were caring for store the balloon

_"Yeah. I was in training on my homeland, Furrow Heighs, but my recognition came as a_ Maestro _from Talmina"_ I say

_"You did a great journey, my dear. From Furrow Heighs to Talmina, to Ferretington, to Ottershire! What a voyage! And what you want to do here at Ottershire? We are only an old passageway through the Rabbit Plains and Hills to Vaer, and very few cross those roads nowadays. We live basically on our small cattle and some crops."_ she says

_"I had no idea on what to do at first, but Willa talked me about_ Cair Mastiff'_s story. Is it truth that they never found Vaer Trisquel?"_ I ask

_"To be honest... I think Vaer people are bonkers. It would be an extreme folly to believe Lord Auberon had stolen something that important from them just by sport."_ she says

_"Well, the guy killed his brother, it's folly enough for me and sounds like a thing someone would not do by sport."_ I say

_"Anyway... I don't think this Trisquel is there: my farm is the nearer from_ Cair Mastiff, _aside the King's fields where no one can set foot by Law of the Land, and I saw lots and lots of people look there for the Trisquel, none of them successful. I can't stop you trying, my dear, but believe me, you are not the first and, in my opinion, will not be the last"_ she states

_"That can be, but I want at least give a look. In the end of the day, at least I can see the castle."_ I say

_"If you think this way, you can really enjoy the Castle: I had only once there, as a kitten, when I was... Nine?"_ she tries to remember "Cair Mastiff _is really pretty, and well cared by Rabbiton stewards. Young Lipton Harrington is the  steward nowadays, a great chap. Got to learn with the best professors at Venisonton and Rugby and became Steward to have the right of study the Castle. Very smart, but a really jumpy fellow!"_ she says, giggling _"He also believes the Trisquel is still hidden somewhere on Cair Mastiff. He claims there's some old details that could help, but no one found it yet. There's even a Law of the Land that conclaim that finding the Trisquel would give for the one who accomplish the task Barony on Ottershire."_

_"Well... Who knows, but for me what would be interesting is to give back something that was lost for their rightful owners."_ I say

_"So. At least, take the night here before getting the road: even on Bright here is cold."_ she says

_"I'm accomplished with cold, but anyway I'm glad for your hospitality. Can I tell you some of my stories?"_ I say, as something every wanderer know as a Rule of Hospitality: in some places, stories are as good as money to pay for a stay.

---

Next day, I got my stuff and thanked the old mole lady for the night: the straw bed was so comfortable I had to do my best to not got too comfortable and release my stink in the sleep. And I greeted Willa that now would wait his Swamp Gas canisters to be filled, so she could get back Ferretington. The old lady even gave some black wheat bread, butter and honey. 

_"It's for you to share with Mr Harrington. If you are not a slack, you'll get there in lunch time."_ she says, while I start to walk.

And I'm not disappointed: whatever I could walk, there was them: the cicadas, spread all around, their buzz filling my ears all the walk. Like a song. Like they wanted something on _Cair Mastiff_.

I get some hours and arrives at midday to the front of what looked like an old gate, some guys on Harringlander Army attire playing chess and looking around. 

_"Who are you?"_ says one of them, hand to the sword, trying to not show he was slacking by the utterly total absence of action there. He spoke on common, but the accent from Harringland is really strong

_"I'm a wanderer, Puffers Underhill. I want to look_ Cair Mastiff, _the legend about it sounds really interesting."_ I say

_"Let me look you!"_ says the one with the sword, a beagle no older than 18, while his partner, a little older Owl, got for a crossbow.

_"You don't need to be that tense: I'm just a wanderer, I'm not a danger."_ I say

_"This is up to us, mate!"_ says the beagle, searching my stuff. Until they found it on my stuff.

_"Shucks! He's... A_ Talminare Maestro!" says the Owl one in fear, when the beagle show him my _Maestro_ sigil, the one given to me by _Regina Conigglia_ herself

_"Sorry if I didn't talked about this before, but I'm a_ Maestro _from Talmina, after leaving my Scholarship at Furrow Heigh. Don't worry, I just want to Iva a walk"_ I say, when he give the sigil back

_"Who is a Talmina_ Maestro?" says a nervous voice. It is a nice looking rabbit, a little chubby, with glasses and a formal suit made of tweed, but on a checkered pattern, their pants of a maroon color, gaiters covering his shoes. He has a dark caramel colored fur, with some black and light caramel spots

> + ***Lipton Harrington (He/Him)***
>     + Rabbit Scholar from Venisonton and Rugby Colleges, _Cair Mastiff_  Current steward
>     + Somewhat jumpy, but a very careful research on _Cair Mastiff_, believe the Trisquel is still there
>     + _Ambitious, Inquisitive, Learned, Proper_
>         + Take a calculated risk
>         + Focus on an irrelevant detail.
>         + Ask: “What’s this?”
>         + Reference a text no one else here has read.
>         + Explain how things have been handled in the past.

_"It's me... I believe you're Mr. Harrington. I'm Puffers Underhill,_ Talminare Maestro _sometimes, but most of time just a wanderer trying to learn about secrets and wonders at Haeth."_ I nod

_"Oh. Nice to discover a fellow Scholar for a change. "_ He squeals, part in anxiety and part on excitement _"He can get inside: as a Steward I'm responsible for our guest's behaviour."_ he says as a matter of fact to the guards.

_"Aye, sir."_ said the Owl guard, on a protocol

_"How do you know about me? I saw Willa balloon arriving yesterday. You came from Ferretington? From Preccia, probably? Got some reports from friends there that said about the fall of_ Castello Porcinni _and the_ Buonnandanti _walking again over Talmina."_ he said, excited and nervously while we walked to something like an old game keeper cottage. _"Sorry, but unless with Royal Seal or other statement from government, stay on_ Cair Mastiff _at night is off-limits, so any guest need to sleep with us in the gamekeeper cottage."_

_"No problem. The one that talked about you to me was the old lady from the balloon port."_ I say, while he opened the door and we got inside.

_"Ah, old lady Molasses!"_ he says, and he notice my intrigued face _"Hadn't you asked? Yeah, this is his name, Shirley Molasses. And I can smell she gave you our provisions for the week."_

_"Yeah!"_ I say, giving him the bundle she gave me _"Bread, honey and butter."_ I say

_"Wow..."_ I can see him smell the bundle, smiling happily, even her cotton-puff tail shaking in happiness with his snout twitch feeling the smell _"It's incredible! Remember me home! Hey, do you want to join us on a special tea picnic today? We had been granted authorization for a special night meal at the Bell Tower!"_

_"I heard it was there Old Baron Auberon was lapidated by being thrown there by the Old King."_ I say

_"Ah, looks like Willa talked you about the history of the Two Pawns War!"_ he exclaimed, excited

_"Yeah. It was an interesting story!"_ I say

_"Ah, it's good. But I already investigated all that I could about this story, and found some artifacts I've discovered recently and sent to Rugby and Venisonton. One of them was something that was believed as lost: old Baron Auberon's diary!"_ he said, while storing the food and showing some notes. The gamekeeper cottage is simple, with few utensils, but clean and cozy

_"What I've discovered is: Auberon wanted to use the War, in fact, as a way to profit... In his mind, he could use a possible invasion of Vaer as a way to weaken Rabbiton's power, and maybe escalate things in a way that he could topple Rabbiton for himself."_ he says

_"Whoa, talk about a big head!"_ I say

_"Yeah! Falstaff death was just a way for him to heaten things against Vaer, and maybe try a way to push_ Vaer _aside, making them to go and invade Harringland via Ferretington via sea. Vaer wasn't as strong as us at sea, but as our sea forces where, at time, spread around Haeth defending the crown's interests and fighting pirates and other scum from all around Haeth, Vaer could give a shot to invade and topple Rabbiton, making them refuge into Ottershire, where he would topple the King itself."_ he says

_"But all was flushed down thanks Falstaff's steward arrive at Ballyford, right?"_ I say

_"Yeah. His diary came with him when he was to be executed, his last days on the old Baron's quarters, now his prison, all registered on the diary. This fact is not exactly public, but there was registries of this from the Crown's executor at time."_ he says, serving some lavender tea with scones

_"Why people didn't looked for the diary before?"_ I ask

_"There was all kind of legends about them, about they fight each other as ghosts, about chains and noises all around. You know, that kind of fairy-tale governess tell to make kitten behave."_ he says _"I'm not exactly courageous, you know, but I don't believe on small gods, or ghosts, spirits or other stuff like that."_

_"Well... I can say a had a fair share of experiences with ghosts and stuff, but this doesn't matter."_ I say _"And what is all this stuff involved with the Trisquel?"_

_"You see, there was loads of stories about this Trisquel and how magically powerful it was: it was an ancient relic from Vaerian history. It was said that, before their Unification, the Trisquel itself determined the kingdom with primacy over the other at Vaer. There's loads of legends, histories, registries and anecdotes involving the Trisquel. And probably Auberon knew them: no matter how big his ego was, he wasn't exactly stupid. His diary instead states that"_ he show me some of his notes "'I found it, the Emerald Trisquel, and believe me, this is the key for my Kingmaking!' _He believed that the Trisquel could give him some luck, or magical powers, or anything like that, so he could become Harringland King."_

_"Well... Or this Trisquel wasn't the one, or the Trisquel only sent him to death."_ I say

_"And this is interesting, because, the last words in the diary were_ 'I have it with me. He'll let me vanquish Death, like on the legends.' _Some of the stories involving the Trisquel says he was a so powerful charm that could even prevent death itself!"_ states Lipton

_"Sounds a little too much for me..."_ I say. _"Because... If so... Hadn't confirmed their death?"_

_"They confirmed, right. There's registries on executor's diary of the confirmation: they found his body, stinking from putrefaction, partially eaten by carrion and flies. The King's order was to leave him behind: as Supreme Chief of Harringland Church, a dissidence of_ Talminare Chiesa _many here follow, his world is took as Law also on spiritual issues, and with the execution as traitor he forfeited by consensus any rights of post-mortem rites."_ he says

_"I know this is local stuff... But find it a little barbaric."_ I say

_"Me too..."_ he says _"And as the body fell on a not too good place to be rescued, thanks the hot limestone and slate pieces, they just left it there... It's possible to see his body like a scary scarecrow. Some says even that, as we are nearby roads, this made him become accursed to protect the same lands he exploited so much, until when Vaerian's Trisquel is found."_ he states

_"So, forever: I know the expression, Willa said me."_ I say

_"Now, let us ready everything for the night."_ He says, changing topics _"Could you help us?"_

_"I would be honored."_ I say, when he pushed me a black bumble sausage, to cut for sandwiches

---

The night fell, and we could take our special meal. Each of us brought  a basket and we set up us in the best view we could found.

And I'm not disappointed: from there, I could see Rabbit Plains and Hills on all their glory, lots and lots of small lights on small cottages on small farms that produced wheat and tomatoes and potatoes and aubergines, lots of bumbles buzzing around while eating and growing to provide milk and meat. And there was the cicadas, like gold dust sprinkled over the place by the Gods. It's so pretty, I can't hold my heart on happiness. (+1 Token: 3)

_"Yeah, Willa was right!"_ I confirm _"It is so beautiful. What a vision! No matter what, the travel is paid"_

_"Isn't it?"_ says the Beagle soldier, Roland _"It's my first year here: they sent me because I didn't was good enough for the Army outposts overseas!"_

_"My second one here:"_ says the Owl one, Oswald _"Last year rained, it was a bore, one of those big rains that looks like Monsoon left behind, with all the rain and fog. It was a so boring time that even playing music sounds dull."_ he says, taking a mandolin and starting to play a melancholic music... Like _Winter Winds_

_"What pretty!"_ I say _"What is this music?"_

_"It's called_ The Brother and Enemies, _and it's all about Auberon Mastiff's folly, his brother's nobility on looking for a bloodless solution, and how old Brother was guilty of blood."_ says Oswald

_"Can I play one from the north?"_ I say, taking the violin I got from _South Glens_ and tuned it _"It's as melancholic as_ The Brothers and Enemies, _and it's called_ Winter Winds. _It's said that is about a kith asking the gods about how he would live in the unforgiving North, and the gods saying him that he would be give the strength to endure the nature, if he worked with her, not to conquer her."_ I say and, after checking and tune the violin, I start.

They look upon to me, hearing the music, and then they notice something:

_"The cicadas!"_ gasped Roland, while the small gold lights from the cicadas grows around us, like they are trying to say something to us

_"Looks like they liked your music, Mr. Underhill!"_ replies Lipton, when we see another thing

_"Look, sir!"_ says Oswald, looking for a letter-moth coming, and releasing a special letter _"It has Venisonton College Seal!"_

_"Perhaps is the answer for my Question!"_ he says, while I finish my music and he opens the seal

_"Question?"_ I ask, while he read

_"In fact, it is. Remember the drawings I shown you on Auberon's diary, that looks like the Trisquel? I copied the drawings and sent it to other Scholars on the Trisquel legend, and... Looks like it's really the Trisquel!"_ he says

_"So... If what you said is right? Auberon at least got the Trisquel once..."_ I say

_"And, if so..."_ he says, very excited _"There's a real chance the Trisquel is with his remnants!"_

It was when the cicadas came nearby and made it really gilded!

_"So... Looks like you came for us for some reason, right?"_ I ask (-1 Token: 3 - Asking the Cicadas)

_"The time has come."_ an ethereal voice come and started to talk so everyone could hear _"The old folly is to be cleansed, the Symbol is to be returned to his rightful owners."_

_"Looks like they want us to find the Trisquel!"_ says Lipton _"Let us go!"_

_"Sorry sir!"_ says Roland, posting in front of us _"But you know the way to the deposit is out-of-bound to everyone without the Royal Seal by Law of the Land."_ 

_"Nonsense, boy! It's a really chance to reinstate full relations to Vaer!"_ says Lipton

_"I'm sorry sir! Steward or not, we have explicit orders about the out-of-bounds places by Law of the Land."_ he raised the crossbow _"If you are going, we'll need to stop you. Please, don't make us do this."_

_"I'm the one to be sorry, boys. Just don't be too near the walls."_ I say and, turning my back to them, I sprayed them with my stink, enough to make them cough and knock down. (-1 Token: 2)

_"Ack... What a smell!"_ said Lipton that was behind me but feeling some of my miasma _"What you did?"_

_"We have a couple of hours before they wake up! It's the time we have to find the Trisquel, if so."_ I say _"Let us go!"_

---

We started to go down, but we just arrived in a closed door

_"Never got there! Goddammit! I don't have the keys!"_ Lipton says

_"Looks like the cicadas are really gods of luck. O Small Gods of Luck, I Claim Thee for some luck, so the Symbol can be returned to their own owners!"_ I say to the sky, and I look stupid...

...When we see a place glowing (-1 Token: 1 - cicada luck), showing what looks like a special hidden container.

_"It's a hidden compartment!"_ he said _"No one ever found it, I can't remember anything about this on the lore I've collected on_ Cair Mastiff, and I know it forward to back and forward again!" he says

I smile

_"Looks like we are learning to trust somewhat the small gods and that no one knows everything."_ I say, when we use some techniques to open the compartment and find the key.

When we get out, we see a really pretty thing.

Cicadas...

Lots of Cicadas...

Golden, small points of light, but so much that looks like night become day!

_"Whoa!"_ is all I can say

_"You're right, Mr Underhill!"_ says Lipton. _"The small gods of luck want us to find the Trisquel!"_

_"And they want to show us the way... Perhaps even with all safety through the limestone slates!"_ I state

_"Let us follow them, so!"_ he says

And then, we started to follow and walk through a way, like the cicadas are pointing us the most safe, even not the faster, way through. (+1 Token: 2 - stay a little with the cicadas). 

_"Hope they are getting nearby Auberon's body. If I know my stink, and I know it enough, they will wake up soon. Perhaps they even had wake up already!"_ I say

_"This is bad!"_ stated Lipton _"If so, they'll try to capture us! Their orders are express, from the King himself, by Law of the Land, and override any kind of power I have as Steward!"_

_"Let us trust the cicadas: think they want us to solve this."_ I state, while we continue on the way, even Lipton being nervous with the idea of being arrested by Oswald and Roland.

But we find it! Auberon's Skeleton, still on his beautiful vests!

_"They wanted his body to be a state that he was a traitor, so they made him be dead with the most regal clothes he had."_ stated Lipton

_"Now... Let us give a look."_ I say, and after a fast pray for Auberon, we started to lift his body!

_"YOU!"_ says a voice _"RELEASE THIS BODY! IT'S DESECRATION!"_

It was Roland, with Oswald with him. Roland was already with his sword unleashed, while Oswald was with the crossbow locked and loader on us!

_"This is not desecration: he hadn't a formal burial and hadn't rights for burial rights as a traitor, so saying about desecration is nonsense!"_ I state. 

_"Stop sir!"_ shouted Roland _"This is a folly, a balderdash, the Trisquel doesn't exists!"_

_"If I don't find it"_ he says, with a twitching nose _"you can arrest me! I only ask you a chance. Ten minutes is all I ask you!"_

Oswald looks on the fear of Lipton, but at the same time the confidence he had he was right.

_"Alright. Ten minutes, not a second more!"_ says Oswald, taking a pocket watch

I go to help them, and notice we were not finding it on pockets, on chains, on anything...

_"We are out of luck."_ I say, down

_"No! Not now! It's not about Barony! It's... It's knowledge, it's historical repairing!"_ yelled Lipton, almost taking Auberon's jacket

_"Sir, your time is up!"_ says Oswald

_"NO! I'm so near!"_ shouts Lipton, resolute

_"Get out, sir!"_ states Oswald

_"NO!"_ screams Lipton

_"I have no choice, sir..."_ says Oswald sadly, shooting the crossbow bolt!

It is again the sheer luck of the cicadas comes to us again! (-1 Token: 0)

Just when Oswald finishes to get the jacket out Auberon's skeleton, Oswald crossbow bolt crossed by it, ripping the jacket

_"What you had in mind, Oswald?"_ shouts Lipton, when we hear a sound of something hitting the ground.

A small amulet, green and yellow, a circle with some semi-circles shaped inside it and getting outside.

_"What is this?"_ I ask, almost knowing the answer.

_"The Trisquel..."_ gasps Lipton when the guards came and then he scream _"IT IS IT, IT'S THE TRISQUEL!"_

Roland and Oswald looks as surprised as us!

Lipton looks to him with a gleam in the eye... Not greed, like normally you would see, but with happiness of someone who did his job.

_"It is it! It is it!"_ he says _"Now I can prove I'm not crazy... I always thought he existed!"_

_"Better so we go now!"_ I say _"This needs to be brought back their rightful owners!"_

_"Aye!"_ replies Oswald _"This is Crown's treasure. We need to bring it Rabbiton and let the King take the decision on what will happen to it as fast as we can."_

_"But the time of cicadas is one of the worst times for travel on roads at night! Highwaymen are all around!"_ states in fear Roland _"How would we go?"_

I then get an idea. Crazy, but it could work

_"Any of you have any problem with flying?"_ I ask

---

It was a crazy stuff

That night, we got to Mrs. Molasses house, and she gasped seeing everyone that should be at _Cair Mastiff_ almost putting her front door down, past after midnight, Willa looking for us.

And when we got inside (Mrs. Molasses trying to avoid the ruckus), we explained what happened and our plan.

I helped Willa to fill the balloon envelope as fast as possible and, with the last canisters she had, and we took an express voyage to Rabbiton: sixteen tiring non-stop hours were she let me know the best to help her hold the balloon at the air, enough to make us go for potty as soon we touched ground when we were received on Rabbiton Castle balloon port.

This happened only because, as soon we were read to go, Roland (that knew a little this and that about moth-lettering as part of his functions at Army) sent a moth-letter to Rabbiton Castle, on a special cypher he knew.

And now, after some rest, food, bath and put us presentable, 36 hours after finding the Trisquel, we are in front of the Queen Minerva Rabbiton II at her Royal Room

_"Rise!"_ she says, after we bow ourselves down: I, Willa, Lipton, Oswald and Roland _"After breaking the Laws of the Land by going to try and rescue the body of Traitor Auberon Mastiff, you present me this... And affirm this is the Vaerian Trisquel."_ she states, looking for the Trisquel _"Can you conceive how many had came before with such kind of claim, but with scams to try and abuse the Law of the Land? Hope you know that, if this is not true, your fate are the Galleys and Exile to Laparast, six months ship and half Haeth far away from here!"_

_"Your Majesty, I'm a Scholar on Venisonton and Rugby Colleges, focused on the studies about the Old Times, and I checked with some others when, as I have informed thou, I have found Old Auberon Mastiff's Diary. There he drawn the Trisquel and stated_ 'I have it with me. He'll let me vanquish Death, like on the legends.' _He supposed this would have supernatural powers to revive the Dead, like some Vaerian legends stated"_. says Lipton

_"Hum... There's a way to find if you are truth. Steward, please bring the Vaerian Painting"_ orders the Queen

A beagle bring a nice picture, like a realistic reproduction on painting of the Trisquel.

_"Mr. Harrington, as you are the one claiming that's the Trisquel, give it to the Steward to check. This painting was gave so much time ago, at Jeoffrey Rabbiton's reign, by the Vaerian so we could check for any claims on the Trisquel. If there's the minimal difference, it's a scam, and off to the Galleys ye all go! If otherwise, you, Lipton Harrington, will be sacred as a Baron, Lord of Ottershire"_

The beagle Royal Steward looked the Trisquel and the painting, and was gasping time and another. He took nervous 10 minutes, that almost made Lipton crashes in anxiousness, till he says, on the most phlegmatic tone:

_"Your Highness, I can't believe... There's no other way how to say it, but, it's the real Vaerian Trisquel!"_ he states, making the Queen opening a smile

_"Steward, call Vaerian Ambassador, Mr McLuchóg. Say that is an Official Calling. Send also for all the pages to announce all around the Country the news that the Trisquel was found!"_ she demands, and then she smiles _"I'm royally happy you became the ones that brought us this, so Falstaff Mastiff honor can be cleansed, and ours with his. Kneel, Mrs Lipton Harrington."_ she orders, and using her scepter she says, flicking it over him _"I hereby, by the name of The One, concede you the title of Baron. Now, raise, Baron Lipton Harrington of Ottershire! I conclaim that the old fief of Ottershire is now thy, with all the responsibilities, duties and privileges implied. I conclaim thee to be respectful and good to your liege, to pay the Royal Tithe annually, and otherwise obey the old ways that are expected by from those on your station. Hope your Barony be a good one!"_ 

_"I hear and I obey, O My Queen!"_ says now Baron Lipton Harrington _"If you pardon my inconvenience, I want you to pardon either Soldier Oswald Longeyes and Roland Fussyears."_

_"I claim for thee also, Your Highness!"_ I say, nodding in reverence _"I'm the one that deserves their punishment, as I was the one that forbade them to obey Your Command, after knocking them out with my stench."_ I look for both the soldiers, that were trembling

_"That can't be, O Baron, O_ Maestro. _The Laws of the Land is above even the Queen's one. And they violated those Laws of Land to allowing you to get into_ Cair Mastiff'_s deposit and look upon his body."_ she states _"However, I command both of thee out of Harringlander Army,"_ they look sad  _"and command both of thee as part of the forces to ensure peace and order of the fief of Ottershire as Constables, under the Command of Baron Harrington of Ottershire."_ I can see all of them happy, and only could be happy to me, when a mouse guy came to the Room

_"Your Highness Lady Minerva Rabbiton, Second of her name, your Royal Steward said you claimed my Presence for an incredible reason."_ says the mouse, on nice clothes and a crest from Vaer: no more the badger paw, but the Trisquel.

_"That I did. I have great news, the ones that Vaer waits since the Folly of Auberon Mastiff."_ she says

_"Others of your Lineage did in the past, only to  discover being victims of scams that only resulted on mockery for your crown."_ says the mouse, humbling.

_"But we did the check and can confirm: Harringland found your treasure, robbed by Auberon Mastiff, be his name three time accursed."_ she states, and presents him the Trisquel. The mouse started to look all around the Trisquel

_"Oh... But... It's... All the Gods, the Old and New, be praised! But we need to send this back Ballyford, to Your Highness Lady Ewe Mac Donaghal!"_ he claims

_"But first, we need to shows our honor inside to reinstate our pride and civility, and present those who found the Trisquel as Heroes of the Country and also conclaim new Lord Ottershire, Baron Lipton Harrington, in front of Society."_ she states

_"That is needed. However, this need to be done as soon as possible! Vaer waits for more than 200 years for recover their treasure, each second added is one too much!"_ he says

_"Your Highness, Milord, can you pardon my impertinence? I think I have an idea that can help everyone."_ I say

_"Speak, O_ Talminare Maestro _and Hero of Harringland."_ she says

And I say my plan

---

_"Never though I could fly to Ballyford! People says the vision there is incredible, but Harringlander balloons were pushed back or even dropped down as soon they passed upon Jumper-behind-Cloister, last stop at the way to Vaer!"_ says Willa, on her new balloon, a big basket with lots of new stuff, including a very innovative toilet that could be emptied at solo. This was the way we could now be on a flight that took more than a day with only a stop at Ottershire.

It was ten days since we got to Rabbiton. We stayed at the Castles till two days ago, the day after the big Party of Reconciliation, where it was presented by Queen herself the Trisquel, and people cheered for it and for those who were now Heroes of Harringland. In the party the Trisquel was formally gave back to Vaer, under Lord McLuchóg as his ambassador, and happened the official claiming of Lipton as Lord Ottershire, with both Roland and Oswald Constables of Ottershire. Roland, however, was lent for us by the Baron himself _"so he can help clean old Baron Falstaff Mastiff's, one of former Lord Ottershire, honor. I command thee so Ottershire can be, again, friend of Vaer!"_ So, while Oswald dropped at Ottershire with Lipton, Roland got with us. And Willa was given this new balloon.

_"But now, coleen, with the Crest of Vaer aside Harringland one,_ The Vision of Friendship _will be saw as a good omen, as the Trisquel get back his home at_ Cair Lavast, _Vaer's kings castle!"_ said Vaerian Ambassador, Lord Rían McLuchóg. Nice fellow: gambler, drinker, a great limerick singer (some of them I noted on my travelogue), but a guy with a kind of natural Noblesse I couldn't find on the courts of Preccia and Rabbiton, and that knows legends about Vaer that would make Lipton throw the Barony aside first chance! He avoided the drink on the balloon, but he brought some good bumble spiced stew that we ate with bread as food.

_"Your Highness Queen Minerva II was really generous: this balloon is one of the newer ones, I can't believe she gave this to me as tithe just because I brought you from Ottershire to Rabbiton._ The Vision of Friendship _has gadgets, gizmos and stuff I've never saw before on a balloon, I'll take lots of time to work out all this stuff. And the envelope and basket are so big and pretty, it's a house in the sky. I dream becoming true, never flew so high in the sky!"_ she says, on glee

_"And, with the crests of Vaer and Harringland together again, no one would mess with us, except those crazy enough."_ says Lord McLuchóg _"And very few are them!"_

_"Who are those?"_ I say, pointing some people in the ground. I couldn't see the people, but could see some big orange and green flags being waved down!

_"Oh! I didn't noticed! We just passed Jumper-Over-Cloister and are at Hallowberry! We are at Vaer!"_ she praised, looking to a map

_"And the cicadas are going!"_ I say, noticing that fewer and fewer cicadas could be saw, their pervasive vision going into oblivion.

_"Let them go. They did their job and helped us to recover The Royal Trisquel. Vaer Luck had help you, lad, and this is enough. Even more now that I could see a Furrowian that was a_ Talminare Maestro _become a Harringlander Hero and Diplomat. This is a thing that no one would see even after Vaer Trisquel be found!"_ he jokes, which make us smile.

I think, looking by both sigils, Talmina and Harringland's, over my overcoat that protected me from the high skies cold and from the last bumbles from Swarming, Gateling coming nearby.

_"Hey... Think I'll get down. We need some time to unfill the stuff from the toilet and refill food and so. Mr. McLuchóg, do you know a good balloon port nearby!"_

_"Call me Rián, Lass, and we are luck. There's one, just a little away Hallowberry. We can start to get down now! I'll send some news about our delivery to Ballyford while we have a hot warm bumble stew."_ he says

I understand: on my bag, concealed by my _Albionneri Maestro_ cloak and my _Arlecchino_ and _Buonnandanti_ costumes, it was hidden the box with Vaerian Trisquel. 

It was protected enough, by the spirits of Albionne and Preccia, to help Harringland and Vaer get on good times again.

<!--  LocalWords:  Underhill Tillsoil Asfriteyhi Varst Ack Geroff pre
<!--  LocalWords:  spoilt Seneschal's farmworker Duchacy Heighs Heyia
<!--  LocalWords:  Hardworker Maths teached Nearsight Yay Cep mr Gack
<!--  LocalWords:  Conclaim conclaim conclaimed C'mon sensorial 'The
<!--  LocalWords:  Biggia Saltare Talmina Conigglio Whatcha Talvish è
<!--  LocalWords:  Wind' berrywine Univercittà Wanderhome Feruling da
<!--  LocalWords:  dei Mascherati Tanis Dulce dell'Aqua gamberetto tè
<!--  LocalWords:  Velina Sirenna Foresta uncared Carnevalle Zletvair
<!--  LocalWords:  Furrowian Karash pottymouth Haeth there'll Gennaro
<!--  LocalWords:  feruling Nasone Tinacci Istlevar Talpatone Maestri
<!--  LocalWords:  Univercittà' behavioured arrivist Volpinare Del'la
<!--  LocalWords:  Brighella Commedia Arte Cáspita alunni Danza Dalla
<!--  LocalWords:  mascherata Arlecchino Paollo Piva Canne Corso Il '
<!--  LocalWords:  witfull Lucca's Alunno Grata petrichor Dottore 's
<!--  LocalWords:  campalina Talmina's Batocio Abluzionne Collegiale
<!--  LocalWords:  Leone's Paolo camerata Martinno Aguila Bloommeadow
<!--  LocalWords:  Seneschalhood Castello Porcinni 's Sabrinna Rossa
<!--  LocalWords:  Scoiattolo Signori' Preccia Albionne's bloodlusted
<!--  LocalWords:  Tredici Callaleah Giorno Vulpone splitted Strada
<!--  LocalWords:  Reale Devildays Albionne cattlers Preccian Porcini
<!--  LocalWords:  principi Giacobbe Daughters' putrification Hovkar
<!--  LocalWords:  carrions imponent devoided Thristvnar Oglash Varsh
<!--  LocalWords:  Hiev Talminare Principessa Tagrash Porcinni' Vossa
<!--  LocalWords:  Studioso Maestà principe Principes Principesas tuo
<!--  LocalWords:  stablekith Albionnere Capitano geasa suceed Morte
<!--  LocalWords:  Maestro' Maledicta gettoni Albionneri follyfull te
<!--  LocalWords:  prophetised fetore Maestritura Grazie Otello righe
<!--  LocalWords:  Lontrini Preccia's Preccian's Calleleah piagli ish
<!--  LocalWords:  buonnandanti principesse Conniglia Conigglia Unita
<!--  LocalWords:  descendancy stabilished Chiesa Sacríssimo Giorni
<!--  LocalWords:  Sacríssimo' strawman Fratello' paglia insanguinata
<!--  LocalWords:  pagila Strawmen Tripla Leprione Matronna Chiesina
<!--  LocalWords:  Chiesas vicino importante quanto Leprioni Chiesini
<!--  LocalWords:  insanguinate Preccians Chiesa' Fratelli Precciani
<!--  LocalWords:  colours Inquizittori 'would Spavalderia Briatoli
<!--  LocalWords:  Mugenna Erano algurio Acidittà Cellebrazionne hoi
<!--  LocalWords:  Melina Conigglia's Salvio Rastelini Maressa simbol
<!--  LocalWords:  Apolonio Convocazione Ufficiale Porca Misera puzza
<!--  LocalWords:  suspice Vostra Altessa Precciana Precian Ratiratta
<!--  LocalWords:  Cerimonially embroidements Regina' Preceptore Uno
<!--  LocalWords:  Antonnio Tassini Porpinella Porcospina Precianna'
<!--  LocalWords:  morte Celebrazzione Buonnandantara Strawmen's 's
<!--  LocalWords:  Vecchia Matrona Furrowians heretici cleasing Starn
<!--  LocalWords:  Vecchi polloi puzzore puzzolente Callisteni skunky
<!--  LocalWords:  loathe'em Carverash Altalus Stankart shitshow mio
<!--  LocalWords:  Fredal Butcha Overpious Shaddap Stolas tripulation
<!--  LocalWords:  Savaltare Chiesa's 'common' saalan Chieserini dane
<!--  LocalWords:  L'Imperatore Giusticia Porta sweared gunports Cair
<!--  LocalWords:  Fratello ressurgence Tiesto conspiration expurge
<!--  LocalWords:  heighs Milena kiths Principe' Achille's Oldsmell
<!--  LocalWords:  Thabald danse bloodlust partisani conspiratori di
<!--  LocalWords:  dreamt protettore Regno heighed Spada Tassini's
<!--  LocalWords:  Gentildonna Scudiero Ratto delle vendite sooted
<!--  LocalWords:  Scholarhood Calabrona Lettera Abilitazione sigil
<!--  LocalWords:  Nortissa Herringland Ottershire Ferretington c'mon
 -->
<!--  LocalWords:  Harringland Badgerton Badgerton's Asvreth Laughton
 -->
<!--  LocalWords:  Harringlander Haatheestan criss stablekiths Vaer
 -->
<!--  LocalWords:  cattling Ottershire's Auberon Rabbiton Jeoffrey
 -->
<!--  LocalWords:  Vaerian Ballyford Auberon's Lapidation Belltower
 -->
<!--  LocalWords:  Trisquel Vaerian's farmlady Venisonton Heigh Rían
 -->
<!--  LocalWords:  lapidated Rabbiton's heaten Kingmaking mortem Rián
 -->
<!--  LocalWords:  cypher Laparast McLuchóg Longeyes Fussyears Milord
 -->
<!--  LocalWords:  Donaghal coleen Lavast Vaer's Noblesse Hallowberry
 -->
<!--  LocalWords:  sigils Harringland's Gateling unfill
 -->
