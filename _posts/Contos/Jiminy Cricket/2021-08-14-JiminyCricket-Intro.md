---
layout: contos
title: Jiminy Cricket - Intro
subtitle: Joshua McCarthy arrives at Zurich and becomes Jiminy Cricket
teaser: Joshua McCarthy arrives at Zurich and becomes Jiminy Cricket
category: stories
---

## Zurich

_"Welcome to Zurich. The weather is cool, 23 Celsius Degrees. Please put your seats at upright position. We'll landing in half an hour."_ - said the airplane sound system, while the little kid was waking up, looking all around. He was into a first-class seat, going for a really new life. 

In his mind, he could see the the ghost image of a man getting near him and huffing his messy, strawberry blonde hair. Just some seconds after, he saw the same man getting near him in real life in real life. 

_"How are you, son?"_ said the man, his father, while huffing his hair. _"Think you had seen me coming."_

_"Yup."_ says the kid, using his Pinocchio pajamas. 

_"How are you dealing with this, Joshua? On this flight... All the things you could see.."_ said the man. 

_"I'm okay, Daddy."_, said Joshua, the kid _"There's so much people here that I can't preview too much their moves. Just some fractions of a second."_

_"I'm just trying to be a good dad. It's not so easy to do this when your son's a Breakthrough, a Hero."_ says Daddy 

Until last year, Joshua McCarthy was a common kid, very kind and well behaved, that loved heroes, like Atlas, Ajax, Blackstone, Astra and others that came since The Event. 

He was a very smart kid that loved books and stories and Disney. 

It was when... 

---

He was going back home walking, as he lost the school bus... He avoided to walk back because the bullies that always picked him, he being small and weak, so an easy target. He didn't understand why they didn't liked him, as he was just a kid that just wanted to learn, to be a better person and give a good future for his parents.

It was when he was to cross an street he saw just the worst bully in the classroom, Kyle. He was just the other side of the street, with hawk-like eyes, looking for a prey.

He knew there was no need to try talking with him: he was know as a rotten kid, even by some of the teachers.

He did the only wise thing he could do.

He ran.

He tried his best to run back to your home, but he was the smallest and meekest kid in his classroom.

And he had no friends, like Kyle had.

When he turned a corner, one of the other kids of Kyle's gang catched him and sent him to the ground, into a empty alley.

_"You should not made me run, shrimp!"_, said Kyle, with a very bad smile in his face.

_"Leave me alone, Kyle! Please!"_ said Joshua.

_"'Leave me alone, Kyle! Please!'"_ said Kyle, mockingly _"Look for this momma boy!"_ and he started the punches, followed by the others

Joshua tried his best to protect himself, looking for some help, but no one passed nearby the alley.

As far he knew, he was as good as dead if counting with someone passing by.

He was feeling that the beating was going even harder that time: he could hear one of his ribcage bones crack. After this, his left arms was crushed by a stomp from one of the Kyle's friends...

Curiously, he could feel he was seeing the attacks ___just before___ they were done. But he felt himself so desperately fragile... He could even see possible escape routes, but he could not run, breathing too much difficult because of the broken ribs and legs and other bones...

_"Kyle... It's enough!"_ he heard one of the other kids saying... Worried _"You'll kill him!"_

_"Yeah? So sorry... In this world, it's kill or be killed! Even a Cape would say that!"_ said Kyle, gleeful on sadism.

That made something snapped in Joshua mind... He, at least for a moment, could not feel pain. And he screamed, full of terror and something else he could not understand:

_"PRETTY PLEASE, LEAVE ME ALONE!"_

And, as a miracle, they stopped their attack.

But it was not this... He could see a blue-ish like mirage thing, with them running away, and a woman passing... And some places he could run. But it wasn't ___they___  what he was seeing.

_He could see the future._

He said: _"Pretty please, go away! Let me go home, go away and do this thing no more!_ Against no one!", he stressed on the last and he felt that his voice had a kind of authorative tone that they could not negate. They tried to punch him again, but they ran away, more scared than him... Like he was the one that bullied and walloped him. Even he being the one so badly hurt, walloped for no better reason then _because_.

He limped away from the alley, with difficult, lots of broken bones and the pain getting back to him, even more painfully, when the woman he saw was getting near, a Walmart bag in his hand.

_"Oh Dear! My god, kid! What happened?"_ said the woman worried, looking Joshua's wounds

_"Please... Home... Hurt... too... Much..."_ wimped Joshua, painfully, breathing being very difficult. _"Want... Dad... Mom..."_

_"No ambulance? You need to go for a hospital!"_ worried the woman, carrying Joshua cautiosly on her lap

_"No... First... Home... Ambulance... Later..."_ he stressed

She did her best to put him into her Honda CR-V and hear his directions, which Joshua gave with a growing pain on his voice... Until he arrived at home.

_"Joshua dear!"_ said his mother.

_"Mom... That bad kids... Kyle... Did this... I talked... He stopped... Talked again... He ran away..."_ said Joshua, his father holding him over his arms, before he blacked out into a coma.

---

_"It was not easy for us to be with a comatose son. They found Kyle and the other kids and the police discovered they almost killed you. They were send to the Juvenile Reformatory."_ said his Dad.

_"I know... Then you heard from that woman what I talked to her about what happened to me."_ said Joshua.

_"Yup... It was when we discovered you had gone through Breakthrough, and then you were discovered by some capes based on what happened with you. And it was that, two days after you woke up, that Diana woman came to you."_

---

Joshua was a little better, although his arms were still into stretches. His mother was helping him to eat some beef casserole with carrot and peas, when someone knocked the door... With a little too much strength, by the powerful knocks:

_"Excuse me... I think you are Mrs. McCarthy."_ said the woman while entering. She was black, tall, strong and beautiful, dressed into fatigues, a dog tag over her neck.

_"Yes... And you..."_ said Mrs. McCarthy

_"My name is Diana Souza, capename Diana."_ said the woman.

_"Are you a hero?"_ said Joshua.

_"We could call this way. I'm a cape, a breakthrough."_ said Diana _"To be exact, according by_ Barlow _classification, I'm an A-Class Ajax-Type Breakthrough."_

_"So, superstrong and super-resistant." said Mrs. McCarthy "I worked with insurance until some years, and in my work I had to read the_ Barlow's."

"Barlow's?", asked Joshua

"Barlow's Guide to Superhumans." said Diana _"A book that qualifies the powers and level of prowess for almost all the main types of Breakthroughs_ and _give basic information about the most famous ones. But enough about me... You're the kid that froze a group of bullies and made them go away just using the voice, right?"_

_"How do you know about this, miss?"_ said Mrs. McCarthy _"If you're one of those goddamn_ paparazzi..."

It was when Diana took a badge from the _United Nations High Commissioner for Refugees_, and showed it discreetly for Mrs. McCarthy.

_"Forgot to finish my presentation. I work for UNHCR, and also for_ Heròs Sans Frontiére, _the Heroes Without Borders. This was how we discovered about your son. And I think you already heard about us."_ said Diana

Joshua nodded _"Read about you... You helped the people that lost their homes after_ Eretz Israel _was founded, after the Caliphate War"_

_"And even more things, but I think you know enough about us to understand what I'm here for."_ said Diana

_"No way!"_ said Mrs. McCarthy _"He's just a kid, not a soldier! Even he had undergone a Breakthrough, he is just a kid, he doesn't need to Wear the Cape! He was just getting back from school and all taht happened! He didn't asked for this and no way we could give him to a soldier to put him into some war-stricken place and treat him as cannon fodder!"_

_"You're right on be defensive on him. As his mother, I really respect this on you, believe me. And your're also right. I'm a soldier, that's a fact. In fact, my breakthrough was triggered when I stepped into a Landmine in Angola, serving for Brazilian Peace Forces for UN, only my new Atlas fueled Strength and resilience helping me on avoinding become mincemeat. But_ Heròs sans Frontiére, _that is the organization for what I work besides UNHCR, is not a military force and is not just made of Ajaxes: we are mainly support for  Doctors Without Borders. We are Breakthroughs that offer support and help in catastrophe zones with all kind of needs, so we need all kind of powers on rosters. Normally we don't take on weapons, although the basic training have some personal defense techniques and weapons as demanded. We work on humanitary missions with UNHCR, UNESCO, UNICEF and other UN Agencies, Doctors Without Borders and lots of other humanitary NGOs, using our Breakthrough powers, avoiding fight. Believe me, after an earthquake, having some capes can be very useful."_

_"All right."_ said Mrs. McCarthy _"Let us say that I'm okay with_ Heròs sans Frontiére, _and I'm not saying I'm totally okay with this yet. I think you're here to recruit our son to your organization. For which reason?"_

_"First of all, let us make something clear, I'm just_ asking _for. This is not compulsory. We need all help we can call upon, but we are not for forcing people on do things. If you say 'no', Mrs. McCarthy, it will be 'no', no strings attached. We take Joshua's freedom as Breakthrough and your's as parents very seriously. But we, like almost everyone else, are always looking for breakthroughs to improve our rosters. And your son power could be really useful for us. And for everyone else. Believe me, while we are talking, there's people at Quantico and Langley and Fort Meade looking for your son's powers, with less scruples, with bigger legal powers and for more war-focused uses than us.  I think you understand what we are talking about, Mrs. McCarthy."_

_"FBI, CIA and NSA"_ said Joshua

Mrs. McCarthy and Diana looked to Joshua, surprised.

_"What? Had I said something wrong?"_ said Joshua

_"No..."_ said Mrs. McCarthy _"But how you know about this?"_

_"Dunno."_ said Joshua, _"Just... Know... Maybe heard or read somewhere I never remembered until now."_

Diana looked to Joshua.

_"Mrs. Selena McCarthy,"_ said Diana, turning back to her, and taking a business card from a pocket in her fatigues _"call me if you want to talk more about Joshua. I can say for you:_ Heròs Sans Frontiéres'_s crew are trained in all kind of things, and we can provide for Joshua the best education and training you can imagine, so he can use his powers not only efficiently, but_ wisely. _Looks like he has an incredible power set by what I've saw, from the only recently catalogued Mastermind-Type, that can be very useful in panic-stricken situations. He could help us in more than a way, on non-violent riot control and so. But, as I've said, I'll left the decision for yourselves. I'll be glad if you, at least, go for our HQ at New York for a visit and maybe a better talk. Anyway, thanks for your time."_ finished Diana while getting out the hospital room.

---

_"It was a difficult talk, after that."_ said Joshua

_"I thought that you could have a use for the HWB's overseas education, but your mom, as the pacifist she always was, was almost totally against this. We were surprised when you said you had made your choice."_

---

_"Mom, Dad... I really want to Wear the Cape."_ said Joshua, shocking his parents.

_"WHAT?! But... Josh..."_ said Mrs. McCarthy

_"Mom... I don't know why I had gone through the breakthrough and not died... I don't know how powerful I am... But... You always said me God gave us free will to do the good for everyone. And... I think that, if I can help and save people with my powers, it almost made it worthy being beaten almost to the death by Kyle..."_ said Joshua

Mr. and Mrs. McCarthy looked for his kid with a mix of worry and happiness.

_"Josh... I don't know what happened while you're under that coma... But you had turned wise beyond your age. We are glad..."_ said Mr. McCarthy _"For me, I think you can join the HWB."_

_"What? But... Michael..."_ said Mrs. McCarthy

_"Selena... I think this will be the better way to put his powers. A better use than the Army or CIA could do for them. You felt his power just yesterday, when he used it on you to get an extra_ petit suisse cheese. _Think on Joshua, with his powers and the military indoctrination. Your father was in Vietnam, you Uncle in Korea, your grandfather in the D-Day. You know how they are, what they saw... Think on Joshua with the military doctrination. He could be used as a weapon, and would be as disposable as any Weapon"_ said Mr. McCarthy.

Mrs. McCarthy looked for Joshua and for his husband, and back for Joshua...

_"Your father is right, Joshua. But I think you understand this will not be like the_ Cub Scouts: _there will be people that you'll find with the HWB that will want to kill you for sure, like that_ Seif-al-Din _nut and their followers. You'll swear me, for all that is Sacred, that you'll do your best to use your powers for doing good and save people, not for kill people. You'll be better than those bullies, either Kyle and Seif-al-Din._" said Mrs. McCarthy, a little happiness tear in her eye and hands over Joshua's shoulder, looking into his green eyes.

_"I'll do it mom! Scout's Honor!"_ said Joshua, that was really a _Cub Scout_, doing the Scout's Salute.

---

_"It'll not be easy for us to do this..."_ says Mr. McCarthy

_"I know... But I think I could not left this gift behind or use it for things like the Army."_ said Joshua. _"I'll do my best to use the powers now I know I have, either the_ Bellax Analytica, _the Tactical Analysis, and the_ Pretty Please _to help people. I can use the_ Bellax Analytica _to understand the panic-stricken situations and the_ Pretty Please _to make people goes calmer."_

_"Looks like that Diana woman was right: you have a great gift. But I know better that, more than your powers, you have a great heart, in the right place. We are proud about you, son."_ said Mr. McCarthy, while getting throught the Zurich's customs. While Mr. McCarthy showed his US Passport, Joshua presented a brand-new blue UN _Lassez-Passer_, the UN Passport, with the expressed signed consent for overseas travels. The Customs agents looked weirdly for the kid with a Peter Pan T-Shirt and so presenting the UN Passport, but they confirmed it as a fair and square thing.

After passing the Customs, there was a girl, somewhat 19, looking for them, using what looked like a ballet jacket and thighs... People could think she was a ballerina, wasn't for the HWB badge on her jacket.

_"Mr. Michael McCarthy, Joshua... Think Diana talked about me, but I'll present myself. I'm Eileen MacRae, capename Sugarplum. I'll be Joshua legal guardian, with Seiji Shirou, capename Soldaire, here in HWB. I know you're tired, so we'll get to Geneva, for HWB HQ. Please,  come with me."_ said the ballerina girl

They took their luggage, including all the toys Joshua brought with him, including his prefered Jiminy Cricket plush doll. He loved _Pinocchio_ even more than all Disney animations he loved.

The black Mercedes SUV with HWB logo was waiting for them: Diana was in one seat, and a Japanese guy, dressed in his best Tokyo _Keishichō_, the name for Japanese Police, with a armband at his arm with HWB logo.

_"This is Seiji Shirou, cape name Soldaire, our_ Sentai, _more exactly a Metal Hero. He'll help us to care for Joshua."_ said Diana, presenting the Japanese man. _"I'm here just for some days. I'll get back soon to the field."_

_"So, this is the little Joshua?"_ said Seiji, giving a respectful bow Japanese-Style, which was replied by the McCarthys, and then they shook hands. _"Had you already thought on a capename? If you don't want, this is not mandatory, but this help things at field and gave some security for your parents and family, as outside the HWB people normally will not know you beside your capename."_

Joshua didn't thought about this during all this time, neither during the crash-course on his power he did at New York before going to Zurich. He thought on his powers, and on how much he liked Jiminy Cricket, and about how Jiminy Cricket was Pinnochio's Conscience Voice until he turned into a real boy, and something clicked on him...

_"I think... I'll be... Jiminy Cricket!"_ said Joshua.

The adults looking for him and laughing heartfully

_"At least will be easy to do a costume: we could ask Disney for this. Think they'll help a little hero to be a Conscience Voice for the moments people lost their minds, like Pinocchio did sometimes."_ said Diana. _"So, let's go? The trip will be somewhat fast, but maybe will need to take some meal in the trip. Everything paid by HWB."_ she said, while Mr. McCarthy and Joshua, or better, Jiminy Cricket, entered the car, going to Geneva for his training.
