---
layout: contos
title: "Jiminy Cricket - Operation: SANTA"
subtitle: "The Mission That Wasn't, where Jiminy Cricket and part of his team literally saves Christmas!"
teaser: "The Mission That Wasn't, where Jiminy Cricket and part of his team literally saves Christmas!"
category: stories
---

## Chapter 1

> "Sometimes, the best gifts are those who came from out of nothing."
>
> _From Jiminy Cricket's Journal_

Jiminy was looking himself at the mirror at _Herós Sans Frontières_ Booth at Metrocon for another time.

_"Jiminy, you'll not get more handsome."_ said Blue Fairy (real name: "Shelly Boyar")

_"I just want to be in my best look."_ said Jiminy Cricket (real name: Joshua McCarthy)

_"It's just a public award. Get there, give some words, take the award, get out."_ giggled her, with a handsome Blue Fairy like dress.

_"I'm just a little nervous."_ said Jiminy when his best friend came.

_"Alright, Jiminy, it's showtime."_ said Djanni (real name: Altayr Bashir Tahan), giving a small slap on his shoulders. A ___really___ small slap: if Djanni gone full power as the A-Class Atlas he was, his superstrenght would make Jiminy's shoulder into pulp, his shoulder blades turned into smithereens.

_"Fact, sport:"_ talked them Sugarplum (real name: Eileen McRae) _"you are one the best PR trumps_ Herós Sans Frontières _have. And this award is just PR."_

Jiminy was dressed on a new, formal Jiminy Cricket costume. In fact, he was almost like a Disney Jiminy Cricket cosplayer, as the conscience clothes: black tails with green pants, white formal shirt with a red waist-coast over it, a yellow cravat around his neck and down back his waistcoat. Black shiny shoes with small yellow gaiters, a blue Top Hat with an orange ribbon around the hat's brim. Over the tails handkerchief pocket, a small pin with his crest and a _Herós Sans Frontières_ crest.

Now 16, Jiminy Cricket was a little more like an adult, albeit a little gangly: below his Cricket faced mask there was blue eyes and a blonde hair. Now an experienced cape, Jiminy Cricket was a Mastermind, a rare kind of breakthrough, and even more rarer on "the good guys": his powers were _Bellax Analytica_, a kind of future evaluation/precognition power that allows him to somehow "see" the immediate  future, at least for the most potential ones, never more than some seconds; _Pretty Please!_, a kind of behavior-changing power that allows him to make people does what he wanted, as far they could do it somehow and it wasn't something violent; and a powerful mind, that easily could learn and remember things. Thanks to the last one, he speaks at least 8 languages (4 of them fluently), pilots helicopters and was on a International Law graduation with lots of minors.

However, he was still a teen, even with all cape experience (5+ years as a HSF cape, even underage), and like all teens, he was a little nervous.

_"Let's go, Jiminy."_ said the team leader Soldaire (real name: Seiji Shirou).

Their team, and almost all them, got to the great hall, somehow like Oscars, red carpet and all, and almost as crowded as them: lots of cape fans with cosplays and boards, cheering and screaming as each of the biggest teams passed by, even the small teams like Jiminy's being saluted with cheers and applauses. Screams for the big stars capes like Seven, Chakra, Astra, Blackstone, Rook, Baldur, Vulcan, Euphoria and SaFire could be heard all around, baffling some anti-cape protesters like the Paladins and Humanity First.

Jiminy and his team sat on their chairs at the big Theater next to Chicago Convention Center where the _National Certified Aid and Intervention Conference Awards_ would be revealed to the public. Jiminy was chosen to receive an History and Service Honor Award in _Herós Sans Frontières_' name for the services they done all around the world on calamity places and humanity disaster situations, pairing with other NGOs like their alma mater _Mediciéns Sans Frontières_ (Doctors Without Borders), United Nations' High Comissioneer for Refugees (UNHCR) and World Health Organization (WHO). The prize was being given as a reward for the ORSL project deployment: _Oversea Refugees' Support Line_ was a line of bases in international waters to help on dealing with the immigrant flux the best way for everyone. Even with their critics, the project was giving fruits, as saw on the recent _Hagia Sofia_ earthquake.

Jiminy could see that, as always, the awards were disputed between the hosts from Chicago Sentinels and the Hollywood Knights, both being the most famous cape teams thanks their PR and merchandising machines. The _Excellent Protectors_ also had received an awards, which made them came from Japan for a presentation show during the Awards, and the weird and now deceased _Remarkable Ronins_ had also received an honor award.

It was fun, and for real it was the first time he saw the _Awards_ (the last time he was at Metrocon, he lost the Awards as HSF had closed their booth earlier after a rogue super-villain attack against them). It was when they stopped the normal awards and he could see The Harlequin going to present the Honor Award.

_"Now, it's really common for people to see us capes as a glamorous lot, with all the costumes and fights and dealing with VIPs and so. But there's lots and lots of capes that are working out the loop, dealing with all kind of bad situations. Even now, almost 20 years after The Event, their consequences are still there, some wounds still bleeding, need to be cured. Even without the destruction provoked by super-villains and, to be honest, by Superheroes trying to stop them, there's still problems to be dealt with: earthquakes, typhoons, storms, floods, avalanches, and all kind of natural catastrophes. There's also the political turmoils that make people evades their countries, their homes, their loved ones, to flee away from political or ideological persecution and violence."_

_"And lots of capes are almost literally in the mud, working 24/7 to help those so helpless that even those who should care for them couldn't, as their hands are already full. We believe that, without those real life heroes, The Event would be even more catastrophic for humankind that it was, being potentially apocalyptic."_

_"This year National Certified Aid and Intervention Conference's History and Service Honor Award goes to..."_ said The Harlequin, opening the envelope with the name "Herós Sans Frontières!"

Every cape in the Theater started to clap their hands for them. Jiminy lifted and went to the stage: beside his team, he could see some familiar faces, like Diana, Corin, Squirrel Girl, Prop and Colocation, people that he knew from his tours at Sierra Leone at _Roque Santeiro_ and his newer base at ORSL 1. After he received the small trophy and shook hands with The Harlequin, feeling the rubbery sensation of her rubber hands, he got to the pulpit to give his speech.

_"Thank you very much. I will try to talk not too much, but I want to say how much_ Herós Sans Frontières _was important in my life and in some people everywhere."_

_"My breakthrough was, like with many of you, on a death risk situation. The presenter, The Harlequin, had almost died when she fell during a trapeze show at a Cirque du Soleil show, her body bouncing as it turned into rubber to avoid death. Myself, I was beaten almost to death by bullies and just avoided it by making them to stop using my_ Pretty Please! _power in the eleventh hour, and using my_ Bellax Analytica _to find a escape route and help before I bleed dry, almost all my bones broken, what made me gone comatose just a little after came home. Going to HSF, even so young at 11, was a way I found to put this gift, and I believe it was a gift, even so painfully received, into service."_

_"Since my first mission tour at_ Roque Santeiro, _I saw lots and lots of suffering: people that arrived there so weak and thin that looked like empty scarecrows or, even worse, almost like real-life skeleton models. Kids without parents, they lost into war-stricken places. Parents without kids, grieving their deaths for diseases and violence."_

_"But I found some of my best friends in the field: for example, my teammates Djanni and Marjanah had their breakthroughs under dire circumstances, and now are working with us. The smile of children when I clowned as Spotty. Some people that received for first time under their lives good medical care, even receiving top-notch Vernetech drugs for diseases that kills people like flies at Africa. All this made me see the sweet on my work at_ Heròs Sans Frontieres, _as much I saw the bitter."_

_"To be fair, I have not too much to add on what The Harlequin said: she basically gave my speech."_ Jiminy said, and people gave some laugh on the unintentional joke _"What I can say is:_ Herós Sans Frontières _will be always here, to help people in the direst circumstances. To be, as by our motto,_ super-aide pour les super-besoins, _super-help for super-needs. Because this is exactly what we do: help when the needs are really big."_

_"I'm thankful_  Herós Sans Frontières _allowed me to speak in their name: I know many of you are HSF reserves. You understand what I'm talking about: many of you contributed with precious time, money and resources to gave us, the fixed HSF rosters, support, training, and vital resources we put into service in the direst situations. Not direst as in super-villainy situations, although I had myself tagged at least two or three super-villains. But direst as in the absence of the barest necessities, being in places where clean water and morsels of food are worthier than gold and fame. I believe that putting our powers in service for those who are in the biggest needs is a mission that all capes from HSF accept through and through."_

_"To finish, I want to thank my teammates and all_ Herós Sans Frontières _capes, fixes or reserves: you deserve as much, and in many cases even more than myself, to be here, representing this paramount post-Event organization. I also want to thank all the non-cape HSF personnel: clerks, pilots, soldiers, even the donation teams. You help us doing your jobs, and relieving us of the daily_ quid-pro-cod, _so we could do the difference. So, in the end,_ you also does the difference. _I want also to thank my parents: I still remember my mom's face when I said I wanted to do this, not what people thought would be a saner, better way, by going to Whitlow, Hillwood and so. It was a blind shot, for sure: I almost died many times, but they stayed at my side. To finish, I want to thank you all, because this award shows us that we have your back. And I need to say: you can trust_ Herós Sans Frontières _if some catastrophe happens. We are super-help for super-needs, after all. If a catastrophe is so big that even local CAI teams has not the numbers or resources to deal with it, you can believe that you have our backs."_

_"Thank you very much."_ said Jiminy, and people clapped hands, while The Harlequin ushered him to the backstage for a reception.

There was lots of people there: many of them capes, on their best social costumes or suits, but there was also common people, like  journalists, reporters, cameramens, VIPs and the capes' families. Jiminy saw his parents, Djanni's and Sugarplum's, talking with some people: looks like they joined a kind of _Superhero Parent Support Group_, and his parents were talking with Astra's ones about contributions for a new edition of _Superdads_.

It was when Jiminy saw  a guy into US Air Force formal uniform. He looked as an officer and had black hair and gray eyes. He looked around, trying to read people. It was when he looked to Jiminy and his team and approached them.

_"Jiminy Cricket, sir. Major Thomas O'Mahoney, USAF, sir."_ he said, saluting them before shaking hands. _"It's an honor and need to say I thought your speech tonight very inspiring."_

_"Alright... Sir."_ said Jiminy, replying the salute. _"How are you?"_

_"Well, I'm okay... But I want to talk with you and your team: we need some capes for a mission. Something very important. I can assure,"_ said Major O'Mahoney, looking the worried faces of the parents there _"that has no special danger, but we need capes that could, although their reputation, go under the radar and low profile. Unfortunately, there's lots of classified information for this mission, but I can say it's a very important one."_

_"Sir..."_ said Soldaire _"I understand you have your needs, but we need to know what is happening if you want us to help."_

_"I can say NORAD needs you... But we'll understand if you don't accept this. Let us do this way: we have a flight on O'Hare Airport ready to take you to NORAD at the Cheyenne Mountain Complex. You go and hear our needs, no strings attached: if you hear us and you say no, it will be no. The only thing we'll ask you is to hold any intel about this classified no matter what: in fact, just my superiors have clearance to disclosure intel about it, I don't know all the details to be sure."_ said Major O'Mahoney.

They looked each other:

_"Well, we are in bench now."_ said Hufflepuff _"And if we can't help, we'll just get back ORSL 1, as we would anyway."_

_"For me sounds okay."_ confirmed Sugarplum

_"Do you have no problem with our roster? We have capes that are officially under Refugee citizenry's or under parole."_ asked Soldaire. Marjanah, Djanni, Scheherazade, Lahab, Presto and LionHeart were all under those conditions.

_"I don't think would be any problem: albeit it's classified, I know that the mission has no national-security intel on it."_

_"Okay... Think we can at least spare some hours for you."_ said Soldaire, while they took some finger food.

_"Alright... I'll wait you 0000 sharp at O'Hare. No need of anything: just a change of clothes. If you don't accept the mission, we'll bring you back. Until then."_ said him, saluting them and getting away.

_"Weird thing..."_ said Jiminy _"NORAD needing us?"_

_"What is NORAD?"_ asked Djanni

And, while finishing eating at Metrocon, Jiminy explained about NORAD.

## Chapter 2

> "NORAD's mission, in the post-Event world, became also control the airspace for the presence superhuman assets and threats. To do this, NORAD, with the support of ICAO (_International Civil Aviation Organization_), started to enforce the main flying capes to use the same flying alert devices airplanes are used. To this NORAD has their own team of flying superhumans to deal with unauthorized or just unidentified fliers that can put  North America Airspace in danger."
>
> _NORAD brochure_

Jiminy just got into the Cheyenne Mountain Complex, NORAD base. The clearance procedure took some time, specially because the team had lots of special cases: to be fair, if they had been strict into "law-abiding American citizen" rule, only Sugarplum and himself could get into. 

_"We'll take an undisclosed way here, avoiding all classified sites. Please stay nearby myself or other NORAD personnel and stay identified all the time."_ said Major O'Mahoney.

Their IDs had only their cape-names and photo mugs of their faces as capes, masks included. It was difficult to make Hufflepuff give his real name: as a _Luchador_, as much people knows about his real character, less power he had. But as soon all the information had been put on CLASSIFIED files and only the one that would take his mugshot without mask would see him that way, he complied with it.

They walked a little around the base: there was lots of training rooms, for all kind of things, and some rooms full of clerks and people looking for maps and analysing intel. Some were on civil clothes, some into military fatigues or even on full field soldier apparel, Jiminy noted. Curiously he also saw people with Canadian flags over their desks.

_"NORAD is a joint organization with Royal Canadian Air Force to protect the North American Air Space. We are doing this even before the Event, and now that we have flying people all around, we need to be double safe: the last thing anyone wants is a flying nut invading our airspace to do nasty business, or even Astra crashing against a plane because no one saw."_ said O'Mahoney.

It was when they got into a section a little...

Weird...

The door had some festoons and small packed gifts decoration on it, and the logo on it looked like NORAD's one, but in green... Christmas green?

_"Sir, could we..."_ was saying Jiminy, when O'Mahoney looked to him

_"Just a minute, Jiminy."_ he said, passing a card to unlock the door. As the green light lit, he ushered Jiminy's team into the room.

It was like a Dispatch room, but...

...festive?

They are a little nearby the Holidays, but people are really festive while doing their jobs. Festoons and mistletoe all around: this last one made Sugarplum shiver a little, as mistletoe is a powerful all-purpose magic bane. A big real pine tree totally decorated, with balls and toys and even a Mickey Mouse decoration nearby the star, was in a corner. People was dressed with some festive trinkets on their clothes: there was a guy with a full military apparel using a reindeer antler over his head, and there was a girl using some elf ears that was being held by her telephone headset. He heard the girl saying: _"Yeah! Of course Astra knows Santa! Didn't you saw_ Sentinels _Christmas Special episode?"_

The room smelled on peppermint, cinnamon and hot cocoa: Jiminy could see a big pot full of hot cocoa, and a small marshmallow dispenser nearby. It was then they saw a guy that was under common officer military clothes. As O'Mahoney salute him, they noted that this guy was the big one.

_"Sir, I bring to you the HSF Situation 2 Team 10 capes."_ said O'Mahoney.

_"At ease."_ said the man. _"I'm Colonel LaSarte, NORAD Santa Tracker commander."_

_"NORAD Santa Tracker?"_ asked Jiminy

_"The Santa Tracker started before The Event, at 1955, when Sears misspelled a telephone and, instead to send kids to hear Santa, send them to Colonel Harry Shoup. He didn't like it at first, but with time it became a tradition at NORAD to 'track Santa' at Chrismas."_ said Colonel LaSarte _"But... We need you dearly. Come with me."_

They got into a meeting room, where they saw some folders with NORAD logos, all with CLASSIFIED on it.

They all sat over their chairs and then LaSarte started.

_"Well... Looks like cliche, but there's no other way to start it: **Yes Virginia, there is a Santa Claus!**"_

## Chapter 3

> "Sometimes is difficult to understand the infidels... To associate a pagan symbol to Isa's, Jesus' birth, Peace with Him. However, albeit The Event means that the Mahdi's coming is near, we never know when this will happen. And it's weird to discover that some of this paganism is real."
>
> _From Altayr Tahan's journal_

_"What?"_ said Jiminy, a little skeptic, a little dreamy.

_"Yeah... I know this sounds crazy... But in fact there's a Santa Claus."_ said LaSarte

_"What's next?"_ said LionHeart, with a cynical sneering _"Easter Bunny? Paul Bunyan?"_

_"I don't know about Easter Bunny, but I would not discard it. About Paul Bunyan... I heard he made his HQ nearby Wisconsin, but he doesn't work openly as he wants some privacy."_

_"And what about all this Santa tracking stuff?"_ said Soldaire _"I always thought this was a kind of joke to make kids happy at Christmas!"_

_"It was, indeed, at least_ before _the Event. But now... Bloody hell, you were with_ freaking _Ozma!"_

And it was truth: the self-declared Queen of Oz, Ozma had the behavior and powers to uphold her declarations about being the _real real_ Queen of Oz, not a delusional girl. And Jiminy just enjoyed some afternoon tea with cookies and scones with her a few hours before at Metrocon!

_"Okay... Let us clarify things for those who doesn't know: we had NORAD Santa since 1955 till now, passing by The Event. But until there, it was just a joke, a white lie, a small prank for make kids happy at Christmas, as Mr. Shirou said."_ continued Commander LaSarte

_"It was when, at 24th December in The Event year, some of our allies on Australia detected an UFO. Not the alien one thing, but anyway something not identified in the air. The first coincidence: the UFO got into their airspaces somewhere nearby Christmas' Eve, local time, staying for just some minutes at most. Second, it run very fast, impossibly for a baloon or drone or something like else. However, as the Christmas Eve was getting nearby at our local time, ready to become midnight from Christmas Day, more reports came from our allies: all of them reporting the same very very fast UFO, with consistent time under their airspace. Even in China, North Korean and Iran it passed through, we discovered after the Wars that imploded those countries after The Event made some intel about the time leak."_ 

_"That UFO was not_ totally _unidentified, however. There was an identification prefix on the radars, like an aircraft displays, albeit it exchanged based on the country: in some french countries it shown_ Papa-Nevada-Oscar-Echo-Lima, _on others, those Portuguese and Spanish speakers,_ Nevada-Alpha-Tango-Alpha-Lima. _But when it came into our airspace, it shown itself as_ Sierra-Alpha-Nevada-Tango-Alpha" 

_"SANTA! On the phonetic code!"_ said Jiminy _"And the others are P-NOEL, from_ Père Noël, _Santa Claus in French, and_ NATAL, _Christmas on Portuguese and Spanish."_

_"I could say that night was a nightmare, according my predecessor. We had gone red alert, and some planes just tried to intercept this UFO when they got into our airspace, like almost everyone else did, even Atlas taking his time... And what you think they saw?"_

_"There was reindeers included?"_ sneered Dumont, that was a little quiet _"They really know how to fly?"_

_"They know? They overcome our aces easily: before they could lock any of the missiles, they sprinted all around! With an impossible velocity and without inertia, doing maneuvers even Atlas could not follow on_ and he tried! _But anyway, this event gave us some food for thought, when we received the photos from some of our UATs that did the surveillance on the aftermath. There's some in your folders."_

They could see that there was photos of a big sleigh, red with gold and green details, pushed by nine reindeers, the one in front with a shiny bright red nose. And in the conductor seat...

_"Yes, Virginia... Here's our Santa!"_ said LaSarte _"And, before you ask, no, those photos were not manipulated, and all the involved soldiers pledged, under oath, that they saw what we are showing you, and that they were not hallucinating or under intoxication. Also, some of your allies had similar images, as he had the nerve to allow being photographed at some of the main landmarks around the World: Eifell Tower, Tokyo Bay Tower, Pisa Tower, The London Eye,The Pyramids, China Wall, Stoneheng, St. Stephen Tower_ Burj Khalifah ,_Over the Liberty Statue, Christ the Redeemer statue in Brazil..."_

_"So... You had_ Santo Clós _on your radar, and they made your better systems and staff as fools, right?"_ said Hufflepuff _"Okay... But what we have to do with all this?"_

_"I'll came to it soon, Mr. Hufflepuff. Sometime after, we came with some intel about who would be Santa and where we could find it. And he came into contact with us."_

_"Suing you for trademark infringement on SANTA?"_ joked Dumont _"That would suck hard."_

_"Not a joke at all: he just wanted to make himself known and to announce to us his mission of bringer of hope for kids. We took the information he passed us and sent a team of capes to check it, and crossed it with files on all intelligence sources we could. The only one alive from this team is Blackstone: all the others had already died into other actions, including Atlas and Ajax. If you look into your binders, there's the After Action Report of this. Need to say, this is classified intel, I can't provide this for you."_

Jiminy took his own and used his super-powered mind to speed read it while skimming and take some first guess information, and read it better while Commander LaSarte continued.

_"So, first of all, we confirmed Santa's story and real identity."_ said LaSarte, activating the big screen and showing two photos: the left one from a plain looking guy, with some German/Swede traces. The right one couldn't be described otherwise except as Santa.

_"This is our Santa: Sven Carlsberg, at the Event 20, now 38. Previously a clerk at a bookstore at San Francisco. UCB major on English Literature, post-grad on Classical Literature, was in The Event time on a Ph.D on fairy tales and legends. Single, no criminal records, not even a DUI, except two parking fines. He said that, during The Event blackout everyone had, he felt pine and peppermint smell and heard the noise of little bells. Some weeks after, when the commercial air flights were restated, he received an envelope with a map, a flight ticket to Rovaniemi, on Finnish Lapland, and a small note saying: 'We need you, Santa!'"_

_"So, our Santa is a Breakthrough."_ said Dumont. _"Not surprisingly: you could even imply him as a Verne or Merlin or something likewise. And/or have people with those powers under him."_

_"But there's more."_ said LaSarte

Jiminy could see under his visual field Blue Fairy dressed like a judiciary note-taker

_"What are you doing, Blue Fairy?"_ said Jiminy

_"I'll cross-referencing this intel and LaSarte's history with the_ Future Files _as soon this ends. This doesn't look like fishy, but it's better safe than sorry."_ said her

Jiminy got back to Mr. LaSarte

_"After arriving Rovaniemi, he found a guy called Bernhardt that shown what him a Santa Suit, very luxurious. They got a sleigh to Christmas' Village. We can't trace it in the map, as it is so near North Pole that the compass spun around like a top."_ he said _"When he got at Christmas Village, he saw lots of small and pudgy people with Elvish looks."_

_"Santa Helpers..."_ said Sugarplum

_"Yes. They were readying everything for the Christmas Journey, when Santa got everywhere to deliver his gifts. It was when Bernhardt revealed himself as a Santa Helper and explained all about Christmas Village and how Santa disappeared when The Event happened: looks like they always existed, but they were not 'anchored' to our reality, until The Event. And Sven was the last potential Santa they fount: some of the others had turned themselves into Santa Helpers, some just get back their lives, aside those that thrashed or burnt their mails, taking those as a very elaborated prank."_

_"And how he became Santa?"_ said Sugarplum _"I know that you could change magically someone complexion, but it would be illusory. This image looks so real, it's like he gone old at least 50 years in less than some months, or even days!"_

_"I'm just getting there, Ms. Sugarplum. It was just when he asked Bernhardt how he would became Santa. Santa Claus is not a person, so to say: it's more like a_ title. _There's only one Santa_ at each time for real, _but it was not like the_ first _Santa ever was still there. As soon Santa became sick or too much old or tired from being Santa, he should retire as Santa and find a substitute, someone to replace him by, Bernhardt's words_, sign the Santa Clause, _assuming Santa's Mantle, as they call becoming Santa. When there's a Santa Claus, just what is needed is some kind of special contact, like in a hug or a hand shake, in the right moment when the receiver understand that he'll become Santa and is voluntary on this. This transfer the Santa Mantle from Santa to the new Santa, the old Santa retiring as an old man that would live some years, a decade or two, before dying as anyone else. However, when there is no Santa for some reason, the person should don the Santa Suit, as Carlsberg did. As soon he donned the suit, magically he became Santa. I think, and all the evidence points to this, that this was like a breakthrough, by providing him with the powers that are expected of Santa."_

_"So, the so called Mantle is a breakthrough trigger..."_ said Soldaire.

_"For sure... And this brings us to nowadays and our problems."_

_"As I said, after we discovered about Sven Carlsberg, or better_, Santa, _we established protocols on what to do, dealing with all issues on him. One of them is about the SANTA callsign on his sleigh. We could not let any crazy guy use_ Sierra-Alpha-Nevada-Tango-Alpha _callsign out of nothing, this could be a big national security blunder! However, at the same time we just can't fly and try to take down someone using_ Sierra-Alpha-Nevada-Tango-Alpha: _how could The President of United States came on TV and say_ US killed Santa?"

_"To solve this, we developed a validation protocol: during the year, Christmas' Village get into contact with us on variable, previously negotiated, intervals. The time between the contacts get nearer as the year goes, until they got daily after Thanksgiving. During the week before Christmas, they got half-daily and so."_

_"And the problem is... Recently Christmas' Village ceased the contact. We don't know what happened, but some surveillance satellites shown there was some kind of problem in the region... There is no real way to get photos there, because, as we said, we believe that Christmas' Village is an extra-reality pocket anchored into our world since The Event. The fact that even our best Vernes weren't able to do anything that can be used to survey Christmas' Village is somehow a response for this."_

_"So..."_ said Jiminy _"You need us to go there and see if there's everything okay and discover why they didn't looked for the_ SANTA _protocol. But why us?"_

_"You are unaligned with governments or government agencies, as part of a transnational, global NGO. Any military operation on this place could raise some unwanted and undesired flags: it would be like look for the governments of Finland and Sweden and say_ 'hey pals, we don't believe you can take care for Santa, let us do the job'. _And with all that happened last decade, this would be a real problem."_

_"And so... If something happens with us..."_ said LionHeart _"The Government will deny everything."_

_"Yes... Imagine how chaotic things could be if Santa's existence would be revealed: do you think that Seif-Al-Din would not try to kill him if he knew about it before? And what about those nuts from Undying Caliphate? No offenses on your muslim friends, but I know that they think Santa is a paganism, a_ haram."

_"No problem..."_ said Djanni, a little uneasy _"But what about other terrorist guys, like One-Landers or even some of those Christian extremists nuts like The Zealot that works in Ireland or The Twelfth from Great Britain?"_

_"You have a point."_ said LaSarte, accusing the attack _"However, back to the mission: this is why there's no reference on Santa or even other Christmas Spirits like the Santa Helpers into_ Barlow's Guide to the Super-Humans, _to preserve their secrecy and, by this, the children's innocence. In fact, I think there was no reference of those kind of breakthroughs anywhere: I don't saw an Easter Bunny there, although there's The Rabbit."_

_The Rabbit_ or better _Le Lapin_ (real name: Christien Mileux) is a B-Class French Metamorph whose power is to become any kind of cartoon. He also has access to cartoon physics, violating somehow the laws of physics, under certain limits: he could not become as strong as an A-Class Ajax or Atlas, although he could eat some a ton of TNT and nothing bigger then an almost ultrasonic BURP happens with him after the explosion. He acts on many teams, like a lone wolf with some feet on vigilantism and cause super-villainy. As a side effect, he was always on a cartoon form or another.

_"So, LDS had came to a conclusion: Santa wants privacy, and he's inoffensive enough to be left this way by now. We know that he got away during the year for some 'field research'. I believe it's all that Nice or Naughty thing. This is also good for children and for our society. And we left him alone as far he comply with some rules, especially on driving that goddamn sleigh."_

_"Ok. So to the mission: what about supplies?"_ said Sugarplum _"If I understood, he lives into an extra-reality nearby Lapland, and on this time of the year there will be frostbiting cold! We need some supplies."_

_"No military grade things, sorry: this is classified, even some people over my clearance level doesn't know about this. And as you're civilian, offering military grade things is totally verboten. What we can do, anyway, is provide some basic supplies and money enough you can deal with it."_

_"HSF knows about all this?"_ said Soldaire

_"Yeah... We tried other HSF teams in US and Canada... But they were all with small fish breakthroughs, and they always talk to take the fixed teams. And we heard about you from some very special asset..."_

_"Blackstone!"_ said Jiminy

_"Exact. As an ex-US Mariner and intel specialist, I believe he recommending you is something to be put under consideration."_

_"And about support?"_ asked Dumont

_"There some HSF capes at_ Rovaniemi _this time of the year, because the hunters and people that lost themselves into the Arctic. Unfortunately, no heavy-hitters: we are hoping this stays as an investigation mission. A combat one would demand lots of cooperation with Finland, and I can't trust 100% we would receive help."_

_"Okay... And how we'll discover where is Christmas Village? If all you said is truth, we could lost ourselves in the Arctic, running into circles until we become ice-cones."_ said Jiminy

_"There's a way: we'll lend you something that Santa gave us."_ said LaSarte, taking a small red box, with green ribbons all around. As he opened it in front of the team, there was a kind of old-fashioned looking device, like a pocket watch, made of brass. As he opened the device lid, they could see a weird looking compass, that had a kind of fifth cardinal point, a little aside from North.

_"This is an special compass that Santa gave us via Blackstone on Santa's first contact: this is called_ Christmas Compass. _We still don't know how it works: our guys from Vernetech had tried to reverse engineering this and they FUBARed two other of those without making any discovering except that he works also like a compass and has some kind of unknown Vernetech. This is the last one we still have, so please take care."_

_"For sure they wouldn't reverse-engineer."_ smiled Sugarplum _"This is Vernetech, but there's also magic on it. I can feel it, even without which tradition was used on it. Very clever, even being dangerous: do this without knowing what to do, and there's a chance to blast everything on smithereens."_

_"A Vernetech/Enchanting combo?"_ said Jiminy astonished _"I don't think that even if Vulcan and Ozma could not do this, and they are some of the most powerful Verne and Merlin I know... Besides you two, Dumont and Sugarplum."_

_"Thank you, Jiminy, but Sugarplum is right."_ said Dumont _"Mingling magic on Vernetech is like mixing matter and antimatter: they both exists, they both operates under some rules, but mix both the wrong way and you can do all kind of weird, dangerous thing."_

_"We hadn't know about this magic..."_ said LaSarte _"My bad... However, back to the mission: the compass doesn't spin like a top when nearby the North Pole, but he points to a 'fifth cardinal point', showing the direction you need to take. We had made the experience as soon we took it, and a team of our boys got Christmas Village using it."_

_"So find the Christmas Village will be easy."_ said LionHeart _"But I believe that the problems will start as soon we arrive at Christmas Village."_

_"Do you have any photo on it?"_ asked Soldaire

_"No aerial ones, sorry, and the ones he have on ground doesn't give any useful intel for this mission."_ said LaSarte.

_"Well... Sounds a complicated one. But, this is good news, looks like we will have no need for any of our heavy hitters if we accept this one. This could allow us to stay under ORSL 1 with part of the team and do this one with the other."_ said Soldaire

_"Can I give a suggestion? I think Dumont, me, Sugarplum, Djanni and Marjanah could be the ones."_ said Jiminy

_"Sounds good: a heavy hitter for backup, Verne and Merlin for the Christmas Compass if things goes Foxtrot, and a Mentalist for some Astral plane help. I don't think that Scheherazade and Lahab would help, as their powers are most useful on places with a warmer weather."_ said Soldaire.

_"I want to go too..."_ said Scheherazade _"I know that, under Islam, this Santa is_ haram. _But he's also hope for some kids to have some happiness, I know it. And after what I suffered, and my sister too, under the Caliphate, I could not leave this behind. And... Maybe my powers would work with snow too..."_

_"Like it was a kind of 'cold sand'?"_ said Jiminy _"Sounds good... However, Soldaire is the Team Leader and he is the one with the final word."_

Soldaire looked to everyone. 

_"Okay... We accept this and will include Scheherazade for the roster Jiminy suggested, but under two conditions: (1) we want to have a satlink with them as much we can and (2) if we believe that, at any time, they are under risk, we may treat this mission as compromised and so we can call them back home."_ said Soldaire, looking to LaSarte _"My guys, my rules."_

LaSarte could see Soldaire's face, his helmet removed from the SolArmor suit, the Super Powered suit that provides him superpowers. With the helmet, their powers would be greater, but it use is strenuous to the body, so he had a time limit for using it, of 30 minutes with at least 3 hours cool-down between uses. But, aside his powers, LaSarte could see a commander, worried with their capes... And, even half of the team being underage, and foreigner, and under parole, they are loyal to him and he to them. He can appreciate this, and his needs are dire.

_"So..._ Super-aide pour les super-besoins?" said LaSarte going to Soldaire and shaking hands _"I can live with this, even if this cost me a Martial Court."_

_"How many time we have to ready ourselves?"_

_"No more than 72 hours counting by now. Christmas Eve is getting near and we need to have some time to put a plan B if things goes Foxtrot."_ said LaSarte.

_"Okay... Sugarplum and Dumont, you'll be the Team Leaders on this operation. The others, be ready for any fight but avoid them as possible. Stay alert and go safe."_ said Soldaire

_"We'll give you a ride back Chicago and then you'll can take a ride with_ Herós Sans Frontières. _We have intel of a flight from HSF to the Chinese States getting to Rovaniemi for some capes and donations. I can think that you can find your way after that, and also,"_ said LaSarte, giving the Christmas Compass _"I think you'll need this."_

Jiminy could give a better look to the Christmas Compass: the fifth Cardinal Point, pointing to Christmas Village, was a Christmas pine tree.

_"Alright, we'll do."_ said Jiminy

## Chapter 4

> "I think that, after all, there's no one better than other one under Allah (SWT.). Anyone that says otherwise is crazy, delusional, or ill-intended. Even under the Ummah there's people ill-intended: I know this on my own body, my own soul, that suffered and was abused. My mind was taken, my will taken, by those who took the Allah's gift for their hate-mongering. Curiously, it was the infidel that had pity and freed me from that burden, so I could used this gift that costed me so much on Allah's work of peace and unity."
>
> _From Wahiba Cissé's memoirs_

_"Alright, everyone had double checked for your supplies and tools?"_ said Dumont, as they were getting out the HSF transport at the Rovaniemi airport, that was almost took by the Arctic snow, the track being cleaned by a snow-tower

_"Yeah... But... Here's freezing!"_ said Djanni, pushing his jacket to himself as much he could

_"Let's get into the base. For sure there will be a hotter place."_ said Sugarplum, ushering all of them to get into HSF base fast.

Rovaniemi was the main city at Finnish area of Lapland, its Swedish counterpart being Kiruna. The place was beautiful, but there's lots of snow on this time of the year, as the Sun didn't stayed over the sky for too much, which made the lights be more subtle and diffuse, thanks the Earth's tilt.

They rushed to the base to avoid as much the cold as possible: even with the jackets everyone had, the weather was unforgiving cold, so much that Djanni, Marjanah and Scheherazade were freezing. They got into the base and found it was empty.

_"Where are everyone?"_ said Djanni, running to the shower, while Jiminy was looking the living room.

_"There's something on the board there."_ he said, while looking. _"Is written on many languages: I believe Finnish, Swede, and a weird one I don't understand, and there's also French and English. It says:_ 'We had gone for a rescue mission nearby North Pole. Call...' _I don't know what or who is this name, I can't even read this..._ 'For help.'"

_"Metsätäja."_ said Blue Fairy, using an ugly Christmas sweater and reindeer antlers over his head. _"You can pronounce it_ Meti-shtiah. _Finnish for_ Hunter. _Looks like he's the backup cape on the bench. And the weird idiom is Sami, from the local Lapland indigenous tribes."_

_"Okay... Call this guy, Jiminy. I think we need some warm bath after all this cold."_ said Djanni, while Marjanah and Scheherazade got into the woman's bathroom. _"There's two weird rooms next the bathrooms, like another bathroom without shower or bath-tube."_

_"They are saunas."_ said a man that came into with the flight pilot and copilot. _"I think you were looking for Metsätäja. Well, I'm the one and only."_ The man looked like Oaken from _Frozen_, strong, big and chubby, with a strawberry blond hair, a bushy seal-like moustache and a freckled face that looked to Jiminy like a oversized 8 years kid that gone into a very strict bodybuilding diet and exercise program. _"If you want to get warm after the snow, take a good shower and then I'll go with you to the sauna. This will warm you enough. There's towels in the bathrooms, so you can preserve your modesty. I believe Mariya is just getting back, she can show the girls' sauna. I read about you as soon HSF Chicago contacted us. They sent us your profile, so we are somewhat ready for you."_

Some minutes later, Jiminy was taking a good time on sauna, the hot air being a good alternative for the freezing Laplandish winter outside the base.

_"So, you are into that crazy hunt to Santa for NORAD."_ said Metsätäja _"Hufflepuff is a great friend: I worked with him some times at Kosovo and he talked about your mission. In fact, I know a little this and that about Santa. And, to be fair, by your look, you would not survive your first night at Lapland's wildernessby your own. You have no equipment, no knowledge, no nothing."_

_"And why you are being so aggressive?"_ said Djanni, contemptuously

_"Because you are nice guys that doesn't deserve to die into the Lapland whiteness."_ replied Metsätäja, smiling _"As beautiful nature can be here, it can also be unforgiving: wolves, foxes, bears, and all kind of wild predators lives there. And if you think your Atlas super-healing would help, one of the last ice-cones we rescued was a C-Class Ajax. He fell into a rift straight in the water, below the ice line, and froze to dead before he could regenerate his broken arm."_ said Metsätäja nonchalantly. _"And, to try to look for Santa, you are either the nicest or greediest people. If the latter, the nature will deal with you. If the former, for sure you'll find the way. But, if you want my help, I can turn things easier, as I know Lapland as my hand."_

_"Sounds a good idea for me."_ said Dumont _"It's HSF protocol to work with local capes as they know the place better than us... But I never heard about you."_

_"That's ok."_ said Metsätäja _"I'm a Paragon, and normally we are overlooked by_ Barlow's: _just the most powerful are listed, like Roulette. I'm just a guy that hunted reindeer here in Lapland, and developed a bundle of techniques as tracker, hunter and animal enchanter. Believe me: I could tame the most wild horses, and even calm down a raged elephant. I don't have any superpower, however: I hear the animals like you, I'm not Dr. Doolittle. However, I could somehow feel what the animal wants and needs to be at ease and can use this to achieve confidence with it and make them do what I want."_

They got out the sauna to the dressing room and after dressing some lighter clothes, Metsätäja served some cold cuts as dinner.

_"Reindeer: don't worry, an_ Iman _friend of mine prepared it under_ halal." said Metsätäja

_"Thank you, Mr..."_ said Djanni trying to say Metsätäja's name

_"You can call me Mylläri. My real name is Albearta Mylläri. Metsätäja is my callname, like your capenames."_ said Metsätäja.

_"But..."_ said Jiminy, looking for the cold reindeer cuts with sauce and potato on his plate _"I believed you liked reindeers. How could you serve us reindeer meat?"_

_"I like them... You need to understand that here in Lapland, they are more than meat source: they are transportation, clothes material, milk providers, even friends during the Winter nights. Reindeers are for us what camels are for berbers and horses for the Apache indians and mongolians, partners and part of culture. In some places, when you are born, there's a reindeer foal next to you. This is why I became this weird Paragon: normally, people associates paragons with the Super-Normal concepts from the old fashioned comics, like Hawkeye or Robin, but Paragons could be also people like McGuyver. We are not supers, but we are the nearest of a hero someone could be without a 'for real' breakthrough. The good news is that we don't need to pass through the stress you guys did."_ said Metsätäja, while they took their reindeer dinner, Jiminy including.

_"Before we go,"_ he demanded, while they were eating _"I need to say for you: as soon we get into the Arctic wilderness, I'm boss, right? I say run, you ask how fast. I say jump, you ask how far. I say hide, you go hide quieter than a mouse."_

_"Okay."_ said all of them.

_"Well... First, for clothes: I don't think you have any coldproof costumes, and, to be fair, those would only hinder you. I asked my partner here Mariya, the one that got with the girls at their sauna, to buy some essentials: clothes, food and tools, things like GPS and so.  You can leave your things here, I'll put them into a safe. Just take any weapons or tools you really need. We'll get into some hours: tonight you'll sleep here. Tomorrow morning, or so to speak, we'll take some breakfast and go: I'll go with Ávgos, my reindeer, and Maryia'll take you with a reindeer pushed sleigh at least till our first stop on the Arctic wilderness. There you'll go into a crash course on reindeer riding with a friend of mine: they are somewhat like horses, and they are better than anything in the Arctic, even than snowmobiles."_

_"Alright."_ said Djanni, looking for his clock. 

_"Time for a prayer?"_ asked Metsätäja

_"Right..."_ said Scheherazade, while they got their praying carpets and looked into their cellphones for the right direction to Mecca.

_"No problem at all: I saw other muslim capes doing the_ Salat. _If you want some privacy..."_ 

_"No, I'm okay."_ said Djanni _"We did this lots of time with Jiminy and the other looking, that's not a shame."_

_"Okay... So, do your prayers and we can call a day. Sleep early and well. We have a lot of snow to cover to get to Santa."_  he said, while Djanni, Marjanah and Scheherazade started their prayers.

# Chapter 5

> "So many breakthroughs believe that, after becoming super-powered, they became all-mighty. But Nature always comes to Darwin Award those guys. God bless it."
>
> _Comments from Metsätäja for PowerStreak, a Finnish superhero magazine_

A week after, they were hosted in a reindeer farm almost literally at Lapland entrance, ready to get into the Arctic _per se_.

_"Lapland is a geographic region that take parts of Finland and Sweden. We are at Finnish Lapland."_ said Metsätäja _"There's lots of places like this one here, reindeer farms conducted by Sami people. Good saunas, reindeers, and Sami huts, what you could ask more about here?"_

It was truth, Jiminy thought.

They arrived at the end of the first day, when the farm owner, an old friend of Metsätäja, provided them some place to stay protected from the unforgiving cold and chilly winds: rustic huts, but warm enough to be comfortable, with big beds with mattress made of reindeer hide. There was five days of reindeer riding training and five nights of exchanging stories at the fires on the main Sami hut, eating reindeer meat and other good, savory foods and lots of sweets. All of them were getting a little more pudgy thanks this so caloric diet:

_"That's good."_ said Metsätäja _"This helps you to avoid cold. Skinny people suffers a lot here."_

It was when they bade farewell for the farmer and to Mariya, all on reindeers, getting back to the wilderness. 

The next day, they were some miles away, but now the icy winds were making they go slow, and all the snow in the air making them see nothing, even using snow masks and so.

_"We should stop and camp now!"_ said Jiminy, using the Earbug to be heard over the chilly and fast winds _"It's dangerous! We can't see nothing!"_

_"Look for the compasses, Jiminy!"_ said Metsätäja

Jiminy saw the compasses he had with him, and he could see that the common compass was spinning like a top, while the Christmas Compass was locked on front.

_"We are nearby!"_ said Metsätäja _"We're already somewhere next the magnetic North Pole! This is why the compass is spinning like a top"_

_"How much we need to go?"_ said Jiminy _"Less than some hours? It's nonsense to stay going forward if not, even more with this blizzard."_

_"I believe just some hours, four or five at most. I think we'll arrive before the sun goes down."_ said Metsätäja.

_"But there was no forecast for blizzard today."_ said Marjanah _"We checked!"_

_"Yeah... But Lapland's weather is capricious somehow..."_ said Metsätäja.

It was when Jiminy felt something: his _Bellax Analytica_ rang into himself.

_"Something is coming! I can feel it! Tangos are coming!"_ yelled Jiminy

Djanni was already to get out his reindeer, when he felt it was uneasy. Djanni took a whiff in the air and said _"Something dead... There's a small stench of necrosis!"_ 

Metsätäja yelled

_"RUN! GO! Don't try to fight them!"_ said Metsätäja, as they saw some weird guys mounted in ash sticks flying to them

_"What are those things? They look like people, but with snow white faces!"_ said Scheherazade.

_"They are Cold Hearted, children that the Ice Queen took and turned into undead servants! They are cruel and callous! Don't try to fight them: the Arctic is their home, they have lots of power here!"_ said Metsätäja.

Metsätäja and the others made the reindeers run as fast as they could, but they could see the Cold Hearted flying fast, as the blizzard had no effect on them. It was when some of them passed in front of Jiminy's reindeer and made it goes rampant, prancing crazily in the air and bellowing a very scary warning, throwing Jiminy out of it, making Jiminy falling on the cold snow in the ground, hitting his head on something below the snow.

_"Jiminy!"_ said Djanni, turning back to rescue Jiminy.

_"Djanni, no!"_ said Marjanah

_"We need to get back!"_ said Metsätäja. _"Jiminy's with the Christmas Compass!"_

They got back and circled Jiminy, Sugarplum readying some wards while Dumont pointed his Tesla Arc Cannons to the weird, sinister tangos. Djanni was already unmounted from the reindeer, looking for Jiminy.

_"How he is?"_ asked Metsätäja to Djanni

_"Unconscious. Look like he hit his head on a stone below the snow. This is the bad news. Good news, he's alive and not critical."_

_"Okay..."_ said Marjanah _"Worst news: those guys are really undead. I can't read their minds or see their souls in Astral plane. So, they are only animated corpses almost without free will.."_

_"So..."_ said Djanni _"Let us tackle them out of our way."_ 

It was when he looked they were outnumbered 3 by 1!

_"What do you want?"_ demanded Scheherazade

One of Cold Hearted got in front of the others. They could see that it was the leader, and now they could see that the black vest shirt they were using were opened in the chest, showing weird icy heart shaped pendants in their sickly corpse blue ribcages.

_"We want the Christmas Compass."_ said him, with a cold, callous voice, that made Djanni flinch a little.

_"Yeah? So you don't want us to find Santa."_ replied Marjanah

_"This is not your problem, muslims. I know you never believed Santa. You take him as_ haram. _And we could help you to cleanse this sin."_ said the creature, giving a small smirk.

_"Do you have no feeling for kids all around the world?"_ said Scheherazade

One or two of them gave a little flinch, but Metsätäja said:

_"No, Scheherazade. They are called Cold Hearted for exactly this reason: their hearts were frozen on their last heartbeat. They feel nothing, except the compulsion to do what their cold leader, the Ice Queen, demands them to do."_ he said, taking a big hunt rifle. 

_"Let them to us."_ said Djanni, while Dumont, Sugarplum, Marjanah and Scheherazade got aside Djanni. _"We'll stop those guys. Get Jiminy and go for Christmas Village. Bring help: we'll need it ASAP!"_

_"Sure..."_ said Metsätäja, while taking Jiminy and putting him over Ávgos.

While he was mounting the reindeer, one of the Cold Hearted tried to get to him, just to be hit straight in the face by Djanni

_"No way you take Jiminy,_ murtadd." said Djanni, while Ávgos galloped and ran, bellowing before go, Metsätäja holding its reins over Jiminy unconscious body.

And the fight started.

Djanni pushed two of them, using techniques he learned with Kuntur, LionHeart, Hufflepuff and Prop, the bricks of his team and of teams he got into contact, but he was hurt by one that used a kind of ice made rapier.

_"Ouch! This really hurts!"_ said Djanni

_"Looks like their weapons are magically reinforced to deal damage enough to hurt Ajaxes or Atlases."_ said Sugarplum

_"Sugarplum, don't fly."_ said Dumont _"This blizzard is getting stronger. Fly will be death!"_ he completed, while shooting his Tesla pistols on them.

_"They feel nothing!"_ said Scheherazade using some snow as she would do with sand, turning it into a small rain of glass-like shrapnel. _"No matter how hard we hit them, they just keep coming. It's like a nightmare!"_

_"Let me see something:_ Bel, tighearna an tine, tabhair do bheannachtaí ar mo lámha!" said Sugarplum, shooting some flames from her hands to two Cold Hearted. One of those flames exploded straight into the heart-like pendant in the chest of one of them, making him go back and fell in the ground, defeated. _"That heart shaped pendant: it's their weak spot!"_ 

_"Alright!"_ said Djanni, going and punching one of them straight on that spot. He felt the crash of ice, but soon he felt something warm, somewhat gooey and very familiar, on his hand. He looked to his hand and felt some disgust, as there was a very familiar red liquid on it: _"Blood! Those icy pendants are their hearts, frozen!"_

_"Exactly!"_ said Dumont, shooting some of them with a bullseye's precision _"This is their tragedy: they are beyond rescue. The kindest thing we can do to them now is to kill them fast and painless, making their hearts beats one last time."_ said Dumont, his bullets hitting the open chest of the Cold Hearted he shot, exploding their frozen hearts like bloody balloons

_"No way!"_ said Marjanah, using his mental powers to enforce a shield to avoid some attacks _"There should be a way to rescue them."_ she said, raising a field on sheer willpower to avoid being hit by them

_"If there is,"_ said Sugarplum _"it's out of our power by now. Even I, with my glamour, could not undo this magic without know what did it."_

_"I..."_ said Marjanah

_"I know you don't want to kill, Wahiba."_ said Scheherazade, making the snow become a sharp wind against the Cold hearted _"but there is no way under our power to undo this evil magic. For those guys, unfortunately, there's nothing we can do, beside a clean kill."_

"Maktub, Mirian." sighed Marjanah, saying her sister name as she did hers before, shooting one straight on the chest with a small pistol.

Djanni was attacked by two of them and they made some vicious cuts on his left arm with their ice rapiers and some claws like nails, making him scream in pain, falling in the ground.

_"I'm going for you Djanni! Everyone, give me cover!"_ said Sugarplum, using her fireballs against Djanni's attackers, making them get back enough they all could encircle Djanni and Sugarplum, shooting as much Cold Hearted they could, their ice barriers avoiding the enemies attacks.

Sugarplum looked to Djanni's arm: it had took an icy tone and was cold by touch.

_"Looks like they hit you with some vicious magic in that rapiers and their claws."_ said Sugarplum, taking her fairy wand from her utility belt _"Let me see if I can at least block it, to avoid it get through your body."_ she said, passing the wand and doing some mojo. _"Looks like I stanched whatever magic they used on you to spread over your body, this is good news. Bad news is I can't heal this by now."_

_"Okay..."_ wimped Djanni, getting over his feet _"Let's go!"_ he said, going to fight even knowing his left arm was death numb.

_"C'mon, Metsätäja!"_ said Dumont, looking for his pistols, now depleted from bullets, the Cold Hearted come and come _"We can't hold them forever!"_

It was when they heard bells ringing, and the bellows of reindeers and woof of dogs. They could hear a war cry, and see people on garish colored clothes, with brass-made blunderbusses that launched thunder and bows that looked like children toys throwing all kind of arrows, some of them that explodes on contact, other that follows those they where targeted upon, all of them hitting the Cold Hearted straight in the heart. A Cold Hearted ran away while those weird guys got nearby the group, one of them going to Djanni.

_"Nice you have a Merlin on your team: the magic she did helped to avoid the Cold Heart infection to grow into you."_ he said, taking some weird tools and looking for his arm _"I'll give you some first aid so you can move your arm again, but we'll do something to allow you to heal yourself at Christmas Village."_

_"Lo! Allah is forgiving, merciful."_ said Djanni, exhausted, when he looked to the guy who was taking care for him. He had a kind face, more or less like his, from Mediterranean stock, but with a petite button of a nose, big, rosy cheeks and pointed ears. He also looked a little smaller and pudgy than he could expect, dressed into green pants and yellow jacket.

_"Who are you?"_ asked Djanni

_"I'm Saleh, and I came from Lybia. Now I'm a Santa Helper like those with me, living in Christmas Village to preparing everything for Christmas."_

_"And Jiminy?"_ demanded Sugarplum, worried

_"The boy is okay."_ said a woman, dressed into a big burgundy flower dress, a belt holding it on her body, boots helping her to walk in the snow. _"I'm Susan, and I was the one who looked for your kid. The reindeer got scared with the Cold Hearted and threw your kid in the ground. Just a concussion: he'll wake up soon, and sounds like there was no extended damage. Now, let's get back Christmas Village, before the Cold Hearted came back again. Looks like they knew about your demand and tried to stop you. And we need to treat this guy that was hit by the Cold Hearted's blade."_ 

_"And where is this Christmas Village?"_ said Scheherazade, while the blizzard stopped, and all they could see was the Arctic night, with the Northern Lights over them... And a village of cottages in the horizon, painted on garish colors, with very bright lights everywhere.

_"That is the Christmas Village, ladies and gentlemen."_ said one of them, into what looks like a Renaissance set of clothes, on dark green, the only one on not so garish colors. _"I'm Bernhardt, Elf-in-Chief, and I welcome you to Christmas Village, under the Old Traditions and Laws, in the name of Santa Claus."_

## Chapter 6

> "[REDACTED] was a incredible place. It was sorry I didn't saw it when I arrived there: I was unconscious after [REDACTED]. So, the first thing I saw there was the infirmary."
>
> _From Jiminy Cricket's Journals_

Jiminy started to open his eyes.

_"Hey, Jiminy, wake up!"_ said Blue Fairy 

_"Good to hear you, Blue Fairy. I know I'm not dead or power-blocked."_ thought Jiminy, when he felt a big ache _"OWWIIEE!"_ he whined when he tried to get up. He still feel ashamed on his uses of terms: it was like he was still an eight years. 

He opened his eyes and looked all around and the colors and smells could not be described other way by Jiminy beside festive. He looked around and he noticed he was on some kind of magical wing and a small woman was coming for him.

_"Hold still, kid... I'll lift a little the head of your bed, so you can see things and we can talk. Don't move too much: you had a very hard concussion and passed out, and we don't want this to grow bad, right?"_ said the woman, with a little sing-song voice. He could see she wasn't exactly high, but it wasn't small, also, looking like she was a common size woman shrunk to a 14 years old size. 

She was slim and used a very festive set of clothes under the doctor's white overcoat: green waistcoat over a white shirt, red skirts that gone to the knees, with white petticoats, green pantaloons and white and red striped thighs, a green pair of socks and a pair of green velvety elf boots, with upturned points. She was using a green headdress with white laces on its hem. Her face was a little olive, looking somehow Latino, but she had green bright, almost electric, eyes, almond brown curly hair on pigtails and blushed cheeks that gone almost to the blushed petite nose. On the side of the head, pointed ears were with her stethoscope into them, when she took it out and put it around her neck.

_"Are you an... Elf?"_ said Joshua, while she adjusted the bed, putting Jiminy into a seated instance.

_"We could say this way, but we prefer to be called_ Santa Helpers." retorted a little the woman _"My name is Melissa, although people here call me Milly Milkyway, because I like_ too much _hot cocoa they gave me this name."_ she said, taking two big mugs and filling them with hot cocoa from a big white porcelain _samovar-_like pot. _"Want some marshmallow? Cinnamon? Or just sugar?"_

_"A little bit of marshmallow, thanks."_ said Jiminy, when he looked that he was on a new change of clothes: a big, red, onesie with REAL gilded big buttons _"Where are my clothes? Where I am?"_

Melissa gave a giggle _"Don't worry: your reindeer gone rampant with the Cold Hearted, dropping you in the snow and Metsätäja took you and run to here. As I said, you had a concussion when you fell from the reindeer, and your clothes were so filled with snow and soaked with cold ice that there was a risk for frostbite. So, we changed your clothes and send it to dry. We'll provide you a proper costume as soon you get better. We know enough about you to do it. And, about where you are: you're at Christmas Village."_

_"And my friends?"_ demanded Jiminy, trying to lift again, when he felt the pain in the head again _"OWWIIEE!!"_

_"Here, take this and stay calm, kid: you'll feel better soon."_ she retorted, passing Jiminy the big earthenware mug, almost a tankard, full of hot cocoa _"As soon Metsätäja arrived, he left you with us and took people to help your friends against the Cold Hearted. Lots the people had gone: fighters, healers, everyone. And they took some Toy Soldiers and Nutcrackers to help the fight against the Cold Hearted."_ she said, gently pushing Jiminy back to the pillows. _"Stay calm and rest a little: you had a big of a concussion. Reindeers are greats, but they are terrible when gone rampant. Lucky you were not kicked by one of them: saw some D-, even C-Class, Ajaxes with some very nasty wounds after being hit by them."_

_"Thanks:"_ said Jiminy, sipping a small sip of the hot cocoa _"Yeah, this is great stuff."_ he smiled, putting the mug a little below his nose, smelling the hot chocolate perfume before taking another gulp on the hot brewage, that gave him a much needed heat from the inside.

_"We work hard here, so we like to appreciate those small, fulfilling comforts. And what about that little cyber-pixie on you?"_ said Melissa.

_"How do you know..."_ was saying Jiminy.

_"The TA has, or had, a network of supporters all around our time. Santa is one of them. He gave us some instructions on how to detect people with quantum links, and as soon you arrived, the Alert Bells rang showing you had one with you."_ said Melissa. She then pushed what looked like a big snow-globe, a little small than a fortune teller globe. _"Just touch this: you can download her to this one, so we can talk normally."_ explained Melissa

_"Are you okay with this, Blue Fairy?"_ thought Jiminy, while he saw her changing into something like Cindy Lou from _The Grinch_.

_"Well... In for a penny, in for a pound."_ she said, while Jiminy took the snow-globe, feeling the weird sensation when she "downloaded" herself somewhere else. 

_"At least is a big place here."_ said Blue Fairy

_"Woah, such a cutie thing!"_ said Melissa _"Sorry, but after we sign The Christmas Help Clause, our Santa Clause, looks like grew into us a fondness on cute, cuddly things. So, are you his cyber-pixie? Another 'Shelly Boyar' quantum copy?"_

_"Yeah, but I prefer to be called_ Blue Fairy... _Although I'm more like a_ Red _Fairy now."_ said Blue Fairy, curtsying for Melissa.

_"Well, looks like you're a good one: so this is why 'Shelly Boyar' appears three times on the Nice list..."_ said Melissa _"Now, let me look this bump."_ she said, taking the bandage around Jiminy's head, which made him cringe some times, when she passed her hand over the bump.

_"Go easy with this. It hurts."_ cringed Jiminy.

_"Don't be such a whining boy."_ retorted a little Melissa, finishing removing the bandage _"Okay, looks like a little less swell. I'll gave a look with my MedSpecs and see if there's anything worse. If not, I'll redo the bandage and then we'll double check you before we can clear you out by now."_

She took from a pocket in her overcoat something that looked like a weird brass _pince-nez_, like the ones Dumont used as Mask-cam occasionally, putting over her right eye and giving a small tap on it before started to look around Jiminy. He could see a small blue-ish light getting out the _pince-nez_ while she looked around his head, specially on where the bump was.

_"Alright."_ said her, giving a small tap on the _pince-nez_ with the fingers and taking it out, before putting it back the overcoat pocket _"Nothing on the MRI or X-Ray. Yeah,"_ said Melissa, when looking Jiminy's face _"this is a little Vernetech I built after becoming Santa Helper. With somewhat half a billion kids all around the world, and as much gifts to be built, packed and stored during the year, we don't have too much time to spare with people on the bench, so dealing fast with accidents like broken shins and burning hands is a_ sine qua non _demand here. So, I did those MedSpecs: MRI, X-Ray, ultrasound and other exams in real time, and also can measure some body stats by itself, like temperature, heartbeat and so. This way we can improve and speed up treatment, avoiding to have helpers in the bench."_

_"Nice trick."_ agreed Jiminy, while Melissa took and put some iodine and Merthiolate on the bump _"Ouch! It stings and hurts"_

_"We do some things old school here, sorry."_ said Melissa, while putting it with some lint before bandage the curative back again  _"Now, it will hurt a little while I finish the bandage to make everything to be on the correct place. I believe that the guys from dressing and seaming team we'll bring you a good costume to dress after we double-check you."_

She finished the bandage and asked Jiminy to get out the bed, give a small walk, and turn left and right.

_"No wooziness? No pain? Just a sting, maybe?"_ asked her, and Jiminy shook his head _"Okay, looks that wasn't that bad, but just for sure don't run, do sudden moves or too much effort for some hours. Also don't use your Top Hat and mask: the bandages will be forced against your head and this would only make things painful for you. Just put the costume, no need for the mask here: there's no bad people here in Christmas Village, at least for kids in the Nice list."_ said Melissa, while some Santa Helpers come with a small parcel that Melissa gave for Jiminy.

When Jiminy opened the parcel, it was a version of his own costume, but very... Festive: yellow waistcoat, red cravat scarf, white starched shirt, dark green pants and tails with red sleeves, with gilded buttons and some red on the clothes hem. His black boots now were dark green, with some small bells on it. The mask was as by his common design, but they put a red collapsible Top Hat and a checkered black and red casquette with white details on the hem instead of his classic green Top Hat. On the cravat scarf, as all around his waistcoat and the brim of tails and casquette, there was Christmas textures, like reindeers, snowflakes and pine trees, and there was a big mistletoe shaped gilded pin to hold the cravat on his shirt. Melissa closed the curtains around the stretcher so he could have some privacy. He saw that the shirt sleeve had some green embroidery, his initials _J. McC._ on it. It was like it was a Victorian Caroler version of his own costume

_"Comfy?"_ asked Melissa

_"Well... Not what I normally dress, but really liked it. I'll use this casquette: looks more comfy."_ said Jiminy, finishing to dress himself, just placing the casquette to hide the bandages around his head, and then he opened the curtains, revealing himself.

_"Woah... A really handsome guy we have here!"_ said Blue Fairy, and Melissa nodded.

_"Alright..."_ said Melissa, when they heard some  bells... A little festive tune. _"Looks like your friends were successfully rescued. Let's go receive them."_

---

Djanni and the others got through the Christmas Village walls and looked around. Djanni was being supported by Dumont, as his left arm was still numb and unable to move.

_"Why you lot are dressed so garishly?"_ said Sugarplum

_"If you are lost in Arctic, this is the only way to be found, to use the most garish colors you can. Neutral or pastel colors blends easily with the snow."_ said Saleh, that was walking with the others, while the Santa Helpers were cheering for the team that get back.

_"Where's Jiminy?"_ asked Dumont

_"He's okay."_ said Metsätäja. _"He was unconscious when I got back with support for you, but didn't looked a so big concussion that he would not be okay now."_

Then they were conducted to the Entrance of the main building at Christmas Village.

_"This is where we do our main job:"_ pointed Bernhardt _"There's some R&D that, as they're dangerous, we do in some of the chalets outside. And also there's some houses and support structures, like kennels for snow dogs from local villagers and Rovaniemi people that help us, and of course the reindeer's stables, but the bulk of everything in the Christmas Village is done in the main  building."_

They opened the door, and they could see Jiminy, on his new Christmassy costume and so. Melissa looked Djanni's arm and run to him.

_"Jiminy!"_ shouted Sugarplum, running to him and hugging him _"Glad you're okay!"_

_"Ouch!"_ said him, when she touched the bump _"This bump is still hurting..."_ he whined, when he looked to Djanni _"What happened with Djanni?"_

_"He was hit by a Cold Hearted's ice rapiers and some bad juju had get into him."_ replied Sugarplum, looking worried _"I did my best to stanch whatever was there, and that Saleh guy did some things to improve things after, but looks like the thing is really vicious."_

_"It's a Cold magic curse: lucky you did the First Aid... He would be dead otherwise."_ said Melissa, worried, looking for Djanni's arm.

_"Milly, can you do something?"_ said Saleh

_"Let me see."_ she said, taking the MedSpecs and looking his arm _"Look that the sword's magical venom was refrained in the arm, this is the good news. We'll need to do some exsanguination to remove this bad stuff. Could some of you help us?"_

_"I'll do."_ said Dumont

_"Me too."_ demanded Jiminy

_"All of us or none at all."_ said Sugarplum _"Besides, I want to see what is this juju. Know about magic is my job, after all."_

_"Okay... Let's go to the infirmary."_ said Melissa

When they arrived, they could see Saleh taking a brass salver with some surgical instruments from a rack while Melissa put Djanni on one of the stretchers. The tools were all made of silver or aluminum.

_"Silver surgical instruments. The best for magical surgical procedures."_ said Melissa _"Now, Djanni, we can't put you under anesthesia thanks to this bad magic, and this will hurt. So, be brave and hold still."_ 

_"Go on..._ Maktub." said Djanni

They unceremoniously ripped the left arm of Djanni's costume, and they all could see that he skin was really really whitter than normal for Djanni's olive skin.

_"His arm is almost frozen: the infection had gone really far, we can't just remove the contention magics by now."_ diagnosed Melissa, looking the skin color and temperature on touch, using a latex medical glove to give some small taps on Djanni's infected arm, reading some information with her MedSpecs. _"Saleh, we'll need to excise and exsanguine as much as possible the bad blood from the arm. This will help to remove the curses from his arm."_ she then looked for Djanni _"Which is your blood type?"_

_"B+"_ said Djanni.

_"Someone else is?"_ asked Melissa _"This is not common under Santa Helpers unfortunately."_

_"I am."_ said Jiminy

_"Well, it's not advisable to take blood from a so young person, but... Desperate measures demands desperate procedures... Milly, call Rhonda and Will and prepare Jiminy for the blood donation. I think one unit will do."_ said Saleh, while Melissa made Jiminy sit on a reclining stretcher and got to an old fashioned wall telephone _"I'll do the exsanguination... We need to remove all the bad blood and with it the evil magic, before either the magic spreads or Djanni's arms goes necrosis. Hope we don't need Jiminy's blood, but better safe than sorry, and before Djanni got into shock we hope we have the bag ready."_

_"How?"_ said Dumont _"Preparing blood for donation is not that easy, it needs to be checkered against some diseases, filtered, sterilized and stuff."_

_"We have some Vernetech for this, Dumont."_ said Saleh, while two elves came with some weird machines, that looked like a steampunk brass-made dialysis machine, and put it aside Jiminy's stretcher  _"Now, please, let me do my work. Milly..."_

_"Rhonda and Will just arrived and we are preparing Jiminy."_ said Melissa, while the blonde haired Rhonda punctioned Jiminy and connected him to the machine, a blood bag on the other end of it started to be filled red with Jiminy's blood. _"This will purify Jiminy's blood, put it on a correct temperature, and ready it for transfusion, so we could take the unit and put it straight on Djanni, non stop."_ They looked for Dumont, that was flabbergasted, and then Melissa said _"We can give you some blueprints for those if you need. Think this will be very useful for your job."_

Sugarplum was looking for Djanni and saw that Saleh sterilized and skillfully made an excision on the internal side of Djanni's left forearm with the silver scalpel, which made Djanni cringe, he using all his will to not cry or swear. Saleh put a large brass bowl below the point where he cut Djanni's arm.

_"Calm down, kid... I know this special, Ajax-ready, scalpel hurts, but if we don't do this, you can die... Or worse."_

_"Worse?"_ said Sugarplum, while looking a thick dark red fluid getting out Djanni's arm, that didn't look exactly as human blood, and rose some vicious colored vapors when touching the brass bowl below Djanni's arm, hissing as it was some kind of acid.

_"The Cold Hearted uses this vicious magic to ready victims to be put under their Mistress' will. Those hurted by them are put into a kind of hypnosis and then took and turned into new Cold Hearted by the Ice Queen. If we left this magic untreated, he would be irreversibly under her will by now. Lucky you did that First Aid, as improvised and rough it was. It was a great job for a first time, even more because you didn't know what exactly was happening."_ said Saleh, passing a cloth mask to Sugarplum, that she put on around her face _"Avoid to smell those fumes, they can have vicious effects too."_

_"That is a really bad juju."_ said Sugarplum.

_"It is a really very evil_ Sikr." said weakly Djanni _"I'm getting a little drowsy."_

_"Stay just a little more with us."_ said Saleh, worried. _"Milly, how it is there?"_

_"Almost done!"_ shouted her _"Just some seconds and the blood unit will be ready. Will, ready Djanni. Use the special scalp needles for Ajaxes. The hit arm may had lost some of the super resistance, but the other, where you'll punctured, has full resistance."_

_"Roger that."_ said Will, an auburn guy with a very freckled face, using red pants and a heavy yellow sweater with green waistcoat, taking a needle and, fast and skillfully, putting it into Djanni's arm. _"Access ready!"_

_"Alright."_ said Melissa, looking while the blood bag was finishing being filled with Jiminy's blood. _"Ready here. Rhonda, put the blood unit on him."_

_"Roger."_ said the other Santa Helper, taking the blood bag from the machine.

The blonde Santa Helper put the bag on an infusion holder, while Saleh was massaging Djanni's arm to make the last of the bad blood to get out him, and Melissa took another hot cocoa mug and some cookies for Jiminy, who looked a little drowsy.

_"Take and eat those to recover some strength and not pass out."_ said her, after removing the needle from Jiminy. _"Avoid move this arm too much or too fast for some hours, your arthery is a little hurt and can open if you go too overboard."_. Jiminy nodded while looking for Saleh finishing Djanni's treatment.

_"Okay..."_ said Saleh, when the last drops of the dark fluid got out Djanni _"I think we did enough. We now can lift the magics we did. How are you, Djanni?"_, he asked while either him and Sugarplum did some magic gestures to lift their magics

_"A little drowsy and hurt, but otherwise okay."_ he said, while Saleh stitched and placed some bandages where he cut Djanni's arm and massaged the arm to make the blood pass through it back _"Never found something my Atlas powers could not heal."_

_"This is the problem: this thing is a Trojan Horse. He counts on the body healing capabilities to spread itself around your body. For you it would be even worse: you would fall under the Ice Queen evil magic very fast, it wasn't your friend Sugarplum's help."_

_"Thanks, you all. How are you, Jiminy?"_ said Djanni

_"With a bump in the side of the head and now a little weak by the blood donation, but I'll live."_ said Jiminy, reassuring _"I had worst before."_

_"Now, let you all take some rest: we'll provide some clean clothes and a good bath, as your clothes are filthy  and you are a little smelly."_ said Saleh, which was truth: they had not how to take baths or change to clean clothes in the middle of the Arctic. _"Then we'll provide some food. The two boys will stay here for monitoring. Tomorrow, you'll talk with Santa."_

## Chapter 7

> "[REDACTED] was a really a big place. After we get next day to take some breakfast, we took at least 1 hour on a tour there before we got with [REDACTED]."
>
> _From Jiminy Cricket's Journals_

Next day, Jiminy and Djanni woke up in the infirmary with the smell of hot rolls, hot cocoa with some cinnamon and sweet rolls, that Melissa brought to them into small silver trays.

_"Now you two, eat and I'll check you. Think that I can clear you from infirmary now, but better safe than sorry. Djanni, your new costume is ready so you can dress yourself after you eat."_ she stated, and it was a need: they were dressed into green onesies for the night.

They took their breakfast, took a bath on the big china made bathtub in the infirmary (reinforced for Ajax/Atlas use) and dressed into their costumes. Djanni liked the "Christmas Version" of his costume: red with gold embroidements and white socks, it had a aluminum blade on a sheat in the belt. He looked for himself and approved.

_"Looks like he knows about us all."_ said Djanni _"It's weird, because Allah (SWT) is the only that knows all."_

_"Santa knows enough to do his job, no more or less. It's a lot, to be fair, but it's not like he's all-mighty or omniscient. He can look this way because he can infere things based on what he knows."_ replied Melissa _"We know, for example, that you are a nice guy, your name is Altayr Bashir ben Zayn Tahan and you're muslim"_

_"Wow... That's a lot."_ said Djanni

_"As I said, Santa knows a lot of things, but not all things."_ she recalled _"He can say if you were Nice or Naughty, but not why. This is up to you and your parents."_ she reassured _"Santa can only give the gifts for good kids... And some very weird surprises for those naughty kids that deserves."_

_"I saw also there's people from all around the world, like Saleh."_ said Jiminy _"How you deal with this? For example..."_

_"I know what you want to say, Jiminy: Saleh is a muslim, like Djanni. So, how he does his job as a Santa Helper? It would not be_ haram? _But, in fact, we all learn very fast we need to be... How can I say... Above and beside religion, in the worldly sense, of the hard, cold, stone-engraved institutions with their norms and Commandments and so. We need to understand that we have a bigger responsibility, with kids around the world, specially those totally, utterly bereft: think on those kids that has no parents, no family, no hope... Many times, Santa is their best, last hope for some happiness in a cold, cruel world: if you see someone killing your parents for an ideal, no matter it's called God, Allah, or Peace, how would you cope with this, when Divinities, Prophets and Principles are used as excuse for the very things they are against to? How would you feel about this? For those forgotten kids we live. If this is_ haram, _many of us chose to live with it."_ said Melissa, while checking both the kids, when he looked to Djanni.

_"I know what are you thinking, Djanni, but sometimes we need to think on that Cherokee tale that says inside everyone's soul there are two wolves fighting non-stop for dominance: one is evil, greedy, full of hate and rage and envy and prejudice and false pride; the other is good, full of joice, peace, justice, humility, empathy, benevolence. The one that will win, in the end, is the one you feed more. If you feed evil, no matter how good you think you had done or you are yourself, it will be always evil, and evil will win."_ emphasized Melissa, while finishing to check Djanni's arm.

_"I think I understand..."_ acknowledged Djanni

_"You understand because you are someone that feeds the good wolf the most. There are, on all faiths, those who feed most the evil wolf, and by doing this, distorts the beliefs for their own means: Self-Al-Din, as Zealot, as The Twelfth, as The Paladins... No matter what... They all have more in common they believe they could. As we have with you from_ Herós Sans Frontières. _It's easy to think we are in the 'good side', but we need to put all your actions and thoughts at the light of the Truth, to discover which of those wolves we are feeding in the end. For this, religion is good as a principle and a way to measure our actions. But, if you think your religion makes you better than everyone else_ just because, _you are feeding the wrong wolf. No one is better or worse from anyone else_ per se, _or because something external them, like religion or philosophy or nation or anything like this. In the end, there is our actions and thoughts that shows the world which wolf we fed more. As said by Jesus_, 'each tree is recognized by its own fruit. People do not pick figs from thornbushes, or grapes from briers'." continued Melissa, while finishing Jiminy bandage. Then she giggled and said _"But you are both from the Nice list, so you understand this."_

_"Yeah..."_ agreed Djanni _"Thinking now... I can't see this as_ haram. _You help people, and this is Allah's (SWT) Commandment. If people do_ haram _by idolatrizing you, it's not your fault, as you never asked them for this."_ 

_"So... As now we agree on those things..."_ acknowledged Melissa _"Why not encounter your friends at their room and look for Santa?"_

---

All the team was dressed into very festive versions of their costumes: Dumont had his traditional Fedora replaced by a Top Hat with some mistletoe on its brim, which curiously made him look like a Dickensian caroler, while Sugarplum now was on a violet set of clothes as by Sugarplum Fairy from _The Nutcracker_ ballet, and Marjanah and Scheherazade was into maroon colored versions of their costumes, with embroidements on gold and brown.

_"You are very handsome, everyone."_ said Jiminy, when looking for their friends, while they started to walk with the Santa Helper guide, going to Santa's Room.

_"You two also..."_ agreed Sugarplum _"How are you?"_

_"Better now."_ reassured Djanni _"Still a little sore, but when put against how I was yesterday, I'm as good as new."_

_"Good to know."_ said Dumont _"The bad news is: for some reason, all the radio comms to the outside are blacked out here."_

_"How do you know?"_ asked Jiminy

_"We tried to contact ORSL 1... No signal. Need to say: Soldaire will never admit, but he certainly is having kittens now, I know him enough. For him, the mission had gone Foxtrot times over 9000. Losing contact with an advanced team like us is one of the first signals of a Foxtrot."_

_"Do you think this can be a real Foxtrot?"_ queried Jiminy, while some Santa Helpers with weapons escorted them from a corridor to a big red and gold door, where a Santa Helper dressed as a page  took and opened the doors.

_"To be fair, Jiminy, we'll discover it now."_ commented Dumont, while the page brought them into the room and announced.

_"Jiminy Cricket, Djanni, Marjanah, Scheherazade, Sugarplum and Dumont, this is Santa Claus. Santa, those are the_ Herós Sans Frontières _team sent by NORAD."_ announced the page Santa Helper.

_"Thanks, Lawrence."_ said the man in the throne, looking for a weird piece of parchment, that looked like a mix between parchment and e-pad. _"This is my_ Nice and Naughty list. _I look it some time and another to see if there's some special case: maybe a nice kid on a very dire need, or a naughty one that really need some lesson. Those cases I take on my own, readying everything by myself. After all, good gifts are those you need, not exactly those you want."_ confided Santa

_"Now... I believe that you had not risked your lives to cross the Arctic snow just to learn about my_ Nice and Naughty list." declared Santa _"And looks like you came sent by NORAD."_

_"How do you know?"_ asked Djanni

_"Well... I'm not all-knowing, but I know enough to infere a thing or another. It's on the job description, on that part about know who had been bad or good."_ chuckled Santa, with a weird but good chuckle, those given by a cherished uncle before he gave you a hug or a lollipop. _"And I know that people on NORAD are worried about me and the SANTA protocol."_

_"Yes, Santa."_ reported Sugarplum _"They are worried that you could go rogue or had any problem..."_

_"Well... I know that this is what Colonel LaSarte said for you, but I know him better, and he did that to reinforce  his superiors' orders. He knows I would never double-cross them without a_ REALLY _serious reason, and they didn't gave me one."_ reinforced Santa _"But, if fact, we had a problem... For some reason, all our external communication links are broken."_

_"I noticed this."_ stated Dumont _"I saw that our comms are somehow compromised also: even my Vernetech devices are offline. Could this be a fluctuation on the energies that hold Christmas Village on Earth?"_

_"No... We tested this for fluctuations with our devices and everything is under standard levels."_ disclosed Santa _"We didn't searched more because we need to ready everything for Christmas Day, as it gets nearer and nearer. I will risk myself if need too... But now I believe you can help us to find what's happening."_

_"So... Looks like this was intentional: something is blockading the communications between Christmas Village and external world, so to speak. Had you tried to find the blockade signal generator?"_ queried Dumont

_"No..."_ assured Santa _"As I said, we have a priority, of readying everything for Christmas. This year things gone even more difficult, with all the new toys, and some of the older Helpers, that worked with the previous Santa  doesn't have the skill or knack to deal with those."_

_"Well..."_ stated Dumont _"I think we'll need to find the generator."_

_"But if this generator is not an external one, as we can suppose, it could only be put here by a mole, so Christmas Village has someone from your enemies inside. In fact... Do you have any enemies?"_ asked Jiminy

_"Well... There's those guys, the so-called Night Blizzards..."_ remembered Santa

_"Night Blizzards?"_ questioned Jiminy

_"Those guys are the worst."_ said someone else. When they looked behind, they saw it was Metsätäja, that came with Bernhardt. He was dressed as an Santa Helper, in dark green and red, a green Santa Hat over his hair, a short toy-looking sword into a sheath on his belt. _"Sorry I didn't said you before, but I'm an habitué here, so I already have my own quarters and clothes and can go almost everywhere. But, about the Night Blizzards: they are the nearer we have of a super-villain team on Lapland. They do lots of attacks against the Sami people and on Laplandish cities either on Finland and on Sweden. People wants to catch them hard as they rob houses and kidnap kids."_

_"And who are they?"_ asked Jiminy

_"They, like Santa, are from the so-called_ Ur-Type, _representations of myths and legends from past that couldn't be placed on other types. They are called_ Christmas Spirits, _like Santa himself and the Santa Helpers and Snowmen and Nutcrackers and so. But they are evil_, really REALLY _evil. The Cold-Hearted are part of the Night Blizzards, their minions so to say. The big guys are called Krampus, The Grinch and The Ice Queen."_

_"Like by the myths?"_ checked Sugarplum

_"Exact."_ assured Santa _"You see: there's dark anywhere there's light. Those guys are the dark here."_

_"And who are those guys."_ asked Scheherazade

_"The Grinch is a guy that build Vernetech that does some nasty job, by replacing some of the toys I deliver for some very nasty toys, the so-called Rowdy Boys Toys: no matter what, with lots of explosives and sharp corners and blades and all kind of nasty stuff to hurt and maim kids."_ rasped Santa, on the most un-Santa-ish behavior they could imagine _"There's at least two breakthroughs that were registered thanks those things, and two girls in coma after a doll attacked them and cut their bellies."_

_"That's horrible."_ shuddered Marjanah

_"And_ he's _the most rational of them. The Krampus is a monster, but he's a smart one: he prays on the naughty kids. Every year he does his best to take at least two or three of them. What he does with them... I can only speculate, and nothing he could do could be good..."_ confirmed Santa _"Some legends says he devours them to the bones, growing bigger and stronger and meaner with them. Others says they turn them into lieutenants, smaller versions of himself, monsters to attack other kids. And there's even worst things that legends says."_

_"Like the Cold Hearted?"_ questioned Jiminy

_"Some say that Krampus gave those kids for The Ice Queen.... Not that she needs: she herself does the dirty job of kidnap kids and, by ripping open their chest and make their hearts grew colder without letting them getting into shock, making their hearts goes frozen in their last heartbeat, so creating the Cold Hearted. She's jealous on me: she thinks that I took her thunder by being a symbol of hope in the dark times of Winter."_ snorted Santa

_"But... How they could put someone here?"_ mused Jiminy _"If I understood, all the Santa Helpers undergoes a kind of breakthrough like yours, is this right, Santa?"_

_"It's right, Jiminy..."_ assured Santa _"They take the Santa Helper Mantle, sign their own Santa Helper Clause when joining our ranks."_

_"But... And their families?"_ inquired Marjanah _"They would never forget their families: they have any contacts?"_

_"Some have, yes, although is common to people here to reduce external contact with their families, for their own safety."_ noted Santa _"We give some vacancy time during the year, except by the Holiday times, so they could visit their families"_

_"Do you screen them?"_ asked Sugarplum _"I believe that you looks for someone under a profile: I can't see a bully becoming a Santa Helper."_

_"To be fair, we do some screen and, although he have some cases of_ former _bullies becoming Santa Helpers, it's not that easy and they really proved themselves redeemed and ashamed from their past. We screen for their competence, personality and behavior."_

Jiminy was thinking.

_"Jiminy..."_ said Blue Fairy on his mind when he shooed her

_"Just a minute ... I'm thinking a little. Trying to use my_ Bellax Analytica."

While they were talking, it was when Jiminy had an idea. 

_"Santa... There's Helpers that deal with some kind of hard maintenance: things like cleaning, energy supply maintenance and so, right?"_ asked Jiminy

_"Yeah, for sure. There's lots of energy we use."_ said Santa

_"And any Helper that recently joined their ranks? With big families outside or very linked with them for some reason? Without other social connections, perhaps?"_ asked Jiminy

_"For sure!"_ said Bernhardt _"Leon! He's the newest Helper and was in the maintenance until some days before. I took a check on him and he has a sister called Amelia. Their parents died just a little before The Event and we discovered him after some investigations we do on potential Helpers: Nice list for at least 5 years, no more than two consecutive years on Naughty, if most, and on a situation where we can help him and him could help us. He just completed a major on Arts and just got out the fosterage house as an adult. But... I can't see him as the mole for the Night Blizzards. He does his job very very good and looks like a good chap."_

_"There's one way to check him..."_ said Jiminy _"But I need to say, I will not like it."_

## Chapter 8

> _"People think I like to use my_ Pretty Please! _at whim, as I can make people does things I need them to do. However, no matter how subtle I do this, this is still a will-bending power, one of the kind of powers that, in the comics, are more associated with villainy than with heroism. And this still make me go queasy."_
>
> ___Jiminy Cricket___, _"A Living Conscience: some musings from a life Wearing the Cape"_

Jiminy and the others got into the big warehouse were the gifts were built: lots and lots of levels with lots and lots of working tables, with all kind of supplies and tools, with lots of Santa Helpers working together to build all kind of toys: the smell of cotton and fabric from dolls and teddies mingled with the smell of plastic and SMD solder paste from those who were building perfect replicas of videogames, _Made In China_ logos and all.

_"This is incredible."_ reckoned Jiminy _"But how can you make those pass as by... You know..."_

_"The, quote-unquote, real thing?"_ non-chalantly said Bernhardt _"We have our ways, and even the biggest industries moguls wouldn't like to be in Santa's Naughty List. But let us focus on what we need to do. Leon is somewhere near."_ said Bernhardt, when they saw a small group of Helpers doing some classic board games like Chess and Trouble. One of them, with a strawberry blonde hair and a little smaller than the others, looked to Bernhardt and the others and did the worst reaction.

He ran.

_"Not that easy, chap."_ said Djanni, flying over the balconies.

However, there was too much toys and things that messed around, and Leon used this and his knowledge of Santa's Workshop to difficult things. He pulled a doll from big pile of dolls and teddies against Djanni, that needed to dodge the toy avalanche. Sugarplum shrunk herself to get away, while the others got away as soon Scheherazade did a small wall of sawdust to hold everything. Dumont engaged his Demoiselle and got nearby Djanni.

_"Where he is?"_ growled Djanni, angry.

_"He knows the place."_ stated Sugarplum, while following the genral direction and looking for Leon _"And here's gigantic! We'll lose ourselves here. We'll need to improvise. Djanni, you go front as far you can. I go left and Dumont go right. We'll circle him by the air. Show Jiminy your Maskcams, so he can focus on_ Bellax Analytica _to find him. Marjanah, if you can help using Astral Plane, it would be great!"_

_"Alright."_ said Dumont, flying as Sugarplum said. _"Jiminy, now is with you: use your_ Bellax Analytica _to make us reduce the circle on him."_

_"Right... Sugarplum, on your right!"_ said Jiminy, when he saw Leon on Sugarplum's Maskcam

_"Okay!"_ confirmed Sugarplum, getting nearby, which made Leon run away on Dumont's direction.

They were doing this, working and closing the circle on Leon, until he was cornered. He looked around, fearfully, when Bernhardt hold his arm, while Jiminy and the others were getting nearby.

_"Wait, Leon... We need to talk_ for real!" he said seriously

_"I don't know what you want, Bernhardt. I didn't did anything wrong!"_ panicked Leon

_"Didn't you? Maybe you're a mole for The Ice Queen and this is why we are without comms with external world."_ accused Bernhardt

_"No chance! I would never betray Santa!"_ denied Leon, panicking

_"He's not telling the truth, at least not completely."_ blurted Marjanah via Astral plane _"I can feel his lie on the Astral Plane."_

_"Let me see this Bernhardt. You don't need to play that harsh with him."_ affirmed Jiminy

_"Okay."_ said Bernhardt _"But if Leon try any funny thing, I'll deal with him."_

Jiminy looked to Leon.

_"So... You really would never betray Santa?_ Pretty Please!, _don't lie. You're one of the most recent Santa Helpers at all, and you're the only one that could do this, as part of the maintenance team..."_ said Jiminy, pushing into Leon his _Pretty Please!_ power.

He could see him trying to avoid the compulsion to say the truth, his face writhing in pained faces. Jiminy never saw anyone resist so much his power, even Marjanah and Djanni under the _Sword Verse_, until he couldn't resist anymore and snapped:

_"Sorry, Bernhardt! I did it! It was my fault! They knew about my sister! The Ice Queen blackmailed me! If I didn't did that, they would catch my sister and turn her into a Cold Hearted! I could not let this happen!"_ he wailed, crying, his will almost broken, which made Jiminy goes queasy: he still felt this way when he noticed how powerful his _Pretty Please!_ is.

_"Are you_ insane? _Working for the Ice Queen_ and don't talking about this with us? _If SANTA protocol is not engaged, Santa can be declared a tango on Christmas Day and could be attacked by jets and other capes! What you had in mind?"_ raged Bernhardt. _"You know I'll need to take this to Santa..."_

"Pretty Please!, _Calm down, Bernhardt... You bully Leon doesn't help us."_ said Jiminy, making Bernhardt go calmer.

_"I'll see, but there's a great chance to take this to Santa."_ retorted Bernhardt, when Blue Fairy got into Jiminy line of sight, still like a Cindy Lou.

_"Jiminy, found some intel about this Amelia and know where she is: I don't know why, but my Quantum Link can counter whatever is doing this electronic blockage. Amelia McNish, age 15, living on Saint Paris Orphanage, student at Martin Luther King High School at Chicago, just have Leon as family and vice-versa. What we should do?"_ said Blue Fairy _"I hacked some school cameras and looks like there's some Cold Hearted hidden around nearby, shadowing her, ready to catch her if this thing fumbles."_

_"Could you put Shell and/or the Chicago Sentinels on this? No need to talk about the mission: say you had some intel on creepy guys stalking a girl. Just enough to make things goes coincidental. If possible, talk them to take her to The Dome for some days, until things cool down."_ queried Jiminy _"If they ask details, make sure to reveal just what they need to know."_

_"Roger: I'll talk with them ASAP."_ said Blue Fairy, disappearing with a POOF.

_"Leon,"_ said Jiminy _"I think I can help you, but we need to know what happened: how they discovered about you, and about your sister, and what they talked with you. Also, we need to know all you know about this thing."_

Leon, still shaken, looked to Jiminy: _"Pinky Promise?"_

_"Pinky Promise. Cross my heart and hope to die."_ assured Jiminy, gesturing on his own chest _"Now... How they discovered about you?"_

_"I was contacted by some Santa Helpers at February in Chicago, as soon I finished my arts grad. They talked me about Santa and so, and about my name being in the Nice list almost all my life. They gave me a ticket for a flight to Helsinki and from there to Rovaniemi, and then there I got here, and they talked about what I would need to do. I believe that it was in Rovaniemi the Ice Queen noticed me: I was so naïve and full of myself, after having said in the orphanage I would be always a hamburger flipper, talking in the hotel and restaurant about Christmas Village..."_ mumbled Leon

_"Okay..."_ said Bernhardt, calmer now _"You were really naïve, but it wasn't your fault, at least not totally. The recruiters should had said you to stay low profile and not say a thing about Christmas Village, even this being an open secret on Rovaniemi. And it was good: we now know there's some informants in Rovaniemi for the Night Blizzards. And..."_

_"Well, after I signed the Clause, I was sent to Rovaniemi for bring some supplies till here, and I was blasted away from the sleigh by a wind while going there."_ said Leon _"It was when I was took to The Ice Queen's Castle, and she said that she knew about my sister."_

_"Seems legit: if we have our information sources, they have theirs."_ concluded Bernhardt _"And?"_

_"She demanded me to place that box into somewhere. She didn't said what they would do, and said me to not open it. She suggested me to put it nearby the energy generators, as that thing would need some energy."_ said Leon

_"And you heard her? And didn't talk about this with_ us?" menaced Bernhardt, which made Leon cower.

"Pretty Please!, _Bernhardt, calm down. Leon was coerced. Stress out him even more will bring no good."_ said Jiminy, using his _Pretty Please!_ to make Bernhardt go calmer.

_"Okay, Jiminy, but I'll send this to Santa."_ retorted Bernhardt.

_"I heard enough, Bernhardt."_ said Santa, getting nearby _"And I agree with Jiminy: even being Leon's fault, stressing him out will bring us no good. Now, Leon... What was that thing, and where you put it. Trust me: if something happens with your sister, I'll take this personally."_

_"Jiminy, Astra and Shell discovered those Cold Hearted and dealt with them easily, with Grendel and Megaton's help. They got that girl and put her securely on The Dome."_ beamed Blue Fairy _"Looks like they coincidentally were nearby that school as we were talking and saw those creepy guys and dealt with him."_

_"Great!"_ bursted Jiminy, when he explained as they looked to him like he was crazy _"I passed the intel about your sister to Chicago Sentinels and they dealt with the Cold Hearted. I have my ways, for sure."_ said Jiminy, on a tone that came to Santa and Bernhardt as a way to show he passed the intel via Blue Fairy. _"They took her to the Dome, and almost no one can get into there without they know. She's safe by now."_

Leon was almost crying.

_"Calm down, Leon: you did a big mistake... But everything will be end well."_ assured Santa _"Now... After she gave you the box, how she made you get back?"_

_"She send me back to Rovaniemi and I lied about find my way back there to the others. I hid the box into some supplies I would bring to the warehouses and put it nearby. And just connected the power source: I didn't knew anything on what was that or what that did, I still don't know what that thing is. It was two weeks ago."_ said Leon

_"And_ exactly _since then, we had our communications with the world outside blockaded."_ stated Santa, and Leon gasped, dawned on what he did _"I think you understand you brought us a lot of trouble. But I understood you took a bad decision based on a good motive, so no punishment will be given. However, now we need you to undo the ill you did, or at least help us."_

_"I'll show you were I deployed that thing."_ said Leon, meekly _"I'm really sorry."_

_"No time to be sorry."_ retorted Bernhardt _"We need to undo this thing, so we can engage the SANTA protocol and avoid Santa to be declared a Tango when crossing the sky at the Christmas Eve."_

And they got to the energy center.

## Chapter 9

> _"Okay... So you believe that Verne heroes live the glamour of building things all the time. Well... There's times were people think on Verne like a kind of Hyper SWAT team."_
>
> ___Andre Macedo Luiz___, _"Histories from the_ Demoiselle - _My Breakthrough Memoirs"_

They got down an elevator to a place many meters down the main building structure, were they saw a kind of special boiler for energy generation, red with some green fake wreaths around.

_"This boiler provides all energy for Christmas Village."_ explained Bernhardt _"For redundancy, there's a small nuclear generator on another place, but it's a link we use only in emergencies or programmed maintenance windows."_

_"Well..."_ inquired Dumont _"Looks like a perpetual motion design, right? It's Vernetech for sure, but it would demand an Omega to do this. Or at least an army of powerful Vernes with more or less the same paradigm."_

_"Neither us can violate 3rd Thermodynamics Law."_ said Santa, while they were walking through all the infrastructure _"What this does is to provide almost all energy we could generate using this machine. Maximum Potency efficiency on 9 digits, for 99.9999999%, with very small carbon footprint and without radiation. This use di-deuterium and di-tritium oxides to provide maximum energy efficiency. This is good because here in Arctic there's lots of those oxides, the so called heavy and super heavy water. We are even studying Helium-3 and some hyper-rare Vernetech-produced isotopes, like quatrium, Hydrogen with 4 electrons and He-4, for other energy supply sources"_ 

_"Nice idea."_ marveled Dumont, taking his small notebook and Montblanc pen for some sketching.

Leon got on somewhere behind the main boiler.

_"Here, below the boiler."_ said Leon, pointing the place where he put it. 

_"It's very thin."_ said Dumont, looking down the boiler _"Sugarplum, can you go Tink and push it outside?"_

_"Hope there's not too hot there."_ she said, removing her shawl and shrinking Tinkerbell size.

She flew and started to push

_"This thing is heavy... And I need to push it with care: this is made of brass, so it is very hot. I'm using some magic to cool this thing, but it will be difficult."_ she said, panting, via Earbug

_"OK..."_ muttered Dumont, taking some leather gloves and an apron from the utility belt he bring with him _"Push it until I can take it. You just need a little more."_ 

She finished to push and got off. Jiminy passed her a bottle of cold water as soon she grew back.

_"It was hellish hot there. I almost fainted by heat."_ she panted, while drinking a little of water and throwing a little over her head.

_"Okay... Let us see..."_ looked Dumont, taking a small set of tools from his utility belt and opening it in the floor. _"This looks like Vernetech... But has to work on some physical base, otherwise it would not do its job."_ he said, while mounting a screwdriver to unbolt the box.

Jiminy was looking to it when he felt something on his _Bellax Analytica_:

_"Dumont, DON'T..."_ he shouted, but it was too late.

Dumont finished to unbolt the screws and open the box lid.

And a very audible CLICK could be heard. And smaller _clicks_ on a rhythm.

_"It's a trap!"_ cursed Dumont _"I was so naïve. If it was really that Grinch guy that made this, he would have some counterfeature to avoid it to be shut down and/or to give some punishment for those who would try."_

_"Can you disarm this?"_ worried Sugarplum

_"Maybe..."_ hurried Dumont _"No countdown equals no idea on how near we are from a BOOM. I can see some explosive bars, like C4 but certainly Vernetech, so bad news... And a control circuit. But have no notion on how disarm this. Jiminy, I'll need you here."_ he said, trying to maintain his cool.

Jiminy got nearby

_"Now, I need you to look as far you can on your_ Bellax Analytica... _If, at anytime, you see it would be better to take a plan B, we'll run away and do the best, right?"_ asked Dumont

_"Right. Let's go...."_ said Jiminy

Dumont started to look for places and take some cables here and there to shutdown the bomb... 

It was when...

_"Stop, Dumont!"_ shouted Jiminy _"This is impossible to be disarmed: it will explode in some seconds!"_

_"Can I fly this out?"_ asked Djanni

_"I think so, but you'll have at most twenty seconds..."_ Jiminy was saying, when Djanni took the bomb and flown away with it as fast as he could.

_"Djanni, NO!"_ cried Marjanah.

Djanni flew over the elevator tube and got out the warehouses, running through the windows and thrown the bomb away.

It was enough, but barely.

The big explosion could be heard everywhere at Christmas Village. All the windows were blasted by the shockwave, that thrown Djanni back through a wall, blasting it and throwing him into a big pile of toys. Santa, Bernhardt, Leon and the others gone for where Djanni crashed.

_"Djanni!"_ shouted Sugarplum _"Are you okay?"_

_"Not... Too... Much... Hurts a lot!"_ moaned Djanni, while they could see Djanni's legs on weird angles, like a weird ragdoll thrown away by a kid into a tantrum.

_"It was a hell of explosion. It could had blasted Christmas Village out of map!"_ dawned Jiminy

_"Leon..."_ chastised Bernhardt

_"I didn't know about that, I swear!"_ meeked Leon, almost crying again

_"He's saying the truth."_ said Santa _"This was one example of The Grinch Rowdy Boys Toys."_

_"That was terrible!"_ gasped Scheherazade _"How is Djanni?"_

_"Not that good..."_ pondered Sugarplum, checking him while some Helpers brought a stretcher and all EMT utensils and material they had at hand _"He'll live, for sure, but he had a big concussion and broken legs... He'll recover fast, but he has no fight condition by now..."_

_"Advanced Team, this is ORSL 1..."_ said something in Dumont's pocket with Soldaire's voice

_"ORSL 1"_ said Dumont, after taking a brass made walkie-talkie like thing _"This is AT. Dumont speaking..."_

_"Wow, what a scare you gave us... What happened?"_

_"Santa had been electronically blockaged. When we tried to deactivate the device, a bomb had been activated. Djanni thrown it away and we avoid the worst scenario, but he is really hurt by now. He'll go to the infirmary here."_ related Dumont

_"Things are growing risky..."_ speculated Soldaire _"This is growing compromising... Abort now..."_

_"No way, Soldaire!"_ shouted Jiminy _"We had made this far, we need to finish it. There's lots in game!"_

_"It's an order!"_ stated Soldaire _"It's too risky for you..."_

Jiminy felt something again tingling on him

_"No need to think on this, Soldaire. Things had_ already gone Foxtrot... _There's tangos coming!"_ shuddered Jiminy

And the alarm trumpets were called.

## Chapter 10

> _"When we fought at [REDACTED], we thought it would be an once-and-for-all show-off. How wrong we were."_
>
> _From Joshua "Jiminy Cricket" McCarthy's memoirs_

They could see people  readying themselves at Santa's Village: almost everyone stopped to build the toys and gifts and took weapons or evacuated for safe rooms.

_"Not now, Leon!"_ growled Bernhardt, when he saw Leon taking some of the weapons that were being given in the Armory _"You did enough. When we disarmed the tamper, the Night Blizzards discovered. Now they are trying to attack us."_

_"I know you don't trust me... In fact, neither I would trust myself now. But I want to redeem myself. This was all my fault."_ confessed Leon

_"Sure it is!"_ growled Bernhardt, taking from a rack a blunderbuss and a sword that, albeit looking a toy weapon, had a very real silver blade.

_"Enough, Bernhardt."_ talked Santa, getting nearby _"He had enough, and he understood that he done a really big mistake. No need to stay on this stance. You're too near from bullying him for no good reason. Stop now."_

Bernhardt looked angrily to Santa, but then he felt ashamed...

_"Sorry, Santa, but..."_

_"I know, Bernhardt, I know..."_ began Santa _"Since from before The Event, you are the Head Elf, the one that is responsible for Christmas Village when I'm, or better_, Santa _is out. However, you need to remember that lots of those Santa Helpers are not as experienced as you: they, like me, have at most 17 years, give or take, as they are now. And Leon has just some months as Helper and had just his sister as family outside, it was a too big of an ordeal for him. He failed it and with this put us all on a big danger, but he has the right heart. I know that Highway to Hell is paved on good intentions, but we have no time for fight each other."_

_"Alright..."_ affirmed Bernhardt _"Leon, you and some other elves go for the Safe Room and protect Santa."_

_"Roger."_ confirmed Leon _"Looks like this will be a big fight..."_

_"In fact..."_ said Marjanah _"I think there'll be fight enough soon: they are coming. Jiminy... How much of them?"_

_"I can't say for sure, but the numbers are more or less even. However, they looks like more experienced and ruthless fighters."_ gulped Jiminy, while looking to the horizon, seeing the enemies coming

_"So..."_ mused Dumont, looking for his guns _"They are going all-in. And we are without our main brick, as Djanni is on the infirmary"_

_"Looks weird, like they planned this all..."_ guessed Sugarplum, when the enemies came

And the fight started: Sugarplum and Dumont were the main attackers, as Djanni was in the bench. Marjanah and Jiminy helped by coordinate actions with everyone else, while Scheherazade used her ice storms (like the sand storms she could do in the desert) to barricade them, reduce their numbers and split their troops so the others could flank them with the Helpers.

_"Damn it!"_ cursed Dumont shooting two more Cold Hearted _"They just come and come!"_

_"We need to hold them: Bernhardt is evacuating Santa to the safe room..."_ shouted Jiminy, when again his _Bellax Analytica_ tingled. _"Things are growing even more Foxtrot! The Grinch and Krampus are here!"_ 

_"Oh, crap!"_ whined Sugarplum.

_"What we can do now?"_ asked Scheherazade _"They are powerful! And we already hands full with those hordes of Cold Hearted!"_

Jiminy thought and then stated:

_"Dumont, you and Sugarplum deal with The Grinch. Marjanah, you, Scheherazade and me on Krampus!"_ addressed Jiminy

_"Are you crazy, Jiminy?"_ shouted Sugarplum, while fire-blasting another Cold Hearted. _"He will turn you into mincemeat!"_

_"This is the best way to deal with them, or at least what_ Bellax Analytica _is showing me."_ shrugged Jiminy

_"Alright,"_ sighed Dumont _"But I believe that someday your_ Bellax Analytica _will kill us all."_ he said, giggling, going for the green weird monster it was The Grinch...

---

... leaving Marjanah, Scheherazade and Jiminy to The Krampus, the big dark monster they could see.

But even before they saw, they could _smell_ him...

_"Yuck, he stinks!"_ grunted Jiminy.

_"Indeed, he smells like Jahannam's brimstone and fire."_ admitted Marjanah

_"What we do?"_ asked Scheherazade.

_"Be ready, but no matter what, don't attack first... Hope the legends are truth."_ answered Jiminy

_"Legends?"_ questioned Marjanah

_"Forgot about your muslim background, but even on christian, Santa believing places, Krampus legends are a little less known. But we have not too much time to talk now. He just came."_ pointed out Jiminy, when the smell grew strong enough to almost make them go sick.

It was like rotten food and sewer and unclean bodies that rolled in the floor of a pigstill, all mixed. Krampus also was holding a big, old, filthy sack. They could see her bloodshot red eyes, that looked more like a goat or wolf ones than a human being's. His hands were big and somewhat clawed like a wolf, while his feet was hoof-cloven like a goat. His pelt was of a kind of dirty dark gray, like it was never washed.

_"So..."_ said Krampus, on a weird small rumble that looked like a distant thunder. _"The big ones sent kids to fight me."_

_"They didn't sent us: we've volunteered to."_ shouted Jiminy, on a humble but defiantly tone.

_"Three kids!"_ he boasted, laughing like a rumbling volcano. _"Three kids! This is what they have for me?"_

_"Yeah."_ declared Jiminy, on the same humbly defiant tone _"Three_ Nice _kids!"_

Jiminy's affirmation made Krampus stop his bravado and look shocked.

_"Yeah, I know about you, Krampus!"_ said nonchalantly Jiminy _"Read enough about you before even becoming a cape: you don't touch those who are at Santa's_ Nice List, _and_ you know _whose that are."_

_"Are you sure about this, kiddo? If you're wrong, I could just slash you three with my claws like a sword slashes a piece of paper."_ provoked Krampus, showing his claws, almost touching Jiminy's face, which made either Marjanah and Scheherazade tense themselves

_"No, Marjanah, Scheherazade: he is provoking you to attack him without a fair reason!"_ said Jiminy _"This will take you from_ Nice List, _and this will make you fair sport for him."_

Krampus grunted.

_"Sounds like I said the truth, right? You can't attack us by now: we are in_ Nice List. _So we are out-of-bounds for you."_ said Jiminy, hands akimbo.

It was not that easy, though: on his mind futures and more futures raced each other, second after second. Only his training and experience avoided him to get into fetal stance, rolling and screaming. He was focusing himself on what to do to avoid see the potential futures where he and the girls were being gruesome killed.

_"You're right."_ sighed Krampus _"Maybe I should try my best against those two girls..."_

_"... but if you do this,"_ tutted Jiminy _"you give us free pass to attack you without going out the_ Nice List, _and you don't want to do this... This would be counterproductive for you."_

Jiminy looked around and saw Bernhardt armed with his bow putting Krampus under his target.

_"No, Bernhardt... I want to do this without shed blood."_ warned Jiminy

_"It's_ freaking _Krampus!"_ shouted Bernhardt _"If he attack you, he could kill you like if you're a duckling."_

_"I know..."_ stated Jiminy, fearfully _"And is_ just because this _I don't want a fight."_

Bernhardt looked to Krampus, and it was when he noticed.

Krampus was _thinking_.

Bernhardt downed his bow.

_"Why you think you could make me let you go without shed blood, you knowing I_ crave _for blood?"_ roared Krampus, breathing the smelly breath against Jiminy

_"Because"_ exposed Jiminy, avoiding to cough after breathing that stinky breath _"I know you're_ cunning: _you don't want to be hurt or risk being killed. And you want to be overlooked as the left hand of Santa, the one that punishes the Naughty kids. And I can see you don't like the other Blizzards."_

_"You're right."_ nodded Krampus, giving a weird kind of smile: ugly, weird, but truth. _"You're clever. Those Blizzards are a bunch of morons. If it wasn't the meat, I would just shove off those two."_

_"Meat?"_ asked Jiminy, almost knowning the answer

_"You see: I crave for flesh and blood._ Young _flesh and blood: Naughty kids are like venison for me. They are delicacies that sometimes is so short and difficult to find..."_ announced Krampus, a thick, black, gooey tongue licking his lips on a devious way

_"So... They provide you with this... delicacy."_ cringed Jiminy, trying to maintain his pose under all that pressure

_"Right. Those sweet morsels are my bread and wine."_ answered Krampus

_"But the Blizzards use you as muscle against Santa, and the Helpers are not so tasty, right? They somewhat bought you as muscle and pay you on this... Delicacy."_ stated Jiminy, trying to be as much nonchalantly he could

_"Yeah... And they are so haughty and mighty, but in the end I'm the one that do the heavy job."_ hissed Krampus

_"Well..."_ smiled Jiminy _"I don't want to be eaten... And you can't eat us as we are in_ Nice List. _If you do, you're fair sport now and forever for Santa and the Helpers. And you want to have your pelt. We are under a standoff... But... If you just go away, I assure you I'll not go against you, and you'll have safe way away from here: no one will try to get you, at least by now."_

_"Do you think you can negotiate your way out?"_ roared Krampus

_"Well... Bernhardt is with that bow, ready to shoot those silver magical arrows on his quiver. He also have that Vernetech blunderbuss, and my friends here Marjanah and Scheherazade could knock you out and even kill you as soon you bite myself. Well, when I think I would die before this, it's small consolation, but at least it would be a payback you would have for my blood. If you are so crazy to take me, go on, but if I was you, I would be patient and hope I put myself on Naughty List, so I would be fair sport for you. And you can only do this by staying alive, which you'll not if you try to attack me."_ smirked Jiminy

Jiminy knew this would be a nasty gambit: last time he did something like this, he unintentionally killed a bad guy, and this time he could kill himself.

_"Alright!"_ rasped Krampus _"But remember, Jiminy Cricket: if you just put a finger_, a tad small finger, _out of the_ Nice _List, I'll do my best to find you and crave my teeth under you, to taste your juicy, sweet meat."_ he finished, while turning his back, showing his filthier, stinker back for them

_"If you can..."_ murmured Jiminy Cricket, while the big monster started to get away, disappearing on a mist made of snow.

The others looked to Jiminy, that was static:

_"Are you_ insane?" retorted Bernhardt _"Trying to_ negotiate _with_ Krampus? _You're lucky to get away with this alive, even luckier he agreed with you. He's a monster, a_ goddamned _child devourer! And with all that, you stayed that cold facing him that nonchalantly naïve?"_

There was not too much Jiminy could answer Bernhardt. He thrown up, the adrenaline rush going down and bringing him back to reality, shaking.

_"Jiminy!"_ ran Marjanah for him, holding him while he puked out. Scheherazade and Bernhardt got nearby.

_"I tried my best to hold my composture..."_ said Jiminy, weak, and barfed out a little more before continue _"You see, Bernhardt: Krampus is_ cunning. _From the way he talk, act, and move, all he does is_ intimidate. _It was made to make us run or fight him. Either way he would remove us from the_ Nice _List. Since the beginning, a fight was a no-no."_

_"And why you said Dumont and Sugarplum to leave Krampus to us?"_ asked Scheherazade _"We almost did nothing."_

_"We needed to stand up against him: not play under his rules."_ said Jiminy, cleaning the rest of vomit from his mouth with a handkerchief _"By stand up, we reinforced his own rules: he would_ never, EVER, _try to eat a kid from the_ Nice _List. He can do this at whim, of course, but by doing this he would be putting himself as a fair target under Santa and any good people. And he is, in the end, a bully: clever, cunning, but a bully. And trying to survive against bullies without getting into their game was my life until I undergone my Breakthrough and even after."_

_"Are you okay?"_ said Bernhardt

_"Just weak and shaken: it was risky, really risky. Neither when I fought Alsyf Alayat or The Ascendant I was under so big a risk."_ said Jiminy _"It was really crazy, but better than send Sugarplum and Dumont to fight him: they would be dead by now."_

It was when they heard some screams from the Christmas Village, and an explosion, and a chackling glee over the Northern Lights...

---

Meanwhile, Sugarplum and Dumont got the other way to engage The Grinch:

_"Hope Jiminy knows what he was doing... Challenge_ freaking _Krampus is a big stuff. If he fails, he'll be turned into sushi very fast, and Marjanah and Scheherazade with him."_ talked Sugarplum, while they avoided some shots from a group of Howdy Boys Toy Soldiers, part of the Grinch's Howdy Boys Toys.

_"He'll do..."_ assured Dumont, shooting a wave of aluminum needles from his Tesla Barrage against them _"We need to stop patronize him as a kid: he's still the Disney lover we knew, and still has the same nice heart, but he's now, more than ever, an_ Herós Sans Frontières _cape. He can deal with the challenges that life put for him, even this being Krampus. Remember his_ Bellax Analytica: _he would never go that way if the odds were not with him."_

_"Hope so... Now, let us focus on our tango. I'll go Tink: try to make him focus on you... I'll try to do something to disable him soon: by what Santa said us about him, I think he has some explosives with him, and if so, I'll try to launch him to the sky with them."_ detailed Sugarplum

_"Roger."_ confirmed Dumont, while he saw Sugarplum going Tink, shrinking to a Tinkerbell like size: she was lighter and smaller, so a strong wind could blast here, but she was also faster and harder to see, which covered the defense.

They flown to the green furry little monster, with a bandoleer and an utility belt with two pistol holsters, from where he took two old-looking pistols, as pirate pistols, and shoot a kind of laser, making Dumont goes and take evasive maneuvers. He also gave a shot to where Sugarplum had gone, making her doing evasive maneuvers and growing back.

_"Yeah... Two goody two-shoes to be a prize for me! This will be so fun!"_ he gleed maniacally, taking what looked like a firecracker from a belt and throwing it in the air.

_"Sugarplum, your eyes!"_ shouted Dumont over the Earbug.

It was by just, but they avoided the Rowdy Boys Firecracker's effect: they could, even with closed eyes, notice some of the bright, flashbang-like, light. But this gave for Grinch a chance to shoot Dumont straight in the arm.

_"Dumont!"_ screamed Sugarplum, when Dumont fell in the snow, the Demoiselle straps bursted by the laser shot from Grinch, that cackled maniacally when saw the Brazilian Dandy Caroler falling in the snow.

_"You big son of a bitch!"_ snarled Sugarplum, taking her fairy wand "Debruana, máthair na gaoithe, caith mo naimhde as dom!" she shouted, shooting a powerful blast of air from his wand, pushing Grinch through.

She took the time she gained to look for Dumont: he looked unconscious, and his Demoiselle was some meters over them.

_"He's alive... That's good news. But I'll need to finish that green ball of shit fast: we need to look Dumont for a concussion, and I need to take him out of the snow to avoid a frostbite."_ she thought, when she saw Grinch trying to tackle her, cackling while using a kind of steampunk firecracker on his back, like a weird jet-pack.

Sugarplum shrunk to avoid the attack, and Grinch had gone up, positioning himself on the same height Dumont previously where.

_"Do you think you have power enough to win me, your moronic little Tinkerbell? NO CHANCE! I'll break each bone of you and put you to dance on red-hot iron ballet shoes, until you will pray for people say_ 'I don't believe in fairies', _so you can die fast!"_ he said, gleeful.

Sugarplum took her wand and whispered "Badb, mná na claidheamh, cuir do bheannacht orm chun olc a chosc."

It was when she flew still on her Tinkerbell mode.

_"Your moron! Do you think you can avoid my attacks being small? Take that!"_ he cackled, shooting plasma from a rifle.

But Sugarplum just evade the attack and grew back on his back, her wand now turned into a kind of magical lightsaber. 

She cut throught the firecracker, making Grinch scream when it explode in white hot flames.

_"I noticed your firecracker/jetpack thing is basically a white phosphorus made propeller. As soon I opened it, it just got into your skin like napalm. Who will want to be disbelieved now, you freaking bastard?"_ said Sugarplum, when he fell in the snow.

_"Lucky for you I'm not like your lot, but believe me how much I would love to toast you to a crisp if you give me a chance."_ said Sugarplum, when she looked to the Grinch, that looked now a small, pitiful green thing, full of fear and pain. _"Now, if you want to save your carcass, leave Lapland NOW. And if I know that you gave another of those goddamned things for a kid and that made them hurt, no God, or Gods, or any Superior Force will stop me to find you and grind your freaking green face into a grinder until it became two pounds of mincemeat_, capiche ? _So, get out of my way, if you prize you pitiful skin!"_

The Grinch ran away, when Sugarplum got down and looked for Dumont, that started to wake up.

_"Let me see how you are."_ said Sugarplum, doing some magics to heal Dumont a little. Her healing magic still needed some work, but were good enough for first aid.

_"Ouch... Looks like that Krampus guy hit me hard. I'm feeling pain all around myself!"_ whined Dumont

_"Bad news: you're bruised all around your body. Good news: the snow had absorbed a lot of the worst from the fall. Let me help you to get up and I'll shut the_ Demoiselle _down so you can recover it for repair later."_ she assured, giving a hand for Dumont, that took it. 

When she flew and shutdown the Demoiselle, as soon it fell in the snow, they heard some screams from the Christmas Village, and an explosion, and a chackling glee all below the Northern Lights...

---

They ran back Christmas Village, when they saw the Cold Hearted, the Rowdy Boys Toys and all the tangos getting back.

_"What was that?"_ asked Jiminy

_"Don't know... And doesn't look good!"_ said Dumont, that MacGyvered a repair on the Demoiselle so he could fly back the Christmas Village.

They could see a big hole in the ceiling of the Christmas Village's Main Building, lots of Helpers containing fire and saving toys, dolls and everything that they could.

_"Could it be..."_ worried Sugarplum

_"No, not Leon..."_ assured Jiminy _"He was not under a post-hypnotic trance: I would detect it."_

In fact, they ran to a place where a big brass door was blasted to smithereens. Bernhardt looked down, white as a sheet of paper, looking to the ground: in the floor there was Leon, his blood splat all around the room, being treated for a multitude of wounds by Saleh and Melissa, a big cut through his chest. Djanni was nearby, with a worried and angry face.

_"How he is?"_ asked Sugarplum _"Anything we can help?"_

_"Critical: it was the Ice Queen. I can have all hands and wands on this."_ said Melissa, trying to focus herself on his fallen friend, not looking for Sugarplum, that came to help _"We are doing our best, but I can't say if he'll pass through it. She slashed his guts like it was a piece of paper: the only good news is that her Cold Hearted magics are ineffective on us Helpers, as we are immune to some kinds of bad magic. Bad news: I could not put him on anesthesia... Only local ones."_

_"That attack was a diversion, a ruse."_ dawned Bernhardt _"Santa! It was her objective since the beginning!"_

_"Yup. As soon she blasted here, almost all the Helpers with us had been knocked out," reported Leon in pain, crying and wimping while in the floor, all the Helpers and Sugarplum over him doing his best to hold him stable "only me on fighting condition. I ran against her, trying to do something, and she slashed me. Before I faked unconsciousness, I could see her taking Santa. This is no good: she wanted Santa alive, and I can't see any good reason for that."_

_"Take a rest, Leon. I was wrong about you... You did your best. Now recover yourself and_ don't die." said Bernhardt, while Melissa finished the first aid for Leon and put him under an anesthesia, helped by a sleep magic done by Sugarplum.

_"I could not foresee this ruse!"_ gasped Jiminy _"I don't believe my_ Bellax Analytica _failed!"_

_"It was not your fault, Jiminy."_ assured Djanni _"No one could foresee this: she put so much people and variables on this thing that only an Ultra, or even an Omega, Jiminy Cricket_ maybe _would be powerful enought to foresse this. And even if you had saw this, it would be too late: I was just starting to recover from the explosion and I heard this attack. I tried to get here, although could not run thanks the pain, and I just saw the Ice Queen getting away: I couldn't fly and try to shadow her, even more seeing Leon all mauled like a chicken."_

_"It would be a bad idea, anyway."_ said Bernhardt, while Melissa and Saleh put Leon on a stretcher to be sent to infirmary _"Fighting the Ice Queen alone would be foolish, not to say suicidal: she's strong and fierce and mean."_

_"How are you now, Djanni?"_ asked Sugarplum 

_"Still hurts, still limping, and I couldn't exert all my power, but otherwise okay. And you?"_ said Djanni

_"This crazy kid_ negotiated _his way out a fight with Krampus!"_ exasperated Bernhardt, with a meek smile. _"I would never thought on this one: I would just blast him with my laser blunderbuss as much as I could."_

_"This would be dead: he was strong enough to be a problem to everyone here, maybe except Djanni."_ retorted Jiminy _"He is really strong and dangerous. I never felt so much fear than when looking him, and_ I had _fight Alsyf Alayat and had been under that Kwazani guy and under the Caliphate..._ Without my powers."

_"Dumont had a big crash, but he's not too bad. We should avoid push him, though: his_ Demoiselle _was damaged by The Grinch and is not 100% operational."_ said Sugarplum _"I'm okay, just pissed off enough to kick that hellish cold Madonna's butt very, very hard."_

_"I'll be okay, just can't engage big fights except on the direst necessity."_ said Dumont, massaging his arms and legs _"I'm sore all around, and this is a bore."_

_"She left this behind!"_ shout Bernhardt, with a parchment, written with a flamboyant calligraphy on a silvery ink. "'Santa is mine now! Surrender yourselves and become my thralls! There's no hope for you! Winter is mine!'"

_"She wants to do something really bad with Santa!"_ dawned Jiminy. _"And killing will not be enough."_

_"She wants... The power of the Santa Clause!"_ dawned Bernhardt _"She wants to destroy the Santa Mantle and absorb its power!!"_

## Chapter 11

> _"If you think that all fights 'for the world's fate' are public, like in the old TV shows, you're wrong, just like that, sorry to disbelieve you. In fact, the real fights for world's fate are rough and dirty, done in the backstage, without public disclosure or witnessesses. Laws, sausages, fights for world's fate: you should never watch any of them being made."_
>
> _From Joshua "Jiminy Cricket" McCarthy's memoirs_

Morale was really low at Christmas Village that time: Santa was kidnapped by the Ice Queen, Djanni and Dumont were hurt enough to be not either 75%, even more 100%, Leon was on another surgery, Bernhardt was pushing himself, trying to make people finish the first response on all the chaos, and Jiminy and the others were trying to trace a plan, counting with Bernhardt, Metsätäja and all available Helpers.

_"Okay... What we know about Ice Queen?"_ asked Jiminy

_"One: she's a bitch."_ spat Sugarplum

_"Language, miss."_ retorted Bernhardt _"Here is a Christmas symbol for good children and good behavior, not a place for pottymouths."_

Jiminy almost did one of the childish giggle he does time after time, but he looked and saw that Sugarplum would turn him into a mouse if he tried.

_"Okay, and..."_ stayed Jiminy

_"Technically, she's a Christmas Spirit with some Merlin on it. B-Class, almost A-Class, on both. She's as a whole an Ultra, almost Omega."_ said Metsätäja, that was cleaning his sword.

_"I found some Intel into TA Future Files."_ said Blue Fairy, that was now under a Santa Helper suit, holding a clipboard, looking everywhere, "downloaded" in the Snowball Melissa gave them. She gave some taps in the clipboard and a small holographic screen shown over the Snowball.

_"Her real name is Agnetha Förkylning, 25 and, sorry about this Bernhardt, Sugarplum is right: she is, was and will ever be a bitch. She used his position as heiress of one of Sweden richest families to bully everyone around her. It was when some girls from her college thought she needed some 'humility lesson' during her debutante ball, and contracted some thugs to do the dirty job. Things had fast gone south and they killed five people including Agnetha's parents, when they took and raped her while revealing all the plan, and this was what triggered her breakthrough. Those guys became her first Cold Hearted... Ugh! It was really gross, graphic, and totally R-rated."_ she shunned. 

_"So, she was a victim that become the oppressor."_ assured Bernhardt _"Nothing new in the sun."_

_"But there's more: she took the girls, killed their parents_ in very gruesome and graphic ways in front of them, _before turn each of them into Cold Hearted. While running from authorities, she came to Lapland and found an Ice Circlet that turned her into the Ice Queen, beautiful beyond human comprehension and unforgiving like Lapland's blizzards. The only thing between her and world domination was Santa, as a Spirit of Cheerfulness and Hope even under a so unforgiving weather like Lapland that was anathema for her, his_ Ho! Ho! Ho! _a challenge she could not stand for."_

_"And so, she created the Night Blizzards as meat-shield for her plans."_ stated Bernhardt _"Very clever... Callous, inhuman, but clever..."_

_"If what you've said is truth, Bernhardt,"_ started Sugarplum _"he's alive, but will not be for too long. Tomorrow is December 21st, Yule, Winter Solstice. If she wants to do something, she'll need to do it till them. Otherwise, the magic will not work, as far I can see."_

_"There's not too much to do, so."_ stated Jiminy _"We need to go and invade Ice Queen Castle and rescue Santa. And, to be fair, we should do it literally kicking her door."_

_"This is crazy!"_ exasperated Bernhardt _"Go and invade Ice Queen Castle_ going front _is_ full retard."

_"And_ this _is why this is the plan with better chances of success."_ talked Dumont, nonchalantly. _"She's haughty and mighty, she need to be. On her mind she's a survivor, like almost every breakthrough. From the rape to now she saw herself as a tragic heroine and as the_ predestined _queen of the world. And, as she has no empathy, she only understand power as a feature of those like herself. She can't see others being bold enough to try this..."_

_"If we siege her, perhaps we can find a way to invade the Castle without going front..."_ said Djanni hopefully.

_"I don't think so... She's paranoid."_ stated Jiminy _"The only advantage we have now is that, whatever she wants to do with Santa, she'll be hands full."_

_"So..."_ concluded Scheherazade _"If we want to have a chance, we need to strike her_  front _and we need to strike_ now!"

Everyone looked to Bernhardt.

_"This is crazy, but you're right: there's nothing else we can do, except wait her do whatever she wants to take Santa's Mantle and undo the Santa Clause, even forever. And I could say I can't and don't want to imagine a world without Santa and with that crazy bitch overpowered..."_ Bernhardt blurted

_"He's a pottymouth!"_ said with a sing-song voice Blue Fairy, which made Bernhardt go beet red and this made everyone laugh... Which made the mood go a little less blue than was.

_"I have some conditions, though:"_ demanded Bernhardt _"First, all of you will go armed and armored,_ NO EXCEPTIONS!  _We'll provide some weapons for you if needed. Second, if things get really dangerous, you get back ASAP. Third, even if things go risky for Santa, you'll not take unnecessary risks. Am I clear?"_

_"Right!"_ said everyone, and Bernhardt gave a smile.

_"Now... If she wanted to see what is Nice guys fighting for real, we'll show her. And sometimes God punishes us by giving us exactly what we wish for."_ he finished off, while everyone got to the Armory.

---

Jiminy double checked his weapons: he had now a new Jiminy Cane, in red brass, the green Jiminy Cricket head with a taser and a laser beam, that Dumont made in just an hour with the Santa Helpers' help. Inside, the same Fake Orichalcum blade. He had also a pure silver made rapier, on a green and red sheath. Two pistols on holsters on his waist and two new _Tesla Arc Gloves_ on his hands. 

_"Need to say... I don't like being weaponized."_ stated Jiminy

_"Neither us."_ said Bernhardt _"But the Ice Queen is bat-shit crazy: if she can, she'll kill you without giving a damn."_

_"It's for your protection, Jiminy."_ stated Dumont. _"We are on a situation where we don't have full power. Me and Djanni are into a sub-optimal condition, Sugarplum will need to deal with bad magics, Marjanah and Scheherazade will be our support with you. In fact_," emphasized Dumont "you'll _be our field leader."_

_"WHAT?!"_ blurted Jiminy

_"We can't protect you anymore, Jiminy. This is the time to grow. Not grow up: Peter Pan is right and grow up is too much overrated. But to grow in confidence: your powers are really, really useful when you are in your best mind. You analysis capability always give us an edge no one could take out when you're on your best shot. However, you're a kid no more, and today will be one of the worst missions we even had. We are a reduced team, almost without big hitters and ranged attackers, as both Djanni and me are hurt. You'll be even more important, even more to say us when retreat on a big Charlie Foxtrot."_ looked Dumont into Jiminy eyes while saying

_"Okay... But..."_ was saying Jiminy meekily

_"No_ but'_s, short stuff."_ retorted Sugarplum _"You're able: heck, even_ without your powers _you rescued thirteen girls, including Lahab and Scheherazade from_ freaking Caliphate _and took a Mi-38 heli as war spoil for us. You need to understand: you are braver than you believe, stronger than you seem, and smarter than you think."_

_"So, am I a Pooh bear?"_ said Jiminy meekly.

_"Well... In fact, you still look cute as a button."_ she giggled _"But we'll trust you'll do your best shot. And you'll trust us to do what you need us to do. And we'll rescue Santa and kick that spoiled woman butt very very hard. Right?"_

_"Right!"_ assured Jiminy, putting his Top Hat and his Mask.

_"So, let's go."_ said Dumont, when they started to get to Ice Queen Castle.

As Djanni wasn't 100%, neither him or Jiminy was flying, but mounted on reindeers, and Jiminy could see Bernhardt and Metsätäja at his sides.

_"We'll cover you!"_ said Bernhardt on his Earbug. _"On Christmas Village Dispatch is Charlie, our best strategist. Our mission is to throw ourselves into a clash fight against the Cold Hearted to open a way for you_. Your _mission is to get into Ice Queen's Castle and rescue Santa. Any other engagement is secondary and should be avoided."_

_"Jiminy,"_ talked Metsätäja _"you and your team need to advance, no matter what. We'll work to open the way. Then, we'll do our best to avoid any tango outside to get inside after you. Inside, however, you'll be at your own."_

_"Jiminy, Soldaire here."_ he heard the slightly Japanese accented English from Soldaire _"This is a big issue, and I'm sorry you are on your own there: it would not have time, ways or team to send there. All I can say is good luck and remember..."_

"Super-Aide pour les Super-Besoins... _Super-Help for Super-Needs."_ said Jiminy _"And I can't see a more super-need than this one: if Santa falls... I can't and I want not to imagine."_

_"So, focus on your mission and go!"_ commanded Soldaire, with his trained Japanese officer voice.

"Ryoukai! _Roger that! Out!"_ confirmed Jiminy, closing the external comms.

_"Now, Jiminy, anything you can say by now?"_ asked Sugarplum

_"Sugarplum, go Tink and try to avoid the attacks: don't attack, but take as much images so Blue Fairy can work a map or at least some draft of blueprint. Dumont, you and Djanni go and open way the best you can. Scheherazade can help you. If you can help Sugarplum or Dumont on Astral Plane, Marjanah, go for it, but avoid risks: the Ice Queen is clever and have some Astral traps and other nasty things. Go safe and, if there's dangers, get back."_ commanded Jiminy

_"Roger!"_ they all confirmed, when they started to see the Cold Hearted.

_"And remember to avoid their blades: they are cursed with that Cold Heart curse, and you saw what they did with Djanni."_

_"Okay! Let's go!"_ said them.

Djanni was now ready and started to chop Cold Hearted after Cold Hearted, exploding their frozen hearts into its last heartbeats. They were on a very difficult combat, as the reindeer was uneasy with the undead faint of necrosis.

_"Go forward as fast you can!"_ shouted Metsätäja _"Reindeers are perceptive and fierce when put against the undead or unnatural. You need to pass by the Cold Hearted and cross the Ice Gate before that go wild!"_

"Menköön! Tallokoon! Menköön!" said Jiminy, commanding his reindeer to run fast against the Cold Hearted. The reindeer started to ram the Cold Hearted, his antlers blasting the heart-shaped ice pendants. 

They ran and ran for some minutes, Cold Hearted after Cold Hearted being hit, until they could see something.

_"Look, in the Northern Lights!"_ said Jiminy, looking the Northern Lights turning blood red

_"This is_ haram!" screamed Scheherazade _"It's like someone was opening_ Jahannam'_s gates!"_

_"Indeed, it's really evil_ Sikr!" spoke Marjanah in Astral Space, his body limped over one of the reindeers. _"She's doing something with Santa, I can feel it!"_

_"It's like she's trying to corrupt Santa's Clause!"_ shouted Sugarplum _"We have not too much time!"_

_"Let's go!"_ commanded Jiminy, when they crossed the Ice Gate, just some moments before the Cold Hearted could finish to close the gate. Jiminy got away the reindeer and then they run.

## Chapter 12

> _"There's all kind of powers in the new world: there are those who are with Ummah, doing what they can under_ halal. _And there are those that don't excuse yourselves on being_ haram. _Fortunately, Allah the Most High (SWT) allow us to see under the disguises of evil for those who are into_ haram: _if you think that there's a kind of lives that deserve more than others, you're under_ haram."
>
> *Altayr Bashir Tahan, __"Musings on the Holy Q'uran and the Event, from a Breakthrough Muslim"__*

_"Sugarplum... Which route?"_ asked Jiminy

_"Straight... I can't go too much forward!"_ said Sugarplum, fearfully.

_"Me too!"_ talked Marjanah _"This is a big_ haram, _I can feel it!"_

_"Marjanah, get out the Astral Plane. Help in the Defense: you, Sugarplum, Scheherazade, you stay here."_ said Jiminy _"Dumont, you and Djanni with me."_

_"Roger!"_ said everyone

_"Good luck!"_ cheered Sugarplum

"Fee amaan Allah! _May Allah protects you!"_ blessed Marjanah, when they got forward.

_"Are you really afraid of her, Sugarplum?"_ asked Marjanah

_"To be honest, yes: she's weaker as Merlin them me, but his power as a whole is bigger than mine!"_ said Sugarplum

_"And we should not help them?"_ asked Scheherazade

_"To be fair, their powers are so different that they can bring the fight to an unexpected arena for the Ice Queen... At least I hope so. Anyway, there's Cold Hearted coming. Let us kick some asses!"_

"Subhaan Allah." said the sisters, before they started the fight.

---

_"Alright, Jiminy."_ talked Dumont _"Now, what you can foresee?"_

_"She has no Cold Hearted there: she believes that anyone that would try to invade would not get kicking the front door."_ stated Jiminy, looking forward via _Bellax Analytica_

_"Good..."_ spat Djanni _"We'll fight and defeat her soundly."_

_"Now, forward! Whatever she's doing, she's doing at the Castle's main room!"_ shouted Jiminy. _"And it's a real dark magic!"_

_"Sugarplum would be useful here."_ stated Djanni

_"Not exactly: Sugarplum's powers are on the same arena. And looks like the_ Christmas Spirit _side reinforces the_ Merlin _side: she as a whole is qualified as Ultra. Now, we need to bring the fight for another arena."_

_"Right: we'll play_ sente, _she'll play_ gote." said Djanni, remembering a time where Jiminy said this

_"But this time, she's on_ sente. _We need to take_ sente _back."_ concluded Jiminy, when they arrived into a big room, so white that almost blinded Jiminy.

And blinded Djanni.

_"Jiminy, I'm blind!"_ screamed Djanni

_"Me too!"_ confirmed Dumont

_"Shucks! Whiteout!"_ cursed Jiminy, while his eyes was adapting for the whiteness overload. _"Try to take cover!"_

They split to find places to cover, when they felt some rays getting out trying to hit them. Jiminy heard some yelps.

_"Dumont, Djanni, how are you?"_ asked Jiminy

_"I was hit!"_ shouted Djanni _"My left arm and leg are now frozen!"_

_"Me too!"_ talked Dumont _"Not so bad, but my right hand is frozen. I can't operate Demoiselle and use nothing at all by now!"_

_"Okay..."_ said Jiminy, trying not to panic _"Hold your station now: I'll engage her and try to gain time until you recover!"_

_"She'll kill you!"_ shouted Djanni

_"If we do nothing now, she'll kill Santa and take his Mantle and this would be really_, really bad!" stated Jiminy, pushing his blades. _"Try to recover fast... I'll gain time."_

_"Good luck, Jiminy. May the Force be with you."_ blessed Dumont.

Jiminy counted until three and opened his eyes. Now he could see the wall in front the pillar where he took cover. Everything was white, but not calmly white like on a hospital, but _painfully_ white, as white as white can be, almost hurting with the light that it shone by itself.

_"Okay... Blue Fairy..."_ thought Jiminy

_"This will be difficult."_ stated Blue Fairy, looking like a Christmassy version of a Macross strategist.

_"I know... I don't think I can do the job... So, I want you to record and send this all for Soldaire, Dumont, Sugarplum, Bernhardt and for Chicago Sentinels. If need too, send also for the Kahle Conspiracy, I think they know about Santa. To be fair, send to everyone you can and believe is a good idea. And a rule: if I'm to die, break the Quantum Link between us and go for Shelly/Shell to try find someone else."_

_"No way I would do this, you spoiled brat!"_ pouted Blue Fairy _"One: it's impossible. Some of my processing, and not only for interfacing and presentation, are done on your brain. A very small part, for sure, but enough to make sure that I could not stay stable, and so 'live', without it. And second: we are partners in crime! Don't ask me to split from you. The time I was into Aleph-One protocol was the worst I had, and I don't want that happen again!"_

_"Okay... But all the rest, do as much as you can."_ thought Jiminy, somehow happily _"Do not intervene, unless I'm under life risk: I'll need to use_ Bellax Analytica _almost 100% of time and I can't lose concentration."_

_"Get out, your pesky flies."_ spat The Ice Queen, with a pert, arrogant tone _"You are in_ MY _dominion, and this is_ NOT YOUR FIGHT! _Get out now, and_ maybe _I'll be generous and let you live. Stay, and you die."_

_"I heard enough!"_ thought Jiminy.

Jiminy got out, but his two unsheathed blades crossed over his chest, into an _obvious_ salute stance, being ready for combat. The Ice Queen blasted a shot of white energy against him, that he parried and dispersed with the darker blade, the one made on Fake Orichalcum.

_"To be fair, this is_ MY FIGHT _too. I'm Jiminy Cricket, from Herós Sans Frontières. Our motto:_ Super-aide pour les Super-Besoins, _super-help for super-needs. And I can't see no one in bigger need than Santa now."_ he said, when he saw the man in the white table, as painful white as everything else.

Santa was totally naked over a marble white table, shackled on it. His Santa Suit was in torns, thrown everywhere. He was cut in his chest. But the worst is that Jiminy could see that Santa was not looking like an old, but full of energy gramps. He looked sullen and emaciated, his belly without the jiggling filling, but looking like an empty flesh sac. This was signal of only one thing...

Santa was dying.

_"Too late, brat. I'll be the only Sire in the Winter. I'll be the only power. No more_ Ho! Ho! Ho!_s. Winter will be unforgiving and destructive and dark, and those who not revel on my power will be killed or become my Cold Hearted servants, like that other brat almost became."_ gloated The Ice Queen, shoting again the white energies against Jiminy, that used his blades and his Savate and Fencing and Jeet-Kune Do training to avoid the attacks.

_"I know about you, Agnetha... And I can't say how much you had suffered. But I can say you_ are not _different from those who almost killed me, that saw himself above others. You became a Queen bereft of kindness and happiness. I could had been someone like you: I could had wanted to make that bullies burn in the alley. I still remember some dark things that tried to coil into my head, to convince me to kill them under fire, under gravity, under thunder. But I just wanted them to leave me alone and let me live in peace."_ said Jiminy _"You, however, wanted to be someone to be feared. I can't say I'm not sorry, I would be a liar to say this way, but I need to say that you're not above all kids I saw at_ Roque Santeiro, _ORSL 1, Hagia Sophia, and even the little Sally I helped to birth at Newark. So... I have no choice but stop you."_ lamented Jiminy, presenting both blades in salute.

The Ice Queen cackled like a crazy:

_"You! A kid! A_ freaking _kid! Do you know what I just made with Santa? I'm taking powers that should be_ mine! _And I can see through you... You're meek. Weak! You was kicked almost to death because you was weak, and you almost was killed by your bullies other two times because you still think on good and bad. Smell the coffee, brat:_ might MEANS right." she spat, sending two more cold energy bolts against Jiminy.

One of the bolts was parried, but the other cutted Jiminy's mask, hitting his cheek, making him yelp, a cold feeling on it, that was replaced by the warm blood that got through a paper wound. His mask, cut in the binds, fell in the ground, and then Jiminy risen his face.

"Be water my friend. _En garde!"_ shouted Jiminy, running against The Ice Queen.

She just made a small measure and turned into a kind of snowy mist, thinking she avoided Jiminy's attack.

And she did... But just the first one.

Jiminy looked upon with his _Bellax Analytica_ and he stayed on the move, spinning his body on a spinning attack with both blades. The one with the Fake Orichalcum blade failed, but the Silver one had catch her at her cheek when she solidified herself again. She reacted trying to punch Jiminy, that used the momentum to evade the attack.

_"You foolish brat!"_ said The Ice Queen _"Do you think that know me? Do you think you know my pain? You know nothing, Jiminy Cricket._ Nothing. _I'll kill you, and then Santa's power will be mine! No more Santa! No more Christmas! Where he was when I was being raped? Where he was when my parents died? What do he know about my pain? You all know nothing!"_

_"You are right!"_ shouted Jiminy _"I know nothing about you, except a thing or two we had obtained on your profile. But I can infere. You are a victim, I know. As I was too. But you made your choices_, yours only. _I will not say you could or should forget it, because I can't say. But you can't also say that I don't know about pain: I had many of my bones broken by the bullies that tried to kill me. I got comatose by 6 months after that attack. I was almost killed by Alsyf Alayat. I was kidnapped at least three times, was shot and had lots of pain. Maybe not as painfully as you had, but you offend me and offend yourself saying I don't know about pain!"_

_"Enough!"_ shouted her, using a kind of magic to push Jiminy away, making him hit hard the wall behind you.

_"I'll kill you, and I'll kill Santa with a dagger full of a Nice kid blood! This will be so fortunate!"_ she gloated, running to Jiminy. She got and pierced Jiminy with the dagger, making him scream.

_"JIMINY!"_ screamed Dumont and Djanni, still blind, hearing Jiminy's scream, thinking he would be dead.

_"It... Will... Not... Be... That... Easy!"_ shouted Jiminy, feeling the cold breath from Ice Queen, and pushing her away with a kick, running away. The dagger was craved into his bleeding left arm.

It was when Jiminy felt something, a heat getting into himself that was totally incompatible with where they are.

_"NO! IT CAN'T BE! THIS SHOULD BE_ MINE! ONLY MINE!" despaired the Ice Queen, looking for Jiminy on an extreme fear.

---

While fighting with some Cold Hearted, Sugarplum, Marjanah and Scheherazade could feel something

_"The tide is turning! The magic is getting somehow back! Is not like Santa, but looks like the Ice Queen is on trouble!"_ effused Sugarplum

_"Lo! Allah is forgiving, merciful!"_ praised Scheherazade

_"But how?"_ asked Marjanah

_"I don't know... But more than ever, we need to do our job!"_ said Sugarplum, now full on new energies to fight against the Cold Hearted

---

At the same time outside...

_"The Northern Lights!"_ exclaimed Metsätäja, looking to the lights that were less red now _"The wave is turning!"_

_"Stay in the fight, everyone!"_ boasted Bernhardt _"Those guys are turning the wave back for us! We need to hold the fight! For Christmas!"_

_"For Christmas!"_ everyone boasted, holding the fight.

---

A kind of golden energy was getting from the torn Santa Suit to Jiminy. Jiminy could not see his own clothes changing for something more Helper-like, red and green all around, his Top Hat turning into a beautiful red elf bonnet. His nose had gone petite and snub, lots of freckles over his cheeks. His ears became pointed like a Santa Helper's, his hair becoming more copper-like, his eyes going green as mistletoe.

But he could feel his power growing, into weird ways!

_"Jiminy, I can't say what's happening... But you are into a surge of power! You're going Ultra power... Even Omega!"_ gasped Blue Fairy.

_"Stop, foolish kid! You can't control all this power!"_ said The Ice Queen, in rage and despair.

For some reason, Jiminy just took his blades and touched them hilt to hilt. They fused and their blades became blunt, looking more like batons than swords, becoming a big staff. On each of them one word: _NICE_ and _NAUGHTY_.

_"Looks like Santa Clause has its ways to defend itself."_ smiled Jiminy, spinning the big staff over his head, like he did before into some Jeet-Kune training.

_"I'll kill you, kid!"_ said the Ice Queen, his beauty growing big, but inhuman, like a so beautiful demon that was so horrible to see. She shot some bolts of energy, that Jiminy stopped using the staff, dispersing the energies aside.

_"I'll free Santa as soon I defeat you, Agnetha."_ said Jiminy

_"I! AM! THE!_ ICE! QUEEN!" she shouted, using again her power.

Sugarplum and the Cissé sisters Marjanah and Scheherazade came. They ran for Djanni and Dumont

_"What's happening here?"_ demanded her

_"Can't say!"_ exasperated Djanni _"I'm blind thanks for that woman whiteout. I can say that something happened that is turning the tide, like Santa's magics passing to someone else!"_

_"It's Jiminy!"_ exclaimed Marjanah, looking for the battle there _"The Santa Clause for some reason channeled on him, not on the Ice Queen!"_

_"But... Jiminy will die!"_ gasped Sugarplum _"It's too much power to be held for someone unprepared. Neither I... In fact, neither_ Ozma _would be able to hold so much power! It's almost Omega! He'll burn like a candle!"_ she shouted.

_"Jiminy, finish this bitch fast!"_ shouted Dumont

_"I will!"_ agreed Jiminy, running to her, when she tried to blast a shot against Santa.

_"NOT HIM!"_ he exclaimed, receiving the attack straight on his chest, being blasted against a wall

_"JIMINY!!!"_ exclaimed everyone while the Ice Queen got nearby Jiminy, with the smile of a predator

_"Now..."_ she gloated, taking another dagger from her clothes _"I kill you. I take those powers. I kill Santa. And Christmas and Winter are mine."_ looking for Jiminy's almost unconscious body.

Almost.

When she got to stab his heart, Jiminy jumped away, kicking the dagger from her hand. He then took the other it and, throwing it to Sugarplum, shouted

_"Free Santa! Take care on his wounds! I'll join you as soon I drop this lady down."_ said Jiminy, jumping nearby the Ice Queen and hitting her with the dark side. Jiminy Cane's taser was still there, which made her being knocked back by the electricity induced recoil. Then he run and, using some techniques from Savate, hit her straight in the legs, sending her to the ground. After that, he pushed the staff to her throat.

_"You would not kill me!"_ sneered the Ice Queen _"If you are under the Santa Clause, you would never kill. Even the top Naughty people. Try this, and you'll be put yourself on the Naughty list, and the Clause would kill you."_

_"You're right."_ said Jiminy _"But I can taser you out unconscious. To be fair, I think this is the best solution."_ he finished, while touching the side of her head with the NICE side of the staff, sending the energy through her brain, inducing seizures until she got down.

It was when Santa looked for Jiminy, while Sugarplum treated some of the worst damages...

_"Jiminy, stop now! You'll burn like a candle if you stay with Santa's Mantle...."_ exasperated Santa

_"But... What can I do? My head is aching a lot!"_ whined Jiminy, now that the battle frenzy cooled down feeling the painful power on him

_"Come to me! Hug me!"_ shouted Santa

Jiminy got, in pain. For some reason, his steps were difficult and painful.

_"Jiminy, pretty please!"_ said Blue Fairy into his head, with a somewhat fainted "voice" _"This power is too big to be held by you: it will burn your brain like a firefly! Go and free it back to Santa."_

_"No!"_ said a voice _"This is mine!"_

The Ice Queen got and tried to held Jiminy. And Jiminy turn back and put his hand on Ice Queen throat

_"Yeah! Try to kill me! The power will kill you first and I'll take the Santa's mantle!"_ she said, with a greedy, hungry voice.

_"Jiminy, please! NO!"_ shouted in despair Blue Fairy, that was disappearing into his mind.

He could feel Ice Queen's head going limp. He looked behind her and it was Djanni, that punched her back to unconsciousness.

_"In the name of Allah, the Forgiving, the All-Mighty, the Merciful (SWT), Jiminy, go for Santa and free yourself from this burden, giving his powers back to him!"_ he said, smiling a little

Jiminy then run and hug Santa, to give him the Mantle back. While hugging Santa, he felt his belly filling again, his clothes reconstructing around him, as the energies from the Santa's Mantle migrated from him to Santa, his rightful owner.

_"Ho! Ho! Ho!"_ laughed him, very heartly _"Don't give us this kind of scare again, Joshua. Only your heart allowed you to save yourself. The Santa Clause can be very unforgiving for those that wasn't worthy of them."_

_"I... I..."_ said Jiminy, but he then snapped and cried. His team got for him, hugged him, trying to lift him over.

_"Calm down, Jiminy. You did alright. You were just a little bit from failing, but you have Nice friends with you. You were and would never be ready to take this power. But you did it right, and now it's time to get back Christmas Village and heal your wounds. And... Well, it's December 21th. Maybe you'll want to take a ride for home in some days."_ said Santa, when they heard a rumbling noise.

_"The castle!"_ shouted Scheherazade _"It will fall down!"_

_"We should take the Ice Queen!"_ said Jiminy, running to her, when some icy pillars and chandeliers fell down blocking his way

_"No, Jiminy... It's too late. She's beyond salvation."_ said sadly Santa _"Let her have a fast and peaceful death. It's the biggest mercy we can exert to her now."_

_"C'mon, Jiminy!"_ shouted Dumont _"There's no use for you to die with her!"_

Jiminy run away, tears on his face.

They just had time to get out and jump before the Ice Queen Castle fell down.

_"Is she... dead?"_ asked Metsätäja. They looked and saw that the Cold Hearted were falling down, now without the magics that made them undead. One or another still blinked his eyes and tried to do something, running away.

_"Looks like: the Cold Hearted are no more..."_ said Santa _"But I don't think it was the last time we heard about the Ice Queen. She, as me, is a_ Christmas Spirit, _and she can reappear herself."_

_"Well... By now... It's over."_ said Sugarplum

_"Except..."_ said Jiminy, that looked in the mirror-like ice debris from the Ice Queen Castle _"How I will explain my parents I'm looking like a Santa Elf?"_

They all giggled

_"Hurry up, everyone!"_ said Santa _"The Great Day is nigh, and this mess made us got behind the schedule, even more that we'll need to repair Christmas Village and lots of toys. We have no second to lose."_

_"We'll help you!"_ said Jiminy and his teammates.

_"You already did a lot..."_ was retorting Santa

_"...but without us, you'll not be able to rebuild all the toys."_ said Jiminy _"You're certainly some hands short, and we can help a lot, if you allow us."_

_"Alright... Alright..."_ smiled Santa. _"Let's go: there's no second to lose. Bernhardt, take all the helpers in good shape and schedule them for the hard work. Those that are not so able, put on lighter work: food, rebuilding, you name it. Your choice."_

_"Roger!"_ said Bernhardt, when they mounted their reindeers and got back for the Christmas Village, while the Castle melted back into the snow, like a nightmare that had gone away.

## Chapter 13 - Epilogue

> _"Our mission at [REDACTED] showed me how important was our mission as capes at Herós Sans Frontières: when someone like [REDACTED] needs help, and the stakes are so high, you can't just give your back for it. You need to make a choice, take a stance. And this is why I'll always be a_ Herós Sans Frontières _cape, as far I can."_
>
> _From Jiminy Cricket's Journal_

It was the end of Santa's Voyage around the world. He just touched back Christmas Village with his flying sleigh and reindeers. All the Village was there to greet back the Boss, the Big Guy. Even Bernhardt had a smile now, with Leon at his side, using lots of bandage on his body, recovering from the grueviling attack he suffered from the White Queen. He just pulled the reins back and made the reindeers stop.

_"Woah!"_ he said, passing the reins for a Helper that would usher the reindeers back their stables  _"Alright, my friends. Another good voyage. Take your time and rest for next year."_

_"Any problem, Santa?"_ asked Bernhardt

_"None... Well, maybe... Who had the idea to send a skunk for that bully at Wisconsin?"_ asked him

_"It was me."_ said Jiminy, still recovering from the power oversurge he had after receiving Santa's Mantle: he still had the pert nose and freckled cheeks, but his pointed ears receded a little, the points not too visible as the Santa Helpers'.

_"That was funny, but next time close the cage correctly: it almost got out and it would be a bad thing if all the gifts got stinky after a smelly skunk sprays them."_ he retorted Jiminy, smiling

_"Right."_ said Jiminy, when they all got nearby their reindeers

_"Are you sure you don't want a ride back? I can left you at ORSL 1 with the blink of an eye."_ said Santa

_"Yeah... We want to get back the common way. And we already have lots of gifts, including the blueprints for your Vernetech."_ said Dumont. _"We don't want to be over-demanding."_

_"Never you would be: without you, Christmas would be ruined. You are almost Santa Helpers by yourselves, as you're as caring for others as Santa Helpers."_ answered Santa _"You'll be always welcomed at Christmas Village, as long you stay in the Nice list. If you need anything that Christmas Village can help, you just ask."_

_"Hope we don't need something that big."_ said Djanni _"If we think this way... If neither Christmas Village can help, it would be a big need."_

_"But it is all about what we are, right?"_ responded Sugarplum  "Super-aide pour les super-Bedouin, _super-help for the super needs..."_

_"Right."_ agreed both Scheherazade and Marjanah.

_"Now... What you'll do?"_ asked Santa

_"Get back Rovaniemi, and from there go for Malta. And from there, ORSL 1, our home."_ answered Dumont

_"The One bless you, no matter how you name it."_ said Santa.

_"Hey, everyone. We are getting late: I don't want to be in the wilderness during night. And we are into a tight schedule."_ retorted Metsätäja _"Sorry, Santa, but you know how dangerous Arctic can be during night. Even without the Night Blizzards."_

_"Right. And about the Christmas Compass, I'll send it back to NORAD Santa Tracking. Just remember to send the After-Action Report for them. And remember: this is classified."_ said Santa

_"Right!"_ they replied, mounting their reindeers, while they were being cheered by the Santa Helpers, and they passed through the Christmas Village gate.

_"Hope this wasn't a dream."_ said dreamy Marjanah.

_"It wasn't. Pity we'll can't say about this for everyone else."_ replied Jiminy

_"You can... But for them, this will be a fantasy."_ stated Metsätäja.

_"Do you think that the Ice Queen survived?"_ asked Djanni

_"I don't know... And, to be hoenst, now is not a concern."_ assured Dumont, while they got into the road, now without blizzards, under the Northern Lights back to Rovaniemi.

And to their lives...

---

_"Hey, sir, we found this girl in the water..."_ said the man in the whale hunter boat in the Arctic Sea

_"It's a miracle she is still alive, into a so cold water."_ replied the captain, looking for the beautiful unconscious girl, while the men was wrapping her into a thermal blanket _"Looks like she was released from a kind of curse, to be fair. Her face is beautiful, but somehow sad."_

_"She's waking up!"_ said the sailor.

The girl opened her eyes: they were blue like ice pendants.

_"Where am I?"_ whispered the girl.

_"You fell somehow in the Arctic Sea... How that happened?"_ said the sailor

_"I can't say... I don't remember what happened..."_ said the girl

_"Anyway. This ship is the_ norra drottningen, _the North Queen. I'm Nils, the Quartermaster. Our captain is Durval. And you?"_

_"My name is... Agnetha..."_ said the girl

_"Look like our Sleeping Beauty has a name. Now... We are locked here during the Winter, but we have lots of supplies and we can have you with us for a while. Where you want to go after?"_ said Nils the Quartermaster

_"I believe I want to go somewhere... Warmer... Any chance to leave me at Gibraltar or somewhere like this? I don't want winter for a time, for sure."_ said Agnetha, the former Ice Queen.

_"No problem. As soon we can get out, we'll send you there. And need to say: you have a nice smile."_ said him 

_"Thank you..."_ she said, looking for her face in a mirror. _"You're right: I can't remember the last time I had this kind of smile... A kind one..."_

_"Well, let's go. Think the soup is ready."_ said Nils

Agnetha looked to the Northern Lights and remembered everything, including her time as the Ice Queen. She then gave the back to the Northern Lights, trying to think on a new life.

> The End... And a new Beginning...

<!--  LocalWords:  Jiminy Herós Frontières Metrocon Boyar Djanni HSF
 -->
<!--  LocalWords:  Altayr Bashir Tahan superstrenght Jiminy's McRae
 -->
<!--  LocalWords:  cosplayer gangly Bellax Analytica Soldaire Seiji
 -->
<!--  LocalWords:  Shirou cosplays Astra Baldur SaFire NGOs alma ORSL
 -->
<!--  LocalWords:  Mediciéns Comissioneer UNHCR Hagia Ronins Corin du
 -->
<!--  LocalWords:  Leone Roque Santeiro Soleil Marjanah les besoins
 -->
<!--  LocalWords:  Whitlow Hillwood Djanni's Astra's Superdads O'Hare
 -->
<!--  LocalWords:  O'Mahoney intel Hufflepuff Lahab LionHeart LaSarte
 -->
<!--  LocalWords:  Luchador Shoup Isa Tahan's Ozma th Père Noël UATs
 -->
<!--  LocalWords:  reindeers Dumont Santo Clós Carlsberg UCB Vernes
 -->
<!--  LocalWords:  Rovaniemi NGO Seif muslim haram Landers Christien
 -->
<!--  LocalWords:  Mileux Metamorph LDS Vernetech FUBARed satlink Bel
 -->
<!--  LocalWords:  Soldaire's SolArmor Ummah Wahiba Cissé's Kiruna ar
 -->
<!--  LocalWords:  Metsätäja Mariya Laplandish Kosovo Iman Mylläri Ph
 -->
<!--  LocalWords:  Metsätäja's Albearta berbers indians Hawkeye Ávgos
 -->
<!--  LocalWords:  McGuyver There'll Maryia'll PowerStreak Hearted se
 -->
<!--  LocalWords:  undead muslims murtadd Kuntur Ajaxes tighearna nez
 -->
<!--  LocalWords:  tabhair bheannachtaí lámha Maktub Mirian mojo ssaw
 -->
<!--  LocalWords:  C'mon Saleh Lybia OWWIIEE Milly Milkyway cyber ish
 -->
<!--  LocalWords:  Woah MedSpecs pince casquette McC Christmassy juju
 -->
<!--  LocalWords:  exsanguination whitter exsanguine punctioned Shikr
 -->
<!--  LocalWords:  hurted onesies embroidements sheat ben Zayn Chakra
 -->
<!--  LocalWords:  Colocation Mahdi's airspaces Santa's Bernhardt's
 -->
<!--  LocalWords:  callsign children's shtiah Sami GPS onesie brewage
 -->
<!--  LocalWords:  infere joice thornbushes idolatrizing habitué un
 -->
<!--  LocalWords:  Krampus fosterage SMD videogames chalantly Maskcam
 -->
<!--  LocalWords:  Maskcams McNish Leon's Macedo Luiz di quatrium na
 -->
<!--  LocalWords:  Tink counterfeature shockwave meeked walkie Alsyf
 -->
<!--  LocalWords:  blockaged McCarthy's Jahannam's pigstill knowning
 -->
<!--  LocalWords:  devourer composture Alayat chackling flashbang dom
 -->
<!--  LocalWords:  Debruana máthair gaoithe caith naimhde Badb mná
 -->
<!--  LocalWords:  lightsaber jetpack Kwazani pottymouths Agnetha
 -->
<!--  LocalWords:  Förkylning Agnetha's pottymouth weaponized meekily
 -->
<!--  LocalWords:  Ryoukai Mennä Q'uran amaan Subhaan sente gote Jeet
 -->
<!--  LocalWords:  Macross Kayle Aleph torns gramps shoting Kune Nils
 -->
<!--  LocalWords:  garde Cissé laughted heartly norra drottningen
 -->
<!--  LocalWords:  Durval
 -->
