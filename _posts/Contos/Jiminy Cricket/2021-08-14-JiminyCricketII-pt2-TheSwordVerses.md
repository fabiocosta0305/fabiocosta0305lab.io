---
layout: contos
title: Jiminy Cricket II - pt 2
subtitle: A new challege comes against Roque Santeiro and Heròs Sans Frontiére... And Jiminy Cricket is on targer!
teaser: A new challege comes against Roque Santeiro and Heròs Sans Frontiére... And Jiminy Cricket is on targer!
category: stories
---

## Chapter 12

> _"The_ Undying Caliphate _should not be taken as a Muslim organization as much as KKK should not be seen as a christian organization. In fact, both shows the worst side of religious over-zealousness: as far KKK had gone away from Jesus's path, Peace be with Him, the_ Undying Caliphate _turned away from Allah the Most High and the Prophet's (PBUH) teachings, as expressed in the Holy Qu'ran, by bringing_ Jihad _as a bloodsport, not as a self-purification process."_
>
> **Altayr Bashir Tahan, _"Musings on the Holy Qu'ran and the Event, from a Breakthrough Muslim"_**

It was a tense flight: even seeing all the Disney shows in the flights entertainment system, Jiminy was worried, as he didn't knew what would be waiting for him at _Roque Santeiro_. The intel on his epad was only matters-of-facts, things that he could discover with 5 minutes of Net-Fu, not the _real_ situation at _Roque Santeiro_. He also knew that there was nothing he could do by now, in a flight between Amsterdam and Freetown, beside study the intel into the epad or rest, hoping his recently uncasted leg get better when arriving at Sierra Leone. 

The plane had touched down and he saw a known face when got out the gate: Dr. Lesley Dimantas, capename _Squirrel Girl_. 

_"Jiminy, I'll brief you while we get at_ Roque Santeiro. _We can't waste time: there's a helicopter for us. Cabby is waiting us there."_ said Lesley, today using rabbit's ears, moving all around trying to find some weird sounds, and a skunk tail. 

An A-Class Metamorph, she was able to go _Chimera_, so she could do partial metamorphosis, and combine multiple ones to somewhat "choose" the best natural features she needs in any situation. Normally she just uses squirrel ears and tail,  as fashion, so Jiminy saw that if she was using those parts by their features, this means business.

Kabba 'Cabby' Bangura was a Paragon guy: like Cajun, he didn't had a breakthrough in the strict sense of the word, but  he was a hell of a driver that could pilot anything, from a VW Beetle in the dirt roads at Sierra Leone to an helicopter to a fighter jet to a Dallara Indycar open-wheel at Indy 500. He was the local agent for HSF that supported Jiminy and others in the Derek Kwazani operation. Jiminy thought he was an incredible guy.

Jiminy got out the airport to a heliport where a helicopter was ready for them. Two guys, that looked like Atlases under civil, where looking around while some airport crew loaded the helicopter with his things, including his clown trunk. Jiminy got into the helicopter and Lesley passed him the helicopter headset and a little box. When he opened it, there was a small Taurus 9mm pistol and two extra magazines into a holster. Jiminy worried looked to Lesley and she just shrugged.

_"Superior orders. Diana doesn't want anyone unarmed when outside_ Roque Santeiro." said Lesley, taking a submachinegun.

_"It was that bad?"_ said Jiminy, putting the holster and loading the pistol. As much he doesn't like shooting, he had training on them.

_"Had you read the intel?"_ asked Lesley, and Jiminy nodded _"They came like a storm, with many breakthroughs, breaking into, kidnapping some of the refugees and killing others. We had lots of casualties, even some of the low level breakthroughs being hurt. They also published a video at ViewTube with a_  fatwa _against_  Roque Santeiro _and_  Herós Sans Frontières."

"Fatwa?" asked Jiminy

_"A statement about about something under Qu'ran and Islamic law, Shar'ia's, context. Some Islamic places and populations, specially those overzealous on Islam, take them as law. Problem is: technically any_ mufti _can issue_  fatwas. _Only his fame can say if people will look his_  fatwa _as something_ halal, _'good', as far they never go against some of the pillars of the_ Islam." said Lesley, while Cabby took off from the airport.

_"So... This guy is respected enough?"_ said Jiminy

_"Looks like. The_  fatwa  _wasn't signed or aligned with any of the main Islam groups,_ Sunni _or_  Shia, _but they are calling some people to their cause, because the clerics signataries are all linked to the_ Undying Caliphate _in a way or another."_ said Lesley _"As a matter of fact, Djanni and his father read the_ fatwa _and did the translation, and they said it was a great piece of bullshit. Not in those words, of course."_

_"So, people who believe that_ fatwa _had gone and attacked_ Roque Santeiro?" asked Jiminy.

_"Yes... You need to understand that people on Islam take those_ fatwa _very seriously, and some people thinks that those_ Caliphate _nuts are right on scourging everyone else."_ said Cabby _"And those guys declared that_ Roque Santeiro _were full of_ Kafir, _infidels_, Shirk, _idolaters_, Zindiq, _hereticals and_ Murtadd, _apothates. So, they should be expurged, based on a radical, violent view on Qu'ran."_

_"And they had declared_ Jihad _against_ Roque Santeiro. _Things just grew worse when they distorted the camp name origin to show us like a kind of heretical cult."_ said Lesley _"They came and took at least 50 families, and kidnapped a class of young girls. They killed the refugees that resisted, those that saw the_ Caliphate _ways and were against them, and they just got out when our capes decided that they should dance the Foxtrot."_ 

_"But at least 1000 people were directly or indirectly affected, and the_ Caliphate _said that this was just the beginning. Some of the analysts says the_ Caliphate _are looking to the sub-Saharan African countries as a way to regain the lost power after the War, getting into Mali and Guinea and other Muslim majority countries, and so Sierra Leone is one of their targets. The control of the sub-Saharan Africa would provide them with resources for an all-out war against Israel. Problem is: Israel certainly would go all-out also, and this would end badly for everyone."_ said Cabby.

_"So, what we'll do?"_ said Jiminy

_"First of all, the Camp is on Code Red: until we remake the lost infra and equipments, no one get in or out without Diana and the other big guys knows. Everyone in Cape until further order, and rebuild the camp is priority Alpha. Your team is already deployed, Jiminy, they got yesterday on the field. Soldaire say me this order for you: as soon we arrive, you are to get on our costume and go for Dispatch and be always armed, either with the Cricket pistols, the Tesla Arc Gloves and the Jiminy Cane. Dumont had corrected the glove project and put some batteries for them on your standard Costume."_ said Lesley _"I myself will get for the perimeter: I can turn myself into a meerkat or a orangutan if needed."_

_"Sierra Leone is also under an Alpha situation: the_ Caliphate _had expelled some people and pushed them out of their homes, including Nondaba's village."_ said Cabby

_"What?"_ said Jiminy. The Nondaba village was the place where Jiminy lived some of the most important and difficult experiences in his life, when a fight between Josephine Nondaba, capename _Kilimanjaro_ and his team almost resulted in him going crazy when his _Bellax Analytica_ gone haywire, and where he saw a kid, Senesie, undergone a drug-induced psychotic breakthrough. His actions and his team's were crucial to the kid survival and recovery. Now he was a B-Class Merlin developing healing powers, training under Kilimanjaro and other Merlins.

_"The Nondabas are animistics, from the old Yoruba religion, so for radical Muslims this make them_ Shirk. _The Caliphate attacked the village, and expelled them. Both Kilimanjaro's mother and grandmother had died in the process."_ said Cabby.  

Jiminy felt some sadness: he remembered them, Masseray and Mabinty, caring him when he was crazy on the _Bellax Analytica_ oversurge that haywired his mind into a grim multiple potential futures nightmare. They were very old and wise, like those sage woman from the fairy tales. Jiminy would always remember Mabinty looking to him and saying he had a kind face below the cricket mask.

_"Where they are... I mean, their bodies, they were buried?"_ said Jiminy

_"No: the Nondabas cremates their dead. I think Kilimanjaro is with their ashes."_ said Cabby, when Jiminy saw the _Roque Santeiro_ camp.

_"I would like to show my respects before getting in the field."_ said Jiminy.

_"Alright, but be fast."_ said Lesley, while Cabby touched the _Roque Santeiro_ heliport _"We need you ASAP in the Dispatch."_

---

Just half an hour after, Jiminy was in Dispatch, already back into his costume, armed with their Cricket pistols, energy pistols made on the _SolPistols_ design by Doctor Shokichi Hisagawa, the same Verne that developed the _SolArmor_ Soldaire was donning in the field now that Jiminy freed him from the Dispatch work. In his gloves, two small circles showed the (now deactivated to avoid problems) _Tesla Arc Gloves_, energy weapons based on the Demoiselle's _Tesla Arc Cannon_, made by Dumont. Jiminy just took a rapid shower, put his costume and gone for give his condolences to Kilimanjaro and do a little prayer for Mabinty and Masseray's souls.

_"Jiminy, had you saw the_ fatwa _text?"_ said Diana

_"No... It wasn't in the epad HSF gave me at NY, and Lesley didn't show me the contents."_ said Jiminy 

_"I'll pass you it, and as things are a little calmer now, you should take your time and read it to know what we are dealing with. Mrs. Zahan translated and commented it under the_ Qu'ran _for our allies in the Islamic States, like Guinea and Sierra Leone."_ said Diana, sending him a file via the Dispatch system to his station _"Need to say, this is classified intel: LDS had not made an official statement on this one yet, this was the reason it wasn't into the epad at NY. Obviously people are finding this on the Internet, but officially we still treat this as classified intel."_

Jiminy just took two minutes to read, and other two to get out from the flabbergasted state he gone.

He almost expelled some bad words!

_"What a pile of..."_ said Jiminy

_"I know, but it was done for their audience. They are doing all they could so people gets radicalized after what happened with the_ Caliphate War _and_ Eretz Israel. _For them,_ Roque Santeiro, _by promoting integration and helping the refugees under HSF and MSF organizations, are part of the so called_  'Crusader Forces from LDS'. _They totally ignores that_ Red Crescent, _the Muslim arm of Red Cross, is through and through with us! We are and we'll never be associated to any side in_ any _conflict:_ our side is the refugee side, _is on our mission. To be bluntly frank, as long they doesn't mess with people, LDS and the Caliphate could just nuke themselves into oblivion and I would not give a single damn. But the_ Caliphate _is working the same rhetorics every totalitarian regimen did in the past."_ said Diana

_"And now?"_ said Jiminy

_"We are even with a bigger problem: LDS is calling back their troops, maybe to prepare for an intervention against the Caliphate, so we'll be at minimal forces from them. UNHCR, via UN, will reinforce our security using the Blue Berets, but we needed to call almost all the Situation 2 teams from HSF. And the big kahunas at HSF doesn't like this."_ said Diana _"To be fair, you were the last team 'in the bench' avaliable, and we are all praying for no Omega Events by now, or things will go really Foxtrot worldwide!"_ said Diana, and Jiminy could see, by her voice in the Dispatch comm, that she was really tired.

_"Are you okay, Diana?"_ asked Jiminy

_"Just tired..."_ said Diana _"Hadn't too much sleep since the Attack: just 8 hours in 72."_ said Diana

_"If you want to take some sleep, I assume the Dispatch!"_ said Jiminy

_"Kid, it's a big of a bite for you..."_ said Diana _"But... Let us do this way: Colonel Carlos will take Dispatch for the Berets, and you'll help him by dealing with HSF, okay? Really could have a use for some sleep."_ said Diana

_"Okay!"_ said Jiminy and another voice, that he saw as Colonel Carlos, that he knew during the previous tour at _Roque Santeiro_.

_"Good. Carlos, Jiminy, Dispatch is yours. Remember: we are at_ Code Red, Alpha _Situation. Any serious event, wake me up. I'll be here in my room."_ said Diana

_"Roger!"_ both said

Jiminy was very useful, by coordinating the cape teams to rebuild the local infrastructure and defense lines. He was doing the best for his job when one of _Roque Santeiro_'s intel guys called him.

_"Jiminy Cricket, Corporal McDaggert, Irish Army, sir!"_ said him, in a window from Dispatch video-conference system. He wasn't on the same room, but into a big barrack with a big, green-white-orange flag at his back. _"We received  a video that sounds like_ Caliphate _propaganda from our guys that monitors the Deep Web. One of them felt strange after seeing the video, and another send me it. Didn't saw it and remembered you have some Mastermind powers and could help us on this. Could you have a look on it?"_ 

_"Right. Colonel Carlos, Dispatch is all yours for some minutes, okay?"_ said Jiminy

_"Roger!"_ said Colonel Carlos while Jiminy received the video via a secure, Asimov-encrypted link.

The video started with a kind of ethnical fanfare, and many things written in Arabic. Some of them he understood from the basics on arabic Djanni teached him while he was casted after the Kwazani operation. Things like _Crusaders_, and _Pigs_ and _Heretics_ where showed in the screen. Then he activated the subtitles and noted that the Irish guys did a good job on translating this thing. There was a little man into a turban and traditional Arabic clothes that started to talk.

_"We had success into breaking into the Crusader's hideout called_ Roque Santeiro. _We freed some of our brothers and took 50 girls to provide us new soldiers,_ Mujaheddin _for Allah the Most High, to smite the unbelievers and bring them the words of Muhammad the Prophet (PBUH). But there'll be even more. We'll take all our brothers around the world and will raise our arms in the_ Holy Jihad _against the  Crusaders, to free our brothers in the sub-Saharan countries, to unify the followers of The Only God Allah and shows the world he's the Only One. I thereby urge, in name of the Allah the Compassionate, the Merciful, to all the Muslim, all the brothers part of the_ Ummah _around the world, to act against the Crusaders and join us in the_ Holy Jihad _to show the infidels that Allah is the One, and that the_ Mahdi _is coming to cleanse the world from the evil!"_

Jiminy noticed a kind of compulsion trying to get into his mind, specially in the last part of video, and had to shield his mind against it.

_"Someone else saw this video, Corporal McDaggert?"_ said Jiminy

_"Our translators and some of the intel guys."_ said McDaggert.

_"If you can, quarantine them for a day or two. Take some excuse to see how they act discreetly. This video is laced with some kind of mind-bending compulsion. I felt like he was trying to put me under his will, and the tone he used looked similar of my_ Pretty Please! _power. I think this guy is an unknown Mastermind breakthrough like myself."_ said Jiminy.

_"Alright, I'll do as you said."_ said Corporal McDaggert. _"We'll try to find some intel about this guy too. Looks like he's under the radar by some time."_

_"Alright."_ said Jiminy _"I'll show this for Diana and Lesley as soon as possible. By now, asks the Asimov guys to do their best and block this video. I don't know which could happen people saw this here."_ 

_"Roger, we'll do it ASAP. Out."_ said the Corporal McDaggert disconnecting from the video chat.

Jiminy made a copy of the file into a special flash drive he had,  Vernetech and Asimov encrypted so they could not be used by others, his own DNA and brain waves acting as key to encrypt and decrypt the files. A very new Vernetech trick, but useful.

_"Had you heard our talk, Colonel Carlos?"_ said Jiminy

_"Yeah. Didn't saw this video, but looks like something dangerous. Is this guy that powerful?"_ said Carlos

_"Looks like: Masterminds that have will-bending powers like me and him have some kind of restrictions. As you know, I can't use_ Pretty Please! _to make someone go mean or violent. Looks like this guy have not this restriction. Maybe the message was weak on me as I'm not a Muslim..."_ said Jiminy _"Maybe I'll need to try this with Djanni, albeit this would be risky."_

_"Do you think that this power using could be more effective against Muslims?"_ sound Carlos

_"Maybe... I don't know, however, if it's his power or just his rhetoric... I'll need to see more about this. I made a copy of this one on my special flash drive as evidence if needed."_ said Jiminy _"I took a snapshot on the guy, maybe we should work with intelligence people to find more about him."_

_"Send me this, I'll send for LDS and UN people to work surveillance for extra intel."_ said Colonel Carlos, while Jiminy send the image.

_"Right. Let us focus on the job and then get some rest when the shift goes."_ said Jiminy

---

_"So, you received that video?"_ said Diana

_"The Irish intelligence guys took it from the Deep Web. Looks like it was laced with some kind of will-bending suggestion, like my_ Pretty Please!, _I felt it."_ said Jiminy, when everyone was together at the meeting room _"I need to say, Djanni, that looks like this guy is trying to radicalize Muslims, and not only by rhetorics: looks like his will-bending power is like mine."_

_"Could we test this?"_ said Soldaire

_"This is the problem: looks like his_ Pretty Please!_-like power is focused on people from his own faith. The only way to test it is to see a Muslim reaction for this video."_ said Jiminy _"But I don't want not do this with you, Djanni, if you don't want, because I don't know how strong his will-bending compulsion can be on you. In fact,_ I _felt myself worryingly susceptible to get into his compulsion, and I'm originally a non-follower Catholic and a cape that knows how his power works, this should make me immune."_

_"I have no issues to do this."_ said Djanni _"I've been well educated in the Qu'ran, I can see any_ haram _very fast. And we need to stop this perversion on the Prophet's words (PBUH). Those guys and that_ fatwa _were from the same kind, and I passed through it... I think I can deal with this."_

_"Okay."_ said Dumont. _"But we'll have Kuntur and LionHeart nearby you if needed. We can't be naive enough to believe that this could not affect you. Also, Jiminy, I want you to be ready to_ Pretty Please! _him as strongly as you can if needed."_

_"Alright."_ said Jiminy, putting the flash-drive and playing the video in the meeting room screen. While everyone looked to it, as flabbergasted than he was, Jiminy looked for Djanni, and he saw his face growing angry and angry, until...

He punched the screen with all he could, his face into a strong rage, smashing the TV into smithereens, scraps of it all around the room:

_"Shut the f--k up, you_ murtadd!!!!", screamed Djanni, _"You_ Zindiq!"

"Pretty Please!, _Djanni, calm down yourself!"_ said Jiminy, and Djanni got back his own feet, trembling.

_"Are you okay, Djanni?"_ asked Sugarplum

_"No... It was... Evil! It's_ haram! Haram! Haram! _What this guy is doing is_ haram!" said Djanni _"I just didn't felt under his poisoned honey words because Ozma and Chakra tricks!"_

When in _Metrocon_, _Herós Sans Frontières_ team gave and saw some lectures from other teams. One of those lectures, given by _Chakra_ and _Ozma_, was about psychic invasion, suggestions and hypnotic trances, on how detect and avoid them. The trick was to choose something that could say or sing mentally when suspecting someone was trying to put you into hypnotic suggestion or invasion. Jiminy chosen _When you wish upon a star_ and _Give a little whistle_, from _Pinocchio_, and for Djanni was reciting all the 99 names of Allah and the _Shadahah_, the testimony of _"That there is no god but God, and Muhammad is the messenger of God"_. 

To be so strong that, even using Chakra's tricks, Djanni almost felt into that trance, this guy would have a really strong will-bending power.

_"That's okay, Djanni! You're now with friends._ Pretty Please!, _get calm again."_ said Jiminy, thinking he was needing another _Pretty Please!_ to get back to his normal stance.

Djanni got back to his calm, albeit was shaken.

_"This guy is using the_ Allah'_s gift of power, the breakthrough, to bind the will of others, for hate. I could hear he asking me to hate everyone and everything that was impure under Allah, everyone that wasn't part of the_ Ummah! _To kill the_ Kafir, Shirk, Zindiq, murtadd! _He justify himself as the_ Mahdi _, he treats himself as the_ Mahdi!" said Djanni, shaken, crying while talking _"He's seeing himself as powerful as The Prophet (PBUH)! And I_ almost _fell under his sweet poisoned words!"_

_"Easy, Djanni!"_ said Dumont, while Jiminy was going to give him a little hug.

_"Yeah! It's not your fault! Jiminy was right!"_ confirmed Sugarplum _"Looks like this guy is a_ Mastermind _like Jiminy. Jiminy, how strong he is, in your opinion?"_ 

Normally, heroes with the same set of powers can somehow avail each other based on their own relative strength. So, Ajaxes avail themselves, like Atlases or, in Jiminy Cricket case, Masterminds. With training, a good breakthrough, or even a common person, is able to do a somewhat precise power grading, but the best ratings are done by those with the same power set.

_"B-Class at least... I think, hope I'm wrong, that he can even be an_ Ultra _based on Djanni's reaction. I could say that we should take him as A-Class like me, possibly an_ Ultra, _but fortunately not an_ Omega." said Jiminy, saying, looking over Djanni's shoulder, that needed a friend's hug _"The biggest problem is his_ Do as I said _power is less limited."_

"Do as I've said?" asked Diana

_"All will-bending powers Masterminds have are taken as a single power at_ Barlow's, _called_ Do as I've said." said Jiminy _"My own is called_ Pretty Please! _because this is one of my restrictions: I need to explicitly use the words_ Pretty Please! _before my power engage. And this is important: any_ Do as I've said _power some kind of restriction, limitation, trigger or setback, normally a bunch of them. One of the ways to know how much powerful a Mastermind is puts on accord how many or few of them his_ Do as I've said _power have: the less he has, the more powerful he is. I suspect that this guy doesn't have a trigger word, like me, I'll try to see this again to understand his power better. Djanni, now I need you to remember what you felt while hearing his discourse."_ said Jiminy _"This will help us to understand how this guy's powers works and how powerful he is."_

_"Alright!"_ said Djanni, breathing, trying to focus his mind _"I felt him talking with power and authority, and I could hear something in the back. I can't say if this was subliminal or it was my mind playing tricks with me, but he started to scream_ 'Jihad, Jihad, Jihad!' _all the time, and also_ Kafir, Shirk, Zindiq, murtadd. _This was making me hating everyone non_-Ummah, _non-Muslim, around me. And, when I almost felt into his power, ready to bring death to infidels... I felt myself getting back when I gone into the_ Shadahah, _and noted that he was tricking with my mind. And... I feel myself dirty!"_ said Djanni, almost crying _"Almost attacked you all. I could had killed you, Jiminy, and Sugarplum and Dumont and Soldaire. You were the main targets in my mind when in rage against infidels..."_ he said, crying.

_"It's okay, Djanni. You were out of yourself. I'm sorry."_ said Jiminy, starting to cry together _"It would be my fault if you gone a killer."_

_"No, Jiminy... It's..."_ tried to say Djanni

_"It's no one here's fault."_ pouted Sugarplum _"Those kind of hate guy is the worst kind of people you could see. Those guys of the_ Undying Caliphate, Humanity First, Paladins, NAWB... _There's no talk with them: they took Law of retaliation,_ Lex Talionis, _to their heart. Eye by eye, the world will be blind."_

_"Mahatma Gandhi."_ quoted LionHeart _"This is like those gangs from the Bronx. They looked_ Bronx Shoulder _as a weapon to cleanse the neighborhood from cops and so."_

_"Like the Pure God Work Fraternity."_ said Jiminy

_"At least,"_ said Diana _"you helped us, Djanni. What could you say, Jiminy?"_ 

_"His power is B-Class at least. Sounds like a kind of psychotic breakthrough, because he can induce hate on others via his_ Do as I've said _Mastermind power..."_

_"Call them_ Sword Verse. _I heard it into my mind."_ said Djanni

"Sword verse?" said Jiminy.

"'Then, when the sacred months have passed, slay the idolaters wherever ye find them, and take them captive, and besiege them, and prepare for them each ambush. But if they repent and establish worship and pay the poor-due, then leave their way free. Lo! Allah is Forgiving, Merciful!' _It's the fifth verse of the ninth sura of the Holy Qu'ran. Those who are against the Qu'ran says this is a warn cry against infidels, considering the first part, about sieges, slaying and ambushes... But the next verse is even more important, that made me choose how to use my powers:_ 'if any of the idolaters seeks of thee protection, grant him protection till he hears the words of God; then do thou convey him to his place of security -- that, because they are a people who do not know.' _We are not here to be judges, but helpers, for glory of Allah the Forgiving and Merciful._" said Djanni _"And I think this guy is just looking on the first part of the_ Sword Verse."

_"So,"_ said Dumont _"Sounds as appropriate to call it this way."_

_"Okay..."_ said Jiminy _"So, the now named_ Sword Verse _can induce hate in people, and affects even more Muslims, as it is focused on them. Looks he has no kind of trigger words, like my_ Pretty Please!, _so we need to be cautious when dealing with this guy's videos. Any video from this guy can be laced with his powers: without a trigger word, only a Mastermind could detect it."_

_"Looks coherent, as the original post-Event_ Caliphate _was formed by a guy, Armagan Acar, that sounds like was an Ultra Mastermind. Maybe they somehow were able to recreate those powers, or another Mastermind took his place. This guy could be the one we were trying to find."_ said Diana

_"So, we'll need to deal with this creepypasta?"_ said Sugarplum

_"And soon: LDS and Israel patience is growing small. Some people said that_ Mossad _is already mapping POI targets for nuclear action... And don't say me 'they don't have nukes': it's an open secret that Israel is a nuclear power. Some conspiration theorist even thinks that Israel had nuked himself Tel Aviv to justify an action against the West Bank and Gaza. I don't believe it, but for the ones that associates Israel with ethnical cleansing and straight powermongering, this all make sense: they would only need an excuse to restore_ Eretz Israel _and the_ Caliphate _gave it. There's no saints in politics."_ sighed Diana

_"But how we'll find this guy?"_ said Jiminy

They all heard a siren and the Earbug of all of them ringing. 

_"We have a Code Red situation! This is not a training!"_ said Dispatch _"There's some tangos coming!"_

_"What?"_ said Diana

## Chapter 13

> _"Two attacks in less then a week... Those guys were persistent, and we were in extreme situation. And in extreme situations, real heroic acts happens."_
>
> **Diana Souza, _"Report on Roque Santeiro attacks by the Undying Caliphate"_**

Everyone got into their positions. The new improvement in _SolArmor_, called _Gen3_, have a bigger using time, coming for two hours, although it would be risky to overuse it, and Soldaire donned it as soon they got out the meeting room.

_"Djanni, stay nearby Dispatch!"_ said Soldaire _"Jiminy, on Dispatch. Cajun, out the bench and near Djanni. Sugarplum, Dumont and Kuntur, fly now. Hufflepuff and LionHeart, on me. Everyone armed and ready. Looks like they want to bring us the Foxtrot, so we'll make them dance it!"_

_"Everyone to their positions. Lesley, Corin and Cabby, I want you in the near the Dispatch. Lesley, support on medical teams. Corin, deal with internal security. Cabby, any emergency, you engage the evacuation protocols. I'm going to the field. Carlos, Jiminy, Dispatch is all yours. Situation is a go!"_ said Diana.

_"Jiminy, let me stay with you... I'm not that confident that I was not manipulated."_ said Djanni, fearing something.

_"Okay, Djanni..._ Pretty Please!, _stay with Cajun. Cajun, if you think there's anything weird with Djanni, call me ASAP. But I believe he's not a weak one to get into this guy poisoned words."_ said Jiminy, entering Dispatch

"Oui, Jiminy Criquet!" said Cajun.

Jiminy took away the Top Hat and put the headset and hitted his login information. _"Diana, are you going for field?"_ said Jiminy when he saw Diana information on Dispatch screen.

_"Yeah! Those guys brought the Foxtrot again and this pisses me off. They want to dance the samba? Hope they have the hips!"_ she said, and by the tone Diana looked business.

_"Jiminy, there's a Deep Web live-streaming this thing. And looks that somewhere else is that guy. What should we do?"_ said a Jovert Pienaar, from South African National Defense Force.

_"Put our Asimovs to try and track this guy's IP down... No matter where, he need to connect somewhere in the Internet before get into Deep Web. We'll take the chance and try to find him."_ said Jiminy _"For now, block the IP access for the site, NOW!"_ said Joshua.

_"We're working on it, but looks like there's no way to block that aside a total shutdown."_ said Jovert.

_"Forget it so: internal security, be ready for a Code Red, even a Charlie Foxtrot."_ said Jiminy, while seeing the screens in front of him _"Soldaire, Jiminy on Dispatch: looks they have a big army before you."_

_"You don't say!"_ said Soldaire, trying to reduce the grim situation with a joke. Jiminy knows why: he feared Jiminy's _Bellax Analytica_ could go haywire again and make him useless.

_"Alright, let us see our actual situation!"_ said Jiminy _"They are bigger than us 10 per 1, and they have 4 breakthroughs per 1 of ours, mainly Ajaxes, but you can think on them having all the kind of powers. Also... They are live-streaming all this on Deep Web."_

_"What? Jiminy, you can..."_ said Diana

_"Cut this? Yes... But we need to find that hatemongering guy, and so, it's better to take a risk and find him, putting our Asimovs to search the Deep Web for the streamer."_ said Jiminy

_"Okay, hope you're right."_ said Diana _"Those guys are coming for the barnstormer dance."_

_"They are bringing the Foxtrot. So we'll make them dance!"_ said Jiminy. _"Let's go!"_

They got in front, Soldaire, Hufflepuff, LionHeart and Kuntur at the ground, Dumont and Sugarplum at the air, Sugarplum as a Tink: she was faster in this mode, and she could go and ward almost everyone in Roque Santeiro in moments.

_"Jiminy, what about changing the weather?"_ said Sugarplum

_"No! They have their own Merlins for sure! Better to use the weather control as aggressively you can. Think you ready some tricks before."_ said Jiminy

_"Yeah! Ozma said me we need to be as prepared as possible."_ said Sugarplum _"And already had some ideas."_ 

_"Okay!"_ Jiminy said _"Dumont, Sugarplum, give them our greet!"_ 

_"All the troops! Lock and Load! Let us make them dance the Foxtrot!"_ said Carlos.

_"Dumont, I'll need you for the long range shoot."_ said Jiminy, when the first guys were at fire range _"Fire!!"_

_"Alright! Tesla Shot Barrage!"_ said Dumont.

Lots of small needles, made of a kind of Vernetech aluminum alloy, got from behind Demoiselle and gone to the targets hitting many of them straight, some energy pulses being saw: the electromagnetic pulse doing a powerful recoy and making them gone unconscious and in pain. 

_"Good one, Dumont!"_ said Jiminy _"That cleared some of the tangos! Already designated your targets! All of them looks like Ajaxes, B- or C-Class!"_

_"Now is with us,_ Pepito Grillo!" said Hufflepuff _"It's time for_ LUCHA!" he screamed, running to the biggest tango he could, grappling them into instants and punching it.

_"Now it's time for justice! Twenty-first Century Police Force... SOLDAIRE!"_ said Soldaire, doing a flamboyant sequence of gestures and poses before engaging fight.

LionHeart and Kuntur just shrugged and engaged their designated targets.

Sugarplum recited something and pointed a special wand made of holly she prepared recently to somewhere in the middle of the tangos. A ball of yellow lighting gone to the ground, working like a kind of gigantic concussion grenade, disabling lots of the tangos.

_"Great, Sugarplum!"_ said Jiminy _"Good news: almost no more common tangos on your range, Diana. Bad news: the breakthrough tangos are trying to flank you!"_ he said, looking for the ghost-future images from his _Bellax Analytica_ overlapping his own present time.

_"How many,_ Pepito Grillo?" said Kuntur, after throwing a tango Atlas against other tangos

_"Not too much, but things will grew worse: the rabble already fell, but they are still 3 per 1 and at least B-Classes!"_ said Jiminy, when they heard something via the Earbug.

The _Shadahah_ being played into a way Jiminy recognized easily.

_"Jiminy, I'm not well!"_ said Djanni, desperate _"I feel that hate voice growing on myself!"_

_"Everyone, prepares for a Charlie Foxtrot, a big one!"_ shouted Jiminy _"What about that location, Sargent Pienaar?"_

_"Found a potential place!"_ said the South African intel man _"But they are doing countermeasures: looks like they have their own Asimovs!"_

_"Get out!"_ ordered Jiminy "Code Red _situation: blackout on outside comms! Only Earbug!"_

"Jiminy Criquet, _Djanni is going crazy!"_ said Cajun

_"Hold him, even with Blacklocks if needed! Corin, give Cajun help!"_ shouted Jiminy

_"What is happening?"_ said Diana

_"I know: I'm feeling it... It's a post-hypnotic suggestion! That_ Sword Verse _is a very strong power!"_ replied Jiminy, feeling even more worry as he was into a fight in two fronts, his _Bellax Analytica_ splitting and multiplying and dangerously growing more and more difficult to control.

_"Jiminy, I'll take the battle!"_ shouted Carlos, seeing Jiminy's uneasiness _"Focus on the_ Code Red!"

_"Alright!"_ said Jiminy, getting out the station and running to Djanni

_"He's growing crazy!"_ said Corin. Cajun and Djanni's parents were nearby. Djanni was screaming in pain and hate, while being held by Corin, the only one able to stop Djanni as an A-Class Ajax himself.

"Pretty Please!, _Djanni! This is not you! This is a crazy guy that believes himself the_ Mahdi, _messing your mind!"_ pleaded Jiminy.

Djanni was screaming: _"Murtadd! Murtadd! Haram! Haram! Kafir! Shirk! Zindiq! Murtadd!"_ and looking in rage.

It was when Jiminy looked for him and, crying, slapped Djanni's face: "PRETTY PLEASE!, _STOP IT!!!"_

Djanni looked for Jiminy furious, and tried to advance for him, but it was when someone else slapped him.

It was his father.

_"Stop it! In the name of the Compassionate, the Merciful, the Forgiving, Allah the Most High! No matter what others says: we are not and were never a warmongering faith!"_ commanded Zayn Tahan, while looking his son feeling both _Do As I've said_ powers,  _Pretty Please!_ and _Sword Verses_, fighting inside him _"And if the Most High gave you the power of being stronger than the lion, you should also be peaceful as the sheep and wise as the serpent!"_

Djanni fell crying, but Jiminy noted he was back to his best.

_"Cabby, engage evacuation protocol, but held the run. Corin, take Dispatch in my place. Lesley, help medical support, look like we'll need them."_ shouted Jiminy, trying to hold himself the best he could _"Djanni,_ Pretty Please!, _help me! I REALLY need you now, we need you now, we can't defeat them without you! Remember Ozma and Chakra's tips! I need you to get over this mental control! This guy is trying to take you to their murderous forces, and we need to stop them, to avoid even more suffering!"_ begged Jiminy

_"Son, hear Jiminy Cricket. No matter what this hate guy says, no matter Jiminy being an Infidel, Jiminy is more with Allah the Most High than him. He helped you and was with you and with us from the very beginning. He's your friend above and after all, and he never took you as a terrorist, as you never took him as a Crusader. Now, son, free yourself and release this burden from your mind."_ said Mr. Tahan with a wise voice.

Djanni felt over his knees and praised, still crying. _"Lo! Allah is Forgiving, Merciful!"_ he shouted almost crying, before going into his feet. _"Sorry about all this..."_ 

_"Let us do what we need to and we'll cry together later."_ said Jiminy, cleaning his own tears, while Djanni done the same with his. 

It was when they heard Corin at the Earbug _"We have rioters all around the camp! Some of them took the radio station! Looks like they were sleeper agents!"_

_"They will use it to play a_ Sword Verse-_laced record to radicalize people!"_ said Djanni

_"Let's go!"_ said Jiminy, showing a hook getting out his costume's collar, part of the harnesses he uses in the costume as a way to be flown away by the fliers from the team.

_"Let me go with you!"_ said Squirrel Girl, turning herself into a small meerkat and getting into one of the big pockets front Jiminy's tails.

One of the recent modifications made by Dumont on Jiminy costume included a new hook inside his costume that Djanni could use to take and lift Jiminy, bringing Jiminy to the skies with him.

_"Give us the fast route to the station, quick!"_ said Jiminy

_"Alright!"_ said Corin _"Sending you! The Berets can't hold too much! We can't open fire, but they want to pass over, even by killing the Berets! This can grow into a bloodbath!"_ 

They flew very fast, Jiminy holding his hat and eyes close to avoid problems, when he saw four guys, all of them looking like refugees, with knives and clubs, and the Berets were not able to stop them. Squirrel Girl dropped from Jiminy's pocket, changing from a meerkat to a skunk.

_"Alright, Djanni, let get down! We need to stop them!"_ said Jiminy _"Don't abuse on strength, but go all out with speed!"_

_"Roger!"_ said Djanni, looking a little better and more focused.

When Jiminy and Djanni dropped, Squirrel Girl already sprayed the front guys with her skunk perfume, and  Jiminy used his Jiminy cane to avoid the tangos' attacks and attacking them with his _Jiminy Cane_ in the Taser mode, knocking two of the attackers out. One of them tried to run into the radio, but Djanni put itself between him and the door, and punched him straight in the belly, making him gasp and get down, unconscious.

The last one, unfortunately, got inside, running to the controls. Jiminy used his _Jiminy Cane_ laser and shoot him straight into his hand, which made him yelp, while Squirrel Girl got and sprayed also him. Djanni got nearby when Jiminy looked his _Bellax Analytica_!

_"Suicide Bomber!"_ shouted Jiminy, and he jumped, ignoring Squirrel Girl's skunk perfume, hitting his _Jiminy Cane_ straight in the man's right hand, before the man could activate the explosives. Then, he attacked the man with the Taser in maximum strength, hitting him in the hand and making him feel the spasms on all his body, the lethal contacts from the bomb away from each other.

Djanni looked and wooed: _"Wow... Jiminy, you saved our lives!"_ 

Jiminy got in a corner, almost trembling, when the Berets came and removed the guy carefully.

_"Things are still escalating!"_ said Corin in the Dispatch _"We need to stop this craziness!"_

_"But how?"_ said Jiminy, exhausted

Djanni looked for him

_"We'll do what they want, but quite the contrary!"_ said Djanni

_"What?!"_ asked Jiminy

_"Let us send some message of peace, lacing it with your_ Pretty Please! _C'mon Jiminy, I lost the account on how much I saw you doing this!"_ said Djanni

_"I don't know if I could this... Never tried to do this using radio transmission!"_ exasperated Jiminy

_"If that guy can do the_ Sword Verse _this way, you can do the_ Pretty Please!. _I saw you doing this using megaphones, FM radio is more or less the same."_ said Djanni

_"Alright, but I need_ the words: Pretty Please! _will not work without them. That guy power is focused on raising hate based on ill-directed, misguided Qu'ran quoting. I don't have any word to counter this, so I can't stop everyone more than I could stop_ Humanity First _to put my house down!"_ snapped Jiminy, crying, feeling the pressure.

Djanni came and look to him, serious.

_"Look at me. I'm of the_ Ummah! _I can do the words. I need you to use your powers to pass those words through it, convincing them we are right, reinforcing our message. We can't be defeated by this guy, he's only one delusional guy. We're two, and Allah the Most High, the Compassionated, Forgiving and Merciful One will be with us."_ said Djanni, taking a little green book from the pocket of his Simbad-like waistcoat. Jiminy knows it was the _Holy Qu'ran_. _"We'll show them what_ Jihad _really is!"_ he said, taking a little notepad and pen and starting to write his own message, while Jiminy and Squirrel Girl turned on all the FM instruments.

_"Jiminy, Soldaire here! We've pushed the wannabe invaders, but still have the local_ Code Red _situation. If you can do something, for all that is Sacred, do it NOW! We are with problems to do the crowd control, this will go very fast into a for-real riot!"_ said Soldaire

Djanni finished to scribe the note, and then Joshua took it and sat in the speaker chair, placing the headphone, locked into the sound proof room, while Djanni and Squirrel Girl, back into his common human shape, except by rabbit ears and haunched legs and skunk's tail, got to the sound table. Jiminy breathed longly, putting himself as calm as he could, concentrating himself on what he need to do, and Djanni did a countdown with his fingers beyond the soundproof room, when a light with _ON_ on it lighted up.

_"Everyone hearing this,_ Pretty Please!, _stop the fight!"_ started Jiminy, knowing that his powers depends on the voice and tone and words, adjusting those correctly _"You are being misled. You are being used by someone whose ambitions are_ haram, _are against the Qu'ran, and that would only hurt the_ Ummah, _the People of Islam. In the sixth verse of the ninth sura, Mohammad the Prophet (PBUH) said:_ 'if any of the idolaters seeks of thee protection, grant him protection till he hears the words of God; then do thou convey him to his place of security -- that, because they are a people who do not know.' _You are all in the same place: you were forgotten by people that ignored Allah the Most High, Mohammad the Prophet (PBUH), and Jesus, or Isa, Peace Be with Him. They are the ultimate idolaters, because they know, but seek and idolize power, not  Allah's justice. They are the ultimate unbelievers, because they know, but they doesn't follow Mohammad's words. They are the ultimate aposthaste, because they know, but distorts the High One's words, revealed to the Prophet (PBUH) by Gibril, to their own means. They treat you like dirt. Stop your fight now and hear the words of Allah the Most High, as revealed by Mohammad the Prophet (PBUH)"_ said Jiminy, while he started to say the quotes from the Qu'ran chosen by Djanni.

_"Incredible!"_ said Cabby, looking to the people around stopping the fight.

"Sacre Bleu!" said Cajun via the Earbug _"You're doing a miracle, you two!"_

_"Dispatch! Here's security! There's almost no more fight!"_ said someone _"Some focused brawling, but the lion's share just stopped, those staying on fight being well-known troublemakers."_

_"They are getting away! We're doing it!"_ said Djanni

All those came to Jiminy thanks the sound monitor linked to his Earbug, but Jiminy thought that this was not enough.

_"Now,"_ said Jiminy _"This is the words from Isa, Peace be with him, Jesus, as said by Him into the Mount of Galilee."_ Djanni looked a little surprised, but Jiminy started _"This is me, Jiminy Cricket, for everyone at_ Roque Santeiro, _saying that hate should not have space here. We're all suffering, we're all in the same boat, no matter each one's origin or creed."_

_"Blessed are the meek: for they will inherit the earth. Blessed are those who hunger and thirst for righteousness: for they will be filled. Blessed are the merciful: for they will be shown mercy. Blessed are the pure in heart: for they will see God. Blessed are the peacemakers: for they will be called children of God."_ said Jiminy Cricket, calm and suave.

_"Jiminy, Corin here! They stopped! Almost no one is fighting now! They're stopping!"_ shouted Corin via Earbug, which made them cry.

_"Lo! Allah is Forgiving, Merciful!"_ praised Djanni happily _"We did it! We did it! We stopped the_ Sword Verse!"

They looked to Jiminy, that wasn't well, almost falling in the chair

_"Jiminy?"_ said Djanni

_"Just... Exhausted... Need to... Rest..."_ whispered Jiminy, trying to get out the chair, but almost falling when trying to stand on his own feet.

Squirrel Girl turned back to his normal squirrelly girl fashion and said: _"Now, Jiminy, rest a little!"_

_"No time for..."_ said Jiminy, trying to get over his feet.

_"Jiminy, here's Dispatch. The tangos retreated. We have some casualties, but no kills under our side. LionHeart had the worst, but he's just with a dislocated arm, not too much for an Ajax."_

_"Now, Jiminy. Take some sleep. You and Djanni did very well."_ said Diana

_"And we have some intel about this guy!"_ said Corporal McDaggert _"The Asimov guys did their drill in the meantime and took some intel about them. The tangos disconnected and maybe will try to move away, but we had them locked into target!"_

Jiminy gave a little smile and nodded off, Squirrel Girl carrying him, her tail as a pillow for him, while they got out of the radio.

## Chapter 14

> _"One the things I like the most in our_ CAI _teams at_ Herós Sans Frontières _is: we have people from all origins. In fact, my team is basically a mishmash of people from all around the world: a Brazilian Dandy, a convicted under parole from Bronx, a ballerina from North Carolina, a Japanese super-armored hero, a_ Tecnico Luchador _from Mexico, a flying condor from Bolivia, a flamboyant swordman from Quebec, and two kids, a Tunisian Refugee and the messy Mastermind from Newark that wrote those words. Mixing so much kinds of breakthroughs, powers, skills, and origins, we could do things that others couldn't. As we did in the_ Sword Verse _attacks' aftermath."_
>
>  **Jiminy Cricket, _"From Dress-up to save up: an underage breakthrough story"_**

_"So..."_ said Jiminy

_"People are still into a terrible mood."_ said Diana _"Two attacks in less than a week... There's lots of people going to other refugee camps, like Dadaab or Kakuma, feeling that Roque Santeiro is no more safe. And I can't blame them, to be fair: they fear that those post-hypnotic thing from_ Sword Verse _could get back, even the Muslims are fearing to do the wrong prayer and trigger other post-hypnotic triggers and gone amok trying to kill people, even on their own."_

_"Understand..."_ said Jiminy _"So, you want me to do this job with the_ Clowns Without Borders _guys."_

_"Yeah!"_ said Diana _"I think that we can use this to do some covert surveillance: as far I understood,_ Pretty Please! _and_ Sword Verse _are similar and opposite powers, that_ Do as I said _thing. So, I think you can detect those that could be under a post-hypnotic trance, or at least those that could react weird against your_ Pretty Please! _And by doing this while being_ Spotty the Clown, _you could take people unguarded: they would get themselves guarded against a cape like_ Jiminy Cricket, _but a little clown like_ Spotty _could be seen as inoffensive and so people could let their guard down, even those with the triggers. Besides, after all that happened, I think that everyone needs a laugh: we don't need Boggarts or Dementors here, and we don't have a werewolf Defense Against Dark Arts teacher to help, so we'll do this with a little clown."_ she said, smiling

Jiminy smiled... He though on who could do this and who could help him, besides the _Clown Without Borders_ guys.

He talked with his team. Sugarplum, Dumont and LionHeart proposed to help him with other things: Dumont said he could do some limelights, while Sugarplum could use some little magic sprinkles to make everything more beautiful and LionHeart could go for some juggling show or other things. Hufflepuff said him to do a makeshift _lucha_ show: everyone knows that some of _lucha_ fights are scened, but there was always _Kayfabe_, or _La Passion por La Lucha_ as he said. By doing this, Hufflepuff could use LionHeart's old supervillain alter-ego, _The Bronx Shoulder_, and do something nice and fun. 

Djanni accepted the role of Jiminy's, or better, _Spotty_'s  clown partner: Mrs. Tahan made his clown costume, as the Tahans and McCarthys got back for Geneva after the second attack against _Roque Santeiro_ for security reasons. She would send him from Geneva in the next cargo freight for _Roque Santeiro_, while he would share some of the makeup Jiminy brought from the clown camp. To avoid the language issue, they would adapt some of the skits and do physical comedy skits.

_"But which ones we could do?"_ said Djanni

_"We can see some skits at ViewTube and see if they are funny and there's also the skit book I gained at clown camp."_ said Jiminy, opening the book and his notebook to see the ViewTube videos _"We have 15 days to do the show. And to do what Diana asked us."_

_"Alright!"_ said Djanni.

They rehearsed all the free time they had, and they had a fundamental support: Brick-A-Brack and Mimi helped them while rehearsing and designing Djanni's clown face via video-conference.

And Jiminy found a good way to detect people that could be at the post-hypnotic suggestion from _Sword Verse_:

_"Djanni, we'll go and I'll use my_ Pretty Please! _in the middle of the parade to detect people that could be under a post-hypnotic trance like you were, then we'll indicate them for the others. The idea is that they'll be sent for double-check to Squirrel Girl. She would remove the triggers from those that would be under that suggestion."_ said Jiminy, after one of their rehearsals. 

_"Great! That thing was horrible... Need to say, I never felt so much rage and hate when that time."_ said Djanni

_"I think we can find people that are consciously working for that_ Sword Verse _guy. I said Diana to publicly publish our report, specially my analysis on his power. This guy is full on hubris, he would make people try to find me. And if they did their job really good, five minutes of Net-Fu and they'll discover that Joshua McCarthy **IS** Jiminy Cricket **IS** Spotty the Clown."_ said Jiminy

_"But you'll be at risk!"_ said Djanni

_"As by the Go proverb:_ 'Your life point is enemy's one' _If we don't show them we are after any intel on them, they'll not be feel endangered. If they feel that way, they'll sooner or later make mistakes. And on those mistakes we'll gather more intel on them. We'll play_ sente, _with initiative and pro-activity, and they'll play_ gote, _just doing what we are expecting them to."_ said Jiminy, getting ready for next skit rehearsal.

They did their best and, in the last rehearsal, they showed the full show for Brick-a-Brack and Mimi, and they approved the skits, including the finishing one, _Dead and Alive!_

_"This is_ very _classic, and Joshua, as you'll be the 'dead', Altayr will not have problems on moving and supporting you, as he's an Atlas. Altayr you'll just need to remember the timing for all things."_ said Michelle.

_"And remember to play silly and_ play safe." said Tammy.

_"Alright! Thank you! Tomorrow will be the day."_ said Joshua. _"They'll show this live on ViewTube. Diana thinks that this could be good PR for_ Herós Sans Frontières, _to show we are people as everyone, except by the powers."_

_"Sounds like a nice idea."_ said Tammy _"Now, go take some sleep. Remember: clowns needs to be on their best to make people laugh."_

_"Right. Thanks for everything. Out."_ said Jiminy, finishing the talk.

Djanni and Jiminy woke up early next day and, after a good breakfast, they gone for a barrack that was near some of the _Clowns without Borders_ people where they left all the things from them as clowns, _Spotty_ and _Almuhraj_. They dressed on their clothes and made their makeup, when LionHeart and Sugarplum came in.

_"Dumont said everything is ready for the stream. Are you ready?"_ said Sugarplum, into his best ballerina attire, just a small V-line in the back so she could open her wings if needed.

_"We are. Are you Almuhraj?"_ said Jiminy, or better, Spotty.

_"Ready! Bump a nose, Spotty!"_ said Djanni, or better, Almuhraj.

_"Diana said us to assure you you would be under Earbugs: we never know someone could try any weird thing."_ said LionHeart, in his old-timey Jacket and black mask as _The Bronx Shoulder_.

_"Okay."_ said Spotty, putting the Earbug.

_"Okay! Let's go! Bump a nose you two!"_ said Sugarplum, that knew that _Bump a Nose_ was the clown's _Break a leg_

Spotty and Almuhraj took their Kazoos, high-fived for luck and support, and gone into a small car, while Sugarplum put a little charm on them before Cabby started to drive it all around _Roque Santeiro_, Spotty and Almuhraj playing silly renditions of musics and calling people for the show.

"Pretty Please!, _come to 'Roque Santeiro together' show, it will be very funny!_ S'il vous plaît!, _venez à 'Roque Santeiro ensemble', Ça va être très amusant!_ _Jamilat_ min fidlak!, _watati 'iilaa 'ruk santiru maea' almaerid, wasawf yakun madhakaan jiddaan!"_ said both Spotty and Almuhraj all around the camp. It was Almuhraj, or better, _Djanni's_ idea: Jiminy never tried his _Pretty Please!_ in other languages. Maybe, by using the same trigger in other languages, he could lace people easily into his _Pretty Please!_

And Diana's and Djanni's ideas paid profits: here and there Spotty, or better, Jiminy, saw some people that didn't fell in the _Pretty Please!_ or react weirdly for it. He just did a little weird gesture to them: this could pass as part of the zaniness, but, in fact, was a pointer for Sugarplum amd the others show, via Mask-cam, the affected people. 

Squirrel Girl, Cajun, Corin and Dr. Derek Stiles, a small and timid doctor from CADUCEUS allocated at _Roque Santeiro_, then gone to them and discreetly asked some questions, to find if any of them was under the _Sword Verse_ post-hypnotic suggestion. They could take whose to Squirrel Girl remove their triggers.

When they arrived to the big show stage, Spotty and Almuhraj had gone for the backstage: there were already some musical shows, including ethnic music from some Leonean people, a show from some of the Leonean that played pop music, and other shows into a big festival, the so-called _'Roque Santeiro Together'_ show.

To start Dumont did his _Steam and Steel Show_, a kind of steampunk stage magic and performance show full of lights and things moving at the sound of _Carmina Burana_ and _TRON_ soundtrack. Then came a ballet presentation with Sugarplum, doing the _Sugarplum Fairy Dance_ from _Nutcracker_ ballet with some of the refugees that were ballerinas or had interest on dancing. 

And it was followed by the _Lucha_ challenge between Hufflepuff and _The Bronx Shoulder_, a big _Rudo_ that was, in fact, LionHeart under his old mask (all approved by his parole officer). They simulated a _Lucha de Apuestas_, but it was done into a way to make _Bronx Shoulder_ lose, having his mask ripped apart by Hufflepuff, after that LionHeart talked about himself (as people knew about him), and how there was always a way out of crime. 

By the end, Soldaire, into a very beautiful ringmaster-like costume said:

_"And now!"_ Soldaire said, and some local translators did the real-time translation in French, Arabic and some local dialects. _"To finish our_ 'Roque Santeiro together' _show, a small comedy show with two little clowns from our heroes roster. Ladies and Gentlemen, boys and girls, children of all ages, give a big cheer up for_ Clowns without Borders _and our little clowns_ Spotty _and_ Almuhraj!"

The people gave the cheer and all the clowns got there, and the zaniness started. Skit after skit they made the public roar in laughter, until came the last one, _Dead and Alive!_

They did all the intro, including the moment that Almuhraj gave a fake slap in Spotty (with all the safety precautions) and all the moments with Spotty 'dead', being moved and supported by Almuhraj, until, when they were in the last move, where Almuhraj took Spotty and held him over his shoulders, Spotty, or better, Jiminy, while making a zany face and waving for the crowd, saw something with his _Bellax Analytica_.

A guy with a weapon, a small one, that could kill him, even don't making more than a scratch at Djanni!

But they had thought all the time on this possibility during the rehearsals:

> _"If someone could attack us, we need to provide some codes that could indicate danger is coming, using them the presentation without changing things. When I'm in the 'board' moment, I could gave a little extra push into your legs... When you lift me, I could discreetly tap your side and so on. And you also could provide some codes like that, so we could be safe from any_ Sword Verse-_induced potential tango."_

Jiminy just gave the taps into Djanni's side at the right time: when the guy shoot the gun, Djanni turned out and, using his Atlas' reflexes, took the bullet into his hand, that felt no more than a wasp sting, and discreetly pointed to the man. Joshua felt something and he discreetly said via Earbug.

_"Suicide Bomber. Seize him!"_ said him. The guy was with his hands on the detonator, but Cajun was smarter and faster: when he saw the guy trying to ignite the bomb, he had gone and cut the detonator wire and one of the man's hands in the process. Everyone screamed, but the action was luckily timely: two Speedster came and took the guy away from the public.

_"Let us finish the show!"_ said Jiminy discreetly for Djanni _"Plan B ending!"_

Djanni put Jiminy at the floor and both of them bowed to the crowd, but then they looked at each other, Jiminy faking fear, Djanni faking anger, and they run out of the stage with people roaring out laughing!

_"Jiminy, are you two okay?"_ said Diana, via Earbug, while Djanni cleaned some of the metal chips from his hand left by the destroyed bullet.

_"We're okay. Djanni just like if a wasp stung him when stopping the bullet. And the guy?"_ said Jiminy

_"We are profiling him now, but he's a recent arrive."_ said Diana _"Didn't looked like someone that could be a suicide bomber. Two options: sleeper agent or someone under a very powerful_ Sword Verse _post-hypnotic trance."_

_"Alright: any other intel? And the people Jiminy pointed?"_ said Djanni

_"All under the_ Sword Verse, _but already put under treatment"_ said Diana _"Now, go and take the greeting from the crowd: you deserve it more than they and you know."_ 

Hufflepuff and LionHeart took them over their shoulder and brought them to the stage, and everyone that was in the show looked for Soldaire that chose for some reason to sing _Heal the World_ from Michael Jackson as the last song, but with a slight touch of _Enka_ music, he dressed into a stylish _yukata_, with all the others going as a choir for him. In the end, all the people in the crowd was singing with them, and Dumont's fireworks blasted over _Roque Santeiro_, finishing a very important show, and action.

_"Hope people learn on this all and help each other after this show."_ said Jiminy in the backstage, while everyone was getting there,  cheering and greeting each other, taking a little bottle of water and, after taking a gulp, passing it to Djanni 

_"Sounds like they'll did. They loved the show and I think they understood the message..."_ said Djanni when Sugarplum came, taking her cellphone from the ballerina tutu.

_"Kids, look to this!"_ said her, showing some video reactions and hashtags from social networks. _#roquesanteiro, #healtheworld, #spotty_ and _#Almuhraj_ were all in the trending topics, but the best was see the video reactions from their parents (that were in Geneva) and from Mimi and Brick-a-Brack (that recorded on clown in a very zany way) praising the quality of their show.

_"Need to say that were also the kind of crap that creepy people say those times, but they were not even .1% of all the reactions. People all around the world loved it! And look!_ Rolling Stones _said this is the_ Post-Event Live-Aid, with breakthroughs instead of musicians! _We brought the house down!"_ said Sugarplum

_"Indeed, you made the donation site goes slashdotted!"_ said Diana, looking happy _"They needed to upgrade the hosting service to deal with the donations overload!_ Roque Santeiro _will be easily reconstructed will all the extra donations_ Herós Sans Frontières _and_ Mediciens Sans Frontières _will receive!"_

_"Great!"_ said Jiminy while Djanni gone for _"Lo! Allah is Forgiving, Merciful!"_ 

_"And, extra good news: we now know who is this_ Sword Verse _guy. He sent a challenge for us full of hubris and we gathered enough intel about him to profile him, and gather even more intel!"_ said Diana, showing she was business.

_"So... Challenge Accepted!"_ said Dumont, doing a meme-like face.


## Chapter 15

> _"The_ Kafir, _the infidel Crusaders, mocked our ways and disrespected us, and the_ Zindiq _and_ murtadd, _the hereticals and aposthate, turned their back for the_ Ummah, _working with Shayaten and Iblis to bring the profanity in front of Allah the Most High. By talking about 'tolerance' and 'respect', the Crusaders only repeat the same heresies and hypocrisies they professed since Adam (Peace with Him). They can say sweet words, but only dung insects could be seen beyond them. We will accept this no more. In the name of Allah the Compassionate and Merciful, I hereby urge the_ Ummah, _the people of Islam, against the Crusaders from that profanity den called_ 'Roque Santeiro' _passing those that doesn't recognize the time of the_ Mahdi _is coming under the sword! We are the ones sent to cleanse the harvest for the time of the_ Mahdi _and the fall of the_ Kafir. _Those who joins the Holy_ Jihad _will be the ones to join The Prophet (PBUH) at the_ Firdaws. _Those who stay with the Crusaders will have less mercy under Allah than the inhabitants of Sodom and Gomorrah. We are the new_ Seif-al-Din, _the Blades of the Faith, to provoke more pain and hurt to the infidels and glory for Allah the Merciful. I'm the_ Sword Verses, Alsyf Alayat, _to urge the Ummah!"_
>
>  **From video posted on the Internet by the supervillain _Alsyf Alayat (Sword Verses)_, report from _Herós Sans Frontières_ (translation and comments: Zayn Tahan)**
>
> _"There's no much effort to see he's in_ kibria'_, in Hubris: by urging the_ Sword Verse, _he is showing himself as over the_ Mahdi _and all the Prophets from the past, from Adam to Isa, or Jesus (Peace with Them), to Mohammad (PBUH), saying what everyone in_ Ummah _should do. We were never in past so cruel even with ours. If we are that cruel with our ones, how could us ask for peace and understanding from and with the infidels? Converting the infidel to Allah the Most High and show him the way of Peace after surrending yourself under Allah the Forgiving's voice should be the real reason of our lives, the real way to Praise the Most High, to do what is pleasing for the Most High. Otherwise, even the infidels would be better than us, because they can and would show mercy under Allah the Most High's eyes."_
>
> **Zayn Tahan, _'On the_ Sword Verses (Alsyf Alayat)'s fatwa _and his words under the Holy Qu'ran'_, Herós Sans Frontières.**

They were all flabbergasted after looking the video.

_"So it is this? He wants to bring war against us?"_ said LionHeart

_"This kind of guy is the worst."_ said Sugarplum, disgusted _"It's not different from what KKK and_ Humanity First _does. There's no talk here, even with those 'like them': my father gave some alert shots against some guys that gone against us in Jiminy's proccess afterwards, including a neighbor that was a_ Humanity First_er. He said them next time they tried to trespass our home, he would shot them to drop dead."_

_"And that's exactly is the problem:"_ said Jiminy _"They are crazy, and they want to make people grow resentful against those they see as impure. It's like Kyle, it's like_ Humanity First, _it's like KKK, One Land... They are people full of hate and/or that can profit on all this! I don't think we can show them their errors and misunderstandings."_

_"No chance:"_ said Diana _"If those were one of those cheesy pre-Event comics, they would be now_ 'bwahahahahaha' _and we would being_ SOCK BAMF BIFF WHACK _each other now."_

_"Problem is:"_ said Zayn, via video-conference, from Geneva _"the Islamic people suffered a lot in the past. I'll not judge, but Occident forgot the Islamic people for too much time before and after the Event: this gave rise for nuts like_ Seif-al-Din _and_ Alsyf Alayat, _the Sword Verses. However, they aren't innocent also: they are deluded, in the best case, or warmongers, in the worst. They like violence and pain, and blood and cries are their food and music."_ 

_"Jiminy, this video was laced?"_ said Dumont, worried _"The last thing we need now is another surge of crazy people."_

_"Doesn't look like."_ said Jiminy, shaking his head _"I think this is one of his flaws: he can induce hate, but not when himself is full of it. He need to convince himself and other he's doing 'the right thing', otherwise his_ Do as I said _power malfunctions. It's somehow like my own_ Pretty Please!, _but reversed: I can't induce hate on others, even if I want. He can, but only if he's not himself full of it."_

_"But how he can do this?"_ said Djanni _"Islam is not about violence."_

_"Some people disagree with you, Djanni."_ said Kuntur _"Looks like this guy need to bring the idea of 'righteous violence' and 'vengeance' as a way of Justice, like in the Egypt Plagues and so, to make people hateful. And looks like his interpretation of Islam is all about violence. He's like KKK or_ Humanity First, _that cherry-picks what they want, either in the Bible or in the Qu'ran to justify their nonsense."_

_"There's another problem:"_ says Zayn _"Democracy, self-empowerment and individualism, for good or ill, are very occidental concepts. The oriental  religions and philosophies, like Jews, Muslim and those from Far East, are more focused in the community. Look how Japan deals with their breakthroughs, with compulsory indoctrination and training. No offenses, Soldaire."_

_"Apology accepted. But you're right."_ said Soldaire _"I had a strong, albeit respectful, debate about this topic with_ the Protectors _and_ the Heroes _during Metrocon: the  HSF volunteers from Japan are mostly_ ronin, _unregistered breakthroughs and the nearest thing we have in Japan similar with the western concept of villain, and we still have problems to bring the best ones to us, as they normally go for_ Yakuza. _I said Japan should contribute with more capes, or_ powers, _as we call capes in Japan, to HSF, to show our force for peace. But then they said me they live for the country, that they, and the most into the Japanese powers, even putting the_ ronin _into the account, are focused into defending Japan against potential menaces. Only a few_ ronin, _like the deceased_ Three Remarkable Ronin _and_ Kitsune, _are okay on working overseas. To be fair, I'm an exception into exceptions, being a registered Japanese power that works overseas."_

_"So, outside the Occident, the community's value is bigger than the person's. This could be a good way to achieve consensus, but they can also be a trap for fascism and other philosophies that suffocate the individual. And normally is used by those that thinks that their way is the best for everyone: many people goes into herd behavior very easily..."_ said Zayn _"And, in such a situation, people like_ Alsyf Alayat _goes and flourishes."_ said Zayn, a snapshot from the guy into another video in the screen. _"See this video: I found it a little after the_ challenge video _was posted: a_ mullah _friend of mine send this for me after the recent declaration against_ Roque Santeiro _by Alsyf Alayat. I've already subtitled it and send it to everyone I thought that could need this knowledge. This, however, is still being treated as classified into HSF."_

The video started: the first thing Jiminy noted was he was using a kind of veil over his face, like Djanni did, but thicker than Djanni's, so he could see no facial traces, except by the eyes, full of hate and fanaticism.

> _"I've heard about the_ Zindiq _that doesn't accept my words under Allah the Most High. I will say it again: I'm_ Alsyf Alayat, _Sword Verses, to bring Allah the Most High's Justice to the_ Kafir _and_ Zindiq. _I've seen the eyes of Allah, my face transfigured like Musa and Isa, and like Mohammad the Prophet (PBUH) after being visited by Gibril. I can't show my face anymore, because I had seen the Glory of Allah the Most High: my face is anathema for those that is not from the_ Ummah. _My look is enough to burn the_ Kafir _and_ Zindiq_ to ashes straight to_ Jahannan, _for glory of Allah the Most High! Lo! Allah is Forgiving, Merciful!"_ 

Djanni was really angry.

_"This is the biggest_ murtadd _I've ever saw."_ spat Djanni, disgusted

_"He could does like he said? Burn others by his own look?"_ gulped Jiminy, scared

_"I don't think so."_ assured Squirrel Girl _"There were similar events registered, but only with some_ Ultra _or_ Omega _guys, and you can count in a hand's fingers how many of them could_ actually _burn people just by their looks. About_ mentally _do this, it's another story, as there's reports about Psychotic Breakthroughs so horrid that shattered people's minds, what we call_ Cthulhu events. _Fortunately, they are very very rare also, albeit many times more common than what this nut says he could do."_

_"I can say for sure that this video is not laced."_ reassured Jiminy _"It's safe, beside the hate speech."_

_"The biggest problem is: people listen to him."_ said Zayn _"The_ Caliphate War _scars hadn't healed yet: we can see this on_ Roque Santeiro. _And, to turn things worse, Israel is not giving a word about the_ Undying Caliphate _rise in the sub-Saharan Africa, beside the_ 'Caliphate is terrorist' _thing. But we all know  how this will end in the long run, and it will not_ Israel _or the_ Caliphate _that will suffer. It will be us all."_

_"Alright."_ said Hufflepuff _"So, our only option is to deal with this_ cabrón. _And what we know about him? Any intel?"_ 

_"We had already gathered some intel about him, but in too many places, so much scattered and with so many security levels we needed to exert some influence, cash some debts and asks some favors to compile enough intel to create a profile: looks like our 'friends' in intelligence community are trying to profit on this mess. Anyway, we had gather enough intel to build a profile already downloaded into your epads."_ said Diana, activating the vernetech holoprojector, showing some pictures.

_"This is_ Rahul Musa Al-Gazzawi, _now 36, at the time 18, an intel operative trained by CIA as a double-agent for_ Mossad _into the_ Palestinian Liberty Organization. _He worked during the_ Second Intifada _until The Event, when he was taken as KIA, Killed In Action, until the beginning of the_ Caliphate War." said Diana, showing some photos and videos from the guy. Albeit younger, everyone could recognized him as the _Alsyf Alayat_, the Sword Verses.

_"In the beginning of the_ Caliphate War, _he was acting under the name_ Mufti Musra Hussein Al-Gazzawi. _I know, very_ Cliché. _He was a freelancer intel seller, using his contacts to buy and sell some juicy intel that leaked from the Chinese and North Korean shambles during the Event aftermath for both sides in the War. Until them, he was took as just a common guy, with lots of training, money and contacts, but otherwise normal."_

_"During the_ Caliphate War, _a black op was ordered against a Caliphate skunkwork, a kind of hedgy loosened think-tank, to enhance the chances of breakthrough on Breakthrough Camps. Looks like the project was coordinated by some of the Caliphate's_ big kahunas, _trying to get an edge on this Cold War involving mass breakthrough."_ said Diana _"The action happened at Lebanon, as some of the main guys were from Hezbollah."_

_"The operation had three big objectives: acquire and/or destroy the research data, acquire the less radicalized elements, neutralize the others. The action was very successful, as many of the researchers defected to LDS side. And our man was in the ones acquired. He joined the_ Caliphate _ranks to do his double-crossing."_ said Diana

_"So."_ said Dumont _"He escaped death. Again."_

_"Yup. But not too much time after, he defected US again, getting again into the old_ Caliphate _region via Kosovo and Kurdistan. After that, until now, LDS intel lost his scent, until some time before the_ Roque Santeiro _attack."_

_"Could he be the big boss into the_ Undying Caliphate?" asked Djanni

_"Not the big, but certainly one of the biggest ones: Kurdistan intel operatives discovered that he undergone breakthrough in Kurdistan, when a black op to neutralize him gone to the dogs, five Mariners dead by a local mob, not before killing dozens of them. It was the biggest post-Event military fiasco and the biggest military fumble from US since Operation Eagle Claw."_ said Diana

_"Sounds like a Slash movie version of my own breakthrough! He felt menaced and thought on revenge against those who were pursuing him, and he thought on how to manipulate people to do this. And maybe he thought into the_ Sword Verse _and this triggered his breakthrough. So much hate and anger turned it into a psychotic breakthrough, with him dominating the mob to go against the Mariners. They had undergone a so strong_ Do as I said _that they just thrown their own safety to the dogs."_ said Jiminy, shivering _"That was very terrible."_

_"You're right, Jiminy. The only survivor was also under the_ Sword Verses, _and he killed himself just after reporting back. It was a message from_ Alsyf Alayat _to his old friends to not mess with him."_ said Diana

_"The good news is,"_ said Jiminy _"I don't think his powers are still that great. We all know breakthroughs has an immediate power surge, before he can fine control his powers. I didn't had this kind of thing because I'd gone comatose before."_

_"And we know where he is?"_ said Dumont

_"This is the main problem: he is hidden very well, like all the main_ Undying Caliphate _guys."_ said Diana _"But I think we need something to bring him to a trap."_

_"Or... Wait until his next try."_ said Jiminy _"I don't think they'll stop by now. The time they gave us is that perhaps they are themselves under personel shortage: think we had done some good show."_

_"But looks like the next one will be the worst."_ said Soldaire _"They'll certainly have help and will try to put us into shambles. All that_ Sodom and Gomorrah _language shows their intention: he wants to go full war against_ Roque Santeiro. _So we can't wait him take the initiative and attack us."_

_"So, we'll need to investigate his last stop."_ said Jiminy

_"As far we know, he was found last time in somewhere at Cote D'Ivore, Ivory Coast, but we can think he didn't gone that far. From what we profiled, he's not the main brain in the_ Caliphate, _but he's someone that like to be in the front-line, even being risky: as an old time intelligence worker, we can assume he has at least basic military training, and can defend himself from breakthroughs weaker than a C-Class combatant ones or even more."_ stated Corin

_"Yeah:"_ said Diana _"He was trained as a double agent and infiltrator, and also had some training as a saboteur. It's also sensible to believe he can deal with breakthroughs. The good news is: as far the intel collected shows, he doesn't have a_ Bellax Analytica-_like power. So, his strategies are based only on his field training and experience, not into precognition."_

_"I think he can be somewhere in Guinea, Mauritania, or Liberia, as the nearest countries, by the moves the_ Caliphate _did."_ said Jiminy

_"From those contries, Mauritania is the biggest sympathizer with the_ Caliphate. _Liberia should be discarded, as a Christian Majority country, so hostile to the_ Caliphate, _although can be more accepted at the north, in Lofa County, near Guinea. And Guinea is a secular country, so maybe hostile to the_ Caliphate. _It's sensible, however, to think that he can be somewhere farther as his base and crosses those countries when needed."_ said Diana.

_"Now, we need to find him... And monitoring the videos on Deep Web. I think he's publishing more and more of those videos: if he laces them with_ Sword Verse, _we can see a spree of violence not only here, but everywhere."_ said Jiminy

And it was as Jiminy previewed: some seconds after Zayn showed himself in the main screen.

_"There's a riot report at Tripoli."_ he said _"And the_ Caliphate _is claiming responsability on it. I just received a video from a friend of mine. It's again_ Alsyf Alayat"

They saw the video: things were so hasty that Djanni had to do the translation job, even some of them, like Diana, Jiminy and Squirrel Girl knowing the basics.

> _"Lo! The_ Ummah, _the People of Islam, is raising his blade and joining the Holy_ Jihad _against the_ Kafir _and the_ Zindiq!  Tarabulus al-Sham _will soon be cleansed from the stench of apostasy and blasphemy against Allah the Most High! Our Brothers and Sisters from the_ Undying Caliphate _are insurging against the infidel's slavery! Down with the Crusaders! Down with the Iblis' followers! We, the_ Ummah, _believers in the Allah the Most High and Mohammad his Prophet (PBUH), we'll bring fire and brimstone to the heretic, fire and brimstone to the aposthate, fire and brimstone with the infidel. We'll smite them first in Libya, and them we'll smite them in Africa and we'll smite them in the world, for the glory of Allah and of the_ Mahdi!"

_"He's going even more radical."_ said Djanni

_"This can be good news if Jiminy is right."_ said Diana _"Jiminy, this thing was laced on the_ Sword Verse?"

_"Just in the beginning. As soon he came on all that_ Down with the Crusaders _thing, the power has faded."_ said Jiminy. _"As I said, I can see him having just some kind of control on his power. He has a bigger sheer power then me, but, and I'll not humble myself on this, I have more, as Cajun says,_ finesse _on my power control. He can, when calm, put some strong post-hypnotic trances on their targets. In the first video he was calmer, and see what he did on Djanni, even myself feeling the compulsion. In those last ones, his power is weaker."_

_"Looks like he's praying for their lot."_ said Dumont _"He's trying to unify the Muslims under himself..."_

_"And himself being the_ Mahdi!" said Djanni _"I felt this in the start of those video, but I didn't fell under this balderdash that time. And as Jiminy said, looks like he's going somewhat weaker. Not weaker in strict sense, but with less finesse, less subtlety."_ 

_"Alright."_ said Diana, that received some telephone calls in the meantime _"We are now under_ Code Red. _Looks like there's a big exodus of people coming from Libya, some evicted, some expelled. Basically the Christians and some secular Muslin, not affiliated to the main school of jurisprudence. In fact this is a_ pogrom. _We need to ready ourselves: in four days or so they'll start to arrive. UNHCR already asked for local countries to help enforce a humanitarian corridor for those coming via Maghreb or Sahara. Some of the Berets will join local forces to enforce it, the details being passed soon to you. We'll need to ready everything so people can join us as soon they are triaged. I think you can help people to join here being well-received. Already asking HSF and UNHCR for extra supplies, mainly barracks and hygiene items, clothes and food that will came ASAP. Sugarplum, I want you, Jiminy and Soldaire to help Squirrel Girl at the triage as soon they came: Lesley will train you in the main drill. Hufflepuff, LionHeart, Kuntur, Djanni, you'll help Dumont, Cabby and Corin to put everything ready, manage everyone, and occasionally help into security._ Roque Santeiro _is known on the integration rating, 95%. Now, let's go: there's more of 65 thousand people coming, it's a diaspora, and we need to be ready. This is Alpha Priority: we'll think on the_ Caliphate _and_ Alsyf Alayat _later."_

## Chapter 16

> _"The worst thing in the_ Alsyf Alayat _issue was that people always looks for a savior, and the only Savior is Allah the Forgiving. People should not put faith on man, no matter what. This is a kind of apostasy that people doesn't talk about. It is difficult to accept we are all failable. We always looks for inspiring people, and we always end idolatrize them, when they should stay being inspiration, not devotion. They were as imperfect than anyone else, and should not be praised, no one should be beside Allah the Most High. But as all the Prophets, from Adam, Peace be with Him, to Mohammad the Prophet (PBUH), passing by Musra and Isa, Peace be with them, people should be source of inspiration, not focus of praising. Either by good deeds and bad ones, no matter is one from the_ Ummah _or an infidel"_
>
> **Zayn Tahan, _'On the_ Sword Verses (Alsyf Alayat)'s fatwa _and his understanding under the Holy Qu'ran'_, Herós Sans Frontières.**

Two weeks had passed, and there was still having some triage. A kind of temp camp was mounted outside _Roque Santeiro_ as an emergency, and people were getting into it while triaging into _Roque Santeiro_. The security measures were enhanced  after all the attacks against _Roque Santeiro_:  _Alsyf Alayat_ menaces and the _Tunisian Diaspora_, as called by the news, made things even worse. 

Some people were saying why the Breakthroughs didn't nothing against _Alsyf Alayat_, some of them, like Mal Shankman, looking for political profit, saying _"where is the so called capes, the false idols, when people were attacked at Tunis?"_ So _Varekai_ had enforced a PR lockdown at _Roque Santeiro_: all communications involving local personnel would be screened to avoid PR problems.

Not that Jiminy was involved on all this PR thing: in the time between his shifts at triage, he was always studying and talking with his parents at Geneva, as they were worried with him since the video were _Alsyf Alayat_ revealed himself. Jiminy was not that calm, but he knew his strenght and his team's.

_"And, to be fair, mom,"_ reassured Jiminy, in the video-conference call _"we are as safe here than we could be anywhere else: lots of Berets and breakthroughs, and everyone helping into the security. If this guy really wants us, he will move Heaven and Hell to find us. Here, at least, I have the team and they have me. And, after all, they'll came against me, Jiminy Cricket, at least if he wants to play by the Rules. And if not, there's nothing I can do besides stay safe."_

_"Are you sure, sweetie? Fathama is also worried with Altayr."_ said his mother worried.

_"Djanni is strong, and he can, with us, deal with_ Alsyf Alayat. _Okay, he fell under his power once, but he will not get under this again. And, as far I have him, he have me. It's like rock-paper-scissors: we cover each other. We can deal those meanies."_ said Jiminy.

_"Alright son:"_ said his mother _"but do your job, rest and don't let this guy win. We'll be praying for your safety."_

_"Sure, mom. Thanks... Now, need to go: my triage shift will start."_ said Jiminy

_"Alright, son. Take care."_ said his mother.

Jiminy took a ride with Djanni to _Roque Santeiro_'s entrance and got into the triaging tent outside the camp. Some local Berets were there, doing the security and helping the first screening, while the others did the profiling: by using some connections with intel personnel, the local Asimovs had built a database of possible POI involving the _Caliphate_ and organized some questions that could help them to find people that could be under the _Sword Verses_, that were send for extra screening with _Squirrel Girl_ and to have any post-hypnotic triggers removed. Took time, but was effective.

Jiminy was now seeing interviewing a little meek but young guy. He was dressed into some of the common clothes for desert, so he came via the Maghreb/Sahara humanitarian corridors. He looked very simple.

_"Do you speak English?_ Parlez vous Français? Hal tatakallam arabiya?" said Jiminy.

_"I speak all them, but if you feel better, I'll speak English."_ said the man, with a strong Arabic accent.

_"Alright."_ said Jiminy _"So, what's your name and from where you came from?"_

_"I'm Saleh Tamir Abdul-Samad. I'm from Tataouine."_ said the man

_"Are you single? Have kids?"_ said Jiminy

_"No for both."_ said Saleh, while Jiminy was filling a little form into a notebook connected to _Roque Santeiro_ system. It was a prodigy on Agile Development the local Asimovs did, to build in less than a week a system crossing thousands of terabytes of data. There was systems that did this and even more easily, but the feat was to do so much in so little time.

_"Okay... Just a minute."_ said Jiminy, waiting some data to be retrieved.

It was when things gone to the dogs.

_Bellax Analytica_ has some flaws:

First, he can't preview too much without risking go haywire: to foreview the actions of many people, he needs to restrict it to some seconds, or even fractions of a second. If he want to foreview more time, he need to "remove" people from the analysis.

Second, even after all his training, _Bellax Analytica_ was not a kind of _all-time_ power: when he wanted to use it, he need to somewhat consciously "boot up" it. Otherwise, he isn't on. After "boot up", Jiminy would not need to engage consciously, except to fine tune the time/event relations.

But, in the case, Jiminy was not with it engaged, as it was strenuous in the long run.

Exactly when five people cried together _"Allahuh Akbar!"_

Three of them engaged the local MaxBerets, while another gone for Djanni, when he saw the last one going for Jiminy.

Saleh.

Jiminy just had time to think _"Sleeper Agent?"_ and holding his Jiminy Cane in his hand before the guy jumped and hit him straight in the belly, making him lost his air. _"Not Atlas."_ thought Jiminy, and the guy said: _"Sorry kid, but there's someone that want to see you."_, before Jiminy felt something wrong while going unconscious. 

He felt the vacuum that formed when teleporting.

---

_"JIMINY!"_ cried Djanni, when he saw Sugarplum blasting some magical energy blast against one of the guys that engaged the MaxBerets!

_"We have a situation!"_ said Squirrel Girl, spraying another of them with her skunk tail perfume, before changing her arms into a bizarre cross between wallaby and honey badger's _"Sleeper Agents!"_

_"We're dealing with them!"_ said Soldaire, engaging his _SolArmor_ to fight the last guy, while the Berets secured the safety of the other refugees _"Engage camp lockdown!"_

_"Roger!"_ said Diana, in Dispatch

The fight was rapid and furious, the four guys had been defeated very fast.

When they noted that Jiminy was no more there!

_"Dispatch, Jiminy is not here!"_ shouted Soldaire

_"We are checking the video..."_ said Diana

_"I saw them: the last guy was a teleporter! He took Jiminy!"_ said Djanni, worried

_"We are crossing intel based on the last guy!"_ reassured Diana _"Looks like the name of that guy was fake, the system is showing."_

_"Those_ Zindiq! Murtadd!" screamed Djanni in frustration
 
_"Calm down, Djanni! Being furious will not help Jiminy."_ said Soldaire, disengaging the SolArmor.

_"Those guys are crazy! They... They'll... They'll behead Jiminy!"_ said Djanni, desperate.

_"We'll work on find Jiminy ASAP."_ said Dumont _"Looks like he took his Cane. If so, there's no way someone hide Jiminy from us for too long."_

---

Jiminy woke up into a cell like place, still a little flinched, and he saw through the window: there was some desert nearby, so he knew he wasn't at _Roque Santeiro_ no more, maybe even at Sierra Leone. He was on his costume, but his mask, hat and cane were took from him. His head was supported on an old pillow and the mattress were he was laid was rough, but at least he was alive. His head was spinning while he tried to reassure himself.

_"Hope they didn't discarded the_ Jiminy Cane _before came here."_ thought Jiminy

In the aftermath of the second attack against _Roque Santeiro_, Jiminy asked Dumont to put on his things some active trackers that sent his location for a special system at Geneva. Only five people could get his location: Dumont, Soldaire, Diana and his parents. He started to double check himself and his situation.

He was feeling some aches on his body, but he looked otherwise okay: the wooziness had gone, nothing hurt, broken, sprained or twisted. His cell was small, but clean. He had been put unconscious over a bunk bed like those from military facilities, a kind of latrine and a little old fashioned shower also in the room. A mirror and an old sink with a faucet were nearby, and a small table with a stool was fixed in the opposite wall from the bed. There were bars in the window and the door was heavy bolted and made of steel.

_"Okay, I'm not hurt, so those guys have some interest on me. In fact, if they just wanted me killed, they would did this at_ Roque Santeiro, _it would be easier for them than teleport me."_ thought Jiminy, when a guy entered into the room and, before Jiminy could even speak, he commanded, pointing a pistol to Jiminy. 

_"You talk and try to use your_ Pretty Please!, _you die, simple that. Nod if you understand."_ ordered the man, with a very accented and bad English, and Jiminy nodded.

_"Right. No talk or get out from this room without some of our group. You'll be treated well for a_ Kafir. _Food three times a day, shower has warm water for a good shower per day. You can drink water from the faucet, it's clean. The latrine slab is over the shower, so clean it during the shower. You're responsible by cleaning the cell, and will be punished if you left things messy or dirty. The things are in that corner. We took your devices and mask. Understood? Nod."_ commanded the man, and Jiminy nodded. 

_"We'll put the food and clean clothes through that wheel."_ said the man, pointing it. _"You have half an hour to eat. Put back the tray to the wheel when finished. No talk to anyone, except when asked. Nod if OK."_ Jiminy nodded again.

The man got out the room and closed the door, when Jiminy heard the wheel being spun. He saw a little tray with food, basically Arabic food, a Couscous with some fish and potato. Jiminy felt hungry, and he didn't know how much time had passed. He took the tray and sat near the small table, taking the stool, and started to eat, taking a glass of water to make it go down. It was not exactly what he wanted, but it was food, it was somewhat good, and he needed to stay calm to deal with them.

Just a little time after he put the tray back the wheel, the wheel had spun again and there was a small package. As soon as he took the parcel, the wheel spun again, and again, and another time, each time with another parcel, every time spinning after Jiminy took the parcels. In the last one there was a little piece of paper. Jiminy took it and read in English, French and Arabic.

> _"These parcels are all that you'll need here. No need to stay on costume: we know you."_

Jiminy opened the parcels: there was some Arabic fashioned clothes, big robes and dresses, some underwear and a pair of traditional looking shoes, some towels, a small necessaire with shampoo and a somewhat good scented soap bar. A pillow and some blankets were next. The pillow was still a little rough, but it was ok. Next came a little box where was a small block of paper and a common pen, and a copy of the _Holy Qu'ran_, very beautiful. There was an inscription in the box: _"may Allah have mercy on you,_ Kafir". Anyway, Jiminy couldn't do anything beside think by now. 

_"If they left the Qu'ran here, they are trying to convert me. Not because my soul, but because my power. They want me to their numbers as a second source for_ Sword Verses. _So, let us play under their own game by now. It's not too much different on what dad did with that preachers that messed our Sunday getting in front of our door."_ he thought, and he took to read the Qu'ran. _"If this cell is monitored, they'll know if I read the Qu'ran or not. So, better cooperate by now, until I have more intel about where I am."_

---

_"We need to rescue Jiminy, and we need to do this NOW!"_ demanded Djanni, frustrated.

_"It's not this way."_ said Diana _"But we can't take Jiminy and gave those nuts what they wanted. And this_ IS _exactly what they want."_

The meeting was called ASAP, and in the meantime _Roque Santeiro_ was into a total lockdown: no one got in or out. All the main capes were there, and there was also Cajun, Cabby, Corin, Squirrel Girl, Kilimanjaro and Panther (real names: Josephine 'Josie' Nondaba and Robert 'Bob' Nsele), the last two ex-villains helped by Jiminy to get out from a problem with Derek Kwazani, now under parole at _Roque Santeiro_.

_"We detected very fast were he is. The good news is: we know were he is, at least by now. The bad news is: he's under_ Alsyf Alayat'_s hand."_ said Dumont

_"We discovered who were those guys."_ said Cajun _"Canada intelligence reported them as five Islamic radical from Montreux. The four we fought were not that good as breakthrough and they were all second generation Muslims, so Canadian Born. Their leader, Rashid Bahri, however, is a Pakistani veteran from the_ Caliphate War. _He was registered a POI by the Pakistani Intelligence, but he was clear, so no Interpol information. As far as Pakistani intel says, he is a B-Class Teleporter, and has some combat training in things like Philippine_ escrima _and so."_

_"The system was not fast enough to catch him before he got Jiminy."_ said Sugarplum _"It was like they foresaw our plan. That creepy_ Alsyf Alayat _has a_ Bellax Analytica _power?"_

_"This is a non-issue."_ said Soldaire _"This kind of plan is something anyone could try to see if pay profits without big costs. It was our fault to not ensure Jiminy to be always under his_ Bellax Analytica. _But, if_ Alsyf Alayat _was really this clever, and we need to suppose he is, he would put a plan like this: no risk and big profit. In the worst scenario, he could always plant a sleeper agent."_

_"Now... What can we do? Where Jiminy is?"_ said Djanni

_"Djanni, if our intel is correct, based on the tracking devices at the_ Jiminy Cane, _Jiminy is somewhere at Tataouine, south of Tunisia, nearby Algeria and Libya, both of them with_ Caliphate _sympathy. The LDS is now pressing both those governments to help fighting the_ Alsyf Alayat _insurgence at Tunisia, but both are being morose, in the best case. As a little more secular one, Algeria looks like a more probable help."_ said Dumont

_"And we don't know how much time we'll have. I had already made clear to the_ big kahunas: _we'll give 72 hours to LDS find out Jiminy, or our team will go supervillain"_ informed Soldaire, making everyone gasp

_Go Supervillain_ was the breakthrough term for going black-ops: it's a big deal for any CAI team to go this, and if he's ready to go this far, Soldaire was business.

_"He crossed all the lines."_ said Diana _"I need to say that I'll_ formally _stay with LDS, but they brought the Foxtrot, so they'll dance. No matter what, Jiminy was a non-combatant. Taking him is a big no-no, they should not do this. Now, it's certain we need to give a response, and now we should go Havoc with them."_

Everyone was pissed enough with this.

And they would be even more pissed:

_"Everyone, new video from this guy at the Deep Web."_ hushed Mr. Tahan _"They are commemorating Jiminy's kidnapping."_

The video was played and the _Alsyf Alayat_ was on the video with his veiled face:

> _"Lo! Allah is Merciful! Our brothers had put the hand into the asp's nest, and the asp didn't bit them. And they brought with them one of the asp's brethren, the Demon Spawn, the one who messed with the hearts of our brothers, the infidel with honey and poison in his mouth, the face of a kid and the vileness of_ Shayaten _himself, the so called Jiminy Cricket. In some days, he'll be judged by his apostasy, his heresy, his disbelief. And we'll bring him Justice under Allah the Most High, and Allah will have no Compassion on the infidel, and the forces of_ Shayaten, _the infidel Crusaders will be smitten in glory of Allah the High One! Lo! Allah is Merciful!"_

Djanni just screamed: "MURTADD!"

Sugarplum looked to him and said: _"Now, Djanni, we need to calm down and be into our best mind. Jiminy is strong."_

Kilimanjaro agreed: _"She's right. You saw him crazy during our encounter in the home of the_ Oya. _He was touched by the_ Oya _and survived. He saved Senesie and our souls, and my grandmother and mother had him under good opinion. We need to trust him being okay, and we need to do our best to stay calm, ready a good plan and take him back."_

_"We just received some intel."_ said Corin _"If this is the place Jiminy is, he's nearby the Tataouine old French Foreign Legion base. Looks like the_ Caliphate _took it as their own. We'll confirm the intel, but there's nothing what we can do by now."_

_"For now, we'll sleep and ready ourselves. We'll think on this as a team tomorrow."_ said Diana, dismissing the meeting _"Take some sleep. There's nothing more we can do now, and being tired tomorrow and in the next days will not help Jiminy."_

---

In the end of the day, Jiminy heard a man shouting something somewhere a little far away. He knew, by a kind of _Islam for Dummies_ crash course Djanni gave his team, this was the _muezzin_ from the local mosque asking the faithful for the prayers. By the light dimming, Jiminy knew it was going to be night. The wheel span and there was again a paper, this time just in English: "Alsyf Alayat, _Peace with Him, will came to talk with you after the_ Isha, _the last prayer. Clean yourself, and make yourself respectable in respect of him. Put your costume on the wheel as soon you dress out."_

Jiminy chose to not cross them, at least not until he have information enough to deal with them. Also, he was messy and dirty, so would have a use on a good refresing bath and on put clean clothes. So, while they were praying, and he could hear the last of the mandatory _salat_ prayers, he took a bath and put clean clothes, changing his costume to a set of Bedouin-like clothes from the parcels he took throught the wheel. The clothes were traditional and clean. Then, he folded and put his costume at the wheel.

While waiting the prayers to end, he read the Qu'ran, until he heard the door opening. A small guy, with a veil hiding his face, came in and took a little LED lamp as the night fell.

_"Sorry. This old infidel base had no light in the cells, and we didn't had time to install light bulbs."_ said the man, into a unbelievable polite tone. 

_Alsyf Alayat_ was looking _Jiminy Cricket._

_"Let us not play games each other: we know how much each of us can do, or at least some."_ said Alsyf Alayat

_"So, are you Rahul Musa Al-Gazzawi?"_ said Jiminy _"I know you know my name is Joshua McCarthy. You did your intel gathering on me, otherwise you would not attack Spotty the Clown, you wouldn't know I'm also him."_

_"Right about us knowing about you. You're a very clever kid in fact. Hope the Qu'ran illuminate you and you surrender yourself under the_ Shadahah, _understanding that only Allah is God and Muhammad (PBUH) his only Prophet."_ said Alsyf Alayat _"But, answering you, I'm no more_ Rahul Musa Al-Gazzawi. _I'm_ Alsyf Alayat, _the Sword Verses. I've saw the Djinn and the Prophet, and my face can burn those who's not from the_ Ummah. _But you're somewhat right because, as from the larvae came the pupa and came the butterfly, from the materialistic and almost_ Zindiq _Rahul Musa Al-Gazzawi came the Faithful_ Alsyf Alayat, _that was blessed by Allah the Most High with the power to urge those from the Islam to the_ Holy Jihad."

_"And what about me: I'm not a combatant, never was. I just wanted to avoid people to hurt themselves."_ said Jiminy

_"No, kid... What you did was apostasy by refraining the arm of Allah from cleansing that unholy place called_ Roque Santeiro. _But you are an infidel, at least by now, and you don't understand. Maybe you'll surrender yourself under the_ Shadahah, _and then you'll see how wrong his supposed friends are. If not, there's nothing I can do for you, because, as by Allah's will, I will not have compassion on you."_ said Alsyf Alayat _"You'll have from now seven days and nights. During this time you'll be treated well. If you not surrender yourself under the_ Shadahah, _by free will and pure heart, you'll receive Allah's justice, and them you'll sent to Fire and Brimstone at Hell's Slates in the Reckoning. Think well, and ask Allah for illumination."_ he said, before getting out, leaving the LED light behind.

_"So... Those are their terms:_ 'join me or die.'" thought Jiminy _"Hope the others came soon, or there'll no more Jiminy Cricket. I'll not accept turn myself into something like him: he's crazy."_

Jiminy got to the bed and nod off...

Albeit he woke up in the night... And saw there was someone else there.

## Chapter 17

> _"People assume that time is a strict progression of cause to effect, but actually — from a non-linear, non-subjective viewpoint — it's more like a big ball of wibbly-wobbly... timey-wimey... stuff."_ 
>
> _The Doctor, **Blink**_

The guy had a dark curled hair, with a tired gaze and a five-o'clock beard, beside the bed. He wore a long evening coat, cream-colored with a soft texture that looked almost like velvet, over gray slacks and shirt with a darker vest and tie. He put a small index finger over his lips before Jiminy could scream.

_"Calm down, Jiminy Cricket."_ said the man with a strong British accent _"I came here to talk, and it is important you hear me."_

_"Who are you? How do you know my name? Are you a teleporter?"_ said Jiminy

_"No, Jiminy Cricket. I have saw about you in the future. I am the Teatime Anarchist."_ said the man, making Jiminy gasp. 

Teatime Anarchist: the buffoon turned into one of the Biggest Super-villains after The Event. Some even considered him The Big Bad Guy overall. A death toll of thousands was credited to him.

But, why he looked so meek for Jiminy? And... 

How he came back from the dead? He died into an explosion just some days after The Big One, he saw in the news.

_"Now, I don't know you believe me or not, and I will not try to convince you anything I will say is truth, except two things: first, I am the real Teatime Anarchist; and two, I have no harmful intention against you."_ he said, putting a little device in the ground, that looked like a thin hockey puck. The device released a light blue hue that expanded until cover all the cell as soon he hit a small button over it.

_"A little humming device from 22nd century. They will not hear us talking outside, but it is better to be in a place where you, and better, me, can't be seen outside the window."_ said the Anarchist, going to a corner back the window _"Now, let us talk."_

_"Why me?"_ said Jiminy, going legs crossed over the bed.

_"I saw your future, or better, your_ **potential** _futures. I need to say that you brought some interest for me, but it is a  good one. And I am able to time travelling, before asking me how I saw them."_

_"You're what? How could you have any good interest on me? And what about the people you killed?"_ said Jiminy

_"It was not me, but my, you could say, evil twin. I can't explain all this by now. Maybe later you'll have all the answers you want. I need to ask you if you think it was weird Blackstone have any interest on you during_ Metrocon." said him, and this made Jiminy understand he was a really a time traveller.

_"I can't say I wasn't honored. I just didn't understood why one of the founder members of the_ Chicago Sentinels, the superhero team _by definition, and an ex-US Mariner would be interested into a little kid from Newark."_ said Jiminy

_"It was because my Future Files."_ said Teatime Anarchist _"I know that in your time I am already deceased: in fact, our talk is happening under just some time in my relative future before the events of my death. You may ask about free will, but in fact I_ have _the free will: I don't know how I will die, but I know where and when. I can postergate this a lot into my relative future, but I can't avoid this."_

_"But..."_ said Jiminy

_"I can't spend time to explain you all the physical and philosophical impacts on time-travelling, I don't have the time. I am here to talk about your futures, or at least the most potential ones, and those from some of your allies, and why everything is at risk."_

_"What?"_ said Joshua

_"In all your potential timelines, you have become a breakthrough. In all of them, you have more or less the same power set you have now, but your capename and power uses differed a lot."_ said the Anarchist, starting to lecture him on his potential futures.

_"Let us start with those futures that could no more be, mainly because you have undergone the breakthrough later on your life or had gone to Whitlow or Hillwood instead to HSF: the first one was called_ Ambassador _and your power was called_ Let's Talk. _You could make people agree with you on things, as far you could talk with them directly, being in the same room with them. Your power could not make people change their mind totally, but you could somewhat bend a little both parties to agree on something. This turned you into a very important person at UN and LDS, and you even, in one of those futures, arranged a peace agreement between the Caliphate, there a political party, and Israel. Sadly, in all those futures you were killed by someone that wanted to use you as a martyr or a scapegoat for war."_

_"Another one was called_ CEO. _In those futures, you were an undercover breakthrough that used his power_ Do as I've said _to make people_ Consume! _You could induce anyone to buy or do what you want, at least if it was a single action that would not demand them to go against some boundaries, like go violent and so. You made great money using them, but in many of those futures you tragically killed yourself, either into a single stroke, by shooting himself or something else, or gradually, by abusing on drugs or meds, because your life always became empty, specially when you lost or severed voluntary your connections with your family and friends, going ultra-capitalist."_ that one made Jiminy shiver.

_"Now, the third potential future, and a possible one somehow, is called_ Dominion. _Yes,"_ said the Anarchist looking Jiminy gasping. _"I know that was one of that Kyle kid for 'good' Mastermind names. Your power on this was called_ Sovereign, _and it was very simple: you could use it only_ once ever _against someone, but as soon you did, the person got totally under your will and would execute your order 100% no matter what, to the point to ignore his own safety and freedom to do what you want, more or less like the_ Sword Verse, _no willpower being able to resist you power. In this one, many times you were killed by some other breakthroughs, including your friends from_ Herós Sans Frontières, _and in others you were killed by revolted normal people. At least in one of them, you were the final reason to a dictatorship goes under US."_

_"And, the fourth and most probable one, is your actual one as_ Jiminy Cricket. _Curiously this is the only one that I could not see you being killed in action. In all of them, you turned into a kind of mentor for new heroes, taking the mantle of Blackstone or helping others at_ Herós Sans Frontières _or other CAI Teams in your relative future."_ said the Anarchist. _"Problem is that, at least in four of them, you forfeited being_ Jiminy Cricket, _either by retiring early, or by assuming other of the names, normally after some tragedy, like losing your parents or friends or seeing some of them going supervillain. In those cases, you normally became_ CEO _or_ Dominion." said the Anarchist

Jiminy was in the verge of crying: why this guy was there, talking all this dreadful things about his future, when he was trying his best to not panic and snap after being kidnapped?

_"You're full of fear, even panicking. This is good."_ said the Anarchist, to Jiminy's abashment _"If you heard your own future and all the possible ways to die you had, and you wasn't full of fear, you would either be insane, cold-hearted or arrogant, none of them a good idea."_

_"But why are you telling me this all?"_ said Joshua 

_"To somehow protect you."_ said the Anarchist _"You think they kidnapped you just to convert you to Islam and make you radicalize? That's nonsense. Your powers are the exact opposite of them: in fact, in some of your futures, after getting in contact with Djanni, you and your family did the_ Shadahah _and converted into Islam, sure, but in almost all of them you stayed as Jiminy Cricket. Just in one of them, you had radicalized, the only one that you took the_ Alsyf Alayat _mantle. Curiously, in this one, you were killed by Djanni. In one of them, you exchanged places, you acting in the operation to neutralize Djanni, then called_ Ifrit."

Jiminy was flabbergasted on all of this, and really scared.

_"Don't worry: this is just_ potential _futures. Your_ REAL _future is your own. It is like me: I know I will die and how and when, but what I will do in the meantime is my own. Knowing your potential futures doesn't remove your free will. What you will do with this knowledge is all about you, and you only, no matter what people like_ Alsyf Alayat _believes."_ said the Anarchist.

_"But, if he doesn't want me to be at his side, why he kidnapped me?"_ said Jiminy

_"What do you know about Derek Kwazani and his_ Breakthrough Enticing Drug?" said the Anarchist. Jiminy just gasped.

_"What? He wants to..."_ said Jiminy

"Alsyf Alayat _wants intel about Derek Kwazani's whereabouts. In fact, he developed his powers after using the_ BED." said the Anarchist, and Jiminy gone even more flabbergasted. The Anarchist ignored that fact and continued: _"In some of the grim potential futures I had saw, either Kwazani or_ Alsyf Alayat _used BED to create a country-sized supervillain team, which resulted into an all-out war and with almost all sub-Saharan Africa being sterilized using nukes until it was reduced to calcinated land and dust. In others, the nationalist forces released dispersed themselves, nations fragmented and gone into local fight, shattering Africa back to tribalism and ethnical conflicts. No matter what, politically speaking, in those futures, and potentially in your own, BED was a bigger power-balance shift event than The Event itself, because it had not only_ privatized _force as The Event did, but also_ commoditized _it: given enough money, anyone could be a cape. In one of them, this result into a drug-induced psychotic breakthrough pandemic that create a kind of super-powered version of a zombie apocalypse. Curiously, in this future, your associate called Sugarplum became a supervillain under the name_ Morrigan, _with a Succubus-like Supernatural power set."_

_"Stop saying this meanie things!"_ shouted Jiminy, almost crying.

The Anarchist stopped and looked for Jiminy face, that was full of fear and almost panicking.

_"Sorry..."_ said the Anarchist _"Normally I deal with adults. I forgot that, no matter what you are, you are still a kid."_ 

_"Don't patronize me!"_ pouted Jiminy _"Just stop saying me and my friends could go meanie!"_

_"No... you are right. I should not had pushed you too much. But I need to say, Jiminy Cricket, that this event with_ Alsyf Alayat _will be an ordeal for you in so many things."_ said the Anarchist _"If you want my help, I can, but it can be very dangerous, even potentially lethal."_ he said, pushing him a kind of box. Looking into it, Jiminy saw a little pearl-like small ball, like a pill.

_"This is future technology like my humming device, from the 22nd century I came from. I can't give you all the details, I have no time. But I can say that this is a kind of a quantum link to a computer in the future that will connect you with other people all around the world. They can help you and you can help them: remember that is not just_ Herós Sans Frontières _that wants_ Alsyf Alayat." said the Anarchist

_"You said it would be dangerous."_ said Jiminy

_"There is another device on this that you can use to call me if needed. You can activate it by hitting your own belly. This will activate a time-space beacon that will let me found you no matter where or when. Never tried to use this one with a so small kid. I can say that the last one that tried to use it as a beacon was an A-Class Atlas and it hurted, or will hurt, as hell on her. Otherwise, this will link you with people that can help you. I can't say too much more, because I don't know if you want this help. Sorry about this, but I have not too much time. However, it's now or never, your choice."_ said the Anarchist.

_"Okay: just take this will be hurtful?"_ asked Jiminy

_"Maybe. As I said, I never gave this bioseed for a so young person before. In the adults, it was like take a med pill. I don't know the impact of this in someone like you."_ said the Anarchist

_"Alright: let us take this. I don't have too  much time anyway. Those_ Caliphate _guys wants to turn me into a weapon for them, and I'll not let them do this."_ said Jiminy, taking the pearl-like thing and going to drink some water. 

_"At least, if we are lucky, this will grow in time to provide you help."_ said the Anarchist _"This bioseed will dissolve into your guts and then will connect itself to your brain, providing you access to the help, and installing also the beacon on you. In adults, this took some time until being activated, as they are bigger than you. As a kid, I think this will grow before the six days time gave to you by_ Alsyf Alayat." said the Anarchist, while Jiminy was swallowing the pearl-like ball. _"Remember, the beacon should be your last resort, as it is something painful even for an A-Class Atlas."_

_"Okay..."_ said Jiminy, when he fell his belly aching.

_"Looks like it will start fast."_ stated the Anarchist _"Let me help you to the bed."_ 

The Anarchist gave Jiminy another glass of water and removed one of the blankets, as Jiminy grew feverish, sweat running from his forehead.

_"You are growing hot: think that is the start of the bio-seeding. Don't worry: this not too good start is common. But tomorrow morning you will be okay."_ said the Anarchist _"Now, rest. Have a good dream: it is the best way to defend yourself against the nightmares of the reality."_ he finished, while getting back to the center of the cell, taking the puck-like humming device.

As soon Jiminy closed his eyes, everything gone back dark, like the Anarchist was never there.

## Chapter 18

> _"It was really weird to talk with the Teatime Anarchist. That made me understand that sometimes people goes supervillain for noble causes. It's sad, but at least those people were honest, by assuming the evil role. I don't know if I would be able to do this, though."_
>
>  **Jiminy Cricket, _"From Dress-up to save up: an underage breakthrough story"_**

In the next two days, Joshua made all he could to not cross the guys. The good news is that, the morning after the contact with the Anarchist, he was as good as new. 

He was left all the time with just the Qu'ran, with almost no human contact, so Jiminy read it, at least to memorize and extract what he could to argue about when asked by. It was in the third day night when Alsyf Alayat had came to give some talk with him, asking what he was understanding about the Qu'ran. He was harsh, but not aggressive, when Jiminy said he didn't saw why Christians and Muslims couldn't live without fight.

_"You're a child. Children should not get into the adults' affairs and in the realm of politics, and to try to understand the_ Jihad..." said Alsyf Alayat

_"But, if I remember, some of the Greeks and Romans old books were saved and studied by the Islamic philosophers in the past. Plato, Socrates and Aristotle just came to West thanks to the effort of Islamic philosophers and mathematicians."_ said Jiminy

_"It was for glory of Allah that we took the infidels' knowledge. It's not wrong to took what the infidels did as ours. It's wrong to partake with them."_ said Alsyf Alayat

_"Like you did?"_ asked Jiminy, and he saw Alsyf Alayat blushing red, so the Anarchist was right. _"I've read some intel about a unconfirmed thing about you, that you undergone your breakthrough by using the_ Breakthrough Enticing Drug. _This could explain why your_ Do as I said _power,_ Sword Verse, _only can induce hate on others."_

_"First of all, it's not hate: I urge those of the_ Ummah, _the believers, for the Holy_ Jihad _against the infidel. I saw Gibril and the Prophet (PBUH), while I was agonizing after taking that thing. And now, I'm the_ Sword Verse _incarnate_, Alsyf Alayat. _Second: it's justice. Since the Crusades the West ignored us. The Christians and Jews, albeit being part of the People of the Book, they assume they are the Chosen One's, but they have their heresies under Allah: and they tried to suppress us because they can't see Allah gave us the main resources in the world."_ said Alsyf Alayat

_"So, after all, it's about money, not about faith."_ said Jiminy, calmly.

It was when Jiminy saw he almost crossed the line.

Alsyf Alayat lifted his hand, almost slapping him, but he gave a smirk and said: _"You're just a child, and an infidel one. You can't understand how much my people suffered."_

_"Don't patronize me."_ said Jiminy _"I was lynched almost to death when undergone breakthrough. I saw Djanni being picked by other bullies in_ Roque Santeiro. _Christians, Jews, Muslims... no matter what, bullies are always bullies. This also applies for governments and so. My friend Djanni always talked me about_ Jihad, _and said that, as he learned from his parents, the Greater_ Jihad _were even more important than the Lesser_ Jihad."

_"You are wiser than you look: by hearing the words of your friend of the_ Ummah, _you started your path to surrender yourself under the_ Shadahah. _But there's no Lesser or Greater_ Jihad: _this is saying from the sheep and from the_ Zindiq. _There's only the_ Jihad. _Your friend is deluded, as his parents, by believing that live under the infidel would bring them happiness."_

"'But if the enemy incline towards peace, do thou also incline towards peace, and trust in Allah; for He is One that hears and knows all things.' _Verse 61, eighty_ Sura." said Jiminy, showing him the Qu'ran _"It was not me that said this, but the Prophet Mohammad (PBUH) itself enlightened by Allah the Most High."_

_"At least, you're wise to try to see your arguments into Allah's Words. But soon you will see your errors and surrender yourself under the_ Shadahah. _And then,  you'll give us the information we need to provide even more_ Mujaheddin _for Allah the Most High. Or, your sacrifice will call the_ Ummah _for us, cleaning their ears from the infidels' lies"._ said Alsyf Alayat, doing the _salaam_, the ritual greeting, leaving Jiminy's cell.

_"So, the Anarchist was right."_ thought Jiminy _"This guy wants me to provide him information about Derek Kwazani and his_ Breakthrough Enticing Drug. _He doesn't know that this was embargoed by the UN and CADUCEUS as classified intel, just it's existence bringing a problem into this_ Breakthrough Cold War _that is happening since the_ China _and_ Caliphate Wars." thought Jiminy. _"But now... How much time until that help came to me? My time is growing short."_

Jiminy thought it was a good idea just sleep, but he heard a little giggle somewhere.

_"Who's there?"_ said Jiminy, trying to not raise his voice enough to be heard outsite

_"I'm here,_ Jiminy Cricket." said the fairy girl in front of him...

---

In Chicago, Shelly looked worried to Astra during one of their evening patrols.

_"What happened?"_ asked Astra

_"Didn't you felt?"_ said Shelly _"Looks like another TA's bio-seed was engaged..."_

Shelly was Astra best friend, an origin chaser that killed herself trying to fly over the window, thinking that would made her goes through the breakthrough. In her last moments, the Teatime Anarchist made a deal with her: to be quantum-duplicated into a future computer system, and sometime after put back in contact with her best friend, Hope Corrigan, capename _Astra_. 

In the adventures they had, she first was a quantum-ghost inside Astra's mind, then a conscience downloaded to a Vernetech gynoid called Galathea, then a resurrected girl with an extraordinary learning capacity, thanks Ozma's Miracle Pills, and them BOTH the girl _and_ the quantum-ghost that operated Galathea "remotely", both at the same time and with some conscience sharing. In fact, she was at that moment _gestalted_ into a single conscience with the quantum-ghost.

_"How could it be if he's dead?"_ said Astra

_"Duh!"_ said Shelly, in her best Shelly-face _"Fact: the TA is a time-traveller. In our present he's dead_ but _for his own timeline maybe not. It's a paradox, sure, but in the end it doesn't violate any kind of 'time-travelling physics'. And fact: as I'm linked with Shell, my quantum-ghost twin sister, I can feel if some of them would be activated."_

_"Do you think that TA could give bio-seeds for others?"_

_"The Future Files and the Ourobouros Files shows this as a possibility: the TA is like a_ Bene Gesserit, _even being a man. He always had, or has, plans inside plans inside plans."_ quoted Shelly

_"Looks like his MO."_ stated Astra. _"But why now?"_

_"Looks like it's a real business."_ said Shelly _"I think I know now!"_

_"What?"_

_"It's_ another _quantum-ghost twin sister!"_ assured Shelly

_"What?!"_ exasperated Astra _"How much of you TA could do?"_

_"It's a quantum-ghost, duh!"_ said Shelly _"A digital copy of myself at my 'last moments'. It's like a music CD or downloading music into an iPod: as far you have the media and the means, technically you can copy them indefinitely. Morally it's other history. I don't think he could do too much of them, but certainly he have some extras for any need."_

_"And where it is..."_ said Astra

_"Somewhere in Africa, Tunisia."_ said Shelly. _"It's like she's trying to contact me for some reason... It's about that hero kid!"_ she gasped _"Jiminy Cricket, from_ Herós Sans Frontières!"

_"The one that Blackstone was talking with during_ Metrocon?" said Astra

_"Yup. The Future Files about him were really complex. Looked like he was okay, but now... Maybe we should go for Blackstone."_ said Shelly

_"Okay."_ stated Astra _"But, unless your_ other _quantum-ghost twin sister says he's in_ real real _danger, let us finish the patrol. I don't want Blackstone to waste his preciously few free time with Chakra for nothing."_

_"Alright, but as soon we get back the Dome, we'll talk Blackstone ASAP, even if he and Chakra are..."_ 

_"SHELLY!"_ yelled Astra, entertained and ashamed.

_"Astra, looks like something really serious, and Blackstone would like to know."_ stated Shelly

_"Okay."_ said Astra _"Let us finish the patrol, so."_

---

Jiminy looked to the weird girl flying in front of him, dressed like _Blue Fairy_ from Disney's Pinnochio, wings and tiara and even the fairy wand. Jiminy was accomplished with tiny girls in fairy dresses and wings: heck, Sugarplum ___was___ basically this way in combat. But the problem is he doesn't recognized her from all things he learned about capes and, heck, one of the main powers of _Masterminds_, those like him, was a kind of _Eidetic Memory_: as for an Atlas was fly, and for an Ajax was punch, and a Merlin was magic, it was natural for a Mastermind being a memory and mind power many levels beyond the "common" one. And Jiminy read the _Barlow's Guide to Superhuman_ end to end, back to front and back again, and memorized the main cape team's rosters around the world, and none of them included the _Blue Fairy_.

_"Who are you?"_ said Jiminy, trying to touch the woman like he did with Sugarplum, but he felt his hand passing through her: no mass, no nothing touching his hand. _"Are you magical?"_ 

_"Well, you can always says that_ 'Any sufficiently advanced technology is  indistinguishable from magic.' _FYI, this is the_ Third Clarke's Law. _I think the Teatime Anarchist said you that help would come for you, right? Well, I'm the Cavalry!"_ said the weird _Blue Fairy_ taking a call trumpet that showed out of nothing and playing it like in those cheesy western movies.

Jiminy was a little worried, but he calmed himself enough to get more lucid: being mingled with so many kinds of capes make this on you, and HSF assured that all teams had almost all kinds of heroes he could. So, he could understand that somehow that _Blue Fairy_ was linked with the Teatime Anarchist.

_"Hey, liked this name:_ Blue Fairy. _Blue Fairy and Jiminy Cricket. We just need Pinocchio, right?"_ said the girl, smiling

_"How do you know about this?"_ said Jiminy

_"I'm linked to your mind, duh!"_ said the _Blue Fairy "I'm a quantum copy of a girl that, in the past, did a great mistake trying to origin chase. Shelly Boyar was her name, and is my name now, if you need something beside a capename for me."_

Jiminy recognized the name:

_"Wait: Shelly Boyar,_ Galathea'_s remote operator?"_ said Jiminy _"But how..."_

_"Hey, kiddo, had heard what I said? I'm a quantum copy of her, duh!"_ said Shelly, smiling _"I'm like an avatar into a videogame: run it into two different consoles or computers, with two different players and, even the basic capabilities being the same, they'll go through different paths and different ways, doing different things. And fact: I'm a_ copy _of her just before dying a death-by-origin-chasing. Albeit I can contact them, I'm not either_ Shelly _or_ Shell,  _my twins."_ she said, while Jiminy sat into his stool

_"During the bio-seeding, I had looked for some data and memories on your brain to mold myself into a better construct experience and to know you better, so the communication would be eased. Don't worry, got no dark secret or anything mean you did... Well,_ maybe except _the day you got to the fridge and ate that ice cream pot during the night and woke up next day with a very big painful laryngitis. But, if this is the worst thing you ever did, your parents had a nice good boy. And you deserved that penicillin shot."_

_"What? How do you... You will not talk about this to anyone, will you?"_ said Jiminy

_"How? I'm connected to your brain, duh!"_ said Shelly, laughing like she heard a incredibly funny joke _"If I talk this to my twin sisters, they would think this as really really funny and on how cute you are when taken red-handed after stealing some cookies at night... Or better, some ice cream."_ said Shelly, with a sweet smile. This made Jiminy blushes.

_"Now, let go back to business, because there's not too much time. I'll take contact with my twins at Chicago ASAP. As I just booted up myself now, it will take some time until I can contact them via the quantum computer in the future. I'll explain this better as soon we get out of this."_ said Shelly as soon Jiminy looked flabbergasted. _"And avoid speak 'normally'. No one can see me: what you're seeing is a kind of... how they call now... Ah,_ Augmented Reality _avatar of me that the bioseed connections project directly into your brain's visual center. Neither your eyes are really seeing me. Just a whisper will do by now, and with time just think will be enough."_

_"Alright."_ whispered Jiminy _"But... Have you any kind of blueprints for this base?"_ 

_"Not yet, but working on it."_ said Shelly.

_"So: do you want me to call you Shelly like your 'twins'? And why you are this way?"_ said Jiminy

_"You can call me_ Blue Fairy _to differ me from them, albeit somehow we are all the same. And, about my look: I chose to take a look that could be comfortable to interact with you. I looked for a clown one, after all that Mimi & Spotty thing, but I saw that you chose the capename_ Jiminy Cricket _based on how much you liked_ Pinocchio _and on your powers. And I thought it would be funny to be your own Personal_ Blue Fairy." said Blue Fairy, doing a curtsy. _"Someone to hear your complains and someone who will be always here."_

_"Alright."_ whispered Jiminy, when he heard the wheel. It was the dinner.

_"Better for you to take your dinner and bath, and then go to sleep. I'll try to contact my twins in the meantime."_ said Blue Fairy, while Jiminy got to the wheel to take the food tray.

---

_"So, even dead, the Teatime Anarchist can act?"_ said Blackstone.

_"Well, for him, he's not dead."_ said Shelly

_"And now, he gave_ Jiminy Cricket _one of those bio-seeds, like the one Astra used."_

_"Yeah... And my new twin, that called herself_ Blue Fairy, _just booted up and now is with him. She gave me a kind of vision of the cell where he was. Looks like one of those_ Lawrence of Arabia _things."_ said Shelly

_"So, looks like this confirms the intel about the decommissioned_ Légion étrangère, _French Foreigner Legion, base, took as war spoil by Alsyf Alayat, the Sword Verses, during the actions_ Caliphate _provoked. Shelly, if you can talk with_ Blue Fairy, _say her to take all sounds and images nearby. All information we can gather that can help to trace his position is important. If you have news, bring me ASAP."_ said Blackstone _"Our friends from_ Roque Santeiro _will need this dearly to take some action against_ Alsyf Alayat, _the Sword Verses."_

_"Alright, Blackstone."_ said Shelly _"And I'll try to see for it in the Future Files."_

_"Do it. Everything can help now."_ said Blackstone.

## Chapter 19

> _"Being patronized just because you're a kid is a bore, but sometimes is good: no matter you're a hero, violate the Rules is a big no-no, even more if you're just a kid."_
>
>  **Jiminy Cricket, _"From Dress-up to save up: an underage breakthrough story"_**

It took six days to ready the operation, and it would take some hours to take Jiminy back.

The airplane, flying in the night over Africa, was getting near the Tunisian Airspace, but its transponder was off: after all that was a black-op.

_"They brought us the Foxtrot... We'll make them dance."_ said Soldaire, as they were entering Tunisian airspace. A crate that would be dropped by and took by Hufflepuff held everything they would need. Dumont, Kuntur, Sugarplum and Djanni, the flyers from team, would be ready to take the team before they hit the ground. 

It would not be safe enough to just get into Tunisian airspace. Algeria was a little more okay, because Red Crescent, but Tunisia was closing himself as a _Caliphate_ base, local secular forces from Army trying to take back the country from the radicalized forces that took it.

_"This is a simple extraction mission."_ said Diana via radio _"We'll take Jiminy and bring him back. The_ rendezvous _will be some kilometers after the Algerian-Tunisian Frontier, in Algerian territory. You have 48 hours to do this."_ said Diana, on the Earbug, remotely talking with them from _Roque Santeiro "Now, this will be a HALO drop -_ High Altitude Low Opening. _You'll drop now and open your parachutes just 300 feet before the ground. Soldaire, as you're not a flyer or have great constitution when not at the SolArmor, you're the weak spot in the drop. Kuntur, I want you to be ready to rescue him if anything goes wrong. As soon you get into ground, send the signal to the transport get out and then you're at your own."_

_"Roger!"_ said everyone. This was the first time they gone all this on being supervillain.

_"I hate this_ so much!" said Sugarplum.

_"I know."_ said Hufflepuff _"Even more that LionHeart could not go with us because of his parole, and will stay with Cajun and Corin to secure Cabby and the jet at the_ rendezvous."

_"Alright, you lot."_ said Cabby via the Earbug, when they saw a red light going on. _"Time to drop!"_

They pushed the crate and launched it, them they dropped, parachutes at their backs. First the flyers Kuntur, Djanni, Dumont and Sugarplum. Then Hufflepuff and Soldaire. As soon all of them had dropped, the jet take a turn back to Algerian space and got back.

This first step was okay: they dropped and Djanni and Kuntur, as fliers, activated the parachutes on the crate, to make it dropped safely. Sugarplum got down and used his tricks to charm the region with Glamour under an invisibility spell. Dumont, Hufflepuff and Soldaire finished the drop and got out. They took the crate, opened them and beside some bags with their things, and a special thing developed by some Japanese Vernes that Soldaire got as a favor: a small box full of little aligned and numbered capsules. In a paper were marked in Japanese characters what each number means. Soldaire took one of them, pressed a button and launched it as it was a grenade. The capsule made a POOF and a big black SUV, without identification, had gone in the place. They took and put the crate in the back of the car.

_"Now... We'll get on: if the intel_ Blackstone _gathered us is correct, we are four hours far from the old French base, and Jiminy has no much more time before being executed. The plan is very simple: Kuntur, Hufflepuff and me we'll do some diversion. Djanni, Sugarplum, Dumont, you'll recover Jiminy. As soon we do this, we'll get out and go for the Algerian frontier ASAP."_ said Soldaire

_"Right."_ said Dumont _"This is so much_ Delta Command, _many things can go really Foxtrot."_

_"Jiminy is counting with us. No matter what, we'll bring him back with us, alive!"_ said Sugarplum while she, as everyone, were readying themselves. They put the things in the car and got on.

---

Jiminy tried to stay calm, as he knew that _Blue Fairy_ passed all the information about where he was and the guys that kidnapped him for Blackstone, that forwarded it to _Roque Santeiro_. But that was his last night for the time Alsyf Alayat gave him. If he was not rescued, he would die or got into Alsyf Alayat trap. He knew that, if he converted himself to Islam, as soon he did the _Sword Verse_ would be used against him to try to put him under Alsyf Alayat's control. And he made his mind: even if everything failed and help didn't came he would not fell into his hateful lure.

In those days, Jiminy did his best to take some good look the people that came to him: this provided _Blue Fairy_ with images that, took from Jiminy's mind, she could send via quantum communication link (_'better accept it just works.'_ thought Jiminy) to their quantum twins, that could sent it for _Roque Santeiro_, with any juicy intel Blackstone and Shelly/Shell could provide.

It was when he woke up into his last day, and he saw they passed through the wheel a package, with his costume, cleaned, folded and perfumed, almost as new, and a paper:

_"We'll get in into an hour. No talk. Take a bath and eat, then dress up yourself with your costume. Put the Holy Qu'ran in your pocket."_ the flamboyant calligraphy said.

_"Looks like it is your 'Last Meal'"_ said Blue Fairy.

There was when the wheel spun again and he saw a tray with 'common', American, food. Not the Arabic food he was eating, but a big, honest-to-gosh bowl of Cheerios and milk, and some toasts with butter and chocolate milk.

_"Sounds like they want to do this by the book: if they kill you, they don't want to just kill, but to do all the execution ritual."_ said Blue Fairy

_"You don't turn this easier, you know."_ thought Joshua, going for the shower.

He did as he was saw to do, and then he cleansed himself and dressed into the costume. He felt they had removed everything that could be helpful to him trying to escape, including the contacts that provided energy for the Tesla Arc Gloves. His mask-cam was also broken, but they gave back him his mask and his Top Hat.

Just a moment after finished to dress himself and put the Qu'ran into a pocket from his tails, Jiminy heard the door unlocked. He saw four guys: Alsyf Alayat, the Teleporter that took him at _Roque Santeiro_ and two others. Everyone were dressed into black _ninja_-like fatigues with lifted hoods. Only Alsyf Alayat was dressed another way, into a pure white traditional tunic. He was also the only one without firearms: he had a kind of dagger, with lots of crystals engraved in the gilded, guardless, handle.

_"Come with us."_ said one of the other man. Jiminy just nodded calmly, and he goes out. They started to talk into Arabic

_"They are saying that you are so calm because they think you saw_ Allah _and so he'll do the_ Shadahah, _surrender yourself for Allah, and, in fact, for them."_ said Blue Fairy, doing the translation. _"By the way, the 'unknown' guys are Tarid Zawahi, B-Class Ajax, and the meek one is Sayd Al-Ankari, 'capename'_ Suleiman. _He's qualified as a C-Class Merlin, and it's said he controls some of the Djinn, as his namesake, King Solomon."_

_"So..."_ thought Jiminy _"He does_ Goetia, _formulaic invoking."_

_"I don't know..."_ said Blue Fairy _"But maybe it is, by what I found on Wikipedia about this."_

Jiminy almost gave a giggle, but he controlled himself.

The sun already rose, and he was brought to a place outside, and a car was expecting him.

_"Get into!"_ said the Suleiman man.

Joshua was growing panicking. Where they were bringing he?

---

Soldaire and the others got somewhere nearby the base without being seen. Sugarplum got Tink and flew high to look around and over the base.

_"Weird thing, there's a big space they could use for execute Jiminy... But no movement at all by now."_ she said

_"They normally wants this to be very dramatic."_ said Djanni _"Maybe they'll get out with him for a special execution site. Maybe a dune..."_

_"They doesn't even gave people a burial?"_ said Sugarplum

_"For what? In their idea, they are_ Kafir, _infidels, so they don't deserve a burial. In Islam, we believe that, once someone dies, he or she will have a demo on what will happen with him or her in the Reckoning. Some people believe that the infidels, in fact sinners in general, will be cooked in marble tables during the eternity."_ said Djanni

_"So... Basically, sinners are Devil's Happy Meal?"_ said Sugarplum.

_"Like this, except by the 'happy' part."_ said Djanni, seriously.

_"Sorry, Djanni."_ said Sugarplum, ashamed _"I didn't wanted to mock Islam."_

_"It's okay... We are all very nervous."_ said Djanni, when Sugarplum saw something.

_"There's a car getting out the base, going North. I'm shooting the tracker."_ said Sugarplum.

A kind of weird, pointed, Nerf shaped, device was shot by Sugarplum. It hitted a little below the car bumper, while Sugarplum got back for the car and Soldaire gone and turned the car on. Dumont turned a device on.

_"Let's go. I'll shadow them. Dumont, says me if they turn themselves another way."_ said Soldaire. Sugarplum sat over Dumont's fedora, holding the trim to avoid being lauched back and forth like a moth.

They got and gone shadow the car.

---

_"Jiminy, looks like another car is shadowing you... I can't take any comm, and neither can see it directly without you move yourself."_ said  Blue Fairy.

_"Still try talk with them."_ thought Jiminy, with his eyes closed.

_"Kid, open your eyes now!"_ said the Tarid guy.

_"Forget him."_ said Alsyf Alayat. _"Hope Gibril is illuminating him and making him see the sins in his ways against Allah. Maybe this will make him surrender himself to Allah, and reciting the Shadahah, he goes and says that's there's no other God and Muhammad (PBUH) is his only Prophet."_

_"Stay calm, Jiminy. Sounds like the cavalry is coming."_ said Blue Fairy, when the car stopped in front a place where there was a camera and a small cushion. 

_"Get out from the car."_ said Alsyf Alayat

After get out the car, the Tarid man gave a strong push that launched him to the ground, making Jiminy kneel over the small cushion. _"Alright, Jiminy Cricket. It's time to chose if you want to go to the Hell or you'll surrender yourself to Allah."_ said Alsyf Alayat. Jiminy saw that the camera was recording or streaming what was happening.

_"This is pointless."_ said Jiminy _"You would never accept me as part of_ Ummah. _In fact, you would always look me as an infidel, and you would try to kill me first chance, even turning me into a suicide bomber as soon I gone useless. So, I say no!"_ shouted Jiminy, when he felt the Sword Verses slapping him hard.

_"We gave you the chance to surrender yourself to Allah, saving either body and soul."_ said Alsyf Alayat, angry, putting Jiminy knelt in the cushion, holding him by his hair, his hat aside. _"But you revealed yourself as a_ Kafir, _an infidel, and a Crusader that mocks our faith. So, I proclaim you enemy of the_ Ummah _and sentence you, in the name of Allah the Most High, to Death. And Allah will give you justice in the Reckoning!"_ said Alsyf Alayat, sacking his _Janbiya_, reading to cut Jiminy's throat.

Jiminy felt despair like the first time he was attacked by Kyle and undergone breakthrough, as soon he could see the silvery light from the _Janbiya_ dagger in the air.

He prayed for his parents.

And Alsyf Alayat passed the dagger through Jiminy's throat...

---

_"They stopped, Soldaire!"_ said Sugarplum _"Let me get out. I can buy some time for us if needed."_

_"Alright."_ said Soldaire, stopping a little far away from the other car. They could see the camera and the four guys getting out the car with Jiminy. _"Be ready to get out of the car as soon as possible. Djanni and Kuntur, as Atlas you're the fastest ones. Engage the Teleporter, Rashid Bahri, and their Ajax, Tarid Zawari. Me and the others will engage Alsyf Alayat and the Suleiman guy."_

_"Roger."_ said Djanni and Kuntur, when they opened the doors from the car, in the same time Alsyf Alayat slapped Jiminy and readied himself to cut Jiminy's throat.

_"No way you'll cut Jiminy's throat!"_ said Sugarplum, pushing her fairy wand and shooting the best ward she could, and putting it as discretely she could. When she saw no blood spilling from Jiminy's throat, her saw she did her job good.

_"Go go go!"_ said Soldaire, while engaging the SolArmor and everyone ran to engage the tangos.

---

... and Jiminy didn't felt anything.

Alsyf Alayat looked worried when he saw no blood spraying from Jiminy's throat.

Jiminy then reacted instinctively, by throwing his elbow against Alsyf Alayat, and hit him straight between his legs, which make Alsyf Alayat scream and drop the _Janbiya_ dagger, which Jiminy took, taking with the other hand the dagger's sheath from his hands.

_"Hey, Jiminy, look like your Sugarplum friend is really powerful."_ said Blue Fairy _"She did the protection magic that make your throat stay as it is now!"_ 

_"Okay, but let us fight my way out this!"_ he said, when he saw the teleporter guy trying to get near him, blocking his way. 

But before he could touch him, he saw a shadow hitting him straight in the side.

_"Maybe you need a Jiminy Cricket to put you in a good way,_ Jiminy Cricket." said someone, smirking

_"Djanni!"_ said Jiminy, smiling.

_"Talk later. We'll take you back home."_ said Djanni

_"And..."_ was asking Jiminy

_"Forget those_ murtadd. _We'll deal them another time!"_ said Djanni, when he felt someone shooting to him.

_"There will be no other time for you,_ Zindiq!" said Alsyf Alayat, already recovered from Jiminy's attack, and really furious

Jiminy looked around: he saw Soldaire and Kuntur engaging the Ajax, while Sugarplum and Dumont joined themselves on Suleiman.

And the teleporter woke up and took Djanni, jumping away from Jiminy, while Alsyf Alayat was looking to Jiminy.

_"Now, without the_ Zindiq... _I tried to at least give you a clean death. But important thing, you'll have brimstone in the Hell, you_ Murtadd!" said Alsyf Alayat, unlocking the submachine gun for a new magazine and pointing to Jiminy.

It was when, again, Jiminy felt a great surge of grim futures on his _Bellax Analytica_. Wasn't his control improvement since Kilimanjaro's village events, he would be screaming and rolling in the ground like a kid in pain, but the headache was growing big. Even Alsyf Alayat could see something was really wrong...

And it was when the sands started to raise...

---

Sugarplum and Dumont were engaged on the Suleiman guy, that fought by launching fireballs against both of them. Sugarplum protected them with her _Geasa_ magics, while Dumont used his Tesla Arc Cannons to shot lighting and plasma against him. He jumped away, gracefully floating based on his magic powers, while the ground where he was just moments before were calcinated and turned into glass by the powerful electric and plasma shots.

_"This will never end!"_ said Dumont _"Even B-Class, this guy is really experienced in combat. Maybe a_ Caliphate War _veteran."_

_"Looks like he is growing tired..."_ said Sugarplum when she saw him reciting something and cutting his own hand palm with a knife, dropping some blood in the sand. 

_"Everyone, be ready for a big Charlie Foxtrot!"_ Sugarplum screamed in the Earbugs

_"What?"_ asked Soldaire

_"He'll try to use_ Goetia!" said Sugarplum while the guy chanted and scribbled an invocation circle in the sand with his own blood _"And a big one, by the magical resonance! Be ready for any kind of creepypasta!"_

_"Soldaire,_ Pepito Grillo _is not well!"_ said Hufflepuff _"Looks like is being affected by this_ Brujaria."

_"Go for him!"_ said Soldaire _"Everyone, we'll take him and get out as soon as possible!"_

Hufflepuff ran near Jiminy and Alsyf Alayat. He took Jiminy and just gave a little push aside Alsyf Alayat.

_"If you want to get alive,_ tarugo," said Hufflepuff, pulling Jiminy with him ny his arm, while running _"run now!"_ when the sands intensified

_"It's a sandstorm, you fool!"_ said Alsyf Alayat, taking his submachine gun again "Allah _is enraged with you. A_ Djinn _is coming!"_

_"This guy is in trouble! Looking like he gone too far on his_ Goetia!" said Sugarplum, when they saw Suleiman looking flabbergasted and terrified, as the big mass of sand rose and assumed the form of a hideous man, with horns and a bat-like wing, made of sand, and that made a terrible face.

He tried to run, when the sand demon took him with his sand-made hands, lifted him and just "ate" him, letting him fall inside his sand made body, everyone seeing Suleiman's body being grinded by the high speed sand inside the demon, his meat and blood growing into a big red mess inside the demon's body, Suleiman's body grinded to dust in seconds!

_"This thing is crazy!"_ said Hufflepuff, getting nearby Jiminy, that looked uneasy. They could see Djanni getting back with the Teleporter guy knocked out.

_"Sorry, I took some time to find you back, but was easy to deal with this guy!"_ said Djanni, looking for the demon _"What the..."_

When they saw Alsyf Alayat looking for it also.

_"It is_ Valac! _You fool, Suleiman! You were never as strong as the real Suleiman (PBUH)!"_ said Alsyf Alayat. Then he saw that either his teleporter and his Ajax were also defeated: the Teleporter was with a Sandman bag over his head, and his Ajax was knocked-out and under Blacklocks.

_"No way you'll catch me alive!"_ said Alsyf Alayat, trying to run to his car, while the Valac thing took him also, and lifted, while he was saying, desperate. _"I'm_ Alsyf Alayat, _the Sword Verses! I'm the prophesied_ Mahdi _to cleanse Earth from the infidel's stench!"_

That made no difference for the demon, the Djinn, when it released Alsyf Alayat over his mouth, and they could hear him scream panicking, passing through the sand mouth and throat, until hitting the sand thing's guts, a whirlwind of sand, fast enough to skin and mince Alsyf Alayat into mincemeat and grinding his bones, almost disintegrating him. It was when the creature looked for the _Légion étrangère_ base turned a _Caliphate_ bunker and started to give steps on its direction.

_"And this is what happens with those in_ kibria'." said Djanni, about Alsyf Alayat's fate.

_"It's not over yet!"_ said Jiminy, horrified, when looking the monster, that looked for them.

Everyone dispersed, but Jiminy froze like a deer looking to flashlights, while the big sand hand gone and took him!

_"JIMINY!"_ screamed Kuntur and Djanni.

Jiminy broke the freeze and start to move himself, trying to get out the sand hand, until he felt the _Janbiya_ and, for some reason, he used it against the demon's hand.

The demon felt the hit and released Jiminy, screaming in pain. Jiminy was took in the air by Djanni, still shaken...

_"Let's get outta here! Go go go!"_ commanded Soldaire, still on his SolArmor, in the pilot seat, everyone getting into the car.

_"What we should do?"_ asked Dumont, while Soldaire hitted the gas pedal on the SUV _"We are in enemy territory, but that Valac thing will kill everyone nearby."_

_"And looks like he grows in power after each kill. It's like he grows absorbing human life force... Or souls."_ stated Sugarplum

_"We can't just get out!"_ shout Jiminy, shaking like he was thrown into cold water _"This is a potential_ Omega Event! _If we want to stop it, we need to stop it NOW, or God only knows how big and dangerous this thing can grow!"_

_"Hey, if they hadn't took you, this wouldn't being happen!"_ said Sugarplum _"To be honest, I'm for just get out and save our own  bacon... Sorry, Djanni!"_ she said, looking that Djanni was angry _"We need to take care of us: and we have less then 36 hours to get back Algeria, or we'll lose our_ rendezvous."

_"But I think Jiminy is right."_ said Soldaire _"No matter what the_ Caliphate _did, and even we being here as_ villains, _under black-ops, they need our help, even doesn't deserving it."_

"'If anyone fulfills his brother’s needs, Allah will fulfill his needs; if one relieves a Muslim of his troubles, Allah will relieve his troubles on the Day of Resurrection.' _The Prophet (PBUH) teached us to help others, no matter what.”_ assured Djanni. _"They can't deal this one by their own: Suleiman and Alsyf Alayat were the strongest ones on their lot. Besides, using_ Suleiman's _magic to control Djinn is not an easy task, and that guy messed things hard by invoking one of the most powerful_ djinn, _one that became himself a Lord in Hell!"_

_"Alright."_ said Soldaire _"Anyone against besides Sugarplum?"_ he asked

_"Looks like I'll be outvoted..."_ sighed Sugarplum, shrugging _"OK... But that_ Goetia _thing is a big creepypasta, and I can't work my magic against him directly, I know a nothing of_ Goetia. _So, at least let us try to do something with him, at least to gain some time for help to come."_

_"Okay!"_ said Soldaire _"But first, let us care for Jiminy."_

_"I'm okay..."_ said Jiminy, better, just panting a little _"I did some concentration and the worst of a potential oversurge already faded away."_ 

_"How did you escaped that creature?"_ asked Kuntur

_"I used this dagger Alsyf Alayat tried to use on me."_ he said, showing it. Sugarplum reacted a little worried. _"What?"_

_"This is_ Orichalcum! _It's a kind of magical anathema."_ gasped Sugarplum _"It's a Vernetech or Merlin material that is used to fight and sever the magical energy link on all Merlin or Supernatural capes... Or things. Just sheath this thing back, it gave us Merlins the willies."_ trembled Sugarplum, and Jiminy sheated the dagger back.

_"This is good news."_ smiled Dumont _"If he can be affected by Orichalcum, powerful energy fields also can do some damage. Gain me some time and I'll MacGyver something to catch that guy and destroy him with the things we brought."_

_"Sugarplum, do you think you can help Dumont?"_ said Soldaire

_"I don't know... Mixing Merlin magic and Vernetech is like mixing hot water and boiling oil, not something safe to do."_ said Sugarplum _"But if needed, I can create a strong enough_ Geasa _to contain it for a time. I don't know how much, but I think it will be time enough."_

_"Alright... Looks like the best plan we can do now. Let us get back to the bunker."_ said Soldaire, changing the route, when he saw some guys getting away from the place, using jeeps and SUVs

_"They are leaving people behind!"_ said Sugarplum

_"They were never with the_ Ummah. _As soon they saw they had a problem, they ran away."_ said Djanni.

They arrived at the empty base when they started.

_"Okay... Sugarplum, you and Dumont works into your gigs. Jiminy, I want you to coordinate our moves with_ Bellax Analytica. _The others, let us buy time for them."_ said Soldaire

_"Now, in the trunk there's some swords and other weapons made on fake Orichalcum: a simpler version of Orichalcum and not too powerful, but good enough to deal with this thing by now. Besides, Soldaire, your SolPistols are energetic enough to deal some damage on this thing."_ said Dumont, while collapsing the trunk, a Vernetech one that was, in fact, a kind of portable workbench for Vernes.

_"I forgot to say: they had broken my maskcam and the Tesla Gloves!"_ gasped Jiminy

_"We thought on this and brought you a costume change if you want to engage. Take it."_ said Soldaire, giving Jiminy a bag with a new costume exchange

_"Maybe it's a good idea, for the_ Gloves _and the_ Cricket _pistols."_ said Jiminy.

Jiminy changed his clothes, besides his shirt, waistcoat and pants. He now felt more confident, and when he took the new Jiminy Cane, he was ready for action.

He was back as Jiminy Cricket.

_"Alright! Let's go!"_ said Soldaire

_"Jiminy, let me engage your mask-cam! And I want to present myself for the others."_ said Blue Fairy.

_"There's nothing you can do by now. Let us do this on a better time, okay?"_ said Jiminy

_"Pinky-finger promise?"_ 

_"Yes! Scout's honor!"_ said Jiminy, doing _Cub Scout_'s salute

Jiminy looked around in the now empty base and saw a tower that gave him enough vision. He asked Djanni to fly him to it before he could go with the others engage Valac, except Sugarplum, that could be heard humming some chanting to do her magics, and Dumont, that was frantically working into something, using the tools and materials into the Verne trunk.

Jiminy took a look all around when gone in the top of the tower and said: _"Right, there's three news: good, bad and even worse. Good: this guy is slow. Bad: there's not too much distance between the city and it. Worse: we are in his way."_

_"Okay,_ Pepito Grillo!" said Kuntur, while slashing the thing, but the thing doesn't look like gone weaker, when the cut was regenerated. 

_"It doesn't work!"_ said Djanni, slashing a lot _"He's too much massive: no matter how deep we cut, he grew it back!"_

_"He is going slower!"_ reassured Jiminy _"Just take care, looks he will attack you."_

And, in fact, it turned to them and started to attack.

_"Jiminy, there's comms from the local Army. What we say?"_ said Blue Fairy.

_"Say them to simple_ 'GET THE HECK OUT OF HERE!'" said Jiminy

_"Who are you talking with, Jiminy?"_ said Soldaire, shooting his Power Cannon, a big RPG like weapon that gave big energy shots and looked be the only thing that hurts Valac a little more.

_"Later, Soldaire. No time to talk. We deal with this thing and then I'll talk you all my story."_ shunned Jiminy

_"Alright... Sounds sensible. Deal with monster now, talk about imaginary friends later!"_ said Soldaire

_"Hey, I'm not an imaginary friend!"_ pouted Blue Fairy, hearing via auditory nerves Jiminy comms _"Asked them to get out."_ said Blue Fairy _"They said they'll make a circle if we fail."_

_"Okay..."_ said Jiminy. _"Sugarplum, Dumont, how much time?"_

_"I'm almost ready, Jiminy."_ said Dumont _"Just finishing some energy stabilization: we don't want to do the Tel Aviv here, right?"_

_"Okay. Sugarplum?"_

Nothing...

_"Looks like we need just a little more time. Sugarplum is finishing her mojo!"_ said Jiminy.

_"We can't contain it too much more!"_ said Djanni

_"Get back!"_ shouted Sugarplum _"Just get back: I'm holding my magic shield, but can't do this for too much time."_

_"Okay!"_ replied Soldaire _"Retreat. Back to the bunker!"_

They retreated, and Jiminy saw Sugarplum releasing his magic, doing a big circle with her fairy wand, a bright green shade showing the magical shield engaging.

_"And you, Dumont?"_ asked Jiminy

_"Almost there!"_ said Dumont

_"How much time?"_ said Soldaire

_"Two minutes, and then I need Djanni or Kuntur to bring me where Jiminy is."_ confirmed Dumont.

_"Alright."_ said Soldaire

The sand monster stopped when hit the shield and started to punch it, small pieces of green dust-like energy shards getting away.

_"This thing is too much strong. My spell will hold not too much time! Dumont, go on!"_ stressed Sugarplum

_"Done! Djanni, bring me there!"_ said Dumont, holding a kind of mishmash of electronic and steampunk-like things.

_"Okay!"_ said Djanni, taking him

Jiminy was looking for the sand monster.

_"Let this to me."_ said Jiminy, taking the weapon Dumont MacGyvered so fast.

_"Hold it, Jiminy!"_ said Dumont _"This is a high-powered plasma weapon! It's like_ Ghostbusters _weapon, real life. And, considering the power, it will  have a hell of recoil! And if this thing explodes, you'll be crisp to atoms!"_ 

_"So, even you could not handle this."_ said Djanni _"But I'm an Atlas, and this is where we shine."_

Djanni held the weapon.

_"Kuntur, now I need you take me and Jiminy down in front of the field, nearby the monster."_ said Dumont

_"What? Are you crazy, Dumont?"_ shouted Kuntur

_"Sounds a better idea than let that creepypasta loose."_ said Sugarplum 

_"Alright."_ said Kuntur, and place them where he was needed.

_"Now, Jiminy, as soon he opens a big enough hole in Sugarplum's shield, say us. Djanni, this weapon will release a big concentrated energy field. Use it against that thing. As soon he'll be shot, he'll go crazy. Jiminy, I'll need you to use your_ Bellax Analytica. _As soon he opens some holes nearby his feet or somewhere else, give the sign and launch this device I'm giving you."_ said Dumont, giving Jiminy a small steampunk version of the Ghost Trap from  _Ghostbusters. "When the sand looks like engulfing the device, hit the switch. Hope this works."_

_"Hope?"_ said Jiminy

_"Well... To be fair, our fate if this thing goes amok  wouldn't be worse than be devoured by a demon thing."_ said Dumont

_"You are not helping, you know."_ whined Jiminy.

Jiminy looked to the big sand demon and waited until he putted his head into a space in the shield and started to stomp it and screamed like hell.

_"Now, Djanni!"_ said Jiminy

Djanni activated the thing, that looked and was sized as a Gatling gun. The recoil was strong, but he held himself and directed the energy to the body of the sand thing, that punched two big holes in the ground.

_"Now, Jiminy!"_ said Dumont, throwing his trap thing against one of the legs, Jiminy to the other.

Both hit the switches at the same time, and the energy field released started to push the thing like  he was being pushed by a wheel, until his body was split aside in half, the energies flowing to the traps, a small dust of sand being the remaining of his physical body. 

_"Is that thing... finished?"_ asked Jiminy, looking for the traps that were releasing lots of steam and some bouts of static.

_"By now, at least."_ sighed Dumont  _"Hope that it for once. But we can leave this for some local Merlin deal with him. I think he's angry and frustrated enough to just want to get home."_

_"Like me... I just want to catch the clothes they made me use as memento."_  replied Jiminy 

_"Okay... And we want to know about that_ Blue Fairy _woman that helped us... And... To be fair, where she is?"_ asked Sugarplum. 

_"She's looking for us, we could say... But I'll explain while going back home."_ said Jiminy 

_"Jiminy, the Tunisian Army is asking who you are. They saw the_ Caliphate _operatives running away and they decided to take this base back to regroup and preparing a counter-attack against them."_ said Blue Fairy 

_"Say we're from_ Roque Santeiro, Herós Sans Frontières. _And that we're going home."_ says Jiminy. 

## Chapter 20

> "Alsyf Alayat'_s situation was solved very quickly, and lucky for us on this. I believe that, if he didn't pushed so hard on us by trying to kidnap me, he could had won us. He was really strong, in a different way. People can understand and fight super-powered Atlases or Ajaxes:_ Seif-Al-Din _was one of them, and there's lots of police and military training focused on deal with super-powered villains. They are boasty and their powers so visible that they could deal with it. But the_ Sword Verses _was a different thing: he could, for many people, looks like a meek guy, and then he could launch minions, even people beside you, as sleeper agents. I can somehow understand those most paranoid people: many of the most dangerous breakthrough are those whose powers are not that obvious."_
>
>  **Jiminy Cricket, _"From Dress-up to save up: an underage breakthrough story"_**

_"So, the main objective of_ Alsyf Alayat _kidnapping you was to take intel about Derek Kwazani and BED."_ said Diana, while Jiminy debrief back Roque Santeiro.

_"Right. At first, I believed he was interested into my_ Pretty Please!, _but the Teatime Anarchist was right: he wanted BED. I believe that the Teatime Anarchist wanted us to deal with him, directly or not. This is why he gave me the bioseed that generated 'Blue Fairy'-Shelly. By doing this, he could put me into Shelly/Shell radar, and the quantum-link could be used either as a 24/7 tracking device and as a way to take some extra intel from inside."_ commented Jiminy

_"And,"_ completed Blue Fairy, now 'downloaded' to a special holographic device provided by Dumont _"this way, using the quantum-link network, I could send images for Shelly/Shell and so they could, with Blackstone and Artemis help, gather intel on those tangos."_

_"Incredible."_ smiled  Soldaire _"And now? Because, we already dealt with_ Alsyf Alayat. _So, looks like you finished your job, right?"_

_"I don't know."_ said Blue Fairy _"The TA's_ Future Files _are not clear about what happen now... Besides, as far I know, I could not just shutdown now, as I'm integrated into Jiminy's mind. Need to say that I'm not detectable by common exams from your time."_

_"So, would you like to still with me?"_ said Jiminy

_"There's no other way..."_ said Blue Fairy _"Besides, I don't think a good boy like you could be difficult to deal with."_

_"We just need to set some privacy boundaries."_ said Jiminy

_"I know... Boys are boys. Hope you doesn't make me see any gross..."_ said Blue Fairy with a fake disgust face, when they saw Jiminy Cricket blushing

_"Hope you didn't..."_ said Sugarplum, half disgusted, half entertained

_"NO!"_ shouted Jiminy in despair.

_"Don't worry, Jiminy."_ smiled Djanni

_"Calm down,_ muchacho." laughed Hufflepuff _"We are not asking about this. You're a kid, after all."_

_"Well..."_ said Jiminy, trying to exchange topics _"So, I think we'll still together, Blue Fairy"_

_"Yeah, Jiminy Cricket. And your parents, can I know them?"_ said Blue Fairy

And she knew them and they knew her: it was weird to explain that a quantum copy of a formerly-deceased-now-resurrected origin-chaser girl from the time she tried to origin chase had implanted into their 11 year-old kid via a 22nd Vernetech, by the Big Bad Super-Villain Teatime Anarchist, but after all they heard, they were happy that Blue Fairy was there to help Jiminy.

_"Now, sweetie, what do you think coming back to Geneva for your days off-duty? And someone looked for a certain request for you."_ said his mother.

_"What?"_ said Jiminy

_"It's Mrs. Loretta."_ said his father.

Mrs. Sally Loretta was his English teacher on school until the day he had undergone breakthrough, and she was the one that defended Jiminy and other kids that suffered bullying the most. But she was always outvoted when she asked for more rigid rules against bullying at school.

_"What happened?"_ said Jiminy, worried

_"Calm down... She's fine. In fact, she now knows about you: looks like some_ paparazzi _gone to your old school when you were being sued. Now... They had a lot of problems with origin chasing. She want you to give a talk for the other kids."_ said his mother

_"Are they trying to origin chasing? Any nasty thing I need to know beforehand?"_ said Jiminy

_"Well... Two other kids from earlier classes gone blind trying to use some kind of meds cocktail and got overdoses, and a kid almost killed himself by jumping from the school balcony. Now he's in the Hospital, recovering as much as possible from a spinal cord injury that left him paraplegic."_ said his mother, a little down, and that made Jiminy gasp 

_"They are trying to do this after what happened with ME?"_ said Jiminy _"This is crazy! I didn't asked for this! I'm only glad to undergone breakthrough because the option would be death!"_

_"We know."_ said his father _"But... They're kids, doing kid stuff. They don't believe that a breakthrough is something totally life-changing and that happens only in the direst circunstances!"_

_"I know."_ said Jiminy _"Say Mrs. Loretta I'll go ASAP."_

Jiminy then took his days off and bought himself (or better, his parents bought) the tickets for a Freetown/Lisbon/New York flight, from where he gone to Newark. As his house was now down (and they didn't had time, money, or guts to rebuild it), Mrs. Loretta accepted him at home, when he came with a bag of clothes and a notebook into a backpack.

_"Need to say that you had grown nice and strong, Joshua."_ said Mrs. Loretta

_"Thanks... What can you say me about those origin chasing spree?"_ said Joshua

_"The kids are all proud of you, but some of them are grown jealous. I know you didn't asked for this, but they don't understand how complicated this is, and that is easier to win Indy 500_ AND _Charlotte 600_ SAME YEAR _than undergone breakthrough."_ said Mrs. Loretta _"I know this is a sensible topic, so I will understand if you just say no, but..."_

_"I understand. I'd talked with a friend that study breakthrough and origin chasing, and she gave me some help on what to talk about with the students."_ said Joshua, while drinking a glass of juice and taking the notebook from the backpack.

---

It was a commotion when Joshua came to the school next day. To call even more the attention, he had gone in cape, and entered by the main door. A lot of the kids saw him and came with Jiminy Cricket's Top Hats and photos so he could sign up. Even some of his former bullies asked for autographs _"for my little sister"_. He gave all the signs he could, but he needed to go to the Principal's room where he had a little chat with the Principal, that was happy on how he grew and about him helping them with the origin chasing problem. Then, he gone to the school theater and it was the time for him to talk.

_"Well, everyone, I know that recently there was lots of people here that were searching about, looking for and trying to origin chase. So, I asked someone that, until  undergone breakthrough somewhat 3 years before, was in our circle, to talk a little about this. Please, some cheers for Joshua McCarthy, capename Jiminy Cricket."_ said Mrs. Loretta, and then Joshua gone to the pulpit from the theater room.

_"Hello, everyone. I think some of you never saw me in this school, but until somewhat 3 years ago, I was one of you, in this same school. Getting in here for the first time in... three years, at least... I remembered the classes and playgrounds where I studied, played and learned, and the good time where all my worries were my homework and funny things, like what to wear at Halloween and so. I had my problems, sure: you can see, I'm not exactly big or strong like Atlas or Astra or Ajax, quite the contrary. But that time I was even smaller and weaker. Didn't help that bullies always liked to pick me up as an easy target. Sometimes, and I would be a liar to say otherwise, I thought on how I could deal with the bullies if I was strong as Ajax, or could fly like Astra or Atlas, or even if I could do magic tricks like Blackstone. I know that you all, specially those that are always picked by bullies, thought on this at least once, because I had thought the same before."_ started Jiminy

_"It was when, more or less 3 years ago, a group of bullies picked me outside the school and pushed me to an alley. I said them this was not a good idea and asked to them to leave me alone, but they just laugh on me and started to lynch me. I don't know how much time passed, and even today I have nightmares that wake me up in the night remembering that alley, and my bones being cracked one by one. I just passed by this alley coming here, and it was painful to remember, to even look to it. It was really painful, and I can say for sure to you: even after fighting Super-villains I wasn't hurt as much as I was that day."_

_"If you look on_ Barlow's Guide to Superhumans, _and I think there's a copy in the school library, it's said that_ 'A breakthrough is an unpredictable survival mechanism'. _The keywords here are_ unpredictable _and_ survival. _Because, and this is very important, 90% of all capes undergoes the breakthrough after a life-threating situation. There's some fortunated ones, like 9.9%, that just train hard or meditate or otherwise undergoes breakthrough after some epiphany, like_ Chakra _from Chicago Sentinels or my Paragon mentor_ Cajun. _There's a so-rare .1% that is just being born as breakthrough, normally_ Paragons _that goes human-peak on their skills. But, for the other 90%, the way to breakthrough is too much near the highway to death."_

_"As I said, on my case I heard many of my bones being broken, one by one, when something triggered into myself. I can't say_ exactly when _I developed my powers, but I screamed to the bullies to stop hurting me._ 'Pretty Please!' _sounds like a stupid name for a power, I know, but it was the first words I asked to them, before they could give me the final blow, to stop hurting me and going away. They froze when they heard these words, and I asked them to no more being meanie. And they did exactly as I've said, at least that time..."_

_"But I was still hurt: I was not fortunate enough to receive healing powers, like Atlases or Ajaxes. I was still hurt, still bleeding, almost dying, and then I discovered another power... I saw a kind of 'shadow' from a woman coming nearby and gone to her, that came just some seconds after. It was her that took me home, really hurt. When I came into home, I passed out..."_

_"Do you know what is to wake up from coma? That was exactly what happened to me: one moment, my dad was with me on his lap at home running to the local ER when I passed out. In the other, when I woke up, I was in a bed, plugged to machines, a tube through my mouth and throat. I saw the doctors coming_ before _really coming, and so, my_ Bellax Analytica _precognition powers screaming into my mind. I almost lost my mind that time, when I was freed from all the machines but the doctors and nurses and I couldn't control my mind from seeing parallel futures where my heart stopped and so on, they being obliged to sedate me back to sleep to avoid the worst. I still stayed some weeks casted because all the bones were still repairing and mending themselves."_

_"That was the 'price' I've 'paid' to be a cape. In fact, I didn't asked for this: I would love to be the little kid I was then, maybe on your place, hearing someone else talking about origin chasing to me as I'm talking to you now, thinking on homework and scout meeting at weekend and on Halloween or other fun things. But, after what happened with me, I believe that, as for some reason I didn't died, I should put those powers into service. I couldn't just forget them and left all this behind, getting back the school. It wouldn't be fair. And it was when, 7 months later, I arrived at Zurich airport, with my UN_ Lassez-Passer, _the so-called UN Passport, to report for duty at_ Herós Sans Frontières."

_"I know that some of you envy the capes, the breakthroughs, and I need to say you: don't. Remember that, beside the fortunate ones that gone under some epiphany, the breakthrough is stressful, painful, dangerous, or all of them. Sugarplum, one of my teammates, shrunk to a Tinkerbell size when put under gunpoint by some anarchist robbers. Hufflepuff just survived the Event by growing super strong and lifting the rabble from the_ Lucha _gymnasium where he was under. His father was not that lucky, dying after being trespassed by a steel bar. Djanni, one of my best friends, had undergone breakthrough while running away from some meanie kids from the refugee camp that wanted to do bad things with him. Soldaire undergone a strenuous training to be able to don the_ SolArmor _that is his main power source. And so on."_

_"By trying to origin chase, you're risking yourself: almost certainly, if your body doesn't undergo the breakthrough, and there's no warrant that this will happens, you'll die. With luck, you'll survive this if you don't undergo breakthrough, but with harsh side-effects. And, even if you go through breakthrough, there's a chance things goes even worse."_

_"I heard some of you are trying to use drugs, legal or not, for origin chase... I'm not here to judge you, I'll not call you stupid or crazy and I don't want to know which of you are doing this and how. I'm not here for this. I'm here to warn you. Using drugs, specially those normally used for origin chase, is dangerous. They have lots and lots of side effects, and can have even more when randomly used together. And even if you undergo breakthrough this way, there's a good chance that you go crazy in the process."_

_"Using origin chasing drugs is dangerous because they mess with your mind in dangerous ways for you and others. This is even cataloged at_ Barlow's _as_  psychotic breakthroughs, _breakthroughs where the people goes crazy and dangerous. In fact, many of those breakthroughs kills people during their breakthrough, as they lose control on themselves and their powers, their risk so great that normally and unfortunately they need to be killed."_

_"I once saw a guy, a kid like us, even younger than me, undergoing a psychotic breakthrough, and it was difficult to stop him without killing him. We were fortunate into doing so, and he was fortunate to not lose his life and even more fortunate by passing through this with his sanity. He is an exception under exceptions under exceptions. Those that undergoes psychotic breakthrough normally can't be restored to their right minds, even if they can stopped without being killed to avoid casualties."_

_"By just a moment, imagine feeling physical and mental pain, for real, so big that looks like you would explode. Think that this makes your mind shatter, and make you feel so much rage and pain and suffering and anger that all you want is to relieve it by breaking down everything nearby."_ said Joshua _"This is what can happen with you if you try to go breakthrough using those drugs. It's very, very,_ very _risky."_

_"If you ask me if I like being a breakthrough, surely I'll say yes. But sometimes, I would like to be just a kid, like you. In the last years, since I became a cape, or better, a CAI-certified breakthrough, I have just very precious few moments of peace with my parents. I had a broken leg, twisted another, had been shot, thrown by a crazy monster against a wall, almost roasted to death by one of my old time bullies, put after gunpoint lots of time, had frozen in pain when my power gone haywire, held hostage by some_ KKK-_wannabes and kidnapped by a heir of_ Seif-al-Din. _And if you think your homework are a problem, think that, even with all this, I still have to do homework when I'm not 'saving the world', or daddy, mommny or Sugarplum takes my dessert!"_ said Joshua, what made people laugh.

_"I had good times too, to be fair: known lots of people, including my best friends. Turned into a inspiration. Known the world and protected it. Did lots of different things. Had lots of fun. And even helped_ Herós Sans Frontières _collect some donations. To be fair, I can't say I would exchange all this by being with you, hearing other cape talking about all this. But, at the same time, I could not say otherwise also. What I want to say is that, as someone said me and I knew beforehand, breakthroughs are not gods. We eat, drink, sleep, sweat, piss, cry, laugh, learn, teach, love, hate, live, dream, can do some things and can't do some other things. Beside our powers, we are exactly like anyone else."_

_"One of the best things about breakthroughs is: you can't do everything alone. For example: I'm not a flier, but Djanni is, and when I need to go somewhere by flying, I need Djanni's help. But, when he needs help to see if he should go and fight goons or not, he counts on me, as my powers and my training ensures him that I'll take the best decisions, based on his capabilities and safety. At the same time, Dumont is our expert on supertechnology, as our Verne. I'll put into account when he says that a small needle-like weapon is really dangerous. As I put into account Sugarplum's knowledge and instinct about magic and so."_

_"To finish, you don't need powers to be a hero. This is cliche, I know. But cliches are so because they bring some truth on them. My job at_ Roque Santeiro  _is based more on my training and experience than on my powers. Sure, my powers helps, but I was trained to use them to do my job. Without that training, my powers could be useless at best case and misleading at worst. I also learned lots of other things and helped lots of people even without making use of my powers. You can do good things even without being a breakthrough: when I lost my house after a_ Humanity First _attack, people came and brought us food and comfort. There's lots of non-breakthrough people that helps others in bad events: policemen, firefighters, nurses, doctors, teachers, lawyers, even someone like Brick-a-Brack, my clowning teacher."_

_"So, if you want to help people, you don't need to be a cape, you don't need to be a breakthrough. You just need to be good with others. You just need to crave for help other. If you do so, if you care for others, you're ALREADY a hero. Even without powers, you'll be as Atlas, Astra, Ajax, Blackstone... And like Djanni, Sugarplum, Dumont... And like Jiminy Cricket too."_ said Joshua, finishing his talk.

_"Now... Anyone have questions?"_ said Joshua.

And they started to ask questions about _Herós Sans Frontières, Roque Santeiro,_ his powers and villains he fought against, like Derek Kwazani and Alsyf Alayat. And they liked to hear all the stories that Joshua had talked to them.

_"Hope you all now understand why you shouldn't go origin chasing. This is dangerous."_ said Joshua, and the kids agreed with him, clapping hands for Jiminy. 

Then, while they were getting out the theater for going to some fun time, Jiminy felt something weird, as he re-engaged his _Bellax Analytica_.

_"Mrs. Loretta, take everyone out the theater, if possible to the parking lot, and call for help!"_ said Joshua, going almost full Jiminy Cricket and running to the school's playground _"Don't let people get to the playground."_

_"What?..."_ she was saying

_"Looks like there's people getting here and there's the chance of a 'tag'."_ said Jiminy, while getting to the playground _"No matter what, don't let the kids get out to the playground!"_

_"Had you asked for a demonstration 'tag' to your school?"_ said Blue Fairy, showing herself into a blue-tinted Mary Poppins cosplay, carpetbag and parasol and all.

_"Not at all..."_ said Jiminy _"Contact local Dispatch: sounds we'll have a situation here and soon!"_ 

_"Alright. Spit spot!"_ said Blue Fairy, doing a curtsy and 'disappearing' from his line of sight.

He saw then three guys and recognized all of them.

They were some of the bullies that tried to kill him when he gone breakthrough and than were sent to reformatory.

_"Let me see..."_ said Jiminy, as he looked they were under a combat stance _"You had undergone breakthrough also, right?"_

_"Right, crappy."_ spat one of them, called Jonathan Leaf _"And you know how it happened, right?"_

_"Yeah... As for Kyle, you were spanked during a riot at the reformatory."_ 

_"Sure... But our powers were not as offensive as Kyle's, and we were smarter than him."_ said another guy, a big Slavic one called Sturm Williams _"We gone low profile after the riot and than we had gone to Whitlow's. Now we are at Hillwood."_

_"So, what you want to do something here? Just get back Hillwood, do your CAI Certification and live your own life. I never wanted to be a breakthrough and I don't want to deal with you."_ said Joshua

_"You don't understood."_ said the last one... It was the only that looked to him when he was almost killed at that corner. He was somewhat nerdy now, and looked like Presto from _Dungeons & Dragons_. His name was Carlyle Galatas, his parents a couple of Greek delicatessen vendors.

_"Jiminy..."_ said Blue Fairy, back into his line of sight _"Have bad news for you... They all are in the_ Future Files. _And need to say: except by that Presto-looking guy, they are really bad sport."_

_"Carlyle, you still have a chance... You can go back to Hillwood and live your life freely. There's kids in the school: an attack here and you'll go to a SuperMax, perhaps Detroit!"_ said Joshua

_"No bodies, no problem."_ said Jonathan, launching some razor sharped leaves from his hands.

Jiminy just had time to evade the attack, just saying a yelp after one of the leaves opening a scratch in his face, when he felt a vacuum back of him and his _Bellax Analytica_ engaging. He had time to avoid the punch the Sturm guy tried to gave him using his Jiminy Cane, but he was pushed back by the punch. He saw Carlyle also doing some kind of magic, holding a somewhat stylish rod.

_"Okay, Blue Fairy, what we have?"_ whispered Jiminy

_"The Jonathan guy is, or will be, known as_ Razorleaf. _B-Class flora projector: he can make razor-sharp leaves and vines from his body. The Sturm guy,_ Ballistic, _is a B-Class Teleporter/Ajax, and uses the teleport for punching successively his targets. The Carlyle kid shows under various names, according the Ourobouros Files from Shell and the Future Files, like_ Presto, Elminster _and even_ Vecna. _B-Class Merlin, with_ Dungeons & Dragons-_eque powers and theme."_ said Blue Fairy, while Jiminy avoided two vines. He had no way to get away from nearby the main building.

_"They know the_ Rules of Engagement _if they study at Hillwood."_ thought Jiminy, when tried to tasered Ballistic, avoiding his punches _"Had they already sidekicked with other heroes?"_

_"No... Just Whitlow's_ Hero 101 _by what I found."_ said Blue Fairy.

_"Any suggestion of creepypasta coming?"_ asked Jiminy _"For some reason, I can't read Carlyle moves!"_

_"Hope not... If he can take things from the_ D&D Monsters Manual, _he can bring creepypasta enough to make Italian Cthulhu happy!"_ corny-joked Blue Fairy trying to lighten things, when Jiminy felt a big lighting bolt hitting him straight in the chest, making him scream in pain, while felt some seizures by the static shock.

Jiminy had clashed against the wall pushed back by the lighting. Even in pain, he rolled aside to get out the building and to avoid Ballistic's attack, that jumped to smash him and hitted the ground.

He looked for all of them, and he saw that Carlyle was very scared on the power he just released, and by how Jiminy felt.

_"Looks like he didn't understood how deadly his power is and how weak your body is."_ said Blue Fairy

"Pretty Please!, _there's people there: if the building fall, you'll hurt them, even kill them."_ panted Jiminy, trying to use his _Pretty Please!_ power, even know the chances of it engages were small.

_"Not our problem."_ shooed Razorleaf. _"Now, you are the_ hero _here."_ he mocked _"If you can't stop us using that_ Pretty Please _crap, then they will die, and Kyle will sound stupid."_

_"Jiminy, I know you don't want to hurt people, but there's no way to avoid escalate this just by talking. You saw your_ Pretty Please! _isn't working. They aren't here for a hero vs villain 'tag'. This is just a bonus. They are not even to just kill you, they are here to kill_ everyone!" shouted Blue Fairy.

Jiminy rose, standing on his feet, and pointed his _Jiminy Cane_ to the guys.

_"Carlyle looks like the only one slightly affected by your_ Pretty Please! _This can ease things for you."_ said Blue Fairy.

_"Carlyle... If you kill them, you'll be hunted as you would never be. Kill innocents, and even some villains will want your head. This is the time you need to do your own choices."_ informed Jiminy

_"Shut up, you little faggot."_ said Razorleaf, using his vines to encircle Jiminy's throat.

_"Jiminy! If you don't fight back, you and all those kids will die!"_ said Blue Fairy.

Jiminy needed no more stimuli. 

He sacked the sword from the cane and used it to cut the vines, making Razorleaf scream. He felt the vacuum formed by Ballistic's teleporting, but using the cane size, he hitted the button for the taser and hitted Ballistic straight in the chest, making he goes unconscious.

_"One down, two left."_ he thought, when he looked Razorleaf screaming.

_"YOU. F---ING. FAGGOT!!!!"_ Razorleaf screamed, making leaves projecting from all his body, many of them hurting Jiminy for real. _"PRESTO, DO SOMETHING AGAINST THIS FAIRY!"_ 

Jiminy was hurt, and he looked for Carlyle.

_"Presto... Carlyle... It's up to you."_ said Jiminy in pain, lots of bleeding cuts on his body.

Carlyle was doing some magical gestures, readying some magic when four Magic Missiles showed over his head...

...and got away to Razorleaf's chest, that making him growling in pain.

Jiminy, ignoring the pain over his slashed body, got nearby Razorleaf, tasered him with his Jiminy Cane and sacked from his pocket a Sandman bag, part of his kit. Presto was about to running away, when the local Newark heroes came.

_"Alright, what happened here..."_ said one of them. Jiminy knew him: he didn't used masks, as almost all Newark breakthroughs. Johnny Mascherano was a B-Class Atlas, and one of the main staple for local heroes. _"You again, Jiminy Cricket?"_

_"Not... My... Fault... They just... Came... Attacked... School..."_ said Jiminy, when he fell over the ground in pain and Presto was being chased by the other local heroes into the playground.

_"Sylvia, do the first aid on Jiminy... I'll talk with school people..."_ he said, when Jiminy screamed.

_"DON'T! TAKE... PEOPLE... OUT! THEIR ATTACK... COMPROMISED... STRUCTURE!!!!"_ he shout all he could, even with his pain, when they heard the foundations starting to collapse.

Presto used his power and made some vines get out his magical hat and reinforce a little more the structures.

_"I CAN'T HOLD IT FOR TOO MUCH!"_ he screamed. _"EVERYONE GET OUT!!"_

The professors and kids got out from the building while Presto and the local heroes did his best to hold the building time enough to people get out. Everyone had time to get out before Presto needed to release the magical vines and the structure collapsed, the building fell down.

Jiminy was exhausted physically and mentally.

_"You did your best, Jiminy. For a non-combatant, you did very well."_ said Mascherano, while the woman Sylvia, called Sylvia Day, did the first aid job, using her _Dragon_ powers to reestablish his _Prana_ lines to improve his healing. _"It was just sad the school had fell... You studied here before become breakthrough and gone to_ Herós Sans Frontières, _right? Mrs. Loretta said me you came for a talk and those other kids came and attacked the school. Do you know them?"_

_"They were part of the bullies who thrashed me almost to death and made me go under the breakthrough. Then they had their own medicine at the reformatory. They had gone to Hillwood and then came to take me into a 'tag'."_ said Jiminy, still feeling a lot of pain.

_"Okay."_ said Sylvia, still touching in a kind of chiropractic treatment. _"I did my best for now. Just doesn't breath too heavy or does fast movements. Two weeks and you'll be as good as new."_

_"Thanks... I feel a little better."_ said Jiminy, while he saw Presto being blacklocked. _"Sir, he helped us in the end."_ 

_"I know."_ said Mascherano, seriously _"But we need to do these things fair and square: he'll be processed back into the system. I think he'll take a good parole, as he did his best to save people, but I think that he'll need to go for SuperMax... Or..."_ said Mascherano. _"Do you think that_ Herós Sans Frontières _would like another hero under parole?"_

_"I think so."_ said Jiminy, while being lifted to get to the ambulance. 

Presto looked to him and said: _"I didn't understood you until now. I think I was ashamed to be what I am, in the end. To be somewhat nerdy. It was weird to develop those_ D&D-_based powers. I was the local DM at the reformatory, and then at Whitlow's and Hillwood..."_

_"That's okay. Take care. Maybe we work together in the future."_ said Jiminy, while being sent to the ambulance. In there, Mrs. Loretta was being cared also:

_"Are you okay?"_ said Jiminy

_"Just a sprained ankle during the run."_ said Mrs. Loretta _"In some days I'll be good. You were incredible."_

_"Thanks... But, if Carlyle didn't changed his mind, maybe there would be no Jiminy Cricket to talk now."_ 

_"I saw... But you did what you could, and no one would blame you. And, in the end, you convinced Carlyle to get back to the good side."_ said Mrs. Loretta _"Now, go sleep a little, while people care for you."_ soothed said, sitting beside the paramedic while Jiminy was put in the stretcher, and he got back to the hospital.

## Chapter 21 - Epilogue

> _"Sometimes people confound our lives with the comics. We don't live in the comics, fighting a supervillain every day. We do, in fact, a lot of other things, as important or even more important. So, if you think the breakthrough are glamorous, don't. Our life can be as harsh as everyone else, only in other ways. So, for us_ Carpe Diem, _Enjoy your Time, is so important."_
>
>  **Jiminy Cricket, _"From Dress-up to save up: an underage breakthrough story"_**

Joshua visited the kids that tried origin chase based on himself and had some good talk. Also, he gone for the reopening of the school, into a new building lifted by the local Crew team and using pre-built foundations. Jiminy donated some items for the school do an auction to rebuild the school, including a complete, honest-to-gosh, albeit without the functional Vernetech, Jiminy Cricket costume, and some other items donated by his team.

But it was time to get back Roque Santeiro. A week at Geneva and then back to action at Sierra Leone.

_"Joshua!"_ said Mrs. Loretta. _"I heard you'll not come back Newark. Is it true?"_

Joshua looked for her and she could see eyes of someone that suffered a lot.

_"My home was burned down. I was sued by the one who tried to kill me. I fought again people that tried to kill me before. I brought danger for you. Maybe it's no more safe for me to come. And there's people with super-needs in the world. And as we says in_ Heròs Sans Frontiéres, Super-Help for Super-Needs." said Jiminy.

_"You're right."_ said Mrs. Loretta. _"But... If you want to know: the new building will be called Joshua McCarthy."_

_"I'm glad!"_ said Joshua, taking his bags when the flight was announced. _"Well... Need to go back Geneva."_

_"Your family is there, right?"_ 

_"Some of them: my team is also my family and vice-versa. But taking some days without worries with my parents is all I need now, after all the things that happened since the Metrocon."_ said Joshua, hugging Mrs. Loretta _"But I'll miss you."_

_"Me too... If we need more talks about origin chasing, can we count on you?"_ asked Mrs. Loretta.

_"Sure. But it's time to go."_ said Joshua, taking his UN _Lassez-Passer_ and going to the departure gate.

And know he has the feeling he's a world citizen, above all.

                              THE END...

<!--  LocalWords:  Jiminy bloodsport Roque Santeiro Fu Leone Dimantas
 -->
<!--  LocalWords:  Kabba Bangura VW Kwazani ViewTube Herós Shar'ia's
 -->
<!--  LocalWords:  shaikh iman Shia Djanni Kafir Zindiq Murtadd Zahan
 -->
<!--  LocalWords:  Soldaire Nondaba's Nondaba Bellax Analytica LDS IP
 -->
<!--  LocalWords:  Senesie Nondabas animistics Masseray Mabinty Eretz
 -->
<!--  LocalWords:  haywired SolPistols Shokichi Hisagawa SolArmor Lex
 -->
<!--  LocalWords:  Demoiselle's Masseray's McDaggert Ummah LionHeart
 -->
<!--  LocalWords:  murtadd Ozma Chakra Metrocon Shadahah Chakra's Oui
 -->
<!--  LocalWords:  Djanni's Talionis sura Armagan Acar Mossad Aviv Ça
 -->
<!--  LocalWords:  Criquet hitted Jovert Pienaar Asimovs Jiminy's Isa
 -->
<!--  LocalWords:  Tink Simbad haunched longly Gibril Sacre Bleu por
 -->
<!--  LocalWords:  squirrelly swordman Dadaab Kakuma Boggarts lucha
 -->
<!--  LocalWords:  Dementors limelights Kayfabe supervillain Tahans
 -->
<!--  LocalWords:  McCarthys Brack sente gote Almuhraj S'il vous va
 -->
<!--  LocalWords:  plaît venez être très amusant Jamilat fidlika ruk
 -->
<!--  LocalWords:  watati iilaa santiru maea almaerid wasawf yakun de
 -->
<!--  LocalWords:  madhakaan jiddaan Carmina Burana Rudo Apuestas Oya
 -->
<!--  LocalWords:  Enka yukata roquesanteiro healtheworld Mediciens
 -->
<!--  LocalWords:  Shayaten Firdaws Seif Alsyf Alayat kibria BAMF nd
 -->
<!--  LocalWords:  bwahahahahaha ronin Kitsune Musa cabrón Rahul Lofa
 -->
<!--  LocalWords:  Gazzawi Intifada Musra Cliché skunkwork hedgy Isha
 -->
<!--  LocalWords:  Kosovo D'Ivore Tarabulus idolatrize Fathama Parlez
 -->
<!--  LocalWords:  Français tatakallam arabiya Saleh Tamir Samad meds
 -->
<!--  LocalWords:  Tataouine Allahuh Nsele Montreux Bahri escrima DM
 -->
<!--  LocalWords:  salat wibbly timey wimey postergate timelines TA's
 -->
<!--  LocalWords:  Whitlow Hillwood Kwazani's commoditizing Corrigan
 -->
<!--  LocalWords:  Astra's Galathea timeline Ourobouros Bene Gesserit
 -->
<!--  LocalWords:  Kwizat Haderach Eidetic Boyar Légion étrangère
 -->
<!--  LocalWords:  Tarid Zawahi Sayd Ankari Goetia Zawari Brujaria
 -->
<!--  LocalWords:  Valac sheated SUVs mojo muchacho Superhumans Sturm
 -->
<!--  LocalWords:  Lassez cosplay Galatas SuperMax Razorleaf teleport
 -->
<!--  LocalWords:  Elminster Vecna eque tasered sidekicked Mascherano
 -->
<!--  LocalWords:  Ballistic's teleporting Prana blacklocked Heròs
 -->
<!--  LocalWords:  Frontiéres
 -->
