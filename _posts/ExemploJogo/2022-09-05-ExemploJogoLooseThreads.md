---
title: "Exemplo de jogo em _Loose Threads_"
#teaser: "Quando a turma precisa realizar um Teste de Coragem, os jogadores vão explorando o cenário e as regras!"
layout: post
date: 2022-09-05
categories:
    - RPG
header: no
excerpt_separator: <!-- excerpt -->
tags:
  - Fate
  - Aventura
  - Exemplo de Jogo
  - Loose Threads
  - ExemploDeJogo
---

Sílvio está continuando uma campanha de [_Loose Threads_](https://evilhat.itch.io/loose-threads-a-world-of-adventure-for-fate-core), onde um grupo de personagens de Fábulas que ainda procuram seu verdadeiro Desejo do Coração, o Final Feliz que realmente almejam, enquanto vivem as Tensões entre as facetas de si mesmo. 
 
No caso, os jogadores são:
 
+ Renata, que trouxe para a Companhia [Aubergine](http://fabiocosta0305.gitlab.io/personagens/Aubergine/), um Coelho Branco Falante, antigo Seneschal de uma Rainha Louca e que agora procura um pouco de paz e sossego, ainda que seja arrastado para a confusão;
+ Júlia, que criou [Rotnase Hans (Hans do Nariz Vermelho)](http://fabiocosta0305.gitlab.io/personagens/RotnaseHans/), um Bufão simplório cansado de uma vida sem motivo, procurando um motivo simples: obter uma vida digna, preferencialmente tornando-se Rei!
+ Mário, que colocou a [Maestrina Regina Orlana](http://fabiocosta0305.gitlab.io/personagens/MaestrinaReginaOrlana/) uma poderosa feiticeira que no passado foi uma Rainha Má, atualmente em busca de Redenção
+ Vitória, que está interpretando [Julian Brightsword](http://fabiocosta0305.gitlab.io/personagens/JulianBrightsword/), um soldado raso que possui o desejo de Heroísmo de um Príncipe Encantado de verdade
 
Depois de uma aventura anterior, onde receberam [___Tiras de Metal Mágico___](http://fabiocosta0305.gitlab.io/aventuras/TirasDeAco/) como uma pista para seus Desejos do Coração, eles foram parar em um ___Sultanato___ no meio de um deserto cruzado pelo ___Caminho de Ouro e Seda___. Descobriram então sobre o ___Fiel Hassam___, um serviçal que acompanhou a saga de seu Príncipe Hamid, transformado em Sapo por uma maldição mágica que foi quebrada. Entretanto, ao voltar livre de tais Tiras, ele teve que colocar similares, para que seu coração não explodisse de tristeza ao descobrir que sua amada Fátima tinha sido acusada de ser a ___Terrível Dama-Ladra do Deserto___, que estava atacando caravanas.

Em uma investigação prévia, notaram que as pessoas não acreditaram que Fátima era essa assassina cruel da qual era ela era acusada, e culpavam o ___Grão Vizir Hassad___ ao qual chamavam de _Hamad Hassad_ (Hassad, o Invejoso). Um homem melífluo, conselheiro do Sultão, comandou o Sultanato com mão de ferro durante a ausência do Sultão. 
 
Obviamente tem algo a mais nessa história, então cabe à Companhia das Fábulas Desajustadas que são os personagens obterem mais pistas, e para isso, depois de comprarem alguns suprimentos, incluindo a preciosa água, eles vão ao Deserto, onde certamente provações e perigos estão os esperando, como em todo Conto de Fadas

<!-- excerpt -->

---

**Sílvio:** Pois bem... O Deserto não demanda muitas explicações sobre o que é: areia até aonde a vista alcança, aqui e ali podendo serem vistos alguns Oasis a muita distância. Ocasionalmente, enquanto vocês caminham em seus camelos ou a pé, vocês também notam sargaços e outras plantas rasteiras de pequeno porte, secas pela quase completa ausência de umidade. O Sol escaldante está se pondo no momento em que vocês começam a ver o Sultanato como um ponto bem pequeno.
 
**Mário (Regina):** _Espero que saiba o que está fazendo, Coelho! Esse Deserto pode acabar com a minha beleza inestimável, ou coisa pior!_ (em sua voz natural) Regina está preocupada com a possibilidade de encontrarem perigos mágicos que ela não consiga lidar, ainda mais nessa Intermediação!

> Intermidiações, ou **In-betweens**, são espaços em _Loose Threads_ onde provações mágicas, Testes e perigos ocorrem para testarem os personagens quanto suas condições

**Renata (Aubergine):** _S-s-s-sim, Majestade! Eu verifiquei com as pessoas que questionei em Al-faizar que esse Deserto é lar de muitos bandidos e pessoas perigosas, mas também de sábios suffi e outros que podem nos ajudar a encontrar A Dama-Ladra e verificar se a fama dela procede._

**Vitória (Julian):** _Aubergine, meu caro: nunca se acusa uma mulher sem provas reais. Suspeito que a Fátima foi difamada com algum propósito, e que nos caberá descobrir quem foi e o porquê?_

**Júlia (Hans):** _Ora! Será que nenhum de vocês tem neurônios a mais que um bacalhau? Isso é óbvio como água, meu caro cavaleiro de uma nota só! Para ser sincero, já vi mais cérebro em um peixinho dourado afogado do que nessa Companhia de Pobre-Coitados!_

**Mário (Regina):** _Poupe-nos de suas pilhérias, bufão! Temos negócios mais importantes a resolver ao invés de ouvir suas patéticas patetices!_

**R:** E lá vamos nós!

**S:** Vamos lá... Então acho que Regina quer demonstrar que manda ali... Acho que isso puxa sua Tensão para o Poder, já que ***O Poder é um Vício que me puxa para fora do caminho da Paz***, não?

> Em Loose Threads, certos Aspectos possuem uma especial função. No caso da Tensão, ele puxa o personagem para uma direção dentro de suas contradições, o que pode melhorar ou piorar alguns tipos de ações. No caso de Regina, as contradições são para a Paz (melhora _Criar Vantagens_ e piora _Atacar_) e para o Poder (melhora _Superar_ e piora _Defesa_). Sílvio está forçando a Tensão, puxando ela para o _Poder_

**M:** Não... Acho que nem chega a tanto. Regina apenas está frustrada por ter que estar em um local tão desconfortável e inseguro, mesmo esperando que isso traga pistas para uma forma de realizar o Desejo do Coração dela. Não vale a pena aceitar isso agora... Pago um PD para recusar essa Forçada, pensando em como ela poderia chutar o traseiro desse bobo para dentro de um vulcão ou para a boca de um dragão e ninguém daria falta dele, mas qual seria o ganho dela? 

> Sempre que um Aspecto é Forçado, o jogador pode recusar a Forçada, justificando o por quê dentro do mesmo Aspecto. No caso, Mário recusa a Forçada da Tensão ao lembrar de que deve conter o Vício pelo Poder se deseja alcançar o Desejo do Coração dela

**S:** Tudo bem, aceito seu PD!

**J:** Caraca... Sabe que isso pode nos prejudicar lá na frente, né? Regina é nossa única usuária de magia.

**M:** Por isso mesmo não é tão boa ideia.  OK... Vocês vêem Regina se segurando para não enfiar o pé no traseiro de Hans até que ela responde (Regina) _"Vamos então! Quanto mais tempo gastarmos aqui, pior será nossa situação! Nossas provisões não são exatamente vastas, vocês sabem!"_

> Sérgio gosta do que ela disse: ele pode agora encaixar um Empecilho, uma situação que visa testar os personagens sobre seu comportamento, complicadores típicos de Contos de Fadas. No caso, ele vai pegar justamente um _Teste de Bondade_

**S:** Justo quando vocês falam isso, em uma duna com um rochedo próximo, vocês vêem uma velha caída ao chão, o corpo quase seco, sussurrando (Ele muda a voz para a da velha) _"Água... Água para uma pobre velha..."_

**M (Regina):** _Velho é esse truque. Conheço isso: vexatório alguém se rebaixar a esse truque baixo!_

**R:** Aubergine se aproxima com muita cautela: ele sabe que esse pode ser um truque, mas ele está disposto ao menos dar o benefício da dúvida.

**S:** Rola sua _Percepção_, Aubergine. A dificuldade é _Razoável (+2)_ e é um teste de Superar, já que você quer notar se ela está mentindo ou não.

**R:** Vamos lá então. O bom é que Aubergine é _Bom (+3)_ em _Percepção_!

> Renata rola os dados para `0+0+`{: .fate_font}, para um resultado final ***Excepcional (+5)***, formando um _Sucesso com Estilo_

**S:** Aubergine, você sempre ouviu em Al-Faizar comentarem sobre pessoas que se perdem no Deserto ou cujos suprimentos se perdem ou ficam escassos, e você lembra que diziam que ***No Deserto, a mão que oferece água é melhor que a que oferece ouro!***. Você nota que ela realmente está muito desidratada, até perigosamente. Entretanto, você sabe que se oferecer água a ela, vai ser de sua parcela, e isso provavelmente vai lhe provocar problemas, em especial por você ser um Coelho todo felpudo que cozinha no fogo do Deserto.

**R:** Tudo bem, vou topar isso... Abro o meu alforje e me aproximo da velha, oferecendo aos poucos parte da minha água.

**S:** OK... Marque a caixa 1 de Estresse. Essa água certamente lhe fará falta depois. Isso vai contar como um Sacrifício de Vitalidade.

**M (Regina):** _Coelho parvo, desperdiça sua água em uma velha prestes a morrer?_

**R (Aubergine):** _Ao menos, caso ela morra, não morrerá em vão! Nem tudo é sobre poder._

**S:** Está Forçando a Tensão dela? Não pode fazer isso: cada personagem só pode ter a Tensão forçada uma vez por cena.

**R:** Eu sei... Estou Forçando a minha, ___As Amarras Sociais___. Aubergine está de saco cheio de ver gente usando as relações sociais como algo para bel prazer ou para ferrar a vida dos outros!

**S:** Entendi... Vai puxar para a _Liberdade_?

**R:** Sim... Isso vai reduzir em 1 meus testes de _Ataque_, mas aumenta em 1 meus testes de _Superar_

**S:** Para mim, perfeito. Toma 1 PD. Aubergine sacia a sede da Velha. _Obrigado meu caro_ Arnabi... _Alguns_ tuaregues, _bandidos da região, me roubaram boa parte das provisões, incluindo toda a minha água. Aliás, meu nome é Samira. Quem são vocês? Não são muitos os que caminham pelo Deserto, e você salvou minha vida, portanto você e seus amigos são meus Hóspedes durante essa noite, em meu nome e de meus Ancestrais, dos Poetas e dos Santos e de todas as Divindades._ Ela diz, levantando-se com cautela e acompanhando vocês até a entrada do rochedo, onde vocês vêem uma gruta que, mal e mal, permitem que vocês se protejam do calor inclemente que ainda faz.

**M:** Regina se aproxima com cautela: ela ainda desconfia que tem algo errado nessa mulher.

**S:** Rola tua _Magia (Zaps)_ para ver. Dificuldade _Razoável (+2)_

> Regina sabe utilizar uma forma de magia chamada _Zaps_, que é aquele tipo de magia utilizada para efeitos poderosos de chamas e eletricidade. Mas todo tipo de magia é útil para reconhecer outros usuários de magia.
> 
> Mário rola `+0+0`{: .fate_font} para +2 no Dado. Como Regina possui _Magia (Zaps) Ótimo (+4)_, foi para um resultado _Fantástico (+6)_, o que é um Sucesso com Estilo.

**S:** Você percebe que ela é uma usuária de magia poderosa. Provavelmente um daqueles _suffi_ que falaram para vocês em Al-faizar...

**M (Regina):** _Você não consegue esconder seu poder de mim, velha. Vocẽ é uma usuária de magia, provavelmente uma Suffi._ Quero que ela revele alguma informação útil no Impulso... Pode ser como um Aspecto fixo, mas sem Invocação Gratuíta?

**S:** Sem problema. (Velha) _"Ora... Então você também usa artes mágicas... Inclusive tendo vergonha de sua idade. Tola! A idade é a prova da sabedoria! Mas... Como seu amigo Arnabi me salvou, extendo minha Hospitalidade a você. Mas acredito que estejam a mando de Hassad Hamad para roubar mais de meus conhecimentos!"_

**V (Julian):** _Não, minha senhora... Estamos procurando descobrir maiores informações sobre a donzela conhecida pelo populacho como a Dama-Ladra. Acreditamos que ela possa ter sido difamada por alguém, vom algum objetivo ulterior. Viemos procurar pistas para nossas vidas, mas descobrimos que Hassam, Fiel Servidor do Sultão, novamente prendeu seu coração com Tiras de Metal para não sofrer a dor de ver sua amada ter seu nome na lama!_

**S:** A velha arregala os olhos e diz _Mostre-me as Tiras antigas! Imagino que estejam com vocês!_

**M (Regina):** _Veja... Acho que você deve entender o que disso se trata!_

**S:** A velha faz uns gestos e diz _Essas tiras foram rompidas pela felicidade, quando o Sultão saiu de sua maldição... Mas agora... As peças começam a se encaixar. Antes que perguntem, sim: já fui uma Suffi do Conselho do Sultanato, provendo o Sultão com minha sabedoria. Mas na época em que a maldição ocorreu e começou a ser difamado o nome de Fátima, eu me afastei do Sultanato, me retirando para meditar aqui no Deserto e o tornando meu lar. Aquele Hamad Hassad! Deve ter aprendido de alguma forma sobre o Bracelete de Submissão!_

**J (Hans):** _Bracelete de Submissão? Isso parece bastante específico!_

**V (Julian):** _Deixe a senhora falar, Hans... Precisamos de toda informação possível..._

**S (Velha):** _Devem saber que existe nesse Deserto uma poderosa Corte Mágica dos Djanni, os espíritos livres da Natureza. Esses espíritos podem ser ocasionalmente capturados em poderosos itens mágicos, anéis ou braceletes ou outros itens. Entretanto, capturar um Djann é algo muito arriscado: faça de maneira mal feita, e mesmo que a Ira do Djann não seja o suficiente para os matar, a Corte lhe negará a importante Hospitalidade do Deserto, e ninguém, exceto os mais poderosos ou tolos, iriam contra a Corte dos Djanni. Considerando isso, tudo se encaixa._

**M (Regina):** _Como?_

**S (Velha):** _Primeiramente, me chamem de Samira... Fui tutora de Hassad no passado, coisa do qual me envergonharei até o dia que for para o túmulo. Mil vezes o nome dele seja considerado odioso! Que seu nome seja jogado fora como o Besouro Rola-Bosta faz! Ele deve ter descoberto sobre o Tabu do Bracelete de Submissão! Esse item seria tão poderoso que seria capaz de prender qualquer Djann, até mesmo o Poderoso Ad-Demiri Omar, o Sultão Supremo dos Djanni, cujos olhos viram a Criação da Existência e cujos ouvidos aprenderam os Segredos do Universo diretamente dos Ventos Primordiais!_

**J:** Então, se eu entendi, esse item seria uma espécie de _Master Ball_ dos Djanni.

**S:** Isso mesmo.

**M (Regina):** _Mas o que tudo tem a ver? Itens mágicos poderosos são difíceis de serem criados, dependem de poderosa magia e materiais especiais, como Oricalco._

**S (Samira):** _Por isso disse que você é tola! Não existe material mais poderoso que aquele que carrega em si as emoções humanas. Posso ver em seus olhos que a sua Ambição já foi sua Ruína, ao menos uma vez, deveria saber que isso ocorre. Tudo passa a se encaixar! Maldito Hassad! Ele deseja que Hassam, Fiel mas em dor, o coração dividido entre o Serviço e o Amor, veja sua amada ser morta com o nome em descrédito, acusada de crimes que não cometeu! Seria demais até mesmo para o mais Fiel dos Serviçais do Sultão, ainda mais depois de uma experiência dolorosa de procurar e recuperar seu senhor depois da Maldição! Ele não suportaria, seu coração explodiria em dor e tristeza! E isso, com as Tiras de Metal Mágico, geraria um material poderoso, mas um verdadeiro_ Haram, _um crime, uma heresia, um pecado! Um Metal Mágico tão poderoso que, moldado alquimicamente, poderia conter qualquer Djanni sem a menor chance de resistência!_

**M (Regina):** _E ele utilizaria esse item para capturar o Sultão Supremo dos Djanni, certo? Realmente ambicioso._

**R (Aubergine):** _Então precisamos cada vez mais descobrir a verdade sobre a Dama Ladra, preferencialmente achando seu esconderijo e tentando a convencer do risco que ambos correm, Hassam e Fátima... Talvez ajudando até mesmo eles a fugir, mas isso imaginando que Hassam decida-se a quebrar seus laços de vassalagem com o Sultão... Nada fácil, sei como é isso: só fugi porque a Rainha de Copas era louca de tudo. Cedo ou tarde ela perderia a cabeça, metaforicamente falando, e eu a perderia de maneira literal!_

**S (Samira):** _Não será fácil encontrar Fátima: o Deserto é perigoso. Mesmo durante a noite: o frio inclemente dá lugar ao calor escaldante, podendo matar pessoas congeladas, múmias que viram alimento para as serpentes. Agora, se realmente pretendem seguir adiante, sugiro ao menos que passem essa noite aqui._

**R (Aubergine):** _Não podemos, cara Samira. A vida dos dois está em perigo, e creio que o Sultanato também: se o Poder de Al-Demiri Omar é tão grandioso, caso ele caia nas mãos de alguém que ainda controlou um poderoso_ Ifrit, _certamente será algo ainda mais perigoso... Até mesmo outros Reinos próximos correriam riscos, como Ekvar ou Baronia, próximos o suficiente para que ele exercesse sua Ambição!_

**S (Samira):** _Vejo que não posso os dissuadir... Então, ao menos permita-me impor minhas mãos sobre vocẽs: a minha benção é suficiente para que vocês sejam notados em todos os Oasis como pessoas dignas da Hospitalidade do Deserto. Lembrem-se de que isso implica manter a cortesia e evitar o abuso da boa vontade dos povos do Deserto._ Samira diz, e ela levanta suas mãos e aplica uma ___Benção Mágica___

**M:** E o Aspecto que eu pedi lá atrás?

**S:** Desculpa, esqueci disso! OK... Agora você sabe do ___Plano do Bracelete de Submissão___ do Vizir: isso pode ajudar com pessoas que conheçam Fátima e que acreditem nela...

**M:** Beleza. Regina faz uma reverência ao entender que Samira é uma maga muito poderosa e diz (Regina) _Vamos! Não temos um segundo a perder, ou isso custará tudo para nós, não apenas nossos Desejos do Coração!_

**J (Hans):** _Isso! Não sejamos patéticos peripatéticos! Vamos adiante._

**R (Aubergine, segurando a mão de Samira):** _Obrigado por tudo! Faremos o possível para descobrir a verdade e revelar a maldade do Vizir._

**S (Samira):** _Que o Vento Leste, e todos os bons Espíritos e Divindades do Deserto lhes protejam._ Ela diz, enquanto vocês seguem o seu caminho. Nesse tempo que vocês ficaram com Samira, a noite já caiu, e vocês notam que realmente é verdade o que ela disse: o Deserto continua inclemente, mas pelo motivo reverso. Faz um frio absurdo, se você considerar o calor infernal que estava até pouco mais de uma hora atrás.

**M (Regina):** _Pilhéria! Que frio horroroso! Ao menos acho que temos roupas apropriadas junto às provisões que obtivemos._

**S:** Sim, as ___Provisões___ que vocês possuem incluem algumas roupas similares a camisolões que são quentes o bastante para suportarem o Frio. 

**J (Hans):** _E o que estamos esperando? Vamos fazer uma pausa e colocar tais roupas. Não quero virar um picolé!_

**S:** Hans, como você é ***Mais sábio do que aparenta (por isso se finge de idiota)*** estou Forçando esse Aspecto para que você decida algum perigo que vai ocorrer agora e que Hans nota antes de todo mundo.

**J:** E se aparecer alguma Cobra? As Cobras do Deserto são conhecidas por serem muito perigosas e venenosas.

**S:** Na realidade, vou colocar algo a mais: vocês pararam perto de um sargaço, onde vocês ouvem um _baaaaahhh_ desesperado. Ao olharem perto, vocês notam um carneiro preso no meio dos sargaços, e uma Naja se levantando pronta para atacar! O que vocês vão fazer?

**J:** Rotnase Hans não tem dúvida: dos bolsos de suas roupas de bufão, ele tira bolas de malabarismo e as arremessa contra a Cobra!

> Sílvio sorri, pois sabe que um conflito assim pode resultar nos PCs tendo sérios problemas!

**S:** OK... Role teu _Atirar_ e eu vou rolar a Defesa da Cobra!

> Rotnase Hans tem _Atirar Regular (+1)_, e consegue um `-0-0`{: .fate_font}, caindo para _Ruim (-1)_. A cobra possui Defesa _Medíocre (+0)_, e consegue um `+0+-`{: .fate_font} para um _Bom (+1)_, se esquivando

**S:** OK... A boa notícia é que a Cobra deixa de se focar no Carneiro, mas a má é que se foca em vocês! As suas bolas, Rotnase, não foram suficientes para atingir a Cobra

**J:** Eita! Agora estamos encrencados! Quem quer ser o próximo?

**R:** Pode ser eu? Aubergine pensa em tirar o carneiro dos Sargaços, de modo a deixar caminho livre para Regina usar sua magia.

**S:** Parece interessante! Pode agir, Aubergine... Rotnase já agiu nesse primeiro turno do Conflito, já que ele o abriu ao atiçar a Cobra!

> Sílvio passa para Aubergine uma Pena vermelha de uma velha Fantasia de Peter Pan a ser usada como indicador de Iniciativa

**R:** Vamos nessa, dadinhos, ajudem Aubergine!

**S:** Você vai precisar passar pela Cobra para pegar o Cordeiro, já que ela é o obstáculo!

> Aubergine consegue um `00+-`{: .fate_font}, e a Cobra consegue um `0+-+`{: .fate_font}. Como isso foi uma Defesa, a Cobra tem _Medíocre (+0)_ para um _Regular (+1)_, enquanto Aubergine também consegue um _Regular (+1)_ como um resultado final.

**S:** Bem... Empatando... Quer melhorar?

**R:** Na verdade, lembra que tou a +1 em _Superar_ por causa da Tensão?

**S:** Sim... Verdade! Realmente! Com isso você passa a _Cobra_ e puxa o Carneiro dos Sardaços.

**R:** Isso... Me afasto o suficiente para que Regina possa fazer seu show!

> Renata passa a pena para Mário

**M:** Regina nem tem dúvida. Ela ergue seu _Cajado de Poder_ e recita uma Invocação Mágica e Atira uma Bola de Fogo, sem pensar meia vez!

**S:** Acho que a cobra já era.

**M:** Ela mexeu com a pessoa errada!

> Mário rola a _Magia (Zaps) Ótimo (+4)_ de Regina, e ainda adiciona +2 de uma Façanha chamada _Cajado de Poder_ por o usar. Como se não bastasse, Ela ainda consegue `-+++`{: .fate_font} para +2 nos dados com um resultado final _Lendário (+8)_!
> 
> A Cobra não tem a menor chance! Sua defesa _Medíocre (+0)_, somada a seus dados `+00+`{: .fate_font} resultam em um _Razoável (+2)_.

**S:** Ela não tem Aspectos o bastante para eu usar todos os PDs para tentar conseguir Empatar... Então, você a torra com um _Sucesso com Estilo_. O que você quer que aconteça? Vai baixar em 1 ou quer alguma coisa adicional?

**M:** Quero que Regina seja observada por todas as criaturas mágicas da região como a Poderosa Maga que ela é.

**S:** Bem... Isso encaixa direitinho com o que estou pensando... Bem... O Sargaço está pegando um fogo Azul, que é obviamente mágico... Enquanto Aubergine dá a volta com o Carneiro, um garotinho se aproxima e diz (Garotinho) _"Forasteiros... Esse carneiro é meu, eu o perdi enquanto pastoreava. Estou procurando o mesmo a horas!"_

**R:** Aubergine se aproxima e entrega o carneiro _Foi sorte estarmos aqui... Tinha uma cobra Naja ali que estava pronta para tornar o seu carneiro em jantar_ 

**S (pastor):** _Obrigado! Realmente foi uma sorte. Acho que os Espíritos iluminaram tudo para que vocês o salvassem!_

**M (Regina):** _Melhor então levarmos você de volta para seus pais. Onde você vive?_

**S (pastor):** _Meus pais vivem em um Oasis próximo. São poucos minutos de caminhada._

**M:** A gente segue o menino.

**S:** Conforme vocês avançam, do nada aparece um homem gigantesco, de pele levemente azulada e farta barba preta, obviamente besuntada em perfume. Ele clama para vocês: _"Eu sou Daruich, senhor da Magia e o mais forte dos fortes. Conclamo entre vocês alguém a me desafiar em qualquer desafio!"_

**M (Regina):** _Quer desafio? Terá seu desafio!_ Regina já está pronta para pegar esse cara.

**J (Hans):** _Deixa eu lidar com o grandão, minha cara despirocada! Ei, bolo fofo!_ diz Hans _Se é tão sabichão assim, quero ver me pegar. Se você me capturar, você será o vencedor. Entretanto, se eu fizer você cansar, eu vencerei você e você não passará de um falastrão irritante!_

**V:** Eita! (Julian) _Hans, não seja assim! Isso nos colocará em encrenca!_

**S (Daruich):** O homem, que vocês notam ser um Djann, diz _HAHAHAHHAHA!!!! Gostei! Aprecio sua coragem, ou talvez a sua falta de noção, ó Príncipe das Pilhérias! Que assim seja! Você pode correr por onde quiser, e eu não recorrerei à minha magia para o impedir de Escapar. Eu irei apenas usar a mesma para me deslocar: canse-me, e serás vitorioso._

**J (Hans):** _Que seja assim então, nobre falastrão irritante!_

**M (Regina):** _Tolo! Não o provoque! O poder da magia dele é superior a tudo o que conhecemos!_

**S:** Seja como for, está aberta a disputa entre Rotnase Hans e Daruich. Ele vai tentar te Capturar usando seu _Lutar_ e acho que Rotnase Hans vai tentar cansá-lo por _Atletismo_, certo?

**J:** Exatamente o que estava pensando!

**V (Julian):** _Deixe de patetice, Hans, eu resolvo isso com a minha lâmina!_ Posso Forçar o meu _Método_ para partir para ignorância contra o Djann? Afinal de contas, na cabeça de Julian, ***Heroísmo é a alma do Príncipe Encantado***, mas ele ainda não consegue discernir Heroísmo de Síndrome de Lemingue.

**J:** Você é besta? Não vai ter a menor chance de sobreviver...

**S:** Está bem... Mas... Como você está interferindo, Daruich usa sua _Grande Magia_ e aplica uma poderosa Maldição mágica em você. Preencha seu _Sétimo Aspecto_ com ***A Forma de um Burro para um Burro em Pessoa!***

**R:** Caraca... Foi pesada essa!

**S:** Eu não tenho culpa... 

**V:** OK... Topo isso!

> _Grande Magia_ é uma forma de Magia em Loose Threads focada em Bençãos e Maldições Mágicas, que normalmente não está sob acesso dos jogadores. Daruich a possui em _Fantástico (+6)_
> 
> O _Sétimo Aspecto_ é um Aspecto Especial que começa a Campanha vazio, mas que pode ser usado para colocar Promessas mágicas, Maldições e coisas similares. No caso de Julian, o Aspecto representa a transformação em burro que sofrerá. 

**V (Julian):** _Ióóóóó! Ióóóóó! Ióóóóó!_

**R (Albergine):** _Que enrascada você se meteu, Julian! Existe momento para a coragem, e esse não era um!_ Aubergine se aproxima para cuidar do agora burrico Julian.

**S:** Agora acho que podemos resolver a Disputa entre Daruich e Rotnase Hans. Melhor de Cinco, quem rolar melhor leva a vitória... E duas se for por estilo

> Sílvio e Júlia pegam os dados. Daruich irá usar sua _Grande Magia_ para se deslocar como o vento, enquanto Rotnase Hans irá contar apenas com seu _Atletismo Bom (+3)_ para fazer cabriolagens para escapar do perigo! Sílvio começa com um `000-`{: .fate_font}, enquanto Júlia tem muita sorte nos dados, obtendo um `0+++`{: .fate_font}. O rolamento de Sílvio baixa a _Grande Magia_ de Daruich para _Excepcional (+5)_ e o rolamento de Júlia eleva o Atletismo de Hans para _Fantástico (+6)_

**J:** Legal! A primeira vitória é minha!

**S:** Não comemora tão cedo! Daruich vai de um lado para o outro por magia, mas você evita que ele o agarre.

**J (Hans):** _Vamos lá! Eu sei que pode fazer melhor!_

**S:** Vamos então para a segunda rodada

> Sílvio e Júlia rolam novamente. Sílvio obtem `+0-0`{: .fate_font} para +0 nos dados e Hans consegue apenas um `0+-+`{: .fate_font} para +1. Nesse caso, Daruich está com um _Fantástico (+6)_, enquanto Hans consegue um _Ótimo (+4)_

**J:** Dá para eu usar o meu Método ***Mais sábio do que aparenta (por isso se finge de idiota)*** para dar melhorar um pouco isso?

**S:** Se o fizer, provocará um empate, quando as condições poderão mudar!

**J:** Tudo bem! Parece que vai ser muito complicado continuar! 

> Júlia passa uma carta de baralho que estão usando como Pontos de Destino para Sílvio

**S:** OK. Então... Já sei! Quando Hans vai fazendo suas Pilhérias, Daruich se torna uma Tempestade de Areia por acidente, colocando em Risco o jovem Pastor e o Carneiro!

**V:** Hora do Príncipe em pele de Burro dar o show! Eu me coloco diante dos demais, protegendo todos!

**S:** Isso é um _Sacrifício de Vitalidade_: sabe que vai demorar para você se recuperar.

**V:** Sim... Mas é aquela: ***Heroísmo é a alma do Príncipe Encantado***

**S:** Beleza então! Quando a tempestade de areia se forma, vocês ouvem Daruich gritando _SE AFASTEM! NÃO CONSIGO ME CONTROLAR!_ Uma poderosa nuvem de areia se formando! Julian, o quão de frente você vai se colocar para a tempestade de areia?

**V:** Vou apenas dobrar o pescoço, visando proteger os olhos, mas de outra forma, formando uma barreira.

**S:** OK... Que tal isso ser uma _Consequência Suave_ ***Couro lacerado***? Pensa que é como se você tivesse o couro raspado por lixa!

**V:** Ai... Doloroso, mas fazer o quê?

**S:** E como é um _Sacrifício de Vitalidade_, vai demorar mais para que você recupere isso... O que vai manter Julian como um Burrico por algum tempo!

**V:** É difícil ser um Príncipe Encantado!

**S:** Desse modo, enquanto Daruich faz a Tempestade de Areia, os demais se ocultam atrás de Julian, e o pastor, junto com Albergine, joga um pano sobre todos para evitar os problemas da areia!

**V (Julian como Burro):** _Ióóóóóó! Ióóóóóó! Ióóóóóó! Ióóóóóó!_

**R (Aubergine):** _Só mais um pouco, Julian! Logo vai passar!_

**S:** E Aubergine está certo: tão rápido quanto começou a Tempestade de Areia provocada por Daruich, a mesma para, o Djann sentado na areia em uma forma mais humana (Daruich) _Essa foi realmente dura! Você me venceu, bufão, e por mais vergonhoso que isso seja, devo admitir que teve galhardia ao me enfrentar._

**J (Hans):** _Não seja por isso, meu caro Gênio. Todos temos um lado louco e um lado sábio. A diferença é que sou humilde o bastante para reconhecer quando alguém é mais poderoso que eu, ainda que não necessariamente o seja para fugir._

**S (Daruich):** _AHAHAHAHAHAH!!! Admirável! Plenamente Admirável! Vocẽs são divertidos! E você, bufão, compreende bem a importância da cortesia, mesmo que a ignore quando lhe convêm! Muito bem! Aceito minha derrota, e fico contente que vocês aceitem bem a vitória, sem prepotência!_ Daruich faz uma reverência.

**J (Hans):** Rotnase Hans faz o mesmo salamaleque, mas não da maneira cheia de pilhéria e pueril que normalmente faria, mas respeitosa, ainda que de uma maneira propositalmente desengonçada. Afinal de contas, Hans é ___Mais sábio do que aparenta (por isso se finge de idiota)___

**S:** Pretende usar um PD? Qual seu objetivo?

**J:** Deu para sacar que isso é um teste, e quero que Daruich ofereça informação para nós. Ele deve conhecer bastante esse lugar, e já que aparenta que a Companhia caiu nas boas graças dele, Hans vai aproveitar. (Entreja um PD e assume a voz de Hans) _Mas ora, sábio Djann, saiba que procuramos informações sobre uma certa donzela, cujo nome foi desgraçado em Al-Faizar, jogado na lama ao ser denominada Dama-Ladra, uma assassina segundo o que dizem as multidões do Sultanato. Entretanto, supomos que ela na verdade seja inocente, vítima de um torpe esquema._

**S:** _Se essa é a opinião que Al-Faizar possui da Princesa das Tâmaras,  Fatima Hakim Samara_ bint _Mudiya_ ibn _Rashid, bem se vê a decadência do Sultanato, e o quanto aquele_ Hamad Hassad _tem envenenado a mente do Sultão. Fatima só foi difamada como afirmam pois foi a única que se levantou diante do poder e da ignomia do Vizir. Não bastasse as heresias que ele estuda ao procurar sobre os meios de dominar meu povo, ele explorou o povo o quanto possível na ausência do Sultão!_

**V (Julian como Burro):** _Ióóóóóó! Ióóóóóó! Ióóóóóó! Ióóóóóó!_

**R (Aubergine):** _Isso, Julian! Pode ser que realmente tudo case agora! Se o que descobrimos com Samira for verdade, a coisa toda se torna ainda mais interessante para o Vizir, e mais perigosa para Hassam e Fatima!_

**S (Daruich):** _Como assim? Samira lhes disse alguma coisa?_

**M (Regina):** _Descobrimos com ela sobre a Magia das Tiras Mágicas, e sobre como ele aprendeu os métodos de aprisionar o seu povo. Além disso, ela falou sobre o **Tabu do Bracelete da Submissão**. Considerando o que você nos contou, faz muito sentido supor que ele deseje realmente que Fatima seja aprisionada e morta, visando com isso não apenas eliminar inimigos, no caso Hassam e Fatima, mas obter o material mágico visando aprisionar o Sultão Supremo dos Djanni._

**S (Daruich, enfurecido):** _MAS... QUE PETULÂNCIA A DE HASSAD! Visar aprisionar o Supremo Sultão? Que heresia! Ad-Demiri Omar aprendeu seu conhecimento do Senhor Supremo da Existência, antes mesmo do Tempo nascer! Ele viu os primeiros ventos soprarem, e a primeira Tâmara ser plantada! Acha ele que fará essa desfeita? Veremos! Venham comigo, eu os guiarei até o esconderijo de Fatima!_

---

E assim a Companhia consegue a pista para encontrar Fátima, mas eles não suspeitam que podem estar sendo seguidos por agentes, ou observados magicamente pelo Vizir. E seus designios odiosos visando desgraçar e matar tanto Fátima quanto Hassam, fazendo seu coração arrebentar de maneira literal em dor e sofrimento, para obter o ***Bracelete da Submissão*** para aprisionar o Sultão Supremo dos Djanni, Al-Demiri Omar. Será que a Companhia irá conseguir impedir isso? Julian recuperará sua forma verdadeira? A que custo? Poderá Regina escapar de tender ao Poder, diante de tanta Magia e Poder, o Vício que a distancia do caminho da Paz? Ou será ela a Abdicar do Desejo do Coração dela, talvez até optando por tornar-se novamente a "Rainha Má" para proteger dois amados?

Saberemos apenas... Na próxima história de _Loose Threads_
