---
title: "Exemplo de jogo em Fair Leaves"
teaser: "Quando a turma precisa realizar um Teste de Coragem, os jogadores vão explorando o cenário e as regras!"
layout: post
date: 2022-08-05
categories:
    - RPG
header: no
excerpt_separator: <!-- excerpt -->
tags:
  - Fate
  - FAE
  - Fair Leaves
  - Light Hearted
  - Aventura
  - Fair-Leaves
  - Exemplo de Jogo
  - ExemploDeJogo
---


> *Quatro jogadores estão na mesa: o Narrador, Carlos; sua esposa Maria jogando com o [**Certinho** William Shepard](/rpg/ArquetipoFairLeaves-Certinho/#personagem-exemplo---william-shepard); seu filho Rafael, que decidiu jogar com a [**Geninha** Sally Saintbern](/rpg/ArquetipoFairLeaves-Geninho/#personagem-exemplo---sally-saintbern); e sua amiga Lilian, que decidiu jogar com o [**Tímido** Hugh Hedginger](/rpg/ArquetipoFairLeaves-Timido/#personagem-exemplo---hugh-hedginger). Além disso, Carlos colocou mais um membro da Turma como NPC, um personagem especial jogado por ele, chamado [Simeon Skunder](/rpg/ArquetipoFairLeaves-Descolado/#personagem-exemplo---simeon-skunder). Eles ainda são novatos, mas já tiveram algumas sessões. Recentemente Richard McHog, o garoto mais mandão de Fair Leaves, desafiou seus personagens a um Teste de Coragem. Portanto, agora eles estão diante da assustadora Mansão Bieberaus*

<!-- excerpt -->

---

**Carlos:** _Bem... Está muito escuro na rua: mesmo os postes de luz não são o suficiente para manter alguma iluminação perto da velha **Mansão Bieberaus, a mansão mais mal-assombrada da região**._ (ele escreve essa informação em um cartão e a coloca na mesa, como um **Aspecto**, um detalhe tão importante sobre alguma coisa que pode afetar a mecânica e indica uma verdade narrativa do jogo, algo que não pode pura e simplesmente ser negado sem motivo) _Além disso, o frio está muito grande por ser outono. A única coisa que garante que vocês estejam se arriscando fora de suas camas quentinhas é a teimosia de Simeon Skunder, o **Descolado** que se considera o autoproclamado líder da Turma._ "Vamos lá, Turma... A gente tem que cumprir esse Teste de Coragem do McHog, entrar na Mansão Bieberaus e fazer tocar o relógio de carrilhão no quarto do Último Bieberaus. Se não fizermos isso, vamos ser chamados de franguinhos até o Halloween, e isso eu não vou deixar o Richard fazer com a gente!" (ele faz uma voz meio rouca e animada, como um garoto gambá meio malandro)

**Lilian (como Hugh Hedginger, fazendo uma voz meio assustada e fina de um garoto Tímido):** "Mas... Simeon... Aqui é muito escuro e assustador! E não tem aquelas lendas sobre... Fantasmas?" 

**Rafael (como Sally Saintbern, fazendo uma voz de quem se acha sabichona por ser a Geninha da Turma):** "Isso são só lendas, Hugh: dizem que o velho Lars Bieberaus era envolvido com magias, e que estava procurando algo para curar seu filho, quando os demais Patriarcas de Fair Leaves na época decidiram que não podiam mais esperar e emparedaram pai e filho para impedir que a Tifo se disseminasse pela cidade... Mas quanto a fantasmas? Se é por causa dos barulhos estranhos, é o vento batendo nas janelas tapumadas, ou a dilatação natural das ripas conforme a temperatura diminui..."

**C (como Simeon, ralhando):** "Fala mais baixo, Sally. Não podemos ser vistos, ou vão nos pegar e nossos pais vão nos colocar de castigo pra valer."

**Maria (como William Shepard, em um tom preocupado e sério de um garoto Certinho):** "A gente devia estar em casa... Não gosto disso tudo. Tem toda a pinta de ser alguma armadilha de Richard para nos colocar em enrascada. Além disso, eu tenho que obter estrôncio para os fogos de artifício para a festa de Halloween, a professora Laughton me pediu..."

**C (como Simeon):** "Quanto mais rápido a gente fizer isso, mais rápido conseguiremos esse tal estrônchulo..."

**M (como William):** "ES-TRÔN-CI-O! É obtido a partir da reação de celestita, e não é algo tão fácil de fazer, em especial depois de um dia de estudos e com essa sua ideia de topar esse Teste de Coragem do Richard. HUMPH!"

**L (como Hugh):** "Não podemos brigar, gente. Já é o ruim o bastante como está. William, vamos lá. A gente termina logo e volta para as nossas casas..."

**M (William):** "Está certo... Mas nada me tira da cabeça que vamos nos meter em alguma confusão!" (em sua voz normal) _Carlos, posso fazer com que isso conte como uma **Forçada** no Aspecto de William ser **Certinho**? Quero dizer: pode ser que não tenha nada errado nessa história, mas William tem quase certeza que vão se meter em alguma confusão séria._

> **Carlos pensa: em sua ideia original, a aventura seria algo mais simples, apenas um Teste de Coragem sem muita consequência imediata exceto permitir que Maria, Rafael e Lilian explorassem seus personagens. Mas a sugestão parece muito boa, e como eles estavam pedindo alguma coisa mais fantástica e com elementos de contos assustadores e de fadas, ele decide colocar um plano em ação.**

 **C:** _Me parece uma boa ideia. Toma um Ponto de Destino._ (ele diz, passando uma pedra de Damas que tá usando como contador de Pontos de Destino para Maria. Ele anota em um cartão a frase **A Lenda dos Últimos Bieberaus**, rememorando uma coisa que Rafael mencionou anteriormente como Sally, colocando esse Aspecto na mesa) _E você também recebe um Ponto de Destino, Rafael. Sally pensa na Lenda dos Bieberaus e como tudo sobre os Últimos Bieberaus é envolto em lendas... Conte um pouco mais, Sally, sobre o que você sabe dos Últimos Bieberaus._
 
> **Carlos quer que Rafael coloque elementos narrativos sobre a história dos Últimos Bieberaus em mesa. Na prática não serão todos esses elementos que aparecerão como parte da _Lenda dos Últimos Bieberaus_, afinal de contas, muita coisa pode e provavelmente foi aumentada por velhas mexeriqueiras. Mas certamente Carlos vai aproveitar algumas coisas sobre o que Rafael disser**

**R (como Sally, em um tom curioso):** "Sabe... É muito curioso que existam tantas lendas sobre os Bieberaus, e ao mesmo tempo tão pouca coisa seja realmente registrado. Dizem que os Bieberaus sempre foram bruxos, e que conheciam muitas criaturas no Bosque do Casebre. Ao mesmo tempo, dizem que eles ajudaram os O'Wool a vencer a velha Bruxa do Casebre, a destruindo, ainda que dizem a sua alma, dizem, seja tão podre que nem pode ir ao Inferno, estando presa ao casebre..." (Rafael então completa em sua voz normal) _Já que vamos realmente entrar na Mansão... Qual seria a melhor forma? Pelo que entendi, as janelas e portas estão todas com tapumes._

**C:** _Sim... Exceto pela porta da cozinha, que as lendas dizem que foi o único ponto que, descobriu-se depois, não foi barricado pelos Constábulos quando a Mansão foi isolada e os Bieberaus, emparedados na lareira_

**R:** _Faço um sinal com as mãos dizendo para todos avançarem, como aprendemos com os Escoteiros da Tropa Texugo, quando fomos com eles para o Jamboree em Deep Lakes, lembram?_ (ele diz, imitando um gesto para todos virem em silêncio. Lilian e Maria concordam, por Hugh e William)

**C:** _A sorte é que à essa hora da noite, não passa viv'alma perto da Mansão: mesmo durante o dia as pessoas evitam o lugar. Vocês notam, entretanto, que uma luz vai se aproximando, aparentemente de uma lanterna. Vocês imaginam que seja o Constábulo Bullfry: ele é muito antiquado em como fazer as coisas_

**L:** _Posso usar um Ponto de Destino meu no fato da **Mansão Bieberaus** ser a mais mal assombrada da região para que o senhor Bullfry não venha para cá?_

> **Não é a melhor das explicações para essa declaração narrativa, e poderia até fazer todo mundo dizer _Fala Sério!_ e tornar essa invocação inválida, mas Carlos decide não dizer pura e simplesmente não _Não_, mas sim _Sim, mas..._ e ver se Lilian consegue melhorar essa declaração, mostrando como ela seria útil. Isso é importante, pois todo uso de um Aspecto deve o fazer _indicando sua utilidade_, sem cair no _Fala Sério!_**

**C:** _Normalmente não... Mas parece interessante. Como você explicaria isso? Qual ideia você tem para que isso acontecesse?_

**L:** _E se aparecessem umas corujas em uma árvore nos fundos da Mansão e começassem a piar do nada, de maneira bem assustadora e agoureira?_
 
**C:** (Sorrindo) _Definitivamente assustador! Nem precisa pagar o Ponto de Destino, pois acho que o próprio Hugh ficaria assustado como um garoto **Tímido**, então seria uma Invocação E Forçada ao mesmo tempo. OK. Vocês ficam meio cautelosos quando o senhor Bullfry vai se aproximando, mas quando ele está para apontar a lanterna na direção da mansão, vocês ouvem um **piar agoureiro**, que faz suas cerdas de ouriço arrepiarem, Hugh, mesmo Simeon ficando com a cauda tensa, a muito pouco de mandar um jato fedido de gambá de tanto susto! O Constábulo olha e, apontando a lanterna para a árvore, vê uma pequena ninhada de corujas, mamãe coruja servindo um rato como janta para os bebês_ "Que bicho agoureiro!" _diz o Constábulo, fazendo uma mandinga qualquer para espantar o susto._ (falando como Simeon) "Puxa, que susto foi esse! Quase que a coisa ia feder!"

**M (William):** "Literalmente, você quer dizer! Já tava pronto para tapar o nariz!" _William comenta em tom de brincadeira_

**C:** _Simeon dá um sorrizinho meio tímido, mas é o suficiente para acalmar vocês o bastante para terminarem de ir até a porta da cozinha._

**R:** _Sally, por ser uma **Geninha**, deve saber como arrombar uma porta, não? Provavelmente leu nos livros do Agente Patinari sobre como arrombar uma porta e testou na porta do próprio quarto._

> **Isso é uma possibilidade, já que Sally, sendo uma *Geninha*, pode conhecer muitas coisas que outras crianças não conhece. Entretanto, Carlos pode colocar algumas restrições quanto a isso, ou mesmo demandar pagar um Ponto de Destino para justificar essa artimanha, dependendo do caso. Aqui, Carlos acha que é o bastante exigir testes, já que é um momento interessante de Sally se mostrar útil**

**C:** _Sim... Mas uma coisa é você saber, outra é você tanto ver se é possível arrombar a porta sem a quebrar quanto conseguir fazê-lo_

> **Carlos decide que é uma boa hora para um _Desafio_, com dois testes de _Superar_ para a porta: um para saber se é possível a arrombar sem a danificar, usando a Atitude _Esperta_, outro para realmente _Arrombar a porta_ usando _Ativo_. Se eles conseguirem sucesso em ambos os testes, podem continuar o Teste de Coragem. Entretanto, se falharem, muitas coisas interessantes podem acontecer, de imediato ou no futuro**

**R:** _OK... Quero então saber se consigo arrombar a porta, baseado no que já sei e fiz._
 
**C:** _A porta da cozinha dos Bieberaus é meio diferente por ser antiga, então vou considerar que é um teste **Razoável (+2)** para conseguir ver se é possível arrombar a porta. Você vai usar sua Atitude **Esperta**._

> **Em Fair Leaves, existem cinco _Atitudes_, as formas pelas quais cada um faz as coisas. Elas são: _Ativo, Desenvolto, Divertido, Esperto_ e _Fofo_**

**R:** _Legal! Justo a melhor Atitude de Sally!_ (Sally possui **Bom (+3)** em _Esperto_) _OK... Vamos lá dadinhos, ajudem a Sally._

> **Rafael rola os dados e consegue `-00+`{: .fate_font}. O `+`{: .fate_font} adicionaria 1 ao _Esperto_ de Sally em 1 para +4 (*Ótimo*), mas como o `-`{: .fate_font} reduziria o _Esperto_ de Sally em 1 para +2 (_Razoável_), os dois acabam se anulando, os 0 não servindo de nada. Mesmo assim, ainda é o suficiente para um Sucesso.**

**C:** _Sally, você observa por alguns instantes a tranca e ela parece bastante diferente: ela é maior e mais travada que a tranca do seu quarto... A única sorte de vocês é que, apesar da ferrugem, ela não aparenta estar emperrada. Será uma dificuldade __Razoável (+2)__ em um teste de __Ativo___

**R:** _Oh, puxa! Justo a minha pior Atitude! Que dia para a Rita não poder jogar, Sara seria muito útil aqui!_ (Sally possui **Regular (+1)** em _Ativo_)

> Rita é uma jogadora que joga com a **Travessa** irmã de William, Sara Shepard, mas que justo hoje não pode participar por estar doente

**M:** _Espera um pouco! E se eu lembrar que meu **Canivete de Escoteiro** está comigo? Seria possível, Carlos?_ 

**C:** _A ideia é boa... Mas acho que você não poderia usar **Esperto** nesse caso para **Criar Essa vantagem**. Que tal se for por **Divertido**? E vou colocar como dificuldade **Ótima (+4)**, já que não é uma coisa que você, enquanto **Certinho**, anda carregando por aí?_

**M:** **GULP!** _OK... Vamos lá então!_ (diz Maria, apreensiva)

> **Ao rolar os dados e conseguir um excelente `+0++`{: .fate_font}. Como cada `+`{: .fate_font} adiciona 1 ao seu _Divertido_ que é *Razoável (+2)*, ela consegue um resultado *Extraordinário (+5)*. Carlos considera Invocar de Maneira Hostil o Aspecto da _Mansão Bieberaus_ para aumentar em +2 a dificuldade do teste, usando 1 dos Pontos de Destino de sua Reserva para a cena (que são 3, um para cada personagem mais importantes), mas acha que é melhor para a aventura não arrumar complicação nesse momento.**

**C:** _William, conforme você remexe seus bolsos, você nota que seu **Canivete de Escoteiro** está no bolso de trás. Provavelmente você esqueceu ele ali depois de estudar como emendar cordas para o próximo Exame dos Escoteiros._ **(Carlos puxa um cartão e anota nele _Canivete de Escoteiro_, fazendo um desenho de uma caixinha no topo do cartão)** _Lembre que agora você pode usar essa Invocação Gratuita_ (**Ele diz apontando a caixinha**) _para invocar o Canivete uma vez sem precisar pagar Pontos de Destino._ **(Ele termina, entregando o cartão para Maria)**

**M (William):** "Acho que isso aqui deve ajudar, Sally. Acabei de notar que estava com ele." **(diz Maria entregando o cartão para Rafael. Agora quem pode usar o _Canivete_ e a _Invocação Gratuita_ nele é Sally, não William, já que Sally é quem está com o _Canivete de Escoteiro_)**
 
**R: (Sally)** "Sim... Isso vai vir bem a calhar! Vamos nessa!" _Eu vou usar o **Canivete de Escoteiro** do William para, com o maior cuidado possível, arrombar a porta, usando a Invocação Gratuita para auxiliar nesse caso. Isso deve somar +2 ao **Ativo** de Sally para **Bom (+3)** para esse teste, certo?_
 
**C:** _Bem, creio que você sabe que pode esperar os dados rolarem para Invocar Aspectos para um novo rolamento ou +2 aos dados, não?_

**R:** _Sim, mas esse é um resultado que precisamos bastante, não? Se não entrarmos na Mansão, nada de tocar o Carrilhão e a Turma vai passar vergonha com o McHog, não?_ (Rafael termina, marcando com um X a caixa da Invocação Gratuita, indicando seu gasto)
 
**C:** _Tudo bem... Sabe que se falhar ainda assim a Invocação Gratuita vai ser perdida, certo? Então vamos lá._
 
> **Rafael rola os dados e consegue `+-0+`{: .fate_font}. Normalmente, o resultado +1 no dado, somado ao +1 em _Ativo_ de Sally seria o suficiente apenas para um _Razoável (+2)_, portanto para um _Empate_, onde eles emperrariam a porta, danificariam a tranca ou alguma coisa similar, mas sem maiores consequências. Entretanto, Rafael marcou a caixa da Invocação Gratuita, para a usar, como tinha descrito, e isso somou +2 ao _Ativo_, o que resultou em um sucesso**
 
**C:** _Conforme você vai lentamente colocando a parte de abridor de lata do canivete como uma gazua improvisada, você consegue ir mexendo na mesma até que, depois de alguns instantes bastante tensos, você ouve um **click**. Você usa a maçaneta e a porta se abre com um rangido meio assustador..._
 
**R (Sally):**  "Obrigada, William. Esse canivete veio mesmo a calhar."
 
> **(Rafael devolve o cartão para Maria. Apesar da Invocação Gratuita ter sido usada, o Aspecto do _Canivete_ pode ser novamente utilizado sempre que desejado, seguindo as mesmas regras de qualquer outro Aspecto. Apenas se algo acontecer que justifique o aspecto sumir, como o canivete ser roubado ou cair em alguma fenda que ele não tenha como pegar de volta, o Aspecto do Canivete sai em jogo)**
 
**L:** _Hugh pode ter lembrado de trazer lanternas? Sabe, como a Turma **São as pessoas que me Aceitaram desde que cheguei**, Hugh não os deixaria na mão e com certeza daria um jeito de ter algo útil com ele._
 
**C:** _Me parece válido... Mas como são muitas Lanternas, não tem invocação gratuitas nesse caso._

**L:** _Está tudo bem... O importante é que isso não vai permitir que a escuridão seja invocada contra nós!_
 
> **Lilian usou uma _Declaração Narrativa_ para trazer as _Lanternas_ como equipamento para o jogo. Carlos escreve os Aspectos em Cartões e diz, entregando os cartões para todos. Carlos sorri ao notar que Lilian foi bem esperta: ele *REALMENTE* tinha a intenção de trazer a *Escuridão* como um Aspecto para usar contra os personagens.**
 
**C:** _Conforme vocês ligam as lanternas, vocês vão entrando na velha cozinha. Um fogão de metal pesado está ligado a uma velha lareira, e uma mesa meio carcomida está no meio da mesma. Vocês conseguem ouvir os guinchos de ratos por aí... E nessa hora, quando vai entrar, por alguma razão vocês ouvem Simeon gritar de medo e escorregar na entrada, torcendo o pé e se Machucando. O pior é que aos poucos o garoto gambá perde o controle sobre seu mal cheiro, que começa a aparecer._ (como Simeon) "Droga... Que fique entre a gente, mas eu tenho um baita medo de ratos. Uma vez um primo meu..."

> **Carlos precisava de uma desculpa para remover Simeon da cena, já que agora que os personagens dos jogadores vão entrar, ele não é mais tão importante. Para isso, ele fez Simeon se Machucar com o susto dos ratos. Ao mesmo tempo, isso permitiu a ele fazer Simeon perder o controle sobre o mau-cheiro de gambá dele, e com isso fazer algo que obrigue os jogadores a fazer Simeon voltar para casa**
 
**R (Sally, segurando o nariz e fazendo uma voz anasalada):** "Sei, sei, sei... Mas se você não pode continuar, melhor voltar para casa, ainda mais que pelo jeito você machucou o pé."
 
> **Desse modo, Simeon deixa a cena, deixando apenas Hugh, Sally e William no teste de coragem**
 
**C:** _Conforme Simeon se afasta, manquitolando, o cheiro de gambá deixa a cozinha._
 
**M: (William, abanando a mão na frente do rosto)** "Puxa vida! Eu sempre me impressiono como Simeon fede quando perde o controle." 
 
**R (Sally):** "Vamos continuar... Já estamos aqui mesmo... É melhor que consigamos fazer isso, ou todo esse esforço do Simeon será em vão!" (Rafael complementa) _Dá para notar que Sally realmente ficou triste por Simeon ter que ir embora_
 
**C:** _Bem. Seja como for, vou considerar agora uma **Disputa** para ver se vocês conseguem seguir adiante com tranquilidade ou se a escuridão e rangidos vão fazer vocês correrem como um bando de galinhos. A **Mansão** conta como uma oposição "ativa" __Razoável (+2)__ nesse caso, já que podem existir ratos, baratas, madeira podre e todo tipo de sons assustadores para perturbar vocês, mesmo que ela não seja inteligente. O "objetivo" dela é fazer vocês fugirem de medo, enquanto o de vocês é chegar onde está o Relógio de Carrilhão. Lembrem que serão turnos de ambos os lados, enquanto vocês vão caminhando pela Mansão. Qual de vocês vai rolar na Disputa? E os demais vão fazer alguma coisa? Como vocês vão andar? Pensem um pouco e depois a gente começa._
 
> **Enquanto rapidamente os jogadores confabulam e planejam suas ações, considerando as opções de regras, em um papel à parte, meio escondido, Carlos rascunha uma "ficha" da Mansão Bieberaus. Nesse caso ele usa uma ideia do Fate chamada de _Fractal_ para "transformar" a Mansão em uma oposição "ativa" e, com isso, usar a regra de Disputas. Ele poderia o fazer com oposição passiva, mas seria chato. Então ele usa o Aspecto *Mansão Bieberaus, a mansão mais mal-assombrada da região* que já está na mesa e inclui um segundo Aspecto *Segredos antigos escondidos* para caso precise apertar as coisas um pouco mais. Por fim, ele coloca uma Atitude _Assustadora_ com nível _Razoável (+2)_. Ele não precisa de mais nada, já que como é uma Disputa, onde nenhum deles querem provocar prejuízo (o que seria um Conflito) e espera os jogadores decidirem. Tudo isso não levou mais que dois minutos**
 
**L:** _Se andarmos com muita cautela, podemos usar **Esperto**? Me parece algo assim, já que seria Esperto em uma invasão em casas ter cautela._ (pergunta Lilian, após os jogadores confabularem)
 
**C:** _Não nesse caso: o local é muito assustador,é meio complicado de pensar direito. Vocês estão meio tensos, ainda mais depois do susto que Simeon tomou. Agora, se quiserem, podem usar **Desenvolto**, como se uns tivessem demonstrando aos outros mais coragem do que realmente têm_

**M:** _Todos temos **Desenvolto** em **Razoável (+2)**. Podemos ainda assim usar **Trabalho em Equipe**. Tipo... Todo mundo dando apoio um ao outro?_
 
**C:** _Claro! Lembrando que apenas um de vocês irá rolar por turno e os demais não poderão fazer mais nada se o fizerem, mesmo gerar vantagens usando as lanternas para iluminar o caminho, o que poderia ser útil._
 
> **Carlos citou a regra de Trabalho em Equipe: como eles vão sacrificar ações para que um deles role na Disputa... Cada um deles irá oferecer +1 ao personagem que for rolar, mas sem poder fazer mais nada durante o turno.**
 
**L:** _Parece ainda assim garantir com o Trabalho em Equipe! Beleza! Então temos no caso **Ótimo (+4)** nos rolamentos, certo? Posso rolar pela gente?_ **Pergunta Lilian, enquanto os demais fazer que sim com a cabeça** 
 
**C:** _Vamos lá então. Relembrando: quem for melhor no rolamento ganha 1 Vitória para o seu lado, 2 caso tenham sido sucedidos com estilo, e que é uma melhor de cinco: quem conseguir três vitórias para o seu lado, vence._ **(Carlos e Lilian pegam os dados)** _Primeira rodada!_
 
> **Lilian rola na primeira um `+-+-`{: .fate_font}, não mudando o _Ótimo (+4)_ da Turma. Carlos rola `000+`{: .fate_font}, colocando o _Assustadora_ da Mansão em _Bom (+3)_** 
 
**C:** _Bem... Normalmente seria uma vitória para vocês, mas como a **Mansão Bieberaus é mal-assombrada**, vou Invocar isso para aumentar em +2 meu rolamento, elevando ele para **Excepcional (+5)**. Com isso, consigo a vitória nessa rodada!_
 
**L:** _Puxa vida! Não tem nada o que a gente possa fazer? Que tal a gente pagar PD também em algum Aspecto nosso?_
 
**M:** _Deixa por agora, Lilian... Ainda temos pelo menos mais quatro turnos para dar a volta por cima. Não precisamos ir tão rápido._
 
> **Carlos sorri. Maria entendeu o plano pelo jeito**
 
**C:** _Enquanto vocês caminham, vocês notam que a madeira é meio podre: dá para ouvir os rangidos quase como se ela quisesse estourar a qualquer momento sob seus pés. Isso não ajuda muito quando vocês notam que não conseguem ver nada fora de onde a luz das lanternas bate, e os barulhos de ratos e outros bichos nojentos vai aumentando._
 
**R (Sally):** "Okay... A coisa tá realmente ficando complicada agora"
 
**C:** _Segunda altercação então_ (diz Carlos, enquanto ele e Lilian pegam os dados)

> **Lilian Consegue outro `+0++`{: .fate_font}, sortuda, elevando a turma para um _Épico (+7)_, enquanto Carlos consegue apenas `+0-0`{: .fate_font} para a Mansão, mantendo a mesma em _Razoável (+2)_. Re-rolar com 1 PD não é uma boa ideia, pois poderia gerar um rolamento ainda pior, e ele não tem PDs *ou Aspectos* o bastante para melhorar o rolamento da Mansão, é melhor ele manter a coisa como está e dar o Sucesso com Estilo para a Turma. Os jogadores sorriem ao perceber que Carlos não tem muito o que fazer nesse caso pela cara meio frustrada que ele faz)**
 
**C:** _Um Sucesso com Estilo! Portanto, está 2 a 1 nessa Disputa para vocês._ (Ele diz, recuperando a compostura) _Vocês avançam até a velha sala, onde tudo parece bem comum, seja o sofá e as poltronas velhas, seja o balcão com uma prateleira atrás cheia de vidros e caixas. A coisa mais estranha, e assustadora, é a parede de tijolos que circunda a velha lareira, onde diz a Lenda que tanto o pai Lars quanto o filho Kaspar foram emparedados a mando dos demais Patriarcas de Fair Leaves da época_ (ele completa)
 
**R:** _O que Sally sabe sobre as coisas nas prateleiras?_
 
**C:** _O senhor Lars Bieberaus era apotecário, ou seja, fazia remédios a partir de substâncias extraídas de plantas e afins. Possivelmente todas as caixas contenham tais substâncias, mas obviamente estão vencidas e portanto são muito perigosas._
 
**L (Hugh, interpretando a situação):** "O que tem nessas caixas?"

**R (Sally):** "Deve ser o material de apotecaria do senhor Lars Bieberaus?"
 
**L (Hugh):** "A-po-o quê?"
 
**M (William):** "Uma forma antiga de fazer remédios usando substancias extraídas de plantas... Algumas dessas substâncias inclusive são muito perigosas, como Arsênio, Láudano, Beladona..."
 
**R (Sally):** "HUM... HUM... Podemos continuar?" (ela diz, demonstrando a irritação de Sally)
 
**C:** _Muito bem... Terceira rodada!_
 
> **Lilian e Cláudio vão para os dados novamente. Com um `+-+-`{: .fate_font}, Lilian fica com o _Ótimo (+4)_ da Turma contando o Trabalho em Equipe. Entretanto, Carlos rola `-+-0`{: .fate_font}, reduzindo em 1 o _Razoável (+2)_ da Casa para _Regular (+1)_. Carlos ainda tem 2 Pontos de Destino para essa cena: como são três personagens importantes na mesma pela Turma, começou com 3, mas ele gastou um lá atrás. Ele decide não facilitar demais as coisas para a Turma dessa vez.)**
 
**C:** _Bem... Normalmente vocês venceriam agora essa Disputa... Mas como a **Mansão Bieberaus é a mais mal-assombrada da região** e ela parece ter **Antigos Segredos escondidos**, invoco esses 2 Aspectos para +4, subindo meu rolamento para **Excepcional (+5)**. Após darem uma olhada na parede de tijolos que serve de mausoléu aos últimos Bieberaus, vocês meio que fraquejam, indo muito mais na teimosia que na coragem para o andar de cima. Isso fica ainda mais patente conforme vocês vão ouvindo, passo após passo, o ranger da escadaria para o andar de cima da casa, quase como se a madeira estivesse tão podre e carcomida que a qualquer momento ela pudesse estourar!_

**L (Hugh):** "**GULP!** Definitivamente não devíamos estar aqui! Estou com medo!"
 
**M (William):** "C-C-Calma, Hugh... É só a madeira. Logo chegaremos lá ... Não é, Sally?" (diz assustado)
 
**R (Sally):** (Em um tom nervoso) "Aconteça o que acontecer, não façam movimentos muito bruscos. Essa madeira deve estar realmente muito podre pela umidade da casa, pode estourar a qualquer momento se não formos cautelosos."
 
**C:** _Bem... Está 2 a 2, então esse rolamento fechará a Disputa... Vocês vão conseguir chegar no quarto do Último Bieberaus, ou vão correr para casa, gritando que nem franguinhos assustados?_ (ele diz, pegando os dados)
 
**L:** _Nada disso... A gente vai chegar no Relógio de Carrilhão!_ (também pegando os dados)
 
> **Carlos tem um baita azar, rolando um terrível `-0--`{: .fate_font} para a Casa justamente após queimar seus Pontos de Destino, para um resultado *Ruim (-1)*. Mesmo com o rolamento de `-+0-`{: .fate_font} de Lilian, ela consegue um *Bom (+3)* para a Turma, fechando a disputa em 4 a 2 para a turma, o último rolamento sendo outro Sucesso com Estilo para a Turma. Ele sorri de qualquer modo, já que esse momento foi muito assustador, o que é bom nesse caso, pois foi divertido e ajudou a aventura a ir em frente.**
 
**C:** _Bem, vocês vencem a Disputa e com isso conseguem continuar e, seguindo os conselhos de Sally, chegam até o piso superior da casa, onde ficam os quartos. Em especial vocês visam o Quarto de Kaspar Bieberaus, o filho de Lars com Adalgisa Bieberaus._ (Diz Carlos desenhando um mapa rápido do andar superior) _Existem três quartos na casa: o maior, que pode-se supor que seja o dos pais de Kaspar; o do meio, que é na realidade o menor, que parece algum tipo de quarto de hóspedes e o mais no fundo do corredor, onde provavelmente Kaspar viva. Por alguma razão, a madeira nesse piso superior parece mais reforçada, então vocês podem caminhar com maior tranquilidade!_
 
**M (William):** "Puxa vida! Essa me deixou com o coração na mão. Fiquei com medo da madeira estourar debaixo dos meus pés." (diz ele aliviado) _Vamos na direção do quarto de Kaspar! William quer tocar esse relógio e se mandar, não ficar olhando tudo que tem na casa!_
 
> **Carlos tinha em suas anotações prévias tudo sobre a Mansão Bieberaus, exceto pela ideia que desenvolveu a partir da Forçada de William lá atrás. Essas notas podem ser úteis no futuro, mesmo não o sendo no momento**
 
**C:** _Quando vocês abrem a porta, um cheiro de guardado e de pó empesteia vocês. Vocês tossem ao notar a poeira no quarto. É um quarto bem simples: uma cama velha e puída, com um colchão de penas que deve certamente ter visto dias melhores coberto pelos restos de um cobertor carcomido pelas traças; uma mesa velha com uma cadeira de madeira bem simples; uma cadeira de balanço onde pode ser vista uma almofada velha coberta por uma manta ainda mais velhas; um pequeno console onde pode-se ver alguns brinquedos e livros; e um velho armário de roupas. O Relógio de Carrilhão está bem perto do armário_
 
**R (Sally):** "Interessante... Os brinquedos são velhos: pião, xilofone... E esses livros são tão velhos quanto: uma gramática de Alemão, um livro de inglês, uma tabuada, uma velha Bíblia..."
 
**L (Hugh):** "O que será que tem no armário? Que roupas eles usavam?"

**C:** _Hugh, quando você abre o armário, um cheiro muito forte de mofo sai. Você percebe que as roupas estão cheirando mal pelo tempo em que estão guardadas, mas apesar de tudo, por um milagre elas ainda estão inteiras. Parecem terninhos que crianças pequenas usam, mas para o tamanho de crianças de mais ou menos de 7 anos de idade._
 
**L (Hugh):** "Eca, essas roupas estão fedendo... Mas parecem bem inteiras... Será que não daria para lavar isso?"
 
**R (Sally):** "Os sabões modernos para limpar roupas destruiriam elas, em especial no estado em que estão agora. São roupas feitas de materiais mais simples, como linho, seda, musselina e algodão, mais delicados que tecidos atuais. William, o que você está fazendo?"
 
**M (William):** "Quero tocar esse relógio e me mandar!" (afirma determinado) _O relógio está funcionando? Esses relógios costumam precisar de dar corda, não?_
 
**C:** _Sim... Você pega a chave e vai a colocar no mecanismo... Mas conforme você tenta girar, você nota que o mecanismo está emperrado por dentro, o que não deixa você dar a corda._
 
**M (William):** "Sally, preciso de ajuda aqui.  O relógio está emperrado!"
 
**R (Sally):** "Já estou indo, Will!" _Quando Sally chega perto, ela pede para William o **Canivete de Escoteiro**. Será que meu **Esperto** é útil nesse caso?_
 
**C:** _Na realidade, é a única opção, já que é a única atitude que explicaria a possibilidade de você conhecer como funciona um relógio de carrilhão. Apesar de antigo, o mecanismo de um relógio de carrilhão  funciona do mesmo jeito atualmente, portante a dificuldade **Razoável (+2)** é o suficiente nesse caso._
 
 **R (Sally):** _Vamos então!_ (Rafael rola os dados é consegue `0+-0`{: .fate_font}, ficando com o ***Bom (+3)*** de Sally) _Beleza... Consegui!_
 
> **Carlos sorri... Fosse pelo planejado, a aventura acabaria nessa cena com eles dando a corda no relógio, o tocando e indo embora. Mas ele tem que lidar com a Forçada de William, e é aqui que a complicação aparece de vez**
 
 **C:** _Lembra que você disse que teria alguma confusão, William, bem lá atrás?_ (A Turma se entreolha e notam que esqueceram a complicação de William) _Bem... Conforme Sally consegue remover um objeto que estava emperrando o carrilhão, você começa a dar corda, William, até que ouve o ranger dos mecanismos do relógio voltando a funcionar... E eles começam a tocar! Vocês olham para os relógios de pulso de vocês e notam que é meia-noite!_
 
**M (William):** "Essa não! Está tarde para caramba! Mamãe deve ter ido ver o meu quarto! Me danei! Estou muito encrencado"
 
**C:** _Na verdade, William, quando você vê o relógio começando a se mexer... Você fica MUITO ASSUSTADO, e a Turma com você, pois vocês notam o relógio andando PARA TRÁS. Dos ponteiros meio velhos e do corpo com pintura descascada, um brilho dourado e estranho começa a surgir, e se refletir em tudo no quarto... Ao mesmo tempo, um sono esquisito, pesado, começa a tomar vocês de assalto, quase como se algum dos **Velhos segredos escondidos** na Mansão Bieberaus estivesse para se revelar._ (Carlos usa o Aspecto da Mansão para Forçar a Turma como um todo, oferecendo 1 PD para cada um. Eles se olham, entendem como a Forçada vai funcionar e decidem aceitar)
 
**L (Hugh):** "Sally? O que estAAAAHHH AcontecendoOOOOhHHHH!" (diz, sonolento) _Hugh, por ser leve e pequeno, vai se deitar no chão cautelosamente._
 
**R (Sally):** "Eu... não... seiiiiii..." (diz, como se tivesse a ponto de desmaiar) _Sally se deixa cair na cama, torcendo para que ela seja resistente o bastante para não despencar com ela até o andar de baixo!_
 
**M:** _William imita Hugh e se deita no chão, mas tenta ficar o mais lúcido possível, tentando não dormir... É possível algum rolamento?_
 
**C:** _Infelizmente não nesse caso, William, já que é parte da Forçada recente da **Mansão Bieberaus**, cujos **Velhos segredos escondidos** fazem vocês caírem no sono. Mas conforme você vai apagando, William... Curiosamente você vai notando os objetos ficando cada vez mais novos: pinturas ganhando cor e textura de novas, a madeira do próprio chão ficando mais reforçada... Quase como se vocês estivessem realmente voltando ao passado. Até o momento que o sono é pesado demais para resistir e você apaga_
 
> **Todos olham meio assustados, se perguntando o destino de seus personagens, apesar de saberem que ainda não é o fim. Carlos começa a pegar os cartões de Aspectos em cena e os retirando da mesa, alguns realmente sendo rasgados, como os das Corujas, outro guardando para uso futuro. Ele deixa apenas o da *Mansão Bieberaus*. O Aspecto do *Canivete de Escoteiro*, entretanto, ainda permanece com Maria, já que não aconteceu nada para que o Canivete saisse de jogo**
 
**C:** _E com isso abrimos uma nova cena. Hugh... Você é o primeiro a sentir o sono o deixando. É quase como se você tivesse tido uma bela noite de sono na sua cama na Fazenda Hedginger... Mas você nota que você ainda está deitado no chão... Conforme você se levanta, a primeira coisa que você nota é que tudo ao redor parece novinho em folha. Além disso, a janela está aberta com o sol forte aparecendo... Você olha para o lado e o relógio de carrilhão mostra 9 horas, sendo que ele está andando normalmente, como um relógio deveria andar, quase glorioso na beleza e no quão novo ele se encontra. Você vê que Sally e William estão próximos... Mas tem algo errado com eles: as roupas dos mesmos estão diferentes, as de William lembrando as roupas que estavam no Armário. Além disso, você olha na Cadeira de Balanço e tem um garoto ali, um garoto castor, a cauda dele passando pelos vãos da cadeira. Apesar do clima gostoso que está fazendo, você nota que ele está usando capotão e boina, além da manta cobrindo seus pés, e um cachecol, como se ele tivesse em um frio intenso..._
 
**L (Hugh):** _Hugh está realmente assustado, mas não o bastante para ficar congelado... A primeira coisa que ele vai fazer é tentar acordar William._ (Ela então fala como Hugh, assustado) "William! Acorda! Alguma coisa aconteceu depois que você tocou o relógio! Alguma coisa muito esquisita!"
 
**M (William):** "Só mais cinco minutinhos, mamãe..." (diz, manhoso)
 
**R:** _Sally começa a acordar também?_
 
> **Carlos pensou em deixar rolar uma cena mais prolongada de Hugh assustado tentando acordar William, mas decide acelerar as coisas**
 
**C:** _Sim, Sally. Conforme você acorda, você sente que a cama está fofa e firme novamente, não o tijolo puído como o no qual você se deitou. Você nota todas as coisas que disse a Hugh. Além disso, você ouve Hugh tentar acordar William. Você observa que as roupas dos dois parecem roupas antigas, como as que estavam no armário. No caso, Hugh está com um terninho verde-limão bem comportado, com calções e meias longas, e um par de sapatos de couro. William parece mais ou menos no mesmo estilo, mas com uma roupa um pouco mais larga e com um boné, em azul claro._
 
**R:** _Posso usar minha façanha **Já li sobre isso** para ter alguma noção do que nos ocorreu? É tudo muito estranho, e não quero que Sally seja pega de calça curta._
 
> **Rafael está tentando usar a Façanha Exclusiva como de Sally Geninha *Já li sobre isso*. Ele poderia usar 1 PD para conseguir isso *OU* usar a Invocação Gratuita dessa Façanha que ela tem por sessão, mas Carlos pensa um pouco diferente**

**C:** _Normalmente não seria tão fácil assim por toda a estranheza que está acontecendo, você dormir e acordar em um local completamente similar mas ao mesmo tempo completamente diferente... Mas se você topar usar a invocação gratuita dessa Façanha que você tem direito, para mim está ok._
 
**R:** _Feito! Não acho que seja uma boa ideia Sally ser pega de calças curtas_
 
**C:** _OK... Sally, conforme você vai vendo tudo, você fica assustada e sua reação imediata por algum motivo é se beliscar. Sentindo uma leve dor, você logo nota que as coisas são ainda mais estranhas do que aparentam. Você consegue, depois de alguns minutos, racionalizar algo que, por mais maluco que seja, parece a melhor explicação do que aconteceu com vocês, considerando tudo que você sabe. Apenas guarde o **Aspecto** que estou te passando mais um pouquinho_ 
 
 > ***Normalmente Carlos passaria Aspectos de maneira aberta, mesmo que fossem apenas para um ou outro personagem, mas ele passa escondido o Aspecto para Rafael para tornar as coisas um pouquinho mais interessantes e deixar o clima um pouco mais tenso. Rafael o lê e acena com a cabeça, virando o cartão ccom o Aspecto para baixo***
 
**M:** _William acorda agora e observa os dois, com as roupas antigas, e fica assustado_ (William) "O que aconteceu? Onde estamos? O que foi aquele brilho do relógio?" (voltando a falar como Maria) _William se volta e vê o relógio andando normalmente... Aliás, como Sally está vestida?_
 
**C:** _Sally está usando um vestido amplo, com ao menos um saiote, anáguas e ceroulas. O vestido é em um tom de marrom, meio ocre, mas um pouco mais claro. Além disso, ela tem luvas nas mãos, botas longas nos pés e um grande chapéu na cabeça, cheio de rendas e outros fru-frus._
 
**L (Hugh, fungando):** "Eu queria estar em casa! Nem sei onde estamos! Nem quando! Podemos estar em qualquer lugar! Não parece nem estarmos no nosso tempo!"
 
 **C:** _Quando vocês falam isso, vocês notam um senhor gambá, vestido em um terno bem severo, gravata escura com camisa branca, apenas o focinho permitindo determinar ele ser um gambá pela faixa branca que desce do pelo preto do rosto, tirando um chapéu-coco ao entrar no quarto e dizer:_ (ele diz, com um tom profundo e professorial) "Guten Morgen, mein kleine Herrchen Kaspar." _E o garoto na cadeira de balanço, com uma voz fraquinha e doente diz:_ (fazendo uma vozinha baixa e fina, meio cansada) "Guten Morgen, Lehrer Stinktier!"
 
**M (William):** "Desculpa senhor, a gente já está de saída!" _Diz William, se aproximando_
 
__C:__ _Você nota que o homem o ignora sumariamente, mas o garoto parece ter notado você de alguma forma, ainda que se foque no homem_ "Alguma coisa errada, Kaspar?" (diz Carlos com a voz do professor Wilheim) "Nada, professor..." (diz Kaspar) _Vocês conseguem notar que a voz de Kaspar dá a entender que ele notou que tem algo errado por ali, quase como se soubesse que tem outras pessoas ali._
 
__R:__ _Ao notar isso, Sally diz, como se todas as peças definitivamente se encaixassem_ "Interessante... Parece que não nos notam. Ao menos não totalmente"
 
__M (William):__ "Como assim? Faz alguma ideia de onde estamos? E o que houve?"
 
__R (Sally):__ "Bem... Parece loucura o que vou dizer... Mas é a melhor opção considerando tudo que sabemos, e a mais simples, então a Navalha de Occam se aplica, em especial considerando tudo o que conhecemos sobre a Mansão e sobre os Bieberaus. Por alguma razão, viemos parar no passado, e o fizemos por magia!" _Eu revelo o Aspecto então_ (diz Rafael, virando o cartão para cima, com o Aspecto **Levados ao passado**)
 
__M (William):__ "Espera um pouquinho aí? Viagem ao passado? Magia? Como assim? Logo você falar disso, Sally?"
 
__R (Sally):__ "Qualquer outra opção é tão ruim quanto essa. Me belisquei ao acordar, e notei que sentia dor, o que não acontece quando você está sonhando. Além do mais, é muito raro sonhos que todos sonhem do mesmo jeito e ao mesmo tempo. Claro, considerando a existência da  magia, isso se torna algo possível e factível, e sabemos que as lendas afirmam que o Senhor Bieberaus era envolvido com magia. Somado a isso, eu lembrei que, pouco antes de apagar, eu notei que a cama estava ficando como nova, como se voltasse no tempo também. Por fim, notem que todos os objetos, exceto por estarem muito novos, estão no mesmo local onde estavam em nosso tempo. Agora, para que tudo isso?"
 
__L (Hugh):__ "E quanto a essas roupas? Onde estão as nossas?"

__R (Sally):__ "Bem, partindo  do pressuposto que estamos no passado e viemos por magia, isso explicaria as roupas: se alguém puder nos ver, o que não parece muito normal, seria um tanto estranho ver crianças com roupas do século XX. Se estamos na época dos Bieberaus, devemos estar mais ou menos em 1870."
 
__M (William):__ "Isso são mais de 100 anos do nosso tempo!" (ele diz Espantado)
 
__R (Sally):__ "Exato... Agora... Se estamos no mesmo quarto em que estávamos, e essa é uma suposição lógica de ser feita, por tudo que vimos, é de se supor que ainda estamos em Fair Leaves, só não na **nossa** Fair Leaves, mas a Fair Leaves de quando ainda tinham os Bieberaus, e que estamos ainda na Mansão Bieberaus... Afinal de contas, esse" (Sally diz, apontando) "é Kaspar Bieberaus."
 
__L (Hugh):__ "Aquele que foi emparedado?" 
 
__R (Sally):__ "Sim... E ele parece estar bastante doente..." (Sally diz, e Carlos dá umas tossidas por Kaspar)
 
__M (William):__ "Mas por que tudo isso? Quero dizer... Se o senhor Bieberaus é tão poderoso por magia que pode trazer pessoas do futuro... Por que ele não cura seu filho?"
 
__L (Hugh, meio choroso):__ "E acima de tudo... Vamos poder voltar para casa?"

__R:__ _Acho que é isso que teremos que descobrir, não é, pai?_
 
__C:__ _Com certeza... E vocês já possuem algumas pistas. O que vão fazer agora?_

---

> Agora cabe aos jogadores descobrir como voltar para seu tempo... E se vão poder impedir o destino terrível que eles sabem que ocorreu (ou ocorrerá) aos Bieberaus... E quais serão as consequências de tais ações em seu próprio futuro.
