---
title: "Exemplo de jogo em Young Centurions"
#teaser: "Quando a turma precisa realizar um Teste de Coragem, os jogadores vão explorando o cenário e as regras!"
layout: post
date: 2022-08-20
categories:
    - RPG
header: no
excerpt_separator: <!-- excerpt -->
tags:
  - Fate
  - FAE
  - Aventura
  - Exemplo de Jogo
  - Young Centurions
  - ExemploDeJogo
---


> Hélio está narrando [_Young Centurions_](https://evilhat.com/product/young-centurions/) para alguns amigos, que representam _Jovens Espíritos do Século 20_, jovens que incorporam as esperanças e proezas do Novo Século em 1911. No caso:
> 
> + Elisa, que está com o personagem [Nicola Castrogiovanni, o Espírito do Otimismo](http://fabiocosta0305.gitlab.io/personagens/NicolaCastrogiovanni-Brasil/)
> + Cristina, que está com a personagem [Mayfield Wright - O Espírito da Filantropia](http://fabiocosta0305.gitlab.io/personagens/MayfieldWright/)
> + Viviane, que está com o personagem [Sidney “Knacks” Sheeran - O Espírito da Desenvoltura](http://fabiocosta0305.gitlab.io/personagens/SidneySheeran/)
> + Fernando, que está com o personagem [Mairead Mag Raith (ou McRae) - O Espírito da Comunidade](http://fabiocosta0305.gitlab.io/personagens/MaireadMagRaith/)
> 
> Durante uma apresentação do circo no qual Nicola atua como um pequeno palhaço e cantor, sob a alcunha de _Nicolino_, que ocorria no Central Park em Nova York, uma briga esquisita estourou, pessoas avançando contra outras pessoas completamente alucinadas. Esse evento não foi isolado, já que algo similar ocorreu durante um jogo de baseball dos _New York Highlanders_ (futuro New York Yankees) e em outras partes da cidade. 
> 
> Depois de algumas investigações, auxiliados por um Tenente da Polícia que é também um Patrocinador do _Clube do Século_, uma entidade que funciona para ajudar a desenvolver os Espíritos do Século, descobriram que todos os envolvidos estavam sob efeito de uma espécie de substância chamada [_Fortivitus_](http://fabiocosta0305.gitlab.io/aventuras/Fortivitus/), que ajudaria as pessoas a trabalhar com mais força, mas sob risco de ficarem loucas ou violentas.
> 
> Então, mesmo não sendo muito responsável, os mentores dos jovens Espíritos do Século foram para as docas, com a ajuda da mentora de Mairead, ["Lady" Brigit Danaan](http://fabiocosta0305.gitlab.io/npcs/BrighitDanaan/) uma velha tida como louca, mas que foi o Espírito da Comunidade do Século 19.
> 
> Então, a cena começa com eles chegando ao East Side, uma charrete deixando-os pouco antes da ponte que liga Manhattan e Nova York ao East Side e ao Porto.

<!-- excerpt -->

---

**Hélio:** Pois bem... Vocês já meio que desconfiam que tem algo de errado no Porto, segundo o que o Tenente McLaggen disse. Parece que, quem seja que produza e fornece essa droga trabalha nos antros mais sujos do Porto, locais que pessoas como você, Mayfield, evitaria ir não importa como.

**Fernando (como Mairead):** _A bonequinha de luxo tá com medo de entrar? Não deve ser nada como o que acontece em Clinton._

**Cristina (como Mayfield):** _Ora bolas, Mairead! Acha que não tenho coragem para isso?_

**F (Mairead):** _Apenas me pergunto se você dá conta, ainda mais com essas roupas muito luxuosas..._ **(Fernando volta a sua voz normal)** Gente... Sei que ainda mal nos recuperamos da última confusão que tivemos lá no circo... Mas é se usarmos algum de nossos Aspectos para Forçar uma turba a aparecer? Algo me diz que será muito útil irmos tentando conseguir o máximo de Pontos de Destino até o clímax da aventura.

> Fernando está sugerindo trazer uma Forçada no Aspecto de Alguém para que ganhem um Ponto de Destino a mais, mesmo tendo que lidar com o complicador.

**Elisa:** Podemos combinar duas coisas: Nicola é um _Garoto do Circo Extremamente Otimista e Ingênuo_ e Mayfield é uma _Pobre menina rica, tem momentos em que é sapeca_. E se isso chamar demais a atenção de alguns fanfarrões? Podemos supor que estamos andando com roupas que chamariam a atenção demais, não, Hélio?

> Hélio gosta da ideia: um pouco de ação rápida poderia colocar quem quer que esteja distribuindo Fortivitus sob alerta, e viria bem a calhar como uma boa forma de justificar ele estar mais preparado no futuro.

**H:** Boa ideia! O que acha, Cristina? Acho que Elisa aceita que o Aspecto de Nicola seja forçado, né?

**C:** Está ótimo para mim, até porquê Mayfield também está brava demais de ser esnobada por Mairead por ser rica.

**H:** Ok... Então, tomem cada um 1 PD, e a coisa começa a complicar logo de cara, quando, assim que vocês atravessam a ponte, um grupo de uns 20 Moleques dos Portos olham para vocês e dizem, com tom de poucos amigos. (Hélio assume uma voz condizente com um dos Moleques dos Portos) _"Ora, ora, ora... Um Anjinho e uma Bonequinha se perderam de casa? Roupinhas muito bonitinhas essas, acho que vai dar para fazer uma grana no_ Green Pint _..."_ e já vão com a ideia de agredir vocês.

> Em geral, um grupo pequeno de capangas não é problema para Espíritos do Século... O problema é que são muitos!
> 
> Para refletir isso, Hélio cria eles como grupos de personagens, o que torna mais difícil para os mesmos serem derrotados rapidamente:
> 
> + ***Gangues de Moleques dos Portos:***
> + **Moleques dos Portos; Só aprenderam a resolver as coisas pela Violência; Menos corajosos do que aparentam**
> + **Perito (+2) em:** _Bravatas, Violência, Agir em Grupo_
> + **Ruim (-2) em:** _Coragem_
> + **Estresse:** `2`{: .fate_font} (5 Moleques)

**C:** Se eles vão atacar, obviamente vamos nos Defender, não?

**H:** Sim... Eles estão partindo para o ataque. Cada um de vocês está sendo atacado por 5 moleques. Isso me dá +2 pelo Tamanho dos grupos.

**E:** Eita... Aquela ideia do Ponto de Destino parece péssima agora.

**H:** Vocês aceitaram os PDs, agora lidem com as consequências. Vamos resolver um grupo por vez. Rolem suas defesas e vou rolar os Ataques de Cada grupo.

> Hélio começa rolando os ataques de cada um dos grupos, marcando quem ataca quem.
> 
> + **Grupo 1 (atacando Mairead):** `-0-+`{: .fate_font} - -1 no rolamento, somando com +2 por serem _Peritos_ em Violência e +2 do tamanho do grupo, ficam com um resultado _Bom (+3)_
> + **Grupo 2 (atacando Sidney):** `-+-+`{: .fate_font} - 0 no rolamento, somando com +2 por serem _Peritos_ em Violência e +2 do tamanho do grupo, ficam com um resultado _Ótimo (+4)_
> + **Grupo 3 (atacando Nicola):** `-0-0`{: .fate_font} - -2 no rolamento, somando com +2 por serem _Peritos_ em Violência e +2 do tamanho do grupo, ficam com um resultado _Razoável (+2)_
> + **Grupo 4 (atacando Mayfield):** `-00+`{: .fate_font} - 0 no rolamento, somando com +2 por serem _Peritos_ em Violência e +2 do tamanho do grupo, ficam com um resultado _Ótimo (+4)_
> 
> Por sua vez, Mairead, Sidney, Nicola e Mayfield rolam suas defesas:
> 
> + **Sidney:** `-+--`{: .fate_font} - -2
> + **Nicola:** `0000`{: .fate_font} - 0 
> + **Mairead:** `--+0`{: .fate_font} - -1
> + **Mayfield:** `+++-`{: .fate_font} - +2

**Viviane:** Eita! Rolamento ruim pra caramba esse meu! Mas... Será que posso usar meu _Esperto_ para tentar relembrar como a Gillian fazia seus Kung Fu? Isso pareceria bem apropriado.

**H:** Normalmente não... Mas parece interessante já que isso implica desenvoltura, e você é o _Espírito_ disso. OK... Pode ser...

**V:** Isso fecha com o Sidney conseguindo um _Regular (+1)_ na defesa, já que ele tem _Esperto_ _Bom (+3)_ e rolou -2... Isso não é tão bom assim, já que levo 3 de Estresse por eles terem conseguido um _Ótimo (+4)_ no Ataque... Ao menos continuo no combate.

> Viviane marca na ficha de personagem a caixa 3 de Estresse

**E:** Bem... Nicola pode ser _Otimista_, mas existe uma pequena diferença entre Otimismo e Comportamento Suicida. Acho que como Mairead disse que ali era perigoso, Nicola estaria sendo sempre cuidadoso com o que fazer, certo? Portanto observando o momento de evitar um ataque e tudo mais.

**H:** Bem... Mesmo considerando que tudo isso começou com uma forçada envolvendo você e Mayfield, acho que podemos fazer isso assim. 

**E:** Maravilha! Consigo passar na minha Defesa! _Bom (+3)_ no Cuidadoso, mais +2 no Rolamento supera o Ataque deles de _Razoável (+2)_

**C:** Acho que eu vou na mesma do Nicola! Mayfield não é tão tola quanto todo mundo imagina, e ela quer provar isso, então ela foi sempre mais cuidadosa do que os outros imaginam.

**H:** Bem, me parece bastante honesto... OK

**C:** UFA! Escapei dessa por pouco! _Bom (+3)_ em _Esperto_, mais +2 no Rolamento, fechou em _Excepcional (+5)_, contra o _Ótimo (+4)_ dos Moleques

**F:** Bem... Mairead vive caminhando nesses locais, e sabe como ler os sinais de encrenca. Então acho que Cuidadoso pode ser uma justificativa de defesa aqui, não?

**H:** Sim... Mas isso não impede você de receber Estresse

**F:** Sei, mas ao menos reduz o estrago. _Bom (+3)_, com -1 no rolamentos para _Razoável (+2)_, e eles tiveram um _Bom (+3)_ no Ataque, o que significa que sofri 1 de Estresse 

> Fernando marca na ficha de personagem a caixa 1 de Estresse

**H:** Bem... A parte boa é que agora é a vez de vocês agirem. Quem quer começar?

**F:** Deixa eu tentar uma coisa... Como eu vi durante o circo como o Nicola chamou alguns do pessoal de apoio do circo com aquele _Hey, Rube!_ (Fernando imita a voz que Elisa usa quando interpreta Nicola), acho que posso usar minha Façanha Centuriã para tentar o imitar, o que acha?

> A Façanha Centuriã de Sidney, _Já fiz isso… Eu acho_, lhe dá +3 ao _Criar Vantagens_ sendo _Esperto_ para fazer algo que ele normalmente não faria. Façanhas Centuriãs são Façanhas que Jovens Espíritos do Século possuem que são um pouco mais fortes que as Façanhas comuns do Fate Acelerado

**H:** Me parece legítimo, ainda mais que normalmente isso complicaria Nicola também, já que o jeito de usar _Hey, Rube_ muda de circo para circo, e passar isso para quem não é do circo é no mínimo visto como algo de mal tom. Role contra _Razoável (+2)_. (Diz Hélio, passando uma estrela de xerife de brinquedo como indicador da Iniciativa para Fernando, indicando que Sidney quem está agindo)

> Os dados sorriem para Sidney!

**F:** Legal! `0++0`{: .fate_font}! Isso dá +2 nos dados, com o _Bom (+3)_ do _Esperto_ de Sidney, e +3 da Façanha Centuriã, conseguimos _Lendário (+8)_!

**H:** Belo _Sucesso com Estilo_! Você faz o _Hey, Rube!_ e em pouco tempo uma série de ***Brucutus do Circo*** que estavam aproveitando seu tempo no porto se aproximam!

> Hélio puxa um pedaço de papel com o Aspecto, colocando duas caixas de Invocações Gratuitas... Usos dos Aspectos que podem ser feitos sem pagar Pontos de Destino

**E:** Posso pegar a iniciativa, Fernando? (Diz Elisa, e Fernando passa a Ficha para a mesma. Agora Nicola é quem age). Quero fazer uma coisa aqui, vê se rola! Como Nicola é muito ingênuo, todo mundo do circo tem um certo carinho paternal com ele. Quando os ___Brucutus do Circo___ se aproximam e veem a situação, eles vão vir loucos para comer os ___Moleques do Porto___ na pancada! Vou usar as duas Invocações dos ___Brucutus___ para gerar essa narrativa, que tal?

> Hélio reflete... Não seria algo normal, mas parece uma boa forma de agir, ainda mais que ele queimou as duas Invocações Gratuitas para isso.

**H:** OK. Boa! Descreva o que aconteceu.

**E:** Depois que Sidney usa o seu _Hey Rube!_, os brucutus começam a chegar e todos notam uma cara bem chorosa e boba de Nicola. Vocês três (Elisa referencia-se aos demais jogadores) notam que isso é OBVIAMENTE planejado, mas os brucutus não, quando Nicola grita meio choroso _"ELES QUERIAM BATER EM MIM E NOS MEUS AMIGOS!"_. Os caras ficam loucos e saem correndo, até alguns armados de porretes e socos-ingleses. Um deles diz para nós: _"Fujam que a gente dá um jeito nesses moleques!"_ e a gente sai correndo, mas não de volta para ponte, mas por algum caminho que Mairead conhece!

**H:** Perfeito!

**F:** Ufa! Essa ia ser tensa... Bem... Já que isso acabou com o Conflito (diz Fernando e Hélio faz que sim com a cabeça) Mairead leva os demais por alguns becos e, depois que respiramos um pouco diz _"Eu sabia que a gente não podia vir desse jeito! Vocês tão limpinhos demais, bonitinhos demais, exceto pelo Knacks... Vamos dar um jeito nisso..."_ Pretendo usar minha Façanha Centuriã ***Caldeirão de Dagda*** para achar uns ***trapos bem fedidos e sujos*** que possam ser usados por Nicola e Mayfield para não tanto na telha...

**E:** E se combinarmos com a minha Façanha Centuriã **Jogo do "Bom o Bastante"**? A ideia para completar seria fazer com que tanto eu quanto Mayfield ficássemos pelados, fingindo que fomos roubados... Assim alguém da vizinhança poderia nos arrumar umas roupas velhas. Que tal?

> As Façanhas Centuriãs de Mairead e Nicola basicamente fazem a mesma coisa: *duas vezes por sessão* eles podem conseguir coisas que precisem. No caso de Mairead de coisas descartadas por outros, no caso de Nicola pedindo a outros... É uma combinação bem poderosa, mas Hélio decide forçar um pouco a barra.

**H:** Me expliquem como vocês vão fazer

**E (Nicola):** _Mayfield, nossas roupas chamam demais a atenção, você viu que quase nos pegaram! Precisamos jogar elas fora, ou ao menos as esconder. Se não fizermos isso, não será o primeiro grupo de valentões esse a tentar nos machucar, ou até pior._

**C (Mayfield):** _Ok... Só vamos ver se dá para esconder em algum canto: minha mãe vai dar cria se eu perder JUSTO esse vestido._ Aceito a ideia do Nicola... Vamos tirar tudo TUDO? Quero dizer, pelados totalmente?

**E:** A ideia é essa... Assim fica mais fácil para eu recorrer ao ***Jogo do Bom o Bastante**.

**C:** (Suspirando) Isso deveria ser para crianças

**E:** E que criança nunca andou por aí pelada por alguma razão?

**H:** (Rindo) OK... Me parece válido... 

**V:** Knacks esconde as roupas atrás de uma caçamba de lixo ou algo similar para resgatarmos depois de tudo.

**H:** Está certo. E como Mairead age então,Fernando?

**F:** Bem... Normalmente no ***Caldeirão de Dagda*** ela retiraria diretamente do lixo as roupas... Mas estou pensando em uma ideia que pode encaixar com o que Elisa sugeriu para Nicola. Vou pegar algum borralho ou coisa similar perto e passar no corpo de Nicola, Mayfield e Knacks, deixando eles bem sujos. Isso deve desarmar as pessoas ao redor de qualquer ideia que eles sejam pessoas de fora.

**C (Mayfield):** _Que confusão na qual eu me meti, meu Deus!_

**F (Mairead):** _Quando tudo isso acabar, você pode chegar em sua casa e tomar quantos banhos precisar. Agora deixa eu passar esse borralho em vocês. Já demos na telha demais: se alguém tá procurando pessoas esquisitas, com certeza uma bonequinha de luxo e um anjinho chamaram a atenção o bastante. Lugares assim são comunidades bem fechadas, quem não conhece como a coisa funciona fica visado facinho._

**E (Nicola):** _Vamos lá, Mayfield, vai dar tudo certo! Vamos pegar esses caras!_

**C (Mayfield, fazendo beicinho):** _Está bem! Vocês estão certos, mas vamos logo!_

**H:** OK! Está perfeito para mim essa preparação. E aí?

**F (Mairead):** _Agora vamos passando de porta em porta. Nicola, você faz a cara mais chorosa que você puder, e Mayfield, se apoie em Knacks como se tivesse passando bastante frio. Isso deve ser o bastante. Com certeza esse pessoal deve ter uns trapos velhos que prestem!_

> Elise e Cristine fazem que sim com a cabeça, indicando que Nicola e Mayfield entenderam o plano.

**H:** Perfeito! Para mim está OK então combinar tanto o ***Caldeirão de Dagda*** com o ***Jogo do Bom o Bastante***. Conforme você bate nas portas, Mairead e explicam a história inventada que foram roubados de suas roupas, as pessoas vão dando tanto a Mayfield quanto a Nicola um bom ***Conjunto de Trapos Muito Bons***, com camisas, calças para Nicola e saia para Mayfield e botinas, muito velhas, mas ainda minimamente resistentes. Além disso, uma boa parte dessas roupas podem ser passadas adiante, já que, apesar de tudo, ainda estão em um estado minimamente decente.

**V (Knacks):** _Puxa... Boa ideia essa, Mairead! Talvez seja bom termos algumas peças de roupa extras para qualquer eventualidade._

**C (Mayfield):** _Agora precisamos ir ao tal bar_ Green Pint

**E (Nicola):** _Putz! Não era para lá que aqueles valentões disseram que iam vender nossas roupas se conseguissem?_

**F (Mairead):** _Não tem o que possamos fazer agora, Nicola. A não ser que você tenha alguma ideia melhor._

**E (Nicola, sorrindo):** _Se não acabou bem, ainda não acabou. Vamos nessa!_

**C (Mayfield):** _Um dia isso ainda vai acabar mal para Nicola..._

**H:** Bem... O ***Green Pint***, vocês descobrem, fica em uma região do porto mais próximo do Rio Hudson. O cheiro não é dos melhores, mas considerando os cheiros da região ao qual mesmo seu nariz tão sensível a cheiros se acostuma, Mayfield, é bem tranquilo. Parece um pé-sujo, um bar bastante simples, daqueles que os trabalhadores do porto, garotos jornaleiros e outros tipos com menos recursos veem para comer um ***grude minimamente comestível*** após um dia de trabalho.

> Hélio remove os cartões de Aspectos que estavam na mesa antes, exceto pelo ***Fortivitus*** que descobriram no passado e dos ***Conjunto de Trapos Muito Bons*** que obtiveram antes

**F:** Bem, vamos então. Mairead assume a frente.

**H:** Conforme vocês entram, vocês notam que o bar é sujo, mas não muito: é mais a bagunça de um dia intenso que qualquer outra coisa. As pessoas lá não parecem muito bacanas: alguns estivadores meio bêbados jogando dardos em um velho alvo em um canto, alguns homens, aparentemente de descendência irlandesa, fumando um fumo fedorento em um canto, outros jogando baralhos, uma mesa de bilhar com meia dúzia de caras, e alguns garotos de jornal em um canto. Quando vocês entram, vocês notam que todos param, já que vocês obviamente são novatos na região, e ficam todos ouriçados, mesmo vocês sendo crianças. Um senhor de cabelo branco e pele bem escura atrás do balcão pergunta _"Querem alguma coisa?"_

**V (Knacks):** _"O que você tem aí para comer?"_ Não é o primeiro rodeio de Knacks nessas situações. Ele avança direto para o balcão. Tem espaço para nos sentarmos ali?

**H:** Sim... Tem quatro bancos velhos que devem aguentar vocês... Vocês se aproximam e o homem mostra o cardápio no topo. Pelo horário, uma sopa que cheira a peixe com batata está sendo cozida em um canto. 

**E (Nicola):** _Parece bom... Quanto é essa sopa?_

**H (representando o balconista):** _Vinte cents a tigela, mais 5 se quiserem um pouco de pão para reforçar._

**C:** Acho que Mayfield ainda tem algum dinheiro consigo, não?

**H:** Normalmente você não teria, mas... Me explica como isso se justifica? Imagino que você pretenda recorrer à sua Façanhas Centuriã _Posso pagar por isso_.

**C:** Sim, a ideia é essa! Normalmente meu dinheiro sempre fica escondido em algum lugar no meu vestido, mas pedi para Knacks esconder algumas notas bem amassadas dentro de suas roupas. Já que ele já era meio sujo, isso deve passar a ideia de ser algo tipo dinheiro de bêbado.

**H:** Me parece bom o bastante. Você pretende fazer o que?

**C:** Quero comprar um tanto dessa sopa e mostrar que ela está boa, e com isso fazer as pessoas se interessarem pela mesma. Assim acho que consigo abrir o balconista para a gente. Seria possível assim?

**H:** Sem problemas. Rola contra uma dificuldade _Medíocre (+0)_. Ele parece à vontade, mas vamos dar uma rolada para ver se a coisa continua assim.

> Mayfield possui _Cuidadoso Bom (+3)_, e Cristina rola `+--+`{: .fate_font}, para +0 nos dados mantendo seu _Bom (+3)_. Isso é um Sucesso com Estilo

**H:** O balconista puxa quatro pratos meio velhos de lata e coloca a sopa neles, pegando dois pães pequenos e os dividindo em dois cada um para vocês. Vocês pegam as colheres e começam a comer, e a sopa, apesar da cara estranha, é realmente boa. Não extraordinariamente boa, mas boa o bastante para que as pessoas se animem a comprar e comer. Você vê, Mayfield, que ele não aceita apenas dinheiro: alguns pagam com jornais, pequenos pedaços de corda, lenços, gaitas e outros objetos pequenos e que podem ser facilmente usados para revender, depois de limpos.

**C (Mayfield):** _O senhor sempre faz isso? Quero dizer, aceitar objetos e não dinheiro para a sopa?_

**H (balconista):** _Menina, deveria saber que aqui o pessoal quase nunca tem dinheiro. Esses pobre coitados são ferrados de marré deci._

**F (Mairead):** _Desculpe a Lucy, ela chegou a poucos dias vinda da Escócia. Ainda está se acostumando com os Estados Unidos._ 

**C (Mayfield):** _Verdade... É que não é comum isso, de onde vim. Aliás... Como posso o chamar?_ Digo enquanto passo uma das notas para ele.

**H (balconista):** _Podem me chamar de Josh. Meu velho era dono desse bar depois que ele e meu avô fugiram do sul. Infelizmente o velho morreu a alguns anos, então fico aqui no lugar dele. Mas isso é o suficiente para mim._

> Hélio coloca na mesa um cartão escrito ***Joshua Pelgrane, dono do Green Pint***, com duas Invocações Gratuitas

**E (Nicola):** _Senhor, eu não sei como perguntar isso... Mas... Meu pai disse que não posso voltar para casa sem conseguir uma coisa para ele. Ele disse que ia me bater se não conseguisse isso, é uma coisa que dizem que estivadores estão usando para trabalhar mais, e ele quer fazer mais dinheiro..._

**H:** Você tá tentando perguntar isso diretamente, sobre a Fortivitus?

**E:** Não... Quero enrolar ele e usar o fato que Nicola acredita que _Todo mundo é bom no fim das contas_ para ver se consigo alguma informação dele, mas sem o ofender. Imagino que acusar alguém de traficar ou produzir drogas que deixam qualquer um lelé da cuca não me parece uma boa ideia nesse momento.

**H:** Pode rolar então.

> Hélio sorri, pois Elisa chegou a uma conclusão muito boa! Elisa consegue um `00-0`{: .fate_font}, o que é suficiente para que ela descobra um Aspecto em Josh

**H (Josh):** _Eu não sei quem é esse canalha que tá mexendo com esse bagulho, mas espero que a polícia pegue ele e mande para o xilindró, ou antes, que alguma gangue o jogue para os peixes. Meu velho se matou depois que minha mãe morreu usando ópio. Ela ficou louca de pedra, queria fumar e fumar e fumar aquela coisa. Pior que Cigarros Índios. Se vieram só saber disso, espero que não tenham se decepcionado. Já ouvi histórias que dizem que fazem esse escambo aqui no meu bar. Sinceramente, nunca notei nada, mas se alguém aqui tiver aprontando desse porte e eu descobrir, eu mesmo vou fazer o corpo dele boiar no Rio Hudson._

> Hélio coloca um outro Aspecto ***Mãe morreu após o uso de ópio*** junto ao Aspecto de Josh

**V:** Knacks procura ver como são as reações ao redor ao comentário, imaginando que ele tenha dito isso de maneira alta.

**H:** Rola _Sorrateiro_ e vamos ver.

> Hélio sabe que a dificuldade é _Razoável (+2)_, mas quer deixar alguma tensão no ar. Para isso, ele não diz a dificuldade e faz Viviane rolar "às cegas". Ela rola `0-++`{: .fate_font}, o que indica um Sucesso...

**H:** Knacks, você rola que acontece uma comoção em todos ali: parece que todos sabem esse lance que a mãe de Josh morreu pelo ópio. Curiosamente, tem um cara que te chama a atenção: usa um chapéu coco, tem cabelos ruivos bem chamativos, um bigode fino e um sorriso de fuinha. Parece um pouco o Zé Bagunça do Pinóquio, mas mais velho e ainda pior.

**V (Knacks):** _O que o senhor pode me dizer sobre aquele fulano ali?_ E aponto com a cabeça o cara.

**H (Josh):** _O Lawrence? Ele chegou a algumas semanas no porto da Ilha Ellis vindo de Limerick, na Irlanda. Tem feito uma grana grande no porto, mas não sei como. Dizem que tem um bacana pagando ele para enviar umas cargas daqui para a Irlanda. Sinceramente... Não sei, e acho que tem algo meio esquisito, mas prefiro não me meter. Não é problema meu._ Josh termina, voltando à limpeza dos utensílio.

**V:** Knacks termina seu prato e decide então ir conversar com esse Lawrence. 

**F:** Mairead o acompanha

**H:** Knacks, Mairead, vocês veem que ele está com outros dois caras, rolando um carteado, aparentemente poker ou whist, uma variação de bridge bem popular na época.

**V:** _Opa... Será que posso sentar?_

**H (fazendo Lawrence):** _Depende se você tem dinheiro..._

**V:** Knacks teria algum dinheiro? Acho que não, né?

**F:** E se você usar uma das Invocações Gratuitas dos ***Conjunto de Trapos Muito Bons*** para gerar um Aspecto de ***Trocados perdidos nos bolsos***? Como se tivessem achado algumas moedas no meio das coisas.

**C:** E se Knacks ainda tiver com parte do dinheiro de Mayfield nos bolsos? Poderia ser?

**H:** Usando sua _Façanha Centuriã_ novamente, Cristina?

**C:** É o máximo que Mayfield poderia ajudar nesse momento?

**H:** Faz sentido... Rola então seu _Razoável (+2)_

> Normalmente isso seria impossível, mas como disseram antes que parte do dinheiro de Mayfield foi escondido por Knacks, faz sentido esse uso. Cristina rola por Mayfield, e consegue um +0. Com o _Cuidadoso Bom (+3)_ e +2 da Façanha Centuriã, é um Sucesso com Estilo sendo _Excepcional (+2)_

**H:** Knacks, você corre seus bolsos e lembra do pequeno maço de notas que Mayfield lhe deu antes de esconderem o vestido dela. Você separa alguns desses ***Trocados perdidos nos bolsos*** para usar 

> Hélio traz os ***Trocados perdidos nos bolsos*** como um Aspecto com Duas Invocações Gratuitas graças ao Sucesso com Estilo de Mayfield

**V (Knacks):** Knacks apresenta as notas e diz. _Isso é o bastante, parceiro?_

**H (Lawrence):** _Com certeza, amigo. Pode sentar. Conhece as regras?_

**V (Knacks):** _Conheço..._ (ela então revela o plano de Knacks para Hélio) A ideia de Knacks é ver se consegue descobrir se é Lawrence quem vende o _Fortivitus_, e de quem ele recebe a droga.

**H:** Vamos então abrir uma Disputa entre você e Lawrence? Até onde você sabe, tudo que Lawrence quer é deixar você pelado, ao menos metaforicamente falando. E você quer descobrir se Lawrence está envolvido com _Fortivitus_. Então me parece uma Disputa. Relembrando: melhor de cinco, a cada turno o lado com melhor rolamento obtêm 1 vitória, 2 se for com estilo. Um empate provoca alguma mudança nas coisas. OK? Vamos então começar

**V:** Vou avisar que Knacks vai usar tudo que aprendeu nos trilhos sobre jogos de baralho e suas regras, rolando por _Esperto_.

**H:** Por mim ok. Vamos então.

> Hélio e Viviane pegam os dados. Viviane não sabe, mas Lawrence, ou Lórcan, é ***Um tipo Esquivo***, capaz de trapacear até diante do túmulo da mãe. Os testes dele serão sempre por _Sorrateiro Bom (+3)_. O primeiro rolamento de Lórcan é um `-000`{: .fate_font} para -1, totalizando um _Razoável (+2)_ nesse turno, enquanto Knacks consegue um `+0+0`{: .fate_font}, totalizando +2 nos dados e _Excepcional  (+5)_ nesse turno. Hélio decide não facilitar.

**H:** Normalmente isso seria um Sucesso com Estilo para 2 Vitorias para Knacks, mas... Eu uso 1 Ponto de Destino revelando um Aspecto da ___Sorte dos Leprechauns___ de Lawrence para rerrolar os dados.

> Hélio consegue agora um `+000`{: .fate_font} para um _Ótimo (+4)_ de Lórcan. Ainda assim, ele quer impressionar.

**H:** E como, além disso, ele é ***Um tipo Esquivo***, uso outro Ponto de Destino para +2 para conseguir a Vitória dessa Rodada.

**V:** Cacetada, e eu achei que essa ia ser fácil!

**F:** Você já fez ele queimar 2 Pontos dessa cena, isso alivia as coisas.

> Fernando está certo. Hélio usou 2 dos 3 Pontos de Destino que ele tem para a cena só para impedir Knacks nessa Rodada

**H:** Bem... Vamos então, é 1 a 0 para mim. Segunda rodada!

> Hélio e Viviane rolam novamente, conseguindo `0-+0`{: .fate_font} e `+-00`{: .fate_font}, ou seja, ambos conseguiram +0 nos dados, e portanto ambos estão em _Bom (+3)_. Hélio pensa bem e decide.

**H:** Um empate nessa rodada seria um problema, então vou pagar meu último Ponto de Destino da cena na _Sorte dos Leprechauns_ de Lawrence para conseguir outra Vitória. 2 a 0 para mim!

**V:** **GULP!** Se esses dados não forem bons agora, vou precisar usar PDs!

> Para a terceira rodada, Hélio e Viviane rolam `-0-0`{: .fate_font} e `0-+0`{: .fate_font} respectivamente. Isso faz com que Lórcan obtenha um _Regular (+1)_ e Knacks consiga um _Bom (+3)_

**V:** Beleza... Consegui essa vitória! Não será tão fácil me vencer!

**H:** Tudo bem! 2 a 1... Quarta rodada

> Viviane rola um `+++0`{: .fate_font} e Hélio um `+++-`{: .fate_font}, o que coloca Knacks com um _Fantástico (+6)_, e Hélio com um _Excepcional (+5)_. Viviane sorri

**V:** Como Knacks ***Quer fazer amigos como ele (embora não saiba o que isso significa)***, gasto um dos meus Pontos de Destino para aumentar em +2 meu rolamento e conseguir um Sucesso com Estilo, já que os amigos dele dependem de ele descobrir se Lawrence é quem achamos que ele é, e a única forma de convencer ele que queremos comprar Fortivitus é vencendo esse carteado.

**H:** Ok... Nem tenho como impedir... Você baixa por fim um _Royal Straight Flush_, o que assusta Lawrence. Ele dá um sorriso meio maldoso quando se recompõe e diz, passando algum dinheiro. _Acho que isso aqui é seu._

**V (Knacks):** _Não estou atrás do seu dinheiro... Meu amiguinho ali precisa de um bagulho para o pai dele. Acho que você sabe do que estou falando..._ Digo, mostrando o rótulo de _Ginger Ale_ com a inscrição _Fortivitus_ nela, discretamente para ele.

**H (Lawrence):** _Ah... Sim... Me encontrem daqui a alguns minutos, em um armazém quatro armazéns para baixo. Vou estar com o bagulho e espero o dinheiro._ Ele diz, enquanto você discretamente coloca o dinheiro no bolso e volta para o balcão com Mairead.

**V (Knacks):** Antes de chegarmos no galpão, sussurro discretamente para Mairead. _É ele. Ele é quem vende a coisa._

**F (Mairead):** _Vamos partir para cima dele então! O que estamos esperando?_ digo conforme nos sentamos no balcão para terminar a sopa

**V (Knacks):** _Melhor não... Gente demais aqui. Podemos provocar um tumulto que não ajudará a gente._

**F (Mairead):** _Eu vou explodir de raiva! São imbecis como esses que destroem comunidades para seus fins egoístas, e fazem com que preconceitos se justifiquem!_ Digo bem alto

**H:** Opa! Então estamos considerando uma Forçada no fato de Mairead ser uma ***Descendente de Irlandeses que vive pela sua comunidade***

**F (Mairead):** Sim! Mairead aponta para Lawrence e diz _São adultos cretinos como você que tornam a vida de crianças como eu um inferno! Vendendo drogas como essa Fortivitus!_

**H:** Ao falar isso Mairead, você vê Josh assustado, mas logo em seguida se levantando. _Se isso for verdade, Lawrence, hoje você vai nadar com os tubarões da Ilha Ellis!_

**E (Nicola):** _Ai meu Deus! Acho que é melhor a gente dar no pé!_ Nicola tenta correr para um canto.

**H:** _Não tão rápido!_ dizem uns caras meio com cara de maluco dos portos. Você percebe de olhar pelo que vocês viram antes com o Valentão do Educandário que eles estão ***Sob efeito da Fortivitus***!

**C (Mayfield):** Cansei! Vou dar na cara desses rufiões antes que eles possam partir para cima de Nicola!

**H:** Vai de _Ágil_?

**C (Mayfield):** _Nada disso! Não tenho ginástica a toa no Educandário! Vou correr e pular por cima de uma das mesas, girando minha perna como se tivesse dando um_ pas-de-Deux _, a minha perna atingindo a cara deles!_

**H:** Me soa _Estiloso_, então. Manda bala!

> Cristina rola o _Estiloso Razoável (+2)_ de Mayfield, e consegue `0-00`{: .fate_font} para _Regular (+1)_, enquanto os Valentões conseguem `++-+`{: .fate_font}. Como são _**Peritos (+2) em** Atacar em Bando_, eles tem um resultado final de _Excepcional (+5)_ na Defesa!

**C:** Oh oh!

**H:** Em sua ânsia de defender Nicola, na prática Mayfield você conseguiu se colocar em ___Uma situação Perigosa___. Esse Valentões vão agir para capturar vocês, tentando ajudar Lawrence a sair do _Green Pint_ em segurança por meio de reféns.

> Hélio rola pelos Rufiões, que conseguem apenas `0+00`{: .fate_font}.

**H:** Bem... Defendam-se como puder!

**E:** Acho que _Cuidadoso_ não cabe aqui, Nicola está assustado demais, mas decide ir de _Estiloso_, usando truques acrobáticos de circo para fugir de perto dos rufiões.

**C:** Vou seguir o caminho que Nicola abrir. Acho que isso conta como Trabalho em Equipe, não?

**H:** Sim. E como você também tem _Estiloso_ acima de _Medíocre (+0)_, você pode fazer isso.

**E:** Ótima ideia, Cristina!

> Elisa consegue `++++`{: .fate_font} para +4 nos dados, somando com seu _Estiloso Razoável (+2)_ e +1 do Trabalho em Equipe de Mayfield, resulta em um _Épico (+7)_ na Defesa

**H:** Isso é MUITO Sucesso com Estilo na Defesa... O que vocês querem fazer nessa Defesa?

**E:** E se, na bagunça toda que está rolando, do nada Lawrence tentar fugir e for alvejado por Josh?

**H:** Epa! Esse cenário é PG, não existe morte por arma de fogo aqui, ok?

> Em _Young Centurions_, as aventuras são definidas por _Níveis de Campanha_, como se fossem as faixas indicativas de filmes. No caso, um cenário PG implica em um certo grau de violência, mas nada muito acima do normal. 

**E:** Não quero que ele morra... Quero que esse primeiro tiro seja de alerta, e que Lawrence fique ___Sob a mira___ de Josh.

> Hélio acha a ideia interessante, ainda mais que ele tem uma carta na manga.

**H:** Está certo! Vocês fogem para longe dos valentões e, enquanto tentam voltar para perto de Knacks e Mairead, Lawrence tenta fugir quando vocês escutam um ***BANG!*** _Eu disse e vou cumprir, Lawrence. Se você realmente vende essa porcaria e usa meu bar como Casa de Ópio para seus viciados, é bom você abrir o bico e dizer quem vende essa merda, ou você é quem vai ir dormir com os peixes do Rio Hudson!_ ele diz, mantendo Lawrence na mira.

**E (Nicola):** _Quem é que vende para você isso? Onde isso é feito!_

**H (Lawrence):** _Aquele janota almofadinha, Lemont Fitzlefevre, não vai me perdoar se eu deixar vocês irem. Ele nem tá tão longe, seu depósito onde ele faz esse bagulho fica no fim do cais. Mas saibam que vocês não vão chegar lá. Ele vai me matar se eu deixar, então... Vamos lá!_ Ele diz, pegando uma garrafa com um líquido meio leitoso. Vocês sabem o que é!

**F (Mairead):** _Fortivitus!_

**H:** Ele bebe o líquido, mas é esquisito que a reação parece mais forte. Todo mundo começa a estranhar ele chacoalhando como se tivesse convulsões ou um ataque epiléptico. Mas ao mesmo tempo, os braços vão crescendo e ele vai virando algum tipo de... Não tem outro nome... _Homem das cavernas_, suas roupas explodindo tipo Hulk, os olhos injetados e vermelhos das veias inchadas. Ele dá um grito de ódio quando algum dos bêbados grita _"Corram pelas suas vidas!"_ 

**E (Nicola):** _Qual a rota de fuga mais próxima? A gente não tem a menor chance contra esse monstro!_

**H (Josh):** _Venham comigo, garotos! Se quiserem viver!_

---

Agora cabe aos Jovens Centuriões, os Espíritos do Século XX, fugirem pelas suas vidas do Monstro Atávico do Fortivitus. E, se sobreviverem a isso, tentar encontrar a fábrica dessa droga terrível e descobrir quem é o tal janota almofadinha que Lawrence mencionou, Lemont Fitzlefevre, que parece ser quem está por trás de tudo!

<!--  LocalWords:  Royal Straight Flush Ginger Ale PG Lemont Hulk
<!--  LocalWords:  Fitzlefevre
 -->
 -->
