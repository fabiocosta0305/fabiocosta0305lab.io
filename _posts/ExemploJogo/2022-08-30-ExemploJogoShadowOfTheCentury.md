---
title: "Exemplo de jogo em _Shadow of the Century_"
#teaser: "Quando a turma precisa realizar um Teste de Coragem, os jogadores vão explorando o cenário e as regras!"
layout: post
date: 2022-08-30
categories:
    - RPG
header: no
excerpt_separator: <!-- excerpt -->
tags:
  - Fate
  - Aventura
  - Exemplo de Jogo
  - Shadow of the Century
  - ExemploDeJogo
---

> Priscila está jogando um Filme em [Shadow of the Century](http://fabiocosta0305.gitlab.io/adventures/TheHappiestArmaggedonOnEarth/), uma aventura aventura curta onde os jogadores são agentes de uma organização secreta dos Anos 80 chamada NOVA (_Network Of Variable Agents_ - **Rede de Agentes Variáveis**), atuando para descobrir as razões por trás de um estranho assassinato de uma mulher que era parte do elenco (trabalhava) da Disney World que, basicamente era amiga da Branca de Neve (ou seja, trabalhava como personagem vivo da Branca de Neve). Os mesmos são:
> 
> + Rodrigo, jogando com [Jonathan Frasier](http://fabiocosta0305.gitlab.io/personagens/JonathanFrasier/), um ex-agente da CIA que viu coisas demais no Vietnã e em El Salvador e deu baixa para não pirar
> + Sérgio, com [Jenny Williams](http://fabiocosta0305.gitlab.io/personagens/JennyWilliams/), filosofa metida a _cyberpunk_, mas muito inocente e infantil
> + E Olívia, com [Jeung Kwang-Ho](http://fabiocosta0305.gitlab.io/personagens/JeungKwang-Ho/), que após um Mestrado em Geopolítica, viu seu nome colocado em uma lista de alvos de alguma organização comunista (ou não);
> 
> Com a ajuda de um funcionário do parque, eles se infiltraram no parque e descobriram que um grupo de funcionários por alguma razão estava tentando provocar problemas, inclusive sabotando algumas das atrações, como o _Peter Pan's Flight_ ou o _Dumbo the Flying Elephant_, além de tentar tornar o clima do parque um inferno! Eles descobriram então que esses funcionários estão limpos  ***Apenas em sistemas computadorizados***: uma investigação por papel, comum, revelou que eles ***Não existem oficialmente***, todos os documentos reais simplesmente são inexistentes. Eles não possuem traços em local nenhum!
> 
> E, como se isso não fosse pouco, descobriram que esses malucos possuem contatos com uma organização ultra-direitista chamada de ***Bastião do Governo Mundial Unificado***, mas pior ainda... Descobriram que esse grupo é uma fachada para uma organização de uma realidade paralela visando invadir a nossa!
> 
> Por que diabos? Isso é o que eles vão ter que descobrir, quando voltam para o dia seguinte de trabalho na Disneyworld


<!-- excerpt -->
---

> Priscila começa colocando uma música dos anos 80 para tocar, no caso _Duel_, da banda _Propaganda_, já que é uma cena mais introspectiva

**Priscilla:** Bem... Depois do que descobriram sobre aqueles caras, vocês ainda estão bolados pra caramba, enquanto estão trocando informações perto do _Mad Tea Party_, vendo crianças e adultos girarem despreocupadamente sentados nas xícaras de chá.

**Olívia (como Jeung):** _Deixa eu ver se entendi... Esses malucos querem, de alguma forma, abrir um portal entre a realidade deles e a nossa para dominar o mundo. Isso é possível?_

**Sérgio:** Bem... Jenny é alguém que manja de cultura futurista e tal, deve ao menos saber se existe alguma teoria estudada sobre isso e por aí afora... Seria o caso aqui? Aliás, se lembro bem, ao usar minha _Enciclopédia Perambular_ no início da aventura, coloquei ***Física de Multiverso*** como uma das áreas de Conhecimento.

> Essa Façanha de Jenny oferece a ela algumas especialidades que entram em jogo como Aspectos com Invocações Gratuita e que ela pode usar para conseguir um sucesso automático

**Priscila:** Sim, isso seria possível. Pretende queimar a Invocação agora ou deixar para depois? O teste seria de _Conhecimentos Fantástico (+6)_, já que vocês não tem sequer noção sobre o que poderia provocar isso.

**Sérgio:** Vamos para os dados e depois a gente vê... (Diz, pegando os dados)

> Sérgio rola e obtêm `0+-+`{: .fate_font}, para um +1 nos dados. Isso é um Empate.

**Priscila:** Podemos então pensar que você somaria o Impulso obtido à sua ***Física de Multiverso***? Ou quer melhorar o rolamento?

**Sérgio:** Não, deixa assim mesmo! Ela pode ter um Insight extra, mesmo não percebendo do que se trata. Pode ser como um Impulso atrelado à ***Física de Multiverso***?

**Priscila:** Pode... Jenny, você leu durante seu tempo na faculdade vários documentos de Física e Tecnologia, incluindo um muito esquisito, que falava sobre um fator chamado de ***Variância Hiperdimensional Síncrona***, um efeito que alguns dizem ocorrer em nível hexa ou mesmo heptadimensional que provocaria rupturas no tecido do Espaço-Tempo por onde coisas estranhas viajariam. Essa tese é super-apócrifa no meio da Física, mas muita gente tem usado na literatura.

> A ***Variância Hiperdimensional Síncrona*** é uma característica do cenário de _Shadow Of The Century_, provocada por um de seus maiores vilões, onde o tecido da realidade é tão fino que permite que todas as bizarrices dos filmes dos anos 80 ocorram no cenário. 

**Sérgio (como Jenny):** _Pode ser que eles pensem em provocar uma Variância Hiperdimensional Síncrona. Mas até onde sei é algo considerado extremamente teórico e muito apócrifo, quase herético dentro do que os físicos estudam na Teoria de Multiversos_

**Rodrigo (como Jonathan):** _Muito blá-blá-blá... Deve ser uma dessas viagens de comunista. O negócio é simples: vamos descobrir quem são os babacas que tão fazendo isso e sentar o dedo._

**S (Jenny):** _Eu lá tenho culpa que seu cérebro é músculo e você não consegue ver um palmo diante do seu nariz?_

**R (Jonathan):** _Vai dando risada: é tipo em Khe Sanh: tudo são flores até a hora que os vermelhos tão com a faca no teu pescoço!_

**O (Jeung):** _Ei, vamos parar com o festival de testosterona! A gente não sabe que merda eles pretendem fazer, nem se é possível!_

**R (Jonathan):** _Seja lá o que for, vamos ter que descobrir, pois parece que esses malucos do **Bastião** são mais bem organizados que a Klan. Pouquíssimas pistas, ainda mais que eles não existem de maneira real, ainda que tudo que tá nos computadores esteja muito perfeito._

**S (Jenny):** _E essa é a parte que mostra que é bem provável que eles **realmente** sejam de outra dimensão! Duvido que o governo teria deixado uma falha grotesca como essa passar._ (para Priscilla) Posso provocar isso como uma _Forçada_ na _Síndrome de Peter Pan_ de Jenny? Ela prefere achar a explicação mais alucinada possível, e nunca seria pé no chão como Jonathan.

**P:** Perfeito! Toma um PD

> Priscila realmente estava com algumas coisas planejadas, e Jenny deu justamente o gatilho para que tudo isso acontecesse. Uma série de complicações vão acelerar a aventura a partir de agora!

**R (Jonathan):** _Ainda acredito que deva ser da turma de Cuba aprontando algo..._

**P (fazendo a voz grave de um NPC):** _Conte-me mais sobre isso..._

> Todos param, enquanto Priscila coloca na mesa um cartão de Aspecto escrito dos dois lados. O lado para cima está escrito apenas ***Yan, o Custódio*** com nível *Ótimo (+4)*, seguindo as regras específicas para criar-se Extras em Shadow of the Century, NPCs quase que literalmente descartáveis que só precisam ficar ali uma cena ou outra

**P:** Nessa hora aparece esse cara, em uma roupa de Custódio do Parque. Ele parece ter cara de poucos amigos, uma barba longa e escura, um nariz adunco e um olhar penetrante e rígido, quase assustados, mas apenas quase. (voltando à voz grave) _"Gostaria de entender o que vocês estão fazendo parados aqui... Não é horário de serviço de vocês?"_

**O (Jeung):** _Desculpa... Pausa para almoço, e ainda não conseguimos nossas orelhas, ainda estamos pegando o jeito._ Será que Jeung consegue convencer esse custódio que não tá pegando nada demais e é só um papo amistoso? Aliás, o que é um Custódio?

**P:** Custódios seriam uma mistura de faxineiro e zelador, com mais algumas funções. Talvez vocẽ consiga, role sua *Persuasão* eu rolo por ele. Talvez, já que você disse que ainda não _Conseguiu suas Orelhas_ ele dê uma chance...

> Priscila sabe que usar a desculpa de _Conseguir as Orelhas_ é meio fraco, já que mesmo estagiários da Disneywolrd (pessoas ___Atrás de suas Orelhas___) se dedicariam mais que os agentes infiltrados da NOVA. Ela rola `+-0-`{: .fate_font} , conseguindo um -1, que ela soma com o _Ótimo (+4)_ desse NPC para um _Bom (+3)_, enquanto Olívia consegue `0-+0`{: .fate_font} para +0, mantendo sua _Persuasão_ em _Razoável (+2)_

**O:** Será que eu usar o Aspecto de Jeung ***No fim das contas, tudo são pessoas. O Chefão é um* avatar** eu poderia convencer esse cara que tá tudo bem? Sabe, como que desviando dele já que ele poderia supor que esse cara está envolvido com o Bastião e coisa do gênero?
 
**P:** _Fala Sério!_ Não faz sentido: você não tem como ver qualquer ligação entre esse cara e alguma conspiração! Você não  conhece de Adão e ele nem fez nada demais, exceto os questionar pelo horário! Ele só parece um Custódio qualquer mais sério, até onde você sabe!

**S:** Pior que é verdade!

> Olívia não tem como usar esse Aspecto de uma maneira convincente para metê-lo em alguma conspiração... ***Ainda que*** na realidade Priscila tenha alguma ideia melhor sobre isso. Mas como Jeung não tem essa ideia, Priscila acionou a _Regra do Fala Sério!_ do Fate Acelerado, no qual o ***Shadow of the Century*** se baseia

**O:** Está bem... Vou topar a Falha... Mas ao menos não queimei um Ponto de Destino a toa

**P:** Sim... Ele observa vocês e vocês notam que a desculpa _definitivamente_ não cola. (Como o tal Yan) _Bem... Melhor voltarem ao seu trabalho... Mas... Gostaria de conversar sobre algumas coisas que ouvi de relance com vocês. Me encontrem após o fechamento do parque no Corpo de Bombeiros da Main Street. Notei que vocês perceberam algo estranho lá, algo muito especial. Eu posso comentar algumas coisinhas sobre vocês e sobre o que estão procurando. Até lá então..._ Diz o homem, se afastando.

**R:** _Droga! Esse cara pode estar trabalhando com o Bastião! Deve ter notado o nosso disfarce!_

**P:** Jenny, na realidade, tem algo que preciso te dizer: você nota sim algo de interessante sobre esse homem... Mas... Ele deveria ***existir apenas em Desenhos!*** (Priscila coloca esse Aspecto junto a ***Yan, o Custódio***. Ao mesmo tempo, ela passa uma nota para Sérgio, explicando o que tá acontecendo sem que os outros saibam)

**S (Jenny):** _Meu Deus! Se... Não... Isso é demais até para mim... Se isso for verdade, explicaria muita coisa, mas complicaria ainda mais as coisas!_

**O (Jeung):** _Qual a sua ideia?_

**S (Jenny):** _Prefiro não dizer! Ia passar por Louca!_

**R (Jonathan):** _Isso já sabemos que você é, apenas desembucha!_

**S (Jenny):** _Nem fodendo! Aliás, vamos fazer o seguinte... Vamos ver qual é a desse cara. A gente se encontra no Corpo de Bombeiros do_ Main Street _assim que o parque fechar! Vamos manter nosso disfarce até lá, tenho muito algodão doce para vender!_ (em sua voz normal) E saio vazada. Isso teria a ver com aquela luz que notei no mesmo?

> Logo no início da aventura, Jenny notou uma luz esquisita no Corpo de Bombeiros da _Main Street_ enquanto estava parada vendendo algodão doce. Isso é uma tradição relacionada ao fato do Escritório de Walt Disney no parque ficar lá... Mas isso no _Disneyland_, em Anaheim, não no _Disneyworld_. Apenas Jenny sabe essa informação

**P:** Sim, teria a ver.

**R (Jonathan):** _Espera, Jenny! Merda, ela nunca escuta, tem a cabeça no ar, mais infantil que Peter Pan! Droga! Onde Alpha estava com a ideia de chamar ela para uma missão como essa?_

> Alpha é a recrutadora da NOVA, que organiza os times conforme a necessidade de cada missão. Isso eles sabem pois foi passado logo no início da aventura

**O (Jeung):** _Sinceramente, não sei, mas Alpha devia saber que ela seria útil. Vamos manter a cabeça no lugar. As coisas já estão malucas o suficiente com esses maníacos do **Bastião** e suas ideias xaropes, sem colocar viagens esotéricas e coisas do gênero._

**R (Jonathan):** _Está bem... Vamos ver esse cara! Parece que ele é a única pista quente que vamos ter..._

**P:** E com isso cortamos para a próxima cena! (Ela remove alguns cartões de Aspectos que não são mais úteis) Os fogos de artifícios que indicam o encerramento das atividades no _Magic Kingdom_ acabaram de explodir, e as pessoas estão indo embora. A maioria das pessoas no parque, mesmo contando elenco, são custódios, varredores, equipes de manutenção e um ou outro gato pingado. Vocês já estão fora do horário de trabalho e estão agora fora das fantasias, ou seja, dos uniformes. Vocês chegam no Corpo de Bombeiros da _Main Street_ e notam a luz que Jenny mencionou para vocês.

**S (Jenny):** _Viram? A luz está ali, não sou maluca!_

**R (Jonathan):** _Deve ser alguma pegadinha de alguém..._

**S (Jenny):** _Nada disso, Jon, é de verdade!_

**R (Jonathan, irritado):** _Me chama de Jon de novo, e vou meter uma bolacha na tua fuça!_

**O (Jeung):** _Calma, Jonathan... Não adianta se estressar por não achar um alvo. Vamos ver o que tá acontecendo..._ Tem como eu notar se tem alguma porta aberta ou algo do gênero?

**P:** Na realidade... Vocês ouvem baixinho música vindo do andar de cima do Corpo de Bombeiros... O que _não deveria acontecer_, o andar de cima deveria ser apenas cenográfico!

**R (Jonathan):** _Que porra tá pegando?_ Jonathan olha para os lados e discretamente deixa sua mão debaixo da axila, perto do coldre da Glock que ele sempre tem à mão!

**S (Jenny):** _Vamos lá, gente... Acredito que ele irá nos ajudar!_

**O (Jeung):** _Quem, o tal custódio?_

**S (Jenny):** _Não! Yen Sid!_ 

**R (Jonathan):** _A maconha derreteu seu cérebro, guria! Quem é esse Yen Sid?_

**O (Jeung):** _Olha, para ser sincero, estamos em uma encruzilhada, sem pista quente nenhuma. Qualquer coisa que vier é lucro se queremos descobrir porque mataram aquela moça._ Bem... Você não falou se tem um acesso

**P:** Jeung, vocẽ nota que, perto da entrada, que está aberta, tem uma escada que parece subir. Jenny... Você sabe que essa escada **NÃO ESTAVA LÁ** antes, e que essa porta deveria estar fechada com o encerramento do dia no _Magic Kingdom_!

**S:** Jenny corre desembestada, sem se preocupar com o perigo!

**R (Jonathan):** _Ei, guria! Droga! Vamos lá! Se for algum inimigo, ao menos ela não vai morrer se formos lá resgatar ela!_

**S:** Machão! Faltou apenas gritar _Yippee ki-yay, motherf$#+er_!

**R:** Eu tenho lá culpa se Jonathan é bem esse tipo de heroi?

**P:** OK... O festival de testosterona tá maravilhoso (sarcástica), mas vamos seguir adiante. (Priscila muda a música para _O Aprendiz de Feiticeiro_. Ela colocou uns efeitos no player para deixar a música com chiado). Quando vocês sobem, vocês vão ouvindo essa música de fundo, o que faz vocês, por algum motivo, andarem bem de soslaio, quase se tivessem entrando em um covil de ladrões. Mas o que vocês vêem um _flat_ decorado de maneira simples, mas elegante, com uma velha Vitrola Victoria tocando um disco, de onde sai a música que vocês ouvem, do _Aprendiz de Feiticeiro_. E em um canto, vocês vêem que alguém está lá. Suas roupas são tão curiosas que vocês quase supõem ser algum Personagem de Rosto que esqueceu de tirar a fantasia, mas parece muito, mas muito real! Um robe azul cobrindo o corpo e um chapéu de mesma cor, cônico e grande. O rosto barbudo e adunco não deixam dúvidas, é o mesmo homem que encontrou vocês no _Mad Tea Party_! (como o NPC) _Vejo que atenderam meu chamado. Eu sou **Yen Sid, último mestre e Guardião da Magia Disney** e do Imagineering. Ou, ao menos eu era antes de vir para cá!_

**R (Jonathan, sacando sua Glock):** _Chega de palhaçada, chapa! Qual é a sua? Responde logo de uma vez!_

**P (Yen Sid):** _Parece que vocẽ confia muito mais na sua arma que na sua mente e seus olhos. Se você acha que pode resolver isso dessa forma, fique a vontade!_

**R:** Jonathan nem pensa e senta o dedo!

**O:** Você é xarope? Vai apagar o cara assim do nada?

**P (sorrindo):** Na verdade... (ela diz, virando o cartão de Aspecto de ***Yan, o Custódio***, onde tem o Aspecto ***Yen Sid, Guardião da Magia Disney de outra realidade***) eu pago 1 Ponto de Destino no Aspecto dele de _Personificação da Magia Disney_ (ela apresenta um botão que estão usando como Pontos de Destino que ela tem do estoque de cena) para uma declaração narrativa: Jonathan, a sua Glock não dispara no momento que você aperta o gatilho... E você nota que não é porque ela emperrou ou está sem munição. Você pressiona o gatilho mais algumas vezes e "gatilho aperta nada acontece feijoada". Você olha suas balas caídas no chão e nota que  nem mesmo a marca do cão batendo contra a jaqueta da bala aparece. Literalmente a arma não disparou por _motivos_!

**R (Jonathan):** _OK, vovô, a brincadeira foi divertida, mas não podemos ficar de brincadeira. Uma mulher foi morta e tem uns doidos dizendo que podem abrir um portal para outra dimensão, ou para a Iugoslávia até onde a gente sabe._

**P (Yen Sid):** _Na realidade, meu caro, eu realmente sou quem afirmo que ser. Vim para essa realidade no dia 15 de Dezembro de 1975, nove anos após a morte de Walt Disney nessa realidade e na da que vim._

**O (Jeung):** _Bem... Vamos colocar que isso seja verdade. Por que fez isso?_

**P (Yen Sid):** _A verdade é que um grupo totalitário assumiu o governo dos Estados Unidos e dominou o mundo em minha realidade, começando pelos Estados Unidos. Ao corromper as ideais de Walt Disney, que foi assassinado em minha realidade, transformaram as mesmas em um ideário ultranacionalista, igual aos que foram combatidos na 2ª Grande Guerra. Pouco depois de assumirem o comando dos EUA, esse grupo realizou um ataque preemptivo, supostamente, contra Moscou, Kiyv, Belgrado, e Praga, dizimando de maneira rápida e brutal o pacto de Varsóvia, literalmente transformando Moscou, Stalingrado e Leningrado em desertos atômicos, Pequim sendo dizimada e Xangai por muito pouco não tendo o mesmo destino. Em mais alguns anos, por meio de manipulação política, bloqueios econômicos ou guerras rápidas, eles unificaram o mundo sob a égide do **Governo Mundial Unificado**_

**R (Jonathan):** _Esses merdas de novo? E como vamos saber que você não está com eles?_

**P (Yen Sid):** _Eu tive que fugir: eles caçaram todos os que eram capazes de usar Magia Disney e Imageneering e que não estavam alinhados ao seu projeto de cunho fascista. Eles viam em nós um perigo e um potencial. Obviamente devem imaginar o que eu sou se tive que fugir: fui o último que não estava sob a égide dos mesmos ainda vivo, e com muito esforço procurei abandonar minha realidade, para proteger meus conhecimentos, que eram vastos. Mas apenas um não pode enfrentar um sistema sozinho._

**R (Jonathan):** _Você disse que Walt Disney foi morto em sua realidade, e o que isso tem a ver?_

**P (Yen Sid):** _Sim... Em minha realidade, Walt foi vítima de um ataque e acabou morto. Uma investigação foi feita e culpou_ "células comunistas que estavam implantadas nos Estados Unidos". _Mas... Isso não me convenceu: sei que muitos comunistas tinham seus problemas com Disney, mas daí a procurar um assassinato? Não exatamente uma operação fácil e com bons dividendos políticos nesse caso. Me parecia bem mais plausível algum serviço interno do governo contra Disney, ou ao menos provocado por essa ala mais ultranacionalista do governo... E eu sei que nessa realidade, Walt morreu naturalmente, mas outros eventos, como A Queda do Clube do Século aconteceram como na minha realidade._

**R (Jonathan):** _Acha então que isso pode ter sido feito por algum dos antigos Espíritos do Século em vingança?_

**S (Jenny):** _Nem fodendo! Os Espíritos do Século foram perseguidos por aquele cretino do McCarthy_

**R (Jonathan):** _Ele nos salvo dos comunistas e gays que estavam corrompendo nosso país!_

**S (Jenny):** _Vai se f@%er você e seu Pânico Moral, Jonathan! Aliás, quem foi que colocou o Rambo como líder dessa porra?_

**R (Jonathan):** _Não me abusa a paciência, guria! Já matei gente melhor por bem menos!_ Jonathan bota Jenny na mira

**O (Jeung):** _Jonathan, baixa essa bola! Até onde se sabe, não existe prova alguma de que os Espíritos do Século eram agentes comunistas._

**R (Jonathan):** _Jet Black deserdou no Vietnã!_

**O (Jeung):** _Considerando o que o governo fez com os soldados afro-americanos, e considerando que ele próprio era um, não me surpreenderia a reação dele. Ver seus irmãos de sangue sendo passados no moedor de carne enquanto os branquelos brincavam de_ Surfin' in Saigon _não me parece uma razão muito ruim para deserdar!_

**R (Jonathan):** _Se vocẽs vão ficar nessa palhaçada comunista, pode deixar que eu faço o serviço, seu pivete Amarelo!_

**S:** Chega! Aciono a Carta X! (ela diz, tocando um cartão vermelho)

> Como o cenário de Shadow of the Century envolve um  mundo de problemas sérios envolvendo misoginia, racismo e machismo, Priscila incluiu a Carta X como regra para evitar problemas. Quando um assunto incomoda demais alguém na mesa, ela pode acionar a Carta X para que ele pare de ser mencionado na mesa

**S:** E além disso, vou usar meu Aspecto _Eles precisam enxergar além do que existe_ para declarar que Jenny vai enfiar a mão na cara do Jonathan sem que ele tenha reação. Não quero dano, quero que ele fique chocado! (Jenny) _Chega, seu babaca alienado! Você acha que é o único fodido nessa merda? FDP! Eu vi gente morrer em Stonewall! Morrer, tá ligado o que é isso! Perdi a porra de uma amiga minha porque um bando de cretino fascista que nem você atirou contra ela! Ela parecia um queijo suiço quando vi o corpo, tiveram que lacrar o caixão da coitada, ela parecia um saco de carne e sangue. Tudo porque ela dava uns amassos em outras amigas minhas. Tu vem falar de Saigon? E o que foi o Napalm? Mataram crianças! Crianças, caralho! Enfia essa tua Ameaça Vermelha no Rabo!_

**P:** Rodrigo, estou Forçando o seu Aspecto de _Veterano de Muitas Batalhas_ para que você tenha um Gatilho de Estresse Pós-traumático... Conforme ela vai falando isso, você LEMBRA dos eventos, do cheiro do Napalm, dos corpos triturados... De tudo o que você viu, todos os esqueletos no armário que você deixou em operações que você não pode revelar por segredo de estado.

**R:** Caramba, Priscila. É só a interpretação do Jonathan.

**P:** Eu estou passada de você tentar bancar o lobo solitário, Rodrigo. Guarda o Bruce Willis dentro de você para quando o pau torar. Vai por mim, vai ter o bastante mais para frente.

**R:** Bem... Vou receber 2 PDs, certo? Já que é a sua Forçada mais o tapão na cara que Jenny deu, certo? E posso dizer que ele está catatonico? Ele deixa a arma cair no chão e olha para o vazio, literalmente preso no inferno que era seu passado, relembrando das mortes em especial... Pessoas andando como zumbis tentando o pegar, algumas as tripas para fora, outras com pedaços do cérebro vazando dos buracos de balas, as mais assustadoras são as crianças pegando fogo...

**P:** Maravilha! Digna de 2 PDs, concordam? (Ela diz para Olívia e Sérgio, que fazem que sim com a cabeça). _This is the end / My only friend, the end / Of our elaborate plans, the end / Of everything that stands, the end_ Essa é a música de fundo nesse momento.

**R:** The End do Doors! _Apocalypse now_ a essa altura do campeonato? Gostei. 

**O (Jeung):** _Gente, vamos parar de brigar entre nós, beleza? Não precisamos facilitar o trabalho dos caras do **Bastião** sendo babacas uns com os outros._ Digo, tentando ser o cara a colocar panos quentes

**R (Jonathan):** _Você está certo, Jeung... E não sou mais o mesmo daquela época, guria. Vi o suficiente para ver as merdas que esses governos do caralho fazem com todo mundo. Vamos resolver essa bagaça pela Branca de Neve que esses cuzões mataram._

**S (Jenny):** _Está certo... Vamos... Precisamos resolver isso... Mas, como eles vieram para nossa realidade?_ 

**P (Yen Sid):** _Algum Usuário da Magia Disney deve ter manipulado as barreiras pentadimensionais entre as realidades. Como diria seu escritor Asimov_ "Qualquer tecnologia suficientemente avançada é indistinguível da magia."

**R (Jonathan):** _Parece que aquele lance de Variância era verídico então... Mas então... Devem ter tido um baita trampo para passar essa meia dúzia de lunático. Se querem realmente nos invadir, precisariam de um buraco enorme na nossa realidade... Mas como eles fariam isso?_

**O (Jeung):** _Será que teria a ver com aquele dispositivo sabotado que encontramos no_ Peter Pan's Flight? _Esse cara aqui._ Jeung apresenta a Yen Sid o ***Dispositivo de Controle Sabotado*** que encontramos

> Em um momento no passado, quando descobriram sobre os Agentes Infiltrados, eles encontraram um ***Dispositivo de Controle Sabotado*** cuja tecnologia estava anos luz acima da dos anos 1980.

**S (Jenny):** _Sim... Verdade, pode ser isso!_ (para Priscilla) Agora quero usar aquela Invocação Gratuíta da ***Física de Multiversos***. Isso justificaria esse dispositivo ter uma tecnologia muito avançada... E provavelmente ativada pela VHS!

**P:** Maravilha! Funciona para mim! Yen Sid pega o dispositivo de suas mãos, Jeung e faz uns passes esquisitos sobre o mesmo, depois de o abrir e ver os componentes internos: para vocês isso é avançado demais, não lembra os Z80 e 6502 usados nos computadores pessoais, mesmo aqueles 8088 ou Motorola 68000 dos PCs e Apple Macintosh. (Yen Sid) _Sim... Isso com certeza foi criado usando tanto Magia Disney quanto Imageneering. Isso foi desenvolvido pelo Bastião em minha realidade original. Certamente se infiltraram nessa realidade para seus fins! Seja qual for eles... Não são bons!_

**R (Jonathan):** _Maravilha... Ao menos uma boa notícia. Vamos achar esses caras. Quero chutar uma bundas e mascar chiclete. Mas meu chiclete acabou..._

> Priscila sorri enquanto corre a _playlist_ que ela montou no celular e procura uma música específica. ***Mighty Wings*** de _Cheap Trick_, da trilha sonora de Top Gun!

**P:** _Você não precisa esperar muito, pois os ninjas estão chegando... Ou melhor, os **Soldados de uma Realidade Alternativa!** E parecem estar realmente prontos para meter_ laser _na cara de vocês!_ ZOING! ZOING! ZOING!

**R (Jonathan):** _Cuidem do vovô aí, que parece que é o alvo deles!_

**P:** Na realidade, eles também querem derrubar vocês. Eles são um grupo grande, deve ter uns 4 deles para cada um de vocês!

**R:** EITA! Bem... Vamos primeiro sobreviver a esses fulanos!

**P:** Sim... A sorte tua, Jonathan, é que você tem sua Façanha ***Manda Mais que Tá Pouco***, então eles atacam de maneira simples. Mas Jenny e Jeung não têm tanta sorte!


> Priscilla rola quatro vezes para cada um dos grupos de ___Soldados Armados Totalitários de uma Outra Realidade___ que são _Razoáveis (+2)_, mas recebem +1 nos Ataques por Tamanho contra os agentes da NOVA e Yen Sid, exceto contra Jonathan graças à sua *Façanha*
> 
>  + **Grupo 1 (Atacando Jenny): `-+0+`{: .fate_font} - +1 nos dados, total _Ótimo (+4)_**
>  + **Grupo 2 (Atacando Yen Sid): `0+++`{: .fate_font} - +3 nos dados, total _Fantástico (+6)_**
>  + **Grupo 3 (Atacando Jeung): `-0-0`{: .fate_font} - -2 nos dados, total _Medíocre (+0)_**
>  + **Grupo 4 (Atacando Jonathan): `+000`{: .fate_font} - +1 nos dados, total _Bom (+3)_ por não receberem o bônus por Tamanho**

**P:** Antes de me voltar para vocês, vou resolver a defesa e um possível ataque de Yen Sid. Vocês vêm ele fazendo gestos mirabolantes, e um escudo se levanta

> Priscila rola a defesa de Yen Sid por seu papel _Magia Disney_, e consegue `0-+0`{: .fate_font}, dando +0. Somado ao seu nível _Excepcional (+5)_ em _Magia Disney_, ele fica em _Excepcional (+5)_. Ainda assim, ele falha na Defesa

**P:** Vocês notam que, apesar de todo o poder dele, um tiro _laser_ atravessa o escudo e forma uma escoriação no ombro, nada demais.

> Priscila então realiza o ataque de Yen Sid contra o grupo que o Atacou. Consegue no rolamento `++--`{: .fate_font}, novamente para +0 e com um _Excepcional (+5)_ como resultado final. Ela então rola a defesa dos Soldados, e obtem `++00`{: .fate_font}, somando ao _Razoável (+2)_ e o bônus de tamanho, com um resultado final _Excepional (+5)_ para um Empate... 

**P:** Bem, vocês notam que ele faz uns gestos e uma onda de vento avança contra os soldados que o atacaram, ***derrubando-os*** no chão. Agora é a vez de vocês irem se defendendo dos ataques dos Soldados e agirem. Quem começa? (Diz Priscila disponibilizando uma velha fita K7 como _token_ para a Iniciativa Cinemática)

**S:** Posso ir primeiro? Jenny não é exatamente combativa... Mas ela pensa em correr para evitar o combate... Ou melhor: tenho meu _Formado na Faculdade_! Posso pegar _Lutar_ usando ela nessa cena? Pelo menos vou sobreviver a isso

**P:** Sem problemas! Então você vai usar esse _Lutar_ para tentar se defender, certo? Normalmente você não poderia, mas não vejo grande problemas.

**S:** Sim... Minha ideia é chegar perto o bastante dos caras que atiraram contra mim.

> E Sérgio é sortudo! `++++`{: .fate_font} na defesa para um total _Fantástico (+6)_ somando o Lutar _Razoável (+2)_ que Jenny recebeu do _Formado na Faculdade_. Isso permite que ela se esquive sem se machucar.

**S:** E quero agora ___Criar uma Vantagem___ para empurrar esses caras contra o que estão atacando Jonathan, assim ele senta o braço em toda essa turma de uma vez.

**P:** Como você vai fazer isso então?

**S:** Acho que o que faz mais sentido nesse caso é usar meu Lutar improvisando um Tae-Bo da vida.

**P:** Tae-Bo! Gostei... Manda bala...

> Sérgio rola `+-0+`{: .fate_font} para um resultado de _Bom (+3)_ em Lutar. Os Soldados rolam `-+0-`{: .fate_font} para um resultado de _Razoável (+2)_, poranto ela consegue criar a vantagem

**P:** Os Soldados ficam impressionados com a sua impetuosidade, e recuam na direção dos que estão atacando Jonathan. (Ela diz colocando um Aspecto ***Soldados Acuados***)

**S:** Legal! Quem é o próximo? Olívia ou Rodrigo?

**O:** Não tenho muito o que possa fazer... Posso Conceder?

**P:** Quer nem tentar?

**O:** É arriscado demais... Nunca teria chance de nada... Pensei no seguinte... Eu levo uma Consequência Suave ***Ombro Ferido*** e isso me garante...

**P:** Vamos ver a sua defesa pelo menos

> Olívia rola sua defesa e se dá mal: `---+`{: .fate_font} com -2... Como tem _Medíocre (+0)_ em Atletismo ou outras perícias úteis... Ele consegue um _Péssimo (-2)_ na defesa, recebendo 2 de Estresse.

**O:** OK... Vou receber esse Estresse na _Consequência Suave_ ***Ombro Ferido*** e Conceder. Jeung vai fugir e procurar apoio, seja de Timothy Andalasia ou da Sargento Sandra Williams... 

**P:** Bem... Isso vai complicar a vida dos demais, então para mim é o suficiente. Você recebe então 2 PDs pela Concessão: 1 por Conceder e 1 por ter sofrido uma _Consequência_ nesse combate.

**O:** Posso, no próximo turno, deixar meio que engatilhado trazer ou Sandra, ou alguns ___Seguranças do Parque___ chamados por Timothy?

**P:** Seria muito cedo... Vamos ver se eles derrotam os caras, senão no outro turno pode ser.

**O:** OK... Então passo adiante para Rodrigo. Agora sim é hora de você sentar a pua!

**R:** Pode deixar: não tem muito papo com Jonathan, ele vai sentar o braço nesses caras sem muita dó!

**P:** Primeiro se defende... Acho que é por Lutar, não?

**R:** Sim. Vamos sentar o cacete nesses caras.

> E os dados sorriem na Defesa de Jonathan: `0+++`{: .fate_font} para +3... Somando com o _Lutar Ótimo (+4)_, ele consegue um _Épico (+7)_ contra o _Bom (+3)_ do Ataque dos Soldados!

**P:** Maravilha! Sucesso com Estilo! Me diz o que você quer como Impulso...

**R:** Enquanto me defendo dos caras, eu golpeio a mão de um deles e o desarmo, roubando a arma dele.

**P:** Normalmente isso seria apenas um Impulso... Mas que tal se você ficar com uma ***Arma Laser do Futuro***, mas sem Invocações Gratuítas? 

**R:** Sem problemas para mim... Agora vou descer o Pipoco... E já vou declarar que, dependendo do caso, vou dividir o Estresse gerado entre os alvos! _Warriors! Venham Brincar!_

**P:** OK... E lembre que Jenny usou o Tae-Bo dela para empurrar um dos grupos contra você, deixando-os ***Acuados***

**R:** Vamos ver se os dados ajudam!

> Priscila passa um cartão de Aspecto para Rodrigo da ***Arma Laser do Futuro*** e Rodrigo rola `+00-`{: .fate_font}, o que resulta em +0, mantendo o *Excepcional (+5)* original. E é a vez de Priscila ter um rolamento muito ruim: `----`{: .fate_font}, levando a -4. Mesmo com o Tamanho contando na Defesa, os soldados ficam em _Ruim (-1)_! Ela não vai deixar tão fácil!

**P:** Bem, eles são ___Soldados Armados Totalitários de uma Outra Realidade___, então são treinados o bastante para rerrolar!

> Priscila paga 1 PD e rola os dados novamente... E não é muito melhor: `--+-`{: .fate_font} para -2 no dado, ficando com um _Regular (+1)_. Ainda é um Sucesso com Estilo para o Ataque de Jonathan!

**P:** Bem... Ao menos vai reduzir a paulada. Pelo tamanho cada 1 tem uma Caixa de Estresse, o que quer dizer que precisa 2 de Estresse para derrotar. Vai dividir ainda assim? 

**R:** Sim. Se estou certo, posso transferir então 4 de Estresse adiante, certo? E vou pegar a Invocação de ***Acuados*** que Jenny gerou para pegar o grupo que a atacou e colocar para +2 levando para 6.

> Priscila rola a defesa desse grupo e deu `0+-0`{: .fate_font} para +0, totalizando _Bom (+3)_ na Defesa

**P:** Você tem 6 tensões de esforço no ataque e a defesa deles é 3... Sucesso com Estilo de novo! Quer dividir novamente?

**R:** Não... Vou abater esses caras e baixar em 1. Ficaria 5, suficiente para derrotar eles, certo?

**P:** OK... Que impulso você quer?

**R:** Eles vão estar ***Apavorados***. Os dois grupos que sobram vão ficar bem assustados... E com razão, já que, como todo mundo já agiu, eu escolho quem age primeiro no novo turno... E serei eu!

**P:** Então rola e aproveita o momento, Bruce Willis!  _Yippee ki-yay, motherf$#+er_!

> Rodrigo rola e consegue `-++-`{: .fate_font}, para 0 nos dados e _Excepcional (+5)_ no seu resultado final por lutar.

**R:** O alvo é o grupo que atacou Jeung!

> Priscila rola a defesa contra os soldados que atacaram Jeung e não é muito bom o resultado, para `-0+-`{: .fate_font}, conseguindo -1 para um _Razoável (+2)_ na Defesa

**R:** Novamente, pago 1 PD para rerrolar

> Ela paga o PD e consegue um `+--+`{: .fate_font}, para 0, ficando com um _Bom (+3)_ na Defesa

**P:** Teria sido melhor pagar PD para +2... Mas ao menos não rolou outro por estilo

**R:** Não tão rápido! Vou usar o fato de eles estarem ***Apavorados*** para +2! Isso ainda garante o Estilo! E... (ele sorri maldoso), baixo em 1 o rolamento para ganhar um novo Impulso ***Apavorados***...

**P:** Eita ferro! Esses caras vão entrar no Moedor de Carne! O grupo que atacou Jeung foi derrotado! Falta só o que atacou Yen Sid!

**R:** Agora vai, Jenny... (Rodrigo passa a fita K7 para Sérgio)

**S:** Jenny vai no Tae Bo de vez! 

> Sérgio rola novamente o _Lutar Regular (+2)_ que Jenny possui por _Formado na Faculdade_, e consegue `+++-`{: .fate_font} para +2, elevando o Ataque para _Ótimo (+4)_. Enquanto Priscila consegue novamente `--+-`{: .fate_font} para -2 na defesa dos soldados, ficando com _Razoável (+2)_

**P:** Uso o Aspecto dos Soldados para +2 na defesa, levando para _Ótimo (+4)_

**S:** E eu uso o Impulso ***Apavorado*** que Jonathan criou para +2 também, levando o Ataque para _Fantástico (+6)_... Esses malucos já eram!

**P:** Bem... Eles estão derrotados, provavelmente mortos...

**S:** Não... Ao menos um deles fica para interrogarmos

> Como o grupo Derrotou os Soldados Autoritários, o destino deles está nas mãos dos mesmos. Poderiam simplesmente declarar que todos seriam mortos, mas é sempre bom aproveitar e capturar um para um interrogatório, com Jenny fez

**P:** O uniforme tático deles lembra algo similar ao Uniforme de Mickey, mas com algo de Stormtrooper de Star Wars no meio. Vocês arracam a parte que parece a cabeça do Mickey fora de um deles: o cara tá só o pó da rabiola.

**R  (Jonathan):** _Vamos lá, xarope! Começa dando a fita: qual é o objetivo de vocês?_

**P (Soldado):** _A gente... Está no projeto da... Trilha Disney..._

**R (Jonathan):** _Trilha Disney, que merda é essa?_

**P (Soldado):** _Vamos abrir um buraco na realidade para que nossas tropas invadam esssa realidade pífia de vocês._

**R (Jonathan):** _Usando a Variância Hiperdimensional Síncrona? E o que a tal Leanna tem a ver com isso?_

**P (Soldado):** _Ela ouviu nossa comandante da operação conversando com nossa realidade. Precisávamos silenciá-la antes que passasse a informação adiante. Tentamos usar o chip de controle mental que tínhamos, mas supomos que essa tecnologia não funcionou aqui._

**R (Jonathan):** _E vocês a queimaram, certo? Mas não antes de alguém da NOVA reparar!_

**S (Jenny):** _Espera um pouco! Vocês devem precisar de tempo para preparar o que quer que seja que vai abrir o buraco na realidade. Mesmo com a VHS, a energia seria demais para tanto!_

**P (Soldado):** _Vocês são tolos! Energia para ser obtida pela Magia Disney e pelo Imageneering nesse parque é o que mais tem. Todos os equipamentos estão preparados e a energia obtida. A Trilha Disney se abrirá amanhã... A... invasão... é... iminente..._ Ele diz enquanto começa a babar e convulsionar!

**S (Jenny):** _Droga! Capsula de Suicídio!_ Tem como parar ele?

**P:** Nem... O veneno é da realidade deles, mais avançado que qualquer coisa. Jeung, no momento que você volta, você vê que Jenny está segurando o cara já morto. Vou pegar a ideia que você teve e tanto Samantha quanto Timothy chegam com você. (Samantha) _Que diabos aconteceu aqui?_ 

**R (Jonathan):** Esses malucos estavam atrás do vovô... Foram os mesmos sacanas que mataram Leanna. Ele descobriu uma conspiração da pesada. Timothy, esses caras querem usar o Magic Kingdom como porta de entrada para nossa realidade.

**P (Timothy):** _Isso é loucura!_ (Yen Sid) _Não meu caro... O que eles disseram é verdade. O **Bastião** está tentando invadir nossa realidade, usando a Energia da Magia Disney como forma de abrir esse portal entre a realidade deles e a nossa..._

**S (Jenny):** _Isso mesmo!_

**P (Timothy):** _Mas... Como vamos parar o parque? Não temos nem como justificar! O Walt Disney World NUNCA fechou! Mesmo a Disneyland só fechou quando mataram JFK e em um terremoto!_

**R (Jonathan):** _Temos que achar o que quer que eles utilizem para abrir portais para a realidade deles e ver se conseguimos desativar esses dispositivos... Imagino que sejam dispositivos. Vovô, será que dá para dar uma ajudar..._

**P:** Yen Sid vai para um velho quadro de Walt Disney e o move de lado, depois abrindo um painel deslizante e pegando uma caixa com uma estampa que forma uma Silueta do Mickey. (Yen Sid) _Imaginava que cedo ou tarde isso iria acontecer, mas esperava que fosse tarde, muito mais tarde, para ser muito honesto. Entendam: para a Magia Disney, imaginação é poder. Walt dizia que **se você pode imaginar, você pode fazer**. E fiz isso aqui._

**S (Jenny):** O que tem aí?

**P: (Yen Sid):** _Eu chamo esses dispositivos de **Elencadores Disney**. Ao usar esses dispositivos, vocês poderão trazer ajuda dos personagens que Walt tanto criou e amou para ajudar vocês... Entretanto, devo dizer que isso é muito perigoso: vocês terão que imergir nos personagens, aceitar eles, para extrair seus poderes... Fica a critério de vocês aceitar esses dispositivos, e os usar. Infelizmente posso fazer muito pouco além disso. Não sou tão mais jovem._

**S:** Como são esses dispositivos?

**P:** Sabe aqueles morfadores do Changeman? Parecido com aquilo, mas redondos e com um símbolo que lembra a silueta de algum personagem da Disney. Tem vários desses, cada um com uma silueta diferente.

**R (Jonathan):** Só imagino que tenha aí herois, né? Se for assim, acho que apenas alguns dos Príncipes ou Peter Pan seriam úteis em batalha.

**P:** Pensando em algum vilão?

**R:** Que tal o Capitão Gancho?

**P:** Parece divertido. Imagino que Jenny pegaria o do Peter Pan, certo?

**S:** Claro, bem condizente com ela. Ela vê a silueta de Peter Pan e pega ele sem pensar meia vez.

> Priscila sorri e passa a Sérgio um cartão escrito ***Elencador Disney: Peter Pan***

**R:** E Jonathan iria de Capitão Gancho, imaginando que ele esteja aí

> Priscila passa a Rodrigo o cartão com **Elencador Disney: Capitão Gancho**

**P:** E Jeung?

**O:** Jeung sempre foi cerebral demais, quase ao ponto da covardia... Mas nunca deixaria os amigos na mão, como provou procurando reforços.

**P:** Muito Mickey isso, certo?

**O:** Sim, esse é o objetivo. Ele vê um desses negócios esquisitos com a silueta de Mickey e pega

> Priscila passa a Olívia o cartão com **Elencador Disney: Mickey Mouse**

**P (Timothy):** _Bem... Então... Yen Sid... Vamos precisar dar uma repaginada aqui depois desse conflito. Os seguranças vão tirar essas caras discretamente para a Polícia de Orange, onde vão ser detidos pela morte de Leanna. E quanto a isso... Façam o possível para que não usem o Parque para fins malignos._

**R (Jonathan):** _Pode deixar. Esses lesados fascistas vão saber o que é bom para a tosse se tentarem nos invadir_

---

Jonathan, Jenny, Jeong, agora sabem porque Leanna foi morta, e conseguiram um aliado poderoso e ajuda especial em itens poderosos... Mas... Mesmo considerando tudo isso, serão eles capazes de impedir _O Mais Feliz Juízo Final da Terra_

Isso ficará para a próxima sessão de _Shadow of the Century_
