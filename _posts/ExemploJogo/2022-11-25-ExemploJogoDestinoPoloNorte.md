---
title: "Exemplo de jogo no Destino do Polo Norte"
teaser: "Os Elfos da Vila de Natal vão levar as renas para um treinamento intensivo nos Velhos Estábulos, e com isso tambem terão encrencas"
layout: post
date: 2022-11-25
categories:
    - RPG
header: no
excerpt_separator: <!-- excerpt -->
tags:
  - Fate
  - Fate-Core
  - Aventura
  - Exemplo de Jogo
  - Destino do Polo Norte
  - Dry Fate
  - ExemploDeJogo
---


> Vanessa e seus amigos estão no clima de Natal e então decidiram jogar uma coisa rápida e divertida, e usaram o [Destino do Polo Norte](fabiocosta0305.gitlab.io/rpg/DestinoPoloNorte/), um jogo criado com base no [Dry Fate](http://fabiocosta0305.gitlab.io/fate/fate%20b%C3%A1sico/fate%20acelerado/hacks/minimalista/DryFate/). No caso, seu amigos criaram alguns personagens baseados nos vários Clãs de Elfos:
>
> + ***Gabriela*** criou _Metinli_, um elfo do clã Hohooja, que cuida dos animais da Vila de Natal, incluindo as importantes renas, que são treinadas e preparada todos os anos para voarem pelo mundo na Noite de Natal... No caso, ele está cuidando de Strink, uma rena ainda bastante nova, que ele espera um dia puxar o trenó do Papai Noel. Entretanto, apesar de tudo, gosta das coisas bem organizadas, o que o torna quase parecido com um dos Kapunki
> + ***Rodrigo*** decidiu jogar com um amigo de Metinli, _Pimmipo_. Do Clã Kapunki, que cuida de manter as coisas na Vila de Natal organizadas e limpas, Pimmipo entretanto gosta bastante de atuar no ar livre, sendo que ajuda muito na limpeza e organização dos estábulos e do Rancho onde os Holhooja cuidam dos animais. Desse modo, ele mantem bastante contato com os Holhooja, o que ele quase pode ser.
> + ***Heitor*** trouxe para o jogo _Enelda_, uma Inventineira. Como todo Inventineiro, ela adora criar novas coisas, em especial brinquedos. Mas ela tem ideias tão absurdas que muitas vezes as coisas acabam simplesmente explodindo, espalhando fumaça e fuligem por todos os lados, inclusive em suas roupas.
> + ***Maria*** então incluiu um elfo em Treinamento, _Pumpin_. Recém saído das _poinsettias_ de onde todos os elfos nascem, _Pumpin_ ainda está tendo experiências com Elfos de vários clãs para aprender e decidir como ele pode fazer melhor para ajudar a todos. Ele ainda é novinho, com uns 30 anos, mas está disposto a tudo para melhorar, mesmo que meta os pés pelas mãos de quando em quando.
>
> Gabriela começa mencionando o fato que passou algumas semanas desde o Natal e o tempo está melhor, então está na chamada _Semana de Descanço_, onde todos os Elfos podem fazer o que bem entender. Embora muitos continuem fazendo coisas úteis para o seu clã e para o Natal, outros simplesmente ficam de bobeira por um tempo. Seria o plano de todos, se não fosse o fato de Kara, uma das Holhooja mais importante, convidar a todos para ajudarem Metinli no Acampamento de Treino das Renas, onde Renas mais novas são colocadas em treinamento intensivo com aquelas que já tiveram a chance de puxar o trenó. Metinli aceitou na hora, e Pimmipo também, já que é o momento que ele pode ser o Holhooja que poderia ser se não desejasse uma cama quentinha em um quarto limpinho mais do que o ar livre. Eles então convidaram Enelda para ajudar, a quem conheciam da época em que eram Elfinhos em treinamento. Como ela estava com Pumpin, os dois ajuntaram-se com ele.
>
> E depois de um café reforçado preparado por Gordot, o Chefe das Cozinhas, e carregar todos os materiais necessários no Acampamento em algumas carroças, eles pegam o caminho em direção aos Velhos Estábulos, no alto do Grande Fiorde, quase no fim da área da Vila de Natal.
>
> Quais trapalhadas e coisas incríveis eles descobrirão? Só tem um jeito de descobrir.

---

<!-- excerpt -->

**Vanessa:** Vocês saem pelo Velho Caminho do Leste, seguindo em paralelo com o Rio das Renas, que desemboca no Mar. Conforme vocês caminham, alguns elfos puxam uma cantiga de caminhada. Pelas vozes, parece se tratar de Kara e das famosas trigêmeas Nöli, Nikäri e Norrä, uma Carteirista, uma Inventineira e uma Holhooja. O caminho é tranquilo, o sol brilha, mas o tempo não está quente, muito pelo contrário: faz aquele friozinho gostoso que, apesar de tudo, vocês curtem sem utilizar gorros ou roupas de frio. O que cada um de vocês está fazendo?

**Gabriela:** Metinli está sempre ao lado de Strinks

**V:** Sim... Você nota que Strinks está empolgada, correndo faceira na frente das duas carroças com os suprimentos, quase de maneira perigosa.

**G (Metinli):** _Ei, Strinks, para com isso! Vocẽ ainda vai se machucar, e se você quer um dia puxar o Trenó do Bom Velhinho, precisa se comportar! Elas são renas mais velhas e mais fortes e você vai ter que aprender e melhorar bastante!_

> Metinli pode fazer isso graças a sua ***Façanha do Clã*** _Falar com Animais (Literalmente)_. Metinli consegue entender o que os animais falam, mas mesmo assim um animal não pode falar sobre coisas que ele não saiba ou compreenda.

**V (como Strinks):** _Mas eu estou animada! Minha primeira vez no Acampamento, sempre ouvi as outras renas comentar. Quero conhecer os Velhos Estábulos, e toda a cantoria tá me deixando bem empolgada._

**G (Metinli):** _Eu sei, Strinks, mas precisamos manter a calma... Não queremos que nada de ruim aconteça, certo?_

**V:** Strinks faz que sim com a cabeça. Vocẽs notam que Nörra se aproxima de vocês. (Nörra) _Strinks parece ter muito potencial, ainda que esse ano ainda não dá para ela._

**G (Metinli):** _Sim... Mas logo ela vai estar puxando o Trenó do Velhinho!_

**Rodrigo (Pimmipo):** _Isso mesmo! Eu vi Metinli e Strinks treinando todo dia! De vez enquando eu mesmo ajudava eles a treinar._

**V:** _Eu sei!_ Diz Nörra, rindo _Com certeza, senão ela não estaria indo ainda para o Acampamento... A própria ida é um treinamento: puxar essas carroças é pesado, ainda mais quando chegamos na Subida do Fiorde. Aliás... Tá na hora de revezar alguma das outras renas e acho que Strinks pode ajudar... O que vocês acham?_

**R (Pimmipo):** _Verdade. Já estamos longe dos Chalés e das Oficinas... Podemos fazer uma pausa e fazer a troca das renas com calma._

**G (Metinli):** _Que tal?_

**V:** _Eu não sei... Acho que podemos andar mais um pouco até o Bosque do Caminho. Lá podemos parar e tem uns laguinhos onde podemos dar água às renas..._

**Maria:** Será que.... Daria para Pumpin lembrar de algum caminho alternativo ou coisa do gênero? Sabe, ele tá _Em treinamento_, então pode ser que tenha ouvido de algum outro Holhooja de um caminho ou outra coisa assim?

> Vanessa pensa um pouco...

**V:** Bem... vai que de repente Pumpin entendeu tudo errado, pode ser divertido ver as encrencas. OK, mas não é muito fácil para lembrar disso. Mas qual Competência você vai usar?

> No Dry Fate, as Competências são os níveis de perícia associados a cada um dos Aspectos do Personagem. 

**M:** E se ele usasse o fato de ___Querer Deixar o Papai Noel e os outros Elfos orgulhosos___? Ele está fazendo de tudo para ver em que clã ele se encaixa e, acima de tudo, quer ser um bom elfo.

**V:** Não vejo porquê não... Entretanto, será com uma dificuldade ___Ótima (+4)___. Além disso... Será uma ação de _Criar Vantagem_

**M:** Tudo bem, vamos ver o que os dados mostra.

> Maria rola os dados e consegue `+--+`{: .fate_font}. Com isso, os dados resultam em um ___Medíocre (+0)___. Pumpin tem um ___Razoável (+2)___ na competência rolada, o que mantem o resultado em ___Razoável (+2)___ na soma com os dados.

**M:** OK... Acho que vou gastar um Ponto de Destino nesse Aspecto para somar +2. Isso seria o suficiente para um Empate, certo? Acho que não precisamos de mais que isso no momento, já que é um _Impulso_ e isso basta, certo? Pumpin se esforça bastante, ele não quer decepcionar os demais

**V:** Sim... Para mim está bem.

> Maria passa um pequeno boneco de brinquedo que está sendo usado como Ponto de Destino para Vanessa

**M (Pumpin):** _Gente... Acho que ouvi Winky e Yilliä comentando sobre um caminho, naquela direção, que é mais fresco e tem um riacho perto. A subida fica um pouco mais puxada, mas com as renas bem descansadas deve ficar tranquilo para subirmos, não?_ Pumpin diz bem tímido, já que é a primeira vez que ele está em algo tão importante...

**V:** Nörra vê Kara chegando perto e elas conversam baixinho alguma coisa. Como Kara é uma das Holhooja mais importantes, e sua rena Blippini é uma das mais veteranas em puxar o Trenó, ela é a mais respeitada elfa do grupo. Ela observa o caminho e diz _Talvez Pumpin esteja certo: ir pela Fenda é um caminho muito fresco... Tem uns riachinhos, mas o caminho é bem claro. Acho que podemos ir e chegar em um local para almoçar e trocar as renas. Só não quero chegar nos Velhos Estábulos durante a Noite... Lá é muito frio, e o caminho é traiçoeiro, pois pode até mesmo nevar do nada._

**Heitor (Enelda):** _Então, o que estamos esperando? Quanto mais tempo aqui parados, pior será nossa situação._

**V:** Vocês vêem os demais Holhooja tocando as renas para a direção sugerida por Pumpin. Realmente, o caminho é muito mais fresco, um ventinho gostoso passando até onde vocês chegam no ponto de parada, uma clareira perto de um pequeno fio de água. Kara então diz. _Bem, vamos fazer uma pausa aqui, almoçar e depois vamos seguir adiante. Acho que podemos colocar algumas das renas mais novas para puxar as carroças. Strinks, Tilpa, Signe e Livaer podem fazer isso. Mas primeiro, vamos soltar as renas para elas irem pastar e beber água._

**G (Metinli):** _Vai lá, Strinks, e se prepara que a subida vai ser puxada_

**R:** Enquanto soltam as renas, Pimmipo e outros elfos vão descarregando os cestos com a comida que a Senhora Noel, junto com Gordot e outros Kapunki prepararam. (Pimmipo) _"E pensar que lá em cima vai ser por nossa conta."_

**M (Pumpin):** _Como assim? Não vai ter como a gente conseguir comida?_

**R (Pimmipo):** _É um pouco demais achar que a Senhora Noel e todos na Cozinha poderiam preparar tudo para uma semana, incluindo elfos e renas... O que fizeram foi colocar os ingredientes e lá a gente vai fazer as demais refeições nós mesmos._

**G (Metinli):** _O que é uma boa... Por mais que os biscotinhos nutritivos da Nörra sejam bons, você pode ficar a base deles só até um certo ponto_

**V:** Nörra ri do seu comentário, Metinli, e as irmãs dela riem junto. É curioso como, apesar de suas aptidões totalmente diferentes que levaram cada uma delas até um clã, elas riem igualzinho, o que faz todos cairem na gargalhada. Depois de um almoço reforçado, vocês então se preparam para a Subida do Fiorde: são mais ou menos um 800 metros de subida que na prática são mais de 5 quilômetros, já que vocês vão subir por uma das estradas que circundam o Fiorde. Kara se aproxima de você, Metinli, quando você afivela Strinks à uma das carroças, onde Blippini também está (Kara) _"Bem... Já que Strinks tá nessa carroça, quero ver se você cuida bem dessas renas, Metinli. Você vai ser o guia dessa carroça. Nörra irá na outra, conduzindo como lider Kaino, a rena dela. Minha Blippini não costuma ser turrona, mas toma cuidado como lidar com ela, ela não aceita ser comandada de maneira errada. Vai ser um bom teste para você e Strinks."_

**G (Metinli):** _Está certo... Strinks, vamos fazer o melhor... E vou tomar todo cuidado, Blipinni._ digo para as duas, enquanto termino de afivelar as mesmas e colocar as rédeas. _Melhor você vir comigo e com Kara, Pumpin. Acho que Pimmipo e Enelda vão na outra com Nörra, né?_

**R (Pimmipo):** _Eu vou a pé: quero aproveitar e fazer uma caminhada mais forte. Deixa Winky ir com Nörra, já que Tomps está com Kaino na outra carroça_

**G (Metinli):** _Está certo! Vamos nessa então!_ digo, enquanto subo na carroça, Kara sentando ao meu lado e me passando as rédeas.

**M (Pumpin):** Vou subir também e prestar bastante atenção em Metinli e Kara: está bem divertido, quem sabe não vire Holhooja.

**V:** Logo vocês puxam o início da subida. Agora, vou fazer uma Disputa entre você, Metinli e o ***Caminho do Fiorde***: Vai saber as dificuldades climáticas e problemas que podem acontecer. Preciso relembrar as regras?

**G:** Não... Melhor de 5, quem tiver melhor resultado na rodada marca 1 vitória e 2 em caso de Sucesso Com Estilo, Empates provocam mudanças na situação.

**V:** Isso... A dificuldade do ***Caminho do Fiorde*** atual é _Razoável (+2)_. Então vamos lá... Acho que você vai rolar usando sua Competência como Elfo do Clã Holhooja, certo? E vou incluir também um Aspecto de ***Caminho cheio de complicações*** no ***Caminho do Fiorde***

> Vanessa está recorrendo ao que é conhecido como Fractal do Fate: ao colocar as complicações do Caminho do Fiorde, ele o torna um "personagem" com "oposição ativa" por meio de possíveis mudanças no clima

**G:** Sim. Vamos lá então.

> Vanessa rola `0--0`{: .fate_font} para um -2 nos dados, enquanto Gabriela rola `+-+0`{: .fate_font} para um +1 nos dados. Isso resulta em um ___Medíocre (+0)___ do Caminho contra um ___Excepcional (+5)___ de Metinli. 

**G:** Legal! Sucesso com Estilo! Duas vitórias para nós!

**V:** Verdade. O caminho está tão suave que vocês conseguem subir com uma velocidade até um pouco maior que esperada. Em pouco mais de uma hora vocês cobriram quase dois terços do caminho. _"Que bom! Strinks não está deixando a desejar junto com Blippini. O clima está ajudando também. Logo estaremos lá no alto."_ afirma Kara.

**G: (Metinli)** _Sim, isso vai ser fácil!_ 

**V:** Próxima rodada da Disputa

> Vanessa consegue um `00+0`{: .fate_font} pelo Caminho, dando +1nos dados, enquanto Gabriela consegue um `0+00`{: .fate_font} por Metinli, conseguindo +1 nos dados. Isso resulta em um ___Bom (+3)___ para o Caminho e um ___Ótimo (+4)___ para Metinli. Então Vanessa decide complicar.

**V:** Bem, como esse é um ___Caminho cheio de complicações___, vou pagar 1 Ponto de Destino do meu Estoque para somar +2 ao resultado meu e com isso ganhar essa rodada. O clima fica meio esquisito, uma ventania forte que dificulta o andar, ainda mais porque carrega algumas sobras de neve do alto do Fiorde, que derretem e formam uma lama chata de avançar

**G (Metinli):** _Flocos de Neve! Vai ser difícil... Vamos lá, Strinks, Blipinni, falta só um pouco!_

**V:** Terceira Rodada... Com o clima mudando, as coisas estão ficando difíceis para você Metinli!

> Vanessa consegue um terrível `0---`{: .fate_font}, para -3 nos dados, totalizando um ___Ruim (-1)___ para o Caminho, enquanto Gabriela obtem um incrível `+0++`{: .fate_font} para +3 nos dados e um ___Fantástico (+6)___ no final. Um baita Sucesso com Estilo... Mesmo usando todos os PDs que ainda tinha de cena (o que ela não pode pois não teriam Aspectos o bastante), ainda chegaria no máximo a ___Excepcional (+5)___, o que ainda daria a vitória que falta a Metinli.

**V:** Okay... Com essa última vitória, Strinks e Blippini terminam a subida, um pouco exaustas pelo final da rota com a lama, mas ainda assim finalizando com sucesso a subida.

**G (Metinli):** _Ótimo trabalho, Blipinni, Strinks. Vamos começar a ajeitar tudo e vocês vão descansar um pouco e depois a gente dá um banho no capricho em vocês. Vocês merecem._ Metinli diz, afagando Strinks.

**V:** Kara olha para Blippini e diz _Vocês realmente fizeram um bom trabalho. Bem... Pimmipo, pode cuidar de dar uma limpada nos chalés para passarmos a noite?_

**R (Pimmipo):** _Sim senhora! Enelda, talvez precise de sua ajuda para consertar algumas coisas: esses chalés são bem velhos mesmo, de antes até mesmo da Senhora Kara ou mesmo do Sanoma sair das_ poinséttias. _Perdão, Senhora Kara._

**V:** _Está bem, Pimmipo. Além disso, você tá certo: exceto pela Semana de Treinamento, quase não se usam mais esses chalés aqui nos Velhos Estábulos... Mesmo o Sanoma dizia que os elfos que treinaram ele também não lembravam muito bem dos Velhos Estábulos... Então podemos imaginar que não se usam mesmo. Por isso mesmo precisamos de alguns Kapunki e Inventineiros nessa época: não queremos tudo indo àbaixo durante os Treinos, certo?_ Ela diz, sorrindo.

**R:** Pimmipo sorri e olha ao redor, respirando o ar fresco.

**V:** Muitos Kapunki não se sentiriam à vontande, em especial com todo o cheiro de rena ao redor, mas você observa ao longe, do alto do Fiorde, a Vila do Natal e os Ranchos dos Holhooja quase como se fosssem de Brinquedo. Apesar do calor recente da primavera, no alto do Fiorde ainda é um pouquinho mais frio, e tem uns resquícios de neve formando lama, mas já pode-se ver e sentir o cheiro da grama por todo lado.

**R (Pimmipo):** _È bom mudar de ares de quando em quando... Quase me arrependo de não ter entrado para os Holhooja e me tornado um Kapunki._

**M (Pumpin):** _Porque você não quis ser Holhooja?_

**H (Enelda):** _Ele era o mais certinho entre nós... Mesmo gostando do ar livre, nunca lidou bem com as intempéries, como ficar em um chalé, isolado pela neve... Quase endoidou, sempre gostou de tudo certinho... Mas por muito pouco ele e o Metinli não trocaram de posição. Não a toa chamam um de Kapunki que poderia ser Holhooja e o outro de Holhooja que poderia ser Kapunki._

**G (Metinli):** _Para com isso, Enelda! Só porque a Fintelnin não pode vir, vai pegar no pé do Pimmipo?_

**H (Enelda):** _Não precisa ficar bravo, Metinli, só estava contando para o Pumpin porque por muito pouco vocês dois não trocaram de posição._

**R (Pimmipo):** _Mas agradeço esse fato: não sei se gostaria de estar no lugar do Metinli. Mas vir ajudar no Acampamento de Treinamento já é o suficiente... Falando nisso, melhor largar de papo: parece que tudo aqui está precisando pelo menos de uma faxina. Enelda, procure a Syllia e peça para ela te passar as vassouras novas. Vamos vassourar tudo antes de trazer os suprimentos para dentro e fazer os reparos emergenciais. Pumpin, você ajuda Metinli com as renas. Dê um banho nelas e depois pode as soltar para pastar um pouco. Mas quando o sol começar a baixar, ajude os Holhooja a colocar as renas para dentro dos Estábulos._ 

**M (Pumpin):** _Pode deixar! Vou fazer direitinho!_

**G (Metinli):** _Espero que você não tenha problema com o cheiro de rena suada... Não é algo muito comum para elfinhos em treinamento como você._

**M (Pumpin):** _Eu não sou um elfinho!_ Diz Pumpin, fazendo beiço

**R (Pimmipo):** Enquanto rio da reação de Pumppin, vou para os chalés. Eles costumam ficar bem malcheirosos nessa época, se lembro bem...

**V:** A verdade é que esse ano parece estar excepcionalmente pior o cheiro dos Chalés: dá para ver que alguns animais selvagens se abrigaram nos mesmos, pulando por janelas quebradas pelo vento e roendo ou quebrando entradas na madeira podre. Existem alguns restos de ratos, arganazes e outros animais. A única parte boa é que, diferentemente de outros anos, é que não tem ninhadas de animais aí.

**H (Enelda):** Enelda volta com as vassouras e outras coisas. _ECA! Isso me lembra o que eu mais odiei no tempo meu com os Kapunki quando era do tamanho do Pumpin!_

**R (Pimmipo):** _Cada qual com sua coisa. Agora larga a mão de preguiça e vamos varrer tudo... Deixa eu varrer onde as coisas estão mais quebradas, aí você pode consertar: parece que tem umas madeiras da parede, do chão e uns vidros a serem trocados._

**H (Enelda):** _Tudo bem... Já trouxe as ripas de madeira e as ferramentas. Além disso, a Nikäri trouxe uma coisa que ela criou, um material novo para as janelas._

**V:** Pimmipo, rola contra dificuldade ***Boa (+3)*** pelo fato de você ser um Kapunki: tem bastante sujeira, mas vocẽ é acostumado a limpar e deixar tudo organizado, enquanto Enelda vai rolar o fato de ser uma Inventineira contra uma dificuldade ***Boa (+3)*** também: você até sabe como consertar coisas, e isso seria fácil para você, se não fosse o tanto de reforma a ser feita!

**H (Enelda):** _Eu juro que não entendo porque os Holhooja gostam tanto desses Estábulos. Seria mais fácil derrubar tudo e fazer um melhor e mais moderno. Dê uma semana onde duas dúzias de Construtores e Inventineiros possam trabalhar sem parar, e fazemos algo melhor._

**R (Pimmipo):** _Acho que entendo os Holhooja: pensa bem, quantos elfos treinaram renas aqui, desde antes de formarem-se os clâs, antes mesmo do velho Pumpö das estufas... Fica aquela coisa de história, sabe... Saber que elfos que são de muito antes de sairmos das_ poinsettias _vinham treinar renas para o trenó aqui._ Vamos aos dados então.

> Rodrigo rola `0+-+`{: .fate_font} por Pimmipo, e seu +1 nos dados soma-se à Competência ___Boa (+3)___ de Pimmipo para um final ___Ótimo (+4)___
> 
> Já Enelda tem um azar... Consegue um `0-+-`{: .fate_font}, resultando em um -1 nos dados, baixando sua Competência ___Boa (+3)___ para ___Razoável (+2)___

**V:** Topa um Sucesso a Custo, Enelda? 

**H:** Qual é a ideia?

**R:** E se Pimmipo não foi tão bom na sua observação dos Chalés? Sabe... Ele é ___Ativo e Energético___, e isso de vez em quando significa que ele pode acabar metendo os pés pelas mãos, sabe?

**H:** Está certo! Mas... Que tal se Forçar meu Aspecto como ___Eu sou absurda e criativa___?

**V:** Beleza... Vai ser divertido! Vou forçar os dois e cada um ganha 1 PD. OK... Pimmipo, você está varrendo normalmente quando você escuta algum barulho vindo de uma ripa onde Enelda está trabalhando. Enelda... Justo nessa hora, você vê uma mamãe gambá voltando sua cauda contra seu rosto!

**H:** Enelda grita completamente assustada!!!! (Enelda) _AAAAAAHHHHH!!!! Gambá! Gambá!_

**R:** Pimmipo para de varrer e se volta para Enelda.

**V:** Bem a tempo de levar um spray de cheiro de gambá junto com Enelda. O cheiro é tão horrível que vocês dois saem correndo, ___Completamente Fedidos___ como uma Condição!

**R (Pimmipo):** _Caramba! Que cheiro horrível! Vamos ir para o ar fresco, Enelda! Não vamos provocar mais a Mamãe Gambá!_ Pimmipo corre para fora como se a sua vida dependesse disso, sem perceber que o cheiro já pegou nele!

**V:** Metinli, você está limpando Strinks com Pumpin quando você ouve os gritos de Enelda e Pimmipo.

**G (Metinli):** _Por todas as Renas e todas as Corujas da Neve, o que esses dois aprontaram de novo?_

**V:** Você olha e nota que os demais Holhooja dão algumas risadas, divertidas mas simpáticas, quando um cheiro realmente nauseabundo vindo da direção deles chega ao seu nariz, e ao seu Pumpin.

**G (Metinli):** _Deixa vê se entendi... Vocês dois estavam limpando os Chalés até que deram de cara com uma Mamãe Gambá, certo!_

**H (Enelda):** _Culpa do Pimmipo que não olhou direito! Estava arrumando tudo direitinho até que vi uma madeira podre no assoalho e, quando fui tirar, lá estava a mamãe gambá, já com a cauda direto na minha cara!_

**M (Pumpin):** _Acho que lembro alguma coisa sobre gambás! Vou lá ver isso!_ Me diga... Já que tecnicamente Enelda falhou no teste, posso usar minhas Façanhas, ___Ainda aprendendo___ e ___Acho que dá para fazer isso de maneira diferente___ para tentar tirar a mamãe gambá de onde Enelda estava e levar ela para um canto sossegado onde nem Elfo nem Rena possa ir?

**V:** A ideia é boa. Então... Você vai emular a ___Falar com Animas (Literalmente)___ dos Holhooja para falar com a Mamãe Gambá?

**M (Pumpin):** Isso... Nessa caso, poderia utilizar a Competência enquanto ___Elfo em Treinamento___? 

**V:** Acho que sim... Você tá se comportando como Holhooja, e no meio de um monte dos Holhooja... Me parece bem legítimo você estar mostrando um potencial para um futuro Holhooja.

**G (Metinli):** _Cuidado, Pumpin! Mamães Gambá são bem territoriais... Em especial se estiverem com ninhada, o que pode ser o caso!_ Vou usar meu ___Faro para Problemas___ antes que a coisa cheire ainda pior!

**V:** Rola sua Competência enquanto Holhooja, +2 pela sua Façanha... A dificuldade é _Razoável (+2)_.

> Gabriela rola `+-+-`{: .fate_font} para +0 nos dados... Porém o fato de ser um Holhooja, mais o +2 do seu ___Faro para Encrenca___, resulta em um ___Excepcional (+5)___ muito bom para um _Sucesso com Estilo_. Vanessa olha com aquele olhar pedindo para Gabreila descrever o que está acontecendo.

**G:** Metinli se aproxima com cuidado dos Chalés por onde Enelda e Pimmipo sairam correndo e observa que realmente ela está com filotes, e uma ninhada bem grande! _Pumpin, esa realmente está com ninhada, das grandes! Acho que pelo menos uns 6 filhotes!_

> Gabriela coloca um Aspecto ___Gambá com Filhotes___ com duas Invocações Gratuítas

**M:** Eu vou me aproximar com toda tranquilidade... E vou usar uma das Invocações da ___Gambá com Filhotes___ que Metinli obteve para fazer com que ela não veja nada errado em mim. Eu me aproximo com todo o cuidado e digo, imaginando que ela vai me ouvir como ouviria um Holhooja (Pumpin) _Dona Gambá... Perdoa meus amigos, mas está em uma semana importante para todos, eles vão treinar as renas do Pai Natal. Se a gente te colocar em um lugar onde nem Rena nem Elfo vão incomodar você, você sairia? A gente ajudaria seus filhotes. Palavra de Escoteiro!_

**V:** A mamãe Gambá olha para você e diz (Gambá) _Desculpa... Eu não quis atacar eles, mas o susto foi tão grande que eu tive que defender-me e aos meus filhotes. Só me ajudem a levar meus filhotes para um lugar tranquilo e prometemos não atrapalhar._

**G:** Vou usar a segunda Invocação para que o local perfeito esteja lá... 

**V:** As beiradas do Fiorde formam escarpas muito grandes e perigosas: mais de um Holhooja desastrado se machucou feio caindo, mas entre as cercas que impedem de algum Holhooja cometer loucuras e os muros dos Estábulos e dos Chalés há um espaço onde existem algumas caixas que lembram uns baús rudimentares... Deve ser o suficiente para que a ninhada fique a vontade e que a mamãe gambá possa ir e voltar quando necessário.

**G:** Metinli indica o local para os demais.

**R:** Já que Pimmipo e Enelda estão bem fedidos, acho que é um bom momento para ambos ajudarem a carregar os filhotes de gambá, certo?

**V:** Pumpin, você e os demais ajudam a mamãe gambá a ir até um desses caixotes. Ela até mesmo suspira aliviada. (Gambá) _Aqui parece ainda mais gostoso e quentinho... Depois vou sair para procurar comida. Obrigada!_ vocẽs dois, Metinli e Pumpin ouvem ela falar.

**R (Pimmipo):** _Acho que precisamos terminar de limpar os chalés, depois tomar um belo banho para tirar esse cheiro e preparar a janta. Se lembro bem, a Mamãe Noel fez uma bela sopa de queijo e tem um pão redondo que o Gordot preparou que deve estar ótimo._

**G (Metinli):** _Verdade... Puxa vida, o cheiro em vocês está horrível. E deram sorte de não levarem a borrifada no olho. Dizem que dói demais. Curioso essa mamãe gambá ter montado sua ninhada aqui... A subida é longa e perigosa para gambás, corujas as achariam fácil e virariam jantar. Talvez na volta tenhamos que os levar conosco de volta ao Vale._

**H (Enelda):** _Acho que agora sem nenhuma mamãe Gambá podemos terminar o reparo dos Chalés e a limpeza._

**V:** De fato, vocês conseguem terminar a limpeza dos Chalés e os reparos mais emergenciais, além de obviamente remover o cheiro de gambá que empesteou o chalé de modo a ficar mais habitável. Pimmipo, Enelda, acho que vocês vão tomar seus banhos, certo?

**H:** Claro: Enelda está até meio frustrada já por causa disso e que ficar com o mínimo de "lembrança" da mamãe gambá

**V:** Bem... Rolem os seus clãs como uma forma de vermos se nada acontece ao limpar suas Condições.

> Rodrigo e Heitor rolam por Pimmipo e Enelda, respectivamente  `+-+0`{: .fate_font} e `-0--`{: .fate_font}... Isso indica que a limpeza de Condição de Pimmipo é sem problemas, mas não a de Enelda.

**V:** Heitor... Você quer deixar os dados como estão? Quero dizer... Pode ser que tenha alguma complicação por uns tempos, mas a sua Condição ___Completamente Fedida___ vai sumir.

**H:** Acho que posso ficar com alguma complicação... O que pode ser? Imagino que a parte da serpentina para esquentar a água estaria ok... Ela se machucaria com a água excessivamente quente? Ou talvez Enelda poderia gripar?

**V:** Gostei... Pimmipo, depois de tomar seu banho e por roupas limpas, você vai preparar a janta para todos, esquentando a sopa que a Mamãe Noel mandou, junto com o pão redondo de Gordot. Nesse interim, você escuta uns espirros. Enelda, você perdeu a noção do tempo no banho quente, mas esqueceu de contar com a friaca que forma durante a noite e acabou deixando a janela aberta o suficiente para pegar um ___Resfriado leve___: você sai do banho e começa a espirrar!

**H (Enelda, como se estivesse refriada):** _Atchim! Atchim! Bucha fida! Ãinda mais echa! Gripar logo agora!_

**G:** Metinli nota a situação de Enelda e vai verificar se ela está bem.

**V:** Metinli, você nota que Enelda está apenas levemente refriada pelo choque térmico ao sair do banho.

**M:** Será que Pumpin pode dar umas dicas para Pimmipo fazer uma sopa reforçada para Enelda?

**V:** OK... Fica a seu critério.

**R (Pimmipo):** _Pumpin, pode me ajudar a reforçar a sopa da Enelda? Tem pimenta junto com os ingredientes que a Mamãe Noel mandou, além de um pouquinho de prápica e mandioquinha... Eu te oriento e você faz uma sopa mais reforçada para a Enelda, que tal?_

**M:** Pumpin olha assustado para Pimmipo (Pumpin) _"Como assim? Nunca me deixaram coxinhar quando estava com os Kapunki!"_

**R (Pimmipo):** _Tudo tem uma primeira vez. Agora, deixa de onda e vem cá: não posso cozinhar tudo sozinho._

**M:** Pumpin se aproxima com todo cuidado... Será que posso usar mesmo a Façanha dos _Kapunki_ de _Identificar Padrões_ para ajudar?

**V:** Sim... Mas lembra que, devido ao fato de estar _Ainda Aprendendo_, você soma apenas +1, não +2 como um Kapunki pleno como Pimmipo.

**M:** Está certo! Mas posso usar como Competência o fato de ser um _Elfo em Treinamento_, certo?

**V:** Não vejo porque não. Além disso... Rodrigo, você acha que Pimmipo pode dar uma ajuda a ele?

**R:** Como ele está agindo como Kapunki agora, acho que casa para Trabalho em Equipe, certo?

**V:** A ideia é exatamente essa, pois as competências de vocês dois enquanto Elfos, ao menos no momento, permitem isso

> Em _Dry Fate_, existe a regra de _Trabalho em Equipe_: quando dois ou mais personagens possuem competências iguais ou similares, o Narrador pode sugerir ou autorizar um deles a oferece um bônus de +1 em um determinado rolamento. No caso, Pumpin é um _Elfo em Treinamento_, mas como ele está agindo como um Kapunki, a competência de Pimmipo enquanto _Elfo do Clã Kapunki_ permite a ele oferecer +1 em _Trablho em Equipe_ para  Pumpin

**R:** Então, Pimmipo decide orientar Pumpin, da mesma forma que Gordot o orientou no passado

**V:** Então, Maria, pode rolar...

> A Competência efetiva de Pumpin para esse rolamento é ___Excepcional (+5)___: seu ___Bom (+3)___ como _Elfo Em Treinamento_, +1 de sua Façanha _Ainda Aprendendo_ e +1 do Trabalho em Equipe de _Pimmipo_. O rolamento é feito com `+--+`{: .fate_font}, para +0 nos dados, o que não prejudica o _Sucesso com Estilo_ que eles conseguem!

**V:** Você nota, Pumpin, que Pimmipo lhe ajuda a fazer uma sopa REALMENTE cheirosa para Enelda, e acaba complementando para todos! Quando os demais começam a entrar, o tempo ficando escuro e frio lá fora, Nörra e suas irmãs se aproximam e cheiram a sopa (Nörra) _"Puxa, essa sopa parece realmente muito boa, e parece bem quente. Isso vai ajudar com o frio de hoje. Veio bem a calhar, Pimmipo."_


**R (Pimmipo):** _Obrigado, mas o Pumpin aqui me ajudou bem. Além disso, não fosse o resfriado que Enelda pegou, com certeza seria algo mais simples... Acho que todos estamos cansados, queremos comer e ir dormir. Só fiz mais caprichado por causa do resfriado da Enelda._ 

**V (Nörra):** _Então, temos que agradecer a Enelda e seu resfriado!_ Ela diz, rindo e suas irmãs com ela. Em especial Nöli, como Inventineira, parece mais à vontade de rir de Enelda, tentando animar a companheira de clã.

**H (Enelda):** _"Bara, Noli... Isso naum deim graça!"_ Eu digo para ela, mas também rindo. Enelda acha divertido as trigêmeas rindo. 

**V:** Kara então diz sorrindo _Vamos todos sentar enquanto comemos e planejamos amanhã. Nöli, acho que você e Enelda podem dar uma reparada melhor nos Estábulos depois que tirarmos a rena para a primeira marcha mais pesada. Pimmipo, acho que Sylvie, Pumpin e Kypip podem lhe ajudar a organizar as coisas. Vai ser um dia puxado._

**R (Pimmipo):** _Não se preocupa não que a gente dá um jeito. Amanhâ vai ser tudo mais limpo e gostoso._

**V:** E podemos acabar essa cena com todos comendo e, depois de aquecidos pela sopa sua, Pimmipo, vão dormir em  sacos de dormir, já que Enelda não teve tempo de consertar os beliches nos quartos... O dia amanhece bonito, com o orvalho da madrugada aparecendo nas janelas. Pimmipo, você é o primeiro a acordar, mas logo em seguida, enquanto você prepara o café, Kara, Nörra e Metinli acordam... _Prontos para um dia cheio?_ pergunta Kara.

> O que mais acontecerá de estranho e maravilhoso com os Elfos nos Velhos Estábulos? Algum animal perigoso irá tentar alguma coisa novamente? Ou encontrarão algum segredo antigo que nenhum elfo encontrou antes? Isso ficará a critério do grupo de Metinli, Pimmipo, Enelda e Pumpin
