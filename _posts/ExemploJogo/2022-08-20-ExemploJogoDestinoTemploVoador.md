---
title: "Exemplo de jogo em Do: Destino do Templo Voador"
#teaser: "Quando a turma precisa realizar um Teste de Coragem, os jogadores vão explorando o cenário e as regras!"
layout: post
date: 2022-08-20
categories:
    - RPG
header: no
excerpt_separator: <!-- excerpt -->
tags:
  - Fate
  - FAE
  - Aventura
  - Exemplo de Jogo
  - Do
  - Destino do Templo Voador
  - ExemploDeJogo
---


> Roberta está narrando a algum tempo um cenário de [_Do: Fate of the Flying Temple_](https://evilhat.com/product/do-fate-of-the-flying-temple/) com seus amigos:
> + Yolanda, com o [Peregrino Pergaminho Eterno](http://fabiocosta0305.gitlab.io/personagens/PergaminhoEterno/)
> + Vivian, com a [Peregrina Vento Silencioso](http://fabiocosta0305.gitlab.io/personagens/VentoSilencioso/)
> + Douglas, com o [Peregrino Generoso Urso](http://fabiocosta0305.gitlab.io/personagens/GenerosoUrso/)
> 
> Eles acabaram recentemente de resolver sua primeira carta, sobre um mistério envolvendo dois planetas que apareceram em rota de colisão um com o outro, depois do ___Desaparecimento do Templo Voador___ (um Aspecto em jogo, uma frase que descreve algo tão importante para o cenário que pode ser usado tanto para descrever situações quanto para certas vantagens - e desvantagens - mecânicas e narrativas). Com isso, notaram que o ___Dragão que surgiu do Ovo do Templo___ e que desde então os acompanha, cresceu um pouco e desenvolveu alguma inteligência, ainda que ele se comporte como um bebê.
> 
> A aventura começa com eles ainda procurando pistas sobre o desaparecimento do Templo, fonte de estabilidade e ordem dos Muitos Mundos...

<!-- excerpt -->

---

**Roberta:** Bem, vocês acabaram de sair de Jukku, agora que podem seguir adiante após resolver a questão sobre a colisão de Jukku com seu planeta vizinho mais próximo, Ishita. O que vocês fazem agora?

**Yolanda:** Pergaminho Eterno está anotando algumas informações sobre o que aconteceu, até como uma forma de registrar possíveis pistas sobre o ___Desaparecimento do Templo Voador___. (Yolanda então fala com uma voz meio empolada, representando _Pergaminho Eterno_, um Peregrino bastante inteligente e diplomático, mas ocasionalmente muito esnobe) _"Ora, bastou então que conseguíssemos usar os antigos Pilares de Água para resgatar de Ishita uma parcela de sua água, evitando assim que a influência de Rova o empurrasse ainda mais contra Jukku. Nada tão complicado, diga-se de passagem."_

**Douglas (como Generoso Urso, um Peregrino de excessos, de uma risada tão generosa quando a ira contra o mal, em um tom meio bravo e bastante enérgico):** _"Fácil para você falar, Pergaminho Eterno, mas quem teve que mover os Pilares para resgatar água o suficiente fui eu! Não vi você ajudar em nada!"_

**Y (Pergaminho):** _"Ora bolas! Sem os cálculos precisos que efetuei, mover os Pilares teria sido tão ou até mais prejudicial para Jukku do que deixar que Rova arremessasse Ishita contra o mesmo."_

**Roberta:** Ao notar que vocês estão brigando enquanto sentado em cima dele, o Dragão decide dar uma Rosnada Assustadora para mostrar que ele ***Deseja espalhar paz*** (Roberta diz, usando um dos Aspectos do Dragão que foram escolhidos pelos jogadores, no caso por Vivian). Generoso Urso, Pergaminho Eterno, mesmo com tudo que já aprenderam sobre o Dragão, você teme que o Dragão decida os devorar por algum motivo. Ao mesmo tempo, Vento Silencioso, você nota que o Dragão está ficando impaciente com Pergaminho Eterno e Generoso Urso, que tem se bicado com certa frequência, desde que vocês descobriram sobre o ___Desaparecimento do Templo Voador___. O que vão fazer? (Ela diz, apresentando três tampinhas de garrafa que ela usa como Pontos de Destino, indicando a Forçada do Aspecto do Dragão contra os Jogadores. Eles tem que aceitar como um time ou negar como um time).

**Vivian:** Olha, podemos aceitar essa Forçada... Isso sempre pode ajudar a gente na próxima carta que teremos que responder. (Ela diz, e os demais jogadores acenam com a cabeça) Vento Silencioso irá se aproximar da cabeça do Dragão e a acariciar para o acalmar, mesmo sabendo que apenas um dente do Dragão é duas vezes o tamanho de Generoso Urso em todas as dimensões.

**Roberta:** O Dragão fica um pouquinho mais sossegado, mas ainda dá umas fungadas bravas ao lembrar da discussão entre Pergaminho Eterno e Generoso Urso.

**V (como Vento Silencioso, uma Peregrina com a estranha habilidade de ser quase imperceptiva, quase como se não existisse, em um tom de voz quase inaudível de tão baixo):** _"Vocês deveriam evitar provocar o Dragão. Já é difícil demais o que está acontecendo, com toda a instabilidade dos Muitos Mundos, sem vocês dois brigando por picuinhas."_ Ela diz em um tom meio irritado, quase como dando uma bronca nos dois. O que seria engraçado, se não fosse o fato dela estar dizendo verdades duras para os dois.

**Y (Pergaminho, em um tom arrependido):** _"Você tem Razão, Vento... Eu passei dos limites. Desculpe, Urso."_ 

**D (Urso):** _"Eu também me excedi, Pergaminho... Estamos todos cansados e ainda desorientados pelo sumiço do Templo... O que vocês acham que aconteceu? Vimos apenas poeira e aquele buraco que sugou tudo, e por pouco não sugou nem o ovo do dragão e nem a nós, quando ele chocou e nos tirou de lá."_

**V (Vento):** _"Seja o que for, abalou demais os Muitos Mundos... Viram quantos mundos vimos colidindo uns nos outros, e as rotas dos Gansos Celestiais mudando, e as Baleias Aéreas buscando fugir dos caçadores..."_

**Y (Pergaminho):** _"Eu não consigo imaginar o que aconteceu: não havia nos registros do Templo nada sobre ele ter surgido ou mesmo de ele ter algum dia desaparecido. Até onde pude ler e aprender com outros Monges, como Tábula Argêntea e outros, ninguém sabe sobre como o Templo surgiu: pelo que se sabe, ele sempre existiu."_

> **Roberta até queria aproveitar um pouco mais o Roleplay, mas decide introduzir a carta da vez para os jogadores**

**R:** Enquanto vocês conversam, vocês notam o Dragão se balançando, fazendo vocês voarem para fora dele. Lembrem-se que o Dragão é enorme, maior que muitos dos Muitos Mundos onde vocês resolvem, ou arrumam, confusões.

**V (Vento):** _"O que houve, baixinho? Algo incomodando?"_

**R:** Você nota, Vento Silencioso, que ele está coçando uma das patas com a outra, como se tivesse algo encravado

**D:** Eu vou ver o que está acontecendo

**R:** Generoso Urso, conforme você observa a região que o Dragão está coçando sua pata, você nota algo encravado nela. Parece um pequeno bastão ou tudo de bronze e couro.

**D:** Como meu _Avatar_ é ___"Um corpo enorme para um coração tão grande quanto"___ acho que consigo retirar essa coisa do Dragão, certo?

> O _Avatar_ em _Do: Fate of the Flying Temple_ representa o _Conceito_ do personagem, sua mais importante definição. No caso, ele está pensando em arrancar na força bruta o tubo... Pode ser um momento interessante para um rolamento, já que todo tipo de coisa doida e divertida pode acontecer se Generoso Urso for descuidado.

**H:** Bem... Me parece ok... Só me diz, como Generoso Urso tentará arrancar isso? Você pode notar que parece meio fundo: dependendo de como você retirar pode machucar esse Dragão Bebê de alguns quilômetros de cumprimento, e isso não me parece uma boa ideia.

**D:** **GULP!** É verdade, sempre esqueço como o Dragão é gigantesco! Acho que seria melhor se Generoso Urso fosse BEM CUIDADOSAMENTE retirando o tubo, como se ele fosse um tipo de espinho no Dragão.

**R:** Me parece OK. Trata-se então de Superar um Obstáculo, sendo Cuidadoso, com uma dificuldade **Razoável (+2)** bem padrão. Nada excepcionalmente difícil, mas sempre pode dar ruim.

> Em _Do: Fate of the Flying Temple_, usam-se as mesmas Abordagens padrão do _Fate Acelerado:_ _Ágil, Cuidadoso, Esperto, Estiloso, Poderoso_ e _Sorrateiro_. Além disso, as ações são as mesmas do Fate: _Superar_, _Criar Vantagem_, _Atacar_ e _Defender_

**D (pegando os dados):** Vamos lá, dadinhos, não falhem comigo agora.

> Douglas pega os dados e os rola. E logo de cara ela consegue um `+-0-`{: .fate_font}. Apesar de Generoso Urso ter um _Razoável (+2)_ em _Cuidadoso_, seu resultado foi apenas _Regular (+1)_, já que os dados resultaram em -1, apenas um dos `-`{: .fate_font} sendo cancelado pelo `+`{: .fate_font}. Como ele ficou abaixo da dificuldade, foi uma Falha

**D (pensando rapidamente e pegando um de seus PDs):** Isso normalmente seria uma Falha, mas como ***Sempre tenho algo útil***, eu embrulho o tubo em uma fita que ajuda a minha mão a ficar firme na pegada para tirar o tubo com a força necessária, E APENAS ela, não machucando o dragão... Isso garante +2?

**R:** Parece-me válido. Tudo bem! Enquanto você amarra a fita no tubo e o puxa, você nota o incômodo do Dragão, que se torna alívio quando você extrai o tubo. Agora você nota totalmente o que esse tubo é: ele lembra um daqueles cartuchos de diploma, sabe? E parece com um se você desconsiderar as aramelas e todos as firulas em bronze neles. Existe um símbolo em um dos lados do tubo.

**D:** Vou levar o tubo para os demais Peregrinos. Talvez algum deles conheça algo sobre esse tubo. (Como Generoso Urso) _"Ei... Isso aqui estava encravado no Dragão... Seria uma carta?"_

**Y (Pergaminho):** _"Possivelmente... Cartas para o Templo assumem todo tipo de forma, desde couro de animais até estranhos cristais. Deixe-me ver isso."_ (em sua voz normal) Pergaminho Eterno ajeita o _pince-nez_ sobre o olho direito e procura descobrir como abrir o tubo. Todos sabem que a visão de Pergaminho Eterno é perfeita, ele apenas faz isso para parecer mais esperto do que é.

**R:** Você nota, Pergaminho Eterno, que esse tubo é realmente algo vindo de um mundo com tecnologia mais sofisticada: ele é usado em certos equipamentos pneumáticos para transmitir pequenos objetos e notas por meio de um sistema de tubos. Esse em especial parece ter algum tipo de charada ou dispositivo de segurança. Você precisará ter um sucesso em _Superar_ com dificuldade _Boa (+3)_ para desfazer os esse dispositivo sem danificar o que quer que esteja lá dentro.

**Y:** Vamos lá então... Sem abrir isso não saberemos se é uma carta e seu conteúdo. Pergaminho Eterno vai usar todo o seu conhecimento em charadas para abrir isso, pela _Abordagem Esperta_

> Yolanda rola os dados é é muito fortuita: seu `+0++`{: .fate_font} resulta em um Sucesso com Estilo quando somado à sua Abordagem _Esperta Boa (+3)_ já que resultou em um resultado _Fantástico (+6)_, 3 pontos (ou Tensões) melhor que a dificuldade

**R:** Pergaminho, conforme você observa, você nota que isso seria um problema para uma pessoa comum, mas não para alguém tão esperto quanto você: você localiza rapidamente o mecanismo que destrava a tranca do tubo e depois aciona o botão que o abre, revelando a carta dentro do compartimento, em um pedaço de papel sépia, uma letra bastante elegante nela. Você reconhece o mecanismo: é de um Mundo chamado _Albion_, próximo os Céus de Aço e do Crepúsculo.

> Os Céus são grandes grupos de Mundos que possuem características similares. O Céu do Aço é um Céu de grande indústria e inventividade, mas de desejo por recursos a todo custo. O Céu do Crepúsculo é longe o bastante para quase não receber luz e calor do Templo Voador quando o mesmo existia. Rebeca imprimiu a carta, _Spun of Metal and Gold_ e a entrega para Yolanda.

**Y (lendo a carta, como Pergaminho Eterno):** _Eu descobri de onde veio. É um mundo chamado Albion, onde moram pessoas muito inventivas, mas que também é meio sujo e fétido devido aos abusos contra a Natureza. Parece que um certo Amber Carmelian está pedindo ajuda: segundo o que ele diz, seu avô é um grande inventor de maquinários baseados em engrenagens e relojoaria, sendo Amber tomado como aprendiz  após a morte dos seus pais. Entretanto, um certo Tio dele, homem poderoso e vingativo, parece estar ganhando acesso às criações de seu avô agora que o mesmo ficou adoecido. Esse enviou alguns doutores para cuidar do avô, mas o mesmo piorou ao invés de melhorar, e os servos da casa foram intimidados por este tio, ameaçados com processos legais. E cá entre nós, a justiça de Albion é tudo, menos justa._

**D: (Urso)** _"Continue, Pergaminho! Isso me soa como um problema sério. Deixe os comentários para depois de ler tudo!"_

**Y: (Pergaminho)** _"Ele suspeita que tal tio deseje acesso às tecnologias desenvolvidas pelo seu avô em autômatos. Seu avô conseguiu obter uma grande complexidade e refino nessa habilidade, mas que mostrou-se também perigosa por alguma razão, ao exercer trabalhos de certa... natureza delicada... Para Albion, segundo Amber. Ele chegou a essa conclusão ao analisar certos projetos. Agora, ele acredita que seu Tio, chamado aqui de Sir Victor, irá logo conseguir declarar seu avô como incapaz, assumindo todas as propriedades do mesmo, e que verá em Amber um perigo para suas ambições. Ele pede que ajudemos a curar seu avô e, se isso não for possível, levar embora os autômatos construídos pelo seu avô para funções bélicas para o Templo, onde preservaria o trabalho de seu avô de ser o usado para o mal."_ (ela interrompe, baixando a carta, mas ainda falando como Pergaminho Eterno) _Sinceramente, meus caros, me parece um pedido mais do que justo. É nobre o que Amber deseja fazer, mantendo a honra de seu avô protegida de ambições nefastas._

**V (Vento):** *De fato, você está certo, Pergaminho... Mas quão longe é Albion? Será que chegaremos a tempo?*

**Y (Pergaminho):** *"Albion é no Céu de Aço e próximo ao Céu do Crepúsculo, já estive lá. É bem distante de onde estamos agora. Se não quisermos que o pior aconteça  a Amber, temos que partir agora, com a máxima velocidade que podemos alcançar. Cada segundo conta."*

**D (Urso):** *"Então o que estamos esperando? Vamos logo! Estou louco para colocar um pouco de juízo nesse Sir qualquer coisa!"* 

> Roberta pensa se compensa colocar algum perigo na viagem, mas acredita que será mais divertido colocar tudo em Albion, um mundo que é conceitualmente _Steampunk_ e portanto estranho para os Peregrinos, tão jovens, inexperientes, e herdeiros de uma estranha criatura em Muitos Mundos sem um Templo Voador

**R:** Tudo bem... Vocês viajam o mais rápido que conseguem, e em dois dias vocês se aproximam de Albion. Vocês notam que ele realmente é no meio do Céu de Aço: a fumaça fazem vocês tossir conforme se aproximam de Albion, que parece um dos maiores Mundos desse Céu: mesmo o Dragão é pequeno comparado a ele.

**V (Vento):** _"Eca! Esse lugar não parece nada bom... Pergaminho, alguma precaução a ser tomada aqui?"_

**Y:** Como o _Avatar_ de _Pergaminho Eterno_ é __"Contador de Contos, Escritor de Registros, Mestre de Conhecimento"__ e seu _Estandarte_ é __Tanto Conhecimento, Tão Pouco tempo__, cabe eu imaginar que Pergaminho Eterno tenha algum conhecimento sobre Albion, mesmo que pouco?

> Rebeca pensa que poderia ser interessante deixar que os Peregrinos quebrassem a cara um pouquinho para variar, mas decide que é ainda mais interessante se a bomba explodir na cara deles, quando o conhecimento que Pergaminho Eterno possui explodir na cara deles no pior momento, por ser inadequado no momento.

**R:** Está bem... Pergaminho Eterno já esteve a algum tempo em Albion, mas acho que ele ainda lembre uma coisa ou duas sobre o mesmo. 

**Y (Pergaminho):** _"Bem... O que me lembro de mais importante é que eles são extremamente irascíveis quando são tratados de uma maneira que entendam como rude. Além disso, são muito arrogantes. Devemos tomar muito cuidado com eles: uma palavra errada e podemos ter nosso fim."_

**D (Urso):** _"Fala sério, Pergaminho... Não acredito em pessoas que possam ser tão tolas ao ponto de atacar um Peregrino do Templo Voador."_

**Y (Pergaminho):** _"Acredite em mim, Urso... Eles são desse jeito. Além disso, agora que viram o Dragão, devem estar suspeitando de algo. Vamos ter que agir com prudência e discrição. Melhor irmos."_

**V (Vento):** _Fique aqui, baixinho. Sabe que se precisarmos de sua ajuda, vamos pedir, mas é melhor para nós que desçamos de maneira discreta como Pergaminho Eterno disso._

**H:** O Dragão dá uma fungadinha triste, mas entende. Vocês descem para Albion, e vocês notam que, por baixo das nuvens de fuligem que tornam suas roupas imundas, há o que parece uma cidade vitoriana: lampiões a gás iluminam as ruas de tijolos, carruagens podem ser vistas andando pelas mesmas. Algumas dessas carruagens não possuem nenhum animal ou pessoa o puxando, sendo puxados por algum tipo de motor que também solta muita fuligem. Pode-se ver as luzes de bares e café, onde pessoas cantam, comem, bebem e se divertem com alguns jogos. A visão que vocês tem é de um lugar tão sofisticado quanto decadente e sujo. Vocês aterrissam em Albion no meio de uma praça, em um pequeno círculo formado por árvores

**Y (Pergaminho, fazendo como que batendo suas roupas):** _"Lembrei por que achei muito ruim ter vindo para cá. O Monge Poeira Livre que me disse para vir e tentar entender porque é importante não se apegar às aparências. Mas... Puxa vida, é muita fuligem e sujeira."_

**V (Vento):** _"Acalme-se, Pergaminho. Sei que aprecia estar limpo sempre, mas mais importante é cumprirmos nossa missão."_

**H:** Exatamente nessa hora, vocês veem alguns homens, em roupas de soldado, armados com sabres de cavalaria e pistolas, se aproximando de vocês com cara de poucos amigos. (com uma voz arrogante e imponente) _"Não sabeis que o Parque Victoria é área proibida a vadios e mendigos? Voltais para as pocilgas próxima ao Rio Marcuse, de onde sequer deveriam ter saído ."_

**Y (Pergaminho):** _"Não somos mendigos, meu caro soldado. Pode ver por nossas roupas que somos Peregrinos do Templo Voador. Precisamos encontrar um certo Amber Carmelian. Seria possível nos dar uma indicação de onde podemos o encontrar?"_

> Helena dá um sorriso: ela imaginava que Pergaminho Eterno tentaria isso, e já colocou uma encrenca para os Peregrinos

**H:** Um dos soldados ri com gosto: _"HAHAHAHAH! Afirmam ser Peregrinos do Templo Voador? E ainda mais procurando aquele pivete que Lord Greymist tanto tenta educar? Essa é uma patetice gigantesca! Agora, vão antes que tomemos uma providência mais séria! Aliás... Acho que uma Noite na Prisão fará bem para pivetes metidos que nem vocês."_

**Y:** Eles vão atacar assim de cara?

**H:** Isso mesmo. Vocês não tem como saber isso de cara, mas esses ___Soldados Corruptos___ devem estar trabalhando para o Tio de Amber, que suspeita de alguma coisa. 

> Helena apresenta um cartão com a ficha desses Soldados
> 
> **Soldados Corruptos:**
> + **Soldados de carreira; Estão nessa pelo dinheiro; Nome da família mas não sua honra**
> + **Peritos em (+2):** _Lutar em grupo, extorquir os mais pobres, bajulação_
> + **Ruim em (-2):** _Agir em desvantagem_
> + **Estresse:** `4`{: .fate_font}
> 
> Ao mesmo tempo, ela passa um lápis para Vivian, como indicador de quem tá agindo, já que ela adotará a regra de _Iniciativa Cinemática_ do Fate Condensado.

**H:** Eles são um grupo grande, mas a sorte de vocês é que quem inicia o _Conflito_ é Vento Silencioso, por ter o melhor **Ágil** entre todos. Depois ficará a critério dela decidir quem aje. Lembrem-se: como diria a Monja _Folhas Cadentes: "A Violência não é a solução para nenhum problema digno de ser resolvido"_

**V:** Vamos lá então... Acho que o Fato de Vento Silencioso ser ___Uma presença silenciosa no mundo___ me parece OK para tentar uma coisa com eles: vou tentar imobilizar um deles assim que eles tentarem nos atacar, mostrando aos mesmo que a tentativa deles de fazer algo de ruim a nós só trará dor de cabeça para eles. Para isso, vou usar a _Defesa Total_. E passo para Generoso Urso tentar colocar um pouco de juízo na cabeça deles.

> Helena acha interessante a ideia e permite. Vivian passa o lápis para Douglas agir.

**H:** Então você vai somar +2 a todas as suas Defesas, mas não faz nenhum tipo de outra ação nesse turno, certo? Tudo bem... Generoso Urso, o que você vai fazer?

**D:** Claro que vou tentar deitar um ou mais desses caras no chão. Claro que não vou machucá-los para valer, mas acho que as técnicas do Templo Voador podem ser usadas para imobilizar ou desacordar, certo?

**H:** Claro, claro. Então pode rolar seu _Poderoso_ e eu rolo a Defesa dos Soldados. Devido aos seus números, eles possuem +2 no teste.

**D:** Vamos lá então! Eu tenho minha Façanha de _Perito em Artes Marciais_ que me dá +2 no Ataque!

> Roberta e Douglas vão para os dados. Roberta obtêm `+-0+`{: .fate_font} para +1, e Douglas também obtêm `-0++`{: .fate_font}, resultando em +1. Agora vejamos:
> + **Soldados:** são _Peritos em Lutar em Grupo (+2)_, têm um bônus de +2 por tamanho (8 Soldados), e +1 no dado, com um total ***Excepcional (+5)*** 
> + **Generoso Urso:** Possui um _Poderoso Bom (+3)_, +2 pela Façanha _Perito em Artes marciais_, já que está _Atacando_ de _Maneira Poderosa_ com as mãos limpas, e consegui um +1 no rolamento. Isso gera um ***Fantástico (+6)***
> 
> Roberta pensa se compensa deixar que _Poderoso Urso_ acerte seu Ataque, ou gastar Pontos de Destino para melhorar a Defesa dos Soldados. Ela tem três Pontos de Destino nessa cena, por cada um dos Peregrinos, e decide não facilitar.

**H:** Bem, Generoso Urso... Normalmente você acertaria esse ataque... Se não fosse o fato de eles serem ***Soldados corruptos***, mas disciplinados o bastante para saberem agir em grupo, evadindo de seus ataques precisos, mas solitários! (Ela termina, mostrando uma das tampinhas de garrafa dela indicando o gasto de um dos Pontos de Destino que ela tem para a cena)

**D:** Tá de sacanagem que eles escapam! Eu vou gastar um Ponto de Destino...

**Y:** Deixa por agora, Douglas, sinto que podemos dar a volta por cima se entendi o plano da Vivian!

**D:** Tá, então vai lá e faz algo! (diz Douglas, passando o Lápis de Iniciativa para Yolanda. Ele não poderia devolver para Vivian, que já agiu nesse turno, e passar para Roberta seria uma encrenca)

**Y:** Bem... Pergaminho Eterno vai apelar para o que ele sabe sobre Albion. (Ela grita na voz de Pergaminho Eterno) _"Socorro! Socorro! Estamos sendo atacados sem motivo pelos Soldados da Coroa!"_ Se sei bem, os Soldados da Coroa tem uma atuação mais limitada do que a Polícia, não podem pura e simplesmente atacar civis. Ao menos é o que diz nos meus Pergaminhos.

**R:** Você quer fazer eles correr como uma ação de _Superar_? Não me parece algo muito simples, seria uma dificuldade alta, mas permitiria usar seus _Pergaminhos_ como uma Façanha

**Y:** Não... Citar os Pergaminhos foi só floreio. Sei que não posso usar a Façanha para _Criar Vantagem_, mas me parece muito mais simples do que recorrer à mesma, além do fato de ser uma dificuldade Passiva, certo?

> Roberta dá um sorrisinho meio enviesado, pois sabe que Yolanda conhece bem as regras e tá usando direitinho

**R:** Então você pretende fazer alguém aparecer para os ajudar?

**Y:** Sim, a ideia é exatamente essa

**R:** Dificuldade _Razoável (+2)_ e como você está demonstrando algum conhecimento sobre Albion, acho que pode ser por _Esperto_, embora normalmente fosse por _Estiloso_

**Y:** Maravilha... Minha melhor Abordagem! Vamos nessa!

> Yolanda rola os dados e consegue um `-0+0`{: .fate_font} para +0. Isso mantêm o resultado no _Bom (+3)_ de Pergaminho Eterno, que já é o suficiente para passar a dificuldade para o Teste. Mas Yolanda considera que isso não é o suficiente!

**Y:** Como Pergaminho Eterno é um ***Contador de Contos, Escritor de Registros, Mestre de Conhecimento***, quero somar +2 para obter um _Sucesso com Estilo_ ao criar esse Aspecto. Cito ainda que _"Servos da Rainha Georgina Augusta Victoria estão atacando visitantes indefesos!"_ Quero que esses Soldados fiquem bastante **Envergonhados**!

> Rebeca avalia se pretende aumentar a dificuldade passiva por meio dos soldados, invocando esse Aspecto para aumentar em +2 a dificuldade, mas acha que não é uma boa ideia por agora. Então ela simplesmente dá de ombros e aceita o resultado, pegando um _post-it_ e marcando o Aspecto **Envergonhados** e desenhando duas bolinhas nele

**R:** OK... Vocês então tem duas Invocações Gratuitas para o fato de eles estarem ***Envergonhados***. Essa é a boa notícia para vocês. A má notícia é que agora eu Ajo pelos Soldados, não? Lembrando que posso inclusive agir duas vezes por eles

> Como não tem mais nenhum PC, a iniciativa passa para o Narrador como o último a agir naquele turno. Na iniciativa cinemática, o último a agir em um turno tem o direito de decidir quem é o primeiro a agir no turno seguinte, incluindo ele próprio.

**R:** E, para começar... Vou dividir o grupo de Soldados em dois grupos com quatro Soldados cada... Isso vai reduzir o bônus de tamanho dos mesmos, mas vai me permitir Atacar tanto Pergaminho Eterno quanto Vento Silencioso. (Ela então diz como um dos Soldados) _"Seus Pulhas! Hora de mostrar o que A Nobre Força Armada de Albion é capaz!"_ Eles avançam, sacando seus sabres, obviamente com a intenção de machucar vocês.

**Y:** Pergaminho Eterno apenas se posiciona de maneira bastante suave, mas muito treinada no Templo Voador, pronto para desarmar e imobilizar o primeiro soldado que puder

**V:** Bom, Vento Silencioso já está em uma postura de Defesa Total, então não tenho muito o que descreve.

**D:** Hora do show! Vai lá, Pergaminho, Vento!

> Rebeca pega um segundo conjunto de dados pra rolar os ataques. Lembrando que os Soldados possuem +2 por serem ***Peritos em Agir em Conjunto***, mas cada grupo tem apenas +1 agora por Tamanho para enfrentar os Peregrinos. E para "ajudar um pouco mais", seu primeiro ataque obtêm um `----`{: .fate_font}, -4! O segundo grupo ainda consegue um `+0+0`{: .fate_font} para +2. Então um dos grupos está em _Ruim (-1)_, e o outro em _Excepcional (+5)_

**R:** Ok... Como são ___Soldados___, pago 1 PD para realizar um novo rolamento para o grupo que rolou -4... Seria vergonhoso para eles perderem de um grupo de maltrapilhos que se autodenominam Peregrinos do Templo Voado.

> Rebeca rerrola e os dados sorriem, com um `0++0`{: .fate_font}, resultando em outro _Excepcional (+5)_

**R:** (Sorrindo) E agora, como vão defender.

**V:** Vento Silencioso vai simplesmente usar sua velocidade para golpear por baixo a mão da espada dos que a atacam

**Y:** Como Pergaminho Eterno é Esperto, ele simplesmente vai se esquivar nos momentos certos.

**R:** Então é _Esperto_ para Pergaminho e _Ágil_ para Vento, ok? Meio óbvio.

**D:** Não pode as criticar por tentar fazer seu melhor, né?

> Yolanda e Vivian rolam seus dados. Yolanda consegue um `00+0`{: .fate_font}, para +1, finalizando em ___Fantástico (+6)___, e Vivian consegue `0+-+`{: .fate_font}, também para +1 no dado e um ___Fantástico (+6)___. As duas sorriem e então Vivian anuncia.

**V:** Estava pensando... Já que eles estão realmente ___Envergonhados___... Será que podemos cada uma pegar uma Invocação Gratuita desse Aspecto para melhorarmos o resultado para ___Lendário (+8)___? Isso nos daria um Sucesso com Estilo para cada uma, ativando a _Defesa Elegante_

> A _Defesa Elegante_ é uma regra especial do _Do: Fate of the Flying Temple_, onde os Peregrinos do Templo Voador que consigam um Sucesso com Estilo na Defesa podem fazer com que seus atacantes percam alguma habilidade no próximo turno, bloqueando alguma Abordagem ou outra capacidade... Mas como são capangas, ela decide permitir que eles bloqueiem alguma das coisas que eles podem fazer.

**R:** Ok... O que vocês querem fazer? Que capacidade querem bloquear dos Soldados

**D:** Antes de elas decidirem... Posso usar um Ponto de Destino no fato de eles estarem ***Envergonhados*** para fazer o Capitão deles surgir do nada? Isso pode ser uma forma de resolver esse combate rapidamente!

> Roberta sorri: é uma solução muito interessante

**R:** Me descreve então esse Capitão em termos de aparência

**D:** Ele é muito forte, do tamanho de Generoso Urso, o que é um pouco baixo como um Adulto, mas parece muito mais responsável que os Soldados que estão enfrentando os Peregrinos. Ele tem um cabelo meio grisalho e um dos olhos a menos: dá para ver a cicatriz e o olho falso. Ele é leal à coroa e dá para notar que é íntegro e honesto, já que passou por poucas e boas no campo de batalha... E posso colocar que ele conhece o garoto que estamos procurando ou o avô dele?

**R:** Me parece bastante legítimo... Ok... 

> Roberta um outro _postit_ cria uma ficha sobre um **Capitão Alastor Wilheim**, preparando-a como a de um Capanga, mas usando uma versão de capangas melhorados de *Young Centurions*
> 
> **Capitão Alastor Wilheim**
> + Comandante de Batalhão das Forças Armadas de Albion; Leal á Coroa; Impaciente com incompetentes; Amigo de Longa data de Lorde Greymist
> + **Mestre (+4) em:** Voz de comando; Hierarquia; Lutar
> + **Perito (+2) em:** Estratégia; Experiências de Combate; Ler Pessoas
> + **Ruim (-2) em:** Paciência com incompetentes ou bandidos
> 
> Vivian e Yolanda sorri ao notar o que Douglas fez

**V:** Acho que vou querer que o meu grupo de Soldados tenham sua capacidade de _Bajular_ bloqueada. Eles que saim dessa de outro modo

**Y:** Vou com a Vivian. Isso seria muito a cara de Pergaminho Eterno!

**R:** (Sorrindo) Boa solução vocês todos! Enquanto vocês dois, Pergaminho Eterno e Vento Silencioso se movem da maneira fluída que apenas aqueles treinados no Templo Voador podem fazer para evitar serem atingidos por Ataques, um homem baixo e musculoso, em uniforme de gala grita. (ela assume a postura da voz do Capitão Alastor Wilheim) _"MAS QUE PATACOADA É ESSA! Que vergonha! Já é ruim o bastante para Soldados da Coroa levantarem seus sabres contra jovens, mas adiciona ainda mais escárnio notar que são incapazes de parar um grupo de jovens sem apelar para o excesso de força! Agora, me digam o que diabos está acontecendo aqui, antes que perca de vez minha paciência com vocês!"_

**Y:** _"Meu caro capitão, seus homens não acreditaram quando dissemos que somos Peregrinos do Templo Voador, em missão em Albion. Apenas perguntamos por orientações para encontrarmos um certo Amber Carmelian que nos enviou uma carta, pedindo nossa ajuda. Não desejamos nenhum tipo de inconveniente... Mas eles nos tomaram como moleques dos cortiços por causa de nossas roupas incomuns."_ Pergaminho Eterno diz, antes que eles tentem bajular o capitão. Pode ser entendido isso como parte da Defesa Elegante?

**R:** Sem sombra de dúvidas! Ao dizer isso, Peregrino, você nota que os grandes bigodes do Capitão se eriçam e ele chama um outro soldado, obviamente abaixo dele na hierarquia, mas acima dos Soldados. _"Tenente Christiansen, mande esses soldados para a detenção imediatamente. Se eu os ver na minha frente antes da mesa disciplinar deles depois de amanhã, posso perder as estribeiras e então não respondo por mim."_ ele diz, com um tom óbvio de que pode REALMENTE perder as estribeiras contra os soldados. Eles decidem não tentar mais nada, e vão embora com o Tenente. _"Lamento o que ocorreu. É uma pena. Foram infelizes em sua chegada, meus caros amigos do Templo Voador: percebo que são o que alegam não apenas pela suas roupas, mas pela habilidade excepcional que tiveram ao se defender desses Soldados. E além disso... Devem saber do que acontece com o Lord Greymist. Mas melhor providenciar roupas menos chamativas. Venham comigo: vou levar-lhes até minha residência e arrumar algumas roupas enquanto conversamos."_

**Y:** Pergaminho Eterno nota se ele está sendo sincero nas suas intenções?

**R:** Na realidade, todos vocês notam que ele é absolutamente sincero quanto ao fato de conhecer o avô de Amber, Lord Greymist. Ele conduz vocês até uma carruagem com o símbolo das Forças Armadas de Albion e os conduz pelas ruas. Vocês notam que existem todo tipo de pessoas, mas o mais curioso é a grande quantidade de autômatos, todos em posições subalternas: o varredor de ruas, o marreteiro anunciando notícias, a garota que abre a porta do café. De todos ele é possível notar pequenas nuvens de vapor que saem pelo pescoço, além do que aparenta ser um buraco em algum ponto de suas roupas para acesso ao equipamento que permite dar corda nos mesmos

**Y:** _Senhor Capitão..._

**R:** _Me chame de Alastor. Se são Peregrinos do Templo, devem saber sobre o estado de meu amigo, Lord Greymist. Se estou vivo é graças aos Autômatos dele, que me resgataram no campo de batalha contra o Império de Peternost no passado. Mas melhor conversarmos em minha casa: o Tio de Amber, Sir Victor de Elderheim, é um homem cruel e poderoso, e devem saber que é uma péssima combinação._

**D:** _Melhor então que conversemos em sua casa mesmo._

**R:** E abrimos uma terceira cena.  A ***Mansão Wilheim*** é uma mansão apenas em nome, ainda que seja bem grande: um casarão no meio de uma propriedade gramada próxima à capital de Albion, possui acesso à estrutura de gás e de eletricidade. Ao chegarem, servos de Alastor já providenciam roupas ao estilo de Albion para vocês: ternos com fraque para Generoso Urso e Pergaminho Eterno, e um vestido simples, funcional e discreto para Vento Silencioso. Vocês então estão conversando sobre os acontecimentos com o senhor Alastor em sua sala de estar.

**R (Alastor Wilheim):** _Meus caros, vocês tem que entender que muitos dos Mundos próximos como Peternost,  são belicosos, e não possuímos as habilidades ou o discernimentos que vocês do Templo Voador possuem para alcançar soluções pacíficas o tempo todo, ainda que nos esforçamos para tal. Lorde Greymist sempre foi um patriota, alguém que pensou mais em Albion que em seus próprios bolsos, já que certamente teria feito muito dinheiro se vendesse seus autômatos maravilhosos a outras nações, sem colocar escrúpulos. Muitos espiões tentaram alcançar o senhor Greymist, visando sua tecnologia, mas nenhum teve sucesso... Agora, me preocupa que Lord Greymist e Amber se encontrem sob perigo: Sir Victor é um biltre, mas um biltre influente e cheio de recursos. Qualquer ação deve ser tomada com prudência, sob risco de vida de Amber e de Lord Greymist._

**Y (Pergaminho):** _Claro, compreendo... Mas deve haver alguma razão ulterior. Compreendo que a tecnologia de autômatos de Lord Greymist esteja realmente em um nível muito acima do normal, se a carta do jovem Amber diz a verdade, e tudo leva a crer que assim seja. Mas duvido que apenas o dinheiro justifique. Sabe dizer se existe alguma tecnologia na qual o mesmo estivesse trabalhando que poderia ser inestimável na guerra? Ou algo ainda mais revolucionário, supostamente?_

**R:** Yolanda, role o _Cuidadoso_ de Pergaminho Eterno, e eu vou rolar uma Defesa do Senhor Alastor. Aparentemente você tocou em um ponto sensível.

> Ambos rolam os dados. Yolanda consegue um `+0++`{: .fate_font} e Rebeca um `0-++`{: .fate_font}. Como o Capitão Alastor não tem razões para não confiar em Pergaminho Eterno, sua perícia para tal é _Medíocre (+0)_, não se encaixando nem nos Peritos ou nos Ruins de Alastor, totalizando _Regular (+1)_ do dado. Já Yolanda soma ao _Cuidadoso Razoável (+2)_ de Pergaminho Eterno os +3 dos dados para um resultado final de _Excepcional (+5)_, o que é MUITO Sucesso com Estilo.

**R:** Me diz, Yolanda, o que Pergaminho Eterno imagina que possa ser tal tecnologia, e eu digo a reação de Alastor a isso.

**Y:** _Devo supor então que Lorde Greymist trabalhava em alguma coisa ainda mais avançada que autômatos para guerra ou os autômatos serviçais que vimos pelas ruas, pouco mais que manequins obedientes a reações previamente programadas. Alguma coisa ainda mais sofisticada, além de qualquer coisa que qualquer cientista tenha previamente visto, seja para a Guerra ou não. Estou certo?_

**R:** Alastor sorri: _"Bem dizem que a sabedoria do Templo Voador, ainda que não convencional, é muito superior. Acertou na mosca Peregrino: há alguns anos, meu amigo me confidenciou que estava trabalhando em um novo Moinho de Processamento, o nome que damos ao componente central das respostas dos autômatos, onde as operações mecânicas avançadas formulam os processamentos lógicos que emulam um grau de senciência. Lorde Greymist acreditava ser capaz de criar um Moinho revolucionário, ao qual chamou de Moinho Cuore, capaz de oferecer um novo grau de senciência aos autômatos, aproximando-se do nível dos seres humanos. Entretanto, ele não desejava oferecer isso como arma de guerra: ele acreditava que os autômatos padrão já eram o suficiente para o engenho marcial, não havia necessidade de maior evolução. Eu não sei se Lord Greymist alcançou tal intento, ele me confidenciava ocasionais sucessos e muito menos ocasionais fracassos em seu empreendimento, mas se a preocupação do jovem Amber for realmente tão grande quanto ao estado atual dos eventos a ponto de enviar uma Carta Pneumática ao Templo Voador, certamente algo mais sério está ocorrendo: existem antigos pactos envolvendo Albion e o Templo Voador, pactos de socorro e suporte, de eras e eras do passado, quando os Mundos eram jovens."_

**D (Urso):** _Então, será que Lorde Greymist teria construído tal Moinho e essa seria a ambição real de Sir Victor?_

**R (Alastor):** _Supostamente sim... Nos tempos recentes, Sir Victor tem se arrolado como protetor de Lord Greymist e preceptor do jovem Amber diante da Lei e da Sociedade. Suponho, porém, que ele tenha como objetivo tentar obter seus segredos, incluindo, se bem sucedido, o funcionamento e detalhes sobre o Moinho Cuore. E suponho que, já que Amber está temeroso por sua vida, ele deve saber se o Moinho Cuore foi finalizado com sucesso ou não._

**V (Vento):** _Devemos então procurar os dois... E fazer o possível para os resgatar. O mais rápido possível._

**R:** _Essa é uma situação que infelizmente não posso lhes ajudar: não posso manchar a honra das Forças de Albion, sob o risco de colocar a monarquia e o Estado em risco. Mas conheço um homem que pode lhes ajudar. O nome dele e Luc Pointier, um Pattisiere da Praça Marcuse, um dos principais locais de encontro da Sociedade de Albion. Seu irmão Jacques é mordomo de Lord Greymist e tutor de Amber, sendo um dos poucos dos antigos serviçais do Solar Greymist que não foram mandados embora. Pode ser que ele lhes ajude a se infiltrarem no Solar visando resgatar Amber e Lord Greymist._

> Rebeca coloca _post-its_ de Aspecto na mesa: ___Solar Greymist___ e ___Luc Pointier, Pattisiere amigo do Capitão___ e ___Jacques Pointier, Tutor de Amber___. Isso pode ajudar os Peregrinos a seguir adiante

**Y (Pergaminho):** _Talvez vocês dois, Generoso Urso e Vento Silencioso, possam tentar tal solução, já que depende de discrição ao se infiltrar e velocidade potência ao evadir-se. Não sou muito hábil em tais coisas. Mas... Posso recorrer aos meus conhecimentos para ajudar a, pelos meios legais da Albion, procurar salvaguardar os dois, investigando se realmente o que Amber diz na carta é verdade. Isso certamente seria um crime de Cárcere Privado, ainda mais considerando que se tratam de um jovem e um senhor idoso com saúde debilitada, correto?_

**R:** _Provável. A dificuldade é fazer uma acusação de tal feita ser colocada contra Sir Victor. Precisaríamos de provas contundentes, que não possam ser esnobadas._

**Y (Pergaminho, olhando para os demais):** _Podemos então atacar em duas frentes. Generoso Urso, Vento Silencioso, vocês dois poderiam entrar no Solar Greymist, com a ajuda Luc e Jacques Pointier e obter alguma prova e, caso necessário, planejando um resgate de ambos. Eu posso investigar a cidade para tentar obter mais pistas e atacar Sir Victor no tribunal. Na pior das hipóteses, isso o manterá ocupado em mim, tirando a atenção dele de vocês, auxiliando caso seja necessário realizarmos um resgate de ambos._

**V (Vento):** _Mas por onde começar a achar tais pistas?_

**Y (Pergaminho):** _Capitão Alastor, o que você pode nos dizer sobre os doutores que acompanham Lorde Greymist, os antecessores e o atual?_

---

> Agora cabe aos Peregrinos do Templo Voador descobrir mais sobre esses mistérios envolvendo Amber Carmelian, Lorde Greymist, Sir Victor de Elderheim, e o Moinho Cuore. O que é tal Moinho? Ele foi criado? Ou não? E o que isso tem a ver com o Mundo belicoso de Peternost? Os Monges devem solucionar tais mistérios, mesmo arrumando outros no caminho, e com isso também obter pistas sobre o que aconteceu ao Templo Voador.

<!--  LocalWords:  teaser post categories header excerpt separator of
<!--  LocalWords:  tags FAE Fair Leaves Hearted the Flying Yolanda ok
<!--  LocalWords:  Jukku Ishita Rova imperceptiva Roleplay GULP PDs
<!--  LocalWords:  aramelas nez Spun and Gold Carmelian Sir Steampunk
<!--  LocalWords:  Victoria HAHAHAHAH Lord Greymist aje it rerrola
<!--  LocalWords:  postit Wilheim Young Centurions Christiansen Cuore
<!--  LocalWords:  Peternost Elderheim Luc Pointier Pattisiere its
 -->
 -->
 -->
 -->
 -->
 -->
 -->
