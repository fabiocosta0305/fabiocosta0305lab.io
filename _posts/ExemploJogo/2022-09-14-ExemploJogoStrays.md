---
title: "Exemplo de jogo em _Strays_"
#teaser: "Quando a turma precisa realizar um Teste de Coragem, os jogadores vão explorando o cenário e as regras!"
layout: post
date: 2022-09-14
categories:
    - RPG
header: no
excerpt_separator: <!-- excerpt -->
tags:
  - Fate
  - Aventura
  - Exemplo de Jogo
  - Strays
  - ExemploDeJogo
---

> Vera decide narrar com alguns amigos [_Strays_](http://www.drivethrurpg.com/product/169261/Strays), um jogo baseado em Fate onde Animais de Estimação, depois de anos com seus criadores, passam a trabalhar com o Papai Noel, o Coelho da Páscoa ou outro dos Campeões do Bem para auxiliar crianças e animais a seguirem esse caminho. No caso, os jogadores serão:
> 
> + Sua Filha Cecília, que irá jogar com [Marie](http://fabiocosta0305.gitlab.io/personagens/Marie/), uma gatinha que é muito bonitinha, mas manhosa e mandona como poucas!
> + Seu Filho Fabrício, que vai jogar com [Cofap](http://fabiocosta0305.gitlab.io/personagens/Cofap/), um cachorro _basset_ que ganhou título em cima de título de _Agility_ antes de ir para a Fazenda;
> + Seu marido Durval, que criou [Puffers](http://fabiocosta0305.gitlab.io/personagens/Puffers/), um gambá que era muito malvado e fedido até que foi resgatado de uma armadilha por um garoto e foi cuidado por ele, até ficar bastante velhinho;
> 
> Depois de receber [uma missão do Bom Velhinho](http://fabiocosta0305.gitlab.io/aventuras/UmGarotoProblematico/) de um de seus emissários mais importantes, Joffrey a calopsita, os três saem da Fazenda em direção a uma cidade próxima. Antes, porém, eles procuram um contato que Puffers conhece, um hamster chamado [Samwise](http://fabiocosta0305.gitlab.io/personagens/Samwise/) que é conhecido por conhecer bastante coisa dos humanos, o que vai ajudar eles.

<!-- excerpt -->

**Vera:** Bem, depois de um dia inteiro de caminhada, vocês chegam até a praça que Puffers disse seria o ponto de encontro entre vocês e Samwise. O sol está se pondo e vocês estão realmente exaustos. A praça em questão tem algumas árvores e aqueles brinquedos grandes e estranhos de humanos.

> Vera toma todo o cuidado de narrar tudo do ponto de vista de animais de estimação, então eles não tem como saber que é um parquinho, ao menos não de imediato

**Cecília (Marie):** _Espero que esse Samwise não demore! Estou cansada, suada, com sede e fome, e estou cheirando igual ao Puffers!_

**Durval (Puffers):** _Se você realmente estivesse cheirando igual a mim, ninguém aguentaria. Acredite, minha cara, eu realmente não cheiro bem quando quero ou preciso._

**C (Marie):** _Ainda bem! Não suporto ficar fedida ou sentir cheiros ruins!_

**D (Puffers):** _Você tem suas garras, eu tenho a minha cauda... Cada bicho tem seu jeito de se defender._

**F (Cofap):** _Vamos logo! Enquanto Samwise não chega, que tal brincarmos um pouco! Duvido que você me vença no_ Agility, _Marie!_

**D:** Começamos cedo...

**V:** Que tal se eu forçar os Aspectos de vocês dois, Cofap e Marie? Cofap _Age rápido, mas não necessariamente de maneira esperta_ e Marie _Se considera a líder de campo_. Cofap poderia provocar alguma confusão e envolver Marie nela!

**C, F:** Topamos!

> Vera passa aos dois uns lacinhos de fita que ela usa como Pontos de Destino

**V:** Cofap, todos os brinquedos humanos te lembram sua época de Campeão de _Agility_, e você não consegue resistir e sai correndo pelos brinquedos!

**C (Marie):** _Ei, seu pulguento! Volta aqui!_ Quero que Marie consiga parar Cofap de algum modo!

**V:** Pensando assim, é uma Disputa entre Cofap e Marie. Cofap quer brincar até cansar sem ser pego por Marie, certo? E Marie quer controlar esse cachorro que a desobedece. Relembrando regras: vocês rolam a abordagem apropriada, quem conseguir o melhor resultado obtem uma vitória, quem conseguir 3 vitórias primeiro vence. Entendido?

> Cecília e Fabrício fazem que sim com a cabeça

**V:** Então, descrevam como vocês vão fazer.

**F:** Cofap é um **_Campeão de_ Agility**, então ele consegue ser super-rápido passando pelos obstáculos.

**C:** Marie quer provar que é tão rápida quanto Cofap, e vai perseguí-lo pelo caminho!

**V:** Bem... Então é _Ágil_ vs _Ágil_, beleza? Podem rolar os dados!

> Cofap começa com um ótimo rolamento `0+++`{: .fate_font}, enquanto Marie consegue algo próximo com `++00`{: .fate_font}. 

**V:** Então vamos ver: tanto Cofap quanto Marie possuem _Ágil Bom (+3)_, então temos um _Fantástico (+6)_ para Cofap e um _Excepcional (+5)_ para Marie.

**F:** Mãe, tem a Façanha do Cofap de ___Mestre de Agility___ que soma +2 ao Superar Obstáculos que lembram coisas de Agility, como tudo que tá nesse parque, certo? Além disso, tem o embaraço do Cofap que também soma +1 quando ajo de maneira rápida para conseguir alguma coisa.

> ___Embaraços___ são "Façanhas Menores" que todo _Stray_ tem: elas oferecem +1 em uma situação específica, mas -1 em outra. No caso de Cofap, seu embaraço ___OLHA O FREIO!!!___ oferece a ele +1 sempre que ele precisa agir de maneira rápida para alcançar um objetivo, mas reduz em -1 os rolamentos para evitar problemas relacionados ao fato de agir rápido demais

**C:** Eu sempre esqueço disso!

**V:** Fazer o quê, Cecília? Ele então vai para _além de Lendário (+9)_, somando tudo! São duas vitórias para Cofap!

**C:** DUAS?!

**V:** Foi um sucesso com estilo! Lembra... Marie teve _Excepcional (+5)_, contra o _além de Lendário (+9)_ de Cofap. 9 menos 5 é 4, o que é acima de 2, lembra?

**C:** Verdade... Marie deve estar suando mais do que ela já estava!

**V:** Sim! Cofap se esquiva dos brinquedos humanos como se eles simplesmente não existissem, deixando uma Marie muito cansada para trás...

**C (Marie):** _Saco... De... Pulgas... bobão!_

**F (Cofap):** _Quero ver vocẽ me pegar, Marie!_

**V:** Segunda rodada!

> Fabrício e Cecília pegam os dados Novamente: Cecília consegue um `0-0+`{: .fate_font}, e Fabrício tem um péssimo rolamento com `---0`{: .fate_font}. Mesmo com a sua Façanha e Embaraço, isso significa que eles empatam, já que o rolamento anula os bônus oferecidos pela Façanha e pelo Embaraço de Cofap.

**V:** Fabrício, por enquanto é um empate. Quer usar Pontos de Destino para melhorar o rolamento? Seria o suficiente para você vencer a Disputa!

**F:** Não!

**V:** Bem... Então como é um Empate, as circunstâncias vão mudar... Agora... Que tal se Puffers agir?

**D:** Posso então sugerir algo? Puffers vai sentir que algo está errado, usando obviamente para isso seu faro. Enquanto Marie e Cofap estavam correndo, Puffers observou os arredores e percebeu cheiros diferentes se aproximando. Pode ser?

**V:** Rola o _Esperto_ de Puffers, então... A dificuldade é _Razoável (+2)_, já que não existe nada especialmente diferente ocultando os cheiros, mas tem muita coisa por aí para você detectar.

**D:** Beleza... E isso vai incluir também mais um do Embaraço de Puffers

> O Embaraço de Puffers, ***É tudo sobre cheiros*** lhe dá +1 sempre que ele precisa usar seu faro para discernir coisas, mas reduz em -1 todos os seus rolamentos sociais porque ele realmente não cheira bem.
> 
> Durval consegue um excelente `+0++`{: .fate_font} nos dados... Somados com o _Esperto Bom (+3)_ e o _Embaraço_ de Puffers, o total é _Épico (+7)_

**V:** OK... Puffers, você nota um cheiro conhecido se aproximando... É Samwise, mas algo no cheiro dele está errado, tem muito cheiro de medo nele.

**D:** Puffers eriça sua cauda o suficiente para deixar um leve aroma dele sair, o suficiente para que Marie e Cofap parem de brincadeira. (Puffers) _"Vocês dois... Parece que algo está errado!"_

**V:** Cofap, Marie, vocẽs sentem um cheiro leve, mas bem ruim, vindo de Puffers. Por pior que seja, vocês sabem que ele faz isso como um sinal de alerta. Quando vocês olham, vocês vêem Samwise correndo desembestado e se escondendo em um dos tubos por onde vocẽs correram poucos instantes antes. Junto com ele, vocẽs notam dois cachorrões, que param ao ver vocês. (Um dos cachorrões) _"Ei, seus Bonzinhos! Vocês não tem nada com isso! Se gostam do pelo de vocês, caiam fora e deixem a gente pegar nossa janta!"_

**C (Marie):** _Ei, ele é apenas um hamsterzinho de nada... E se a gente se ajudasse para comer algo mais gostoso e substancial._

**V:** Qual a sua ideia nesse caso, Marie?

**C:** Quero fazer esses cachorrões acharem que podemos levar eles para jantar em outro lugar, por exemplo, revirando uma lixeira de um restaurante, mas aí damos o bote neles.

**V:** Bem... Nesse caso... Seria por _Sorrateiro_... Ou você quer se mostrar para eles?

**C:** É LOGICO que Marie vai se mostrar? A Gata Angorá mais bonita da região não vai levar desaforo para casa!

**V:** Bem... Nesse caso pode ser _Estiloso_, e conta tanto o seu _Embaraço_ quanto seu _Charminho_

> Marie possui uma Façanha chamada ___Charminho___: como ela é a Gata Angorá mais fofa que existe, ela recebe +2 ao _Criar Vantagens_ de _Maneira Estilosa_ envolvendo minha fofura. Além disso, o Embaraço dela, ___Por fora bela viola…___ lhe dá +1 sempre que precisar convencer alguém que não a conheça que ela é muito fofa. Entretanto, ela recebe -1 sempre que alguém que a conheça por mais tempo precisar ser convencida de algo.
> 
> Cecília então coloca uma "ficha" para os Cachorrões:
> 
> + ***Cachorros Malvados:***
>    + ___Cahorros Maus; Esse território é NOSSO, e aquele ali também; Músculos no Cérebro___
>    + ___Peritos (+2) em:___ Intimidar; Perseguir criaturas menores ou mais fracas; Morder
>    + ___Ruins (-2) em:___ serem espertos
>    
> Os dois estando juntos, ela decide tratar os dois como um Grupo de Capangas para +1 em todos os rolamentos.

**C:** Vamos lá dadinhos... Não quero o pelo da Marie bagunçado!

> Vera rola `-0+-`{: .fate_font}, enquanto Cecília rola `00+0`{: .fate_font}

**C:** Parece que o seu resultado foi mais que o suficiente para que os Cachorrões começassem a ___Baixar a Guarda___. Alguém quer fazer algo?

**D:** Puffers não tem muita paciência com valentões... Ele próprio tendo sido um na juventude. Ele olha para Samwise assustado e então diz (Puffers, em um tom sério) _Marie... Deixa esses aí comigo!_

**C:** Essa não!

**D:** Eu quero mostrar um pouco sobre ___Os Velhos Tempos que ainda me assombram___. Vou Criar uma Vantagem e usar isso como narrativa... É possível?

> Qualquer Aspecto em jogo pode ser usado para qualquer das funções de um Aspecto...

**V:** Sua ideia então é usar o ___Perfume do Gambá___?

**D:** Claro!

**V:** Isso parece interessante... Como vai fazer isso?

**D:** Enquanto Marie se exibe, eles não percebem que Puffers aparece por trás dela, já mostrando-lhes a cauda totalmente eriçada!

**V:** Isso vai ser divertido, pois uma Falha pode resultar em Marie ficar toda fedida!

**C:** EI! Isso não vale!

**D:** Só um desastre para isso falhar, Cecília. Vamos ver o que acontece. 

> Durval apanha os dados novamente e rola um `0--+`{: .fate_font}. Esse -1 não complica tanto as coisas, já que seu _Esperto Bom (+3)_, é somado à sua Façanha ___O Perfume de um Gambá___  que lhe dá +2 ao Criar Vantagens de maneira Esperta envolvendo seu mau-cheiro, já que ele é um _Gamba Realmente Fedido_. O Embaraço não conta aqui pois ele não está discernindo cheiros.
> 
> Vera rola `-0-+`{: .fate_font} contra os Cachorrões, o que é um alívio, pois eles são ___Ruins (-2) em Serem Espertos___. Isso garante um belo Sucesso com Estilo para Puffers!

**V:** O que aconteceu então?

**D:** Puffers explode sua bomba cheirosa e é algo tão horrendamente fedido que os cachorros quase têm uma tela azul mental, ficando paralisados pelo cheiro horrível. (Puffers) _"Agora sumam daqui! E avisem todos os Malvados da cidade que **Puffers está aqui**! Tenho mais o que fazer que lidar com valentões como vocês, ainda mais que O Velho do Saco está se aproximando da cidade! Caiam fora!"_

**V:** Os Cachorrões saem correndo, olhos vermelhos de lágrimas ao carregarem consigo a marca de que ***Puffers está na cidade***.

**D (Puffers):** _Odeio recorrer a isso, mas às vezes é necessário._

**C (Marie):** _Eu também odeio! Nossa, que cheiro horrível! Não consegue parar isso não?_

**D (Puffers):** _Minha vida com o Carlos teria sido muito mais fácil se conseguisse simplesmente parar com isso, mas não é assim que tudo funciona._

**F (Cofap):** _Bem, ao menos aqueles cachorrões foram embora! Acho que seria um problema brigar com eles, pareciam bem fortes... Cadê o Samwise?_

**V:** Samwise está todo encolhidinho no tubo onde ele se enfiou fugindo dos cachorrões

**C:** Marie vai e dá umas cutucadinhas com a garra dela. Não para machucar, mas é quase ela brincando com um ratinho...

**V:** Quando você dá a primeira cutucada vocês escutam ele gritar (Samwise) _AIAIAI! Chegou minha hora! Nossa Senhora dos Hamsters me receba na sua morada!_

**C (Marie):** _Para com isso... Samwise, né? Eu sou Marie, esses são Puffers e Cofap. O Velhinho nos mandou vir para cá da Fazenda... Uma missão para tentar corrigir um garoto Malvado antes que o Velho do Saco faça torta dele._

**V:** Vocês notam que Samwise se recompõe um pouco e diz (Samwise) _"Puffers, meu chapa, ainda com aquele cheirinho de sempre, né? Livrou todos nós de uma enrascada, esses cachorrões Malvados são uma dor de cabeça! Prazer em conhecer vocês, Marie, Cofap! Vocẽs parecem meio cansados, com fome e com sede... Posso ajudar um pouco! Me acompanhem! Pode me dar uma carona, Puffers?"_

**D (Puffers):** _Claro, Samwise... Não estou tão fedido, pode subir pela minha cauda!_

**V:** Vocês saem do parque e, Cofap e Marie, vocês notam que Samwise e Puffers se conhecem do passado. As pessoas notam a combinação estranha de um Hamster, um Gambá, uma Gata Angorá e um Cachorro _basset_ caminhando pela cidade, mas evitam o grupo, em especial porque a cauda de Puffers não para de soltar aquele cheirinho de gambá por aí, bastante fraco mas bastante fedido. (Samwise) _"Então... Esse garoto é valentão, e o cachorro dele é quase tão ruim quanto o próprio. Mas apenas_ quase: _parece que ele era usado em rinha de cães. Lancaster é o nome dele, e ele mais parece algo atropelado por um caminhão que um cachorro: falhas no pelo por todos os lados, marcas de dentada nas orelhas e uma cicatriz cruzando o focinho. O Govinda, que cuida dos demais aqui, já foi 'batizado' pelo Lancaster, tendo uma boa parte da cauda arrancada pelo mesmo na mordida!"_

**C (Marie):** _ECA! Será fazer esse menino se redimir via Lancaster pode funcionar?_

**F (Cofap):** _Talvez... Se realmente esse menino gostar tanto de Lancaster, esse  possa ser o melhor caminho. Humanos gostam muito de nós cachorros, às vezes eles percebem que queremos que eles não façam algo muito ruim. Se avisarmos para Lancaster que o Velho do Saco está a caminho da cidade, visando pegar o garoto, talvez isso funcione..._

**D (Puffers):** _Talvez seja realmente o melhor caminho de imediato... Mas talvez seja melhor fazermos uma pausa por hoje... Estamos cansados, com fome, com sede e não sou apenas eu que estou cheirando mal hoje._

**C (Marie):** _Tem razão, Puffers... Eca, eu odeio ficar fedida como estou. Não sei como você aguenta._

**D (Puffers):** _Como já te disse, Marie, vocẽ tem suas garras, eu tenho o meu cheiro._

**V (Samwise):** _E cá entre nós, melhorou bastante o controle do cheiro, Puffers. No passado era realmente difícil aguentar seu cheiro! Ah, estamos chegando no Beco dos Vadios. Govinda cuida de todos e ele que achou esse canto aqui, perto de um restaurante onde podemos comer._

**C (Marie):** _Maravilha! Estou realmente com fome. Será que vai ter peixe?_

**D (Puffers):** _Se tiver vai ser peixe jogado no lixo... Não temos como escolher o cardápio._

**C (Marie):** _Fazer o que?_

> Vera encerra a cena com a chegada dos três ao Beco dos Vadios

**V:** Vocês caminham dentro do Beco e notam que ele é na parte de trás de um velho restaurante, com uma caçamba de lixo bastante grande atrás. Como todo Beco, não tem o melhor cheiro do mundo, mas nada que vocês não sentido visto antes. O Curioso é a grande quantidade de caixotes e caixas próximas, o que permite formar um certo abrigo contra o clima. (Samwise) _Govinda, aqueles amigos meus chegaram!_ diz Samwise, quando um camundongo marrom, com uma parte da cauda a menos, sai de uma caçamba de lixo, meio que carregando um pedaço de queijo meio velho e mofado. (Govinda) _Ah, Samwise! Sejam bem vindos... Eu sou Govinda. Estão com sorte! Chegaram bem na hora do jantar, acabaram de jogar um latão cheio dentro do lixo. Podem se servir!_

**C (Marie):** _Eu é que não faço isso! Me enfiar em uma caçamba de lixo? HUMPF!_ Quero que eles entendam que Marie, sendo ***A Gatinha mais adorável que existe (na concepção dela)***, NUNCA vai se meter na sujeira.

**D:** Puffers tem uma vantagem nesse caso, por ser um animal maior que Marie, Govinda, Samwise e Cofap... Além disso, ele é muito bom em discernir cheiros... E se ele coletasse no meio do lixo tudo que fosse do bom e do melhor e separasse para a janta deles? Isso aliviaria as coisas com Govinda?

**V:** A gente pode descobrir... Rola teu _Esperto_ e pode somar seu Embaraço. Dificuldade _Razoável (+2)_

> Durval rola `++--`{: .fate_font}. Mesmo com esse +0 nos dados, graças ao _Esperto Bom (+3)_ e o +1 do Embaraço, é um sucesso tranquilo pra ele.

**V:** Marie, Cofap, vocês notam Puffers se chafurdando no lixo, de quando em quando pondo o focinho para fora com algum pedaço de comida mais limpo e saboroso: queijo, algumas linguiças, um pão, e assim por diante. Govinda olha com bastante fome.

**D (Puffers):** _Podem se servir._

**C (Marie):** _Obrigada Puffers!_

**F (Cofap):** _Obrigado! Govinda... O Samwise disse que você conhece o tal Lancaster... O que você pode nos contar sobre ele?_

**V (Govinda):** _Se conheço... Todo bicho nessa cidade conhece Lancaster. Ele é quase tão ruim quanto o dono dele: graças ao 'batismo' de Lancaster eu não tenho uma parte da cauda. Já tivemos que conseguir convencer um humano a levar Lilly, uma coelhinha que ele atacou... Para o... Veterinário!_

**C (Marie):** _Puxa... Realmente então Lancaster parece bem malvado. E sobre o dono dele? Ele tem dono, né?_

**V (Govinda):** _Sim... Eu estou sozinho graças a esse garoto, Rodrigo. Sempre que tento me aproximar de alguma outra criança da escola, ele acaba se aproximando e as provocando, fazendo_ bullying.

**D (Puffers):** _Bem... Parece que ele é durão mesmo, o tal Rodrigo._

**V (Govinda):** _Que nada... Ele só ataca porque tá em grupo. Sempre o mesmo grupo, cinco ou seis garotos além do Rodrigo, que só sabem arrumar confusão e fazer arruaça._

**C (Marie):** _Então, o Velho do Saco não está vindo só por causa do Rodrigo?_

**V (Govinda):** _Sinceramente, não sei e prefiro não saber: pelo pouco que sei, o Velho do Saco é perigoso, e muito mais forte que qualquer vadio sozinho pode encarar sozinho. Eu sei que vocês estão pensando em proteger Rodrigo, mas sinceramente o considero um caso perdido._

**F (Cofap):** _Será que não conseguimos fazer nada?_

**D (Puffers):** _Eu era um caso perdido, não fosse o Carlos. Eu não acredito que Lancaster seja durão desse tanto. Amanhã cedo iremos ver esse cara. Vamos comer, tomar água e descansar por hoje. Ainda temos algum tempo antes do Velho do Saco chegar!_

**V:** E vocês terminam de comer e descansam o resto da noite. Quando vocês acordam, já é de manhã, e ainda tem sobras do banquete que você providenciou, Puffers... O que vocês vão fazer?

**F (Cofap):** _Govinda... Será que não pode ao menos nos indicar aonde o Lancaster mora?_ Eu digo enquanto comemos e bebemos antes de seguir adiante.

**V (Govinda):** _Bem... Eu não vou interferir, e só vou indicar o caminho até chegarmos perto da casa dele. Depois vou me mandar. Sinto muito, mas não consigo ajudar Lancaster._

**C (Marie):** _Ratinho mal!_

**D (Puffers):** _Marie, de certa forma Govinda não tem realmente nenhuma razão para ajudar Lancaster mais do que está se propondo. Tudo bem, Govinda. Já será de bastante ajuda._

**V:** Govinda indica o caminho para vocês, subindo junto com Samwise em suas costas, Puffers. e mostrando a direção, até que vocês vêem uma grande casa humana com janelas em cima e em baixo. (Govinda) _Aquela é a casa do Lancaster. O canil dele fica na parte de trás da casa em um quintal. Me perdoem, mas só de pensar no Lancaster me vêem a cabeça ele arrancando a minha cauda. Vou indo nessa... Boa sorte e, sinceramente, espero que consigam o que querem, ainda que ache difícil._

**F (Cofap):** _Obrigado, Govinda. Se encontrarmos outras crianças, vamos dar um jeito de arrumar alguém para cuidar de você._

**V:** OK... Enquanto Govinda se afasta, o que vocês vão fazer?

**D:** Puffers sente algum cheiro esquisito?

**V:** Nada: a vizinhança é bem humana nesses quesitos. A fumaça dos veículos e da comida vinda das casas, lixo, ocasionalmente o cheiro de humanos limpos e sujos... Nada fora do que você esperaria encontrar ali.

**F (Cofap):** Acho que Cofap pode sair por aí e ver se acha algo diferente.

**V:** Olha só... Como você quer Criar uma Vantagem procurando pistas, sua Façanha não conta... Pior, vou considerar que sua parte ruim do Embaraço conta, já que você não faz ideia do que vai encontrar... Rola seu Ágil contra dificuldade _Boa (+3)_.

> Fabrício rola e obtem `++--`{: .fate_font} nos dados, para 0. No caso, ele sofre uma Falha, já que o Embaraço de Cofap reduz seu Ágil de _Bom (+3)_ para _Razoável (+2)_

**F (Cofap):** Ops... E se eu pegar um Sucesso a Custo?

**V:** Pode ser... Mas nesse caso não é você que acha alguma coisa... Alguma coisa acha ___você___. Puffers, Marie, vocês notam Cofap correndo para variar, quando de repente vocês escutam um _CAIM_ do Cofap, como se alguma coisa tivesse o pego!

**C (Marie):** _Cofap está em perigo! Vamos lá!_

**D (Puffers):** _Vamos!_

**F (Cofap):** _Caim! Caim! Socorro, me ajudem!_

**V:** Vocês seguem os ganidos de Cofap e chegam no quintal onde Lancaster está... No caso, vocês notam um cachorrão pitbull que parece estar segurando preso Cofap.

**C (Marie):** _Você é o Lancaster? Para com isso e solte nosso amigo Cofap!_

**V (Lancaster):** (Com uma voz como se algo estivesse na boca) _"Então vocês são amigos desse salsicha aqui? Ele entrou na casa dos outros sem pedir! Devia dar uma bela mordida nele!"_

**D (Puffers):** _Nem pense nesssa ideia! Eu sou Puffers, acho que algum amigo seu deve ter dito que eu estava por aí! Eu quero apenas conversar, mas não vejo problema em colocar um pouco de perfurme de gambá em alguém que precise._ Estou usando aquele Impulso lá do início da Aventura para convencer Lancaster que (1) sou bastante durão mas (2) não quero confusão.

**V:** Está certo! Tirando o susto, Cofap, vocẽ está bem, mas Lancaster olha para você, Puffers, com um sorriso esquisito. Você percebe então que ele não tem uma boa parte de um dos lados do focinho, arrancado a dentadas. Dá para perceber muitas falhas no pelo, e a cicatriz que Govinda mencionou.

**D (Puffers):** _Pelo jeito não teve vida fácil, né, Lancaster? Conheci sua fama por aí. Dizem que você é durão, mas apenas contra cachorrinhos ou bichinhos menores._

**V (Lancaster):** _Tá me tirando, velhote. Eu conheço a tua laia: quer bancar o bonzinho, mas no fim do dia é igual a mim..._

**D (Puffers):** _Juro que fosse outra situação, já estaríamos nos pegando. Mas eu não estou aqui pra isso... Me diz uma coisa, vocẽ gosta do tal Rodrigo?_

**V (Lancaster):** _Ele me salvou quando estava para ser estraçalhado pelos demais cães do meu antigo dono, um babaca de uma rinha de cães. Rodrigo enfrentou aqueles cães com um pau e me trouxe para casa. Cuidou das minhas feridas e tal. Não fosse isso, já teria partido dessa. Agora... As outras crianças são bem babacas também, sabiam? Elas acham que a gente que é bicho não entende, mas eu comecei a sacar. Dizem que ele é tão forte quanto um Touro, e tão esperto quanto um também. Diz se isso não é para humilhar alguém?_

**F (Cofap):** _Parando para pensar não deixa de ser._

**C (Marie):** _Mas isso não é motivo para machucar outras crianças, e nem para você bater em outros bichos._

**D (Puffers):** _Você tem razão, Marie. Lancaster, Rodrigo não pode continuar fazendo essas coisas, isso é perigoso demais. Ele tem que tentar resolver essas coisas conversando e ajudando os demais, não no muque!_

**V (Lancaster):** _Fala sério! As demais crianças sempre caçoaram dele por ser burro._

**D (Puffers):** _Mas agredir e quebrar ossos não ajuda nada, muito pelo contrário. A gente tá aqui porque... O Velho do Saco está vindo para cidade! E o alvo dele é Rodrigo e a turma dele!_

**V:** Nessa hora, quando Puffers fala isso, vocês vêem Lancaster se encolhendo e uivando (Lancaster) _"Auuuuu! O Velho do Saco? Mas... Isso que dizer que..."_

**F (Cofap):** _Sim, Lancaster: Rodrigo está na Lista dos Malvados._

**V (Lancaster):** _Mas... Como? Ele é um garoto bom, só as demais crianças que pegam o tempo todo no pé dele._

**C (Marie):** _Eu não acho que agredir outras crianças é ser bom, mesmo com tudo o que você disse._

**D (Puffers):** _A sorte é que ainda temos algum tempo para convencer Rodrigo a parar de machucar outras crianças e com isso tirar ele da Lista dos Malvados... E vamos precisar de sua ajuda, Lancaster... Vocẽ vai ter que começar a desobedecer Rodrigo se ele mandar você agredir outras crianças. Além disso, tente estimular ele a conversar com os outros._

**V:** Nesse momento, vocês ouvem um grito terrível próximo... É um grito de uma pessoa jovem, vocês reconhecem.

**D:** Puffers vai na direção do grito. (Puffers) _Venham! Isso inclui você também, Lancaster! Algo terrível acabou de acontecer, e espero que não seja o que estou pensando!_

**V:** Vocês correm na direção que Puffers vai... Vocês vão parar em um outro parque, e vêem que Govinda está próximo, junto com Samwise, em uma árvore. (Govinda) _Vimos alguém fugindo naquela direção, mas parece que não tem ninguém para lá!_

**F:** Cofap vai procurar alguma pista na direção indicada por Govinda.

**V:** Cofap, você sente um cheiro nauseabundo conforme você se afasta, até um certo ponto em que o cheiro literalmente some, como se quem quer que libere aquele cheiro tivesse sumido no meio do ar.

**F:** Quando volto explico o que aconteceu aos demais.

**D:** Puffers vai cheirar o local e ver se acha alguma outra pista.

**V:** Faz um rolamento de _Esperto_, e pode somar o seu Embaraço. Dificuldade _Medíocre (+0)_: a coisa é tão recente que é mais para ver o que está acontecendo.

> Somando o Embaraço, o _Esperto_ de Puffers é _Ótimo (+4)_. Com o rolamento `-0++`{: .fate_font}, fica em _Excepcional (+5)_ indicando um sucesso com estilo

**V:** Puffers você está acostumado a cheiros ruins, mas esse cheiro é TÃO mais ruim que mesmo você fica enojado. Você acha então um par de tênis velhos, mas o cheiro não vem do mesmo, mas _ao redor_ do mesmo

**D (Puffers):** _Nunca imaginei dizer isso.... Mas isso aqui fede mais que eu. E isso não é pouca coisa. Conhece alguém que use esse negócio humano, Lancaster?_

**V (Lancaster):** _É... De um... Dos amigos do Rodrigo! Um dos garotos da turma dele!_

**D (Puffers):** _Péssimo sinal... Isso quer dizer apenas uma coisas: **O Velho do Saco já chegou à cidade!** O tempo está se esgotando, não podemos mais perder tempo. Se falharmos, Rodrigo estará perdido!_

---

A chegada do Velho do Saco indica que o tempo de Rodrigo está se esgotando. Será que vão conseguir fazer Rodrigo parar de fazer maldades? E como vão o fazer? Será que Govinda encontrará um lar com alguma das crianças que Rodrigo maltrata?

Dependerá das ações dos Amigos do Bem que são os Strays
