---
title: "Exemplo de jogo em _#iHunt_ (18+)"
teaser: "As Caçadoras Trovadoras vão procurar uma recompensa levadas pela dor de corno de um farialimer"
layout: post
date: 2022-10-10
categories:
    - RPG
header: no
excerpt_separator: <!-- excerpt -->
tags:
  - Fate
  - Fate-Core
  - Aventura
  - Exemplo de Jogo
  - iHunt
  - ExemploDeJogo
---


> Valdeci está com seus amigos jogando uma caçada em #iHunt, onde os personagens são pessoas que, pelas mais diversas razões, precisam de dinheiro para sobreviver e fazer as coisas girarem, e para isso cortam um dobrado caçando monstros por aplicativo. Afinal de contas, _there's an app for that_, mesmo quando isso é caçar monstros
> 
> No caso, os jogadores são de [um grupo de animadores de festa infantil](http://fabiocosta0305.gitlab.io/rpg/CacadoresTrovadores/) que caçam monstros para ajudar a coisa a se manter (se as pessoas soubessem o quanto custa um bom Mickey). Eles são:
> 
> + Débora, que criou uma 66, [Helena](http://fabiocosta0305.gitlab.io/personagens/HelenaTobias/), uma mulher já de mais idade que entrou para o #iHunt para ajudar a pagar as contas dos tratamentos da mãe para Alzeimer e AVC, além de manter a companhia de festas infantis que mantém as contas em dia no fim das contas e um conjunto de imóveis dos pais, artistas de circos famosos no passado, no edifício Copan, que acaba servindo de centro de operações da equipe
> + Renata, que criou a FUI do grupo, [Célia](http://fabiocosta0305.gitlab.io/personagens/CeliaGuerreiro/), alguém que ainda sonha em voltar para o automobilismo, seja como pilota ou engenheira. Enquanto isso, ela estuda na FEI e ajunta dinheiro, seja no #iHunt, seja nas maquetes e estruturas que Helena pede para as Festas
> + E Guilherme, que trouxe uma opção diferente: [Esperanza](http://fabiocosta0305.gitlab.io/personagens/EsperanzaGuanaes/), a Malina do grupo, é uma pessoa agênero, tão confortável nesse quesito que ela aceita o pronome que lhe é mais conveniente no momento. Uma linda princesa ou um nobre príncipe é a forma pela qual ela paga as contas no fim do mês, em parte com a ajuda das festas que Helena a escala, em parte com as caçadas que ela faz no #iHunt.
> 
> O grupo está caçando uma Demônio que o grupo suspeita ter seduzido a esposa de um bacana do Cerqueira César. O mesmo atualmente vive em um _flat_ na Paulista, e tem certeza que _"aquela biscate que está com minha mulher é um demônio! Eu quero o cy daquela vaca!"_
> 
> Um babaca ciumento com dor de corno...
> 
> ... disposto a bancar uma caçada no #iHunt valendo 1500 dólares!
> 
> Bem... Mesmo sabendo que eles vão ter uma dor de cabeça para fazer esse dinheiro entrar no Brasil sem problemas, eles toparam a caçada, em especial porque na última festa uma criança quase destruiu a cauda da Ariel que Esperanza estava usando.
> 
> E após coletar algumas pistas, eles decidem dar uma olhada na casa da mulher.

--- 

<!-- excerpt -->

**Valdeci:** Então, depois de irem até o _Stardust_ e encontrarem a Arqueduqueza Demoníaca de São Paulo Sant'angelo, vocês descobrem que ***Realmente*** as duas ainda moram na suíte do tal Ricardo tinha no Cerqueira César. Como o cara parece um grã-fino cheio da nota, ele preferiu sair da casa sem alarde, fazendo a egípcia para que ninguém notasse uma possível separação. Vocês estão indo para lá ver, pelo que entendi? E a chave?

**Débora (como Helena):** OK... Vou mandar uma DM no #iHunt para o Ricardo e dizer (fazendo como se digitasse e dizendo na voz de Helena) _Meu caro, precisamos dar uma checada na sua casa... Será que dá para você facilitar a nossa vida? Estamos na nossa_ van _e podemos passar a placa e tal, mas precisamos da chave._

**V:** Bem... Vamos ver se cola... Rola teu _Organizador_ e eu rolo pelo Ricardo... Ele parece que anda de mal humor nos últimos contatos. Isso conta como _Criar Vantagem_

> Débora rola os dados e consegue um `0--+`{: .fate_font}, o que resulta em -1 nos dados. Somado ao nível _Grande (+4)_ de Helena, resulta em um _Bom (+3)_ nos dados
> 
> Por sua vez, Ricardo tem um _Influencer_ _Razoável (+2)_, mas consegue um `00++`{: .fate_font} nos dados. Com esse +2, sobre para _Grande (+4)_, portanto provocando uma Falha no teste

**V:** O cara só manda uma mensagem dizendo _Se virem_

**Guilherme (Esperanza):** Vejo isso e digo _Cuzão... A gente não vai roubar porra nenhuma!_

**Renata (Célia):** _A coisa então é simples... Vamos abrir caminho ao nosso jeito: a gente arromba e já era._

**D (Helena):** _Eu bem que gostaria, mas esses prédios de alto valor são cheios de biometrias e câmeras, não vai ser fácil entrar... Se bem que vamos ver se damos sorte. Tem um amigo meu da época do circo que tá trabalhando lá se lembro bem._ Eu coloco o celular no viva-voz e ligo para um contato meu. Afinal de contas, eu sou uma ___Pessoa que Conhece pessoas___. (Ela diz, oferecendo uma tampa de cerveja que estão usando como PDs)

> Em #iHunt Manobra é o nome dado às Façanhas
> 
> A Manobra de 66 de Helena ___Pessoas que Conhece Pessoas___ indica que, ao adicionar um detalhe narrativo em jogo, se for uma pessoa, essa pessoa será um _expert_, com nível _Grande (+4)_ em uma Habilidade (Perícia no Fate Básico) ou _Boa (+3)_, mas com um grupo de seguidores que irá ajudar. Dura uma Cena e tem uma invocação Gratuita.

**V:** E quem é esse cara que você conhece?

**D:** É o segurança do prédio onde o Ricardo mora, o Grande ___Toninho, filho do Palhaço Puxa-Puxa___, que agora é segurança de prédios já que nunca se deu bem com circo ou crianças. 

**V:** Tá pensando que ele tenha _Guerrilheiro Grande (+4)_?

**D:** Nah... Acho que o lance dele é que ele tem _Organizador **Bom (+3)**_, mas ele tem um grupo que trabalha com ele de segurança: coisa séria e fina. 

**V:** Parece justo... (Como Toninho) _Ei Helena... Há quanto tempo? O que tá pegando?_

**D (Helena):** _Oi Toninho! E teu velho, tá legal? É o seguinte... Eu queria saber se você pode me fazer um favorzinho. Tou investigando um lance para um bacana que mora aí no teu prédio, sabe... Para o aplicativo... E o cuzão não quis liberar a entrada. Parece que envolve uma treta com a esposa._

**V (Toninho):** _Tá falando do Ricardo Maldini? Aquele arrombado? O cuzão merece levar um par de chifres daqueles, o que tá levando é pouco. Tentou me foder na última assembleia, achando que eu tava liberando o carro da gostosona que tá colando o velcro da esposa dele. A mulher dele foi quem deu a_ tag _e registrou a placa do carro para a gostosona entrar no prédio e usar vaga de garagem, o que posso fazer? Normas do condomínio, ela usou uma vaga de estacionamento e não posso fazer nada... Olha... Aqui entre nós... Hoje tá rolando uma festa aqui, confraternização de fim de ano dos funcionários... Será que não rola de vocês fazerem algo para alegrar as crianças no processo? Só para não ralar totalmente o peito, sabe. Uma mão lava a outra._

**D (Helena):** _Só um minuto! Esperanza, a gente tem algum vestido de princesa aí? O cara só quer que a gente banque um tempo na confraternização dos funcionários do prédio para liberar o acesso._ 

**G:** Bem... Como sendo ___Príncipe_ E _Princesa eu pago minhas contas___, acho que deve ter algo dentro da _van_, certo? Afinal de contas, saímos de uma festa para cá, apesar de estarmos varados da noite, nem tivemos tempo de passar na _Alegre Trovadores_.

**V:** Me parece legítimo... Mas vocês estão só o bagaço... E vai só uma princesa? 

**D:** É o que tem para hoje, não tenho como chamar ninguém para ser o príncipe dela.

**V:** OK... Se quiser pagar um PD, Guilherme, pode ser... Mas se quiser aprimorar, sempre pode usar sua Manobra ***Hoje sou essa Pessoa***

**G:** Verdade... Vou usar isso então. Vamos ver se meu _Influencer_ ajuda

> A Manobra ***Hoje sou Essa Pessoa*** permite que Esperanza, _uma vez por sessão_, crie um Aspecto mostrando que ela consegue se passar por alguém de maneira quase perfeita. Valdeci pode incluir uma _Dificuldade_ que mostra algo errado nela e que pode ser notado, mas o resultado do rolamento contra _Medíocre (+0)_ indica tanto a dificuldade para Superar a farsa quando o número de invocações Gratuitas que ela recebe no Aspecto.
> 
> Guilherme rola um `0-++`{: .fate_font} para +1, que somado ao seu _Influencer Bom (+3)_ garante um _Grande (+4)_ no teste.

**G:** Ainda assim vou Invocar meu Aspecto para somar +2 ao rolamento e com isso subir para _Fantástico (+6)_. Pode ser? Então vamos colocar a ___Mais Bela de Todas, a Branca de Neve___ em jogo!

**D (Helena):** _Vou pedir que você evite maçãs aí, pois estou com a Branca de Neve aqui no carro! Estamos a caminho: só me passa a entrada de serviço e o resto damos o nosso jeito. E pede para alguém esperar lá para receber a Princesa._

**V (Toninho):** _Beleuza, Cleuza! Te mando no Zap o endereço. E se conheço você, deve ser realmente a Mais Bela de Todas! Até daqui a pouco!_ (Normal) Enquanto vocês se dirigem para o local, vocês notam Esperanza se trocando...

**G:** ...e passando desodorante e perfume, já que estamos a bastante tempo sem banho, e não quero dar a impressão de estar com cê-cê... 

**V:** E ela se ajeita, ainda que seja um tanto estranho uma _**Branca de Neve Altona**_

**G:** Fazer o que? Tem coisas que infelizmente AINDA não tenho como mudar.

**V:** E quando vocês chegam, Helena, o próprio Toninho foi lhe receber na entrada de serviço. Ele não é fortão como as pessoas imaginariam de um segurança, mas dá para ver que ele não é um magricela qualquer. E ele fica abismado com Esperanza! (Toninho) _Puxa! Ela é realmente a Mais Bela de Todas! Onde você encontrou essa atriz?_

**D (Helena):** _Outra hora, Toninho. Marca um dia no Bar do António e a gente conversa. Agora, Branca... Pode acompanhar nosso amigo para alegrar algumas crianças?_

**G (Esperanza como Branca de Neve):** _Mas é claro, minha cara, e espero que tenham sucesso no nosso intento._ Esperanza diz, oferecendo a mão para que Toninho a guie.

**R (Célia):** _Vamos lá então... Qual é o andar dele mesmo?_

**D:** Esqueci desse detalhe... Posso usar a invocação Gratuita do Toninho para dizer que ele me passou essa informação? A garagem desse prédio deve ser enorme... Sem falar que a mulher não deve estar em casa se o que a Sant'angelo disse é verdade: ela disse que ela e a demônio ficam horas após a noite na _Stardust_ em um motel chique "brincando", por assim dizer.

**V:** OK... Normalmente não seria tão simples, mas vou deixar essa. Ele passa que o apartamento da mulher é o 2204. Vigésimo Segundo andar, e um dos mais chiques e caros do prédio. É uma suíte grande pelo que vocês sabem, e certamente não será simples entrar, já que provavelmente tem biometria...

**R:** Célia tem o ___Estoque de FUI___ dela... Não seria o caso de ela ter as ferramentas para arrombar uma porta dessa? Sei que parece meio forçado e tal...

**V:** Tá tudo bem. Funciona para mim. Vocês sobem e entram no apartamento...

**D (Helena):** _Caralho... Esse apartamento é maior que os três lá do Copan, lembrando que um deles é nosso depósito e o outro é o escritório da **Alegre Trovadores**... E eu me aperto no último com a minha mãe entrevada na cama._

**R (Célia):** _Tua mãe é guerreira que nem tu, ela sabe que você tá fazendo das tripas coração para manter ela e tudo que sua família deixou sobre circo. Vamos dar um jeito nessa demônio e depois pensamos nisso._ Quero olhar a casa em geral

**V:** A primeira coisa que vocês notam é que toda a mobília é chique e de bom gosto, parece aquelas que aparecem em revista de decoração. Além disso, tudo caro: TV plasma gigante e quase sem borda, _home theater_ Bang & Olufsen, quadros de natureza morta. Tudo muito bonito, ostentando mas sem deixar de ser minimalista. 

**R:** Será que não tem umas pistas mais fortes por aí? Posso testar _Investigador_ para notar alguma coisa?

**D:** Eu ajudaria Célia.

**V:** Muito bem... Quem tem o melhor _Investigador_?

**R e D:** Temos ambas em _Razoável (+2)_

**R:** Você rola então com +1 de _Trabalho em Equipe_

> Quando um personagem sacrifica sua ação para ajudar outro, ele oferece ao mesmo +1 em _Trabalho em Equipe_, ***DESDE QUE*** ele tenha a mesma Habilidade em valor igual ou superior a _Média (+1)_
> 
> Renata rola os dados e consegue `0+0+`{: .fate_font}, o que somando ao _Razoável (+2)_ e ao bônus de Trabalho em Equipe dá um _Excepcional (+5)_. Ainda não é o suficiente para descobrir nada sobre a demônio, ainda mais por causa do Nível de Ameaça 3 Estrelas que faz a dificuldade de qualquer teste contra o mesmo ser de no mínimo _Fantástico (+6)_ (2 vezes o número de estrelas)... Mas dá para oferecer umas pistas sobre Ricardo e sua Esposa...

**V:** Quer manter esse resultado?

**R:** Olha, quer saber? Célia ___Sempre tem que provar duas vezes mais___ e ela ___Não é apenas uma mulherzinha___. Vou usar 2 PDs e quero subir em +4 o rolamento! Ela tá apostando tudo nesse trampo, já que pode ser uma grana para um curso bacana para ela melhorar ainda mais como mecânica de competição! Isso é suficiente?

> Valdeci sorri... Isso é mais do que o suficiente, até para obter mais pistas sobre a demônio

**V:** Mais do que o suficiente... Você decide ir para o quarto já que, se tem dor de corno na jogada, a cama é sempre a culpada na cabeça do homem. O quarto, como o resto da casa é aparentemente sóbrio e bem decorado. Entretanto, conforme você abre as portas dos armários, você nota uma coisa estranha: uma certa quantidade de fotos impressas, o que não é muito comum nesses dias de WhatsApp e o escambau, guardada dentro de uma velha caixa de sapatos, viradas para baixo. Além disso... Você sente um leve cheiro de enxofre no ar, mas não vê nenhum objeto que, até onde você sabe, serviria de invocação para um demônio...

**R:** Célia quer olhar as fotos.

**V:** Célia, você fica passadassa ao ver as fotos: parece que o Ricardo é tigrão com #iHunter mas é tchutchuca com a esposa! O cara é subimisso pra caralho, e rola um fetiche de _Crossdressing_ e _Sissydom!_ E piora! A demônio de quem ele tava reclamando aparece junto com a esposa, o dominando em um vestidinho de Alice no País das Maravilhas mega curto, a esposa de Rainha de Copas e a demônio de Coelho Branco.

**R (Célia):** _Caray Biridim! Helena, vem ver isso aqui. O bagulho é louco demais!_

**D (Helena):** Helena vê as fotos e diz _Puta merda! O Cara é maricas! Sissyzinha mesmo. Já cruzei com algumas ali perto do Copan!_

**R (Célia):** _Será que o lance é esse? De repente a esposa tá extorquindo o cara? Ou o cara quer dar um jeito na esposa para impedir ele de se foder em uma possível separação?_

**D (Helena):** _Não sei... Mas se a demônio está envolvida, provavelmente foi um deles quem invocou a mesma. Qual deles? Droga... Não achei nada que lembrasse algo para invocar demônio e a Esperanza tá lá embaixo... Ela poderia ajudar mais aqui, já que ela manja mais desses paranauê._

**V:** Aproveitando o Gancho... Esperanza, você vê que as crianças ali são tudo pobre, mais parecida com a molecada do Guilhermina-Esperança onde você mora agora do que com os fedelhos riquinhos do prédio. Elas ficam admiradas com você enquanto você dança e interage como Branca de Neve, sem fazer a mínima questão para qualquer outra coisa, como seu sexo biológico ou altura ou raio que o parta. Quase uma hora de interação que valem por horas de festas em salões chiques. A festa está perto da entrada social e do estacionamento, em um salão de festa menor que o da cobertura. E nessa hora você vê a demônio chegando: ela traz a esposa do Ricardo por uma gargantilha e uma correia. 

**G (Esperanza):** Eu faço um (como Branca de Neve) _Oh... Eu sinto muito, mas tenho que ir embora... O Dunga ama as tortas que eu faço, e está quase na hora de eles voltarem da mina, e eles não sabem se cuidar sem minha ajuda! Mas foi um momento maravilhoso com vocês e espero que vocês tenham adorado tanto quanto eu..._ Eu aceno com a mão me despedindo e me dirijo para fora do Salão. Vou usar uma Invocação como ___Branca de Neve___ só para o Toninho não ficar bravo comigo.

**V:** As crianças fazem um _aaaahhhh_ e algumas até tentam lhe abraçar novamente, mas as mães sabem que é hora de ir. Toninho olha para você e diz: _Obrigado, Branca de Neve, e você será sempre bem vinda aqui._

**G:** Eu pisco para Toninho em agradecimento e saio... Minha ideia é tentar acompanhar as duas, ou ir pelo elevador de serviço até o andar delas.

**V:** Infelizmente o rolamento para você tentar se antecipar a elas é contra uma dificuldade _Fantástica (+6)_. Rola seu _Trambiqueiro_ para ver se consegue.

**G:** Basicamente impossível...

> E foi mesmo! Guilherme rola `00--`{: .fate_font}, o que basicamente anula o _Trambiqueiro_ Razoável (+2) de Esperanza!

**G:** OK... Perdido por um, Perdido por mil... Vou de Sucesso a Custo!

**V:** Beleza... Então... Quando você sai da festa e vai na direção do elevador, a demônio te observa e nota sua presença... Ela apenas aponta para você e faz um gesto indicando para a acompanhar no elevador, meio que com um sorrisinho irônico e cansado do tipo _"Venha ou pague as consequências"_ e de quem já viu esse filme repetir-se algumas milhares de vezes.

**G:** *GULP* Bem, Esperanza não tem muito o que fazer nesse caso!

**V:** A sorte... ou azar... para você é que ninguém repara em vocês entrando no elevador. A demônio, que parece quase uma _cosplayer_ da Annie Lennox em _Sweet Dreams (Are Made of This)_ do Eurythmics, então volta-se para você e diz, sem muitos rodeios (Demônio) _Bem... Acho que você é parte de mais um grupo de caçadores atrás de mim e da minha querida aqui. Então, vamos direto aos negócios. Não sou de firulas: quero que você me diga quem está com você e como eles se comportam e quais são os objetivos de vocês. Ou você sempre pode se sacrificar pelos seus amigos, você decide. Mas devo dizer que vai ser uma pena perder alguém que ainda está em uma crisálida como você._ (Valdeci termina, oferecendo 1 PD). Estou no caso Apostando seu Quadro dos Sonhos, Esperanza, já que ___Família é você quem faz, não necessariamente de onde vem___. 

**G:** Caray! Pesadão! Então as opções são: ou eu entrego todo mundo e dou uma chance mínima de nos salvarmos de alguma forma, mas com a Demônio já sabendo quem é quem e podendo foder a gente de verde e amarelo, ou então eu fico quieta e Esperanza roda sozinha?

**V:** Sim... É por sua conta.

**D:** Pode revelar, Guilherme... Não tem porque arriscar tudo agora.

**V:** Sabem que basicamente isso coloca qualquer chance de obterem a Vantagem contra ela no lixo, né?

**D:** Sabemos... Mas depois do lance do cara ser tchutchuquinha da esposa, algo me diz que tem muito mais na jogada do que o babaca quer assumir... De repente, uma negativada no #iHunt não parece tão ruim quanto a outra opção.

**G:** Bem, então decido revelar. (Esperanza) _Tá legal! Não tou sozinha. É eu e mais duas amigas. A gente se enturmou depois que a Helena entrou no_ app _para ajudar a pagar as contas da doença da mãe e manter as lembranças dos pais da era dos circos._

**V (Demônio):** _Interessante..._ Ela diz e aciona o botão de travar o elevador _Agora, quero que me conte tudinho sobre suas amigas. E dependendo do quão boa for a sua história, eu não voui engrossar... Mas anda logo: estou estressada e não tenho muito tempo!_

**G:** Eu conto todinha a história da gente, da melhor, mais rápida e mais séria forma possível... Será que rola de eu usar Invocações da ___Branca de Neve___ para melhorar os meus dados? Ainda estou como Branca de Neve, posso usar um pouco dos trejeitos para amaciar...

**V:** Sim... Quer usar tudo de uma vez?

**G:** Primeiro vamos ver o que os dados apontam... Sei que como é algo que tem a ver com a sobrevivência dela, certamente você vai usar a Vantagem, certo? Mesmo considerando que ela tem o nível de ameaça a favor dela...

**V:** Sim... Agora é para pegar pesado mesmo, então não posso facilitar... Vou com o Dado de Vantagem

> Em #iHunt, existe a regra da Vantagem, onde quem tem a mesma rola um dado comum de 6 faces no lugar de um dos 4 dados Fate...
> 
> E não que mude muito a coisa... Os dados Fate foram `+0+`{: .fate_font}, com um `4` no dado de 6 faces.... Como ela tem um _Sobrevivente Bom (+3)_, o resultado final é _Acima de Lendário (+9)_

**G:** _Yikes! Vou usar meu Influencer, e depois decido o quanto eu pego das Invocações Grauítas..._

> E Guilherme consegue um `0+0+`{: .fate_font}, somando com seu _Influencer_ _Bom (+3)_ vai para um total de _Fantástico (+6)_... Ainda não o suficiente...

**G:** Vou queimar 3 Invocações da Branca de Neve... A ideia é que faço a coisa toda BEM melosa, do tipo tão açucarado que ou ela vai amar, ou ela vai pedir para parar antes de ela vomitar. Isso eleva para 12 com um _Sucesso com Estilo_...

**V:** É... Pode ser, não compensa pagar PD de cena para ferrar tudo. A demônio então diz (Demônio) _Um mistério bem interessante esse... Então a nossa princesinha não aguenta o tranco sobre seus próprios desejos e quer dar um jeito em mim? Kaamen, a xerife? Ele não aprendeu quem é a Mestra dele? Mas é um bananão mesmo! Com muito dinheiro, mas um bananão ainda assim. Eu estou de saco cheio desse cara: meu lance na real nunca foi com ele, mas com a Alice aqui..._ Nessa hora você nota que Alice muda para uma forma demoníaca, com um rosto que parece a Angelica Houston em _Convenção das Bruxas_!

**G (Esperanza):** _Eita porra! Ela é um demônio também? Sem ofensas, claro!_

**V (Kaamen):** _Sem problema. Ela é que na verdade era meu alvo: algum senhor infernal sentiu falta de um brinquedinho, no caso ela, e me mandou vir buscar. Ela tava bem escondida, sinceramente, mas parece que as brincadeiras dela com o Ricardo ficaram pesadas e ela destruiu o que a protegia das detecções no processo. Aí entrei na roda: descobri ela e a princesinha em uma casa de Swing aqui perto. Dou o endereço depois. Lugar bacana se precisar para alguma brincadeira consensual entre adultos: discreto, grande, confortável, com todos os brinquedinhos que você possa imaginar, uma masmorra que dá quase saudade de casa... Mas, o lance foi esquentando e gostei da Alice aqui: ela sabe dos lances, já que ela obtêm sua energia da Dor de Corno da nossa princesinha... E outras cositas mais._

**G (Esperanza):** _Então basicamente ela colocou o cara no BDSM e tava mandando ver e se alimentando dele? OK... Me parece bem válido. Mas e agora? Se entendi, você tá aqui para levar ela, e tá com ela, portanto, o que te impede de simplesmente dar linha na pipa?_

**V (Kaamen):** _Você é bem novata com demônios, né? Eu sinto uma aura diferente em você, por isso disse da crisálida. Você conhece alguma coisinha de magia, mas ainda não saca quase nada. Deixa eu te dar um outro agradinho além do meu nome: não é assim que o lance funciona... Eu só posso ir embora desse muquifo que vocês chamam de realidade em certas condições especiais, no local e momento certo. Ou ser exorcizada, o que é uma merda, é que nem andar na Linha Vermelha do metrô no fim de uma tarde de sexta... Na prática, meu tempo aqui está realmente se esgotando, tenho que voltar para casa, mas como ainda tenho que ver como vou ficar com meu novo brinquedinho e não o dar para o arrombado que a está esperando no Andar de Baixo, vou curtindo e pensando enquanto isso. Fosse de outra forma, já teria descido: vocês me divertem até certo ponto, mas estou cansada. Quero chegar em casa, sabe como é? Encostar a cabeça no travesseiro depois de um serviço bem feito?_

**G (Esperanza):** _Entendo... Então você quer ir embora de maneira discreta?_

**V (Kaamen):** _É claro... Tenho uma reputação a zelar, e não vai ser um bando de #iHunters que vai sujar ela! Imagina a Sant'angelo contando para todo mundo no Inferno que fui expulsa por um bando de caçadores mequetrefes usando uma faca de bolo Pullman, água benta aspergida de um borrifador comprado na 25 de Março e um pacote de sal Cisne._ Ela diz, e vocês voltam a subir, e o elevador para no Andar onde fica a casa da Alice. _Quando entrarmos, quero conversar com a tal Helena, a líder de vocês... Podemos fazer algo muito lucrativo para ambas as partes, se ninguém mijar fora da bacia... Mas um detalhe: sobre o meu nome, não deve o revelar para **NINGUÉM** em absoluto. Essa foi uma cortesia minha, e espero que entenda que Nomes não são coisas para se brincar._

**G (Esperanza):** _Tudo bem... Não pretendo usar isso mesmo até segunda ordem._ Isso quer dizer que...

**V:** Que o Impulso que você teve no rolamento na verdade é um Aspecto sem Invocações Gratuitas com o ***Nome Verdadeiro de Kaamen***. Agora... Helena, Célia, vocês veem Esperanza entrando com as duas, a mulher e a demônio.

**R:** Célia já fica pronta para sair no braço caso a Demônio tente ferrar Esperanza. (Célia) _Espe, o que aconteceu? Elas te..._

**G (Esperanza):** _Não! Elas querem apenas conversar! Não as ataque... Parece que é o cliente que é um babacão._

**D (Helena):** _Descobrimos! O cara é uma tchutchuquinha da Alice... Submisso e mariquinha!_ Helena diz, segurando as ___Fotos Constrangedoras de Ricardo___ que achamos.

**V:** A Demônio dá um sorriso e diz (Kaamen) _Acharam só as fotos? Se quiserem depois, posso mostrar os brinquedinhos e fantasias do cara, diversão garantida... Eu suponho que você seja a Helena. Quero conversar sobre uma proposta._

**D (Helena):** _OK... Célia, senta aí... Vamos ouvir o que ela tem a dizer. Ela poderia ter matado Esperanza facilmente, mas parece que não é o interesse dela._

**V (Kaamen):** _Sim... Ela foi bastante sábia em me contar as histórias de vocês três, em um resumo suscinto e divertido... Isso é um mistério, mas se tem uma coisa que adoro demais é mistérios! Me contem então se o que ela me disse é verdade: que você Helena caça monstros pois tem uma mãe doente e quer preservar as coisas da arte de circo que sua mãe tem, e que Célia ainda quer voltar para Interlagos como engenheira da Stock Car_

**R (Célia):** _Sinceramente, tou topando até Copa Truck ou TCR se me derem uma chance._

**D (Helena):** _Sim... Esperanza disse a verdade. Não tem porque a gente esconder. E não a culpo, Espe, por ter contado isso para ela. Talvez tenha sido até bom: você evitou sua própria morte e provavelmente a nossa também. Ela provavelmente chegaria pistola e vendo nós duas, seria apenas uns poucos segundos até isso aqui parecer cenário de Hellraiser._

**V:** Ela levanta a sobrancelha e diz (Kaamen) _Não me confunda com aqueles idiotas... Particularmente preferiria algo ao estilo O Chamado ou algo mais que nem os filmes de horror japoneses: adoro a estética do horror oriental. Se é para matar, que seja de uma maneira que faça todo mundo que ver a foto não querer se meter a besta com você. Mas... Voltemos então aos negócios... Quanto a princesinha ofereceu para vocês no #iHunt?_

**D:** Helena abre o aplicativo e mostra as informações da caçada

**V:** A demônio ri (Kaamen) _Hahahahahahahah! 1500 dólares? Isso é quase um insulto! Se vocês fossem #iHunters mais experientes, saberiam que isso é uma forma de ter uma Luta Justa facinho... Isso até é divertido. A princesinha além de tudo é muquirana, ou deve estar poupando para alguns brinquedos novos. Mas... Olha só: como disse para sua amiga crisálida aqui, eu vou me mandar logo logo... Hoje a noite, para ser mais exata. Não quero confusão, não quero barulho... Para mim, no tipo de serviço que faço para as Hostes Infernais, discrição e eficiência é tudo! Minha proposta é simples: cubro o valor, dobro e ainda coloco a chave do meu carro na brincadeira. E ainda faço as caras e bocas para adicionar mais uma calúnia contra a princesinha, para vocês mandarem a foto comprovando o serviço. Assim que o sol se por, a gente vai para algum lugar discreto e cumpre esse acordo. Que tal?_ Ela diz, mostrando a chave de um carro da Lexus!

**D:** Eita caceta! Essas porras não saem por menos de 250 mil! Isso faz o pagamento do #iHunt ser quase um troco de pinga!

**R:** Demorou! Partiu negócio?

**G:** Por mim beleza... Ainda mais terminando tudo e colocando uma naba do tamanho da do Chuck Norris na raba do arrombado do Ricardão, para mim perfeito!

**V:** Bem... Enquanto vocês terminam de negociar, eu Chamo o Aspecto do ___Toninho, Filho do Palhaço Puxa-Puxa___ para complicar a situação! (oferecendo 1 PD para cada um) A ideia é que o corno tá chegando!

**G:** PQP! Logo agora?

**V:** `#sendoumfodido`, fazer o que?

**D:** Vamos topar... Podemos aproveitar e usar isso a nosso favor, em especial agora que a Kaamen quer se mandar discretamente, e provavelmente vai nos ajudar para tal.

**V:** Débora, enquanto vocês tão negociando com a Demônio, o teu celular toca... É o número do Toninho! (Toninho) _Helena, é Toninho! Pica a mula! Chegou o arrombado do Ricardo com uns caras esquisitos! Parecem seguranças particulares... Vocês devem ter acionado algum sensor ou alguma coisa e ele percebeu a presença de vocês aí... Os caras vão levar uns poucos minutos, tão indo pela escada para não chamar a atenção. Certeza estarem armados, só estão discretos. Só corre! Boa sorte!"_

**D (Helena):** _Cacete! A tchutchuca tá vindo aí, provavelmente com os bofes dela! Ele tá vindo com uns seguranças. Vamos nos pirulitar!_

**V (Kaamen):** _Beleza... Vamos sair pela escada de incêndio..._

**D (Helena):** _Eles estão vindo por lá! Melhor irmos pelo elevador... Com sorte não chamamos a atenção e escapamos antes de eles chegarem!_

---

Será que essa será a Luta Justa das Trovadoras Caçadoras? Kaamen irá cumprir seu acordo ou deixará a bomba explodir na mão dos #iHunters? E Ricardo, a princesinha, como vai lidar ao descobrir que mais gente sabe do seu segredo constrangedor, que pode acabar com a carreira dele no banco? Kaamen conseguirá sua saída à francesa, ou vai ter que fazer o dedo no cu e gritaria? O que vai acontecer agora?

Tudo na próxima seção de #iHunt
