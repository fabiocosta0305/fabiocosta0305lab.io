---
title: "Oráculo Fate"
#subheadline: "Um Oráculo para ser usado em jogos _movidos pelo Fate_"
layout: post
categories:
 - Fate
 - Fate Básico
 - Fate Acelerado
 - hacks
 - minimalista
comments: true
tags:
  - Dry Fate
  - Fate-Core
  - Fate-Básico
  - Fate-Acelerado
  - Fate
  - Fate Solo
  - RPG
  - RPG Solo
  - Oráculo
language: br
header: no
do_excerpt: yes
excerpt_separator: \n<!--more-->\n
---

## Um Oráculo para ser usado em jogos _movidos pelo Fate_

+ (CC) 2019 - Por _Fábio Emílio Costa_
  + Usando o Grid Randômico por John Reiher
+ ___Downloads___
  + [Versão _offline_ desse documento](/assets/OraculoFate/OraculoFate.pdf)
  + [_Grid_ do Oráculo](/assets/OraculoFate/GridSolo.pdf)
  + [_Grid_ limpo](/assets/OraculoFate/GridColorido.pdf)
  
Oráculo Fate serve como uma estratégia para simular-se o Narrador em caso de jogos _GM-Less_ ou _Solo_, usando as regras básicas do Fate. Além disso, visa ser completamente compatível com os jogos _Movidos pelo Fate_.

### O Oráculo

Sempre que você precisar responder alguma coisa que não for óbvia em relação aos acontecimentos do cenário, você deve usar o _Grid Oráculo_ (link para download [aqui](/assets/OraculoFate/GridSolo.pdf)). Para usar tal grid, role 4dF: cada `+`{: .fate_font} puxa uma coluna para a direita, cada `-`{: .fate_font} uma linha para baixo

|                      | `0`{: .fate_font} | `+`{: .fate_font} | `++`{: .fate_font} | `+++`{: .fate_font} | `++++`{: .fate_font} |
|---------------------:|:-----------------:|:-----------------:|:------------------:|:-------------------:|:--------------------:|
|    `0`{: .fate_font} | Não, mas          | Sim, mas          | Sim                | Sim, e              | Sim, e               |
|    `-`{: .fate_font} | Não, mas          | Não, mas          | Sim, mas           | Sim                 |                      |
|   `--`{: .fate_font} | Não               | Não, mas          | Não, mas           |                     |                      |
|  `---`{: .fate_font} | Não, e            | Não               |                    |                     |                      |
| `----`{: .fate_font} | Não, e            |                   |                    |                     |                      |

Explicando os resultados do Oráculo:

+ ___Não, e:___ o evento não ocorre ou o personagem não se envolve. Além disso, uma complicação adicional pode entrar em cena (inimigos aparecem, informantes notam movimentação estranha);
+ ___Não:___ o evento não ocorre ou o personagem não se envolve. 
+ ___Não, mas:___ o evento não ocorre ou o personagem não se envolve. Entretanto, alguma pequena benesse pode ocorrer ajude um pouco (talvez permitam o descanso por algumas horas ou deixem escapar alguma migalha de informação interessante)
+ ___Sim, mas:___ o evento ocorre ou o personagem se envolve. Porém, alguma coisa pode complicar a situação (os personagens deixaram alguma coisa passar batido ou um pequeno grupo de traidores podem colocar o grupo em encrenca)
+ ___Sim:___ o evento ocorre ou o personagem se envolve sem problemas
+ ___Sim, e:___ o evento ocorre ou o personagem se envolve. Além disso, alguma coisa adicional aumenta a potência da cena (vilas vizinhas se unem aos personagens, um _backdoor_ esquecido revela novas informações)

> ___Exemplo:___ Célio está jogando uma aventura-solo de _Mestres de Umdaar_ com o personagem [_Delaware_]({% post_url Personagens/MastersOfUmdaar/2016-07-28-MestresDeUmdaar-Delaware %}), um atirador mercenário que serviu no passado os Mestres de Umdaar mas agora quer algumas respostas sobre o desaparecimento de sua família. Para obter algumas respostas, ele descobre sobre o famoso _Monastério de Alemtempo_, uma região "neutra" no conflito entre os Povos Livres e os Mestres de Umdaar, ainda que seja mais favorável aos Povos Livres. Ele decide procurar algumas informações sobre seu pai. A pergunta que pode ser necessário responder é: ___'O Monastério de Alemtempo possui alguma informação relevante?'___ Como nenhuma de suas Abordagens pode oferecer tal resposta, e isso seria algo para o Narrador verificar, ele rola contra o Oráculo e obtém `0-0+`{: .fate_font}. Movendo uma linha para baixo e uma coluna para a direita (pelos dados não brancos), ele obtém o resultado _"Não, mas..."_. Isso provavelmente irá dizer que o Monastério não possui nenhum registro significativo sobre o pai dele, entretanto pode ter alguém no vilarejo próximo que possui alguma informação. Ele descobre que um certo sábio chamado Nicodemos pode ter algumas respostas.

### Pontos de Destino e o Oráculo

Uma coisa importante: a economia de Pontos de Destino ainda é válida. De fato, o Oráculo recebe a cada cena 1 PD que pode ser usado para modificar rolamentos dos NPCs.

Além disso, Pontos de Destino podem ser usados tanto pelo jogador como pelo Oráculo para modificar os resultados dos rolamentos do Oráculo, ___maximizando___ ou ___minimizando___ o resultado do rolamento. Entretanto, como em qualquer uso de Pontos de Destino, isso deve ser feito por meio de Aspectos.

Além disso, o personagem pode Invocar de maneira hostil um dos seus Aspectos usando PDs do Oráculo para modificar os resultados do mesmo de maneira desfavorável ao personagem.

Pontos de Destino usados para Invocações Hostis são recebidos pelo personagem ao final da cena normalmente, conforme as regras do Fate.

> + ___Maximizar___ o resultado implica em transformar qualquer dado não-positivo em um `+`{: .fate_font}.
> + ___Minimizar___ o resultado implica em transformar qualquer dado não-negativo em um `-`{: .fate_font}.
> + Cada Ponto de Destino utilizado para _Maximizar_ ou _minimizar_ resultados do Oráculo transforma 1 dos 4dF usados na direção desejada. 
> + Além de todas as condições normais para o uso de Pontos de Destino, não é permitido utilizar mais de 2 Pontos de Destino desse modo, e sempre na mesma direção. 
> + Entretanto, se usado Pontos de Destino tanto do personagem quanto do Oráculo, é permitido que eles vão em direções opostas. Nesse caso, apenas Maximize ou Minimize a diferença de dados, mantendo os demais dados como estavam.

> ___Exemplo:___ em determinado momento da aventura, Delaware, como guarda-costa de Nicodemos, enfrentou uma _Tropa de Subordinados dos Mestres de Umdaar_. Ele decide então ver se por um acaso ___tais tropas não possuem pistas sobre o paradeiro de seu pai___. Rolando os dados contra o Oráculo, ele obtêm `0++0`{: .fate_font}. Mas ele decide que não é suficiente: ele invoca sua Motivação ___Devo descobrir o motivo do desaparecimento de minha família___ e com isso maximiza um dos dados Brancos para um terceiro `+`{: .fate_font}. Com isso, ele melhora o _"Sim"_, para um _"Sim, e"_. Ele descobre que seu pai deserdou por alguma razão o Mestre que ambos seguiam, e que é considerado foragido e _Persona non grata_ para ser abatida na primeira oportunidade. Além disso, ele descobre que a última pista para o paradeiro do mesmo é a região para qual ele e Nicodemos estão indo, o ___Vale Verde, lar do povo-coelho Usato___

> ___Exemplo 2:___ Ao chegar no Vale Verde, Célio decide ver se ___os Usato são Hostis a Delaware___. Ao rolar os dados do Oráculo, ele obtém um `00-0`{: .fate_font}, com um _"Não, mas"_. Entretanto, pode ser uma boa hora para complicar um pouco a vida de Delaware. Para isso, ele faz o Oráculo invocar de maneira hostil o Aspecto Pessoal de Delaware ___Passado como parte das tropas dos Mestres de Umdaar___, maximizando o `-`{: .fate_font} para um `+`{: .fate_font}, tornando o resultado do Oráculo um _"Sim, mas"_. Com isso, ele vê que os homens-coelhos focam o tempo todo em suas roupas, basicamente as partes que ainda são úteis do antigo uniforme que usava como tropa dos Mestres de Umdaar, e ficam preocupados, alguns até mesmo com as pernas prontas para atacar ele. Entretanto, a presença de Nicodemos é a garantia de que Delaware não vai ser escorraçado de imediato na hospedaria onde passarão a noite, na entrada do Vale Verde, ainda que mensageiros venham e vão com a notícia sobre ele para os Sábios da região.

### Contornando o Oráculo - Descrição narrativa

Se a qualquer momento perceber-se a necessidade de um rolamento do Oráculo, o jogador pode utilizar um Ponto de Destino para _Invocar_ algum Aspecto dele para uma Descrição Narrativa.

> ___Exemplo:___ Delaware está um pouco cansado de ser visto com suspeita pelos Usato, e logo ele passa por uma nova situação onde os ___Usato suspeitam de Delaware___. Ao invés de deixar por conta do Oráculo, ele decide gastar 1 Ponto de Destino para usar sua _Motivação_ ___Devo descobrir o motivo do desaparecimento de minha família___ para explicar que ele deserdou os Mestres. Ao mencionar o nome de seu pai, ele percebe que os Usato abaixam as orelhas em uma posição de tristeza. Um dos sábio, Shuto-Hay, mostra um peitoral similar ao que ele usava, chamuscado de disparos de _laser_. Ele reconhece alguns símbolos e glifos, mostrando o nome de seu pai. Shuto-Hay caminha com ele até um jardim, com um pequeno monte de terra e uma espada cravada no chão. Delaware descobre então que seu pai morreu defendendo os Usato, após negar-se a seguir as ordens de destruir o Vale Verde. Shuto-Hay termina dizendo: _"Seu pai sempre falava de você, e esperava que você seguisse sua própria vida..."_ Delaware decide que vai seguir sua vida... Assim que explodir a cabeça do Mestre de Umdaar com seu rifle de energia.

### Forçando Aspectos

Na mesma forma, o personagem pode Forçar seus Aspectos para adicionar um complicador à história sem precisar utilizar o Oráculo normalmente, conforme as regras do Fate. Lembrando que Forçar um Aspecto muda sobre o que se trata a cena.

> ___Exemplo:___ Depois de uma série de eventos, Delaware consegue, a muito custo, acessar o topo do Mirante das Quatro Luas, uma antiga estrutura da era dos Demiurgos que, diz as lendas, rasgou a terra na região da Estrada do Mirante, separando o Vale Verde do Pântano Necrótico pela Muralha de Âmbar. Enquanto estão tentando desvendar como usar tal poder para impedir que o Vale Verde seja invadido, ele percebe que o líder das tropas dos Mestres de Umdaar que estão vindo é ninguém menos que seu próprio irmão, Dakota. Isso é resultado de uma Forçada no Aspecto de ___Motivação___ do mesmo, ___Devo descobrir o motivo do desaparecimento de minha família___, que o faz ficar maluco de raiva: ele agora entende que seu irmão entregou o próprio pai, ou nada fez para impedir a morte do mesmo. Ele pega as ___Asas de Cerâmica___ que tinha conseguido em certo momento no Mirante e, olhando Nicodemos e a Usato Tessa Lin que os ajudou, diz: _"Vocês cuidam das tropas como um todo, mas o líder deles é meu e APENAS MEU!"_ com fogo nos olhos, enquanto pula e ativa as ___Asas de Cerâmica___, fazendo um pequeno par de asas sair pelos lados, que ele usa para ir atacar Dakota... Sendo que esse pode ter uma posição mais favorável para atacar o próprio irmão.

### Marcos de Evolução

_Oráculo Fate_ não muda nada nos Marcos de Evolução do personagem no Fate. Como um critério de dedo, sempre que ocorrer um _"Sim, e"_ ou um _"Não, e"_, você pode considerar que um Marco ocorreu. O nível do Marco pode depender dos acontecimentos até o momento na aventura.

> ___Exemplo:___ depois de uma batalha encarniçada, Delaware mata o próprio irmão Dakota. Sentindo-se perdido, Delaware pensa o que deve fazer. Para isso, ele consulta o Oráculo perguntando-se se ___a batalha estava encerrada___. O resultado de `-0--`{: .fate_font} indica _"Não, e"_. Ao olhar ao redor, ele percebe que Dakota não era o único líder das Tropas dos Mestres de Umdaar que estão atacando o Vale Verde: na realidade, eles estão conseguindo avançar facilmente, mesmo os poderosos canhões do Mirante não sendo o suficiente para parar as vastas tropas dos Mestres. Ele olha para seu braço e percebe que a tatuagem que o marcava como um serviçal dos Mestres foi queimada por um dos disparos de Dakota (Estresse). Ele pega o sabre de energia de seu irmão e olha para seu olhar morto, mas ainda sanguinário. Ele sabe o que aconteceu: agora ele ___deve vingar a morte de sua família___. Isso torna-se parte de um Marco Menor, onde ele renomeia sua Motivação.

Se chegar um momento onde acontece um rolamento _"Sim, e"_ ou _"Não, e"_, você pode dar início a uma cena final, que pode até mesmo resultar em um Marco Maior, se for o caso.

> ___Exemplo:___ enquanto vai abrindo caminho por entre as tropas para chegar ao Mirante, ele se pergunta se ___o Mestre de Umdaar que seu pai seguia está ali___. Ele consegue no Oráculo um `0+0+`{: .fate_font}, mas ele quer sangue! Pagando 1 Ponto de Destino para usar sua recém-renomeada Motivação ___devo vingar a morte de sua família___, ele maximiza um dos dados Brancos para um `+`{: .fate_font}, tornado o que seria um ___"Sim"___ em ___"Sim, e"___. Delaware percebe que não apenas _Rhangtor, O Antigo_, o Mestre ao qual seu pai servia está lá, como está na linha de frente. Ele decide que é hora de acabar com o _Mistério Vivo_, e para isso ele corre para subir no Mirante. Tudo correndo bem, ele vai chegar lá a tempo de avisar Tessa Lin e Nicodemos, e que eles vão ter uma oportunidade de ouro para exterminar um dos Mestres de Umdaar... Em um Marco Maior para todos!

## Opcional - Preparando novos _grids_

O Oráculo utiliza um Grid Randômico para Fate, [originalmente criado por John Reiher](https://docs.google.com/document/d/1CER_TdVckXgf1ajDu6MBnkmpxc-fh02VNYuY5m1s7rk/edit). Vocẽ pode utilizar esse grid para customizar situações onde você deseje emular situações de _Hexcrawl_ ou outras situações de aparição randômica de personagens ou situações.

Abaixo segue um exemplo de _grid_ randômico limpo (uma versão para impressão pode ser [obtida aqui](/assets/OraculoFate/GridColorido.pdf))

|                      | `0`{: .fate_font} | `+`{: .fate_font} | `++`{: .fate_font} | `+++`{: .fate_font} | `++++`{: .fate_font} |
|---------------------:|:-----------------:|:-----------------:|:------------------:|:-------------------:|:--------------------:|
|    `0`{: .fate_font} | A                 | C                 | B                  | C                   | A                    |
|    `-`{: .fate_font} | C                 | D                 | D                  | C                   |                      |
|   `--`{: .fate_font} | B                 | D                 | B                  |                     |                      |
|  `---`{: .fate_font} | C                 | C                 |                    |                     |                      |
| `----`{: .fate_font} | A                 |                   |                    |                     |                      |
    
    
| ___Resultado___ | ___Chances____ | ___Percentual___ |
|----------------:|:--------------:|:----------------:|
|               A | 1 em 81        | 1,2              |
|               B | 4 em 81        | 4,9              |
|               C | 6 em 81        | 7,4              |
|               D | 12 em 81       | 14,8             |


Ao criar esse grid, quanto mais "interna" for a posição, maior a chance do resultado sair, e quanto mais "externa" for a posição, menor a chance do resultado sair. Pense nisso ao distribuir inimigo e afins. Se desejar explorar apenas determinadas situações, coloque os valores residuais como "Nada acontece" ou "Role novamente".

> ___Exemplo:___ Rafael está planejando uma aventura solo para ele de _Fair Leaves_, onde ele acha interessante que ocorra a possibilidade do personagem (e da Turma, dependendo do caso), encontre ___Fantasias Mágicas___ que possam adicionar poderes e esquisitices no comportamento dos personagens, já que trata-se de uma aventura de Halloween. Para isso ele decide criar um Grid Randômico de Fantasias que podem ser encontradas em certos baus. O resultado é o Grid abaixo
> 
>|                      | `0`{: .fate_font} | `+`{: .fate_font} | `++`{: .fate_font} | `+++`{: .fate_font} | `++++`{: .fate_font} |
>|---------------------:|:-----------------:|:-----------------:|:------------------:|:-------------------:|:--------------------:|
>|    `0`{: .fate_font} | Fada Madrinha  | Chapeuzinho Vermelho | Bailarina         | Lobo Mau                      | Merlim |
>|    `-`{: .fate_font} | Cinderela      | Palhaço              | Pirata            | Bicho (rolar em outra tabela) |        |
>|   `--`{: .fate_font} | Peter Pan      | Caçador              | Soldado de Chumbo |                               |        |
>|  `---`{: .fate_font} | Branca de Neve | Fera                 |                   |                               |        |
>| `----`{: .fate_font} | Alice          |                      |                   |                               |        |
>
> Perceba que pela distribuição, as chances de sair uma fantasia de Palhaço, Pirata ou Caçador é muito maior de sair de Alice, Merlins ou Fada Madrinha. Provavelmente essas últimas são Fantasias mais raras por oferecerem a quem a usar algum poder muito maior do que outras ofereceriam.

Uma coisa interessante, caso você deseje, é ___Encadear Grids___: isso é usado, por exemplo, em ___Mestres de Umdaar___ para gerar bioformas (raças) diferentes, em especial de Bestas Mutantes, Centauro ou Homens Bestas, para definir as "partes" dessas criaturas, como no caso de um Grifo (Águia + Leão, Besta Mutante) ou de um Homem-Escorpião (Centauro com corpo de escorpião ao invés de cavalo)

> ___Exemplo:___ No _grid_ criado anteriormente, perceba que um resultado `+++-`{: .fate_font} resulta em um _bicho_, que pede para rolar. Nesse caso, em outro _grid_ teriam as diversas espécies de animal que o personagem pode tirar, indicado abaixo:
> 
>|                      | `0`{: .fate_font} | `+`{: .fate_font} | `++`{: .fate_font} | `+++`{: .fate_font} | `++++`{: .fate_font} |
>|---------------------:|:-----------------:|:-----------------:|:------------------:|:-------------------:|:--------------------:|
> |    `0`{: .fate_font} | Elefante | Castor     | Burro  | Carneiro | Leão |
> |    `-`{: .fate_font} | Coelho   | Camundongo | Gato   | Macaco   |      |
> |   `--`{: .fate_font} | Hamster  | Cachorro   | Cavalo |          |      |
> |  `---`{: .fate_font} | Ovelha   | Lebre      |        |          |      |
> | `----`{: .fate_font} | Lobo     |            |        |          |      |

<!--  LocalWords:  Grid Reiher GM-Less backdoor aventura-solo Dakota
 -->
<!--  LocalWords:  não-positivo não-negativo Delaware guarda-costa
 -->
<!--  LocalWords:  povo-coelho homens-coelhos Shuto-Hay Tessa Lin
 -->
<!--  LocalWords:  recém-renomeada grids Hexcrawl grid bioformas
 -->
