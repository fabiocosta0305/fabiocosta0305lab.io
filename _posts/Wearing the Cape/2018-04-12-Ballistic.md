---
title: "Ballistic (Real Name: Sturm Williams)"
subheadline: A Sample Character for Wearing the Cape
language: en
layout: personagens
categories:
  - wtc
  - personagens
tags:
 - Wearing the Cape
header: no
---

## Aspects

| _**Type**_          | _**Aspect**_                                        |
|--------------------:|-----------------------------------------------------|
| **Power Aspect**    | C-Class Ajas/Jumper                                 |
| **Villain Aspect**  | Rain of punches turned human                        |
| **Trouble**         | Too Much Arrogant                                   |
| **Background**      | Whitlow's Alumni                                    |
| **Background**      | Might is Right                                      |

## Attributes

| _**Attribute**_ | _**Level**_  |
|----------------:|--------------|
| **Alertness**   | Great (+4)   | 
| **Athleticism** | Great (+4)   |
| **Physique**    | Superb (+5)  |
| **Discernment** | Average (+1) |
| **Willpower**   | Fair (+2)    |
| **Presence**    |              |

## Skills

| _**Skill**_     | _**Level**_  |
|----------------:|--------------|
| **Academics**   |              | 
| **Aid**         | Average (+1) | 
| **Deceive**     | Fair (+2)    | 
| **Drive**       |              |
| **Engineering** | Average (+1) | 
| **Fight**       | Good (+3)    | 
| **Investigate** | Average (+1) | 
| **Larceny**     | Average (+1) | 
| **Provoke**     | Fair (+2)    | 
| **Rapport**     | Fair (+2)    | 
| **Shoot**       | Good (+3)    | 
| **Stealth**     | Good (+3)    | 
| **Survival**    |              | 

## Resources

| _**Attribute**_  | _**Level**_  |
|-----------------:|--------------|
| **Contacts**     |              | 
| **Reputation**   |              | 
| **Wealth**       |              | 

## Stunts [ SFP Remaining: 3 ]

+ _Superhuman Physique x2_ (_Physique_ Epic (+7), SFP 4)
+ _Mighty_
+ _Charge_
+ _Top of the Class_
+ _Hard as Nails_
+ _Close Quarter Combat_ - Roll _Athleticism_ (_Fair_ Difficult) to 'Jump' (Teleport) nearby a target. Must be on a number of zones away the target no bigger than _Alertness_ Attribut Bonus (actually 1)
+ _Instant CQC_ - Can, paying 1 FP, use _Close Quarter Combat_ without rolling. He can do this as a reaction also.

## Stress and Consequences

| _**Stress**_ | _**Bar**_         |
|-------------:|-------------------|
| _Physical_   | `4`{: .fate_font} |
|  _Mental_    | `3`{: .fate_font} |
| _Resources_  | `2`{: .fate_font} |

| _**Consequences**_ | _**Normal**_      | _**Extra Physical**_ | _**Extra Mental**_ | _**Extra Resource**_ |
|-------------------:|:-----------------:|:--------------------:|:------------------:|:--------------------:|
| _Mild (+2)_        | `1`{: .fate_font} | `1`{: .fate_font}    |                    |                      |
| _Moderate (+4)_    | `1`{: .fate_font} | `1`{: .fate_font}    |                    | `1`{: .fate_font}    |
| _Severe (+6)_      | `1`{: .fate_font} |                      |                    |                      |
