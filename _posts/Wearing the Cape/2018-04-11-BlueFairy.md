---
title: "Blue Fairy ('Real' Name: Shelly Boyar)"
subheadline: A Sample Character for Wearing the Cape
language: en
layout: personagens
categories:
  - wtc
  - personagens
tags:
 - Wearing the Cape
header: no
---

_Blue Fairy_ is the third "active" copy of Shelly Boyar's personality. As them, she's somewhat sassy and childish, but she can be very responsible. As she was activated for Jiminy Cricket, she still needs to understand him better, but she works rightfully with him. Also, she can contact her _"quantum twins"_ Shelly and Shell, so she can gather information via them and bypass some restrictions. This way she gave intel that was used in the operation to rescue Jiminy Cricket from _Alsyf Alayat_, the Sword Verses.

## Aspects

| _**Type**_       | _**Aspect**_                                    |
|-----------------:|-------------------------------------------------|
| **Power Aspect** | A-Class Asimov (TRON)                           |
| **Hero Aspect**  | An Augmented Reality Support for Jiminy Cricket |
| **Trouble**      | No Physical Existance                           |
| **Background**   | Your own personal Blue Fairy                    |
| **Background**   | I can contact my other 'quantum twins'          |

## Attributes

| _**Attribute**_ | _**Level**_ |
|----------------:|-|
| **Alertness**   | Great (+4) |
| **Athleticism** | |
| **Physique**    | |
| **Discernment** | Superb (+5) |
| **Willpower**   | Great (+4) |
| **Presence**    | Fair (+2) |

## Skills

| _**Skill**_     | _**Level**_  |
|----------------:|--------------|
| **Academics**   | Good (+3)    | 
| **Aid**         | Fair (+2)    | 
| **Deceive**     |              | 
| **Drive**       | Average (+1) |
| **Engineering** | Average (+1) | 
| **Fight**       |              | 
| **Investigate** | Fair (+2)    | 
| **Interface**   | Good (+3)    | 
| **Larceny**     |              | 
| **Provoke**     | Average (+1) | 
| **Rapport**     | Fair (+2)    | 
| **Shoot**       |              |
| **Stealth**     |              | 
| **Survival**    | Average (+1) | 

## Resources

| _**Attribute**_  | _**Level**_  |
|-----------------:|--------------|
| **Contacts**     | Good (+3)    |
| **Reputation**   |              |
| **Wealth**       | Average (+1) | 

## Stunts [ SFP Remaining: 2 ]

+ _Extra Cores x4_  (_Discernment_ Epic +9, Bonus +4)
+ _Privilege Escalation_
+ _Wireless_
+ _Quantum Link (**Permission:** Aspect - 2 SFP):_ as she is linked to a 22nd Century computer, she can communicate with anyone linked with this system, either by Aspect or Stunt, without need to speak. Any language issues are ignored on this communication. She can also link this system to any equipment she is "download", as per _Wireless_ Stunt


## Stress and Consequences

| _**Stress**_ | _**Bar**_         |
|-------------:|-------------------|
| _Physical_   | N/A               |
|  _Mental_    | `4`{: .fate_font} |
| _Resources_  | `3`{: .fate_font} |

| _**Consequences**_ | _**Normal**_      | _**Extra Physical**_ | _**Extra Mental**_ | _**Extra Resource**_ |
|-------------------:|:-----------------:|:--------------------:|:------------------:|:--------------------:|
| _Mild (+2)_        | `1`{: .fate_font} |                      |                    |                      |
| _Moderate (+4)_    | `1`{: .fate_font} |                      |                    | `1`{: .fate_font}    |
| _Severe (+6)_      | `1`{: .fate_font} |                      |                    |                      |
