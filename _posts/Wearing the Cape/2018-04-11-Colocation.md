---
title: "Colocation (Real Name: Susan Ma)"
subheadline: A Sample Character for Wearing the Cape
language: en
layout: personagens
categories:
  - wtc
  - personagens
tags:
 - Wearing the Cape
header: no
---

> _"Empty your mind, be formless, shapeless — like water._
>
> _Now you put water into a cup, it becomes the cup;_
>
> _You put water into a bottle, it becomes the bottle;_
>
> _You put it in a teapot, it becomes the teapot._
>
> _Now water can flow or it can crash._
>
> _Be water, my friend." (**Bruce lee**)_

Being the only chinese family in the region doesn't made life easy for the Ma. They came from Kowloon for the humanities college, the father Sidney (Shi'en) Ma to give classes on Chinese language, Confucionist philosophy and so. But the little Susan Ma always felt himself the outsider: a small girl, single child, she needed to grew independently to survive somewhat.

When emigrating to US, Susan had to deal with all the bullies that picked her abnd bullied her. She discovered that _**"Be water, my friend"**_ interview from Bruce Lee and she chosed to be water, to heal or to damage, if needed.

And what helped her to cope with the bullying was a local _Sifu_ on Jeet Kune Do, Bruce Lee's martial arts. He learned exactly what Susan needed and wanted. She learned that be water means to flow and find a way out, preferably without damage, by also to save energy to strike fast, hard and full. She started to read all she could about Bruce Lee, not about the actor or the martial artist, but the philosopher: she wanted to understand why, if he had the One-Inch Punch, why he didn't used it to deal with bullies.

She understood, and she calmed herself. She became a vocal activist for the minorities rights. She then gone for the local college for a Major on Humanities, with Minors on Philosophy and Psychology. And she became a _Sifu_ by herself on Jeet-Kune Do. As part of Jeet Kune Do, she mingled Yin and Yang, East and West, trying to grow.

It was when, during The Event craziness, some local Nazi-like punks tried to destroy the academy where she trained all the time, as police needed to deal with the super-punks. After stabbing her Sifu in the chest, they gone for her.

She didn't held herself.

He became water, a tsunami to defeat those punks.

She had her breakthrough.

She discovered that she could "jump" almost to any place, and she mingled it fast with her martial style. She needed only to hit the first one to discover her now was water, but in its destructive power: she blasted a punch that turned the guy sternum into smithereens, exploding his heart, rendering dead...

The others tried to run, but Susan was more effective on contain them without killing. She brought them to Justice, but there was no way to save her Sifu: he died on bleeding, even with all efforts of her focusing on estabilizing chi flow to heal him.

She was freed as they saw it as a hate crime, the attempted manslaughter exchanged into social services.

She was found sometime after by the other guys from her team now and she happily joined the CAI team.

Susan was known previously as _Shuǐ_ (Water), but in time his _Speed of the Dragon_ power became her main staple to find places where people where and then, using the _Water Flow_, destroy debris to free ways to people get out, she decided to change her capename to _Colocation_. 

She became a very important Sifu, giving personal defense classes for minorities and also teaching classes for policemen.

She became water.

## Aspects

| _**Type**_       | _**Aspect**_                                        |
|-----------------:|-----------------------------------------------------|
| **Power Aspect** | C-Class Dragon with "Jumping" capacities            |
| **Hero Aspect**  | Jeet-kune Do, Bruce Lee Heiress                     |
| **Trouble**      | _"I'm no the stereotype"_                           |
| **Background**   | _"Be water, my friend"_                             |
| **Background**   | West and East, Ying and Yang                        |

## Attributes

| _**Attribute**_ | _**Level**_  |
|----------------:|--------------|
| **Alertness**   | Good (+3)    | 
| **Athleticism** | Fair (+2)    |
| **Physique**    | Good (+3)    | 
| **Discernment** | Great (+4)   |
| **Willpower**   | Superb (+5)  |
| **Presence**    |              | 

## Skills

| _**Skill**_     | _**Level**_  |
|----------------:|--------------|
| **Academics**   | Fair (+2)    | 
| **Aid**         | Average (+1) | 
| **Deceive**     | Average (+1) | 
| **Drive**       |              |
| **Engineering** |              | 
| **Fight**       | Great (+4)   |
| **Investigate** | Fair (+2)    | 
| **Larceny**     | Fair (+2)    | 
| **Provoke**     | Average (+1) | 
| **Rapport**     | Average (+1) | 
| **Shoot**       |              | 
| **Stealth**     | Good (+3)    | 
| **Survival**    | Average (+1) | 

## Resources

| _**Attribute**_  | _**Level**_  |
|-----------------:|--------------|
| **Contacts**     |              | 
| **Reputation**   |              | 
| **Wealth**       |              | 

## Stunts [ SFP Remaining: 2 ]

+ _Heart of Mountain x3_ (_Willpower_ Legendary +8 - 6 SFP)
+ _Reflected Force_
+ _Dance with Bullets_
+ _Disrupt Chi_
+ _Water Flow_ - Swap _Willpower_ for _Athleticis_ for Attacks by _Fight_ while using chi (_including_ any Attribute Bonus)
+ _Strength of the Dragon_
+ _Speed of the Dragon_ - can Teleport automatically to any place she knows intimately _or_ is in lineof sight by 1FP. Maximun range is Willpower Attribute Bonus in zones (minimun 1). Can be done _before_ doing an action

## Stress and Consequences

| _**Stress**_ | _**Bar**_         |
|-------------:|-------------------|
| _Physical_   | `3`{: .fate_font} |
|  _Mental_    | `5`{: .fate_font} |
| _Resources_  | `2`{: .fate_font} |

| _**Consequences**_ | _**Normal**_      | _**Extra Physical**_ | _**Extra Mental**_ | _**Extra Resource**_ |
|-------------------:|:-----------------:|:--------------------:|:------------------:|:--------------------:|
| _Mild (+2)_        | `1`{: .fate_font} |                      | `1`{: .fate_font}  |                      |
| _Moderate (+4)_    | `1`{: .fate_font} |                      |                    | `1`{: .fate_font}    |
| _Severe (+6)_      | `1`{: .fate_font} |                      |                    |                      |
