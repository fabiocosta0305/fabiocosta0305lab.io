---
title: "Split/Second (Real Name: Winston Clemens)"
subheadline: A Sample Character for Wearing the Cape
language: en
layout: personagens
categories:
  - wtc
  - personagens
tags:
 - Wearing the Cape
header: no
---

Winston Clemens had already deal with the hardships in life.

Born into a somewhat poor, son from an ex-Black Panther, he learned fast to solve problems with all he could. He learned some fighting and also gone for school for study. He wasn't good enough for one of the big colleges, so he gone for the local community college for a Engeneering Major and then for the Army.

He served at places like Angola and Irak, on all he had worked with the bomb squad. His proficiency on disarming bombs gave him what he could to better his life, and as soon he was honorably discharged from Army, he came back to his city, and got into Police's Bomb Squad.

But this could be his death sentence.

A crazy Verne attacked the city and put a Vernetech bomb on it. The local capes isolated the place, but they had not the knowledge on how to deal with explosives. So the Bomb Squad was called.

A Vernetech device, even from a D-Class Verne, is a great problem. So Winston chose to deal with this by himself.

Bad idea.

As soon he started to analyse it, he noted that the countdown gone fast. He dealt with it as good as possible, but he failed.

The bomb exploded.

And he had his breakthorugh, whem he thought on how to get away with the building, as it was only him there.

He noticed the time dilation and he started to run away from the shrapels released by the bomb. He was hit by some of them, but if wasn't the breakthrough, he would be made into mincemeat.

He got out just in time to see the building collapsing and getting into the ground.

He was discharged from Police and it was when the local CAI team, Prop, asked for his help.

He accepted, choosing as capename the time he had to run away from that bomb: a _Split-Second_.

He is very touchy on racism or otherwise hate-speak: as the son of an ex-Black Panther, he learned to fight the fight against prejudice.

He is an specialist on mechanics and all kind of engeneering things: he never commited the same error he did on his breakthrough again.

## Aspects

| _**Type**_       | _**Aspect**_                                        |
|-----------------:|-----------------------------------------------------|
| **Power Aspect** | B-Class Speedster                                   |
| **Hero Aspect**  | From Bomb Squad to Hero Squad                       |
| **Trouble**      | I'll not do the same mistake again                  |
| **Background**   | _"If I die, at least I'll die lightly"_             |
| **Background**   | Son of an ex-Black Panther - Touchy about racism    |

## Attributes

| _**Attribute**_ | _**Level**_  |
|----------------:|--------------|
| **Alertness**   | Good (+3)    | 
| **Athleticism** | Great (+4)   |
| **Physique**    | Good (+3)    | 
| **Discernment** | Good (+3)    |
| **Willpower**   | Fair (+2)    |
| **Presence**    | Fair (+2)    |

## Skills

| _**Skill**_     | _**Level**_  |
|----------------:|--------------|
| **Academics**   | Average (+1) | 
| **Aid**         |              | 
| **Deceive**     |              | 
| **Drive**       | Average (+1) | 
| **Engineering** | Superb (+5)  | 
| **Fight**       | Average (+1) | 
| **Investigate** |              | 
| **Larceny**     | Fair (+2)    | 
| **Provoke**     |              | 
| **Rapport**     | Average (+1) | 
| **Shoot**       |              | 
| **Stealth**     | Fair (+2)    | 
| **Survival**    | Average (+1) | 

## Resources

| _**Attribute**_  | _**Level**_  |
|-----------------:|--------------|
| **Contacts**     |              | 
| **Reputation**   |              | 
| **Wealth**       |              | 

## Stunts [ SFP Remaining: 2 ]

+ _Absolute Speed x3_ (_Athleticism_ Epic +7 - 6 SFP)
+ _Plenty of Time_
+ _Can't Stop Me_
+ _Popular_
+ _I've Read about That!_
+ _The MacGyver_
+ _Power of Deduction_

## Stress and Consequences

| _**Stress**_ | _**Bar**_         |
|-------------:|-------------------|
| _Physical_   | `3`{: .fate_font} |
|  _Mental_    | `3`{: .fate_font} |
| _Resources_  | `2`{: .fate_font} |

| _**Consequences**_ | _**Normal**_      | _**Extra Physical**_ | _**Extra Mental**_ | _**Extra Resource**_ |
|-------------------:|:-----------------:|:--------------------:|:------------------:|:--------------------:|
| _Mild (+2)_        | `1`{: .fate_font} |                      |                    |                      |
| _Moderate (+4)_    | `1`{: .fate_font} |                      |                    |                      |
| _Severe (+6)_      | `1`{: .fate_font} |                      |                    |                      |
