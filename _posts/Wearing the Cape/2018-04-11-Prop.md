---
title: "Prop (Real Name: Jake Haigh)"
subheadline: A Sample Character for Wearing the Cape
language: en
layout: personagens
categories:
  - wtc
  - personagens
tags:
 - Wearing the Cape
header: no
---

Born from an English family that migrated to US for work some years before The Event, Jake always played rugby. He played hard, he studied hard, and he lived always by the rugby principles. It was when he was 16 and he was playing a match in the college rugby league...

And the Event happened...

When he woke up, he started to hear the sound of a small airplane falling down straight to field's grandstand!

He just _thought_ on jump to scream to people to run, but he just _flew_ and got to the plane, holding it and put him in the ground, saving them.

He looked around and saw how much problems The Event provoked.

He looked to the field. 

And it was the last time he played rugby with common people.

But soon he found some fellows to help people as a breakthrough, a cape, under the name of his rugby position.

Prop.

Now he lives under the effigy of rugby principles for his action as cape. He's the leader of a local CAI team, linked with SPAT teams and police from a small-to-medium city.

He has a good life and can use his powers for good, after 10 years from the Event.

But rugby is still into his mindframe.

## Aspects

| _**Type**_       | _**Aspect**_                                    |
|-----------------:|-------------------------------------------------|
| **Power Aspect** | B-Class Atlas                                   |
| **Hero Aspect**  | US Rugbier - The Team Prop                      |
| **Trouble**      | Leader by example to a fault                    |
| **Background**   | _"Crouch, Touch, Pause... ENGAGE!!"_            |
| **Background**   | The Rugby Principles as Cape Philosophy         |

## Attributes

| _**Attribute**_ | _**Level**_  |
|----------------:|--------------|
| **Alertness**   | Great (+4)   |
| **Athleticism** | Great (+4)   |
| **Physique**    | Superb (+5)  |
| **Discernment** | Good (+3)    | 
| **Willpower**   | Good (+3)    | 
| **Presence**    | Good (+3)    | 

## Skills

| _**Skill**_     | _**Level**_  |
|----------------:|--------------|
| **Academics**   | Fair (+2)    | 
| **Aid**         | Fair (+2)    | 
| **Deceive**     |              | 
| **Drive**       |              |
| **Engineering** | Average (+1) | 
| **Fight**       | Fair (+2)    |
| **Investigate** | Average (+1) | 
| **Larceny**     |              | 
| **Provoke**     | Average (+1) | 
| **Rapport**     | Fair (+2)    | 
| **Shoot**       |              |
| **Stealth**     | Average (+1) | 
| **Survival**    |              | 

## Resources

| _**Attribute**_  | _**Level**_  |
|-----------------:|--------------|
| **Contacts**     |              | 
| **Reputation**   | Average (+1) | 
| **Wealth**       |              | 

## Stunts [ SFP Remaining: 2 ]

+ _Superhuman Physique x3_ (_Physique_ Legendary +8, Bonus +3)
+ _Your Own Missile_
+ _Supersenses_
+ _Charge_
+ _Superhuman Recovery_
+ _Hard as Nails_
+ _Top of Your Class_

## Stress and Consequences

| _**Stress**_ | _**Bar**_         |
|-------------:|-------------------|
| _Physical_   | `4`{: .fate_font} |
|  _Mental_    | `4`{: .fate_font} |
| _Resources_  | `2`{: .fate_font} |

| _**Consequences**_ | _**Normal**_      | _**Extra Physical**_ | _**Extra Mental**_ | _**Extra Resource**_ |
|-------------------:|:-----------------:|:--------------------:|:------------------:|:--------------------:|
| _Mild (+2)_        | `1`{: .fate_font} | `1`{: .fate_font}    |                    |                      |
| _Moderate (+4)_    | `1`{: .fate_font} | `1`{: .fate_font}    |                    | `1`{: .fate_font}    |
| _Severe (+6)_      | `1`{: .fate_font} |                      |                    |                      |
