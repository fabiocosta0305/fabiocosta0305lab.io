---
title: "Alsyf Alayat, the Sword Verses (Real Name: Rahul Musa Al-Gazzawi/Mufti Musra Hussein Al-Gazzawi)"
subheadline: A Sample Character for Wearing the Cape
language: en
layout: personagens
categories:
  - wtc
  - personagens
tags:
 - Wearing the Cape
header: no
---

_Alsyf Alayat_ is one of the main pieces on _Undying Caliphate_, as she's a Mastermind Puppet Master that can manipulate the minds of those who are from the _Ummah_ (Islamic people) to do his will, even implanting post-hypnotic suggestions that can be triggered in the worst moments. 

## Aspects

| _**Type**_       | _**Aspect**_                                          |
|-----------------:|-------------------------------------------------------|
| **Power Aspect** | A-Class Mastermind (Puppet Master)                    |
| **Hero Aspect**  | The Prophiesied _Mahdi_ (self-deluded)                |
| **Trouble**      | _"Down with the infidel! Brimstone to the Crusader!"_ |
| **Background**   | Ex-Double Agent                                       |
| **Background**   | Part of the _Undiying Caliphate_ Head-Of-State        |

## Attributes

| _**Attribute**_ | _**Level**_    |
|----------------:|----------------|
| **Alertness**   | Great (+4)     |
| **Athleticism** | Fair (+2)      |
| **Physique**    | Great (+4)     |
| **Discernment** | Superb (+5)    |
| **Willpower**   | Fantastic (+6) |
| **Presence**    | Superb (+5)    |

## Skills

| _**Skill**_     | _**Level**_  |
|----------------:|--------------|
| **Academics**   | Fair (+2)    |
| **Aid**         | Average (+1) |
| **Deceive**     | Good (+3)    | 
| **Drive**       | Fair (+2)    |
| **Engineering** | Average (+1) | 
| **Fight**       | Fair (+2)    |
| **Investigate** | Great (+4)   |
| **Larceny**     | Average (+1) | 
| **Provoke**     | Good (+3)    | 
| **Rapport**     | Good (+3)    | 
| **Shoot**       | Average (+1) | 
| **Stealth**     | Average (+1) | 
| **Survival**    | Fair (+2)    |

## Resources

| _**Attribute**_  | _**Level**_  |
|-----------------:|--------------|
| **Contacts**     | Good (+3)    |
| **Reputation**   | Fair (+2)    |
| **Wealth**       | Average (+1) | 

+ _Superhuman Mind x4_  (_Discernment_ Epic +9, Bonus +4)
+ _Sword Verse_ (_Do As I Said!_ - +2 against Muslims; -2 against anyone else; can't use when angry or in rage)
+ _Today I'm this_

## Stress and Consequences

| _**Stress**_ | _**Bar**_         |
|-------------:|-------------------|
| _Physical_   | `4`{: .fate_font} |
|  _Mental_    | `4`{: .fate_font} |
| _Resources_  | `3`{: .fate_font} |

| _**Consequences**_ | _**Normal**_      | _**Extra Physical**_ | _**Extra Mental**_ | _**Extra Resource**_ |
|-------------------:|:-----------------:|:--------------------:|:------------------:|:--------------------:|
| _Mild (+2)_        | `1`{: .fate_font} |                      | `1`{: .fate_font}  |                      |
| _Moderate (+4)_    | `1`{: .fate_font} |                      |                    | `1`{: .fate_font}    |
| _Severe (+6)_      | `1`{: .fate_font} |                      |                    |                      |
