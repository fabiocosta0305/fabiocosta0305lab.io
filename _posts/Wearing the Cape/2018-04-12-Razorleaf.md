---
title: "Razorleaf (Real Name: Jonathan Leaf)"
subheadline: A Sample Character for Wearing the Cape
language: en
layout: personagens
categories:
  - wtc
  - personagens
tags:
 - Wearing the Cape
header: no
---

## Aspects

| _**Type**_          | _**Aspect**_                                        |
|--------------------:|-----------------------------------------------------|
| **Power Aspect**    | B-Class Flora Projector                             |
| **Villain Aspect**  | Superpowered Bully                                  |
| **Trouble**         | Behavioral Issues                                   |
| **Background**      | Whitlow's Alumni                                    |
| **Background**      | Intimidating Leadership                             |

## Attributes

| _**Attribute**_ | _**Level**_  |
|----------------:|--------------|
| **Alertness**   | Good (+3)    | 
| **Athleticism** | Great (+4)   |
| **Physique**    | Great (+4)   |
| **Discernment** |              | 
| **Willpower**   | Good (+3)    | 
| **Presence**    | Superb (+5)  |

## Skills

| _**Skill**_     | _**Level**_  |
|----------------:|--------------|
| **Academics**   |              | 
| **Aid**         | Average (+1) | 
| **Deceive**     | Fair (+2)    | 
| **Drive**       |              |
| **Engineering** | Average (+1) | 
| **Fight**       | Good (+3)    | 
| **Investigate** | Average (+1) | 
| **Larceny**     | Good (+3)    | 
| **Provoke**     | Fair (+2)    | 
| **Rapport**     | Fair (+2)    | 
| **Shoot**       | Fair (+2)    | 
| **Stealth**     |              | 
| **Survival**    |              | 

## Resources

| _**Attribute**_  | _**Level**_  |
|-----------------:|--------------|
| **Contacts**     |              | 
| **Reputation**   | Average (+1) | 
| **Wealth**       |              | 

## Stunts [ SFP Remaining: 3 ]

+ _Leaves and Vines_ - WR 6 (SFP 3)
+ _Lethal Attack_
+ _Area Attack_
+ _Multiple Targets_
+ _Stun Attack_
+ _Great Control x2_ (_Willpower_ Superb (+5), SFP 4)
+ _Shape the Field_

## Stress and Consequences

| _**Stress**_ | _**Bar**_         |
|-------------:|-------------------|
| _Physical_   | `4`{: .fate_font} |
|  _Mental_    | `3`{: .fate_font} |
| _Resources_  | `2`{: .fate_font} |

| _**Consequences**_ | _**Normal**_      | _**Extra Physical**_ | _**Extra Mental**_ | _**Extra Resource**_ |
|-------------------:|:-----------------:|:--------------------:|:------------------:|:--------------------:|
| _Mild (+2)_        | `1`{: .fate_font} |                      |                    |                      |
| _Moderate (+4)_    | `1`{: .fate_font} |                      |                    | `1`{: .fate_font}    |
| _Severe (+6)_      | `1`{: .fate_font} |                      |                    |                      |
