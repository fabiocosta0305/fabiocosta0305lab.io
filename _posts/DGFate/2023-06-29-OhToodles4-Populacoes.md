---
title: "Oh, Toodles! 4 - As Ferramentas em Fate do Mr. Mickey - Populações (_Loose Threads_)"
subheadline: Uma Introdução ao Fate 
date: 2023-06-29 15:30:00 +0300
layout: post
categories:
 - RPG
tags:
 - Fate
 - Fate Core
 - Fate-Core
 - Artigos
 - Dungeon Geek
 - Introdução ao Fate
 - Domando o Fate
 - Iniciando no Fate
 - Ações
 - Teste
 - Rolamento
 - introducao-ao-fate
order: 24
header: no
excerpt_separator: <!-- excerpt -->
---

> _"Oh, Toodles!"_

Mais um artigo da série _Oh, Toodles!_.

Veremos mais algumas novas ferramentas que poderão lhe ajudar a lidar com problemas normais do RPG dentro do Fate, visando estabelecer coisas interessantes. Coisas que coletei para formar meu próprio _Toodles_, minhas Ferramentas de Narrativa em Fate.

No artigo de hoje, vamos falar sobre uma coisa que é muito complicada em alguns jogos: como lidar com _populações_. Povos, raças, nacionalidades, etnias, cultos, organizações e afins de maneira a trabalhar tanto as mesmas no nível _macro_ quanto os indivíduos no nível micro.

Então, vamos começar?

Então digam: _Oh, Toodles!_

<!-- excerpt -->

## O problema de populações

Uma das coisas mais complicadas em qualquer RPG é lidar com _populações_: o RPG é focado normalmente no indivíduo específico, ou em pequenos grupos. Qualquer RPG (e Fate não é diferente) sofre ao descrever uma população de uma região geográfica maior. Em geral, quando isso ocorre, é dito de maneira muito generalista, sem facilidades para tornar parcelas dessas populações únicas, como se tratasse o grupo como um indivíduo só.

Isso pode ser ruim, pois às vezes você pode precisar criar um _Ferreiro da Vila_ do nada, o que torna muito complicado de o fazer em sistemas mais complexos como os baseados em d20. A solução melhor nesses casos é recorrer a algum módulo, como um Livro dos Monstros ou alguma coisa similar. Entretanto, isso pode não trazer a flexibilidade necessária e pode forçar a preparação excessiva.

## Era uma vez três porquinhos...

E foi quando Tara Zuber nos presenteou com _Loose Threads_: um jogo sobre personagens de contos de fada que não tiveram seu final feliz, ou tiveram e descobriram que ele não era tão feliz. Envolto em tensões entre coisas opostas e com um desejo do coração digno de uma música de Phil Collins, eles andam por mundos e vilas, reinos e paragens, enfrentando desafios de bondade e coisas que podem demandar sacrifícios maiores do que se imagina.

E para isso, eles vão encontrar todo tipo de pessoas estranhas: Fadas Madrinhas, Gênios, Caçadores com Machados, Homens Amaldiçoados com Cabeças de Burro, Fadas Travessas, e por aí afora. E em alguns casos até mesmo intersecções desses tipos!

Para isso, a solução que foi encontrada envolve justamente as _Populações_ (___Populations___). Em Loose Threads, as Populações são usadas para definir Espécies, regiões onde as criaturas vivem, religião ou qualquer outra coisa. Elas possuem três ___Características___, palavras simples que atuam como Aspectos da mesma, uma ___Discórdia___, algo que divide os participantes daquela população de alguma forma, e uma ou mais perícias em um determinado nível.

Por exemplo:

> + Os ___Coelhos da Fábrica de Ovos___ são ___Laboriosos, Ordeiros___ e ___Pacíficos___, sendo possível os ver na Mata buscando determinadas matérias primas para os Ovos de Chocolate. Eles estão divididos entre _Fugir_ quando se veem em perigo e _Defender_ pessoas próximas quando atacadas. Todos possem _Atletismo **Bom (+3)**_ e _Ofícios **Razoável (+2)**_ ou melhor
> + Os ___Elfos do Papai Noel___ são ___Benfazejos, Pacíficos___ e ___Pequenos___, e normalmente se sentem divididos entre _Criar_ novos brinquedos e presentes e _Construir_ coisas, focando-se na melhoria dos itens que já sabem construir. Todos possuem _Ofícios **Bom (+3)**_ ou melhor;
> + As ___Matilhas de Lobos___ do Ártico costumam ser  ___Cruéis, Honrados___ e ___Territoriais___, sentindo-se divididos entre _Manterem seu Território_, aceitando o que a Natureza lhe dá e apenas mantendo forasteiros longe e _Tomar tudo o que puderem_, não se preocupando com o amanhã. Todos costumam ter _Furtividade **Bom (+3)**_ ou melhor;
> + Os ___Magos Alquimistas___  tendem a serem ___Místicos, Sábios___ e ___Teimosos___, divididos entre pesquisarem a _Sabedoria_ da Alquimia para auxiliar a população ou obter _Poder_ Místico para seus fins. Todos possuem _Alquimia **Bom (+3)**_  ou melhor;

Com essa descrição rápida, temos uma série de populações possíveis de serem usadas. Você pode facilmente declarar qualquer situação e usar as estatísticas da própria População como as do indivíduo quando necessário. 

E de maneira mais amplas, é possível utilizar-se as várias populações para criar-se indivíduos únicos. De maneira geral, basta combinar uma ou mais _Populações_ e usar os elementos dela para criar seu personagem. Para isso, escolha uma das característica de cada População com sendo do Personagem, defina uma _Agenda_, um objetivo que ele deseja fazer, e os custos que envolver-se com tal NPC terá ao personagem. Além disso, defina as perícias somando as perícias listadas em cada População e as ajustando caso queira dar uma enfase em alguma característica do mesmo: talvez reduzindo-as se ele não for tão ligado a tal população ou aumentando-as para personagens de maior importância, ou mesmo não as listando.

> Pegando as populações que listamos acima, vamos criar um NPC rápido... No caso, vamos fazer um Coelho da Páscoa Alquimista (?!)
> 
> ### Amelia, a Coelha Alquimista
> 
> + ___Descrição:___ uma coelha um pouco maior que o normal, e menos cheiinha, ela possui como ponto mais curioso uma mancha rosada em forma de coração sobre o olho esquerdo. Seu pelo, de outra forma, é totalmente branco. Meio destrambelhada, mas de bom coração e mais sábia do que aparenta
> + ___Populações:___ Coelhos da Fábrica de Ovos; Magos Alquimistas
> + ___Características:___ Laboriosa, Sábia
> + ___Agenda:___ Amelia quer aprender novas formas de magia que a ajudem em preparar ovos melhores
> + ___Custos:___ Servir de cobaia para as experiências destrambelhadas de Amelia pode resultar em Sacrifícios de Vitalidade (marcar Estresse sem poder recuperar por um tempo) ou em Transformações (Aspectos Especiais indicando mudança de forma). Procurar novos livros de magia para ela estudar pode implicar em uma Promessa (um Aspecto que pode ser usado o tempo todo contra um personagem quando o mesmo não se focar nele). 
> + ___Perícias:___ _Atletismo **Regular (+1)**; Ofícios **Razoável (+2)**; Alquimia **Regular (+1)**_

## Qual a vantagem disso para o narrador?

Bem...

Se você parar para pensar, você pode compartimentar bem coisas que você pode utilizar para criar NPCs específicos de maneira bem simples.

Desse modo, você terá um caminho mais fácil para desenvolver aquilo que você precisa.

Pense em todas as situações que você precisou criar NPCs _on-the-fly_: usando um sistema de Populações como esse, você pode agrupar elementos que você queira para criar um NPC simples e rápido. Não tão simples quanto ir _free-style_ Fate, mas com a garantia de que poderá criar NPCs baseados em modelos de populações bastantes interessantes. 

E a parte mais interessante é que você pode reciclar vários deles: vou disponibilizar abaixo uma lista dessas Populações que você poderá usar em seus jogos, e sugiro que você crie os seus também, representando grandes vilas, cidades, reinos, religiões, cultos, guildas, e assim por diante, conforme sua necessidade. 

Além disso: ao ter tais Populações preparadas, você sempre pode aproveitar e criar AVENTURAS de maneira rápida. Pense o seguinte... Se os _Elfos do Papai Noel_ são divididos entre _Criar_ brinquedos novos e _Construir_ brinquedos clássicos, como decidir o que é o melhor? E se um dos Elfos resolver criar um brinquedo novo que perca o controle? Ou os brinquedos clássicos podem estar chatos, como dar uma melhorada? 

Claro que isso pode ser sempre apimentado. Pense nas populações a seguir:

> + Os ___Beduínos e Comerciantes___ são ___Desconfiados, Hospitaleiros___ e ___Tradicionalistas___, sendo divididos entre continuar cruzando o Deserto _Comerciando_ produtos exóticos de um lado ou outro do _Caminho de Ouro e Seda_, ou contentarem-se em _Pastorear_ as preciosas ovelhas e carneiros nos Oasis. Todos possuem  _Comunicação_ em no mínimo _Bom (+3)_;
> + Os ___Tuaregs___ são ___Impiedosos, Supersticiosos___ e ___Traiçoeiros___, sendo divididos entre utilizar sua força como _Mercenários_, protegendo as caravanas que cruzam o Deserto de um lado a outro do Mundo, ou _Ladrões_, roubando as preciosas mercadorias que cruzam pelo ___Caminho de Ouro e Seda___. Todos os _Tuaregs_ possuem no mínimo _Bom (+3)_ em _Lutar_;

Como você pode aproveitar elas em aventuras? Vamos para algumas opções:

+ Um poderoso líder Tuareg conhecido como _A Mão dos Antepassados_ baixou uma norma entre as demais tribos que um determinado Oasis não poderia ser alvo de combates, sendo um Elíseo Sagrado. Entretanto, um senhor Beduíno chamado Tahid estaria vendendo crianças nesse mesmo Oasis, aproveitando-se das "boas" intenções do _Mão_. O que está por trás de tudo isso, e como revelar a verdade, que Tahid está cometendo tal ignomia?
+ O corpo de um Tuareg for encontrado seco perto de uma rota comercial. Ele não aparenta ter sido morto em combate, o que implica que sua morte foi desonrosa. Há quem diga que seu grupo de Tuaregs, os Escorpiões da Areia, estão prontos a vingar tal ignomia, e aparentemente sua vítima seria Khaled Al-Hatani, um dos principais líderes beduínos. Mas Khaled afirma ser inocente... Como descobrir a verdade, sendo que ele parece ter morrido de sede
+ Os beduínos estão cansados de serem atacados por algumas tribos cruéis de Tuaregs, e estão dispostos a recorrer a um Alquimista que prepare um poderoso encantamento. Com esse encantamento supostamente eles irão colocar na rota comercial um poderoso Janni, um espírito do Deserto, que irá proteger os Beduínos. Os Tuaregs, entretanto, sabem disso, e encontraram alguns tomos do passado com magia poderosa e terrível, capaz de trazer criaturas da mítica Stygia, o reino mais ignóbil da Antiguidade! O que pode acontecer quando tanta magia perigosa pode aparecer.

Entre outras coisas.

Além disso, você não precisa restringir suas Populações a Fantasia:

+ ___Samurai Urbanos___ são ___Durões___, ___Poderosos___ e ___Aprimorados___. Eles se dividem em ___Zelar por sua reputação___,  cumprindo seus Contratos à Risca mesmo a custa do ganho futuro, e ___Obter qualquer vantagem___ possível, sabendo que sempre pode acontecer de seus contratantes os trair. Todo Samurai Urbano costuma possuir _Atirar_ e _Lutar_ em ___Bom (+3)___ ou melhor
+ ___Membros de Gangues___ são ___Leais___, ___Violentos___ e ___Espalhafatosos___. Eles costumam se dividir entre ___Manter seu território___ livre de encrencas e agir na moita e entre ___espalhar o caos___, causando morte e destruição por onde passam. Membros de Gangue costumam ter _Provocar_ e _Atletismo_ em ___Bom (+3)___ ou melhor
+ ___Gerentes da Saigoro Corp.___ são ___Efetivos___, ___Discretos___ e ___Cheios de Recursos___. Eles costumam ser divididos entre sua ___Lealdade___ à Empresa, agindo de acordo com os ideais (públicos ou não) da mesma, e às ___Possibilidade de Ganhos___, usando a estrutura da empresa para seus próprios fins, inclusive para eliminar a concorrência dentro da mesma sem sujarem as mãos. Os Gerentes da Saigoro costumam ter _Recursos_ e _Comunicação_ em ___Bom (+3)___ ou melhor
+ ___Neo-Thule___ são ___Racistas___, ___Violentos___ e ___Leais___, divididos entre realizar ___Panfletagem___ para propagarem sua ideologia neo-facista e ___Ações Diretas___, matando o máximo de pessoas possíveis antes de serem pegos e irem para o Valhala. Todo ___Neo-Thule___ tem _Atirar_ e _Comunicação_ ___Razoável (+2)___
+ ___A Ordem de São Pantaleão___ são ___Zelosos___, ___Hábeis___ e ___Unidos___, divididos entre ___Observar a Rede___ por qualquer coisa que possa comprometer a Santa Igreja e ___Agir de maneira mais dura___ contra os Hereges da Nova Reforma. Sãopantaleoneos costumam ter _Conhecimentos_ ___Ótimo (+4)___

Com apenas alguns elementos elaboramos cinco populações que podem oferecer uma série de tramas interessantes. Como um pequeno exercício, tente imaginar interações entre todas as populações acima... E personagens com lealdades diferentes: um membro de Gangue da Ordem de São Pantaleão? Ou um Neo-Thule da Saigoro Corp? As possibilidades são limitadas apenas pela sua imaginação.

## O quanto posso Expandir?

Na prática? Indefinidamente

Populações tem a grande vantagem de serem compartimentizadas o bastante para serem usadas de maneira independente quando damos o _Zoom out_, vendo o grupo como um todo, e oferecerem mecanismos para criar indivíduos únicos e interessantes utilizando parâmetros já bem conhecidos.

Imagine uma situação de Fantasia Medieval: você pode criar vários personagens usando combinações de Populações que representem _Nações_, _Religiões_, _Profissões_ e afins.

Por exemplo:

+ ___Nações___
   + _Salvare_ é um reino composto de pessoas _Cultas_, _Otimistas_ mas _Maquiavélicas_, que se dividem entre ___Ampliarem seu poder___ por meio de todo tipo de manipulação e chantagem e ___Crescerem no Saber___, desenvolvendo sua ciência. Todos costumam possuir _Conhecimento_ ou _Comunicação_ em __*Bom (+3)*__;
   + _Holz_ é um reino de _Guerreiros_, _Líderes_ e _Serviçais_, sendo um povo que se divide entre os que seguem a rígida ___Hierarquia___ da _Orden des Rosenkranzes_, o verdadeiro Exército do país, ou se unir a uma série de companhias de ___Mercenários___ que estão apenas um passo acima de ladrões. Todos costumam possuir _Vigor_ em __*Bom (+3)*__;
   + _Assiaria_ é um reino de pessoas _Misteriosas_, _Mistícas_ e _Cheias de Histórias_, um povo que se divide entre as ___Tradições___ do seu povo, contada pelo _Tumi alhukmah_, o Livro dos Sábios, e os ___Portentos___ que têm sido visto desde o surgimento do _malak muta'aliq_, o Anjo Resplandescente. Todos costumam possuir _Conhecimentos_ __*Bom (+3)*__;
   + _On Tsi_ é formado por pessoas _Laboriosas_, _Tradicionalistas_ e _Melífluas_, que sempre se veem divididas entre ___Entender___ o Passado e desvendar mais informações sobre seus antepassados ou ___Cultuar___ o mesmo, tomando suas palavras pétreas com força de lei. Todos costumam ter _Comunicação_ __*Bom (+3)*__
+ ___Religiões___
    + _Giulianna_ é a divindade da Vida e da Dança cultuada como principal culto entre os poucos cosmopolitas de Salvare que ainda se preocupam com Deuses. Seus seguidores, como sua Deusa, costumam ser _Energéticos_, _Lascivos_ e _Suaves_, se dividindo entre os que ___Dançam___ no ritmo que a vida impõe e os que tentam ___Mudar a música___, controlando seus destinos. Todos costumam possuir _Ofícios_ em *__Bom (+3)__*;
    + _Rosenkranz_ foi um famoso guerreiro que ascendeu ao nível de uma divindade após sua morte. Cultuado em _Holz_ e tendo a primazia de ser a religião oficial do mesmo, ele é uma divindade ___Guerreira___, ___Forte___ e ___Justa___. Seus seguidores se dividem entre aqueles que procuram a ___Justiça___ descrita nos _Rosenkranzbücher_, tomos de sabedoria militar e elementos filosóficos considerados sagrados pelos _Rosenkranzen_, e buscar uma forma de ___Tirania___ justificada pelos _Rosenkranzbücher_. Todos costumam ter _Comunicação_, _Vigor_ e _Lutar_ ___Bom (+3)___
    + _Malak muta'aliq_, o Anjo Resplandescente, tem se tornado uma divindade muito seguida em Assiaria. Autoproclamado Senhor do Sol e da Verdade, seus seguidores, assim como ele, tendem a ser ___Severos___, ___Implacáveis___ e ___Verdadeiros___, pois a mentira não se arrola no meio deles. Entretanto, eles se dividem entre os que compreendem que a ___Tolerância___ com as Tradições do passado e com outras divindades é importante, e entre os que são ___Belicosos___, entendendo que são a Visão de Fogo Que Queima, devendo purificar o mundo para a segunda vinda do Anjo. Costumam todos ter _Lutar_ __*Bom (+3)*__, além de _Percepção_ tão alta quanto os mesmos
    + _Hong Wan_, o Burocrata Supremo, o Administrador do Mundo, o Soberano Indicado, entre outras tantas denominações, é a principal divindade de _On Tsi_, sendo uma divindade ___Paciente___, ___Dedicada___, mas ___Dura___ quando tem que agir, assim como seus principais fieis. Seus seguidores são divididos entre ___Observar___ todas as situações e agir quando cabido, ou ___Agir___ tão logo tenha uma razão indicada pelas Tradições . Todos possuem _Ofícios_, _Conhecimento_ e/ou _Comunicação_ em ___Bom (+3)___
    + Uma estranha entidade que aparece aqui e ali, _O Gato_ (chamado assim nos idiomas de todos os reinos) é uma Divindade ___Estranha___, ___Caótica___ mas ___Sábia___ dentro de seus raciocínios malucos. Seus seguidores, identificados pelas roupas multicoloridas e pelos chapéus que sempre possuem algum tipo de pelo de gato, se dividem entre aqueles que, por meio de suas pilhérias, procuram ___Ensinar___ as lições de sabedoria prática do Gato, e os que procuram apenas ___Sobrepujar___ os outros, provando-se mais espertos não importa o custo. Todos costumam ter _Enganar_, _Furtividade_, _Atletismo_ ou _Roubo_ em ___Razoável (+2)___ ou melhor;
    + Um culto antigo, perdido desde a última Queda do Mundo, _O Sombrio_ é talvez uma das divindades mais perigosas de serem invocadas, já que ele é ___Perigoso___ por sua natureza ___Mórbida___ ao cuidar de ___Encerrar___ ciclos. Seus reclusos e ocultos seguidores se dividem entre aqueles que apenas desejam _Acompanhar_ os eventos que levarão ao fim os eventos que seu Senhor deseja, evitando que eles acabem antes ou depois do estipulado na Ampulheta das Gerações, e os que desejam _Direcionar_ o fim dos eventos, assumindo um papel ativo ao serem a Mão Ativa de seu senhor. Todos possuem _Furtividade_ e _Lutar_ ou _Atletismo_ em __*Bom (+3)*__;
+ ___Profissões___
    + As várias ___Ordens de Cavalaria___ que existem em todo o mundo são sempre compostas de homens e mulheres de ___Força___, ___Garra___ e ___Resignados___, estando acima dos mercenários por seu comportamento ordeiro e capacidade de fazer os números serem ainda mais poderosos. Costumam ser divididos em ordens mais ___Militares___, com treinamento bélico maior mas menor flexibilidade, e Ordens ___Tradicionais___ de Cavalaria, conjuntos de nobres e aventureiros que compensam seus menores números por meio de Argúcia e Tática. Costumam ter _Montaria_ (_Direção_ renomeada) e _Lutar_ em ___Razoável (+2)___ ou melhor;
    + Em qualquer lugar pode-se encontrar ___Trapaceiros___: do falso pedinte que se finge de cego até precisar correr da guarda ao Apostador que faz jogadas intensas, os melhores sabem quando confiar na sorte, na lábia, na prestidigitação ou em uma ocasional Adaga entre as costelas de um pobre diabo. Sempre sendo ___Sortudos___, ___Melífluos___ e ___Oportunistas___, dividem-se entre aqueles que seguem algum tipo de ___Ética___ em seus golpes (não atacar pessoas mais pobres que eles; nunca usar armas; aceitar uma ocasional falha em seus golpes) confiando que os Deuses preservam aqueles que se mantêm na linha, e aqueles que visam única e exclusivamente o ___Lucro Rápido___, não importa a que custo! Costumam ter _Furtividade_ ou _Comunicação_ em ___Bom (+3)___
    + Seguidoras de antigas tradições passadas desde muito antes da Era dos Registros, as ___Curandeiras___ são ___Tradicionalistas___ e ___Sábias___ que, em sua ___Teimosia___, não costumam aceitar grandes novidades: suas poções são feitas segundo receitas é métodos tradicionais. Entretanto, embora a maioria seja focada nas ___Receitas Tradicionais___, existem algumas (normalmente jovens e insensatas) que procuram ___Novas Formas___ de se fazer suas magias e poções. Costumam possuir alguma forma de _Magia_ em nível ___Bom (+3)___

Agora, quer ver a coisa ficar ainda mais confusa?

Para começar, vamos lembrar que as _Populações_ são _Extras_. E que basicamente um _Extra_ pode ser criado como um personagem.

Até aqui tá okay...

Mas o desafio agora é utilizar as Populações não para criar um novo personagem, mas uma _nova População_!

Acha impossível? Vamos ver:

+ As ___Walküren der Rosenkränze___, Valquírias de _Rosenkranz_, são uma ordem de Curandeiras-Guerreiras de Holz, que vivem no _Tal der Milch_, o Vale do Leite, onde saem como companheiras de batalha de ordens militares poderosas o bastante, ou para realizar poderosas missões para Holz ou (em especial), para _Rosenkranz_. Sendo Holzerinnen, elas são ___Guerreiras___, mas são ___Sábias___ como qualquer Curandeira e  ___Justas___ como seguidores de _Rosenkranz_. Elas se dividem ocasionalmente em sua lealdade a ___Holz___, já que são considerada uma primazia dos Reis Guerreiros de Holz, ou a ___Rosenkranz___, estando prontas para ocasionalmente derrubar um que saia da linha. Elas costumam possuir alguma forma de Magia ___Boa (+3)___ como Curandeiras, mas também possuir _Lutar_ no mesmo nível como seguidoras de Rosenkranz;
+ O ___Cirque Fantastique___ é uma mixórdia de _Trapaceiros_ que são seguidores _dO Gato_ que se dividem em pequenos grupos de mamulengos. ___Estranhos___ como seu Patrono, e aparentemente Insanos, na verdade eles são ___Melífluos___ anarquistas que espalham o caos em locais onde os reis e potentades esqueceram de que ainda estão abaixo dos Deuses, sendo muito ___Sortudos___ na Graça de sua Fiel Felinidade. Os mamulengos se dividem entre ___Ensinar___ as lições que aprenderam com O Gato por meio de piadas, troças, canções de escárnio e uma ocasional poça de estrume para ser usada por um Lorde insensato, ou obter ___Lucro Rápido___ e aproveitar-se do caos que eles próprios instauram para seu benefício. Costumam possuir _Comunicação_ ___Bom (+3)___ como bons Trapaceiros que São, mas também possuem a _Furtividade_ e o _Atletismo_ dos seguidores dO Gato;
+ Os ___Hayeshan___ são uma estranha e perigosa Ordem de Cavaleiros de On Tsi: eles são uma heresia no governo do mesmo, pois eles acreditam que Hong Wan na verdade é uma das várias facetas dO Sombrio, uma verdade (imaginando que seja) obtida por meio de uma leitura pervertida dos Analectos do Burocrata! Sendo ___Pacientes___ como seria de se esperar dos seguidores de Hong Wan, são entretanto tão ___Perigosos___ quanto os seguidores do _Sombrio_ e ainda possuem uma combinação de Artes Marciais antigas e perigosas técnicas místicas passadas entre certas ordens de cavaleiros de On Tsi que os tornam extremamente ___Fortes___. Apenas o fato de se manterem ocultos, seus poucos agentes mais ativos morando no lado mais sombrio da Ferida, a grande fenda que separa On Tsi do resto do continente mantém eles distantes, mas alguns acham que eles estão visando _Direcionar_ o fim de On Tsi, destruindo o Reino dos Mil Alabastos, ainda que alguns digam que eles tentam _Entender_ os eventos e esperar portentos que lhes indiquem como agir. Todos eles possuem _Furtividade_ ___Bom (+3)___ como a dos seguidores dO Sombrio, mas também possuem a _Comunicação_ de homens de On Tsi e a _Montaria_ tão boa quanto a de outras Ordens de Cavalaria.

Como isso é possível?

Bem, lembra-se que, quando falamos sobre Extras, dissemos que Extras _podem ter Extras eles próprios_? Então, não deveria ser de espantar-se combinar os elementos de múltiplas Populações não apenas para criar indivíduos, mas também outras Populações.

E lógico que podemos complicar ainda mais um pouquinho, usando tais Populações em conjunto com outras para criar um novo indivíduo!

> #### ___Mathäusz Scheinberg Wulfbahn III, die Gans___
> 
> + ___Descrição:___ parece alguém que já teve dias melhores. Sua aparência geral é um tanto nobre, mas suas roupas rotas e com ocasionais pelos de Gato o revela para aqueles que conhecem mais sobre isso como parte do ___Cirque Fantastique___
> + ___Populações:___ _Cirque Fantastique_, _Holtz_, _Ordem de Cavalaria_
> + ___Características:___ _Estranho_, _Guerreiro_, _Força_
> + ___Agenda:___ aparentemente _die Gans_ é apenas um artista de rua louco como todos os do _Cirque Fantastique_, mas quem conversar com ele vai descobrir que sua missão é derrubar o governo do Duque Wulfbahn, de quem é filho ilegítimo. Assim o ordenou O Gato!
> + ___Custos:___ interferir na missão do Gans pode custar-lhes ter como inimigo o _Cirque Fantastique_ (e potencialmente _O Gato_); ajudar Gans pode fazer com que os personagens sejam vistos como Anarquistas: o Duque  Wulfbahn não possui filhos legítimos e a linha sucessória de seu Ducado é confusa... sendo que o Ducado possui riquezas o bastante para estimular os ducados ao redor a tentarem integrar o mesmo em seus territórios, mesmo que isso implique em guerra!
> + ___Perícias:___ _Furtividade, Lutar, Roubo, Atletismo_ ___Bom (+3)___
 
### É possível fazer o reverso?

Você então deve estar se perguntando: _é possível fazer o reverso? Gerar uma população a partir de um indivíduo?_

Bem...

Na prática, indivíduos são mais complexos que populações, que na verdade são representações _macro_ de elementos comuns de indivíduos no nível _micro_ (na prática, conjuntos de estereótipos pensados dentro de uma visão homogênea dos indivíduos).

Dito isso, não há absolutamente _nada_ que impeça você de tomar seus personagens e criar a partir deles populações que representem os seus povos, crenças, ordens ou qualquer outro elemento que você deseje.

Entretanto, é bom levar algumas coisas em consideração:

1. O personagem é um indivíduo mais _"padrão"_ de tal população? Isso pode impactar em como escolher quais ___Características___ e ___Perícias___ serão levadas do personagem para a população
2. O personagem é uma "ovelha negra"? Alguém que está indo contrário à maioria? Ou ele representa uma "facção" da mesma? Isso pode permitir que você trabalhe a ___Discórdia___ da população a ser criada e mesmo que obtenha mais populações a partir do personagem
3. As interações do personagem com a população são constantes? Isso pode refletir tanto na ___Discórdia___ (pois indicará que o personagem está ligado de maneira maior ou menor à população) e em ___Características___ (personagens dentro de uma população tendem a assumir as características da mesma como suas, é parte do processo social)

### Conclusão

Considero que as Populações são ferramentas maravilhosamente simples ao descrever um agrupamento qualquer de pessoas, seja um reino, uma ordem, ou mesmo uma casta, e permitir que, ao misturar-se a mesma, você possa criar rápida e facilmente personagens únicos para você usar no momento (e/ou trabalhar melhor para o futuro).

Além disso, graças aos mecanismos do Fate de Extras e _Fractal_, ela se torna uma ferramenta muito interessante ao montar-se todo tipo de agrupamento e indivíduo, e até mesmo de novas Populações, combinando elementos de várias Populações em uma só

Espero que você aproveite muito bem essa ferramenta!

_Está na mão! É só ação!_

## Apêndice 1 - Um modelo para criar Populações:

> + ___Nome da População:___
> + ___Descrição:___
> + ___Características:___
> + ___Discórdia (duas coisas opostas):___
> + ___Perícias:___    
 
## Apêndice 2 - Criando um individuo usando Populações:

1. Defina o nome
2. Descreva o indivíduo
3. Escolha uma ou mais ___Populações___ das quais o personagem faça parte (entre duas e três estará de bom tamanho)
4. Escolha uma (e apenas uma) ___Característica___ de cada uma das populações - considere-as Aspectos do mesmo
5. Baseando-se nas Discórdias envolvidas, descreva a ___Agenda___ do personagem
6. Descreva o ___Custo___ que os personagens terão ao interagirem com a ___Agenda___ do personagem, para o bem ou para o mal
7. Pegue a lista das perícias das Populações envolvidas e ajuste-as para as ___Perícias___ do personagem
8. (opcional) complemente com Façanhas que sejam relevantes ao personagem

## Apêndice 3 - Algumas Populações úteis em Fantasia

+ Os ___Suffis___ são ___Místicos, Sábios___ e ___Teimosos___, sentindo-se pressionados entre _viver uma vida monástica_, guardando seu conhecimento apenas para si mesmo ou _oferecer sua sabedoria_ aos interessados, tanto do Deserto quanto do Sultanato. Todos possuem _Conhecimento_ em no mínimo _Bom (+3)_
+ Os poderosos ___Jannis___ são ___Mágicos, Arredios___ e  ___Honrados___, divididos entre a ideia de _Trabalhar com os povos locais_, oferecendo sua magia e conhecimentos para auxiliar os mesmos, ou os _Dominar_, usando sua magia para dominar os povos mortais, obviamente mais fracos que os mesmos. Todos possuem _Magia (Qualquer tipo)_ em no mínimo _Excepcional (+5)_
+ Os esquivos integrantes da ___Corte do Sultanato___  são ___Ambiciosos, Melífluos___ e ___Sábios___, divididos entre suas _Obrigações_ com a População do Sultanato e as _Disputas_ por poder e influência. Todos possuem _Comunicação_ em no mínimo _Bom (+3)_
+ Os ___Magos Alquimistas___  tendem a serem ___Místicos, Sábios___ e ___Teimosos___, divididos entre pesquisarem a _Sabedoria_ das Alquimia para auxiliar a população ou obter _Poder_ Místico para seus fins. Todos possuem _Alquimia_ em no mínimo _Bom (+3)_
+ Os ___Janni Aprisionados___ tendem a ser ___Subservientes, Arredios___ e ___Mágicos___. Eles ficam divididos entre enfrentar os poderosos feitiços que os prendem aos objetos de poder que seus captores controlam, buscando com isso a _Liberdade_, ou então aceitar de maneira _Fatalista_ o Destino de serem para sempre serviçais de outrem, e tentar fazer algo a partir disso. Todos possuem _Magia (Qualquer tipo)_ em no mínimo _Bom (+3)_
+ Uma das mais misteriosas entidades no Deserto, a ___Corte dos Janni___ é composta de seres ___Mágicos, Tradicionalistas___ e ___Honrados___. Apesar de seus milhares de anos, eles ainda possuem dúvidas, e são divididos entre aqueles que acham que os _Janni_ devem _Colaborar_ com mortais e outras criaturas visando um melhore entendimento de todos no deserto, ou se _Isolar_, envolvendo-se apenas quando coisas ocorrerem que afetem os _Janni_, agindo sempre de maneira rápida e direta quando tais coisas ocorrerem. Eles possuem em geral _Magia (qualquer tipo)_ em nível _Excepcional (+5)_ ou melhor.
+ Os ___Moradores dos Vilarejos___ costumam ser ___Ordeiros, Trabalhadores___ e ___Desconfiados em relação a forasteiros___ e se sentem divididos entre _Ajudarem os viajantes_ perdidos no meio das vastidões geladas e _Fecharem-se em Copas_, por já terem muitos problemas eles próprios. Todos possuem _Comunicação_ Bom (+3) ou melhor;
+ As ___Matilhas de Lobos___ do Ártico costumam ser  ___Cruéis, Honrados___ e ___Territoriais___, sentindo-se divididos entre _Manterem seu Território_, aceitando o que a Natureza lhe dá e apenas mantendo forasteiros longe e _Tomar tudo o que puderem_, não se preocupando com o amanhã. Todos costumam ter _Furtividade_ Bom (+3) ou melhor;
+ As ___Raposas___ do Ártico costumam ser  ___Trapaceiras, Sábias___ e ___Esquivas___, sentindo-se divididas entre _Observar_ os acontecimentos, e com isso ver como agir, e _Agir_ de modo a obterem o que desejam, não se preocupando com o amanhã. Todas costumam ter _Enganar_ Bom (+3) ou melhor;
+ Os ___Corvos Malignos___ que vivem nos bosques são ___Traiçoeiros, Perceptivos___ e ___Fanfarrões___, sendo divididos entre _Guardar seus conhecimentos para si_ e com isso ganhar a vantagem no momento certo e _Usá-los contra seus inimigos_, eliminando-os de uma vez por todas. Todos possuem _Conhecimento_ Bom (+3) ou melhor;
+ As ___Corujas___ que vivem nos bosques são ___Sábios, Perceptivos___ e ___Misteriosos___, sendo divididos entre _Guardar seus conhecimentos para si_ e com isso ganhar a vantagem no momento certo e _Compartilhar eles com as pessoas necessitadas_, ajudando pessoas perdidas nos bosques gélidos. Todos possuem _Conhecimento_ Bom (+3) ou melhor;
+ Os ___Trolls___ que vivem no meio da vastidão gelada são ___Burros, Teimosos___ e ___Brigões___, e costumam sentir-se divididos entre _Cuidar dos seus_ e deixar os outros em paz e _Destruir tudo_ em sua sede de sangue. Todos possuem _Lutar_ Ótimo (+4) ou melhor;
+ As Terríveis ___Megeras___ são criaturas com forma de mulher, mas tão abomináveis em aparência quanto o são em seus corações, com cabelos desgrenhados e sujos, dentes pontiagudos e amarelos e narizes aduncos e gigantes. ___Traiçoeiras, Melífluas___ e ___Mágicas___ são divididas apenas por seus métodos: algumas acreditam que serem _Sutis_ é a melhor forma de alcançar seus objetivos, enquanto outras preferem provocar _Destruição_ sempre que podem, como exemplo de seu poder. Todas possuem algum tipo de _Magia_ (normalmente Zaps) em Bom (+3) ou Melhor;
+ As ___Criaturas Mágicas da Floresta___ são ___Mágicas, Mexeriqueiras___ e ___Travessas___, e estão sempre divididas entra ___Apenas Assistir___ o que está acontecendo e ___Interferir___, usando pequenas trapaças e magias para ajudar (ou atrapalhar) as pessoas perdidas. Todas possuem _Magia (Geasa)_ em _Bom (+3)_ ou melhor
+ Os ___Gnomos da Floresta___ são ___Engenhosos, Ordeiros___ e ___Mágicos___, sempre tendendo entre _Ajudar_ os povos da Floresta e _Cuidarem de sua Própria vida_, não arrumando dificuldades a mais do que as que já possuem. Todos costumam ter algum tipo de _Magia_ em _Bom (+3)_ ou melhor;
+ Os ___Altos Elfos da Floresta___ são ___Tradicionalistas, Arrogantes___ e ___Mágicos___, sempre tendendo entre _Ajudar_ as pessoas contra seus inimigos maiores e _Não intervirem_, exceto quando vão para a preservação da sua floresta. Todos costumam ter algum tipo de _Magia_ ou _Conhecimento_ em _Excepcional (+5)_ ou melhor;
+ Os ___Velhos e Velhas Sábios da Floresta___ são ___Sábios, Irreverentes___ e ___Místicos___, sempre tendendo entre _Estudar_ cada vez mais suas artes místicas e _Observarem os Acontecimentos_ e ocasionalmente se envolver neles. Todos costumam ter algum tipo de _Conhecimento_ em _Bom (+3)_ ou melhor;
+ As ___Fadas Madrinhas___ são ___Teimosas, Sábias___ e ___Criativas___, estando divididas entre o papel de _Juízas_, entidades inquestionáveis que decidem quem é digno ou não de ajuda mágica, e _cuidadoras_, mentoras que podem falhar enquanto oferecem suporte, ao invés de recompensas e punições. Todas possuem no mínimo _Excepcional_ em _Magia Maior_;
+ Os ___Caçadores e Lenhadores___ são ___Tradicionalistas, Desconfiados___ e ___Bondosos___, estando divididos entre _Auxiliarem pessoas_ que estejam perdidas na Floresta ou de outra forma necessitada e _Cuidar da sua própria vida_, armando as Armadilhas ou afiando suas armas. Todos possuem _Lutar_ ou _Ofícios_ em _Bom (+3)_ ou melhor;
+ As ___Pessoas Magicamente Perdidas___ são ___Amaldiçoadas, Desesperadas___ e ___Dignas de Pena___, estando divididos entre _Implorarem por socorro_ de qualquer um que aparente ser capaz de ajudar e _Capturar pessoas_ que possam os ajudar e forçá-los a o fazer, sob pena de os próprios personagens ficarem na mesma situação. Todos possuem _Empatia_ ou _Comunicação_ em _Bom (+3)_ ou melhor;
+ Os ___Marinheiros dos Sete Mares___ tendem a ser ___Práticos, Arredios___ e ___Incultos___, tendendo entre apenas _Ajudar_ navios e pessoas perdidas no mar ou a _Cuidarem de si mesmos_ de maneira indiscriminada. Em geral, possuem _Pilotar_ e _Conhecimentos_ em _Bom (+3)_ ou melhor;
+ Os ___Piratas dos Sete Mares___ tendem a ser ___Práticos, Cruéis___ e ___Incultos___, tendendo entre apenas _Saquear_ navios e manterem as pessoas vivas ou a _Matar e Pilhar_ de maneira indiscriminada. Em geral, possuem _Pilotar_ e _Lutar_ em _Bom (+3)_ ou melhor;
+ Os ___Capitães dos Sete Mares___ tendem a ser ___Espertos, Autoritários___ e ___Ambiciosos___, tendendo entre serem _Benfeitores_, mantendo suas tripulações bem pagas e satisfeitas como forma de manterem a mesma na linha, ou _Cruéis_, mantendo-as a pão e água enquanto tomam para si a maior parte, senão TODO, o espólio. Em geral, possuem _Pilotar_ e _Comunicação_ em _Bom (+3)_ ou melhor;
+ Pessoas ___Magicamente Tocadas___ tendem a ser ___Mágicas, Fora da Realidade___ e ___Incompreendidos___, tendendo entre _Colocarem seus Dons Mágicos em uso_ a outros, oferecendo ajuda aos que necessitam, e _Viverem sua própria vida_, se afastando de pessoas e lugares que não os compreendem. Todos possuem _Magia (qualquer tipo)_ em _Bom (+3)_ **ou** alguma perícia em _Fantástico (+6)_ ou melhor;
+ Os ___Habitantes das Ilhas___ dos Sete Mares tendem a ser ___Desconfiados, Irreverentes___ e ___Tradicionalistas___. Podem tanto _Ajudar_ marinheiros perdidos, até mesmo os tornando Reis e Rainhas das Ilhas, ou _Não se envolverem_, deixando que o Mar decida quem deve viver ou morrer. Todos possuem _Comunicação_ em _Bom (+3)_
+ Muitas tribos possuem ___Xamãs___, praticantes de formas estranhas de magia, que tendem ___Misteriosos, Estranhos___ e ___Místicos___, se dividindo entre usar sua Magia para o _Bem da Tribo_, ajudando a mesma a crescer, ou a usar em _Seu Próprio Benefício_, preferencialmente colocando a tribo em submissão aos mesmos. Todos possuem _Magia (qualquer tipo)_ em _Bom (+3)_ ou melhor;
+ As  ___Crianças do Vilarejo___ tendem a ser _Obedientes, Aventureiras_ e _Energéticas_. Costumam ficar divididas entre _Aventurar-se_ e aprender sobre as coisas pela vida, ou _se comportarem_ e aprender as coisas pelas experiências dos adultos. Costumam ter _Atletismo_ ou _Comunicação_ em _Regular (+1)_ ou melhor;
+ Os ___Adultos do Vilarejo___ tendem a ser _Reverentes, Educados_ e _Preocupados_. Costumam estar divididos entre _Cooperar_ uns com os outros de modo a fazer a vila ou cidade crescer e _Cuidarem da sua própria vida_ e deixar que os demais cuidem das mesmas. Costumam ter _Conhecimentos_, _Ofícios_ ou _Comunicação_ em _Razoável (+2)_  ou melhor;

<!--  LocalWords:  Trolls Zaps Geasa
 -->
