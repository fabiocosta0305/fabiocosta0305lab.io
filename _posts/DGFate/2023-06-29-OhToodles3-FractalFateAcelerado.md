---
title: "Oh, Toodles! 3 - As Ferramentas em Fate do Mr. Mickey - Fractal com o Fate Acelerado"
subheadline: Uma Introdução ao Fate 
date: 2023-06-29 15:10:00 +0300
layout: post
categories:
 - RPG
tags:
 - Fate
 - Fate Core
 - Fate-Core
 - Artigos
 - Dungeon Geek
 - Introdução ao Fate
 - Domando o Fate
 - Iniciando no Fate
 - Ações
 - Teste
 - Rolamento
 - introducao-ao-fate
order: 23
header: no
excerpt_separator: <!-- excerpt -->
---

> _"Oh, Toodles!"_

Mais um artigo da série _Oh, Toodles!_.

Uma coisa muito legal sobre o Fate é o _Fractal_ do Fate, um recurso que é indispensável para o Narrador criar regras especiais para o Fate. Isso é importante porque, ao invés de criar subsistemas para coisas que não sejam Aspectos, Habilidades, Façanhas, Estresse ou Consequências, você utiliza toda uma combinação desses elementos para, com isso, utilizar as mesmas regras que se aplicam aos personagens a tais elementos.

Mas é uma ferramenta muito abstrata, até demais para o narrador/jogador iniciante no Fate. Como entender essas modificações? E o que pode ser ou não bem utilizado em um momento ou outro.

Já falamos um tanto sobre isso [lá no começo dessa série, ao falar de Extras](https://www.dungeongeek21.com/iniciando-no-fate-extras-e-o-fractal-do-fate/), inclusive com uma série de considerações, mas aqui o foco é mais de tentar entender o que você deve pensar e levar em consideração ao criar um Fractal.

Bem, vamos mostrar como pensar em um Fractal... E para isso, vamos utilizar um pequeno _cheat_: vamos utilizar como base total o Fate Acelerado.

Então, estão prontos para ver como desenvolver e utilizar um Fractal?

Então digam: _Oh, Toodles!_

<!-- excerpt -->

## Retornando às bases

Primeiro, vamos relembrar algumas definições importantes antes de começar:

Primeiramente, vamos rever o conceito de _Extra_: o Extra em Fate é tudo que não é Aspecto, Habilidade, Façanhas, Estresse e Consequências. Ele pode ser ou não composto por tais elementos e na prática é tudo que pede algum tipo de "regra especial". Magia, Organizações, Veículos, Dispositivos Especiais, e por aí afora, tudo isso é tratado pelo Fate como um Extra.

Ocasionalmente, Extras também podem demandar _Permissões_ (o custo relacionado a ter acesso a determinado Extra) e _Custos_ (como ser melhor nas coisas relacionadas a esse Extra).

Dito isso, vamos definir o Fractal, lembrando da descrição no [_Fate Básico_](http://fatesrdbrasil.gitlab.io/fate-srd-brasil/fate-basico/extras/#a-regra-de-bronze-ou-fate-fractal):

> ***Em Fate, tudo pode ser tratado como se fosse um personagem. Qualquer coisa pode possuir aspectos, perícias, façanhas, barra de estresse e consequências se for preciso.***

Ou seja, você pode, na teoria, ter uma ficha de personagem completa para qualquer coisa que seja interessante de ser tratado dessa forma dentro do Fate. Um caso extremo é o de SLIP, um Mundo de Aventura onde _A PRÓPRIA CAMPANHA_ possui uma ficha de personagem e, portanto, pode _ATACAR E SER ATACADA_ pelos personagens!

Dito isso, já relembramos o que são o Extra e o Fractal. Então precisamos lembrar o que levar em consideração ao definir um Fractal. Os elementos mais importantes de um Fractal são os mesmos de um personagem: _Aspectos, Habilidades, Façanhas, Estresse e Consequências_ (e Extras, se aplicável)

E como definir o que vai ser usado ou não? Novamente, como dito no livro do [Fate Básico](http://fatesrdbrasil.gitlab.io/fate-srd-brasil/fate-basico/criando-um-extra/#atribuindo-elementos-ao-personagem):

> + Se o extra influencia a história, então deve usar **Aspectos**.
> + Se o extra cria um novo contexto para a ação, então ele deve usar **Habilidades**.
> + Se o extra permite realizar algo extraordinário com uma perícia, então deve usar **Façanhas**.
> + Se o extra pode sofrer dano ou ser consumido de alguma forma, então deve **poder receber estresse e consequências**.

OK... Dito isso tudo, vamos aplicar ao nosso exemplo, onde criaremos um Extra de Culturas, nos baseando no Fate Acelerado.

> ### E por que usar o Fate Acelerado?
> 
> Um dos _game sharks_ menos usados no Fate é o Fate Acelerado: o Fate Acelerado não apenas te dá um jogo rápido para introduzir novos jogadores, mas para narradores experientes pode servir de base para Fractais que tenham e executem ações nos bastidores que possam ser influenciados ou influenciar os jogadores de maneira muito ativa, em uma espécie de "jogo nos bastidores", ou que possa ser usado em momentos onde a ação sai dos jogadores e vai para alguma outra coisa, como em uma perseguição de carros, onde os carros são o foco, mas os jogadores ainda podem influenciar as ações dos carros e ser influenciados pelos carros.

## Influenciando a História - Aspectos

Para começar, vamos entrar nos Aspectos.

Como no caso dos Aspectos dos Personagens, precisamos deixar claro o que queremos definir com os Aspectos de cada Cultura. Como estamos usando o Fate Acelerado como base, vamos nos ater ao mesmo limite de Cinco Aspecto para a Cultura. 

> A vantagem de chamarmos de _Cultura_ é que isso pode se aplicar não apenas a grupos com territórios estabelecidos, mas para minorias dentro desses territórios e culturas apátridas, e suas interações (!!!)

+ Primeiro, vamos definir um conceito para essa Cultura, mas que chamaremos de **Fundamentos**. Os Fundamentos são a definição mais básica e fundamental de uma Cultura, e isso ditará uma série de interações com outras culturas, assim como seu impacto no mundo e motivos para ações com ou contra ela.
+ Em seguida, vamos colocar o equivalente à Dificuldade, na forma de um **Estereótipo**. O Estereótipo deve ser visto como uma visão reducionista e quase sempre negativa, mas com certo fundo de verdade, em relação àquela cultura. Desse modo, ela pode ser direta ou indiretamente relacionada aos Fundamentos daquela Cultura, mas em geral será algo mais intransigente, valores que são vistos como um grande problema pelo resto do mundo.
+ Vamos colocar um terceiro Aspecto nomeado, chamado **Relações**. Ele funciona como a **Motivação** do Fate Condensado (e de outros jogos Fate), para ligar essa Cultura a outras no mundo, ao menos próximas a elas. Isso é importante, pois nenhuma Cultura é solta no mundo, mesmo que ela seja ___Isolacionista e Xenofóbica___. Ela só o é pois essas relações ocorrem com o mundo exterior a ela própria.
+ Um quarto Aspecto interessante de ser demandado ou nomeado é o Aspecto de **Feitos**: uma Cultura sempre tende a ser conhecida externamente por alguma coisa que tenha sido feito por ela, mesmo que ocasionalmente a visão disso possa ser terrível (o que coloca isso próximo do ___Estereótipo___). Esses ***Feitos*** tendem a ser algo que o coloca como alguém a ser de alguma forma respeitado: Hobbits do Condado são ___Moles como Manteiga, Mas Duros como Cascas de Velhos Carvalhos___, o que indica sua empatia e determinação quase obstinada pela qual seriam reconhecidos.
+ O último Aspecto deixaremos Livre, pois isso pode dar chance para adicionar-se novas Relações e Feitos.

Vejamos um exemplo de aplicação dessa regra:

> Holtz, um reino formado pela consolidação de vários estados guerreiros no meio do Continente de Aleste, é um dos principais reinos desse Continente. Portanto, será necessário descrever o mesmo na forma de uma Cultura segundo essas regras.
> 
> Os ___Fundamentos de Holtz___ são baseados na cultura paladínica, já que seu fundador, Rosenkrantz, foi o mais poderosos dos reis guerreiros da região, que em sua luta por justiça unificou os reinos e tornou-se uma Deidade. Um bom Aspecto para os Fundamentos de Holtz é relacionado aos **_Éditos dos_ Büchen des Rozenkranz _que guiam uma nação_**: Rozenkranz, antes de ascender ao status de Deidade, escreveu um gigantesco arcabouço de conhecimento, os _Büchen des Rosenkranz_ (Livros de Rosenkranz), tratados sobre filosofia, política, estratégia militar e outros tópicos. Esse livro procura indicar um meio de vida que é seguido pelos Holtzerinn como filosofia de vida, e determina a maior parte das regras que um Holtzerinn irá usar em sua vida
> 
> Já o ___Estereótipo___ associado a Holtz tem a ver com isso: devido a seu estoicismo exagerado e severidade em relação a leis e ordem, os  Holtzerinn tendem a serem vistos como ___Incultos, Simplórios e Violentos___, pouco mais que bárbaros com belas armaduras e armas. Quem conhece bem os Holtzerinn sabe que eles possuem canções belas e danças energéticas, incluindo as _Schwerttänze_, danças vigorosas e vistosas que seus guerreiros (e quase todo Holtzerinn) executam usando suas armas como elementos performáticos, mas normalmente o típico visitante não verá o suficiente dos Holtzerinn para ver além dessa casca
> 
> Nas suas ___Relações___, a mais curiosa é o desprezo total pelo reino vizinho: na visão de Holtz, ___Salvare é um reino de cobras, e sempre é bom observar cobras bem por perto___. Isso se deve ao fato de que Salvare é um reino cosmopolita e sorrateiro, mais dado a subterfúgios e outros artifícios para alcançar seus objetivos ao invés dos negócios honestos e das trocas de golpes no campo de batalha. Entretanto, mesmo Holtz sabe da importância de ficar atento aos movimentos de Salvare, para evitar que eles se cresçam demais. E negociar ocasionalmente com eles pode ser uma boa ideia.
> 
> Nos ___Feitos___ de Holtz, sem sombra de dúvida a Ascensão de Rosenkrantz e a formação da ___Igreja dos Sacros Reis Guerreiros de Rosenkrantz___ é a principal fonte de reputação dada a Rosenkrantz: mesmo em reinos vizinhos existem Paladinos de Rosenkrantz levando a palavra do ___Herói da Espada e da Acha___ para todos os guerreiros, prometendo iluminação na batalha e força para, mesmo no momento da Morte, sorrir ao ser enviado aos Campos dos Heróis por um guerreiro honrado, não por um bárbaro homicida. Isso torna a Igreja de Rosenkrantz e seus Paladinos conhecidos por todo o Continente, e com isso Holtz
> 
> No ___Aspecto Livre___, vamos colocar uma ___Relação___ extra, mas mais interna que externa: a Igreja de Rosenkrantz tem uma relação complicada com um culto druídico antigos, que prega a paz e o respeito total à vida e abomina a morte violenta, mesmo aquela dada como justificável diante dos Éditos de Rosenkrantz. Esses ___Druidas da Goldener Reh___ a Ordem da Corça Dourada, são um problema quando se metem nos assuntos do governo, em especial quanto à necessidade de derrubada de árvores e abertura de minas para obter recursos importantes para a evolução da nação, mas ___Não se pode negar o poder e a influência dos Druidas___, e mesmo os próprios seguidores de Rosenkrantz sabem que é pragmático aceitar ocasionalmente a cura e as bençãos deles, mesmo de maneira reticente.
> 
> Portanto, os Aspectos de Holtz são:
>
> |      ***Tipo*** | ***Aspecto***                                                                |
> |----------------:|------------------------------------------------------------------------------|
> | **Fundamentos** | _Éditos dos_ Büchen des Rozenkranz _que guiam uma nação_                     |
> | **Estereótipo** | _Incultos, Simplórios e Violentos_                                           |
> |    **Relações** | _Salvare é um reino de cobras, e sempre é bom observar cobras bem por perto_ |
> |      **Feitos** | _Igreja dos Sacros Reis Guerreiros de Rosenkrantz_                           |
> |                 | _Não se pode negar o poder e a influência dos Druidas_                       |

## Contexto nas Ações - Competências

Uma coisa muito interessante é pensar em como um Extra ou Fractal pode atuar e interagir com o jogo.

E aqui fica claro a melhor forma de determinar se um extra deve ter Competências (Lembrando que falamos de Competências como um termo global para Habilidades/Abordagens/etc...), saindo da definição do Fate Básico:

> Se o seu Extra interagir diretamente ou indiretamente em relação a ações dos personagens ___de maneira independente a eles___, o seu Extra deveria ter Competências.

O que isso quer dizer?

Se o seu Extra for basicamente "inútil" sem que o personagem o envolva diretamente, ou seja, se este Extra for relacionado apenas a ações que sejam de proatividade exclusiva do Personagem, esse Extra pode ter outras formas de melhorar as possibilidades do personagem (provavelmente por meio de uma Façanha).

O exemplo dessa situação seria um cenário onde os Deuses não interagem diretamente com o mundo, o fazendo única e exclusivamente por meio de clérigos, paladinos e afins, provendo-o com poderes mágicos e domínios de poder e afins. Desse modo, não há interação direta dos Deuses nesse mundo, portanto não existe necessidade de Competências nesse caso. Os poderes divinos poderiam ser representados por outro caminho.

Entretanto, se houver momentos do jogo onde a interação do Extra for mais importante que as ações do jogador e/ou acontecer de maneira independente aos mesmos (ainda que possam suportar ou ser suportados por ações dos personagens), certamente esse Extra deveria ter Competências.

Pensando nesse caso, um cenário onde os Deuses atuem de maneira mais direta no mundo poderia demandar que os Deuses tivessem Competências para agir no mundo, _mudando o contexto das coisas!_ (lembram da definição lá em cima, a oficial do Fate Básico) Ao fazer isso, eles precisam ter como determinar seu real impacto no mundo. Embora suas ações possam estar em uma escala fora da dos personagens, ainda assim suas ações serão diretas, independentes e impactarão o mundo, portanto precisam de uma competência.

### E no caso de haver interação entre os dois, como determinar a "parte dominante"?

Não existe uma parte dominante nesse caso. Se o seu Extra pode agir no mundo, ele pode agir no mundo *tanto quanto os personagens*. Você pode criar alguma regra para limitar que um Extra poderoso seja usado o tempo todo, e por aí afora, mas sem nenhuma dessas limitações, tecnicamente o Extra pode agir tanto quanto os personagens.

De fato, vamos pegar três exemplos onde Extras atuam em relação ao cenário em conjunto dos personagens: 

+ Nações de [Romance in the Air](https://evilhat.itch.io/romance-in-the-air-a-world-of-adventure-for-fate-core);
+ A Convergência de [SLIP](https://evilhat.itch.io/slip-a-world-of-adventure-for-fate-core/);
+ As Fadas em [Boa Vizinhança](https://evilhat.itch.io/good-neighbors-a-world-of-adventure-for-fate-core/);

Em _Boa Vizinhança_, cada jogador cria dois personagens que são conectados um ao outro de alguma maneira (podemos dizer que um personagem é um Extra do Outro), um humano e uma Fada defendendo sua cidade e os locais mágicos e sobrenaturais da mesma, junto com suas culturas e tradições das garras de uma Grande Fábrica que tem como único objetivo O Lucro (parece familiar, né?). Cada um age em seu turno (cada cena é dividida em duas, uma onde os humanos ajam e outra onde as Fadas ajam), e nesse cenário temos as ações de ambos os lados se afetando. 

> Em especial, a ação de _Criar Vantagem_ é muito importante aqui: se Scintilla, a _pixie_ que encontrou Rafael, descobriu pistas sobre uma ___Substância Tóxica e Fedorenta___ perto do Círculo Feérico da Floresta do Urso Malhado, essa Aspecto pode ser útil para que Rafael descubra os ___Resíduos Tóxicos___ que a Grande Fábrica está utilizando contra as águas, que podem estar aumentando os casos de Câncer na cidade!

Em _Romance in the Air_ as coisas são mais ou menos similares, mas um pouco mais desassociadas: os personagens são inocentes cidadãos dos mais poderosos Impérios da Europa Vitoriana (ou assim uns imaginam que os outros sejam) em uma viagem por toda a Europa em uma barca voadora, envolvendo em aventuras amorosas, flertes, diversão e tensão, enquanto suas pátrias-mães estão se movendo no _Grande Jogo_[^1] 

> Aqui pode ser interessante deixar claro que o quanto os personagens (e os jogadores) vão se envolver no Grande Jogo vai depender deles. Entretanto, o Grande Jogo provavelmente sangraria diante dos personagens. E a coisa pode ir na outra direção: talvez aquele professor sueco na verdade seja um espião para o Império Moscovita, que por baixo de sua ideia de investigar a disseminação de certas orquídeas pela Europa estaria em uma missão para espalhar ordens para eventos focando tumultuar a Europa...
>
> ... e que justamente a jovem donzela Britânica deseja evitar, focando em descobrir informações secretas de outros países que possam estar agindo por meio de agentes nessa barca voadora.

Já em SLIP, o Narrador deve definir o nível da Convergência (a "realidade parasita" que está tentando invadir e dominar a nossa) no início da campanha, e as ações dos personagens e da Convergência, _COMO UM CONFLITO_, vão determinar os resultados da campanha.

> Entretanto, existem regras sobre como a Convergência e os personagens podem se Atacar ou interagir uns com os outros.
> 
> + A Convergência possui um número de ações que pode ser usadas durante cada sessão, dependendo de uma das perícias que a Convergência possui. A Convergência então pode executar ações para corromper personagens, exterminá-los, ou isolá-los de outras pessoas, tornando a realidade bizarra ao redor deles;
> + Personagens, entretanto, só podem atacar a Convergência Diretamente se descobrirem ___Aspectos___ que justifiquem tais ações: basicamente seria como eles descobrirem não apenas que a realidade está mudando, mas como ela está mudando, e o porquê tal Aspecto justifica a ação. Eles vencem se derrotarem a Convergência.

Bem... Dito isso, vamos voltar ao nosso Extra com o Fate Acelerado.

### As Capacidades das Culturas

Voltando a nossas Culturas, vamos determinar as competências da Cultura. Vamos utilizar o termo Capacidades, assim meio que dá uma maneira de não confundir com Habilidades e Abordagens que estejam sendo usadas.
 
No caso, uma forma de fazer é a tradução um-a-um de cada Abordagem para uma dessas Capacidades. Poderíamos adotar um número igual de Capacidades quaisquer, mas aqui essa abordagem na ação de fractalizar ajuda a manter um foco em algo que já existe, mas apenas está sendo editado.

Agora, vamos lembrar que temos seis Abordagens:

+ Ágil
+ Cuidadoso
+ Esperto
+ Estiloso
+ Poderoso
+ Sorrateiro

Vamos então pensar em como mapear as Capacidades nas Abordagens:

+ Para _Ágil_, podemos adotar ***Comércio***: é conhecido que o Comércio foi uma forma de proliferar rapidamente informações e ideias entre Culturas (pense na questão, por exemplo, dos números arábicos ao invés dos Romanos);
+ Para _Cuidadoso_, podemos usar ***Filosofia***: em muitos casos, grandes filósofos e mestres religiosos foram companheiros e conselheiros de grandes líderes políticos e militares (como no caso de Alexandre O Grande e de muitos dos Reis da China);
+ Para _Esperto_, vamos adotar ***Ciência*** como Capacidade: ela indica o desenvolvimento em termos de ciência e conhecimento daquela Cultura em relação às demais. Pense na questão da disseminação de elementos culturais greco-romanos ou árabes;
+ Para _Estiloso_, vamos utilizar ***Cultura***: às vezes, uma grande capacidade de criação de arte e outros elementos culturais retorna na forma de influência local e afins. Pense na cultura judaico-cristã no final do primeiro e início do segundo século, e a cultura flandrina e florentina no início do Renascimento;
+ Para _Poderoso_, o óbvio é usar ***Militar***: isso não significa apenas números, mas aqueles com melhores armas, conhecimento tático, treinamento e afins. Isso pode explicar os espartanos ou as tropas militares na época do _Bakumatsu_ no Japão, algumas vezes muito menores, vencerem tropas gigantescas;
+ Para _Sorrateiro_, vamos pensar em ***Espionagem***: o uso de batedores, espiões, cortesãos e outros tantos súditos leais infiltrados em outras culturas sempre pode ser vantajosa. Nas duas direções: afinal de contas, disseminar informações falsas é um elemento bastante válida de agir. A questão da Guerra Fria e suas ações de inteligência e Contra-Inteligência são sempre uma opção para entender-se esse tipo de abordagem

> Vamos ver então as Capacidades da Cultura de Holtz.
> 
> Podemos pensar que obviamente ***Militar*** será a capacidade de nível _Bom (+3)_ de Holtz, já que eles são focados em muitas questões militares: toda a história de Holtz é focada em culturas de guerra e batalhas, mesmo em batalhas simuladas e disputas amistosas de formas não letais de combate.
> 
> Por outro lado, a capacidade *Medíocre (+0)* de Holtz sem sombra de dúvida seria ___Espionagem___: os Holtzerinn são conhecidos por serem extremamente honestos, e não são habituados à arte da decepção e enganação, para o bem e para o mal.
> 
> Para as Capacidades _Razoáveis (+2)_, vamos colocar elas em ___Cultura___ e ___Filosofia___: os _Büchen der Rosenkrantz_ são tratados filosóficos extremamente respeitados fora de Holtz, e as _Schwerttänze_ são danças vigorosas e alegres, apreciadas em muitos campos de batalha e até mesmo ocasionalmente em salões de festas, mesmo pelos inimigos mais jurados de Holtz! Existe também toda uma série de outras contribuições de menor porte de Holtz em ambos os Aspectos.
> 
> Para as Capacidades _Regulares (+1)_ de Holtz, portanto, sobram ___Comércio___ e ___Ciência___: os Holtzerinn, até pela sua honestidade, não costumam ser usurários e nem perdulários, portanto suas trocas comerciais tendem a ser mais básicas, talvez com possíveis elementos como metais especiais de outros reinos, que sua _Ciência_ ainda consegue manipular de maneira incipiente, mas não os produzir. A _Ciência_ Holtzerinn ainda é meio incipiente, embora possuam uma metalurgia excepcional, capaz de fazer todo tipo de objetos de metal (em especial armas) com excepcional qualidade!
> 
> Então, as Capacidades de Holtz ficam assim
> 
> | ***Capacidade*** | Nível           |
> |-----------------:|-----------------|
> |     **Comércio** | _Regular (+1)_  |
> |      **Ciência** | _Regular (+1)_  |
> |      **Cultura** | _Razoável (+2)_ |
> |   **Espionagem** | _Medíocre (+0)_ |
> |    **Filosofia** | _Razoável (+2)_ |
> |      **Militar** | _Bom (+3)_      |


## Coisas Extraordinárias - Façanhas (ou Recursos, como chamaremos)

As Façanhas sempre representam as coisas extraordinárias que um personagem pode fazer: alguma habilidade ou perícia especial, equipamentos, contatos, entre outras coisas interessantes que possa ser usado pelo personagem. E para fractais baseados no Fate Acelerado, não muda.

Uma Façanha nesse caso pode ser algum tipo de recurso a ser utilizado pela Cultura para se proteger ou para evoluir sua influência sobre as demais, seja de maneira agressiva ou não, visando prejuízo ou não (isso é determinado pelas ações).

E como estamos baseando-no no Fate Acelerado, podemos utilizar exatamente as mesmas fórmulas sugeridas para Façanhas:

> + Como eu _[coisa especial que sou/tenho]_ recebo +2 ao _[uma das quatro ações: Superar, Criar Vantagem, Atacar, Defender]_ de maneira _[Uma das capacidades: Comércio, Ciência, Cultura, Espionagem, Filosofia, Militar]_ quando _[circunstância especial que deve ocorrer]_; **OU**
> + Como eu _[coisa especial que sou/tenho]_, _uma vez por sessão_ posso _[Descreva uma exceção de regra]_

Sem grandes mistérios aqui, podemos dizer que o mesmo tipo de criação de Façanhas do Fate Acelerado está disponível, na forma de três Recursos por Cultura (não há nenhum mecanismo similar à Recarga aqui, a não ser que você deseje algum recursos de Intervenção - falaremos adiante)

> Vejamos como criar alguns recursos para Holtz:
> 
> Mencionamos anteriormente que os Holtzerinn são conhecidos por sua Cultura Paladínica. Nesse caso, poderia ser interessante tornar isso uma maneira de aprimorar seu Ataque por ___Militar___. Mas aqui pode ser interessante se for feito de outra maneira: Criando Vantagem por ___Cultura___ já que os mitos de Cavalaria de Holtz são extremamente conhecidos por todo o Continente!
> 
> + ___Contos de Cavalaria:___ como somos uma Cultura Paladínica, nossos Contos são conhecidos por todos os lados, e portanto recebemos +2 ao ___Criar Vantagens___ por ___Cultura___ ao disseminar nossa cultura e formas de ver o mundo usando tais contos pelo Continente;
> 
> Por outro lado, sem sombra de dúvidas, os Holtzerinn são extremamente resilientes em Batalha: os _Büchen der Rosenkrantz_ são ensinados a todos os habitantes de Holtz desde a infância, não importa sua função na sociedade. Esse estoicismo e tradições de batalha e filosofia os tornam um povo tão resiliente que a resistência deles a qualquer forma de ataque é monstruosa! Claro que mesmo a mais poderosa das espadas pode se partir, mas ninguém que tenha tentado partir a Lança de Rosenkrantz (o apelido do reino de Holtz) sobreviveu para contar a história!
>  
> + ___A Resiliência de Rosenkrantz:___ devido a nossa filosofia estoica, vinda dos _Büchen der Rosenkrantz_, podemos resistir a Ataques por qualquer meio que vise nos isolar de recursos importantes para nossa sobrevivência usando ___Militar___, com +2 no rolamento;
> 
> Por fim, essa mesma formação de nação voltada à cavalaria torna Holtz um grande celeiro de Guerreiros e Ordens de tremenda reputação, e tais Guerreiros e Ordens sempre vêm ao socorro de Holtz. Esse elemento poderoso pode fazer toda diferença em um momento de guerra ou escaramuças mais complexas!
> 
> + ___Ordens de Heróis:___ devido às diversas Ordens Heroicas que existem em Holtz, Holtz pode, ___uma vez por sessão___, trazer para jogo uma Ordem para agir em seu nome em qualquer situação, tanto em combate quanto fora dele. Role a Capacidade adequada ao evento (normalmente, mas não obrigatoriamente, ___Militar___). O esforço resultante determinará o Poder dessa ordem como um ___Aspecto Nominal___ de valor equivalente (como ___Die Walküren der Rosenkrantz___ _Ótimo (+4)_). O Narrador definirá uma ___Dificuldade___ com valor _Terrível (-2)_ (se Esforço ficar abaixo de ___Bom (+3)___) ou _Ruim (-1)_ (caso maior), como em ___Lutam apenas a Cavalo___ _Ruim (-1)_. A Ordem Heroica terá um número de caixas de Estresse equivalente ao Esforço e permanecerá presente por até uma Cena. Uma mesma Ordem pode ser trazida várias vezes em múltiplas sessões, mas com níveis diferentes no Aspecto Nominal e Dificuldade: esses devem ser rolados no momento do uso da Façanha. A Dificuldade pode alterar conforme a circunstância


### E se um personagem precisar da sua ajuda?

Caso você deseje que personagens possam recorrer a Façanhas de um Fractal, no caso os Recursos de sua Cultura, é melhor que você crie uma Façanha Específica para o personagem que (a) seja simplesmente uma cópia do Recurso da Cultura ou (b) permita que ele, _uma vez por sessão_ recorra ao Recurso da Cultura para uso próprio. Em ambos os casos, pode ser interessante impedir usos que por algum motivo coloquem a Cultura em Risco, já que isso iria de encontro à mesma.

Uma coisa importante: se você fizer isso, a não ser quando dito em contrário, qualquer rolamento utilizará as estatísticas da Cultura, não do personagem.

> ___Por exemplo:___ Jens Sternwächter é um Holtzerinn em meio a um grupo de aventureiros caminhando na região terrível da Ferida, perto de On Tsi, quando algumas criaturas terríveis os atacam. O grupo está para cair, quando ele lembra que tem uma Façanha ___Abençoado Por Rosenkrantz___ que permite ele utilizar qualquer um dos Recursos de Holtz, ___uma vez por cenário___. Ele está desesperado, e sabe que uma falha aqui será o fim! Ele nem pensa muito em recorrer a isso, e nem tem dúvida do que ele vai demandar: ele clama por uma ___Ordem de Heróis___ de Holtz que esteja por perto e reconheça as invocações sagradas ensinadas nos _Büchen Der Rosenkrantz_ para os ajudar. O narrador acha bom e pergunta qual ordem ele quer que esteja por perto. Ele escolhe as ___Walküren der Rosenkrantz___ uma ordem de damas guerreiras que se dedicam ao culto de Rosenkrantz e ao combate em todos os locais onde são convidadas. Isso entra em jogo como um ___Aspecto Nominal___. O narrador rola a ___Cultura___ de Rosenkrantz (já que Jens mencionou o Culto de Rosenkrantz, parece mais adequado que ___Militar___), e o resultado é 0+0+, o que somado à ___Cultura___ de Rosenkrantz resulta em um _Ótimo (+4)_, indicando que a ___Dificuldade___ das mesmas para esse momento será _Ruim (-1)_. O Narrador decide que há um ___Número Reduzido___ delas ali como Dificuldade, o que entretanto não afeta o número de caixas de Estresse (4). Jens agradece a Rosenkrantz ao ver as mulheres carregando lanças com cabo de bronze, seus corpos usando belos vestidos brancos em honra a sua pureza para Rosenkrantz enquanto seus torsos são protegidos por um conjunto de placas de metal que pode ser ajustado para aquelas que estão grávidas (a gravidez nunca para uma Valquíria de Rosenkrantz, ao menos não até a última semana)

## Danificado ou Consumido - Estresse/Consequências/Condições

Essa é uma parte que pode ficar normalmente de fora, pois muitos Extras não são consumíveis. 

Mas a coisa pode ficar interessante se pensarmos, além do Estresse e Consequência como demonstrados no Fate Acelerado, pensarmos possíveis variações do Estresse e Consequências.

A primeira: usar Condições, tanto como a possibilidade de nomeamento prévio de Consequências, conforme apresentada no [_Fate Ferramenta de Sistemas_](https://fatesrdbrasil.gitlab.io/fate-srd-brasil/ferramentas-de-sistema/condicoes/).

Desse modo, você pode manter algo mais interessante aos jogadores já mais acostumados ao sistema tradicional ainda estarão confortáveis, e ficará mais claro o impacto que ataques poderão resultar ao seu Fractal.

> No caso de Culturas, poderíamos pensar em algumas Condições Possíveis
> 
> 
> | __Momentâneas (1)__ | __Estáveis(2)__ | __Duradouras (4)(4)__ |
> |:-------------------:|:---------------:|:---------------------:|
> | Exausta             | Heréticos       | Fragilizada           |
> | De luto             | Questionada     | Em Colapso            |
>
> Claro que essa é uma distribuição comum. Podemos fazer uma distribuição onde um dos tipos de Condições tenha maior número que outras:
> 
>
> | __Momentâneas (1)__ | __Estáveis(2)__ | __Duradouras (4)(4)__ |
> |:-------------------:|:---------------:|:---------------------:|
> | Exausta             | De luto         | Fragilizada           |
> |                     | Questionada     | Heréticos             |
> |                     |                 | Em Colapso            |
>  
>  Apenas lembre que algumas dessas Condições são muito difíceis de serem removidas, mas ao mesmo tempo aumenta um pouco a resiliência de uma Cultura em relação a outras

Outra forma, é adotar um sistema de Condições similar ao de _Uprising_, onde cada Ataque sofrido provoca a Marcação de uma Condição (duas em caso de um Sucesso com Estilo no Ataque). Isso torna extremamente simples a resolução de dano, além de que cada caixa tem um nome específico que já fica associada como uma Condição acima mostrada.

Entretanto... Perceba que ao criar condições segundo esse segundo mecanismo, você deverá deixar claro as consequências de sofrer tal Condição (além do Aspecto com Invocação Gratuita para a oposição) e como que acontecerá a recuperação (Limpeza) da mesma. Você pode colocar também efeitos especiais sobre algumas dessas condições que podem quando ela está marcada. Em _Uprising_, por exemplo, apenas quando a condição _Marcado Para Morrer_ está marcada, um personagem derrotado pode ser morto (ou sequestrado e usado pelo Governo, ou qualquer outro evento similar) ao ser tirado de jogo.


> Vamos utilizar como exemplo as Condições que listamos anteriormente, mas no mecanismo demonstrado acima.
> 
> + ***Exausta***
>    + ***Efeito:*** Enquanto marcada, a Cultura está passando por dificuldades Militares, devido aos efeitos de deserções ou perdas em batalha. +2 na Dificuldade em todos os testes por ___Militar___
>    + ***Recuperação:*** Após algum tempo (ao menos uma sessão), role alguma Capacidade, dependendo de como você vai reenergizar sua Cultura, contra dificuldade _Razoável (+2)_. Uma Falha sempre será entendida como um Sucesso a Custo Maior. A caixa é limpa ao final da cena onde a recuperação foi realizada.
> + ***De luto***
>    + ***Efeito:*** A moral das pessoas estará em baixa, devido a alguma perda significativa. Descreva a mesma. Ela entra como um Impulso no jogo, que só desaparece após ser usado ou após a recuperação
>    + ***Recuperação:*** Após algum tempo (ao menos uma sessão), role alguma Capacidade, explicando como ela vai ajudar a superar o Luto da Cultura pela perda sofrida, contra dificuldade _Razoável (+2)_. Uma Falha sempre será entendida como um Sucesso a Custo Maior. A caixa é limpa ao final da cena onde a recuperação foi realizada.
> + ***Fragilizada***
>    + ***Efeito:*** Por alguma razão, a Cultura é vista como fraca e submissa, colocando em risco tentativas de negociação com outras Culturas. +2 na Dificuldade de todos os testes por ___Comércio___
>    + ***Recuperação:*** Após algum tempo (ao menos uma sessão), role alguma Capacidade, explicando como ela vai ajudar a recuperar a confiança de seus vizinhos, contra dificuldade _Razoável (+2)_. Uma Falha sempre será entendida como um Sucesso a Custo Maior. A caixa é limpa ao final da cena onde a recuperação foi realizada.
> + ***Questionada***
>    + ***Efeito:*** Os Feitos da Cultura são questionados pelas demais. +2 na Dificuldade em Todos os Testes de ___Cultura___
>    + ***Recuperação:*** Após algum tempo (ao menos uma sessão), role alguma Capacidade, explicando como tais feitos são importantes para a mesma, contra dificuldade _Razoável (+2)_. Uma Falha sempre será entendida como um Sucesso a Custo Maior. A caixa é limpa ao final da cena onde a recuperação foi realizada.
> + ***Heréticos***
>    + ***Efeito:*** Questionadores e rebeldes começam a levantar a voz contra a Cultura, por alguma razão (ou nenhuma), devido aos acontecimentos recentes. +2 em todos os Testes de ___Filosofia___
>    + ***Recuperação:*** Após algum tempo (ao menos uma sessão), role alguma Capacidade, explicando como tais questionamentos foram reduzidos (ou sufocados, não importa), contra dificuldade _Razoável (+2)_. Uma Falha sempre será entendida como um Sucesso a Custo Maior. A caixa é limpa ao final da cena onde a recuperação foi realizada.
> + ***Em Colapso***
>    + ***Efeito:*** A Cultura está sofrendo pelos resultados de ação recente um nível de desagregação que pode fazer ela ruir. Crie um Aspecto relacionado aos eventos que levaram a marcar essa condição. Ele entra como um Aspecto em jogo com uma Invocação Gratuita, que desaparece quando a Recuperação ocorre.
>    + ***Recuperação:*** Após algum tempo (ao menos uma sessão), role alguma Capacidade, explicando como tais feitos são importantes para a mesma, contra dificuldade _Razoável (+2)_. Uma Falha sempre será entendida como um Sucesso a Custo Maior. A caixa é limpa ao final da cena onde a recuperação foi realizada.
>    + ***Especial:*** caso uma Cultura seja Derrotada em um Conflito onde ela já entrou com essa condição Marcada, ela pode ser derrotada em definitivo e extinta, não podendo mais agir no jogo.

Existe ainda algumas outras opções interessantes, sobre as quais vamos falar abaixo, nos Extras e Fractais

> Aqui vamos deixar de imediato o sistema Padrão de Extras e Consequências... 
> 
> ... _por agora!_

## Expandindo Conceitos - Extras e Fractais (!!!)

Aqui a mente explode se você começar a entrar na _Toca do Coelho_!

Vamos lá... O Efeito do Fractal é que você pode tratar qualquer coisa usando elementos de um personagem ao criar um Fractal, certo?

Mas aqui cabe uma coisa legal de pensar: você pode fractalizar _Elementos do Fractal_ seu!

E como diz nosso amigo Xzibit:

![[Yo Dawg](/assets/img/xzibt-fractal.jpg)](/assets/img/xzibt-fractal.jpg)

_"Como assim, Bial?"_

OK... 

Um personagem "normal" em Fate pode possuir extras, compostos por fractais, OK?

Pela mesma lógica, você pode incluir extras e Fractais a um Fractal que você esteja criando.

E o quão fundo você pode descer na toca do Coelho?

Tecnicamente, você pode ir nível Alice!

Na prática... Evite descer demais.

O motivo mais óbvio é o tanto de controle de mesa que você vai precisar fazer, o que sufocará seu jogo, tirando dele o que é importante, a diversão.

Entretanto, o fato de você poder fractalizar fractais dentro de fractais permite um ajuste fino no seu fractal.

> Por exemplo? Vamos ver um sistema diferente de Estresse e Consequências nas Culturas, usando o fractal para fazer com que os danos sofridos por uma cultura tenham impacto real!
>
> Para isso, vamos recorrer a um sistema de fractal já muito usado no Fate e que apareceu primeiro em _Dresden Files Accelerated_, que é a ideia de usar barras de Estresse para definir o acesso a algo.
> 
> Nesse caso, vamos definir que cada Capacidade tenha um número de caixas de Estresse equivalente a seu valor, cada uma absorvendo 1 Ponto de Estresse e podendo ser marcadas quantas necessárias, ao modo que é demonstrado no Fate Condensado. 
> 
> Quando todas as caixas de Estresse forem marcadas, a Cultura em questão não pode usar aquela capacidade até que alguma ação tenha sido tomada para restaurar tais caixas (a critério do Narrador, mas sugerimos que você possa recuperar uma caixa para cada cena passada do Conflito). As Capacidades Medíocres contam como apenas 1 Caixa para efeito de Dano e recuperação.
> 
> Por sua vez, cada Aspecto de uma Cultura poderia ter caixas valendo de 2 a 10 Pontos, escolhidas à vontade do criador da Cultura (exceto a 10, obrigatoriamente nos ___Fundamentos___). Entretanto, só poderia-se marcar uma delas por Ataque Sofrido e aquele Aspecto não poderia ser Invocado pela Cultura até ser limpo (poderia ainda assim ser Invocado ou Forçado contra a Cultura). Além disso, nesse caso, ao marcar uma Caixa de Estresse de um Aspecto você ofereceria uma Invocação Gratuita para o atacante naquele Aspecto, e só poderia limpar o Aspecto depois de um certo tempo, renomeando-o para refletir os eventos que ocorreram: por exemplo, se ___Holtz___ sofresse um Ataque e marcasse seu ___Estereótipo___, ela ao limpar o mesmo poderia renomear de ___Incultos, Simplórios e Violentos___ para ___Um Exército com uma Nação em Frangalhos___.

Esse tipo de distribuição de Dano exemplificado aqui como uma aplicação de Fractal aparece também, por exemplo, em _Psychedemia_, onde as _Aptidões Psíquicas_ possuem estresse que indicam o quanto o personagem forçou seus poderes até o ponto de se esgotarem.

Lembrando: você não é obrigado a implantar nenhum fractal em seu fractal (ou mesmo em um jogo, para ser sincero). Tente pensar se o fractal que você está trazendo vai adicionar sabor ao jogo ou se simplesmente está transformando o jogo em algo extremamente mais complexo que o necessário.

> Vamos adotar que exista um Fractal simples, similar ao que mostramos agora a pouco, para o dano afetar as Capacidades e Aspectos de uma Cultura. 
> 
> Primeiro, vamos adotar a ideia das Condições similares a _Uprising_ (ataque bem sucedido: marque 1, ou 2 em sucesso com estilo), sendo que cada Capacidade possuirá uma caixa de Condição que, quando marcada, impede aquela Condição de ser usada.
> 
> Além disso, cada Aspecto terá também 1 caixa cada. Entretanto, Aspectos só podem ser atacados se uma Capacidade for minada (marcada) anteriormente. Isso impede que todos os Aspectos sejam visados de imediato: você teria que marcar 1 Capacidade antes de marcar 1 Aspecto, e em seguida teria que marcar outra Capacidade antes de marcar outro Aspecto. O Aspecto de ___Fundamentos___ só pode ser marcado depois que todos os demais Aspectos o for. Aspectos marcados não podem ser Invocados pela Cultura, mas podem ser Invocados ou Forçados contra Elas, e quem marcou o Aspecto recebe uma Invocação Gratuita nele.
> 
> Por outro lado...
> 
> Recuperar-se uma Capacidade demanda rolamento de alguma outra Capacidade contra _Razoável (+2)_, enquanto recuperar-se um Aspecto  demanda um rolamento de alguma Capacidade contra _Ótimo (+4)_. Sem falar que é necessário esperar-se passar uma Cena (para Capacidades) ou Cenário (para Aspectos) para rolar alguma Recuperação! 
> 
> Simples e efetivo, reduz a necessidade de _bookkeeping_ e faz com que os danos sofridos pela Cultura tenham ___impacto real___ no jogo
 
### _Regra Opcional:_ Quando as ações do personagem são importantes para a Cultura: Intervenção e Pontos de Intervenção

Essa é uma regra que pode ser interessante em certos jogos onde certos Fractais e Extras podem intervir no jogo junto com os personagens. Entretanto, deve ser usada com parcimônia, senão corre-se o risco de tirar totalmente a proatividade de personagens.

Nesse caso, podemos dizer que existiria uma possibilidade de um Fractal como fazemos aqui ter também uma recarga... Ou para adicionar um principio como os listados no início do Artigo

> + Se o seu Extra puder Intervir Diretamente na Narrativa sem depender diretamente dos personagens, ele poderá ter ___Recarga___ e ___Pontos de Destino___

Um exemplo que podemos pensar é a possibilidade de uma Cultura intervir a favor dos personagens (ou contra eles) usando sua ___Recarga___ que vamos chamar de ___Intervenção___. Os Pontos de Destino da Cultura poderia ser chamado de ___Pontos de Intervenção___ e usados para realizar intervenções diretas na narrativa conforme as necessidades da Cultura.

> Por exemplo, vamos imaginar que as ações dos jogadores são interessantes para a ___Igreja dos Sacros Reis Guerreiros de Rosenkrantz___, mas que os personagens estão sofrendo muito em um conflito. O Narrador, ou um Jogador que por um acaso esteja comandando Holtz, pode determinar que para Holtz o resultado favorável aos jogadores seja interessante às intenções de Holtz, então decide ___Intervir___ pagando um Ponto de Intervenção e, usando a  ___Igreja dos Sacros Reis Guerreiros de Rosenkrantz___ para declarar que um grupo de Paladinos de Rosenkrantz está passando por lá e decide ajudar os personagens "por pura coincidência"

Perceba, entretanto, que nem todo jogo cabe esse tipo de intervenção, e que é possível utilizar outras maneiras de permitir essa interação entre os personagens e seu fractal.

## O que realmente preciso disso tudo?

Sinceramente?

A resposta correta é _"Você decide"_

A resposta longa é: 

Você, como dito em muitos momentos nesse _Oh! Toodles!_ deve considerar o que você realmente necessita. Às vezes, você não precisa de Habilidades/Competências, já que seu Fractal não terá como mudar o contexto por ações (ao menos não diretamente), ou não precisará de Estresse e Consequências, pois não poderá será deteriorado ou consumido.

Seguindo os princípios listados lá em cima e pegando um pouco de _cancha_, logo você saberá que elementos do Fate Acelerado (ou combinações) que, alterados, poderão lhe prover Fractais mais rapidamente e de maneira mais adequada ao seu jogo

> Por fim, não iremos utilizar Intervenção nas Culturas, já que não existe essa necessidade

## Conclusão - Aplique as ideias

Esse _Toodles_ é bem diferente da maioria, pois aqui estamos mostrando como utilizar os conceitos do Extra e do Fractal para, a partir de algo já existente (no caso o Fate Acelerado), criar algo totalmente novo e interessante.

Esse _game shark_ pouco utilizado é uma das melhores fontes de Fractais que você pode imaginar. Usei isso bastante no passado (como no caso do jogo/Fractal para Fadinhas de Anime [_Spirit Helpers_](http://fabiocosta0305.gitlab.io/rpg/SpiritHelpers/)) e vira e mexe revisito essa técnica, pois é uma ótima porta de entrada e forma rápida para construir-se fractais, em especial naquela situação que demanda um Fractal caprichado que defina muitas coisas no jogo e que você precise tirar "da cartola" (é o _Mickey-Objeto Surpresa que vai nos ajudar mais tarde!_).

Dito isso, também fica claro como funciona a criação de um Fractal e como pensar cada elemento a ser usado em um Extra por meio do Fractal do Fate, certo?

Então, nos vemos no nosso próximo artigo e até lá...

___Está na mão, é só ação!___


## Apêndice 1 - O Reino de Holtz (segundo as regras escolhidas nesse artigo ao final de cada sessão)

### Aspectos

| ***Marcada*** |      ***Tipo*** | ***Aspecto***                                                                |
|:-------------:|----------------:|------------------------------------------------------------------------------|
| ( )           | **Fundamentos** | _Éditos dos_ Büchen des Rozenkranz _que guiam uma nação_                     |
| ( )           | **Estereótipo** | _Incultos, Simplórios e Violentos_                                           |
| ( )           |    **Relações** | _Salvare é um reino de cobras, e sempre é bom observar cobras bem por perto_ |
| ( )           |      **Feitos** | _Igreja dos Sacros Reis Guerreiros de Rosenkrantz_                           |
| ( )           |                 | _Não se pode negar o poder e a influência dos Druidas_                       |

### Capacidades

| ***Marcada*** | ***Capacidade*** | Nível           |
|:-------------:|-----------------:|-----------------|
| ( )           |     **Comércio** | _Regular (+1)_  |
| ( )           |      **Ciência** | _Regular (+1)_  |
| ( )           |      **Cultura** | _Razoável (+2)_ |
| ( )           |   **Espionagem** | _Medíocre (+0)_ |
| ( )           |    **Filosofia** | _Razoável (+2)_ |
| ( )           |      **Militar** | _Bom (+3)_      |

### Recursos

+ ___Contos de Cavalaria:___ como somos uma Cultura Paladínica, nossos Contos são conhecidos por todos os lados, e portanto recebemos +2 ao ___Criar Vantagens___ por ___Cultura___ ao disseminar nossa cultura e formas de ver o mundo usando tais contos pelo Continente;
+ ___A Resiliência de Rosenkrantz:___ devido a nossa filosofia estoica, vinda dos _Büchen der Rosenkrantz_, podemos resistir a Ataques por qualquer meio que vise nos isolar de recursos importantes para nossa sobrevivência usando ___Militar___, com +2 no rolamento;
+ ___Ordens de Heróis:___ devido às diversas Ordens Heroicas que existem em Holtz, Holtz pode, ___uma vez por sessão___, trazer para jogo uma Ordem para agir em seu nome em qualquer situação, tanto em combate quanto fora dele. Role a Capacidade adequada ao evento (normalmente, mas não obrigatoriamente, ___Militar___). O esforço resultante determinará o Poder dessa ordem como um ___Aspecto Nominal___ de valor equivalente (como ___Die Walküren der Rosenkränze___ _Ótimo (+4)_). O Narrador definirá uma ___Dificuldade___ com valor _Terrível (-2)_ (se Esforço ficar abaixo de ___Bom (+3)___) ou _Ruim (-1)_ (caso maior), como em ___Lutam apenas a Cavalo___ _Ruim (-1)_. A Ordem Heroica terá um número de caixas de Estresse equivalente ao Esforço e permanecerá presente por até uma Cena. Uma mesma Ordem pode ser trazida várias vezes em múltiplas sessões, mas com níveis diferentes no Aspecto Nominal e Dificuldade: esses devem ser rolados no momento do uso da Façanha. Se desejado, a Dificuldade pode alterar conforme a circunstância.

[^1]: _"O Grande Jogo é um termo usado para definir o conflito e a rivalidade estratégica entre o Império Britânico e o Império Russo pela supremacia na Ásia Central."_ ([Wikipedia](https://pt.wikipedia.org/wiki/Grande_Jogo)) Em alguns estudos e Jogos da Era Vitoriana, o termo foi extendido para todos os movimentos políticos, diplomáticos e ocasionalmente militares, visando aumentar o poder e influência dos mesmos ao redor do mundo, em especial focado na Europa, Ásia Central e América do Norte.

