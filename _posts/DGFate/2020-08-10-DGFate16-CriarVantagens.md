---
title: "Domando o Fate - Parte 16 - A necessidade imprescindível de Criar Vantagem"
subheadline: Uma Introdução ao Fate 
date: 2020-08-10 19:00:00 +0300
layout: post
categories:
 - RPG
tags:
 - Fate
 - Fate Core
 - Fate-Core
 - Artigos
 - Dungeon Geek
 - Introdução ao Fate
 - introducao-ao-fate
 - Criar Vantagens
order: 18
header: no
---

> Publicado Originalmente no Site da [Dungeon Geek](https://www.dungeongeek21.com/domando-o-fate-a-necessidade-imprescindivel-de-criar-vantagem/)

Uma das ações mais fundamentalmente importantes em Fate, e uma das menos compreendidas, é a ação de _Criar Vantagem_. Sua versatilidade e sua característica mor de trazer ao jogo novos Aspectos que podem ser usados a favor (e contra!) dos personagens dos jogadores é tão fundamental ao jogo que várias ações serão necessariamente de _Criar Vantagem_.

Entretanto, para o jogador de RPG típico, a ação de Criar Vantagem costuma passar batida: poucos jogos estimulam atualmente em termos mecânicos a colaboração entre personagens em termos _explícitos_ de _mecânica_ e de _narrativa_.

Fate o faz de maneiras muito inteligentes, e aqueles que conseguem entender a ação de Criar Vantagem conseguem domar o sistema em níveis inimagináveis.

Portanto, esse artigo visa dar uma melhorada nisso, mostrar por que é importante entender tal ação e como ela pode ser útil nos mais diversos momentos do jogo.

Dito isso, então vamos começar

## Revisitando a ação de Criar Vantagens

Vamos começar revisitando o conceito de _Criar Vantagem_. Primeiramente, no [Fate Básico](http://fatesrdbrasil.gitlab.io/fate-srd-brasil/fate-basico/as-quatro-acoes/#c-criar-vantagem), a ação de criar Vantagem é definida como:

> _"A ação de criar vantagem abrange uma grande variedade de esforços no sentido de usar suas habilidades para tirar vantagem (daí o nome) do ambiente ou da situação."_
 
Uma coisa aqui importante é: a Ação de Criar Vantagem ___não resolve___ problemas (isso é _Superar_), ___não provoca prejuízos___ (isso é _Ataque_) e ___nem impede___ você de ser alvo de tentativas das demais (isso é _Defesa_).

Portanto, para um RPGista que não compreenda o Fate, parece uma ação inútil, né?

___ENTRETANTO___, a ação de Criar Vantagem permite que você prepare vantagens relacionadas ao ambiente ou situação, como mostrado na descrição anterior. Isso é importante, pois ela funciona como uma _"ação de preparar terreno"_: sempre que você precisar ajudar alguém, incluindo a si próprio, para conseguir algo, isso é uma ação de _Criar Vantagens_.

Vamos descrever um exemplo de uma situação onde a ação de Criar Vantagem pode ser útil:

> Os Fae Guardians precisam ajudar uma Criança que foi amaldiçoada pelos duendes de Morgause a voltar para sua forma real. Atualmente ela está _**Amaldiçoada com uma forma bestial**_ de um duende, ainda que seu coração ainda seja puro como o de uma criança. Após todos voltarem ao Edifício Dakota, Helen realiza algumas pesquisas em seus tomos de magia e descobre o que precisa.
> 
> _"Bem... precisamos certamente de algumas flores específicas para preparar uma poção a ser usada como um banho mágico..."_ diz Helen, enquanto os outros estão se empanturrando de frango frito. _"A linguagem das flores diz que precisamos de Erva-Doce e Crisântemo, já que são flores que falam de verdade... Alecrim, para relembrar a forma antiga dela..."_
> 
> _"Não seja por isso!"_ comenta Robert _"Eu conheço uma florista maneira no Bronx, ela vai nos arrumar isso facinho"_
> 
> _"Não tão rápido..."_ retorque Helen
> 
> _"Ok... Sabia que ia ter alguma pegadinha."_ diz Pericles, exasperado _"O que vamos ter que fazer?"_
> 
> _"Ei, não tenho culpa que a magia é algo caprichoso: ou você faz bem feito ou não faz, não existe fazer nas coxas se não quiser transformar Nova York em Tão Tão Distante."_ diz Helen, fazendo muxoxo _"Elas precisam ser colhidas sob a Lua Cheia... E não podemos demorar para usá-las para o banho. Quanto mais frescas melhor."_
> 
> _"Então... Vamos ter que achar onde se planta essas coisas? E hoje?"_ exasperou Pericles
> 
> _"Exato... Hum... Bobby, aquela sua amiga florista, será que ela poderia nos colocar em contato com algum floricultor?"_ pergunta Helen

Bem... Vamos então entender o exemplo acima:

Helen está tentando curar uma menina de uma maldição mágica. No caso, essa é uma ação de _Superar_ mágica, que ela irá resolver usando seus _Conhecimentos_ (graças à sua Façanha ___Bibbity-Bobbity-Boo___). Em termos mecânicos, nada impediria tentar ela de sobrepor tal maldição por um rolamento simples, exceto a dificuldade altíssima __*Épica (+7)*__. Mesmo com seu _Conhecimentos_ __*Bom (+3)*__, a chance de ela conseguir isso pura e simplesmente seria miníma (um +4 no rolamento conseguindo apenas um empate). 

Ela então decide "preparar o terreno" (aqui entra uma ação de Criar Vantagem) verificando se existe alguma forma de realizar uma poção ou algum outro efeito que permita a ela remover tal maldição. Isso é um teste mais simples, com uma dificuldade __*Razoável (+2)*__, contra seus _Conhecimentos_. Rolando `00+0`{: .fate_font}, ela obtêm um __*Ótimo (+4)*__ nesse rolamento, e isso revela que a maldição pode ser possível remover a maldição por meio de um banho em uma poção mágica.

Ela então decide avaliar o que eles vão precisar: um novo rolamento contra _Conhecimentos_, com os dados dando `+0-+`{: .fate_font} resulta em outro __*Ótimo (+4)*__, e ela percebe que pode ser um banho feito com uma poção mágica envolvendo algumas flores específicas, baseado na Linguagem das Flores: crisântemo, erva-doce, alecrim, entre outras.

Mas tem um problema, que Bobby não percebeu: as flores tem que ser colhidas em situações específicas e nos momentos corretos, não é só comprar na floricultura.

Com isso, os _Fae Guardian_ se veem na situação de ter que atuar para obter essa flores.

## Representando em termos narrativos a ação de Criar Vantagem

_Criar Vantagem_ é mal compreendida porque, na prática, pode significar _muitas_ coisas. Diferentemente das demais ações, que fica bem claro que fazem (superar alguma dificuldade com uma perícia, provocar prejuízo ao atacar ou se defender de alguma outra ação), Criar Vantagem pode significar muitas coisas diferentes, tanto de maneira ativa quanto passiva.

De maneira geral, podemos dividir a ação de Criar Vantagem, nessa questão narrativa, em:

1. Ativamente tentar preparar algo
2. Encontrar/Descobrir algo útil que já esteja no cenário

Perceba que essas são as situações normalmente previstas em termos mecânicos, mas elas podem se desdobrar de diversas formas.

### Ativamente tentar preparar algo (ou _Momento McGuyver_)

Nesse primeiro caso, você usa coisas que estão no cenário _mas que não são o que você quer_. Em termos mecânicos, isso traz para o jogo um novo Aspecto. 

Perceba que essa coisa pode ser algo que estava lá no cenário o tempo todo, mas que não tinha necessidade de ser um _Aspecto_, sendo apenas uma descrição narrativa. Isso pode implicar em coisas como ter os materiais adequados para preparar alguma coisa, ou simplesmente descobrir que no local onde você está você já tem aquilo que precisava, mas que na prática você não sabia, ___e que não era importante até aquele momento___ (ou seja, não era um Aspecto em cena).

> Enquanto a criança está dormindo e Pericles e Bobby vão procurar algumas outras plantas de uma pequena lista durante a lua cheia, Helen dá uma olhada na estufa que fica na sacada do _flat_ do Edifício Dakota onde eles mantém sua base: desde que passou a entender melhor sua magia, Helen tem cultivado algumas plantas misticamente úteis. Ela acredita que deve ter alguma coisa útil que ela possa colher ali. Ela rola seus _Conhecimentos_ duas vezes contra uma dificuldade ___Boa (+3)___ e consegue novamente `00+0`{: .fate_font} e um outro teste para a segunda flor com `0++0`{: .fate_font}, com resultados finais ___Ótimo (+4)___ e __*Excepcional (+5)*__. Ela encontra em meio as flores um pouco de ___erva doce___ e ___alecrim___. Cortando delicadamente os pequenos galhos com sua tesoura de prata, ela embrulha os mesmos em um papel manteiga especialmente tratado. _"Duas foram... Espero que Bobby e Pericles consigam a que falta."_ ela diz, guardando-as em um pequeno estojo

Em termos de regras, Helen procurou em meio à sua estufa (que até então era uma descrição narrativa) por flores que ela precise. Com os sucessos, ela obteve dois ___Aspectos___: ___Alecrim___ e ___Erva-Doce___, ambas com uma invocação gratuita cada. 

#### E se eu falhar?

No caso de uma falha e você quiser um _Sucesso a Grande Custo_, a Invocação Gratuita passa para o Narrador (ou para a Oposição) e o Aspecto pode ser renomeado naturalmente: pense que, no caso acima, Helen não seguiu os ritos necessários para a magia, o que tornou as ___Ervas Compurscadas___ e pode fazer com que o narrador Invoque tal Aspecto para adicionar dificuldade ou provocar ___Efeitos Espúrios___.

#### E posso invocar um Aspecto que surgiu a partir de uma falha?

_Tecnicamente falando_ SIM. Em termos de regras, nada impede você Invocar _qualquer Aspecto conhecido pelo seu personagem_. ___ENTRETANTO___, o narrador pode impedir você de usar, por você não saber ou confiar no efeito daquela vantagem preparada. Além disso, mesmo que você conheça, você terá que usar Pontos de Destino e nada vai impedir o Narrador de usar a Invocação Gratuita contra você.

Dito isso, uma vez que um Aspecto esteja em jogo e você _**(1)** conheça o mesmo **e (2)** tenha os meios de usá-lo_ (seja por Pontos de Destino ou Invocações Gratuitas), nada impede você de usar os mesmos à vontade. E o mesmo também é verdade para a oposição: qualquer Aspecto seu que seja conhecido pela oposição pode ser usado contra você! Isso é chamado ___Invocação Hostil___ e você recebe o Ponto de Destino usado (caso aplicável) ao final da cena.

E esse conceito dito acima, do ___Conhecimento dos Aspectos___ é importante para a segunda situação onde se aplica a ação de Criar Vantagem, que é _ao Encontrar ou Descobrir algum Aspecto_ que já esteja no cenário.

### Encontrar/Descobrir algo útil que já esteja no cenário (ou _"Vou vasculhar a sala"_)

Parece meio bizarro ter uma ação para descobrir algo que já existe...

Até que você pensa em termos de _metagame_: o fato dos _jogadores_ saberem que determinados Aspectos estão em jogo não implica nos ___Personagens___ saberem que tais Aspectos estão em jogo.

Isso é importante porque o Narrador pode desejar colocar alguns Aspectos já previamente existentes na cena. O fato de eles existirem, além de oferecerem um "sabor" narrativo à mesma, permite que os jogadores (e a oposição também) possa utilizar tais elementos de maneira interessante de maneira mecânica, Invocando-os e Forçando-os uns contra os outros para seus objetivos em cena.

> O Narrador decide que Pericles e Bobby estão indo para a Fazenda dos McCullough, os floricultores que fornecem plantas para a amiga de Bobby. Ele decide que eles estão ___Sob a Luz da Lua___ e que eles podem ver um ___grande campo de Flores___. Não há nenhum inimigo ou algum antagonista por perto.
> 
> _"Bem, acho que consigo invadir facinho isso aí!"_ diz Bobby _"A **Luz da Lua** deve ser suficiente para enxergar bem o caminho, mas não o suficiente para que alguém me veja em meio ao **Grande campo de flores**"_
> 
> _"Sem chance, Bobby."_ diz Pericles _"Se você fizer isso, corre o risco desses caras atacarem você com uns tiros de rifle, e mesmo um garotinho de madeira não iria durar tanto. Além disso, você faz alguma ideia de que diabos são essas plantas? Eu também não, então..."_
> 
> _"Qual a ideia, então, chefe?"_ ele reclama
> 
> _"Vamos negociar com eles... Deixa eu ver se tenho algum dinheiro no bolso..."_ diz Pericles, sacando uma carteira
> 
> _"Ok... Mas eu negocio... A última vez que você tentou isso a gente se deu mal!"_ diz Bobby, indo para a casa da Fazenda.

Aqui temos algumas coisas...

O Narrador deixa claro os Aspectos da ___Lua Cheia___ e do ___Grande Campo de Flores___ estão disponíveis. Não apenas ele os descreve, mas podemos imaginar que ele coloca algum tipo de identificação de que o Aspecto está em jogo, como um cartão ou um _post-it_, indicando que ele está disponível para os jogadores. 

Narrador, ao deixar um Aspecto público, sempre coloque ele de visível a todos os jogadores: pode ser no meio da mesa, colado em um escudo, ou mesmo escrito em um quadro branco que você tenha disponível, não importa. O importante é deixar claro que aquele Aspecto está em jogo. Para Aspectos que apenas um ou alguns jogadores conheça, passe tais mecanismos (como os _post-it_) diretamente aos jogadores com os Aspectos e quaisquer invocações gratuitas envolvidas e deixe a critério dele revelar ou não os Aspectos em questão.

Dito isso, os jogadores pensam em ideias para lidar com a situação: Bobby poderia tentar utilizar a ___Lua Cheia___ para enxergar seu caminho e o ___Grande Campo de Flores___ para se ocultar, mas Pericles avisa que a ___Lua Cheia___ também pode o tornar um alvo, e no meio de tantas flores dentro do ___Grande Campo___, como eles vão reconhecer as certas?

Nesse caso, vamos ver como os dois vão resolver essa encrenca:

> _"Ok... A Sorte é que não perdi tudo no poker ontem, e dei sorte no_ Fantasy Football _, então  consegui alguma grana."_ diz Robert, sacando um ___pequena maço de verdinhas___ do bolso, enquanto eles se dirigem à casa dos floricultores e Robert bate à porta.
> 
> _"Quem é? Isso lá é hora de incomodar alguém?"_ diz uma voz irritadiça dentro da casa, e eles veem um homem com uma cara meio humorada abrindo a porta.
> 
> _"Desculpe, senhor McCullough, mas... É que acho que você conhece a_ **Rebbekah Ma, da _Pink Lotus_**. _Ela é minha amiga e me disse que você poderia me ajudar com um problema bem sério..."_ diz Robert
> 
> _"Ah... A Rebbekah? Sim, ela é uma excelente cliente. Mas por que você não falou com ela?"_ diz o homem, um pouco menos irritado
> 
> _"Então... É que apareceu esse problema, e precisamos de um pouco de crisântemo. É para uma amiga, um trabalho de faculdade de arte. Sabe como são essas coisas, né?"_ diz Robert
> 
> _"Sei... Cada coisa doida... Acho que devo ter um pouco de crisântemo. O que foi?"_ pergunta o homem, curioso
> 
> _"Ela precisa usar flores em um projeto, seguindo aquele lance de linguagem das flores, e ela precisa expressar pureza e verdade, pelo menos foi o que ela disse, e precisam estar o mais frescas possíveis e colhidas durante a noite... A gente vai pagar, obviamente, pelas flores e pelo inconveniente de ser tão tarde"_ ele comenta, enquanto Pericles apresenta o maço de verdinhas
> 
> _"Ah!"_ ele diz _"De fato, uma arte antiga... Espere um minuto, meu jovem, que devo também ter lírio branco, que é símbolo da pureza. Me acompanhem até a estufa."_ ele diz, buscando um casaco para se cobrir.
> 
> Enquanto ele se afasta, Pericles diz: _"Caramba... Você é um baita de um mentiroso, não?"_
> 
> _"Eu não menti..."_ diz Robert, dando de ombros _"Não tenho culpa se o seu paradigma de verdade não é o mesmo que o meu."_
> 
> _"Seu nariz deveria crescer agora, sabia?"_ termina Pericles, enquanto o homem se aproxima com um casaco, um avental e algumas ferramentas de jardinagem

Vamos lá...

O que se passou nessa cena?

Primeiramente, tanto Pericles quanto Robert utilizam Pontos de Destino para trazer Invocações Gratuitas em alguns Aspectos deles na forma de Impulsos:

+ O Narrador ofereceu previamente aos jogadores um Aspecto sem Invocações Gratuitas do ___Crisântemo___, que eles sabem que precisam... Entretanto, eles não possuem invocações gratuitas na mesma.
+  Pericles usa sua Dificuldade ___Vem fácil, Vai fácil___ para declarar que, apesar de normalmente se dar mal, no dia anterior tinha tido uma boa estrela e ganhado  ___um pequena maço de verdinhas___ que pode ser útil. Ele não precisa de testes para trazer as verdinhas para o jogo, pois é parte de uma Declaração Narrativa. O Narrador permite que ele utilize isso para trazer _ou_ um Impulso _ou_ um Aspecto sem Invocações. O impulso parece mais útil para Pericles agora, já que tornar isso um Aspecto sem invocações pode demandar testes e/ou usar Pontos de Destino para tornar tal Aspecto útil, e se a grana acabar depois não tem grande problema
+ Por sua vez, Robert vai usar um de seus Aspectos, ___Aprender a Confiar e a ser confiável___, para trazer para o jogo um contato que pode ser útil de seu grupo de reabilitação. ___Rebbekah Ma___ é dona de uma floricultura, a ___Pink Lotus___, e ela comentou certa vez sobre esses floricultores específicos com Robert. Ele decide que esse Aspecto também pode vir como um Impulso: ele não precisa se arriscar do cara ficar puto com a Rebbekah (e por tabela com Robert), e então ele pretende apenas usar isso como uma carta coringa para caso tudo dê errado!
+ O Narrador define que o __*Sr McCullough, Floricultor*__ é um NPC que tem apenas um Aspecto do Nome, com um nível de __*Bom (+3)*__, e tem um Aspecto de Dificuldade de ___Não tem muita paciência___, com _Ruim (-1)_ (ele está usando a regra de _NPCs Extras_ de _Shadow of the Century_)
+ A primeira coisa que o Narrador verifica é se o nosso senhor McCullough está de bom humor... E ser acordado quase à meia-noite depois de um dia inteiro de trabalho duro não parece algo que deixa uma pessoa de bom-humor. Ele usa o _Ruim (-1)_ do Senhor McCullough contra a _Empatia_ __*Razoável (+2)*__ de Bobby para ver se o mesmo está disposto a conversar. Um `+0+0`{: .fate_font} o resultado do Senhor McCulllough para __*Regular (+1)*__, o que indica uma _Falha_. É quando Bobby queima seu _Impulso_ para conseguir o Sucesso (adicionando +2 ao rolamento) e fazer com que o Senhor McCullough converse com os mesmos, já que foram indicados por ___Rebbekah Ma___. 
+ Robert então tem a chance de rolar sua _Comunicação_ __*Razoável (+2)*__ para, depois de explicar a situação para o senhor McCullough. Antes mesmo de rolar os dados, Pericles adiciona o ___Maço de Verdinhas___, dizendo que Pericles mostra discretamente as notas ao floricultor, indicando que eles sabem que estão incomodando o procurando tão tarde. O resultado dos dados é `0--+`{: .fate_font}, resultando em um __*Regular (+1)*__, que é somado ao +2 do ___Maço de Verdinhas___ de Pericles para um resultado final __*Bom (+3)*__. Com isso, eles são _bem-sucedidos com estilo_, já que o Floricultor é uma dificuldade __*Medíocre (+0)*__ para esse efeito, já que ele não tem nenhuma coisa a favor ou contra eles, superado o fato de arrancarem ele da cama. Quando Robert menciona a _Linguagem das Flores_, ele sugere arrumar também ___Lírio Branco___ como sinal de pureza como o crisântemo. Ou seja, além de incluírem uma Invocação Gratuita no Aspecto dos ___Crisântemos___, também conseguiram um novo Aspecto do ___Lírio Branco___, com uma Invocação Gratuita!

### Qual a diferença entre os dois tipos?

É mais difícil trazer um Aspecto ao jogo do que "reforçar" ele com Invocações Gratuitas, pois trazer um Aspecto ao jogo representa você criar algo novo, no máximo modificando as coisas de maneiras narrativas e tornando-as interessantes narrativa e mecanicamente, enquanto no segundo caso você apenas reforça algo que já o é.

Isso é representado em especial pelo efeito de um Empate:

1. Quando você tenta ___Criar___ um Aspecto novo, um empate resulta em uma vantagem de curta duração, na forma de um Impulso: por exemplo, um hacker que tente obter uma ___Falha de segurança___ precisa garantir que algum de seus _exploits_ entre no servidor desejado. Um Impulso aqui pode representar que ele conseguiu achar uma brecha que vai aceitar ___comandos arbitrários___. Ele pode utilizar isso tanto para obter rapidamente os ___dados___ que precisa e se mandar ou ainda pode usar essa falha para implementar um ___Backdoor___ que ele pode utiliza no futuro.
2. Quando você ___Descobre___ um Aspecto, na prática você tá usando algum aspecto que já estava lá. Em caso de Empate, você adiciona uma Invocação Gratuita àquele Aspecto, como se você tivesse um sucesso. Isso também serve para premiar esforços que envolvem preparar o terreno antes e trabalhar para manter o mesmo ativo. Por exemplo, no caso acima, se o hacker já tivesse a ___Backdoor___ instalada e estivesse a reforçando com funcionalidades mais poderosas para preparar um grande ataque ao sistema, um empate já colocaria uma invocação gratuita no mesmo.

Por outro lado, em uma ação que reforça um Aspecto que já está lá, é sempre importante lembrar que pode acontecer da oposição o conhecer, mesmo não podendo usar Invocações Gratuitas (se tiverem). Já um Impulso desaparece rapidamente.

## E o que uma Vantagem pode representar

Essa é a pergunta que torna a ação de Criar Vantagem tão complexa: na realidade, como dito no Fate Básico, a ação de _Criar Vantagem_ representa ___absolutamente QUALQUER coisa que possa ser usada no futuro___ para te beneficiar, e isso inclui:

+ Contratar Aliados
+ Preparar Armas especiais
+ Preparar Condições Específicas
+ Analisar Circunstâncias
+ Investigar Pontos Fracos
+ Infiltrar Pessoas
+ Reconhecer aliados nas filas inimigas
+ Abrir brechas nas defesas
+ Obter informações adicionais
+ ...

Ou seja, qualquer coisa que possa te beneficiar no futuro.

### E qual a diferença com Superar?

A palavra chave aqui não é tanto Vantagem, como seria de se esperar, mas ___Futuro___. 

Uma ação que visa obter uma vantagem imediata (como conseguir um objeto ou uma passagem) pode não ser uma ação de criar vantagem necessariamente, mas ocasionalmente sim de _Superar_, já que você tenta obter uma vantagem para resolver um _problema imediato_, ou seja, _Superar algum obstáculo_.

Entretanto, se você realiza uma ação para obter algo que vá ser usado ___depois___ (mesmo que esse depois seja na ação de seu aliado a seguir), isso é ___COM CERTEZA___ _Criar Vantagem_.

Desse modo, essa é a maneira de pensar quando quiser diferenciar se sua ação é de _Criar Vantagem_ ou _Superar_.

## Ligando a ação de Criar Vantagem com as demais Ações

Por si só, _Criar Vantagem_ talvez seja a mais inútil das ações, já que ___na teoria___ ela apenas prepara algo que você irá usar no futuro, não resolvendo (novamente ___na teoria___) nenhum problema, não provocando dano ou não provendo defesa...

Entretanto, a grande vantagem da ação de Criar Vantagem está ___EXATAMENTE___ no que a torna ___na teoria___ inútil: ao preparar vantagens, ela cria possibilidades de reforçar suas chances de fazer ___QUALQUER___ outras das ações, de duas Formas:

1. Ela trás ao jogo novos Aspectos. Novos Aspectos representam novas maneiras de mudar a cena a seu favor e também de obter mais pontos de destino por meio de forçadas e invocações hostis em momentos onde sua reserva de pontos está baixa;
2. Ao trazer para o jogo um novo Aspecto ou "descobrir" um Aspecto já em jogo, ele reforça sua importância por meio das _Invocações Gratuitas_. Elas representam o tempo que você gastou para preparar as coisas e irá favorecer você no momento do uso. Em especial os jogadores podem gastar _mais_ de uma Invocação Gratuita de um mesmo Aspecto ao usá-lo, mantendo as demais limitações (pode usar só o que já tem, não pode usar mais de uma vez em um mesmo teste). Essa é a "compensação" por ter gasto tempo em algo ___na teoria___ inútil. Pense, por exemplo, em CSI: na teoria, todo aquele processo meticuloso de coleta de provas é apenas _flavor_, não servindo para nada... Mas quanto mais provas eles coletam, mais Aspectos do Crime são descobertos e estão disponíveis, portanto eles mais facilmente vão acossar o criminoso!

Isso é mostra o maior e mais importante detalhe da ação de ___Criar Vantagem___: ela é uma ação de _preparação de terreno_. Ela permite que você prepare tudo para vencer seus desafios.

Angus McGuyver, Hercule Poirot, John McLane, Frank Dux, Horatio Kane, Penelope Garcia. Todos eles tem muito mais em comum do que você imagina.

> Por fim, os Fae Guardians conseguem obter as flores que são necessárias para a magia. 
> 
> Helen prepara tudo e cria misticamente uma poderosa Poção de Cura para eliminar a maldição sobre a menina. Helen observa a menina com seu rosto bestial e ela a observa, em seus trajes de Fada Madrinha, enquanto termina de macerar a infusão de flores e coloca ela em um frasco.
> 
> _"Isso vai dar certo, moça? Vou poder voltar a ser eu mesma?"_ pergunta a menina.
> 
> _"Esperamos que sim... Infelizmente, a magia é algo caprichoso e é uma pena que você estivesse no lugar errado na hora errada. Mas meus estudos me mostram que essa é a forma de resolver o seu problema."_ ela diz, pegando uma pequena bacia de prata e colocando água nela, enquanto recitava as palavras de magia
> 
> _"Pura água, de cor cristalina, revele a verdade, remova a má sina."_  ela recita, e abre o pequeno frasco com a poção de flores, pingando algumas poucas gotas.
> 
> _"As quatro flores, como as estações, desfaçam a maldade, renovem as feições."_ 
> 
> Ela faz um gesto com a cabeça, e a menina leva as mãos ferais e mergulha-as na mistura mística, levando-a ao rosto e esfregando os pelos animalescos com tal mistura, enquanto Helen finaliza sua magia com sua varinha.
> 
> _"Aquele que nessas águas se banhar, à sua forma verdadeira retornará.... Bibbity-Bobbity-Boo!"_ Ela diz, mexendo sua varinha e apontando-a para a menina.
> 
> E os _Fae Guardians_ veem o belo rosto da menina voltando enquanto as feições bestiais a deixam. Ela se vê no espelho e sorri maravilhada.
> 
> _"Sou... Eu! Eu mesma!"_ ela diz, extasiada, e abraça Helen _"Obrigada, moça! O que posso fazer para agradecer?"_
> 
> _"Basta não contar para ninguém o que aconteceu aqui, está bem?"_ ela diz, no tom suave e tranquilo de uma Fada Madrinha.
> 
> _"Sabe... No fim das contas, coisas como essas fazem toda essa encrenca valer a pena."_ diz Pericles
> 
> _"Com certeza."_ diz Robert

E como foi que isso tudo aconteceu em termos mecânicos?

> Primeiro, vamos lembrar que os _Fae Guardians_ obtiveram quatro vantagens relativas às Flores que precisavam para a ___Poção de Cura___ (o narrador já trás esse Aspecto ao jogo):
> 
> As duas conseguidas por Helen
> + ___Erva doce___ `1`{: .fate_font} (OBS: cada caixa representa uma Invocação Gratuíta)
> + ___Alecrim___ `1`{: .fate_font} 
> 
> A que era de um Aspecto conhecido por Robert e Pericles
> + ___Crisântemo___ `1`{: .fate_font} - eles precisavam dessa flor, então sabiam desse Aspecto
> 
> E a que conseguiram como parte do _sucesso com estilo_ na negociação de Robert como um Impulso
> + ___Lírio Branco___ 
> 
> Dito isso, Helen vai preparar a poção para eliminar tal Aspecto ruim das ___Feições Bestiais___ como dito acima. Isso é fácil de ser feito: um teste ___Bom (+3)___ de Helen é o suficiente. Ela gasta 1 Ponto de Destino se preparando e paramentando enquanto ___Fada Madrinha___ para +2, elevando seu _Conhecimentos_ ___Bom (+3)___ para __*Excepcional (+5)*__. E ela é extremamente bem-sucedida: `0+++`{: .fate_font} para um resultado final __*Lendário (+8)*__. O normal seria todos os Aspectos das Flores desaparecerem, incluindo suas Invocações (já que as mesmas foram usadas e portanto saíram de jogo), mas na prática ele torna essa poção ainda mais poderosa (como se tivesse várias doses), unificando todos as invocações nela e colocando duas invocações gratuitas adicionais em uma espécie de Super-Poção:
> 
> + ___Poção de Cura___ `6`{: .fate_font}
> 
> Agora, é hora da verdade: após todos os rituais (que basicamente são narrativa), Helen rola contra o ___Épico (+7)___ da Maldição. Para garantir que o resultado será bem-sucedido, ela declara antecipadamente que usará algumas gotas da Poção de Cura, não importa o que aconteça, usando uma de suas Invocações Gratuitas. O resultado final dos _Conhecimentos_ de Helen mais o rolamento que teve `+0++`{: .fate_font} retorna um __*Fantástico (+6)*__. Ainda não é o suficiente sozinho, mas as poucas gotas da poção de cura fizeram o serviço, ao somar +2 ao rolamento, elevando para ___Lendário (+8)___ o resultado final! A menina vê o rosto dela, seu verdadeiro rosto, restaurado da forma animalesca à qual estava aprisionada.

## Por que não queimar todas as invocações gratuitas de uma só vez?

Na letra dura da regra, as invocações gratuitas de Aspectos atuam como "PDs travados" ao mesmo. Entretanto, eles tem como benefício especial o fato de você poder usar quantos delas você quiser em um teste, _mantida a observação de usar **UMA VEZ POR TESTE**_.

Isso quer dizer que você pode, por exemplo, gastar 2 invocações para somar +4 ao rolamento (+2 por invocação), ou então usar 1 para um novo rolamento, mas não pode, por exemplo, após usar para re-rolar usar novamente para +2.

Então, continua a dúvida _"Por que não queimar todas as invocações gratuitas de uma só vez?"_

Bom...

Primeiramente, os Aspectos não somem imediatamente ao serem usados, mesmo no caso de acabarem-se as invocações gratuitas. Mecanicamente falando, o único caso em que o Aspecto desaparece imediatamente após o uso envolve _Impulsos_.

Então, dito isso, às vezes compensa você ___não utilizar tudo de uma vez___, aproveitando o Aspecto ao máximo.

Pense em uma Arma: a não ser que você queira ___REALMENTE___ deitar seu adversário e garantir que ___ELE NÃO VAI SE LEVANTAR___, você não precisa descarregar o pente/tambor nele de uma vez. Em geral, um ou dois tiros é mais do que o suficiente (não tentem isso em casa, crianças)

E o mesmo vale para as invocações gratuitas: muitas vezes, você pode declarar o uso de uma ou duas invocações e isso bastará para a maioria das situações.

Cada invocação representa um uso parcial dos benefícios originais: algumas gotas, uma bala, um pouco de dinheiro, uma carga da vara mágicas...

Portanto, faz bastante sentido, pensando assim, não exaurir tudo de uma vez, exceto que seja ___REALMENTE___ importante.

> _Helen está arrumando as coisas após a menina ir embora com Pericles e Robert, que estão a levando de volta para casa. Ela observa o pequeno frasco, que ainda tem muito conteúdo e pensa que pode ser uma boa guardar ele em seu armário pessoal de poções. Ela cola um pequeno adesivo e identifica a poção, e a guarda. Nunca se sabe quando será útil._

## Quando um Aspecto desaparece?

Bem... Já mencionamos isso previamente.

Mecanicamente, Aspectos desaparecem como parte de uma ação de _Superar_, como no exemplo que mostramos: a ação de Helen ao usar a magia foi para _Superar_ a maldição. Ela _aproveitou uma Vantagem_ (a poção) para melhorar suas chances, mas a ação em si foi de _Superar_.

Em termos narrativos, o Narrador pode inutilizar Vantagens que deixem de fazer sentido quando uma cena é trocada. Isso pode ser temporário ou permanente. 

> ___Por exemplo:___ se os personagens colocaram um prédio ___Em Chamas___ e o deixaram arder, ao voltar para o local algumas cenas depois o Aspecto terá desaparecido, já que provavelmente só sobrarão ___cinzas e destroços___ do mesmo. Entretanto, se for apenas ele indo para uma cena curta em outro local e voltando, o prédio poderá estar ainda ___Em Chamas___.

Esse é um detalhe narrativo que pode (e certamente irá) ocorrer e para o qual você deverá estar preparado.

Uma coisa importante: qualquer invocação gratuita que exista em um Aspecto que desapareça, desaparece juntamente. Ele é perdido e não há nenhuma compensação para isso. Isso é uma forma de garantir que os personagens não se super-preparem, formando verdadeiros _Arsenais de Aspectos_, o que evita a possibilidade de uma _Guerra Fria de Aspectos_.

> Alguns dias depois, Helen observa suas poções para sua revisão periódica e percebe que a Poção de Cura está diferente. Ela abre um pouco e percebe o cheiro azedo que a mesma está exalando. _"Oh puxa! Eu deveria ter observado melhor!"_ ela lamuria, ao perceber o tanto de poção que foi perdida.

No caso acima, o narrador determinou que, depois de alguns dias, a ___Poção de Cura___ perdeu seu efeito e ficou até mesmo malcheirosa, desaparecendo, com todas as invocações que sobraram.

## É possível renomear Aspectos?

Sim... Esse é um _cheat_ que a ação de Criar Vantagem permite.

Ao usar uma ação de Criar Vantagem, pode acontecer eventos que mudem a situação. Quando isso ocorre, os Aspectos envolvidos podem ser reajustados, não apenas no número de invocações disponíveis nos mesmos, mas também _em suas próprias descrições_.

Isso ocorre para refletir (por exemplo) mudanças de lado de aliados ou da situação, consolidação de múltiplos Aspectos em um Aspecto maior (como vimos na Poção), etc...

A única forma mecânica descrita de renomear um Aspecto é dependente do resultado da ação. Se você está conversando com ___Aliados___ e Falha, pode acontecer que eles fiquem ___Hostis___ a você de uma hora para outra e vice-versa. De outro modo, o narrador pode agir da maneira adequada a representar mecanicamente a narrativa.

> Helen observa a poção que tinha feito... Ela se pergunta se dá para fazer alguma coisa... Conversando com o narrador, a jogadora de Helen topa uma sugestão: para não perder a ___Poção de Cura___, ela vai tentar reaproveitar a mesma transformando-a em um ___Filtro de Visão Verdadeira___ que ela irá usar na forma de pequenas cápsulas que, quando rompidas, liberam uma fumaça que revelará qualquer coisa que esteja oculta ou invisível. Ela precisará usar um novo teste de _Criar Vantagem_ para renomear tal Aspecto, mas dito isso, essa ação de reaproveitar uma poção em outra, portanto renomeando o Aspecto, é totalmente possível.

## Conclusão

Ações de _Criar Vantagem_ são um elemento fundamental no jogo do Fate: entender a mesma é importantíssimo para aproveitar melhor o jogo, tornando tudo mais fácil e proveitoso tanto para os jogadores quanto para o narrador. Desse modo, procure entender a mesma e aproveitá-la sempre. Desse modo, todos poderão agir e ajudar nos eventos.

E até a próxima...

... role +4!
<!--  LocalWords:  tags Dungeon Geek c-criar-vantagem RPGista Fae Ok
 -->
<!--  LocalWords:  Guardians Morgause Dakota Erva-Doce Robert Bronx
 -->
<!--  LocalWords:  facinho Pericles Bobby Bibbity-Bobbity-Boo flat of
 -->
<!--  LocalWords:  Guardian McGuyver Compurscadas metagame McCullough
 -->
<!--  LocalWords:  post-it poker Fantasy Football Rebbekah Pink Lotus
 -->
<!--  LocalWords:  NPCs Shadow the Century bom-humor exploits Hercule
 -->
<!--  LocalWords:  Backdoor Poirot McLane Frank Dux Super-Poção PDs
 -->
<!--  LocalWords:  rerrolar cheat
 -->
