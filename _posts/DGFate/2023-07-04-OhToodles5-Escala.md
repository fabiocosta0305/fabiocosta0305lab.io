---
title: "Oh, Toodles! 5 - As Ferramentas em Fate do Mr. Mickey - Escala"
subheadline: Uma Introdução ao Fate 
date: 2023-07-04 09:30:00 +0300
layout: post
categories:
 - RPG
tags:
 - Fate
 - Fate Core
 - Fate-Core
 - Artigos
 - Dungeon Geek
 - Introdução ao Fate
 - Domando o Fate
 - Iniciando no Fate
 - Ações
 - Teste
 - Rolamento
 - introducao-ao-fate
order: 26
header: no
excerpt_separator: <!-- excerpt -->
---

E temos mais um artigo da série Oh! Toodles!

E vamos falar nesse artigo sobre algo que é bastante problemático em qualquer RPG: sistemas de Escala.

Veremos a importãncia de definir em relação a _o que_ a Escala está se referindo, além de observarmos algumas regras que podem ser úteis para sistemas de Escala, inclusive revisitando o avô do Fate, o Fudge.

A idéia é apresentar soluções que sejam interessantes e divertidas conforme o seu estilo de jogo.

Então, vamos começar?

Então digam: _Oh, Toodles!_

<!-- excerpt -->

## A "Escala Zero"

Antes de começarmos, é interessante fazermos uma delimitação e definir um conceito importante sobre Escala, que é o conceito de _Escala Zero_.

O Fate carrega de seu avô, o Fudge, a ideia de que a escala dos personagens não será definida baseada na "população padrão" do mundo, mas sim nos personagens envolvidos. 

O que isso quer dizer?

Vamos pegar, por exemplo, dois tipos de cenários de jogo que se passam em nossa realidade: _Camundongos Aventureiros_ e _Ultraman_. 

Em _Camundongos Aventureiros_, dois camundongos, Emily e Alexander, viajam o mundo resolvendo mistérios e conhecendo pessoas, enquanto em _Ultraman_ seres gigantescos ajudam a proteger a terra de invasores espaciais.

A parte curiosa é que, em Fate, não é necessário fazer nenhum ajuste de regras para jogar-se com um ou outro.

Por que?

Porque, a não ser que você crie interações dos camundongos com coisas maiores (como Gatos e Seres Humanos) ou dos Ultras com coisas muito menores (como pessoas e aviões), não apenas não é importante quanto ___não faz o menor sentido___ tentar incluir uma regra de escala.

Isso também pode evitar uma série de problemas quanto aos modificadores de escala: imagine dois Ultras (ou dois camundongos) enfrentando-se mutuamente. Em um sistema onde a escala é baseada na população do mundo, ficaria extremamente estranho ter que aplicar modificadores que basicamente se anulariam o tempo todo em rolamentos. Ao trazer a preocupação de escala para os personagens, definindo a Escala Zero, o padrão, _para os personagens_, você remove uma preocupação adicional.

Dito isso, vamos começar a falar sobre como lidar com escalas de personagem.

## Façanhas e Aspectos como Escala

A ideia mais óbvia e direta dentro do Fate é recorrer às Façanhas e Aspectos para gerar a noção da Escala. Isso é legal quando poucos personagens estão fora da Escala Zero dos personagens e no qual o fato do personagem ser muito maior ou menor que o personagem for a única vantagem, não importando o quão maior ou menor isso mude. 

Isso torna as coisas bem simples, pois já deixa tudo mais fácil, sem a necessidade de regras especiais de Escala, tudo estando suportado nas Façanhas ou Aspectos. Por outro lado, se existem variáveis níveis de tamanho e isso for importante, essa solução não será tão útil.

> ___Por exemplo:___ Emily e Alexander estão em Manaus, visitando seu primo local Guaraci e ajudando a investigar o mistério sobre alguns roubos no Teatro Amazonas. Mas o problema é que existe um ___Gato Sarnento___ no mesmo que, obviamente, quer tornar os três em um petisco saboroso! O ___Gato Sarnento___ pode usar seu Aspecto para somar +2 em qualquer rolamento contra os camundongos para representar a diferença de tamanho em relação aos mesmos.
> 
> Em outro momento, Emily, Alexander e Guaraci descobrem que os roubos foram executado por ninguem mais ninguem menos que Sem-Rabo Não Vale Nada, o mais infame rato ladrão de todo o mundo, e ele está fugindo com o produto de seu último roubo rapidamente, pegando um barco no Rio Amazonas! Seria um problema terrível encontrar o mesmo nesse lugar, mas eles conseguiram ajuda com Tainá, a Tucano. Tainá possui uma Façanha ___Uma Ave Bastante Grande___, que permite que ela some +1 em qualquer teste cujo o tamanho seja relevante, como em procurar e perseguir.
 
Perceba que isso é bem interessante de maneira geral, mas ele sofre do problema de não traduzir mecanicamente diferenças "reais" de escala: não importa se a criatura em questão for um pouco maior que o personagem ou ESTUPIDAMENTE maior que o personagem, isso não faz diferença mecânica. Emily e Alexander enfrentarem um gato, uma criança ou um elefante não muda em absolutamente NADA o impacto da escala nesse caso.

E pode ser exatamente isso que você deseja: situações onde a Escala tem impacto tanto narrativo quanto mecânico.

Para isso, veremos a seguir algumas sugestões de sistemas de Escala.

## Níveis de Escala - A Solução do _Ferramentas de Sistema_

Uma forma razoavelmente simples, mas elegante, vem [diretamente do _Fate - Ferramentas de Sistema_, página 57](http://fatesrdbrasil.gitlab.io/fate-srd-brasil/ferramentas-de-sistema/escala/). Esse sistema é usado em alguns _Worlds of Adventure_, incluindo aí _The Secrets of Cats_.

Para isso, antes de começar-se o jogo, deve-se definir alguns níveis de escala, do menor para o maior. Após definir isso, para cada nível de escala do maior em relação ao menor, ele recebe um ou ambos os benefícios abaixo, conforme a necessidade:

+ +1 no Ataque _ou_ +1 na Defesa;
+ _Arma: 2_ (provoca 2 a mais no Estresse em caso de Ataques bem-sucedidos) ou _Armadura: 2_ (reduz em 2 o Estresse recebido);

É importante que você não tenham uma quantidade muito grande de níveis aqui, a não ser que isso faça sentido: lembre-se que _Fate é pouco granular_, portanto qualquer bônus de +1 faz muita diferença. A não ser que você pretenda fazer um jogo onde camundongos enfrentem o Godzilla, mais do que quatro níveis de escala pode não fazer o menor sentido!

> ___Exemplo:___ Vamos voltar ao caso dos _Camundongos Aventureiros_. Para criarmos bem o cenário, decidimos colocar uma escala onde a Escala Zero são os Camundongos (d'uhh!). Entretanto, existem muito poucas criaturas menores que um camundongo, e muitas muito maiores. Nesse caso, vamos colocar apenas duas escalas para baixo, que iremos chamar de _Barata_ e _Pulga_, e quatro para cima: _Gato_, _Cachorro_, _Humano_ e _Elefante_, formando a seguinte linha do menor para o maior:
> 
> _Pulga > Barata > Camundongo > Gato > Cachorro > Humano > Elefante_
> 
> Além disso, definimos que será oferecido _Arma:2_ e _Armadura:2_ para criaturas maiores, enquanto será oferecido +1 para Ataque e Defesa para criaturas menores.
> 
> Imaginemos que Emily e Alexander estão com um problemão, literalmente: uma ___Horda de Elefantes___ estão vindo em um estouro, atacando eles dois e seu primo africano, Kimobwa. Eles recebe +4 para a Defesa (+1 por passo de _Camundongo_ até _Elefante_), o que provavelmente irá garantir que eles se salvem...
> 
> ...e esperamos que sim, pois caso sejam atingidos por uma pisada de Elefante, existe uma bela chance de virarem patê de camundongo, já que os Elefantes recebem _Arma:8_ (YIKES!!!!!) contra os três!

Essa opção pode ser interessante, mas tem que ser avaliada, em especial porque tem a combinação de oferecer bônus de Ataques e Defesa e níveis de Arma e Armadura: dependendo de como isso será usado, tornará a diferença de escala potencialmente frustrante e letal. Tome muito cuidado para ajustar isso.

## Escala de Supers em _Wearing the Cape_

Um sistema de Escala de Poder interessante aparece em _Wearing the Cape_. Nesse jogo de Supers, os personagens são divididos tanto pelo _Tipo de Poder_ (definido por um pacote de Aspecto de Poder + Façanhas de Poder, sobre os quais já comentamos anteriormente ao falar de classes e arquétipos de personagem em Fate) quanto pelo seu _Nível de Poder_, que varia dentro do jogo de D a A, além dos dois níveis superiores _Ultra_ e _Omega_. 

Nesse jogo, quando dois personagens de tipo de poder equivalente se encontram (tipo dois _Atlases_ - estilo Super-Homem - ou _Ajaxes_ - estilo Hulk), o que possui o tipo de poder maior recebe +2 para cada nível de diferença contra o personagem mais fraco em todas as ações envolvendo o uso de super-poderes.

Isso deixa interessante pois, ao mesmo tempo que consegue emular a diferença de poder entre personagens "na mesma liga", também permite criar o efeito de _Pedra-Papel-Tesoura_, onde um super-cara estilo _Atlas_ pode ter problema contra personagens mágicos (Tipo _Merlin_) ou velocistas.

Essa pode ser uma forma que você deseje colocar em seu jogo, por exemplo, para Extras de Veículos ou para pessoas com poderes mágicos e coisas do gênero. É uma solução um pouco menos "genérica", mas pode ser bastante funcional para o jogo que você deseja.

## A Escala do Sobrenatural - _Dresden Files Accelerated_

Em _Dresden Files Accelerated_ existe toda uma gama de criaturas que disputam entre si em ligas muito acima da do ser humano comum (não estamos falando da Detetive Murphy, caso você esteja se perguntando). Esses personagens são capazes de causar muito estrago, e de realizar feitos muito acima do que o ser humano pode esperar.

Para realizar isso, ele les utilizam um sistema de escala onde personagens que estão acima de outros dentro da escala podem optar por um dos seguintes benefícios _no momento do rolamento_, por nível de diferença:

+ +1 no rolamento;
+ +2 no esforço final _após o rolamento_, em caso de sucesso;
+ +1 invocação gratuíta em caso de um sucesso ao gerar um Aspecto;

Isso pode ser interessante ao dar flexibilidade para mostrar a diferença de poder conforme a necessidade, de muitas formas diferentes. Entretanto, perceba que isso só se aplica se as coisas estão acontecendo em uma determinada liga (por exemplo, quando um mago for capaz de usar suas magias). Esses bônus podem ser anulados de maneiras razoavelmente coerentes (por exemplo, jogando água para inutilizar as velas rituais) e coisas do gênero.

## Lidando com os _Monstros Gigantes_ - Monstros de Várias Zonas

Isso é muito comum: você está lidando com monstros que são gigantescos ao ponto de ocuparem espaços muito maiores que qualquer jogador, não importa sua escala. Desde o dragão que está atacando os personagens ao Kaiju com seu hálito atômico. 

Para isso, temos uma solução muito boa! Na [página 153 do _Fate: Ferramentas de Sistema_](http://fatesrdbrasil.gitlab.io/fate-srd-brasil/ferramentas-de-sistema/monstros/#monstros-de-m%C3%BAltiplas-zonas) é apresentado um sistema onde, ao invés de considerar um monstro desse porte como se fosse uma criatura única, divide-se ela por múltiplas zonas, cada uma com um ataque próprio, incluindo também Façanhas de Transformação que são acionadas pela destruição de algumas das Zonas.

Na prática isso pode ser considerada uma _reversão_ do conceito de _Grupos de Capangas_: ao invés de você ajuntar vários inimigos em um só, você "separa" as várias partes de um inimigo gigante que ataca individualmente.

> ___Exemplo:___ Vamos imaginar que os personagens estão atacando o Dragão de Cristal, uma criatura gigante e temível!
> 
> ### Dragão de Cristal
> 
> + ___Sou antigo como o Tempo!, Meros Insetos não podem me derrotar!___
> 
> + ___Cabeça___
>     + _**Mordida** Bom (+3)_
>     + ___Raio de Energia:___ _Uma vez por conflito_ e _A Cada Membro derrotado_, posso realizar um ataque _Excepcional (+5)_ contra todos os alvos em todas as Zonas próximas. O estresse sofrido por cada alvo após a Defesa é dobrado antes da absorção. Todos os alvos se defendem individualmente
>     + ___Parte de um Todo:___ Caso o corpo seja atingido, o estresse pode ser passado para a cabeça
>     + ___Estresse:___ [2][2][2]
>     + ___Consequências:___ [2][4]
> + ___Corpo___
>     + ___Parte de um Todo:___ Caso a cabeça seja atingida, o estresse pode ser passado para o corpo
>     + ___Estresse:___ [4][4][4]
>     + ___Consequências:___ [2][4][6][6][8]
> + ___Garras (x4)___
>     + _**Garras** Bom (+3)_
>     + ___Parte de um Todo:___ Caso a cabeça ou corpo seja atingida, o estresse pode ser passado para uma garra
>     + ___Estresse:___ [2][2][2]
>     + ___Consequências:___ [2][4]
> + ___Cauda___
>     + _**Cauda** Bom (+3)_
>     + ___Raio de Energia:___ _Uma vez por conflito_ e _A Cada Membro derrotado_, posso realizar um ataque _Bom (+3)_ contra todos os alvos em todas as Zonas próximas. O estresse sofrido por cada alvo após a Defesa é dobrado antes da absorção. Todos os alvos se defendem individualmente
>     + ___Parte de um Todo:___ Caso a cabeça ou corpo seja atingida, o estresse pode ser passado para a cauda
>     + ___Estresse:___ [2][2][2]
>     + ___Consequências:___ [2][4]


Perceba o tamanho da encrenca que os personagens terão! Além dos _Raio de Energia_ da cabeça e da Cauda, a possibilidade da Cabeça e do corpo transferir Estresse para outras partes do corpo e entre si torna a coisa ainda pior. E perceba que a Cauda também possui ataques poderosos!

Esse sistema permite emular muito bem monstros clássicos de D&D que possuem múltiplos ataques, sem precisar necessariamente se preocupar com um mecanismo de escala: só o fato de ser várias partes e cada delas atacar individualmente já torna a coisa muito complicada e letal para os personagens!

YIKES!!!

## Peso - Um sistema de Escala que envolve compensação

OK...

Mas você deve estar achando que tudo isso causa problemas, pois todos esses sistemas mostrados levam em consideração que dificilmente você tem como uma criatura de menor tamanho ou poder afetar uma de maior poder de maneira específica.

E isso é uma verdade.

Mas existe um sistema de escala bastante interessante para resolver isso, baseado no conceito de _Peso_. Esse sistema foi apresentado em [_War of Ashes: Fate of Agapthus_](https://www.drivethrurpg.com/product/157134/War-of-Ashes-Fate-of-Agaptus), um jogo baseado em um boardgame de criaturinhas que tentam enfrentar-se mutuamente e evitar criaturas maiores, incluindo deuses!

A Vantagem desse sistema é que ele é bem simples e coloca em conta não apenas os personagens individualmente, mas também eles enquanto um grupo, considerando inclusive o equipamento que eles estão carregando.

Nesse sistema, a primeira parte é contabilizar os pesos dos lados envolvidos em um combate ou ação. Para isso:

+ Cada personagem de escala padrão e com equipamento comum conta como Peso 1;
+ Personagens muito pequenos ou muito mais fracos (como goblins contra aventureiros) são considerados Peso 0. Para que sejam considerados como Peso 1, é necessário que eles estejam em um bando;
+ Criaturas um tanto maiores ou mais poderosas que os personagens conta como Peso 2. 
+ Criaturas muito maiores podem contar como Peso 3. 
+ Para monstros de múltiplas Zonas, como visto acima, considere cada Zona ainda ativa como uma criatura de Peso 1;
+ Alguns itens especiais (como uma ___Lança matadora de Dragões___) pode oferecer Peso 1 adicional;
+ Para saber o Peso total de cada lado, some o Peso de todas as criaturas envolvidas;

Se um dos lados passar o Peso do outro por um mínimo de 2 para 1 (por exemplo, Peso 2 contra Peso 1), o lado vencedor poderá ___maximizar___ (transformar um dado não positivo em um dado positivo) um dado em todos os rolamentos. Se a proporção for de 4 para 1 (por exemplo, Peso 8 contra Peso 2), ele poderá maximizar ___dois___ dados. E assim sucessivamente, dobrando a proporção.

A grande vantagem disso é que, ao mesmo tempo que permite que um alvo tenha um impacto ___óbvio___ quando for realmente poderoso, ele também dá chance para que a força dos números se tornem impactante e dinâmica.

> ___Exemplo:___ Imaginemos que Dois aventureiros vão enfrentar o Dragão de Cristal como mostramos anteriormente. Eles estão armados normalmente, não estando preparado para encarar o Dragão. Portanto, eles são considerado Peso 2.
> 
> Já o Dragão de Cristal é um Monstro de Múltiplas Zonas, composto por 7 Zonas (Cabeça, Corpo, 4 Patas, Cauda). Com isso, o Peso total dele é 7.
> 
> Nessa situação atual, o Dragão de Cristal pode maximizar 1 Dado contra os Aventureiros: ele tem uma proporção maior que 2 para 1, mas não que 4 para 1.
> 
> Agora, imagine que o Dragão de Cristal comande uma ___Tribo de Kobolds___ que o adora como a um Deus. Se pararmos para pensar, Kobolds como criaturas mais fracas seriam Peso 0, mas como são uma Tribo, ou seja, vários deles, é possível considerar eles como uma criatura de Peso 1. Isso aumentaria o Peso total do Dragão de Cristal para 8, portanto uma proporção de 4 para 1, o que permite que o Dragão ___e os Kobols___ maximizem dois dados no rolamento.
> 
> Por outro lado... Se cada um dos Aventureiros tivesse uma ___Lança dos Dragões___, uma Arma poderosa forjada pelas Damas dos Dragões que oferece Peso 1 adicional contra Dragões, seu peso total seria de 4. Contra o Peso 7 do Dragão, isso o impediria de maximizar dados contra os personagens. O mesmo aconteceria, por exemplo, tão logo os personagens derrotassem ao menos as patas do Dragão, pois nesse caso o peso dele cairia para 3, portanto abaixo da proporção 2 para 1 de diferença de Peso.

## Encerrando: Escala deve ser avaliada

Lembrem-se sempre de, ao utilizar o conceito de Escala, você ___não precisa necessariamente___ de um novo conjunto de Regras: em especial, enquanto os personagens lidarem com oponentes e afins ___do mesmo nível de tamanho e/ou poder___ que eles, não se faz necessário Escala. Além disso, Escala pode ser ignorada em situações onde ela não for necessariamente relevante: por exemplo, quando os camundongos aventureiros precisam conversar com algum amigo humano, não se faz necessário regras de escala, pois parte-se da premissa que eles não afetarão ou serão afetados pelo mesmo.

A partir daí, você pode, como sempre, optar por usar a ferramenta, ou combinação, daquilo que você achar necessário. Nunca acima nem abaixo da necessidade.

Então, nos vemos no nosso próximo artigo e até lá...

___Está na mão, é só ação!___
