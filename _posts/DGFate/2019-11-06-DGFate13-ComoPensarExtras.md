---
title: "Domando o Fate - Parte 13 - Como Pensar os Extras"
subheadline: Uma Introdução ao Fate 
date: 2019-11-06 12:00:00 +0300
layout: post
categories:
 - RPG
tags:
 - Fate
 - Fate Core
 - Fate-Core
 - Artigos
 - Dungeon Geek
 - Introdução ao Fate
 - introducao-ao-fate
 - Criando NPCs
 - NPCs
order: 14
header: no
---

> Publicado Originalmente no Site da [Dungeon Geek](https://www.dungeongeek21.com/domando-o-fate-como-pensar-nos-extras/)

Bem, se você leu até os artigos anteriores, parabéns: você já viu as bases sobre como tocar a suas aventuras e campanhas, criar personagens e NPCs e lidar com todas as coisas mais básicas no Fate.

A partir de agora, vamos ver coisas para, como digo, _Domar o Fate_. Aqui vamos pensar em como lidar com as regras visando o melhor em seu jogo, e como lidar com as coisas no Fate.

Esses artigos são um pouco mais avançados e às vezes serão mais teóricos. Não se espante e para citar o famoso filósofo Douglas Adams, _"Não entre em pânico"_. Leia, releia e vá aplicando em seus jogos esses princípios. Logo você vai perceber que você estará acertando a mão.

Dito isso, vamos falar uma coisa que considero fundamental: como pensar nos Extras e como utilizar os extras da melhor maneira possível.


## Revisando Extras

Como dissemos anteriormente, os Extras são definidos de maneira geral como tudo que não pode ser resumido a apenas Aspectos, Perícias, Façanhas, Estresse e Consequências. Na prática, podemos redefinir Extra como tudo que  precisa de regras especiais fora das regras padrão citadas. 

Em geral, em outros RPGs, tudo que seriam Extras em Fate seriam resolvido por subsistemas. O exemplo mais clássico são os sistemas de Magia, que tendem a ser subsistemas do sistema básico.

Qual a desvantagem disso? É o fato de, em especial o narrador, mas os jogadores também, ter que aprender todo um conjunto específico de regras, que algumas vezes pode ser completamente diferente do sistema de regras básico (GURPS Vehicles, estou falando de VOCÊ!).

No caso do Fate, isso é resolvido revisitando o conceito do Fractal, que veio do avô do Fate, o Fudge: a idéia que você pode tratar QUALQUER coisa dentro do sistema de regras como se fosse um personagem. Isso permite que você consiga usar elementos de regras já conhecidos para os elementos novos.

Lembrando que conceitualmente existem três coisas a levar em consideração ao definir seu Extra:

+ As _Permissões_ que serão necessários para adquirir o Extra
+ Os _Custos_ necessários para melhorar tais Extras
+ Os _Elementos_ que serão usados para definir o Extra
    + _Aspectos_ para definir coisas que mudam a história
    + _Perícias_ para novos contextos em ações
    + _Façanhas_ para realizar coisas extraordinárias
    + _Estresse_ para coisas consumíveis

Feito essa revisão rápida (volte para nosso artigo original para mais informações), vamos conversar agora sobre como considerar um extra.

## _"Por que eu devo pensar nos meus Extras?"_

Essa é uma pergunta que muitos se fazem quando começam no Fate, já que, à exceção do que aparece no _Ferramentas de Sistemas_ (e nos outros Toolkits, como o _Adversary_ e o _Horror_) e no Capítulo 11 do Fate Básico, ele não traz regras "avançadas" sobre, por exemplo, Perseguições, Magia, Raças, Psiquismo e Superpoderes, Artes Marciais e assim por diante.

Bem:

Fate é um sistema pensado para dar suporte a qualquer tipo de narrativa dentro da premissa original de _proatividade, competência_ e _drama_ de seus personagens. Ele visa dar um suporte aos elementos narrativos e _fazer eles importarem_ nas regras. Se o fato do cara estar armado com uma pistola ou com uma metralhadora ou com um estilingue ou com um canhão gravitrônico Neumann não fizer diferença para o cenário, então você _não precisa_ de regras especiais: use rolamento de _Atirar_ pura e simplesmente. O tipo de arma fica como descrição narrativa.

___ENTRETANTO,___ o Fate provê todo tipo de possibilidades de fazer com que elementos específicos da narrativa possam impactar nas mecânicas de jogo. No caso, se o tipo de arma fizer diferença narrativa, você pode incluir o uso de regras de _Arma e Armadura_ (página 253 do Fate Básico) para que armas mais poderosas realmente façam diferença. 

Claro que isso tem que ser balanceado: o fato do cara ter uma arma poderosa tem que provocar um "distúrbio na Força" que faça com que todos os caras poderosos o ataquem com a mesma facilidade que os subalternos fracos desapareçam da vista dele. 

Desse modo, Fate estimula você a considerar em termos de regras aquilo *__E APENAS AQUILO__ cuja aleatoriedade vá ter um impacto na narrativa*. Se o fato do cara ter um carrão envenenado não for tão melhor que o cara ter um Fusquinha, não inclua regras para veículos: use uma Disputa de _Direção_ e acabou, simples assim. Agora, se você for criar uma campanha de rachadores fugindo da lei e combatendo crime e corrupção ao melhor estilo _Dukes of Hazzard_, você DEVE incluir regras para perseguições, e portanto um Extra de perseguições.

## Extras devem ser simples

Uma das coisas que pode acontecer com quem está começando no Fate (eu incluso, acreditem) é tentar _fazer o kitchen sink_: ao criar um Extra, trazer todas as possibilidades possíveis e imaginaveis e criar um mega Extra com regras de todos os tipos.

Não faça isso.

Vou repetir: NÃO FAÇA ISSO.

A não ser que você vá fazer uma coisa que seja útil em muitas situações e em diversos contextos, mantenha um Extra sempre como algo o mais simples possível. Isso ajuda a não quebrar o sistema. _Keep It Simple and Safe_, KISS, é algo muito importante de se pensar.

O exemplo clássico para esse tipo de situação é o de Espaçonaves. 

A não ser que você precise de algo que cobre, por exemplo, batalhas espaciais e que queira colocar no jogo o drama de errar os cálculos de astronavegação e mandar a nave para algum lugar aleatório no espaço n-dimensional, você não deveria esquentar a cabeça com isso: permita que os jogadores descrevam a nave de uma forma mais simples como ___Millenium Falcon, o Lixo Espacial mais rápido da Galáxia___ (como Façanha) ou __Cormoran, *o meu Lar no Espaço*__ (como Aspecto) e siga em frente.

Por que isso?

Fate é um RPG com regras fundamentais simples e rápidas de se expandirem: se, a qualquer momento, você perceber que precisará dar um maior foco a navegação e combate espacial e nos dramas envolvidos nisso (tipo, ter o ___Motor de Dobra Danificado___ no meio do nada), você pode expandir regras, adicionando novos elementos ao sistema conforme a necessidade. Ao usar o Fractal, você pode adicionar facilmente esses elementos e os expandir, ao fato de poder ter Extras com Extras (uma Espaçonave que tem Armas que tem Munição, por exemplo)

Obviamente você pode deixar certos detalhes já pré-planejados, se você imaginar que sua aventura possa tomar um determinado rumo. Isso não é ruim. Mas é importante evitar a super-preparação, tomando cuidado de não chegar ao ponto de ter sistemas complexos que NUNCA serão usados. Isso apenas vai adicionar dores de cabeça com as quais você não vai querer ter que lidar.

## Fate e Lego

O que Fate tem de igual ao Lego?

Lego se baseia em um conceito básico (encaixe de blocos) e o expande em níveis estratosféricos, mas sempre a partir de um mesmo fundamento, o bloco.

O Fate também funciona assim: baseando-se em alguns elementos simples, você pode gerar Extras extremamente complexos e elegantes. Ao mesmo tempo, você pode "reciclar" em seus módulos idéias que você viu em outros jogos Fate, importando Extras interessantes que permitam a você fazer coisas que você deseja.

Por exemplo: se você está criando um cenário em Fate de Alta Fantasia Medieval com muitas armas mágicas interessantes, ao invés de tentar recorrer você mesmo a criar um extra, pode ser interessante utilizar o sistema de Talismãs de Nest (recentemente traduzido pela Pluma Press). Esse Extra é bem simples e rápido, e pode ser bem útil. Por exemplo, veja o item mágico criado abaixo, usando esse sistema:

+ ___Espadas de Gel Cinético___
+ Espadas de Gel Cinético são Espadas ___Forjada com Materiais que não sofrem inércia___, sendo lendária pela dificuldade de serem produzidas. Apenas os melhores armeiros são capazes de trabalhar o Gel Cinético, e apenas os melhores Espadachins são capazes de usar essas espadas sem correrem o risco de decepar os próprios membros no processo. Entretanto, se o guerreiro for hábil o bastante para controlar tal arma, ele se torna praticamente um exército de um homem só, graças ao poder destrutivo dessa arma, que é capaz até mesmo de destruir muros e armaduras
    + Você recebe +2 em _Lutar_ para Atacar com essa Arma, e também possui os benefícios da Façanha ___Golpe Matador___;
    + Por 1 PD, você pode usar _Lutar_ no lugar de _Ofícios_ para Superar obstáculos físicos, removendo Aspectos de Barreira. Você ainda recebe +2 para o fazer;

Simples e rápido, você tem uma arma muito interessante e que agrega muito à narrativa, usando apenas um Aspecto (___Forjada com Materiais que não sofrem inércia___) e duas Façanhas bem Poderosas. Entretanto, ela é, por si, um Aspecto também importante: ela pode ser roubada, chamar atenção indevida, e você sempre pode decepar fora seu próprio braço se o narrador forçar o fato dela ser ___Forjada com Materiais que não sofrem inércia___ quando você teve um sucesso a Grande Custo.

Poderia passar horas falando de Extras interessantes em outros materiais do Fate, mas fica a dica de consultar todos os materiais de Fate que você tiver acesso ao planejar uma aventura: pode ser que alguns Extras que você estava pensando já estejam prontos, ou ao menos quase prontos, para o que você precisa.

Alguns exemplos:

+ Os já citados _Talismãs_ de Nest servem muito bem para qualquer tipo de Artefato mágico ou supertecnológico;
+ Usar a idéia de _Espíritos_ de _Frontier Spirit_ ajuda quando você quer criar, por exemplo, hordas que na prática façam parte de uma mente coletiva;
+ O sistema de Mantos de _Dresden Files Accelerated_ é bem interessante quando você quiser criar classes que tenham poderes especiais (vamos falar mais sobre isso adiante)
+ Os _Tipos de Poder_ de _Wearing the Cape_ tornam criar um super-heroi algo extremamente simples, ao agregar certas coisas como permissões ou restrições oferecidas pelo Aspecto e outras coisas resolvidas por Façanhas ou mudanças nos comportamentos de perícias
+ A _Convergência_ de _SLIP_ e o _Governo_ de _Uprising_ são exemplos interessantes de como criar uma estrutura que vai ficando cada vez mais hostil conforme ela vai descobrindo sobre os personagens e suas intenções de os destruir;

## Construindo o Extra

OK... Ainda assim, apesar de tudo, você acha que você tem uma ideia muito legal e inovadora de Extra.

Sem problemas, vamos então começar a pensar nas coisas.

Três coisas que você sempre deve ter em mente são:

### Permissões

_Permissões_ são coisas que _"Destrancam"_ o Extra, por assim dizer. O exemplo mais fundamental em Fate envolve a idéia de _Árvores de Façanhas_, onde uma Façanha pode "destrancar" a possibilidades de Façanhas ainda mais poderosas. 

Vamos rever um exemplo que colocamos quando comentamos sobre Façanhas, lá no início da série:

> + _**Clínico Geral:**_ você possui um foco na medicina e em detectar doenças, recebendo +2 nos rolamentos relacionados[^2]
> + _**Cirurgião Geral:** (requer_ **Clínico Geral**_)_ você é capaz de realizar cirurgias corretivas e preventivas. Você pode, como uma ação de _**Criar Vantagens**_, renomear uma Consequência, passando em um teste de _Ofícios (Medicina)_ com dificuldade igual ao nível da Consequência. Se a consequência já tiver sido renomeada, você pode realizar um novo teste, contra nível+2, para remover a Consequência. É necessário dar um tempo mínimo equivalente ao de renomear a Consequência para realizar um novo teste. Em caso de Falha, o personagem deve absorver um ataque equivalente ao resultado rolado;
> + _**Neurologista:** (requer_ **Clínico Geral**_)_ você é especializado no conhecimento do cérebro e do sistema nervoso. Sempre que precisar identificar distúrbios mentais ou situações de comportamento neuroatípico, pode substituir _Empatia_ por _Conhecimentos_;
> + _**Neurocirurgião:** (requer_ **Neurologista** _e_ **Cirurgião Geral**_)_ você consegue realizar cirurgias neurológicas, que de outra forma seria algo extremamente arriscado. Recebe +2 nos testes de _Ofícios_ ao realizar tais cirurgias e, em caso de Falha, pode rolar para evitar danos ao paciente;
> + _**Infectologista:** (requer_ **Clínico Geral**_)_ você conhece muito sobre os efeitos de microorganismos no corpo humano. +2 ao realizar testes de _Criar Vantagens_ envolvendo tal conhecimento;

Perceba que, ao comprar __*Clínico Geral*__, o personagem destranca a possibilidade de comprar __*Infectologista*__, __*Neurologista*__ e __*Cirurgião Geral*__, sendo que se ele comprar as duas últimas, ele pode comprar __*Neurocirurgião*__.

Esse mecanismo, comparável ao sistema de _Tech Tree_ de muitos jogos modernos de _videogame_, permite enfatizar especializações e tornar a coisa mais interessante, ao forçar certas obrigatoriedades para que o jogador obtenha determinadas Façanhas que podem ser, inclusive, muito mais poderosas que Façanhas Comuns, como no exemplo das _Façanhas Centuriãs_ (_Centurion Stunts_) de Young Centurions, que são mais poderosas que o padrão.

## Custo

Uma vez que você tenha a permissão para obter um Extra, você pode precisar ter uma medida de o quão forte alguém é naquele Extra específico. Isso é feito por meio do _Custo_.

Pode não ficar exatamente claro a diferença entre uma _Permissão_ e um _Custo_. No caso, vamos utilizar o exemplo de nosso cenário exemplo dessa série, _Fae Guardians_.

Vamos comparar, por exemplo, nossa ___Portadora do Escopo da Fada Madrinha___ Helen Hemingway, e um amigo dos personagens, o **super-hacker _ciente das criaturas Fantásticas_** William Tsu. Ambos podem fazer tudo por meio de seus _Conhecimentos_ para obter informações e tudo o mais. Entretanto, Helen pode utilizar Magia, enquanto Tsu não pode, mesmo que ambos tenham o mesmo nível de _Conhecimentos_. 

Por que? Porque _Helen_ cumpriu a permissão de ser uma _Portadora de Escopo_, que autoriza ela a usar Magia por _Conhecimentos_. Na prática, ela comprou uma _Façanha_ que lhe permitia usar magia por _Conhecimentos_ (*__Bibbity-Bobbity Boo!__*), que não apenas William não comprou, mas _NÃO PODERIA_, por não ter acesso devido às _Permissões_ envolvidas (___Escopo da Fada Madrinha___)

Isso pode ser ainda mais interessante quando percebemos que o Custo pode ___Não ser obrigatório___: se Helen ___NÃO QUISESSE___ comprar *__Bibbity-Bobbity Boo!__*, ela ainda assim poderia declarar que conseguiria fazer coisas estranhas, devido ao seu Escopo (Permissão), mas não poderia fazer ela tão facilmente (falta do Custo). 

Outro exemplo, que acho clássico, é o de Harry Potter, onde quase todos os alunos de primeiro ano são muito ruins em magia, embora todos tenham _Permissão_ por terem Aspectos de _Bruxaria_. Entretanto, nesse momento apenas Hermione pagou o Custo de colocar _Feitiços_ em sua Pirâmide de Perícias, o que a tornou melhor que os demais nisso.

No próprio exemplo que mostramos da Árvore de Façanhas, o ___Neurocirurgião___ tem que pagar os Custos tanto de sua Façanha quanto das demais façanhas necessárias enquanto Permissão. Isso que torna a diferença entre Permissão e Custo algo confuso. 

Entretanto, vamos clarificar as coisas quando vermos a terceira coisa necessária de se levar em conta ao criar um extra.

## Elementos 

Como dissemos anteriormente, podemos partir do entendimento que Extras e o Fractal do Fate estão relacionados, ainda que não totalmente. Você pode ter um Extra simples sendo simplesmente algo que adiciona novas possibilidades, como fizemos com a Magia de Helen para _Fae Guardians_, que basicamente é uma nova coisa em _Conhecimentos_.

Isso é importante pois, ao criar um Extra, você deve considerar quais serão os elementos que estarão envolvidos. Em uma listagem rápida, podemos considerar cinco elementos. Os quatro padrões são citados no Fate Básico:

+ _Aspectos_ para definir coisas que mudam a história
+ _Perícias_ para novos contextos em ações
+ _Façanhas_ para realizar coisas extraordinárias
+ _Estresse_ para coisas consumíveis

O quinto elemento é a própria Narrativa. Por exemplo,  o Um Anel foi encontrado por Bilbo em _O Hobbit_ de maneira "aleatória". A posse do mesmo foi conquistada por Bilbo apenas como um evento aleatório, mesmo Gandalf reconhece isso no início de _O Senhor dos Anéis_. Portanto, curiosamente, a _Permissão_ para ser o _Portador do Um Anel_ e ter acesso a suas habilidades é simplesmente _Estar em posse do mesmo, seja por cessão, roubo ou de qualquer outra forma_. 

Isso poderia ser resolvido com um ___Aspecto___? Sim, mas pode não ser necessário se for algo muito curto: deixe em aberto, ou caso decida manter como ___Aspecto___, deixe de tal modo que o mesmo não precise ___NECESSARIAMENTE___ ser um Aspecto de Personagem: Bilbo pode ter rolado um teste de Percepção e descoberto aquele ___Anel estranho e sem valor perdido___ (ação de ___CRIAR VANTAGEM___, portanto gerando ___ASPECTOS___) ali no meio das coisas e só depois usado ele, rezando para que aquilo fosse útil. 

De qualquer modo, considere também a narrativa como parte das coisas ao definir Permissões e Custos.

Dito isso, vamos agora analisar cada um desses elementos enquanto sua importância para Permissões e Custos, e como tais coisas podem permitir Extras muitíssimo interessantes.

### Aspectos

Aspectos são importantes porque definem coisas em uma história, isso é um fundamento que tem sido repetido nessa série desde seu início, desde os exemplos com os ___Cavaleiros do Touro de Pedra___ até mesmo a nossa ___Portadora do Escopo da Fada Madrinha___, temos visto toda uma série de situações onde Aspectos, em especial nos Personagens, funcionam como uma _Permissão_.

Esse tipo de _Permissão_ é interessante também para situações onde o _Cenário_ pode ser alterado de maneira estranha, com regras que não funcionam como deveriam. Em meu cenário de Desenhos Animados baseados em _Uma Cilada Para Roger Rabbit_, uma das coisas que estipulei é que nenhum humano poderia utilizar Lógica de Desenho _per se_ (não possuem a Permissão), mas que eles poderiam usar Lógcia de Desenho em _Locais_ que sejam de Desenho Animado, como na Desenholândia (o _Local_ tem a permissão). Quanto aos Custos, são outros quinhentos·

Falando em Custos: alguns Extras que você pense e que sejam realmente poderosos podem demandar algum tipo de _Custo_ como Aspecto. Você pode pensar, por exemplo, em Pactos, onde o personagem tem acesso aos poderes de uma entidade sobrenatural uma vez que ele tenha colocado um Aspecto _"Pacto com..."_ nele.

Um exemplo recente que fiz foi criar uma Façanha Estranha para _Shadow of the Century_, onde a idéia é que o personagem, ao se colocar em uma situação tal de encarnar um personagem ele quase que literalmente o trás a vida e com isso pode distorcer a realidade a um nível onde pode convocar outras pessoas como aliadas dentro de uma temática relacionada ao personagem que ele está encarnando.

Mesmo com as regras de Shadow of the Century permitindo coisas realmente absurdas, achei que esse era um nível de absurdo que as regras não comportavam. Decidi, então, utilizar um custo adicional, além de todos os envolvidos: o personagem deveria optar por sofrer uma Consequência (que são ___ASPECTOS___) _"Encarnando o Personagem..."_ com nível variável, dependendo do tempo que o personagem deseja ter tal Façanha disponível. Esse Aspecto tornar-se parte do personagem (parte das Consequências) por um tempo adequado, e permite que ele seja Forçado no mesmo, o que compensa todos os benefícios que ele tem.

Aspectos enquanto Custo podem ser úteis por isso, por mostrarem que nada vem de graça: uma Forçada no seu Pacto no pior momento pode resultar em coisas terríveis!

O maior problema aqui é que fica um pouco difícil saber quando um Aspecto entra como Permissão e quando entra como Custo, por sua própria característica. Na dúvida, acho interessante que se considere como Permissão. Se necessário colocar como Custo, use as Consequências do Personagem, mas permita que ele também use tais Aspectos, tornando versátil o Extra.

### Perícias

Antes de começarmos, vamos lembrar que, quando falamos aqui de Perícias, pode ser qualquer tipo de estatística que ajuda na decisão aleatória dos dados, sejam Perícias mesmo, Abordagens, Profissões de Jadepunk, não interessa.

Perícias tendem a ser o alvo preferencial para _Custos_, uma vez que muitos Extras podem oferecer novas Perícias, que irão ocupar as pirâmides dos Personagens em substituição a outras capacidades. 

Novamente lembrando uma situação que vimos no artigo sobre Extras, você pode criar uma Perícia apenas replicando ela para um contexto mais exclusivo. Como citamos lá, criar uma perícia _Conhecimentos Bruxos_ permite que um personagem de Harry Potter entenda melhor a cultura dos Bruxos, o funcionamento de sua sociedade e por aí afora. Entretanto, ele sempre vai querer saber qual é a função de um patinho de borracha.

Essa é uma situação legal quando você tem povos que são tão isolados entre si que informações sobre a cultura e estrutura social deles sejam parca. Claro que você pode simplesmente utilizar permissões de Aspecto para dizer se a pessoa compreende aquilo ou não, mas você pode querer deixar mais explícito isso. Apenas tome cuidado para não tornar os personagens excessivamente focados ou incompententes no processo.

Perícias enquanto _Permissão_ não são tão vistas, mas você sempre pode pensar, em especial, em usar Perícias como permissão para Façanhas mais poderosas. Por exemplo, um _Sniper_ pode precisar ter _Atirar **Bom (+3)**_ ou melhor para comprar a Façanha de Sniper. Ou mesmo um personagem pode precisar ter _Vigor **Bom (+3)**_ ou melhor para poder utilizar um ___Rifle Gravitrônico Portátil___, já que ele é grande, pesado e desengonçado. 

Ao definir uma Perícia enquanto Permissão, é interessante clarificar que trata-se de um nível mínimo, exceto se você quiser fazer alguma outra coisa situação, como ___Olhai os lírios do Campo___, onde o personagem não pode ter ___Recursos___ em sua pirâmide, mas pode utilizar ___Comunicação___ no lugar de ___Recursos___ para obter bens de primeira necessidade, com +2 no teste. Em um cenário de guerreiros e monges em uma Cruzada pode ser interessante para representar a capacidade de um monge pedinte obter recursos para os personagens.

### Façanhas

Outra que é muito comum de ser adotada como Permissão e como Custo.

Como Permissão já mencionamos anteriormente a ideia de você utilizar como forma de criar Árvores de Façanhas que permitam especializações cada vez maiores, além de caminhos paralelos.

Como Custo, elas funcionam bem quando coisas com tal Extra podem ser diferenciadas entre si baseando-se em certas coisas que umas podem ter e outras não. Um exemplo interessante é em Harry Potter: várias habilidades mágicas mais poderosas, como Animagia, Metamorfomagia, Legilimancia e Oculmancia podem ser tratadas como Façanhas, uma vez que nem todos os bruxos possuem treinamento nessas características.

Uma coisa interessante, em especial em Extras com alto grau de customização, é você tratar uma Façanha como um _placeholder_ de capacidades. Por exemplo, você pode desenvolver algo como

+ ___Rifle Laser Paragon (3 Façanhas):___
    + _Mira Telescópica:_ +2 ao Atacar por Atirar alvos a longa distância
    + _Bateria adicional:_ +2 ao Superar por Atirar problemas de falta de bateria
    + _Tiro Carregado:_ pode sacrificar um turno para carregar energia para um único disparo mais poderoso, com +2 por turno passado carregando. Limite de 3 turnos: caso tal limite seja superado, o atirador recebe um Ataque de Estresse igual ao bônus acumulado (no caso, 6)

No caso, ter um Rifle Paragon equivale a três façanhas.

### Estresse e Consequências:

Aqui temos uma coisa muito interessante.

A parte óbvia é aceitar como _Custo_ a marcação de Estresse e Consequências, como no exemplo que vimos anteriormente ao falar de Aspectos. Além disso, tanto em _Dresden Files Accelerated_ quanto em _Fate Horror Toolkit_ sugere-se o uso de Condições que são marcadas segundo situações específicas, indicando consumo de recursos. Vimos algo similar quando introduzimos Extras e Fractal, com a Metralhadora LionHeart

> **Metralhadora da Corporação _LionHeart_**
>  
> + _**Permissão:**_ Aspecto indicando fazer parte da Corporação _LionHeart_ ou ter a obtido de um Soldado _LionHeart_
> + _**Custo:**_ Façanhas (mínimo 1)
>  
> Equipamento padrão dessa companhia de Mercenários, ela é customizada conforme a necessidade e especialização do soldado que a usa por meio de acessórios que podem ser facilmente plugados à mesma. São consideradas _**Confiáveis**_, mas também são meio _**Pesadas**_
>  
> + _**Poder de Fogo**_: Toda Metralhadora _LionHeart_ oferece +2 ao Atacar alvos em até duas Zonas devido ao poder de fogo da mesma
> ( ) ( ) ( ) ( ) ( ) _**Munição finita**_: a cada turno que for usada, a Metralhadora _LionHeart_ perde uma parcela de sua munição. Marque uma caixa. Quando todas as caixas estiverem marcadas, ela estará _**Sem Munição**_, e o personagem precisará gastar um turno recarregando a Metralhadora. Se desejar recarregar mais rápido ou fazendo alguma outra ação no processo, deve rolar contra dificuldade _**Razoável (+2)**_
> 
> Entre os acessórios mais comuns estão:
> 
> + _**Mira Telescópica:**_ (custo adicional 1 Façanha) você recebe +2 para _Criar Vantagens_ usando _Atirar_ durante Conflitos para colocar alvos _**Na Mira**_. Em outras situações, pode ser usado para _Criar Vantagens_ observando alvos que estejam a longa distância usando _Investigar_
> + _**Baioneta:**_ (custo adicional 1 Façanha) você pode utilizar sua Metralhadora como uma arma de estocadas, recebendo +2 em suas ações de _Lutar_ ao Atacar alvos próximos
> + _**Clip de Munição maior:**_ (custo adicional 1 Façanhas) o(s) clip(s) de munição de sua Metralhadora são maiores que o padrão. Adicione duas caixas adicionais de __*Munição*__ em sua Metralhadora por Façanha. Pode ser adquirido múltiplas vezes
> + ( ) __*Clip de Recarga Rápida*__ (custo adicional 1 Façanha) você pode recarregar automaticamente sua Metralhadora uma vez por conflito usando esse _Clip_. Apague todas as caixas de Munição e marque a caixa do clip. Você precisa gastar uma ação fora de Conflito para recarregar seu Clip de Recarga Rápida e limpar essa caixa. Você não pode usar o Clip se a caixa estiver marcada. Pode ser comprada várias vezes
> + ( ) ( ) _**Lançador de Granadas:**_  (custo adicional 1 Façanha) Sua metralhadora possui um lançador de Granadas Antipessoais, que pode ser usado tanto para _Atacar_ todos os alvos em uma zona específica de maneira indistinta por _Atirar_ ou para _Criar Vantagem_ por Atirar com _**Zonas de Fumaça**_ ou _**Gás Lacrimogêneo**_

Perceba que a ___Munição Limitada___, o ___Clip de Recarga Rápida___ e o ___Lançador de Granadas___ funcionam com Barras de Estresse que são marcadas conforme condições específicas ocorrem durante o jogo. Isso serve para criar situações que funcionam como chave Liga/Desliga: uma vez que você marque sua Caixa do _Clip de Recarga Rápida_, você limpa sua Munição Limitada, indicando recarga, mas você não tem mais seu clip de recarga, não podendo fazer isso novamente.

Agora, uma coisa interessante é você pensar em possibilidades de usar Estresse e Consequências como _Permissão_. Se pensar bem, não é muito diferente do que já fizemos nesses casos apresentados, mas vamos citar um exemplo um pouco mais claro:

> + ___( ) Limit Breaker (Permissão: Consequência):___ marque a caixa do Limit Breaker só após sofrer ao menos uma consequência nesse conflito. Seu próximo Ataque tem um bônus no rolamento equivalente à Consequência mais alta sofrida nesse conflito. Se o ataque acertar, ele é considerado como tendo um nível de Arma equivalente à Consequência mais alta. A caixa é limpa ao final do conflito

Aqui, curiosamente usamos Estresse como Custo e Consequência como _Permissão_: tão logo o personagem tenha sofrido uma consequência, ele pode utilizar essa Façanha. Ela é realmente muito poderosa, se você imaginar que, com uma Consequência Extrema, ele pode somar +8 ao Ataque que se torna um Ataque de Arma:8, mas basta considerar que ele demoraria vários arcos para poder alcançar um poder tão grande que as coisas ficam mais equilibradas.

## Fractal com Fractais

Uma coisa interessante de Fractais na matemática é que eles têm um comportamento de incluir-se a si mesmo na formula que lhes gera. E essa característica permanece no Fractal do Fate.

Não existe nenhum limite, exceto o de importância narrativa e condução de jogo, de o quanto você pode colocar de Extras e Fractais dentro de um jogo. Além disso, você pode colocar inclusive Extras e Fractais que possuam Extras e Fractais.

Parece confuso? Vamos pegar um exemplo: Veículos de Grande Porte

Veículos de Grande Porte podem ser pensados como tendo Aspectos, algumas Perícias e Façanhas e Estresse (que vamos adotar como Condições)

### Cormoran, a Barca Voadora dos _Fae Guardians_

+ #### Aspectos:
    + Leve como uma Pluma, resistente como uma Baleia
    + Resiliente, mas não tenha pressa
    + Uma cidade no ar
+ ### Perícias:
    + _Resistência **Épica (+7)**; Conforto e Instalações **Excepcional (+5)**, Velocidade e Manobrabilidade **Medíocres (+0)**_
+ ### Façanhas:
    + _Bússola Telúrica:_ +2 para _Superar_ problemas  relacionados a condições adversas para manter-se uma rota;
+ ### Condições:
    + [1] Danificada
    + [1] Gravitação Falha
    + [2] Danos generalizados
    + [2] Sem Bússola Telúrica
    + [4] Cristal Mageia Danificado - não pode seguir adiante
    + [4] Em queda livre

Aparentemente parece algo mais ou menos similar a um personagem, certo? Mas podemos tornar ela ainda mais rica. Para isso, vamos colocar um Fractal adicional de _Estruturas_: uma nave seguindo esse fractal possui um número de Estruturas igual a soma de seus níveis em _Instalações_ e _Conforto_. Cada sala pode possuir uma Façanha representando algum tipo de equipamento ou função extraordinária, além de possuir sua própria Condição __[4] Destruída__, o que remove ela, ao menos temporariamente, de uso, quando ela absorver esse total de dano.

Vamos criar algumas instalações, sendo que a _Cormoran_ possui um total de 10 instalações

+ _Canhoneiras x4:_
    + _Canhões:_ +2 ao atacar outras Belonaves Aéreas, Arma: 2
+ _Biblioteca:_
    + _Aetereografo:_ +1 ao _Criar Vantagens_ ou _Superar_ por _Comunicação_ ao fazer solicitações ou demandas a outras naves com Aetereografos
+ _Sala de Máquinas Avançada:_
    + _Cristais Mageia Adicionais:_ pode continuar se movendo, mesmo que marque a Condição ___Cristal Mageia Danificado___. Os personagens podem optar por se renderem, fugindo com a belonave, mas marcando a Condição ___Cristal Mageia Danificado___ e a Condição ___Destruída___ dessa sala.
+ _Postos de Observação x2:_
    + +2 em todos os testes de _Percepção_ para os personagens que estiverem nesses pontos específicos para perceber movimentações próximas
+ _Centro Médico:_
    + +2 em todos os testes de _Conhecimento_ para renomear Consequências indicando o início do processo de cura
+ _Depósito de Mercadorias:_
    + +2 para _Criar Vantagens_ ao negociar com possíveis atacantes. Os personagens podem conceder um conflito marcando a Condição ___Destruída___ dessa sala, sem a necessidade de negociação.

Perceba que basicamente cada sala possui algum tipo de habilidade especial, como se fosse uma Façanha, mas com a Condição __[4] Destruída__ que, se por um lado oferece mais possibilidades de absorção de dano, pode impedir eles de terem coisas importantes quando as mesmas forem destruídas.

E uma coisa interessante, a regra de Salas se torna uma variante da regra para Monstros de Múltiplas Zonas (_Ferramentas de Sistema_, página 153), onde, para o grupo poder exercer as vantagens das Salas, os envolvidos tem que estar na mesma. Se por um acaso uma dessas salas tiverem as caixas Destruídas marcadas, os personagens nas mesmas sofrem tal dano também.

Por fim, essa é a barca final

### Cormoran, a Barca Voadora dos _Fae Guardians_

+ #### Aspectos:
    + Leve como uma Pluma, resistente como uma Baleia
    + Resiliente, mas não tenha pressa
    + Uma cidade no ar
+ #### Perícias:
    + _Resistência **Épica (+7)**; Conforto e Instalações **Excepcional (+5)**, Velocidade e Manobrabilidade **Medíocres (+0)**_
+ #### Façanhas:
    + _Bússola Telúrica:_ +2 para _Superar_ problemas  relacionados a condições adversas para manter-se uma rota;
+ #### Condições:
    + [1] Danificada
    + [1] Gravitação Falha
    + [2] Danos generalizados
    + [2] Sem Bússola Telúrica
    + [4] Cristal Mageia Danificado - não pode seguir adiante
    + [4] Em queda livre
+ #### Salas
    + _Canhoneiras x4:_
        + _Canhões:_ +2 ao atacar outras Belonaves Aéreas, Arma: 2
        + __[4] Destruída__
    + _Biblioteca:_
        + _Aetereografo:_ +1 ao _Criar Vantagens_ ou _Superar_ por _Comunicação_ ao fazer solicitações ou demandas a outras naves com Aetereografos
        + __[4] Destruída__
    + _Sala de Máquinas Avançada:_
        + _Cristais Mageia Adicionais:_ pode continuar se movendo, mesmo que marque a Condição ___Cristal Mageia Danificado___. Os personagens podem optar por se renderem, fugindo com a belonave, mas marcando a Condição ___Cristal Mageia Danificado___ e a Condição ___Destruída___ dessa sala.
        + __[4] Destruída__
    + _Postos de Observação x2:_
        + +2 em todos os testes de _Percepção_ para os personagens que estiverem nesses pontos específicos para perceber movimentações próximas
        + __[4] Destruída__
    + _Centro Médico:_
        + +2 em todos os testes de _Conhecimento_ para renomear Consequências indicando o início do processo de cura
        + __[4] Destruída__
    + _Depósito de Mercadorias:_
        + +2 para _Criar Vantagens_ ao negociar com possíveis atacantes. Os personagens podem conceder um conflito marcando a Condição ___Destruída___ dessa sala, sem a necessidade de negociação.
        + __[4] Destruída__


## Conclusão

Extras e o Fractal do Fate são ferramentas importantíssimas para desenvolver-se todos os elementos necessários para o seu jogo em um nível satisfatório. Na prática, quanto mais você utilizar ambos, mais você os entenderá e conseguirá os aproveitar, seja criando seus próprios Extras e Fractais, seja utilizando Extras e Fractais de outros jogos Movidos pelo Fate. Sempre procure observar os jogos Movidos pelo Fate e lembre-se que Fate tem algo de similar com os Lego: não importa o quão complexo o jogo seja, você pode o destrinchar e tentar desacoplar um Extra ou Fractal para usar o mesmo em seus jogos.

Nos vemos no próximo artigo.

Até lá, role +4.


[^2]: perceba que aqui temos algo similar à Façanha de Conhecimentos _Especialista_
