---
title: "Domando o Fate - Parte 14 - Os Principais Erros de Principiante no Fate"
subheadline: Uma Introdução ao Fate 
date: 2020-01-27 14:00:00 +0300
layout: post
categories:
 - RPG
tags:
 - Fate
 - Fate Core
 - Fate-Core
 - Artigos
 - Dungeon Geek
 - Introdução ao Fate
 - introducao-ao-fate
 - Principiantes
 - Erros
order: 16
header: no
---

> Publicado Originalmente no Site da [Dungeon Geek](https://www.dungeongeek21.com/domando-o-fate-os-principais-erros-de-principiante-no-fate/)

Duas verdades fundamentais sobre a vida:

1. Você vai errar cedo ou tarde em qualquer coisa que você faça
2. Só quem não faz não tem chance de errar

Dito isso, percebo que muitos iniciantes em Fate (e em RPG em geral) cometem alguns erros comuns. 

Isso não é problema: má compreensão e entendimento errôneo é comum quando você não está habituado àquele conjunto de regras. Seria estranho ver alguém cujo conhecimento de regras fosse instantâneo: isso não é realista.

Mesmo eu demorei muito tempo até entender as regras do Fate, e comecei a conhecer o Fate ainda na versão 2.0, e cada mudança de regras mexeu no meu entendimento do jogo e, portanto, foi quase como um recomeço.

A ideia aqui não é fazer julgamento de valor: isso seria não apenas não-produtivo, como estúpido. Aqui vamos citar alguns erros comuns que iniciantes cometem (e alguns nem chegam a ser erros no sentido exato da palavra) e dar sugestões de como os contornar.

Em vários momentos iremos dialogar nesse artigo com artigos que escrevemos anteriores. Isso porque muitos desses erros derivam de uma inexperiência e incompreensão de como funciona o Fate e o seu foco. Desse modo, esse pode ser um bom momento de conversarmos um pouco sobre isso.

Vamos então a algumas coisas bastante comuns de serem feitas

## 1. Mecanicismo Excessivo

Vamos começar com um clássico: o _Mecanicismo Excessivo_.

Isso aparece de duas formas:

A primeira é a do Narrador que pensa, por exemplo, em ter duzentas perícias, ou então uma lista gigantesca de Façanhas, ou ainda utilizar um Fractal monstruoso e cheio de minúcias.

A segunda é a do jogador que, sem entender as nuances do Fate, fica procurando os Poderes Especiais, as Perícias Específicas e as regras detalhadas.

Isso não é problema nenhum, ter regras detalhadas.

Entretanto, a pergunta que você deve se fazer é _"essa regra é realmente necessária?"_

Fate é um jogo narrativista, um jogo onde quem dita as interações não são os valores da ficha ou o estilo do jogo, mas a _narrativa do jogo_. Podemos dizer que a _Narrativa é o Rei_. Importante ressaltar que ela não ELIMINA as regras, mas ela _dita_ de certa forma o que deve ser feito ou não com as regras.

Isso meio que mata o comportamento padrão do jogo, onde o lado do jogo é quem dita as regras: se a regra não existe, você não faz. Aqui, se a regra não existe, ela pode ser moldada ou criada _on-the-fly_, ou obtida de um repertório desenvolvido pelo narrador. (_"Oh, Toodles!"_, lembram?)

Quando comecei a fuçar com o Fate Básico, ainda em inglês, fiz várias experiências com o mesmo, e meu conhecimento das versões do Fate anteriores ao Fate Básico e de seu avô, o Fudge, me permitiriam evitar algumas das Armadilhas. 

Entretanto, eu mesmo caí nesse problema: quando fiz minha adaptação de _Pole Position_, o desenho clássico dos anos 80 de corredores que combatem o crime, fiz um Fractal inteiro de veículos que era basicamente uma variação do Fate Acelerado. Isso em si não é errado... Mas imagine um jogo _one-shot_ onde os jogadores criassem seus próprios veículos e módulos, além dos personagens (os módulos eles próprios personagens plenos segundo o Fate Acelerado)? Tempo demais desperdiçado ao criar personagens complexos demais em uma situação onde pode acabar sendo desnecessário tanta complexidade.

### ___Como evitar?___ 

A primeira dica é: deixe fluir o jogo. Fate tem uma característica maravilhosa que suas regras são concretas o bastante para terem impacto real no jogo, mas abstratas o bastante para poderem ser customizadas durante o mesmo sem o quebrar. 

Segunda dica é: não sobreprepare em excesso... Você pode considerar interessante deixar alguma "gordura" de regras preparadas para situações especiais. Isso pode ser bom, mas apenas tome cuidado para que isso não acabe se transformando em algo excessivo.

Terceira dica: monte seu próprio _Toodles_. Tenha em sua mente regras simples que você possa aplicar em momentos diversos de maneira rápida. Quando pensei na série _Oh, Toodles!_, foi visando isso: demonstrar algumas das ferramentas mais interessantes que notei durante esse tempo envolvido com o Fate e com isso oferecê-las para que outros narradores montem seu arcabouço.

E para jogadores, não se preocupe sobre tentar achar muitas regras. Em caso de dúvidas, consulte o narrador para que o mesmo diga como (e se) uma nova regra irá funcionar. Você é livre para dar sugestões, mas atente ao fato do narrador ser quem _advoga_ as regras, ou seja, as interpreta dentro do contexto do jogo específico: mesmo em Fate, duas situações similares podem ser entendidas de maneiras diferentes.

## 2. _"Era assim no sistema de onde vim"_

Alguns jogadores mais experientes em outros jogos, mas recém-chegados em Fate, tendem a colocar sua experiência em outros jogos como uma base para entender o que está acontecendo em Fate. 

Esse é um comportamento comum e que ocasionalmente funciona, mas quando o jogador vem de jogos onde a Narrativa não é tão importante e o jogo dita o fluxo dos eventos,isso pode ficar complicado, pois pode resultar na situação anterior rapidamente.

Além disso... Em Fate, você pode resolver situações de maneiras imprevistas: por exemplo, em _Weird World News_ são usadas as regras de Conflitos para rolar uma perseguição ao estilo Scooby Doo. E você sempre pode resolver um combate de MMA usando as regras de Disputa, ao invés de Conflitos, se você não tiver nenhuma regra especial a ser usada (_Iron Street Combat_ pode oferecer tais regras).

Fate mantem pouca similaridade com os sistemas tradicionais de RPG, por isso é muitas vezes complexo tentar emular o estilo "tradicional" de jogo no Fate, ainda que seja possível (e até por que não dizer simples) transportar e emular as regras dos sistemas mais tradicionais, incluindo o Mais Jogado, sem muitos problemas.

Entretanto, deve-se tomar cuidado de evitar um possível engessamento das regras do Fate ao recorrer-se a isso. Se a proposta do jogo e for essa e o grupo estiver disposto a tal engessamento, não há problema: às vezes, em um jogo voltado a um estilo determinado de jogo, um engessamento pode ser válido, como acontece no _Iron Street Combat_ e em _Weird World News_. Porém, é melhor evitar tal engessamento se isso puder ser evitado

### Como evitar

Aqui é lembrar sempre que a flexibilidade do Fate é que o torna interessante. Além disso, lembrar sempre que _"Entre regra e narrativa, a narrativa vem primeiro"_: você não precisa trazer todas as regras, mas sim trabalhar suas necessidades durante o jogo. Isso é interessante pois cria uma forma orgânica de tratar não apenas o cenário, mas as mecânicas que o envolve. E isso é extremamente positivo, pois mecânicas que são mais relevantes podem aparecer, enquanto outras menos relevantes podem inclusive serem abandonadas ou resumidas em importância. 

Também possui o outro lado da moeda: se o teu jogo está planejado para um determinado tipo de conceito, definir regras bem explícitas e rígidas que reforcem os tropos do conceito é uma ótima ideia. Jogos como _Loose Threads, Weird World News, Uprising, Strays, Til Dawn_ e _Iron Street Combat_ visam enfatizar certas experiências, e suas regras procuram enfatizar tal coisa. Obviamente, isso pode tornar "desvios" mais complicados de se lidar, mas ainda assim você pode utilizar isso para tornar o jogo ainda mais palatável para iniciantes.

## 3. Trazer tudo, incluindo a pia da cozinha

A super-preparação, o maior inimigo do narrador do Fate.

Uma das coisas mais comuns que acontece ao começar a aprender o Fate é achar que ele é "incompleto". De fato, isso é uma aparência bastante óbvia, haja visto o fato que as regras parecem se focar apenas nos eventos mais simples.

Então, quando você percebe que o Fate tem o sistema do Fractal, você começa a "pirar" e fazer tudo de tudo, tornando mesmo uma arma simples algo gigantesco com grandes nuances e complexidades de regras.

Isso é completamente natural: ao descobrir um novo sistema, é normal e interessante tentar explorar o mesmo e ver até onde se consegue ir sem quebrar o mesmo.

O problema é que isso pode resultar, cedo ou tarde, em você ter uma "superadministração": você ter tantos Aspectos, Perícias, Façanhas, Extras, Fractais e todas as regras envolvidas e com isso perder-se nas interações entre os mesmos. De fato, a maior complexidade do Fate não está nos elementos individuais, mas sim nas interações envolvidas entre eles, ou mais exatamente da quantidade potencialmente abissal de interações.

Isso é natural de qualquer sistema: mesmo os sistemas OSR sofrem quando começa a se incluir elementos demais, não importando se são previstos originalmente ou _homebrews_. Esse problema não é exatamente um problema do Fate, mas graças ao Fractal, isso fica potencialmente perigoso, em especial quando se tem baixa experiência. Acreditem em mim, eu já fiz isso, e você cedo ou tarde fará.

### Como evitar

_Keep It Simple and Safe_ (KISS) é a regra básica: se não houver uma necessidade narrativa e/ou mecânica REAL para aquela mecânica entrar em jogo, não a coloque. Tente utilizar sempre as opções mais simples que atendam a sua necessidade. 

Se o tipo de arma não fizer diferença no jogo, se uma espada ou faca for a mesma coisa, se usar uma seta de zarabatana ou a BFG 3000 não fizer diferença, você NÃO PRECISA de uma mecânica de Armas. 

Se você ser um plebeu sujo no meio do chiqueiro ou o Rei Luis XV em Versalhes não fizer diferença, você não precisa de mecânicas de status social. 

Se você ser um pequeno time com apenas um carro e um motor, ou for a Penske ou a Ferrari não fizer diferença, você não precisa de mecânicas de veículos.

Isso é a ideia de _"entre narrativa e mecânica, a narrativa vem primeiro."_ É ela quem vai ditar se e quando uma regra será usada.

Obviamente, existem regras que se acoplam melhor que outras, mecânicas que, ao serem inseridas ou removidas, impactam mais ou menos nos personagens em termos mecânicos e narrativos. Certamente você perceberá logo de cara quando uma determinada mecânica mais complexa será necessária: em um jogo de magos urbanos, um bom sistema de magia se faz necessário. Em um jogo medieval histórico, um bom sistema de status social e suas implicações é importante. Em um jogo de revolucionários contra um governo corrupto distópico, um sistema que reinforce mecanicamente a repressão narrativa do governo é _sine qua non_. Essas mecânicas podem ser o quão complexas forem necessário. Isso permite que os cenários explorem mecanicamente coisas que ditem o clima da aventura, como acontece em _Til Dawn_ com batalhas de _Drag_ e em _Weird World News_ com sua mecânica estruturada para uma aventura ao estilo Scooby Doo.

De outro modo, evite a complexidade: pode acreditar, se uma mecânica for relevante ao jogo, ela vai aparecer e você vai logo se ver pensando em ideias sobre ela. E facilmente você vai as integrar ao jogo

## 4. _"Esse personagem é inútil!"_

Ah...

O Clássico _"Esse personagem é inútil!"_

Muitos jogadores (e narradores) se vêem em uma situação assim, onde o jogador vê a ficha e, a não ser que ele tenha algo _OBVIAMENTE_ focado em combate, ele acredita que o personagem é inútil. 

Isso (na minha opinião pessoal) ocorre em especial porque a maior parte dos RPGs mais clássicos se foca em regras de combate, o que (somado a uma série de detalhes de construção que não cabem aqui) faz com que um personagem com pouca enfâse em atributos físicos e de combate resultem em um personagem potencialmente chato (sem falar de vida curta).

E para não ser injusto, esse problema também ocorre, por exemplo, em Vampiro, com o famoso Toreador artesão com 5 de Briga (se fosse um _Artista **marcial**_, ao menos); ou em GURPS, com o famoso _"Kit do Psicopata Feliz"_, o fato de comprar a combinação de _Fúria, Sadismo_ e _Sanguinolência_ como forma de justificar uma interpretação _Murderhobo_ além de obter alguns pontos de personagem no processo para melhorar as "chances de sobrevivência" do personagem.

E isso acaba vazando para Fate, em especial com jogadores experientes em jogos clássicos, já que eles carregam consigo as idiossincrasias do sistema clássico.

### Como evitar

Primeiramente, lembrar que no Fate não existe apenas a ação de Ataque durante um Conflito: um personagem pode tentar virar uma mesa para Criar uma Vantagem de ___Cobertura___ para se esconder de tiros, ou ainda pode tentar Superar essa mesma ___Cobertura___ para tirar inimigos de sua proteção. 

Além disso, Conflitos em Fate não são necessariamente apenas físicos, ou mesmo como um todo físicos. O Bárbaro do outro lado é um ___Idiota___ (você descobriu esse Aspecto)? O provoque o bastante para fazer ele ficar ___Puto de Raiva___ (Consequência Suave) e deixe ele vir para cima de você enlouquecido... De modo que seu guerreiro o pegue de jeito com a espada direto na barriga!

Em especial a ação de Criar Vantagem é muito útil nesse quesito: aquele Bardo que normalmente pareceria inútil pode atochar Invocações Gratuítas em uma ___Canção de Escárnio___ humilhando seus inimigos que, enlouquecidos, atacarão de maneira descuidada, o que permitirá aos "batedores" do grupo utilizarem tais Invocações Gratuítas para tornar o ataque ainda mais poderoso, aproveitando de brechas e pontos cegos para tornarem seus ataques mais avassaladores.

Além disso, Façanhas existem para prover meios de ataques "incomuns": você não tem Lutar? _Já Li Sobre Isso_ em Conhecimentos pode te ajudar (_"Segundo o Livro Hong Kong do Kung Fu..."_ - Hong Kong Fu). Não tem Atirar? Bem... Compre uma façanha de _Criador de Explosivos_ que adicione à perícia de Ofícios a ação de Ataque. Ou mesmo utilize sua Façanha _"Por Favorzinho!"_ para trazer para jogo um bando de _Pais Raivosos_ quando você precisar atacar um Valentão.

E lembre-se: Aspectos usados de maneira Inteligente sempre são uma ótima saída. É um _Diplomata_? Provavelmente será capaz de trazer _Forças da Lei_ para lutar por você. Você é um _"Mago Franzino"_? Clássica _Chuva de Meteoros_ para isolar os inimigos de você.

Fate premia jogadores que pensem em todos os seus recursos e saibam ter _soluções criativas_ com as mesmas. Não basta ter perícias altas, mas saber usar seus Aspectos, Façanhas e, acima de tudo, as ideias para os usar de maneira inteligente para que você se saia bem.

## 6. _"Meu personagem não evolui!"_

Outro clássico vindo do design de jogos mais tradicional é a aparente falta de evolução dos personagens em Fate.

Fate possui algumas restrições visando tornar o crescimento do personagem orgânico: pontos de perícia sendo adicionados aos poucos (nos Marcos Significativos), Pirâmide e Colunas, Perícias-Pico, etc, o tornam menos sujeito ao _min-max_, ao mesmo tempo que o torna um pouco menos evoluível, ao menos visto de fora.

Isso se deve em especial por não existir o conceito de _Experiência_ como em jogos clássicos (e alguns não tão clássicos, como o _Cypher_ e os jogos _Powered by The Apocalypse_), onde o jogador tem uma visão _meta_, abstrata, do crescimento do Personagem.

Além disso, a maioria dos sistemas costumam tratar evolução como _crescimento em Poder_: raramente você vê personagens nesses jogos mudando, e em geral é como uma coisa que não resulta em perda (como no caso do sistema de multiclasses), onde na prática você apenas adiciona novos poderes a seu repertório, sem deixar de ter aqueles que você tinha previamente. Isso acaba resultando em maximizações mecânicas que em termos narrativos não acrescentam nada exceto incongruências (quem não lembra do combo do _pegar 1 nível de Ranger_ do D&D 3.0? Até Elminster tinha 1 nível de Ranger!).

Poucos sistemas tratavam a evolução como mudança: _Call of Cthulhu_ tinha algo assim na questão dos _Mythos_, onde quanto mais você aprende sobre os Mitos, menor sua sanidade máxima pode ficar. Você vai ficando corrompido pelos conceitos alienígenas e inumanos dos Grandes Antigos, e isso muda você, sem falar na questão da loucura.

### Como evitar

Aqui a forma de evitar seria...

...

...

...

... Não tem como.

Fate tem uma previsão de muitos pontos de mudança (os Marcos), onde o personagem pode ir se readaptando e evoluindo: habilidades antigas podem ser substituídas enquanto novas entram em seu lugar. Um guerreiro cansado do sangue pode se unir a um Culto da Paz, enquanto um pacifista dizimado pelo ódio após uma tragédia pode se tornar um assassino serial. 

Enfatizar tais momentos é a melhor coisa para o narrador: antes de oferecer um Marco Significativo, dê alguns menores, para os jogadores ajustarem suas perícias conforme o que eles vão percebendo ser mais interessante. Com o tempo, eles irão perceber que tais ajustes permitem fazer com que o personagem aparente mais poderoso em coisas que são _"mais importantes"_, mas ao custo óbvio (e coerente) na redução das competências não usadas.

Lembre que as Perícias podem significar muitas coisas: _Recursos_, como exemplo clássico, não é apenas uma representação de seus recursos financeiros, mas também _a capacidade de obter e exercer bom uso de Recursos_. Se o seu personagem for o clássico protagonista de _Isekai_, o fato de você não ter mais o seu dinheiro pode não ser tão problemático: você logo perceberá como levantar novamente recursos financeiros. 

Para Façanhas, sempre existe a opção da redução da Recarga para obter-se novas Façanhas. Lembre que isso, entretanto, tem um custo: sua Recarga sendo menor, o personagem terá menor, digamos assim, _livre arbítrio_, já que terá menos chances de recusar Forçadas nos piores momentos, ou terá que contar com todo o seu arsenal de ideias (e uns bons dados) para não precisar Invocar seus Aspectos. Esse é o famoso dilema _Batman vs Superhomem_: o Super-homem tem menos "Coisas legais" (Façanhas), mas consegue fazer coisas mais "épicas" graças ao seu arsenal de Aspectos e Pontos de Destino, enquanto o Batman, ainda que tenha mais Façanhas, ainda se vẽ arraigado a uma imagem que tem que sustentar a todo custo, devido à baixa Recarga.

Deixando tudo isso a claro, nada impede que você ofereça recursos adicionais aos personagens, dependendo do cenário. Em _Young Centurions_, todo personagem jogador (e alguns do narrador) tem uma Façanha Especial, a _Façanha Centuriã_, que é levemente mais poderosa que as Façanhas Comuns. Em _Wearing the Cape_, cada personagem heróico pode escolher Façanhas que mudam conforme seu Tipo de Poder, que também oferece certos benefícios ou impedimentos em termos de narrativa (por exemplo, um personagem Atlas pode voar por ter a Permissão vinda do Tipo de Poder, enquanto um Merlin é capaz de usar e identificar magia apenas pelo fato de ser um Merlin), entre outras coisas.

Porém, sempre se foque na noção da evolução enquanto a "mudança do personagem". O Frodo Bolseiro sobrinho de Bilbo não era o mesmo que o Frodo Portador do Anel, que não era o mesmo que o Frodo-dos-nove-dedos. Sam Gamgi que era apenas um Jardineiro não era o mesmo que Sam Gamgi que se tornou Senhor de Bolsão e Prefeito do Condado. E assim por diante.

Os personagens mudam, se tornam mais aptos para os desafios que vão surgindo.

Ou seja, evoluem.

## 7. _"Meus jogadores não colaboram!"_

Por fim, vem o caso clássico do jogador que "vem com vícios": ele tem uma expectativa baseada no seu sistema favorito e não compreende como o Fate funciona.

E isso também vem com duas situações perigosas envolvidas:

1. O _"você está interferindo com a minha agência"_
2. O _"Mas não era isso que eu queria!"_

Fate é um sistema Narrativista, um estilo de sistema de RPG ainda muito incomum (ao menos no ambiente brasileiro). Desse modo, jogadores experientes, ao virem para o Fate, carregam seus "vícios", as tarimbas que aprenderam em outros sistemas e o estilo e formas de pensar-se o jogo dos mesmos.

Que muitas vezes não condizem com o que é proposto no Fate.

Ao mostrar-se como um sistema narrativista, a primeira coisa é _"entre regras e narrativa, a narrativa vem PRIMEIRO!"_ Isso não apenas quer dizer que o que vale são as ações, mas que as mesmas devem ter impacto e serem retroalimentadas e impactadas pelo sistema.

Em alguns jogos mais tradicionais, em estilos mais tradicionais, certas regras podem ser ignoradas ou torcidas. No Fate, curiosamente, isso não é tão possível, pois as regras não são ignoradas e torcidas pelo estilo de jogo (sim, Carisma 8 em OSR - estou falando de você). 

Isso costuma ser justificado pela noção de _"agência do jogador"_, alegando-se que pedir rolamentos infringe tal agência do jogador, que deveria ser capaz de tentar tudo e ser beneficiado com uma ideia criativa.

Em momento nenhum Fate ignora isso...

O detalhe é que muitas vezes esse argumento é usado por jogadores mais experientes como:

+ Forma de roubar tempo de tela

Isso é feito porque jogadores mais experientes sabem os pontos de regra mais cobrados de maneira mecânica, pontos onde não haver a cobrança mecânica (rolamento de dados, uso de recursos e assim por diante) é algo impossível. Em geral, isso acontece em situações de combate, onde curiosamente a ideia de testar-se o jogador e não a ficha é abandonada. 

Desse modo, o jogador experiente de certa forma "trapaceia" o resto do grupo, ao alocar seus melhores naipes naquelas situações que serão obviamente demandadas mecanicamente, pois saberá que, naquelas onde não há grandes cobranças mecânicas, ele pode conseguir usar sua "perícia de jogador" para cobrir a deficiente "perícia de personagem", em um dos lados mais perversos do _metagaming_. Esse problema não é restrito ao Carisma, mas classicamente é onde o Carisma é mais afeta, sendo isso tão patente que isso afeta até o equilíbrio dos atributos em jogos um pouco mais modernos, como no D&D 3.5, onde uma redução de Força impacta mais do que a do Carisma.

+ Forma de criar um _gatekeeping_

Como derivação do fato acima, jogadores mais experientes conseguem usar isso como uma forma de _gatekeeping_, tanto contra jogadores novatos com contra narradores que tenham outra forma de ver o jogo.

Alegando-se que a cobrança do uso dos recursos do personagem (ou como é dito de maneira depreciativa, "a ficha") seria restringir a "agência" do jogador, ele se beneficia do estilo do jogo para podar jogadores que investiram em outros estilos de personagem (ou simplesmente foram ruins demais no sorteio de seus atributos), além de impedir que o narrador tente, por meios mecânicos, oferecer "tempo de tela" para os jogadores que não se sobressaem em momentos onde a cobrança mecânica é _sine-qua-non_.

Por fim, aqui deve ter ficado claro o que ocorre: jogadores experientes conhecem as falhas do sistema (mesmo que não assumam ou não considerem falhas). 

Isso não é ruim, muito pelo contrário: é parte da diversão para alguns explorar as falhas do sistema de jogo em benefício seu e (nos BONS jogadores) em benefício do grupo. 

O problema é que. ao irem para o Fate, tais jogadores ficam incomodados quando o Fate expõe que qualquer coisa que possa ter impacto suficiente na narrativa deva ter um impacto mecânico: isso impede que eles subvertam os pontos fracos do sistema para se benificiar em detrimento ao "tempo de tela" de outros jogadores.

### Como evitar

A primeira coisa é lembrar sempre que RPG é um jogo _coletivo_: um jogador que roube o "tempo de tela" de outro está fazendo mal não apenas para si, mas para todo o grupo.

A segunda coisa é ficar sempre atento para esse tipo de situação e oferecer ajuda a jogadores iniciantes, mostrando como as mecânicas do Fate podem beneficiar eles a aproveitar melhor o jogo.

Qualquer acusação de que _"Meu Aspecto não representa isso"_/_"Essa perícia não demanda teste!"_/_"Por que preciso de uma Façanha"_ deve ser explicada em termos narrativos, mecânicos e de como um interfere no outro...

+ _"Seu personagem é um **Bárbaro Bruto das Montanhas do Norte**? Que pena... Os guardas da cidade olham torto para você"_
+ _"Você matou todos os guardas, certo? Bem... Agora **tem um peso sob sua cabeça**, e o resto do grupo te observa meio torto."_
+ _"Você tentou negociar uma saída honrosa com o Rei depois de ter exterminado todos os seus guardas? OK... Rola aí um **Comunicação**. Ei... O cara não te conhece de Adão e você é um **Bárbaro Bruto das Montanas do Norte**... Ainda tou dando uma boiada: em uma situação normal você já estaria no cadafalso..."_

Por fim...

Uma dica que vale inclusive para narradores clássicos.

___Agência não significa ausência de Consequência!___

Vou repetir:

___AGÊNCIA NÃO SIGNIFICA AUSÊNCIA DE CONSEQUÊNCIA!___

Você pode _sim_ pedir um rolamento de Carisma do Bárbaro bruto que vai negociar com o Rei. 

> _"Ah, mas meu bárbaro vai negociar mostrando seu machado..."_
> 
> E o Rei te manda para o cadafalso na hora
> 
> _"E eu mato o rei"_
> 
> E a população se revolta contra você
> 
> _"Mas vou convencer eles que o rei era um crápula abusivo!"_
> 
> Rola seu Carisma 8 para ver se eles caem na tua... 
 
Boa sorte...

... vocẽ vai precisar!

Dito esse pequeno _rant_...

Agência implica em você não impedir o jogador de TENTAR fazer qualquer coisa. Você não impede um mago de tentar atacar um goblin com seu bastão quando a magia acaba, não seria justo impedir o bárbaro de tentar negociar com o rei.

Entretanto, Agência _não quer dizer que você será bem sucedido sempre_! Da mesma forma que o ataque com bastão do mago pode resultar em ele (e o resto do grupo) tendo a garganta cortada pelo goblin, a negociação do bárbaro pode resultar em ele (e o resto do grupo) serem enviados para o cadafalso.

Isso em Fate é explicito o tempo tudo: Forçadas (e suas negações) e Invocações de Aspectos podem ser usadas o tempo todo. Sua ficha não substitui sua agência ou habilidade de jogo, mas a _potencializa_.

> _Meu **Bárbaro das Montanhas de Gelo** vai negociar com o Rei!_
> 
> _Bem... você sabe que o reino tem um problema com tribos invasoras **das Montanhas de Gelo**, e o Rei reconhece suas vestes... Ele não parece muito à vontade de falar com um "selvagem ridículo"_ (Narrador oferece PD)
> 
> _Não... Meu bárbaro sabe que é hora de manter a compostura e tentar, mesmo sendo rídiculo, falar de maneira mais cortês_ (Jogador paga PD recusando a Forçada)
> 
> _O Rei observa seu comportamento e diz:_ "Parece que você tem um pouco de educação e respeito... E sua demanda não me parece hostil. Diga-me qual ela é."

Qual a chance de dar certo? O Narrador vai dizer... De repente, se o jogador tiver um bom rolamento, ele pode conseguir... E mesmo com um ruim, ele pode ser mandado para a Masmorra, onde acabará descobrindo que o Rei está mancomunado com os inimigos da mesa.
 
_"Mas se eu aceitar as forçadas, vou ficar complicando a vida!"_

Mas pense em todos os PDs que você poderá usar para tornar sua _Força Bárbara_ ainda mais poderosa por ser um ***Bárbaro das Montanhas de Gelo***

Fate é focado não no "poder", mas no "tempo de tela": todo personagem Fate pode tentar fazer coisas interessantes em um momento ou outro, basta entender como as coisas funcionam. 

E se, ainda assim, você quiser usar os momentos do jogo para nulificar seus companheiros jogadores...

Desculpe, mas você está no jogo errado.

O Fate diz explicitamente que _"qualquer momento onde a Falha e o Sucesso providenciarem algo interessante para a narrativa, deve-se rolar os dados"_. Isso se deve por ser os momentos onde, mesmo que os eventos sejam fáceis, ainda existe uma chance de uma complicação interessante aparecer. Ou, mesmo onde os resultados forem teoricamente impossíveis, você AINDA TEM, por MENOR QUE SEJA, uma chance de sucesso.

Os dados ajudam a visualizar tal sucesso. Junto com todos os mecanimos ao redor (Pontos de Destino, Aspectos, etc)

Até porque, às vezes, depois das hostilidades do Bárbaro Bruto o Bardo do grupo descobre o melhor caminho para negociar com o Rei, mostrando Aspectos do mesmo... :)

## Conclusão

Erros de principiante são normais, sejam aqueles derivados de inexperiência, vícios, compreensão inadequada ou tentativas intencionais de explorar-se o jogo. Nosso objetivo aqui é mostrar alguns deles e sugerir como evitá-los. Mas a única forma 100% segura de evitar tais erros é estudando o sistema e o conhecendo.

Então, até nosso próximo artigo e...

Role +4!
