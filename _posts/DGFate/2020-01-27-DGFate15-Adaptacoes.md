---
title: "Domando o Fate - Parte 15 - Adaptando algo que eu gosto"
subheadline: Uma Introdução ao Fate 
date: 2020-01-27 15:00:00 +0300
layout: post
categories:
 - RPG
tags:
 - Fate
 - Fate Core
 - Fate-Core
 - Artigos
 - Dungeon Geek
 - Introdução ao Fate
 - introducao-ao-fate
 - Adaptações
 - Erros
order: 17
header: no
---

> Publicado Originalmente no Site da [Dungeon Geek](https://www.dungeongeek21.com/domando-o-fate-adaptando-algo-que-eu-gosto/)

Uma coisa muito comum no mundo de RPG é fazer _adaptações_. Na prática, boa parte dos jogos de RPG que as pessoas jogam são basicamente adaptações: ainda que os cenários comercializados normalmente sejam autorais (ou seja, em teoria, novos), muitos são bem carregados de referência. Além disso, o hábito de realizar-se adaptações deve-se à própria característica humana de querer, ao mesmo tempo, contar novas histórias e "pertencer a algo": ao realizar uma adaptação de um cenário, sempre há chance de, por conta própria, "preencher lacunas" que, na opinião da pessoa, o autor deixou de lado.

Esse processo de _fanfic_ é algo que é muito importante: pensando em séries como Doctor Who, Star Wars e Star Trek, elas só sobreviveram a longos hiatos graças ao processo dos fãs de criar em cima do que foi deixado previamente. Em maior ou menor grau de "oficialidade", todos esses cenários sobreviveram a hiatos graças aos fãs e sua produção independente, ou a produções que foram feitas por comissionamento, como ocorreu no caso de Star Wars e seus módulos do período da AEG. 

Dito isso, adaptar cenários em Fate não é algo difícil: o Fate, pela sua característica de focar na narrativa e pelo fato de trazer regras que suportam, retroalimentam e são retroalimentados pela narrativa, permite descrever facilmente cenários de todos os tipos de mídia. Veremos algumas dicas sobre como realizar tais adaptações, o que também pode ajudar em casos que você queira subverter ou modificar tais cenários.

## Forme Repertório

Fate é um jogo que possui muitos elementos simples que podem ficar bastante complexos conforme a necessidade. Por isso, formar um "repertório de regras" é muito interessante.

Existem muitos jogos para Fate, desde materiais publicados gratuitamente por fãs do sistema quanto manuais caros e ricos em detalhes. O importante é consultar todos os materiais possíveis e formar seu repertório.

E por que isso?

Primeiro: uma série de jogos para Fate são baseados em Tropos genéricos, com algum tipo de _plot-twist_. Por exemplo: tanto _Eagle Eyes_ quando _Nitrate City_ possuem fundamentos em filmes _noir_, mas um se passa em Roma enquanto no outro monstros de filme B sairam de seus filmes e ocuparam a cidade de Los Angeles, a Cidade de Nitrato.

Desse modo, ao ler vários livros Fate, você vê como múltiplas regras podem, à sua própria maneira, enfatizar detalhes do cenário para formar o clima dos mesmos.

Segundo: ao conhecer os múltiplos cenários Fate, você pode utilizar alguns deles como base para jogos em diversos estilos, em especial se a sua adaptação for de algo que segue um tropo bem fixo, como Contos de Fadas ou Investigadores Adolescentes ao estilo _Scooby-Doo_. Para os casos citados anteriormente, você poderia utiilizar _Loose Threads_, _Nest_ e _Weird World News_ como base de suas adaptações, por exemplo, de _Once Upon A Time_ ou _Tutubarão_.

Por fim: você pode aprender a lidar com tropos de maneiras diversas e interessantes. Por exemplo: _Psychedemia_ pode tanto servir tanto para uma adaptação de _Jogo do Exterminador_ (o tropo principal) quanto para algo similar a _Matrix_ (tire a parte espacial e adapte a questão dos poderes paranormais) ou para um jogo com uma pegada de _Babylon V_ (remova a parte da _"Realidade Paralela"_ do Realm, ou a utilize se totalmente se for o caso). Por aí você vê como um jogo pode ser escrito de maneira flexível e ao mesmo tempo interessante com pouco texto.

Desse modo, ao fazer isso, você com o tempo vai percebendo os tropos que cada cenário envolve, como aproveitar eles e como combinar elementos de regras. O que permite que você economize tempo com aquilo que realmente fizer diferença, como mostramos no artigo sobre Extras.

## A milhagem sempre irá variar

Antes de começar, é importante fazer uma ressalva: não importa o quanto você se aprofunde a tentar traduzir mecânicas e características de um cenário para o Fate, você ___NUNCA, JAMAIS___ vai alcançar 100% de precisão na adaptação.

Isso se deve pois cada mídia (e cada meio específico dentro de uma mídia) possui certas características específicas que não é replicável, não importa o quanto você dedique-se.

Um exemplo que podemos tomar no mercado brasileiro, dentro da mídia de RPG, é Tormenta: originalmente voltado para 3D&T, GURPS e AD&D, seu estilo foi se diferenciando até formar o Tormenta RPG que conhecementos, usando Sistema D20. 

Isso deu um determinado tom ao sistema, mais "sério". Entretanto, quando se usa o Tormenta Alpha (para 3D&T), ainda que o cenário "narrativo" seja o mesmo, tem-se a impressão que o cenário fica mais "galhofa", devido à característica mais animesca do 3D&T, com superpoderes avassaladores e coisas do gênero.

Outro exemplo clássico é quando vemos adaptações de livros para filmes e vice-versa: NUNCA você consegue fidelidade absoluta, seja por limitações técnicas (como tentar descrever cenas de batalhas em livros), seja por limitações autorais (Senhor dos Anéis sem Tom Bombadil).

Portanto, não se sinta acanhado se o seu jogo não refletir 100% do cenário base da adaptação: isso é tecnicamente impossível. Concentre-se em trazer seu próprio foco à adaptação. Como dito em _O Carteiro e o Poeta “A poesia não pertence àqueles que a escrevem, pertence àqueles que necessitam dela.”_

Dito isso, vamos começar a pensar.

## Narrativa primeiro, regras depois

Uma coisa importantíssima e que o Fate permite fazer bem fácil: foque primeiro na ___narrativa___ que você quer fazer.

Em um primeiro momento, leia a história e evite qualquer regra que o cenário tenha em sua origem. Isso vai ajudar você a procurar as coisas que são mais interessantes em termos narrativos e traduzir os mesmos em ___Aspectos___. Isso pode te ajudar a trazer as regras necessárias para fazer aqueles aspectos serem "mais" que representações narrativas.

Por exemplo, se pensarmos nos _Reinos de Moreania_, um dos Aspectos importantes do cenário são ___Os Herdeiros dos 12 Animais que se tornaram humanos___, e isso vai já nos indicar que vamos precisar de alguma regra que lide com ___Os Herdeiros Moreau___, que possuem algumas capacidades a mais que o ser humano. Como isso será feito, ficar para um segundo momento.

Outro exemplo é, ao pensar em _Perry Rhodan_, uma ficção científica clássica alemã, existe uma raça chamada de Halutenses. Gerados a partir de criaturas conhecidas como Bestas Feras, são inteligentes, muito fortes e podem comer basicamente qualquer coisa. Entretanto, de tempos em tempos, entram em uma fase que chamam de __*"Lavagem Forçada"*__, quando abandonam seu planeta Halut para satisfazerem sua curiosidade incessante.

Por meio dessa descrição rápida e bastante simplória de um personagem, temos a existência de duas possíveis raças, os ___Halutenses___ e as ___Bestas-Feras___. Os ___Halutenses___ são ___Inteligentes e Muito Fortes___, podem ___Comer Basicamente qualquer coisa___ e, de tempos em tempos, ___Fazem uma "Lavagem Forçada", saindo pelo Universo para satisfazer sua Curiosidade___.

Vários Aspectos, não é?

Então, focar na Narrativa te ajuda a "batelar" as ideias, encontrar os Aspectos mais promissores para o seu jogo e potencializar coisas que você poderá transformar em regras. 

Claro que vale a pena enfatizar que você NÃO DEVE realizar um _Kitchen Sink_ e tentar explodir TODAS as regras possíveis. Faça suas notas e vá explorando as coisas conforme você perceber que elas serão interessantes no seu jogo.

## Começando a moldar regras

Dito isso, a coisa começa a ficar mais interessante, pois agora precisamos moldar o cenário em termos de regras de Fate.

Para isso, vale a pena citarmos novamente o que dissemos anteriormente ao falarmos de Extras:

+ _Aspectos_ para definir coisas que mudam a história
+ _Perícias_ para novos contextos em ações
+ _Façanhas_ para realizar coisas extraordinárias
+ _Estresse_ para coisas consumíveis

Como posso visualizar isso em personagens ou situações?

Vamos mostrar alguns exemplos:

___Aspectos___ como parte das regras pode ser legal quando você precisa representar tipos específicos de personagem, se isso for necessário de maneira ampla. O exemplo clássico são em séries de Ficção Científica, onde você pode transformar as raças dos personagens em ___Aspectos___ que denotem e possam ser usados para invocar ou forçar situações. Por exemplo em Babylon V os Narn tendem a ser ___ritualisticos e guerreiros___, além de ___não gostarem muito dos Centauri___, que por sua vez são ___decadentes ainda que cultos___. Desse modo, tanto ser um Narn quanto ser um Centauri pode ser algo interessante para os Aspectos.

___Perícias___ são interessantes de várias formas, em especial quando você pensa em possibilidades de colocar isso como contraponto entre o uso como Perícia ou como Abordagem: muitas vezes, se todos os personagens puderem fazer a mesma coisa ou serem similares, usar Abordagens pode ser interessante ao deslocar o eixo da ação menos do _o que está sendo feito_ e sim em _como a coisa é feita_. Entretanto, talvez você queira usar Perícias para enfatizar especializações, fragmentando, modificando ou aumentando o nível de perícias, fazendo com que os times tenham os mais diversos focos. Apenas tome cuidado, nesse caso, de evitar uma super-especialização que acabe "capando" o personagem se ele sair de sua "zona de conforto". Além disso, existem várias outras formas de medir-se competência dos personagens, como falamos lá atrás ao falarmos de perícias.

___Façanhas___ são um dos pontos fortes em qualquer adaptação: como elas, como dissemos acima, representam _coisas extraordinárias_, elas sempre vão ser muito focadas aqui. Equipamentos, poderes, capacidades, tudo aquilo que é especial pode ser representado como Façanhas. E se combinarmos isso com o que vimos anteriormente sobre _Trilhas de Façanhas_ e coisas do gênero, certeza que teremos como adaptar até mesmo situações ao estilo _Dragon Ball Z_, com seus Níveis de Super Sayajin tendendo ao infinito: basta encadear eles de modo que um seja façanha do outro.

___Estresse e Consequências___ são um pouco mais complicados de valorizar: em geral, o sistema de Trilhas de Estresse Físico e Mental, mais as consequências funciona bem. Mas aqui existem algumas coisas que você pode fazer de diferente e que tornem seu jogo mais interessante:

+ _Crie uma nova Barra de Estresse_ de um tipo adequado para o jogo. Isso pode ser legal quando ou você tem um recurso que pode ser usado para energizar suas ações, seja uma Barra de _Chi_ para um _Street Fighter_ ou uma de _Momentum_ para um _Runner_ de _Mirror's Edge_, ou algo que pode representar recursos vitais que podem se degradar com o tempo, como a Sanidade em um jogo de Horror ou Recursos em um jogo Cyberpunk. Se você combinar isso com regras de _Esforço Extra_ e/ou de _Caixas de Uma Tensão_ (como mostradas no _Ferramentas de Sistemas_, página 49), você pode desenvolver toda uma gama de sistemas interessantes. Essa, inclusive, foi a forma que usamos para definir a questão do _Escopo_ do _Fae Guardians_, enfatizando que caso algum deles gastasse todo seu Escopo, ele sofreria uma _Sobrecarga_, dando lugar ao personagem de fábulas;
+ Use _Condições_, como mostrado no _Ferramentas de Sistemas_, página 12. A grande vantagem disso é tornar bem mais intuitivo determinar as Consequências das Ações dos Personagens. Isso fica ainda mais interessante quando você usa regras como _Sem Estresse_ (_Ferramentas de Sistema_, págian 49) ou um sistema de dano mais simples como o definido em _Uprising_ (uma caixa em caso de sucesso no ataque, duas em caso de sucesso com estilo), podem tornar o combate ainda mais brutal e rápido, sem falar que permite que você restrinja e impeça que o jogador marque Condições que não fazem o menor sentido em relação ao que aconteceu com ele;
+ Jogue sem Estresse ou sem Consequências: sem Consequências o jogo flui rapidamente, e pode ser interessante quando as coisas forem tudo-ou-nada, sem falar que pode ter o ar de _Barra de Dano_ que alguns jogadores mais _old-school_ podem apreciar, em esepcial quando combinado com _Esforço Extra_. Sem Estresse, cada ataque sofrido pode ter uma consequência terrível, já que não existe o Estresse como "colchão narrativo" para o personagem. Isso sem falar que pode representar muito bem cenários de perigo total
+ Não usar nem um nem outro: essa opção ficou muito visível no _Fate Horror Toolkit_, que em seu Capítulo 6, sobre _Survival Horror_, traz uma solução onde você pode escapar de danos marcando Consequências ou sacrificando NPCs com os quais o personagem tenha ligação. Se você pensar bem, por esses sistema você pode tornar tudo AINDA MAIS brutal: o personagem se sacrifica ou a outrem? Os personagens ficam MUITO frágeis, o que pode ser o que você quer

## Extras... Extras... Extras... _Badger, Badger, Badger, Badger, Badger_...

Esse é um ponto onde é muito fácil se perder a mão: quando você vai criar _Extras_ e _Fractais_.

Como dissemos anteriormente ao falarmos sobre Extras, evite fazer o _Kitchen Sink_: você não precisa tratar tudo como se fosse personagens. Existem situações interessantes para isso, e um excelente exemplo tanto de solução quanto de problema é _Mindjammer_, que traz fractais para Civilizações, Planetas, Espaçonaves, Culturas, etc, etc, etc. 

Isso pode ser uma solução porque ele já traz todos os subsistemas para naves e afins, mas pode se tornar um problema se você tentar usar vários desses subsistemas, pois você vai se pegar tendo que administrar muitos Aspectos, Perícias, Façanhas e assim por diante. 

Dito isso, o ideal é sempre manter as coisas mais simples possíveis, mas não se fazer de rogado quanto ao adotar Extras e Fractais.

E para isso, tanto o livro do Fate, em seu Capítulo 11, quanto os livros _Ferramentas de Sistema_ e demais _Toolkits_, possuem todo o tipo de Extras que podem se aplicar ao seu jogo.

Se, ainda assim, precisar criar algum Extra ou Fractal, procure manter as coisas mais simples de modo que tudo fique interessante.

Alguns Extras que vale a pena mencionar aqui, já que aparecem DEMAIS em muitas adaptações:

+ _Arma/Armadura (Fate Básico, 253):_ como falamos em muitos momentos, sempre mencionamos que Fate sofre do _"Efeito 3D&T"_, onde não importa, em termos de regra, que tipo de equipamento você está usando, em especial Armas ou Armaduras. Nesse subsistema, que o Fate Básico mostra na Página 253, funciona de maneira bem interessante: o nível de Arma se soma a todo ataque que use aquela Arma que "entre" (ou seja, que passe a Defesa do Personagem), enquanto Armadura reduz a tensão de estresse sofrido. Esse pode ser interessante de se avaliar não apenas para dano físico, mas para mental: uma Rezadeira pode ter, por exemplo, _Armadura: 2_ para resistir a tentações ou horrores, enquanto um Ricaço pode ter um _Ataque de Canibalização_ que funciona como _Arma: 2_ contra alvos do mesmo tipo de mercado que os personagens;
+ _Superpoderes (Fate Básico, 256):_ Esse subsistema é bem interessante pois ele cobre basicamente qualquer tipo de poder especial: superheróis, magia, paranormalidade, mutações... Não interessa. 
+ _Equipamentos Especiais (Fate Básico, 259):_ mostra como você pode criar todo tipo de equipamento especial, desde ___Espadas de Gel Cinético___ até ___A Millenium Falcon___, e te dá todo tipo de sugestão a seguir. Isso se combina legal com a parte de _Veículos e Organizações (página 263)_ 
+ _Magia (Fate Básico página 265 e Capítulo 8 do Ferramentas de Sistemas):_ esse é um que, no _Ferramentas de Sistemas_, é MUITO completo, com muitas variações, usando Façanhas, ou com Aspectos e Perícias, ou com outras combinações diversas.
+ _Raças (Ferramentas de Sistema, página 31):_ algumas possibilidades interessantes, envolvendo, por exemplo, uma nova perícia _Elfo_ que mostra o quão bom um personagem é enquanto Elfo, além de Aspectos e assim por diante;

E assim por diante... Tem muitos subsistemas em jogos Fate: uma dica séria é ler os jogos a série _Worlds of Adventure_, pois muitos deles trazem subsistemas bem legais. Apenas para citar rapidamente coisas legais dele:

+ _Magias e Escala_ em _Secrets of the Cats_
+ _Populações e Tensões_ em _Loose Threads_
+ _Talismãs_ em _Nest_
+ _Aptidões Psíquicas_ em _Psychedemia_
+ _Culturas Alienígenas_ em _Andromeda_
+ E _A Convergência_ em _SLIP_

E assim por diante.

E uma dica especial para tradução de regras de RPG: procure perceber que tipo de situação aquela regra procura tratar e considerar a mesma em termos de regras do Fate. Um exemplo é Alinhamento de D&D: ele pode ser um Aspecto que pode incluir uma barra de Tensão de Estresse ligado aos "pecados" (ao estilo que existe em Storyteller), ou ainda uma Tensão ao estilo de _Loose Threads_, entre outras opções.

## Qual a melhor opção de regra?

___Resposta rápida:___ A que funciona para o seu jogo

___Resposta correta:___ na prática, vai depender do estilo de jogo. Você vai precisar experimentar e verificar o que funciona para o estilo do jogo e para seu estilo específico de narrativa. Alguns Fractais e Extras e regras podem ser bem imersivas e bem atreladas ao jogo, como _A Convergência_ de _SLIP_, onde a PRÓPRIA CAMPANHA ataca os personagens (sim, é estranho desse tanto). Entretanto, tome muito cuidado para não saturar a sua adaptação com tantas regras que você possa acabar se pegando fazendo _accountability_ de Aspectos e Façanhas e Extras e poderes e perca o fluxo da narrativa.

Obviamente, você PODE sim querer um sistemas de regras BEM táticas para uma adaptação de Star Wars onde você vai ter a Corrida da Estrela da Morte, ou um sistema mega-tático de esgrima e duelos para seu jogo de Os Três Mosqueteiros. Isso de modo algum é errado. Apenas tome cuidado para que você não se pegue incluindo coisas que possam simplesmente inviabilizar seu jogo, tornando-o arrastado demais, ou ao mesmo tempo não dando a devida importância para o _crunch_ em uma adaptação de _Assassination Classroom_ que simplesmente não dê aos jogadores a chance de matar o Koro-Sensei.

Dito isso, o ponto fundamental de uma boa adaptação para Fate é o Equilíbrio: no Fate, tudo em termos de regra é (ou deveria ser) voltado para enfatizar a narrativa e as características proativas, dramáticas e de competência da pessoa. Se isso não for levado em consideração, você certamente vai sofrer com regras demais ou de menos, tendo algo que reduza a velocidade do sistema ou tire os detalhes táticos que podem adicionar à narrativa. Afinal de contas, algumas das melhores cenas do cinema e dos livros dependem de eventos com muitos detalhes táticos, como a Batalha de Pellenor, a Luta em Mustafar entre Obi-Wan e Anakin, a Batalha do Beruna ou mesmo evitar que a Desenholândia seja riscada do mapa.

## Conclusão

Vimos que, na prática, adaptar sistemas para Fate é uma arte quase tão complexa quanto para qualquer outro sistema, demandando conhecimendo do cenário e do Fate (além do de qualquer sistema do qual você está trazendo o cenário), além de nunca ficar 100% perfeita, já que existem detalhes que não podem ser "migrados" de uma mídia (ou meio dentro da mídia) de maneira perfeita. Também percebemos que o Fate oferece toda uma gama de ferramentas sobre como realizar uma boa adaptação, incluindo em seus materiais toda uma gama de regras que você pode incorporar ao seu jogo conforme a necessidade.

Por agora, ficamos por aqui...

Até a próxima, e role +4.
