---
title: "Domando o Fate - Parte 17 - Mais Gramático que Matemático"
subheadline: Uma Introdução ao Fate 
date: 2021-03-15 21:50:00 +0300
layout: post
categories:
 - RPG
tags:
 - Fate
 - Fate Core
 - Fate-Core
 - Artigos
 - Dungeon Geek
 - Introdução ao Fate
 - introducao-ao-fate
order: 20
header: no
---

> Publicado Originalmente no Site da [Dungeon Geek](https://www.dungeongeek21.com/domando-o-fate-mais-gramatico-que-matematico/)

Cada vez mais, me pego pensando em uma máxima que podemos chamar de Princípio do Mr. Mickey para o Fate:

> _"Fate é muito mais **gramático** que **matemático**."_
 
E isso me pegou ainda mais após ver a seguinte tirinha no Facebook (não sei se ela é oficial ou editada).

![Isso não tem nada a ver com Dragões! Isso é Matemática!](/assets/img/CalvinMath.png)

Podemos pensar que esse é uma questão séria que pode ser pensada sobre o RPG: sua origem remetia aos _wargames_, jogos fundamentalmente matemáticos, baseando-se na questão da distância, força e número das tropas, equipamentos aplicado, etc...

Entretanto, Fate é um jogo fundamentalmente _narrativista_, ou seja, sua preocupação principal não é nas estatísticas ou necessariamente "no jogo", mas na narrativa, na história a ser contada.

Pode parecer que a preocupação com o jogo seja algo importante, e o é, mas não como as pessoas imaginam.

O conceito que temos de jogo é um conceito baseado na ideia de vitória: um jogo só é bom se pudermos "vencer". Mesmo no RPG isso acontece: quantas milhares de histórias sobre combos e "bonecagem" não podem ser pescadas na Internet? 

Alguns vão alegar _"personagem morto não permite roleplay"_, e com certa verdade. Entretanto, não se pode negar como muitos _anti-patterns_, muitas coisas ruins, que acontecem nas mesas de RPG, em especial o fato de que certas regras são muito ignoradas (Atributos mentais baixos e alinhamentos sendo os principais exemplos). Isso é especialmente válido em jogadores experientes, pois eles sabem como _cheatar_ o jogo das _"maneiras corretas"_, como no caso do ___Teste o jogador, e não a ficha___, onde o jogador ignora sumariamente a ficha quando ela não lhe é conveniente (Carisma 8, alguém?), enquanto alguns jogadores não podem ignorar regras (como o mago com apenas 1d4 de vida), e que acabam tendo sua importância _cheatada_.

Não sei quanto a vocês, mas odeio quando isso acontece.

O Fate tem como principal vantagem, no meu ponto de vista, exatamente isso: _ser mais gramático que matemático_.

Isso torna impossível _"bonecagem"_ no Fate? Não, muito pelo contrário: a grande vantagem que o Fate ___ABRAÇA A BONECAGEM___ bem feita. Se você quer fazer um combo em Fate, maravilha: faça com que o combo funcione como _algo que **AGREGUE** à história_, não que _tolha a históra_. Algo que _ajude os outros jogadores a brilhar_, não como algo que vise _"vencer" algo/alguém_. 

Como isso acontece no Fate?

É o que veremos nesse artigo!

## Aspectos, Aspectos, Aspectos

A principal base aqui é entender que, em termos de regra do Fate são os ___Aspectos___. 

Vistos de uma maneira ampla, são os Aspectos que fazem a cola entre o jogo, a narrativa e a simulação da realidade.

Como ele faz isso.

1. Narrativamente, os Aspectos detalham verdades tão absolutas e fundamentais sobre um personagem ou situação que elas impactam nos outros fatores, podendo mudar resultados e mudando as opções disponíveis no jogo. Além disso, eles estabelecem ___fatos___ que são úteis para analisar o que poderá ou não ser feito
2. Simulacionistamente, os Aspectos definem parâmetros para como as ações do jogo irão ocorrer. Embora o sistema do Fate tragam as regras, de certa maneira serão os Aspectos que permitirão ou bloquearão opções de ações e suas resoluções por rolamentos
3. Em termos de jogo, os Aspectos estipulam quais são os tipos de ações que podem ou não fazer sentido, e quão difícil ou fácil é realizar uma ação, impedindo que um jogo de Fate vire um jogo de _Calvinball_

Parece complicado, não é? Mas vamos fazer uma experiência interessante: considere, por exemplo, _Uma Cilada Para Roger Rabbit_. Pensemos no momento em que Eddie Valiant vai para a ___Desenholândia, um lar dos desenhos___ (esse será o nosso Aspecto).

1. Narrativamente, ele trás para o jogo o fato da existência da Desenholândia, e ser onde normalmente os Desenhos moram. Também oferece parâmetros sobre o comportamento dos personagens do jogador e da cena, como o fato de todas aquelas coisas malucas de desenhos animados poderem acontecer aqui.
2. Simulacionistamente, permite ajudar sobre o que é possível realizar dentro daquele lugar. Por exemplo, um personagem pode comer uma banana de dinamite? Cair do 3825724813204° Andar do Grand Hotel direto na rua mas não virar uma panqueca (ou virar, vai saber) e morrer? Você é capaz de usar truques bobos de esquetes clássicas de desenho animado para contornar situações como o caso de uma perseguição?
3. Em termos de jogo, todos esses parâmetros já demonstrados podem ser ajustados conforme a necessidade. Por exemplo, um humano pode tentar fazer tudo o que foi citado anteriormente (Eddie fez tudo, exceto a banana de dinamite)? E um Desenho? Quão mais difícil para um humano fazer isso?

Os Aspectos fazem isso com uma vantagem: você ___NÃO PRECISA PRÉ-ESTIPULAR N-A-D-A!!!___     Ainda que seja recomendável e saudável tentar planejar algumas situações, os Aspectos podem não prever tudo. Mas as regras do Fate, sendo tão simples e com os Aspectos servindo de parâmetros ___CLAROS___ em tempo real de como a narrativa, a simulação da realidade e o jogo interagem entre si, ainda permitem que você não seja "pego com as calças na mão"!

### A Regra do _Fala Sério!_ - limitando o poder dos Aspectos

Isso era um tanto intrínseco antes, mas o _Fate Condensed_ trouxe uma "regra de dedo" para saber quando estão ocorrendo abusos que podem quebrar o jogo através dos Aspectos, a chamada _bogus rule_, que informalmente chamo de _Regra do "Fala Sério!"_: se ao explicar como um Aspecto vai afetar a cena a qualquer momento, você perceber que você ou os demais jogadores ou o narrador estão fazendo explicações muito estapafúrdias, isso é um sinal de que você está "esticando demais o chiclete" e fazendo coisas que aquele Aspecto não permitiriam, ou para os quais eles não se aplicam.

> ___Exemplo:___ Helen por algum motivo quer usar seu Aspecto de _Portadora da Fada Madrinha_ (lembra dos _Fae Guardian?_) para dizer que seus poderes mágicos são capazes de fazer uma maldição terrível contra uma alvo... O que... ___Fala Sério!___ Ela é uma _Fada Madrinha_, não a Malévola! Mesmo tendo poderes mágicos isso é impossível para ela fazer! E ainda que fosse possível, a dificuldade seria muito alta. Fadas Madrinhas são conhecidas por serem usuárias de magias mais voltadas para cura ou bençãos, não para maldições! Dentro do Aspecto de Fada Madrinha, isso não seria possível, então o Narrador pode negar completamente esse efeito e impedir Helen de agir assim!

### Aspectos como Permissões ou Restritores

Como dissemos anteriormente, Aspectos funcionam como _parametrizadores_ também: eles determinam o que pode ou não acontecer naquele momento narrativo ou com aquele personagem (se ele usar seus Pontos de Destino). Além disso, os Aspectos funcionam também como ___Permissões___ e ___Proibições___.

Um jogo que explora bem isso é _Wearing the Cape_: conforme o _Aspecto de Poder_ do personagem, são ativadas certas Permissões ou Restrições: por exemplo, se o seu personagem é um Atlas (o Super Homem do cenário), ele poderá voar e ter superpoderes (enfatizado pelas demais características do mesmo). Entretanto, existe o potencial de ele ferir pessoas graças à sua superforça, ou mesmo que pessoas que não gostam "desses mascarados" não vão com a sua cara e não queiram interagir com você.

> ___Exemplo:___ _Grilo Falante_, o nome heroico do jovem Joshua Clemens McCarthy, é um ___Herói Genial e Consciência Alheia___. Isso determina que seus poderes são relacionadas à sua capacidade mental extremamente elevada, sua capacidade de prever situações por análise dos acontecimentos e seu poder de convencer os outros a não fazerem "coisas ruins". Esses fatos são estabelecidos pelo Aspecto dele. Também é estipulado que ele pode ter "travadas mentais" quando muitas coisas ruins puderem acontecer ao mesmo tempo e que seu poder de convencimento, até pela característica dele de "consciência" não pode ser usado para induzir à violência.

### Aspectos enquanto tom de cenário

Dito isso, se você quer deixar claro uma situação específica em cena, como uma ___Multidão Hostil___, um ___Potencial Apocalipse___ ou ___Pradarias Calmas no meio do Caos do Mundo___ que os personagens deverão proteger, os Aspectos podem ser usados para isso: se você, por exemplo, criar uma ___Cidade muito movimentada___ onde uma luta entre super-heróis e vilões está rolando, você pode fazer os vilões atacarem ___A CIDADE___, não os heróis. Se os heróis protegerem as pessoas, os vilões poderão escapar, mas caso contrário, onde eles estavam quando a pequena Sally morreu sob os escombros do Ataque do _Herdeiro do Álamo_?

Um Aspecto de Cenário pode sempre ser uma boa forma de simular situações onde regras que normalmente não estão em jogo entram em jogo: na situação que falamos, podemos trazer uma regra de ___Dano Colateral___ onde quem sofre o dano que os personagens deveriam sofrer é, por exemplo, a reputação do grupo (___Sally Morreu pela inação dos Heróis!!!___) ou qualquer outra coisa adequada.

Lembra do que falei na Desenholândia? Você pode utilizar a Desenholândia como uma forma de permitir que personagens humanos consigam (ainda que com certa dificuldade), utilizar a mesma lógica maluca dos Desenhos Animados, se safando caso venham a ver um caminhão que tente passar por cima deles, os ___achatando igual panquecas___. Desse modo, você pode criar, habilitar e desabilitar facilmente regras e elementos do jogo conforme a necessidade, usando os Aspectos.

### A Arte de Escrever bons Aspectos.

Todo bom Aspecto, como vimos lá no primeiro artigo dessa série, tem que ser _ambíguo, descritivo_ e _dizer mais de uma coisa_.

Por exemplo: ___Forte___ não é um bom Aspecto, pois só diz que o personagem é Forte, não descreve muito sobre essa força e, exceto em situações extremamente óbvias, não pode ser usado "contra o personagem" para apimentar a narrativa. Entretanto, um ___Paladino Solarii___ é bom, pois indica que ele é forte, provavelmente pelo treinamento militar de paladino, que ele é de um grupo chamado Solarii (o que é esse grupo pode ser definido depois), e dá oportunidades de ser usado "contra" o personagem trazendo situações de dilemas morais ou inimigos de sua ordem ao jogo.

Perceba que "contra o personagem" não é exatamente ___contra o personagem___. Partimos das premissa que seu personagem aceita esses Aspectos como parte de tudo o que ele é, não algo que ele jogará fora na primeira oportunidade. Se você é um ___Espírito do Otimismo___ você o será para o bem e para o mal, sendo otimista mesmo quando toda a racionalidade disser que o desespero é a melhor opção. Se você para você ___O poder é um vício que o tira do caminho da paz___, você trará situações onde você irá exercer o poder para seus próprios fins egoístas e se sentirá mal por isso.

## Perícias - o que você faz de bom

Mecanicamente, as Perícias são muito importantes, já que eles representam _as competências_ do personagem, como dissemos [LÁ NO INÍCIO dessa Série](https://www.dungeongeek21.com/bg/iniciando-no-fate-pericias). Portanto, eles são cruciais para o sucesso ou fracasso do seu personagem.

Ser algo mais _gramático que matemático_ nesse caso ajuda ao jogador a ter noção tanto das habilidades do personagem dentro da perícia quanto o resultado final das ações e as dificuldades envolvidas.

Vamos primeiro lembrar que a Escala vai de -2 (Péssimo) a +8 (Lendário), como abaixo.

| ___Nome___ | ___Valor___ |
|-:|-|
| Péssimo | -2 |
| Ruim | -1 |
| Medíocre | +0 |
| Regular | +1 |
| Razoável | +2 |
| Bom | +3 |
| Ótimo | +4 |
| Excepcional | +5 |
| Fantástico | +6 |
| Épico | +7 |
| Lendário | +8 |

O importante aqui, é entender as ___palavras___, não os ___valores___. 

As palavras ajudam a ter uma ideia da competência geral do personagem naquela perícia, e o quão competente alguém precisa ser para, em condições ideias de temperatura e pressão, realizar a tarefa estipulada.

Além disso, por não serem valores abstratos, pode-se usar os mesmos termos dentro da escala do jogo. Então, um camundongo com _**Atletismo** Bom (+3)_ fará o mesmo que qualquer outro camundongo, mas não que um humano ou um Guerreiro Z de Dragon Ball.

A vantagem disso é que tudo que você fará terá uma dificuldade ou resultado mais fácil de ser determinado.

Por exemplo... Vamos pegar um humano com _**Atletismo**_:

+ Um personagem _Péssimo (-2)_ terá muitos problemas, se cansando rapidamente e não conseguindo realizar a maior parte das corridas: mesmo correr atrás do busão quando se atrasa será um problema.
+ Um personagem _Medíocre (+0)_ é uma pessoa comum, que às vezes perde o busão quando tá atrasado, mas que de vez em quando consegue pegar ele.
+ Um personagem _Razoável (+2)_ é alguém já melhor condicionado: se precisar correr para pegar o busão irá conseguir na maior parte das vezes, e pode até ser que já tenha participado em uma ou duas corridas urbanas.
+ Um personagem _Ótimo (+4)_ é alguém muito treinado, capaz de aguentar muitas provas com facilidades, e às vezes até consegue um resultado alto em uma ou outra prova
+ Um personagem _Fantástico (+6)_ é provavelmente um corredor profissional, ou ao menos um amador de grande talento, alguém que certamente já ganhou provas de alto nível, podendo até mesmo arriscar as olimpíadas.
+ Um personagem _Lendário (+8)_ é um medalhista olímpico, alguém conhecido por sua velocidade, o melhor ou um dos melhores. Vencer provas para você é algo do dia-a-dia, e mesmo nas olimpíadas você é favorito devido ao seu treinamento, equipamento e perícia.

Perceba que não digo que _Lendário (+8)_ é automaticamente o melhor no que faz. Primeiro porque temos as Façanhas para melhorar ___Ainda mais isso___ em certas especializações. Segundo, porque os dados ainda podem acontecer e colocar tudo a perder: imagina nosso Usain Bolt rolando um -4, isso poderia o levar a perder para um amador bem treinado em um dia de relativa sorte.

### Aproveite suas Vantagens - Os Aspectos em Ação!

E aqui percebemos a importância das Perícias embasarem os Aspectos e os Aspectos reforçarem as Perícias. 

Por exemplo: se nosso Usain Bolt _Lendário (+8)_ tiver ___Uma Vida dedicada ao Atletismo___, ele dificilmente se verá em uma situação onde pode ser derrotado por um amador _Ótimo (+4)_. Primeiro, que ele sempre pode invocar o Aspecto (se tiver Pontos de Destino) para +2 e garantir sua vitória, ou mesmo re-rolar se achar que isso não é o suficiente.

E não apenas isso: lembre-se que existem diversos Aspectos em jogo, e que você pode trazer novos Aspectos em jogo por meio da ação de ___Criar Vantagem___, dado que narrativamente você tenha tido tempo e sucesso em se preparar o suficiente.

> Como exemplo, vamos pegar _Golgo 13_, ou melhor, o Sr. Duke Togo, assassino profissional. Ele é conhecido por ser ___O Assassino Supremo___ e para ele ___Qualquer detalhe não verificado pode significar a derrota___. Ele precisa assassinar um governador quando ele está preste a dar o perdão a um agente duplo condenado à morte. Duke investigou o local e descobriu que as janelas possuem esquadrias muito espessas de um material ___À Prova de Balas Comuns___. Como isso ele procurou preparar uma ___única bala de um material especial___ capaz de passar pela esquadria. 
>
> Ele já tem um _**Atirar** Lendário (+8)_, o que seria o bastante para derrubar esse governador em situações normais. Mas ele é alguém que acredita em se preparar muito. Então ele Paga Dois Pontos de Destino para obter +4 no seu rolamento, +2 por ser ___O Assassino Supremo___ e +2 porque ___Qualquer detalhe não verificado pode significar a derrota___, então ele se preparou bem. Além disso, como o material ___À Prova de Balas Comuns___ e a ___única bala de um material especial___ vieram por Ações de Criar Vantagem, elas trouxeram invocações gratuitas, ou seja, usos que ele não precisou pagar com Pontos de Destino. No caso, ele pegou essas invocações gratuitas para somar outros +4. Com isso, ele já tinha um Lendário (+8) em ___Atirar___, e +4 dos seus Aspectos de Personagem para +12, e ainda levou para mais longe, com um +16!!!! Mesmo com um rolamento desastroso dele e um ótimo rolamento do governador na Defesa (imaginando que ele tenha), ainda assim é certo que o Governador irá morrer!

Quando as opções dos Aspectos e as ações de Criar Vantagem entram em jogo, se usadas corretamente fazem com que o lema não oficial da NSA se aplique: _"Tudo é possível, o impossível apenas leva mais tempo!"_

### Justificando suas ações (ou sobre como perícias diferentes podem justificar uma mesma ação)

Importante aqui notar algumas coisas importantes sobre as perícias em Fate:

1. Em Fate, se você tem uma perícia em sua lista de perícias, você não apenas tem a capacidade e conhecimento de exercer a perícia, mas também tem as _ferramentas_ para exercer a mesma. Embora isso não queira dizer que suas ferramentas são as melhores (veremos mais sobre isso mais abaixo), você não tem problemas de falta de ferramentas (exceto se Aspectos em jogo que digam em contrário estiverem em jogo)
2. Perícias em Fate são amplas, e abarcam vários tipos de conhecimento dentro da área de atuação da mesma. Então Atirar não quer dizer apenas saber usar armas, mas pode aplicar também conhecimento de táticas com armas, diferenças, pontos fracos, e até mesmo a avaliação e manutenção das mesmas!

O que isso quer dizer?

1. Dado que exista uma justificava razoável para que o personagem seja capaz de usar uma perícia, pode usar qualquer perícia, dependendo da narrativa. Porém;
2. A dificuldade o rolamento pode variar conforme as perícias que serão usadas.

Vamos a um exemplo usando os _Fae Guardians_:

> Bobby, Helen e Pericles tiveram um duro combate contra um dragão de pelúcia do mal (quem teve essa ideia? Quem deixou?), mas agora eles tem que lidar com a possibilidade de uma turba hostil a eles, já que todos abusaram dos seus Escopos e seus corpos assumiram a forma de seus Escopos.
> 
> Agora eles terão que tentar lidar com isso e acalmar essa turba. O Narrador questiona os jogadores como farão isso.
> 
> + Pericles, graças a seu retrospecto policial, decide partir para a carteirada: _"OK, pessoal, o show acabou! Todo mundo pra casa!"_
> + Robert, mais capaz de negociação, decide tentar convencer meio que tentando trapacear: _"Gente, é só um show, tá?"_
> + Já Helen, com sua capacidade de lidar com o público, prefere usar a ideia de Robert para convencer as pessoas de que tudo era um espetáculo: _"Estamos apenas ensaiando um grande espetáculo para as crianças de um orfanato local!"_
> 
> Primeiramente, essa é uma ação de Superar, onde eles tentam remover o Aspecto da ___Turba Hostil___, pois essa turba podem acabar avançando contra os Fae Guardians (estimulados por uma força hostil? Quem sabe?)
> 
> Agora, o narrador define quais serão as perícias a serem usadas para esse teste. Ele percebe que os três tentam de maneiras diferentes: Pericles vai usar ___Provocar___, Robert ___Enganar___ e Helen ___Comunicação___.
> 
> As dificuldades serão diferentes, entretanto, já que as turbas estão nervosas o bastante para não admitirem a carteirada de Pericles (dificuldade ___Bom (+3)___), mas com certeza Robert e Helen terão melhores chances (dificuldade ___Razoável (+2)___)

Desse modo, ainda que seja difícil, pode-se ter chances com perícias inapropriadas. Por exemplo: reparar uma arma poderia ser por Conhecimentos (envolve saber como a arma funciona), Ofícios (envolve saber usar ferramentas e detectar pontos quebrados) e ___ATIRAR___ (afinal de contas, ao menos em teoria você deveria saber como dar manutenção em armas). Atirar também poderia ser usada, por exemplo, para encontrar negociadores de armas (claro, com dificuldade do que por Contatos). 

Por isso, procure, como jogador ou Narrador, como tornar mais rico e interessante os usos de perícias. Lembrando sempre que a perícia não inclui apenas as habilidade da mesma, mas ___os recursos envolvidos no uso da mesma___ como equipamento, perícia, diagnósticos...

> ___Exemplo:___ Bobby está tentando levar todos para invadir um depósito onde eles ouviram rumores está depositado uma série de itens contrabandeados que, eles acreditam, vieram de um outro plano, provavelmente a mando de ou coletados por _Morgause_. Enquanto eles vão se preparando, Helen pega uma corda que ela tinha no seu Flat no Edifício Dakota para ajudar. Entretanto, Bobby suspeita que a corda não seja forte o bastante. Normalmente para saber se uma corda é forte o bastante, o rolamento mais provável seria _Conhecimentos_, mas _Vigor_ e _Roubo_ também são opções, _Vigor_ por ser literalmente testar se a corda estoura quando tem força aplicada nela, e _Roubo_ por cordas serem uma típica ferramenta de um ladrão. Após um rolamento ___Bom (+3)___, o Narrador decide que Robert percebe que a corda não é das melhores, mas que dá para reforçar de maneira improvisada com nós e trançados.
> 
> _"Helen, da próxima, tira o escorpião do bolso, por favor. Vamos gastar um tempão agora!"_ diz Robert, um pouco frustrado, enquanto improvisam rapidamente reforços na corda que trouxeram.

### Narre, não peça! (ou como uma descrição esperta pode salvar você)

Dito isso, uma coisa importante: se você quiser fazer o velho truque de tentar forçar os rolamentos a serem sempre para sua melhor perícia, ___JUSTIFIQUE___. Use de maneira Inteligente suas descrições para convencer o narrador que os resultados serão mais interessantes se você utilizar sua melhor perícia.

Isso não é errado: o Fate é um jogo que se foca em um estilo mais _cinematográfico_, onde você pode (e deve) tornar suas descrições mais interessantes. Boas descrições irão favorecer você!

> ___Exemplo ruim:___ Pericles precisa consertar sua arma que está ___Emperrada___. Então ele diz ao narrador _"Quero consertar minha arma!"_ Desse modo, o rolamento seria por ___Ofícios___, que Pericles tem _Medíocre (+0)_. Além de ser uma descrição CHATA, ela não ajuda Pericles.
> 
> ___Exemplo bom:___ Pericles precisa consertar sua arma que está ___Emperrada___. Então ele diz ao narrador _"Bem... A grande sorte é que na Polícia, quando te ensinam a usar uma arma, te ensinam a USAR uma arma, e isso inclui realizar a manutenção da mesma!"_ Essa descrição torna mais interessante as coisas a trazer um pouco de descrição e narrativa. Nesse caso, o Narrador percebe que a descrição justifica usar ___Atirar___ ao invés de ___Ofícios___ para realizar a manutenção da arma. Afinal de contas, como dissemos anteriormente, as Perícias oferecem não apenas a capacidade de realizar as ações, mas as ___ferramentas___ para o fazer.

### Lembre das consequências das ações

Uma coisa importante: rolamentos falhos não implicam em falha. Existem coisas ___BEM MAIS INTERESSANTES___, em especial em caso dessas _"trocas de perícias"_.

Pense que o resultado da falha pode ser tanto não alcançar o que você deseja quanto _"alcançar, mas a um grande custo"_! Aqui, coloque sua máscara BDSM (apenas para adultos, em quatro paredes e sendo consensual!) e imagine as possibilidades. 

Em 2018, no blog da Evil Hat ([traduzido por este que vos fala](http://fabiocosta0305.gitlab.io/rpg/Riscos/)) ele sugere a ideia de analisar os riscos envolvidos no rolamento e, baseando-se nisso, decidir potenciais riscos em caso de Falha. Isso pode ajudar a decidir custos interessantes que permitam a história se mover de maneira divertida e fluída.

> ___Exemplo:___ O narrador decide que Pericles pode sim utilizar ___Atirar___ no lugar de ___Ofícios___ para reparar sua pistola. Entretanto, ele também resolve pensar nos _Riscos_ envolvidos.
> 
> Pensando na lista do Artigo apresentado _(Custo, Dano, Revelação, Confusão, Desperdício, Ineficácia, Efeito Colateral, Atraso)_, o narrador pensa que Custos, Revelação, Dano, Efeito Colateral ou Atraso são boas opções para essa situação:
> 
> + ___Custo:___ Robert pode ter que comprar umas ___peças no mercado paralelo___ e isso depois isso se voltar
> + ___Dano:___ O cansaço e estresse de desemperrar arma faz com que Pericles fique ___Exausto___
> + ___Revelação:___ entre as peças que ele substitui pode estar algo com o ___número da sua arma___ o que vai dar pistas para adversário o seguir
> + ___Desperdício e Atraso:___ Robert teria que ficar de fora da próxima cena, reparando com todo cuidado sua arma (e vai ser nessa cena que Morgause vai pegar os _Fae Guardians_ em separado!)
> + ___Efeito Colateral:___ A arma está reparada, mas com uma chance que, em caso de falha durante um ataque, ela ___Exploda___ na mão de Robert
> + ___Ineficácia:___ usando peças de segunda mão, a arma tem problemas na ___Potência___ da mesma, o que pode reduzir a força do ataque

Isso é legal: embora você possa, desde que narrativamente explicado, utilizar outras perícias no lugar de uma específica, pense que ___consequências___ podem ser interessantes de trazer, ou mesmo nenhuma se tudo for explicado corretamente. Lembre-se: em Fate, você só rola os dados se ___sucesso e falha forem interessantes na narrativa___. Se a falha não permitir nada interessante, apenas aceite o sucesso, ou peça um Ponto de Destino. Não há necessidade de complexidade excessiva.

## Façanhas: As coisas em que você é foda!

Façanhas são basicamente a forma que o Fate te provê de mostrar aquelas coisas que você é fantástico ou que você tem de fantástico.

Pode parecer que isso também tem a ver com Aspectos, e de certa forma até tem (veremos abaixo). Entretanto, os custos são menores (em geral não precisa pagar Pontos de Destino e Façanhas não podem ser usadas mecanicamente "contra" você), mas os usos também (você tem normalmente restrição na perícia, ação e/ou situações nas quais se aplicam os efeitos).

Muitas vezes, também, as Façanhas em termos narrativos são usados para explicar coisas especiais, como um treinamento secreto, uma arma especial, uma capacidade específica de seu povo/raça/espécie/clã/ancestralidade/preencha-aqui-a-forma-de-distinção. Em alguns casos, isso será uma permissão que você precisará para ter acesso a isso, [como vimos quando falamos de Extras e do Fractal do Fate](https://www.dungeongeek21.com/iniciando-no-fate-extras-e-o-fractal-do-fate/).

Um exemplo é relacionado a _Wearing the Cape_, onde os poderes são relacionados a Façanhas associadas a cada Tipo de Poder (um Aspecto que você compra )

> ___Exemplo:___ Andrew "Aubergine" Lamarca é o ___Portador do Coelho Branco___. Devido a isso, ele possui a capacidade de _Sincronicidade_, ou seja, tudo acontece para que ele esteja em um local ou ocorra um evento a ele favorável, mas só quando ele estiver sob pressão em relação ao tempo (🎵 _É tarde, é tarde, é tarde!_ 🎵). Essa Façanha então seria descrita assim
> >___Sincronicidade:___ quando sob pressão relacionada a tempo (por exemplo, se atrasando para chegar a um lugar), Andrew recebe +2 nos rolamentos de Superar relacionados.

Se você parar para pensar, Façanhas podem ser entendidas como formas consolidar certas habilidades especiais. Inclusive, como dissemos ao mencionar Façanhas, mostramos [como um personagem pode ser muito mais forte em uma situação específica que um "generalista" padrão do Fate](http://fabiocosta0305.gitlab.io/rpg/DGFate3-Facanhas/#as-façanhas-e-a-perícia-pico-ou-por-que-você-não-precisa-ter-atirar-como-pico-se-você-quer-ser-um-sniper).

Vamos mencionar agora como ser mais gramático que matemático ajuda nas Façanhas.

### Qual equipamento eu tenho?

Como dissemos acima, temos que lembrar que em Fate, as perícias na pirâmide implicam que você não apenas tem a habilidade necessária, mas _as ferramentas_ para a exercer. Diferentemente de outros jogos, você não precisa explicitamente comprar uma Espada se seu Lutar estiver na Pirâmide. Entretanto, a sua Espada, ou Lança, ou Sabre de Energia, não farão diferença real no combate, como falamos em nosso primeiro _Oh! Toodles!_ ao mencionar o [_Efeito 3&DT_](http://fabiocosta0305.gitlab.io/rpg/OhToodles1-ArmasArmaduras/#o-efeito-3dt).

Algumas vezes, entretanto, você vai querer equipamento mais poderoso. Aqui a ideia é que você tente descrever equipamento de uma maneira que enriqueça a narrativa. Conseguindo fazer isso de maneira legal, o benefício mecânico ficará óbvio. Você não estará cortando o inimigo com uma espada qualquer, mas sim com a _Excalibur_ ou com a _Espada Olímpica_. Atirar com qualquer arma é legal, mas usar uma _Gunblade_ ou _O Velho 38 de serviço do meu pai_ é bem legal!

Equipamentos serem Façanhas é bem legal, pois isso ajuda a dar o senso mais "tradicional" das armas como algo que aumenta a força do personagem, mas ao mesmo tempo permitindo que você adicione novas capacidades.

### Nomes genéricos _vs_ nome específico

Muitas vezes, ao ler manuais, você perceberá que nomes de Façanhas tendem a parecer "genéricos" _(Golpe Matador, Escudo da Razão, Já li sobre isso...)_. Aqui fica claro que isso é porque manuais tem que ser referências boas, de modo que você possa a usar em qualquer cenário.

Mas uma boa forma de dar um tempero é reescrever o nome da Façanha para algo mais dentro do cenário, pois isso ajuda a trazer as coisas para dentro do estilo.

> ___Exemplo:___ _Golpe Matador_ pode ser, por exemplo, _Técnica Especial: Jutsu do Vazio_ em um jogo de Ninjas, _Força do Touro_ em um estilo medieval ou simplesmente _Chutando o Saco_ em um jogo de Gangsteres!

Isso pode ser uma boa forma também de tentar traduzir uma Façanha de uma outra perícia para algo que você queira usar em uma perícia na qual você seja melhor. Também pode ser interessante para realizar ajustes temáticos conforme a perícia.

> ___Exemplo:___ Helen possui muito dinheiro devido aos seus _Recursos_ altos, então ela decide que pode ser legal ter uma gama de _Peritos Confiáveis_ em sua agenda para a ajudar nos momentos certos. Para isso, ela opta por comprar uma Façanha com esse nome, mas similar a _Já li sobre isso_. Ela quer também não depender de pagar Pontos de Destino. O Narrador propõe uma mecânica de rolamento em _Recursos_ que, em caso de Falha, implicará que Helen estará ___Em Débito com___ o perito. Além disso, Helen só pode recorrer a isso uma vez por sessão.
> 
> Então, o que Helen teria seria algo como abaixo:
>
> + ___Perito Confiável:___ Uma vez por sessão, Helen pode trazer à cena um ___Perito em___ alguma coisa relacionada a uma outra perícia, e usar o resultado do rolamento no lugar dos testes da mesma. Para isso, ela deve rolar _Recursos_ contra uma dificuldade ___Razoável (+2)___, como uma Ação de Criar Vantagem. O valor para os testes é igual ao rolamento desse teste ou a dificuldade dele, o que for maior. Em caso de Falha, o Perito agirá por Helen, mas ela estará ___Em Débito___ com ele (como um Aspecto nela). Dê um nome e um Conceito, o Narrador colocará uma Dificuldade.

## Estresse e Consequências - Apanhando de Maneira Narrativa

Fate é um jogo tão focado na Narrativa que o dano é apresentado em especial na forma de _Estresse_ (a _plot armor_ do personagem, os pequenos ralados, contusões e sustos) e _Consequências_ (ou _Condições_, em alguns casos. 

No caso, vamos focar nas _Consequência_ e nas _Condições_ (as últimas na prática sendo apenas Consequências prenomeadas). Consequências são Aspectos que aparecem em jogo como resultado de ferimentos, traumas mentais, gafes sociais, ações financeiramente equivocadas, entre outras formas de prejuízos sofridos pelo personagem em Conflitos.

Muitas vezes isso parece ruim: afinal de contas, na ação de Ataque, quando você sofre uma consequência, ela entra como um Aspecto. E pior: com uma invocação gratuita para quem provocou essa Consequência nele. 

Entretanto...

...uma Consequência é um Aspecto, e portanto pode ser Invocado a seu favor também.

Duvida?

É porque você não conhece [_Kannin Dragon_](https://www.youtube.com/watch?v=yZS5FGKjU1Q), o único ninja que realmente sentou o braço no Jiraya.

E ele estava ___Bêbado___ (Consequência Suave).

Lembre-se que Consequências tem seu nomeamento negociado entre as partes. Normalmente será algo "negativo" de imediato (lembre-se que aqui estamos falando de um Aspecto, portanto não existe _Negativo_ ou _Positivo_). Mas um jogador criativo pode usar bem (na verdade, ___Deveria___) seus Aspectos e descrições que o ajudem. 

Em especial, após um Conflito, você deve _renomear a Consequência_ sofrida para que a recuperação comece a ocorrer. Isso é feito com testes específicos ou com a passagem de tempo, conforme o jogo.

> Helen sofreu muito no último duelo mágico com _Lilandre_, uma das lugares-tenentes de Morgause: ela utilizou uma maldição mágica que fez com que ela tivesse sua ___Juventude Roubada___. Após algum tempo de descanso, Helen renomeou essa Consequência para ___A Face de uma Bondosa Velhinha___ indicando que ela já não tem mais a dor do sofrimento da juventude que lhe foi roubada.
> 
> Enquanto os _Fae Guardian_ estão caminhando de volta para casa, uma gangue de delinquentes vai tentar alguma gracinha com eles. Ela então declara: _"vou olhar bem nos olhos deles, com o sorriso mais doce que eu puder, mostrando as bochechas coradas da_ **Face de uma Bondosa Velhinha** _e dizer 'Meus jovens, o que vocês pretendem? Não acham que um grupo de jovens como vocês atacar uma pobre velhinha e seus acompanhantes não é um pouco demais? Que vergonha, puxa vida...'"_ Essa descrição, usando a Consequência que ela sofreu permite que ela role sua _Comunicação_ _Bom (+3)_ como um _Ataque_ contra a ___Gangue de Arruaceiros___ _Regular (+1)_. Ela consegue um bom `+-++`{: .fate_font} (para um final _Excepcional (+5)_). Eles conseguem apenas manter-se em _Regular (+1)_ com um `-++-`{: .fate_font}. Desse modo, eles acabam sofrendo dano o bastante para os derrotar, fazendo eles se voltarem morrendo de vergonha de não conseguir atacar uma vovozinha parecendo a Fada Madrinha da Cinderela.
> 
> _"Imagina se eles soubessem que você tem apenas 42 anos."_ diz Pericles, rindo quando eles viram a esquina.
>
> _"Nunca se pergunta ou revela a idade de uma dama, sabia?"_ diz Helen, dando um tapinha de leve no ombro de Pericles.

### Estresse, Consequência e Sucesso a Custo: Esforço Extra e Esforço Extremo

Uma regra interessante que foi apresentada no Fate Ferramenta de Sistemas (e tornada mais válida no Fate Condensed) envolve o conceito de _Esforço Extra_ e _Esforço Extremo_. Basicamente ela diz que, em caso de Falha, o Mestre pode autorizar que você "compre" o sucesso sofrendo Estresse e Consequências.

Como isso pode funcionar? A qualquer momento que você precise (sob autorização do Narrador), marque uma Caixa de Estresse, ou anote uma Consequência em um _slot_ vazio, e receba o valor daquela Caixa ou _Slot_ marcado como um bônus para o seu rolamento. 

Isso viola um tanto a Economia de Pontos de Destino, mas pode ser dramaticamente interessante se bem feito. Se o seu Narrador adotar essa regra, tenha certeza de descrever bem o resultado dos rolamentos em conjunto com o Narrador, para quem sabe ele te autorizar a obter um Sucesso por meio do Esforço Extra.

> Hoje não é o dia dos _Fae Guardians_: alguém os mandou para a Floresta das Trevas, onde Pericles obteve seu Escopo após provações e dores terríveis. E eles conseguiram chegar em casa de tijolos no meio da mesma. Entretanto, o rolamento de Pericles não foi nada bom: um _Medíocre (+0)_ contra o rolamento _Bom (+3)_ dos Lobisomens que querem os devorar. O jogador de Pericles então diz: _"Pericles começa a fazer a maior força possível sem recorrer ao Escopo, gritando: 'Não dessa vez, seus desgraçados!', empurrando quase ensandecido a porta pela qual dá para se ver o focinho babando dos lobisomens loucos por carne fresca"_ 
> 
> O Narrador decide que parece uma boa hora para dar uma chance ao ___Esforço Extremo___ de Pericles (ao menos aparenta ser o que ele está pedindo) e descreve. _"Bem, Pericles... Você faz uma força tremenda, o que te impede de perceber que seu braço está em uma posição totalmente ruim para empurrar uma porta. Você só se dá conta do **Braço Deslocado** quando a  porta se fecha, e seu braço desencaixa da clavícula, com um dolorosa e familiar sensação."_
> 
> Pericles senta-se atrás da porta, enquanto Bobby procura acender a lareira para gerar luz e calor que assustem os lobisomens, e Helen corre para os primeiros socorros em Pericles. _"Já não sou mais tão novo para fazer dessas."_ diz Pericles, lacônico, com a mão boa no braço deslocado.

Isso permite um recurso narrativo extra: troque um dano ao personagem com uma boa descrição narrativa por um sucesso em algo que normalmente não teria.

### Consequências extremas - quando tudo muda

Consequências Extremas não são mencionadas na lista de _slots_, e existe um motivo: são formas de dano tão sérias que modificam um dos Aspectos do personagem, removendo-o e substituindo pelo que aconteceu ao mesmo. Ela absorve 8 pontos de Estresse, mas só pode ser pega uma vez a cada Marco Maior, e substitui algum dos Aspectos do personagem à exceção do Conceito, não podendo ser renomeado até o Marco Maior SEGUINTE!

Mas às vezes, pode ser interessante para momentos onde o drama está no espaço e as apostas são grandes, onde sacrifícios realmente pesados precisam ser feitos. Pense nisso combinado com o Esforço Extremo.

> Morgause derrubou Pericles e Bobby, que estão à beira da morte. Helen também está muito desgastada, mas ela sabe que ela não pode parar agora, a não ser que ela queira que Nova Iorque vire uma Floresta Sinistra, com todo tipo de criaturas terríveis perseguindo as pessoas como já estão fazendo no Central Park. Ela olha para sua varinha e decide que é hora de ir no _All In!_
> 
> _"Morgause, você fez a única coisa que não deveria: deixar uma Fada Madrinha realmente brava!_ Para a Escuridão Expulsar, minha aparência e juventude quero sacrificar! ___BIBBITY-BOBBITY-BOO!!!___" , fazendo uma série de gestos místicos com a varinha
> 
> _"Peraí. Deixa eu ver se entendi."_ diz o narrador _"Helen vai jogar fora algum dos Aspectos dela e se tornar uma_ __Velha Fada Madrinha__? _Agora é pra valer?"_
> 
> _"Bem... É isso ou deixar essa zinha vencer."_ diz a jogadora de Helen _"E estou pela tampa com ela. Helen sabe o que está em jogo."_
> 
> _"Ok... Me parece interessantemente dramático. Mas isso vai ser uma Consequência Extrema. O que você acha de colocar a_ __Velha Fada Madrinha__ _no lugar do_ __Pobre Menina Rica__?"
> 
> _"Era exatamente o que estava pensando... Com um pouco de fé, confiança... E bons dados... Vou expulsar Morgause."_ diz Helen.
> 
> Com seu nível ___Razoável (+2)___ em _Fábulas_, mesmo o `++++`{: .fate_font} Inacreditável que ela rolou não iria ferir misticamente Morgause, que possui _Lendário (+8)_ na perícia, mesmo considerando o fato que Morgause conseguiu apenas `0000`{: .fate_font}. 
> 
> Entretanto ela quer garantir que vai mandar Morgause pro espaço, e ele tem outras formas para garantir...
> 
> Morgause também está ___Exaurida Misticamente___ para se proteger dos diversos ataques de Pericles (uma Consequência que Pericles Aplicou previamente), além de estar ___Ensandecida___ com as Provocações de Robert (outra Consequência). 
> 
> E Helen decide usar tudo isso:
> 
> _"Morgause vai abrir a guarda ao perceber o que estou fazendo: ela sabe que se eu conseguir essa magia, ela está Morta, bem Morta. Eu quero aproveitar que ela está **Ensandecida** e **Exaurida Misticamente**, e somar isso com o meu Esforço Extremo para +12... Acho que com o +6 do Esforço que eu tive vai para +18, o que passa em 10 o Esforço dela."_ diz a jogadora de Helen.
> 
> _"Meio_ Overkill, _Mas efetivo: quando Morgause percebe, tarde demais, o que você quer fazer, ela corre que nem uma possessa contra você, soltando algum tipo de imprecação. Pericles e Robert vê uma poderosa aura mágica brilhante sair do seu corpo e engolfar Morgause, que se desmancha como se fosse borralho de lareira. Quando vocês todos acordam, vocês veem que Helen agora parece, mais do que nunca, a Fada Madrinha da qual carrega o Escopo, uma senhorinha de bochechas gordas e coradas com um robe azulado com forro em rosa. Ela parece bem exausta."_
> 
> _"Caramba!"_ diz Robert _"Espero que essa seja o fim!"_
> 
> _"Sem dúvida."_ confirma Pericles _"Parece que Helen no fim das contas pagou o preço para proteger a nossa realidade... Pena que poucos vão saber."_
>
> O sol se ergue, tocando a pele de Helen, revelando algumas rugas abaixo dos olhos que se abrem.

## Conclusão - Mais Gramático que Matemático

Fate tem uma grande vantagem em relação a outros RPGs: em qualquer situação bem descrita, a descrição permite que você se beneficie mecanicamente da mesma e que os resultados mecânicos da mesma sejam interessantes na narrativa.

Não há, em Fate, "ação perdida". Usando bem suas ferramentas, você sempre será útil, sempre poderá fazer algo e sempre conseguirá o que deseja. 

É só propor a narrativa e correr os riscos certos.

Até mais e lembrem-se...

...rolem +4!

<!--  LocalWords:  title subheadline post categories Fate-Core order  -->
<!--  LocalWords:  introducao-ao-fate header Mr Mickey Facebook Roger -->
<!--  LocalWords:  wargames anti-patterns cheatar cheatada históra OK -->
<!--  LocalWords:  Simulacionistamente Calvinball Rabbit Eddie Grand -->
<!--  LocalWords:  Valiant Desenholândia PRÉ-ESTIPULAR N-A-D-A Fae Ok -->
<!--  LocalWords:  Guardian Wearing the Joshua Clemens McCarthy Sally -->
<!--  LocalWords:  Solarii Dragon Ball busão dia-a-dia Usain Bolt NSA -->
<!--  LocalWords:  re-rolar Golgo Duke Guardians Bobby Pericles Flat -->
<!--  LocalWords:  Robert Morgause Dakota BDSM Evil Hat Andrew Jutsu -->
<!--  LocalWords:  Aubergine Lamarca plot armor Kannin Jiraya slot -->
<!--  LocalWords:  nomeamento Lilandre Condensed Slot slots Park All -->
<!--  LocalWords:  BIBBITY-BOBBITY-BOO Peraí zinha RPGs -->
