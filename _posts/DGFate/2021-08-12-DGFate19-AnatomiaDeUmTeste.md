---
title: "Domando o Fate - Parte 19 - Anatomia de um Teste"
subheadline: Uma Introdução ao Fate 
date: 2021-08-12 12:10:00 +0300
layout: post
categories:
 - RPG
tags:
 - Fate
 - Fate Core
 - Fate-Core
 - Artigos
 - Dungeon Geek
 - Introdução ao Fate
 - Domando o Fate
 - Iniciando no Fate
 - Ações
 - Teste
 - Rolamento
 - introducao-ao-fate
order: 22
header: no
---

> Publicado Originalmente no Site da [Dungeon Geek](https://www.dungeongeek21.com/domando-o-fate-a-anatomia-de-um-teste/)

Parece estranho falar do Teste em Fate: como em muitos sistemas, a base do rolamento de dados do Fate é simples. Na prática, o teste no Fate é _roll-over_, ou seja, o seu objetivo é, ao somar a Perícia com o rolamento, obter um resultado superior à dificuldade (ou rolamento) oposto.

Entretanto, devido às próprias idiossincrasias do sistema, um rolamento em Fate é um tantinho mais complexo que mostrado. Aqui, portanto, vamos tentar oferecer uma espécie de anatomia dos acontecimentos que ocorrem durante o rolamento.

> + ___A Anatomia do Rolamento em Fate - versão de 30 segundos___
> 
> 1. Determina-se a necessidade do rolamento, o tipo de ação, sua dificuldade e perícia envolvida
> 2. Invoca-se Aspectos necessários à ação
> 3. Efetua-se o rolamento
> 4. Aplica-se modificadores baseados em Façanhas que sejam aplicáveis (se desejado)
> 5. Avalia-se esse primeiro resultado
> 6. Invoca-se Aspectos para melhorar-se os resultados (de todos os lados envolvidos)
> 7. Repete-se (5) e (6) até que:
>    1. Nenhum lado tenha mais Aspectos que possam ser usados
>    2. Todos os envolvidos estejam satisfeitos com o resultado final
> 8. Encerra-se o teste com o resultado final aplicado à história

Na prática, isso é muito mais intuitivo do que aparenta: esse processo é facilmente aplicável no dia a dia. Entretanto, fazemos essa estrutura necessária para comentarmos elementos importantes de mecânica em cada um deles, de modo a demonstrar a importância do uso de cada um desses elementos para chegar ao resultado desejado por ambos os lados.

Além disso, aqui não tratamos de situações envolvendo rolamentos complexos, como Disputas, Desafios e Conflitos, mantendo-nos nos rolamentos simples. Isso devido ao fato que tudo que dizemos aqui também se aplica a tais situações.

Dito isso, vamos começar a "destrinchar" um rolamento de Fate.

## 1. Determina-se a necessidade do rolamento, o tipo de ação, sua dificuldade e perícia envolvida

Para começar, determina-se se há a necessidade de um rolamento:

Em Fate, se prevê que você [_**"Role os dados quando o sucesso ou falha de determinada ação possa contribuir com algo interessante no jogo."**_](http://fatesrdbrasil.gitlab.io/fate-srd-brasil/fate-basico/durante-o-jogo/#quando-rolar-os-dados) (___Fate Básico___, página 175).

Isso parece bem simples, mas na prática não é tanto: algumas vezes, um sucesso em determinada ação pode não trazer nenhum movimento adiante e vice-versa. Se uma situação como essa ocorrer, apenas diga que o personagem é bem-sucedido ou peça 1 Ponto de Destino (imaginando que ele tenha Aspectos adequados) para que aquilo aconteça.

Dito isso, é necessário então determinar o _tipo_ de ação. Lembre-se que em Fate quase todas as suas ações podem ser resolvidas por meio das _Quatro Ações_ de _Superar, Criar Vantagem, Ataque_ ou _Defesa_. Em um rolamento simples típico, muito provavelmente será uma ação de _Superar_ ou _Criar Vantagem_, com qualquer oposição ativa tendo uma ação de Defesa a seu favor.

Agora que mencionamos isso, é parte desse primeiro momento a determinação de qual _Perícia_ será usada e a _Dificuldade_ da mesma. Lembre-se que quando mencionamos aqui _Perícia_, na prática colocamos uma _Competência_ a ser testada, o que pode ser uma _Abordagem_ do Fate Acelerado, o nível de um _Aspecto_ de Boa Vizinhança, uma _Profissão_ de Jadepunk, e assim por diante. Além disso, certas Façanhas podem permitir que, em certas circunstâncias os personagems possam efetuar rolamentos de alguma perícia usando outras em situações especiais.

E a dificuldade a ser determinada pode ser baseada em um rolamento oposto de um alvo ativo, ou nas circunstâncias específicas em caso de uma situação onde não existe uma resistência ativa. 

O Fate Básico [traz toda uma guia para se seguir para definir-se dificuldades](http://fatesrdbrasil.gitlab.io/fate-srd-brasil/fate-basico/durante-o-jogo/#o-nível-de-dificuldade). Em especial, fica as seguintes considerações:

1. O "CD 10" de Fate pode ser definido em _**Razoável (+2)**_: personagens treinados em suas perícias normalmente passarão em seus testes nessa dificuldade, talvez precisando usar PDs ou tendo sucessos com estilo quando estão muito acima.
2. Fate é _pouco granular_, portanto lembre-se que, ao definir uma dificuldade, se ela estiver em um nível equivalente à perícia do personagem+2, é bem provável que ele possa Falhar ou precise usar Pontos de Destino para ser bem sucedido. Entretanto, caso a dificuldade seja igual à perícia do personagem-2, dificilmente ele irá falhar;
3. Definir dificuldade como ___Medíocre (+0)___ é válido em situações extremamente fáceis ou cotidianas, mas para as quais possa ser interessante uma falha: pense em personagens tentando se misturar em uma multidão.
4. Dificuldades acima de ___Ótimo (+4)___ devem ser reservadas a situações realmente sérias, pois mesmo os especialistas naquilo, personagens que tenham tais perícias como sua perícia-pico, terão alguma dificuldade para alcançar sucesso nesses momento: pense em um investigador tentando achar pistas em uma cena de crime que foi previamente limpa

> Vamos buscar o [exemplo que mostramos quando falamos pela primeira vez de ações e resoluções](https://www.dungeongeek21.com/iniciando-no-fate-resolucao-de-acoes-as-quatro-acoes-e-as-quatro-resolucoes/), quando Helen, nossa Guardiã com o Escopo da Fada Madrinha decide transformar seu parceiro Guardião Bobby em Mickey Mouse e usar esse processo todo para o descrever bem detalhadamente.
> 
> Primeiramente, vamos considerar: o resultado de sucesso e falha é interessante para a ação? Bem, eles tem uma missão de _“Curar a dor de um coração puro”_ para obter maiores informações sobre o que aconteceu com eles e o que eles tem que fazer para voltar ao normal (se possível). E para isso eles vão trazer um pouco de Disney para um garotinho que não tem condições de ir para a Disney. Para o sucesso de tal missão, o sucesso nesse teste é fundamental. Entretanto, a falha é interessante: existe toda uma gama de efeitos mágicos bizarros que poderiam decorrer devido aos desastres que Helen enquanto Fada Madrinha pode provocar com alguma magia destrambelhada.
> 
> Dito isso, sabemos que precisaremos de um rolamento, já que o sucesso e a falha são interessantes nesse caso.
> 
> Em seguida, determina-se o tipo de ação. No caso, eles precisam de uma ação de _Criar Vantagem_, já que a transformação em Bobby será tratada como um Aspecto sobre o mesmo de ___Forma de Mickey Mouse___ (em caso de sucesso). Em caso de falha, toda uma gama de coisas estranhas podem vir na forma de Aspectos (Helen virando ___Minnie Mouse___? Ou Pericles virando ___O Lobo Mau___? Ou mesmo o Edifício Dakota virando o ___Castelo da Cinderela___?)
> 
> Por fim, determinamos a perícia e a dificuldade. Como Bobby é um alvo voluntário e fazer tal mudança será algo razoavelmente fácil (ela não vai tentar fazer um ___Mickey Mouse Perfeito___ com tamanho proporcional, mas sim como se o Personagem do Parque tivesse vindo), a dificuldade será _**Razoável (+2)**_. Caso Bobby fosse um alvo involuntário, Helen provavelmente rolaria contra alguma perícia dele, como _Vontade_. Esse tipo de coisa poderia demandar alguma perícia mágica, mas Helen possui uma Façanha chamada ___Bibbity-Bobbity Boo!___ para realizar tal rolamento usando _Conhecimentos_. 

## 2. Invoca-se Aspectos necessários à ação

Nesse primeiro momento, o personagem pode resolver usar vantagens que estejam disponíveis por meio dos Aspectos do Personagem ou da cena para já meio que tentar garantir o sucesso. 

Aqui é importante dizer que isso tudo é válido, desde que:

1. Você tenha a possibilidade de usar tais Aspectos, tendo Pontos de Destino ou Invocações Gratuítas;
2. Os Aspectos se apliquem nessa situação;

O objetivo aqui é tornar interessante o aproveitamento de Aspectos que foram criados previamente para melhorar suas chances de conseguir o resultado. 

Importante: _com Aspectos o bastante, é possível garantir o sucesso, não importa o resultado dos dados._ Isso não é ruim, muito pelo contrário, é parte da própria filosofia do Fate, de tentar tornar qualquer ação interessante e seus resultados relevantes. Isso aparece quando você considera que a ação de _Criar Vantagens_ permite trazer ao jogo Aspectos que podem ser usados ___EXATAMENTE___ nesses momentos, como uma preparação prévia para a ação propriamente dita (como discutimos anteriormente), isso faz com que, sendo narrativamente interessante, mesmo o ato de limpar a testa do cirurgião seja relevante (afinal de contas, ___Óculos Embaçados___ durante uma cirurgia cerebral não parece algo bom)

> Como as coisas estão muito em cima da hora, não existem Aspectos de Cenário que Helen possa usar, e ela decide que é melhor guardar seus Aspectos de Personagem para caso os dados não lhe favoreçam. Se tivessem tido mais tempo, talvez ela pudesse preparar alguns ___Amuletos de Magia___ ou mesmo estudar um ___Ritual Mágico de Transformação___, que apareceriam em jogo como Aspectos criados por ações prévias de _Criar Vantagem_. Mas... _C'est La Vie_.

## 3. Efetua-se o rolamento

Não existe muito mistério aqui, pois já apresentamos nessa série de artigos exemplos o bastante de rolamentos de dados. O padrão é 4dF (dados Fate), mas existem algumas alternativas que são apresentadas no livro básico do Fate, como d6-d6 ou o _Baby's First Fudge Dice_. De qualquer modo, os dados serão rolados, já que já se determinou a necessidade do rolamento.

> Júlia, a jogadora de Helen, rola os dados e consegue um `-+--`{: .fate_font}, tendo um resultado nos dados ___Terrível (-2)___

## 4. Aplica-se modificadores baseados em Façanhas que sejam aplicáveis (se desejado)

Por que coloco aqui a parte de aplicar-se Façanhas, ao invés de colocar como uma parte fixa da avaliação do resultado do dado?

Primeiro, pode ocorrer que você _não deseje_ aplicar uma Façanha: lembre-se que Façanhas podem, por exemplo, representar _Equipamentos_ mais poderosos, e que podem incorrer no risco de derrotar um Alvo, o que pode não ser desejado, ao menos em um determinado momento.

Segundo, nem toda Façanha irá se aplicar. De maneira geral, existem dois grandes grupos de Façanhas: 

+ um onde um bônus é aplicado quando um personagem usa uma _Perícia_ específica é usada em um _tipo de ação_ específica em um certo tipo de _Circunstância_ específica. Tais façanhas só podem ser usadas quando TODAS essas condições (_Perícia, Ação_ e _Circunstância_) se aplicarem;
+ outro onde determinadas coisas especiais podem ocorrer baseando-se em usos limitados por sessão ou cena ou ainda por meio do pagamento de Pontos de Destino ou circunstâncias especiais. Se tais requisitos não forem cumpridos, elas não podem ser usadas;

Dito isso, qualquer façanha que se aplique a um determinado teste pode ser usada _em qualquer situação_ dentro do rolamento. Isso será mais importante quando visto abaixo em uma situação específica envolvendo Aspectos.

> Helen de certa forma já está usando uma Façanha, _**Bibbity-Bobbity Boo!**_, para poder realizar sua ação, e suas outras duas Façanhas não se aplicam aqui...
> 
> + ___Já li sobre isso___ pede para pagar Pontos de Destino para substituir uma perícia por outra... Isso não faz sentido nesse momento
> + ___Assistência Técnica___ também não é aplicável, pois só seria possível para Aspectos já criados, e o que ela está tentando fazer é criar um novo. Além disso, é só para ações com a perícia _Recursos_, não _Conhecimentos_.

## 5. Avalia-se esse primeiro resultado

Agora, é chegado a hora de avaliar o resultado.

Primeiro, vamos determinar o _Esforço_ que o personagem obteve, ou seja, o quanto ele conseguiu fazer. Para isso, some o resultado dos dados com o valor da perícia, os bônus por Aspectos usados e com qualquer bônus envolvendo Façanhas que sejam usadas.

Após isso, compare o _Esforço_ com a _Dificuldade_ do teste:

+ ___Esforço menor que a Dificuldade:___ Falha
+ ___Esforço igual que a Dificuldade:___ Empate
+ ___Esforço maior que a Dificuldade:___ Sucesso
+ ___Esforço maior ou igual que a Dificuldade+3:___ Sucesso com Estilo

E siga as orientações [sobre as quais já comentamos anteriormente](https://www.dungeongeek21.com/iniciando-no-fate-resolucao-de-acoes-as-quatro-acoes-e-as-quatro-resolucoes/) para como determinar o que pode acontecer...

> Bem...
> 
> Vamos avaliar o resultado:
> 
> + A perícia de _Conhecimentos_ de Helen é ___Bom (+3)___
> + O rolamento foi ___Ruim (-2)___
> + Helen não usou Aspectos ou possui Façanhas Aplicáveis.
> 
> Portanto o Esforço final de Helen é (3 + (-2)) = ___Regular (+1)___
> 
> A Dificuldade tinha sido previamente estipulada em ___Razoável (+2)___ 
> 
> Como o Esforço ___Regular (+1)___ é menor que a Dificuldade _**Razoável (+2)**_, ocorreu uma ___Falha___ até aqui.
> 
> Isso pode querer dizer que Helen ___transformou Bobby totalmente em Mickey___... Ou tornou o Central Park na ___Floresta Sombria da Branca de Neve___, ou qualquer outra coisa...
> 
> .. imaginando que ela aceite esse resultado

## 6. Invoca-se Aspectos para melhorar-se os resultados (de todos os lados envolvidos)

Aqui entra uma coisa importante do Fate: a chamada _Regra das Reticências_

Um resultado só é considerado ___FINAL___ quando ninguém mais pode (ou deseja) o alterar. Existem algumas formas de se fazer isso:

1. O lado derrotado pode tentar pedir um _Sucesso a Custo_. Isso se aplica em especial a testes de _Superar_ ou _Criar Vantagem_, e permite que o personagem consiga o que deseja, mas com alguma consequência que pode variar de acordo com as circunstâncias e o resultado do rolamento.
2. Qualquer um dos dos lados pode usar a _Regra das Reticências_, que envolve invocar _qualquer Aspecto **NÃO PREVIAMENTE USADO** naquele Teste_ para obter bônus adicionais ao rolamento ou mesmo para _pedir um novo rolamento_.

Aqui é importante uma coisa:

Perceba que se diz _qualquer Aspecto **NÃO PREVIAMENTE USADO**_.

O que isso quer dizer?

Quer dizer que, em um único TESTE, um Aspecto específico _só pode ser Invocado **UMA ÚNICA VEZ**_!

E como isso afeta os Aspectos?

1. Se você usar Aspectos Situacionais ou outros Aspectos que tenham Invocações Gratuítas, em geral você pode usar mais de uma das mesmas se você tiver disponível. Porém, você __NÃO PODE DIVIDIR__ bônus entre múltiplos rolamentos dentro de um mesmo teste: você tem que declarar todas as Invocações que serão usadas de uma só vez;
2. Uma das opções que um Aspecto tem é de "resetar" o rolamento, ou seja, fazer uma nova rolagem (basicamente, voltando para o passo 3 mostrado anteriormente). Entretanto, você descarta o resultado dos dados obtido e fica com o novo, ___MESMO QUE PIOR___. Além disso, todos os bônus relativos a Aspectos usados no passo 2 são PERDIDOS!

Por isso é importante manter-se a consciência de que Aspectos podem ser usados _UMA ÚNICA VEZ **POR TESTE**_. O Teste só é considerado encerrado quando _todos os lados envolvidos_ não podem mais interferir com o resultado por meio de Aspectos e/ou aceitam o resultado.

E por isso que existe a Regra das Reticências

> Bem... 
> 
> A situação não está nada boa para os Fae Guardians: um Bobby pirado como Mickey ou uma Floresta Mágica brotando no Central Park não é uma boa ideia de maneira alguma.
> 
> Helen avalia então suas opções e decide recorrer a um de seus Aspectos:
> 
> _Helen iria falhar, mas..._ - _REGRA DAS RETICÊNCIAS_
> 
> _... como Helen é uma **Pobre Menina Rica**, ela se sente solidariezada com a tristeza do garoto e, ao ver que a magia não está funcionando ela se aplica um tanto mais para transformar Bobby em Mickey Mouse, ao mover sua varinha e enunciar suas palavras mágicas: **‘Até uma criança alegrar, um rato de desenho serás! Bibbity-Bobbity-Boo!’**_
>
> Claro que o Narrador poderia utilizar qualquer Aspecto de situação para impedir que os Fae Guardians fossem bem sucedidos, como ___Energias Mágicas Espúrias___ afetando a magia, ou mesmo um ___Paparazzi___ que aparece no pior momento e prejudica a concentração de Helen!
> 
> Se ele tivesse algum...

## 7. Repete-se (5) e (6) até que:

Após essas invocações, é necessário reavaliar o resultado, repetindo os passos 5 e 6 e levando em consideração os Aspectos novos envolvidos e Façanhas que o personagem pode desejar usar.

> Ao considerar a invocação adicional de ___Pobre Menina Rica___, o narrador soma +2 ao Esforço de Helen, elevando o mesmo para ___Bom (+3)___ (_Perícia **Boa (+3)**, Dados **Terrível (-2)**, +2 por **Pobre Menina Rica**_), o que faz com que o Esforço seja maior que a Dificuldade _**Razoável (+2)**_, resultando em um Sucesso

Isso se repete até que uma das Condições abaixo ocorra

### 1. Nenhum lado tenha mais Aspectos que possam ser usados

Tecnicamente, nada impede que um personagem vá colocando Aspecto em cima de Aspecto para ir melhorando cada vez mais, _ad infinitum_, o resultado obtido. Em testes comuns, isso é meio desnecessário, mas em especial em ações de Ataque, onde a _Diferença entre Esforço e Dificuldade determinam o Estresse provocado_, isso pode ser interessante.

Por outro lado, o Narrador pode também esperar os jogadores usarem um Aspecto para ele próprio usar Aspectos para aumentar a dificuldade do rolamento (ou o Esforço do rolamento oposto), desde que existam Pontos de Destino ou Invocações Gratuítas o bastante para agir.

> Helen tem um Sucesso... Ela vai conseguir transformar Bobby em Mickey...
> 
> Mas isso não é o suficiente, pois ela sabe que seu ___Bibbity-Bobbity Boo!___ normalmente é imperfeito: ela ainda lembra da ___Carruagem com Cheiro de Abóbora___ que ela fez em um treinamento com as Fadas Madrinhas (muito _cliche_, para falar a verdade). E ela percebe que, conforme Bobby se torna Mickey, ele faz comentários engraçadinhos sobre o fato de ainda estar com seus tênis Vans, e não os icônicos sapatos amarelos de Mickey: 
> 
> Enquanto ela reponde o comentário de Bobby com um _"Meeska-Mooska Vai se lascar!"_ (_"Que feio, Dona Fada!"_), ela vê que ainda tem Pontos de Destino e pode recorrer ao seu Aspecto de ___Portadora da Fada Madrinha___ para melhorar ainda mais seu resultado. Com isso, ela consegue melhorar seu Esforço ainda mais para um ___Excepcional (+5)___  (_Perícia **Boa (+3)**, Dados **Terrível (-2)**, +2 por **Pobre Menina Rica**, +2 por **Portadora da Fada madrinha**_), o que faz com que seu resultado seja um _Sucesso com Estilo_, fazendo com que o Narrador considere que isso elimina a imperfeição do ___Bibbity-Bobbity Boo!___ que ela fez, fazendo os tênis de Bobby virarem os icônicos e esperados sapatos do Mickey.

### 2. Todos os envolvidos estejam satisfeitos com o resultado final

Claro que os personagens ou o narrador podem simplesmente aceitar os resultados dos dados, ou aceitar Sucessos a Custo em casos de Falha. Além disso, cedo ou tarde os personagens e o narrador se cansam de ficar em uma _"corrida armamentista"_ de Aspectos e Pontos de Destino para tentar melhorar tanto seus resultados, ou podem simplesmente esgotar seus recursos para tal.

Tão logo isso ocorra, o resultado final é aplicado à história e considerado final.

Importante isso: tão logo o resultado seja considerado final, ele é ___FINAL___. Quaisquer Aspectos usados podem ser usados em futuros testes, e quaisquer Aspectos que possam desaparecer como resultado das ações desaparecem (isso é excepcionalmente verdadeiro para _Impulsos_).

## 8. Encerra-se o teste com o resultado final aplicado à história

Por fim, uma vez o resultado final tenha sido declarado o Narrador, com a ajuda dos jogadores, descreve o que aconteceu como resultado do rolamento.

> O Narrador Lúcio avalia a situação e diz: *“Beleza: quando você termina de mover sua varinha mágica e entoa seu feitiço* ‘Até uma criança alegrar, um rato de desenho serás! Bibbity-Bobbity-Boo!’, *você vê que Robert vai mudando de forma, suas roupas ficando escuras até que tudo o que ele está vestindo, além da pele preta do Mickey, é o icônico calção vermelho com botões amarelos, que ele ajusta usando suas mãos enluvadas de quatro dedos. Entretanto, como sempre acontece com suas magias, ela não é perfeita: os tênis Vans de Robert continuam ali, em vez de se tornarem os sapatos grandes e amarelos que se esperam de Mickey Mouse.”*
> 
> Ricardo olha para Júlia e, imitando a voz de Mickey que seu personagem Robert agora possui, fala: _“Ah ha, você precisa trabalhar melhor suas magias.”_, com uma risadinha
> 
> _“Meeska-Mooska-vai se lascar!”_ diz Júlia, interpretando a frustração de Helen com a magia imperfeita.
> 
> _“Helen está um pouco brava, mas relembra as lições que teve recentemente com Mirana e, observando os pés de Robert, ela simplesmente se foca e, agitando levemente a varinha, diz_ ‘Bibbity-Bobbity-Boo!’, _fazendo com que um raio saído de sua varinha transforme os tênis humanos de Robert nos sapatos amarelos esperados de Mickey Mouse.”_
> 
> _“Espero que isso volte ao normal, sabe?”_ diz Robert. _“São difíceis de achar, esses Vans!”_
 
## Conclusão

Testes em Fate são mais intuitivos e simples do que aparenta: nosso objetivo ao dissecar os mesmos aqui é apresentar todas as coisas que podem acontecer no mesmo, em especial na sua interação envolvendo Aspectos e afins.

Até a próxima e...

...role +4!
