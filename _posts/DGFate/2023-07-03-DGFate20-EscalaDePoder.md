---
title: "Domando o Fate - Escala de Poder"
subheadline: Uma Introdução ao Fate 
date: 2023-07-04 09:00:00 +0300
layout: post
categories:
 - RPG
tags:
 - Fate
 - Fate Core
 - Fate-Core
 - Artigos
 - Dungeon Geek
 - Introdução ao Fate
 - Domando o Fate
 - Iniciando no Fate
 - Ações
 - Teste
 - Rolamento
 - introducao-ao-fate
 - Escala de Poder
order: 25
header: no
excerpt_separator: <!-- excerpt -->
---

Uma coisa bem comum em RPGs é questões envolvendo a diferença de poder entre dois grupos. Em especial, em situações onde dois grupos de nível de poder assimétrico (entendendo-se assimétrico aqui como _um obviamente **MUITO MAIS** poderoso que o outro_), regras de Escala de Poder tendem a ser importante.

Alguns jogos já trazem tal sistema embutindo, definindo algum tipo de "régua" padrão, enquanto outros podem se focar nos personagens e colocar as regras para escala de poder à parte.

Fate é do segundo grupo.

Mas não se preocupe, que o Fractal do Fate permite criar mudanças no nível de poder que tornem as interações dos personagens relevantes. 

Aqui comentaremos algumas estratégias gerais de como lidar com as diferentes Escalas de Poder e definir padrões nos quais as ações dos personagens poderão afetar inimigos em uma escala maior.

Então vamos começar.

<!-- excerpt -->

## Entendendo Escala de Poder

Podemos considerar que, ao falarmos de _Escala de Poder_, estamos falando em duas coisas: 

1. Qual o impacto que o personagem pode provocar no seu ambiente
2. O quanto o ambiente pode provocar de impacto no personagem

Parecem coisas simétricas...

... mas _só_ parecem.

Na questão 1, por exemplo, vamos pensar em dois exemplos extremos: qual impacto Emily e Alexander, os Camundongos Aventureiros, podem provocar no ambiente? E Goku e outros Guerreiros Z? Enquanto o reverso é a resposta da pergunta 2, qual é o impacto que o ambiente pode provocar em dois camundongos? E o quanto a terra pode "revidar" um Kamehameha?

Dito isso, vamos primeiro deixar uma coisa clara: em Fate, a Escala de Poder padrão é definida em relação aos _personagens_.

Qual a importância disso?

A importância é que, a não ser quando você tenha que lidar com questões que possam ter um impacto muito diferenciado, você não precisa realizar modificações na mecânica devido à escala. Isso é positivo quando você pensa, por exemplo, em uma perseguição entre dois camundongos (Alexander e Sem-Rabo Não-Vale-Nada) ou entre GuerreirosZ de mesmo nível (Kuririn e Tenshinhan?): ao definir a "régua" nos mesmos, evita-se ficar aplicando modificadores inúteis que, no final das contas, não adicionariam nada exceto complexidade e dor de cabeça.

Em geral, isso ajuda bastante... Mas quando o Alexander e a Emily são atacados por um Gato, ou Freeza ameaça rachar o planeta e Goku precisa o impedir, as coisas podem começar a ficar mais complicadas.

Então, vamos falar de algumas sugestões e ideias. 

## Efeito Indireto

Dependendo do quão maior é um personagem em relação à outro, ou o quão ativo ele é em relação ao mesmo, usar ações que provoquem algum tipo de _Efeito Indireto_ não é uma forma ruim de interação.

Pense, por exemplo, no Gato que quer fazer Alexander de sanduíche: 

O Gato pode transformar Alexander em chiclete de camundongo facilmente. Mas Alexander em teoria não tem formas de provocar dano tão grande contra o gato.

Agora... Pense no amigo brasileiro deles, Cláudio, o camundongo palhaço Piruto. Se ele for capaz de usar alguns truques de circo, ele é bem capaz de meter fogo no bichano tempo o suficiente para ele ser Derrotado e fugir (lembre-se que _derrota não implica em destruição_ em Fate). Emily também pode tentar cortar uma corda e liberar alguns tijolos na cabeça do gato.

Nesse caso, não se preocupe com escala: soluções inventivas o bastante podem (e DEVEM) receber o benefício de trazer seu inimigo para o seu ringue, tirando-lhe qualquer vantagem de escala, desde que isso soe coerente.

Um exemplo interessante disso está no _World of Adventure_ SLIP, onde a Covergência, a realidade virótica hostil do cenário que tenta invadir a nossa é criada como um Fractal dentro do jogo. Por ser um Fractal, ela é tratada como um personagem, e como um personagem ela tem estresse e consequências, que os personagens podem minar por meio de Ataques. Obviamente, seu poder individual enquanto pessoas é fraco demais para alterar uma _REALIDADE MULTIVERSAL PARASITA_ por ataques simples e diretos. Entretanto, ao realizar ações que frustram as investidas da Convergência, os personagens vão provocando _prejuízo_ por outros meios... 

E uma ação que provoca um _prejuízo_ contra algo em Fate é um _ATAQUE!_

Ou seja, mesmo quando a escala for muito maior, se as ações do personagem puderem frustrar as intenções de um outro com tal poder, pode-se entender isso como um Ataque. Isso pode explicar como Kuririn acerta um bom ataque contra Freeza ou como Alexander escapa de ser comido por algum bichano fedorento.

## Agora com mais algumas rodadas de _"Waaaaaaahhhhhhh!!!!!"_

OK... Agora pode ser o que você queira é realmente que exista pancadaria ou outras coisas que provoquem vantagem _mecânica_ em um Ataque. 

Você quer o gato ___Rasgue as roupas caras___ do dandy Alexander.

Você quer que Trunks ___Picote___ Freeza.

Nesse caso, você precisa ___realmente___ de uma mecânica envolvendo a Escala, já que esse tipo de ataque não traz o adversário ao seu ringue.

Qual o problema disso?

Bem, para começar, temos a questão da baixa granularidade do Fate: qualquer bônus de +1 ou +2 em algum rolamento já é o bastante para aumentar demais a escala do dano ao alvo. Um bônus +4 pode ser suficientemente poderoso para ser decisivo em combate, se você imaginar que mesmo em Fate Básico não é tão fácil você ter uma caixa de Estresse 4, o que provocaria uma Consequência que, como vimos quando falamos de conflitos, abre caminho para o _Efeito da Espiral da Morte_.

Em segundo lugar, você traz matemática demais ao jogo, e Fate é um jogo que é _Mais gramático que matemático_: as vantagens do Fate se tornam mais óbvias quando se pensa em termo de _Narrativa_ (cortar cordas, aproveitar elementos do cenário como Aspectos, etc...) do que quando se pensa em _Mecânica_ (bônus de ataque + bônus de arma + Bônus de Magia + ...). Desse modo, quando você adiciona matemática demais, você engessa o mesmo.

Dito isso...

Talvez a primeira e mais imediata forma de adicionar uma Escala é por meio dos Aspectos (sim, sempre eles)!

Se você não precisar ficar o tempo todo evidenciando vantagens mecânicas no fato do personagem ser de uma Escala diferente, usar os Aspectos é uma solução muito rápida e muito elegante de administrar-se isso, já que as vantagens mecânicas que uma Invocação de um Aspecto já são normalmente mais que suficiente em situações mais simples.

Vamos pensar em Alexander e seu Terno Caro:

O Gato acerta uma Patada com Tensão 2 final, mas o Narrador decide que quer dar uma Constrangida em Alexander, então ele usa 1 Ponto de Destino para Invocar o fato de o mesmo ser um  ___Gato Enorme (ao menos para um Camundongo)___ e com isso conseguir +2 no resultado do Ataque do Gato, elevando a Tensão para 4... Como Alexander possui apenas até a caixa 3 de Estresse, a estratégia mais prática aqui é fazer como que Alexander marque a caixa 2 e receba a Consequência Suave ___Roupas em Frangalhos___.

Aqui você pode se perguntar: _"mas não seria mais válido colocar uma_ **Perna em Frangalhos** _como Consequência?"_

Bom... Embora Alexander seja um Camundongo, uma ___Perna em Frangalhos___ não seria uma Consequência Suave, mas algo mais complicado, como Moderada ou Severa.

E se você acha que uma pessoa não pode ter problemas em lutar, ou mesmo em fugir, com ___Roupas em Frangalhos___, então você não conhece Alexander, o Camundongo da Cidade.

Essa solução tem a grande vantagem de não ser algo tão complexo mecanicamente e poder ser facilmente administrado sem regras adicionais: você pode oferecer, no início de qualquer situação, um ou dois Pontos de Destino especiais para serem usados por personagens cuja escala é maior que a de um outro.

Entretanto, você ___PODE___ precisar de algo mais _crunchy_, algo mais fixo.

Nesse caso, vamos ver algumas opções interessantes:

## Escala Fixa

Uma solução proposta no próprio _Ferramenta de Sistemas_ (página 57) é criar níveis de poder, do menor para o maior, e aplicar as bonificações abaixo ao maior dos mesmos, para cada nível de poder diferente:

+ +1 no _Ataque_ ou na _Defesa_
+ +2 no _Dano_ provocado/-2 no _Dano_ recebido

Essa é uma opção interessante pois ela "fixa" os bônus normalmente pensados na Escala quando trabalhamos com elas enquanto Aspectos. Além disso, ela evidencia o lado mais forte, o que ajuda a criar o efeito apropriado, dependendo do cenário.

Entretanto, com um número devidamente alto de níveis de poder, pode-se chegar no ponto em que, não importa o quão poderosos os personagens são, se verão contra uma oposição que pode os dizimar e contra a qual eles nada podem fazer (os "personagens Deus"). Deve-se, portanto, tomar um cuidado no número de níveis de poder a ser adaptados, para que não ocorra (ou que seja factível que ocorra) uma situação de "Personagem Deus"

> Vamos supor que essa seja a nossa solução para os personagens Guerreiros Z. Para isso, vamos definir a escala como abaixo
> 
> Humano Comum < Combatente < Sayajin < Seres Cósmicos (Kaiohs) < Super Sayajins < Super Sayjins 2 < Super Sayjins 3 < Deuses da Destruição < Super Sayjins Deus
> 
> Nesse caso, um combate entre, por exemplo, Tenshinhan (Combatente) e Goku (Sayajin) daria a Goku um bônus de +1 nos seus Ataques e nas suas Defesas, além de +2 no Dano provocado e -2 no Dano Recebido. Entretanto, uma luta entre Goku em sua forma normal (Sayajin) contra Beerus (Deus da Destruição), ofereceria para Beerus +4 nos Ataques e Defesas e +8 no Dano Provocado (YIKES!!!)

Quando ocorre essas situações, é interessante que você provenha mecanismos onde personagens podem adicionar Escala a si próprios, seja por meio de Façanhas, Extras ou Aspectos.

> Por exemplo, o treinamento de Goku com o Senhor Kaioh é um Extra que permite a ele aumentar sua escala de Sayajin para a de um Ser Cósmico, enquanto a Morte de Kuririn coloca em Goku um Aspecto ___Super-Sayajin surgido da pura raiva___, aumentando sua Escala para Super Sayajin

De qualquer modo, essa pode ser uma solução viável se você precisar lidar com personagens cujo poder é mais inerete do que oferecido por características como tamanho ou massa.

## Lidando com Monstros Gigantes

Um caso muito clássico de escala é quando você lida com monstros gigantes (e mechas e criaturas gigantes). Essa é uma situação onde normalmente a força de cada parte isolada não é _tão absurdamente_ superiora aos personagens, mas a combinação de _todas elas_ torna o desafio mais perigoso. O caso clássico é o do Dragão na fantasia medieval, mas você pode expandir para coisas como _Kaijus_ ou mesmo uma Cruzador Espacial.

Nesse caso, trate o inimigo (ou o personagem) com um conjunto de inimigos, cada qual em uma zona. Então, no exemplo do Dragão, a Cabeça é uma Zona, cada Pata é uma outra Zona, o mesmo para as asas. Todas essas zonas atacam em conjunto, apesar de terem Ataques e barras de estresse individuais. Pode ser interessante que as Consequências sejam consolidadas em uma área mais importante. Como regra adicional, o estresse que "passe" ao necessário para derrotar uma Zona específica pode "transbordar" para outras (preferencialmente a que possui as Consequências).

## Maximizando e Minimizando Dados

Uma opção interessante para alguns poderes que podem aumentar sua chance de sucesso, e que inclui a questão da Escala, é o uso de _Maximização_ e _Minimização_ de Dados. Essa regra apareceu primeiro em _Tachyon Squadron_ para os Equipamentos, mas ela pode ser usada na questão da escala.

Quando você estipula que um Dado será _Maximizado_, isso quer dizer que após momento do rolamento, um dado qualquer que tenha tido um resultado _não-positivo_ (0 ou -) se tornará positivo. Então, um rolamento +0+0 ou +0+- teria o mesmo resultado com um dado Maximizado, +0++, tendo o último dado Maximizado. Tecnicamente é um dado qualquer que será Maximizado, mas certamente é preferível maximizar o dado que melhore mais o resultado. O mesmo se aplica ao dado _Minimizado_, mas ao contrário: um Dado _não-negativo_ será tornado negativo. Ou seja, -0-0 ou -0-+ resultam no mesmo resultado, -0--.

A _Maximização_ de dado pode ser útil quando você não que "exagerar" no benefício: perceba que o benefício de uma maximização ou minimização é um pouco menor se não sairem dados negativos ou positivos. O resultado final sempre vai tender um pouco mais para um lado ou para o outro, mas sem implicar em algo similar a um bônus no resultado dos dados.

## Sistema de Peso

O que nos leva à aplicação da Maximização como forma de Escala, usando o conceito de Peso que é visto em _War of Ashes: Fate of Agaptus_.

Nesse jogo, baseado em um _Boardgame_, os personagens podem encarar monstros muito maiores que eles. Para definir a situação da escala, adota-se a regra de peso e suas comparações.

Primeiramente, calcula-se o peso da trupe de personagens: 

+ Cada personagem comum tem Peso 1;
+ Algumas Armas, Armaduras ou Poderes Especiais podem prover Peso adicional;
+ Capangas normalmente possuem Peso 0, e apenas um grande grupo deles (uma tribo, por exemplo) provêem Peso 1;
+ Criaturas muito maiores que os personagens ou com poderes muito superiores podem contar como Peso 2 ou mesmo 3;

Definido isso, soma-se os Pesos dos personagens de cada lado. O lado que tiver maior Peso poderá Maximizar um de seus Dados, desde que ele tenha ao menos o dobro do peso do lado perdedor. Se ele tiver quatro vezes mais peso que o lado perdedor, ele pode maximizar 2 dados e assim por diante.

Esse sistema de Peso é interessante pois considera que APENAS a partir do momento em que um lado possuir uma força REALMENTE superiora à do outro é que ele poderá exercer o benefício do Peso. Por exemplo: um Ogro (Peso 2) contra um aventureiro solitário comum (Peso 1) poderia exercer vantagem baseada no Peso. Entretanto, o mesmo Ogro não poderia exercer sua vantagem contra um grupo de três aventureiros (Peso 3) ou contra o mesmo aventureiro armado de uma _Vingadora Sagrada_ (que ofereceria +1 no Peso), e se veria em encrenca contra um grupo de quatro aventureiros bem armados e preparados (Peso 8, permitindo que os mesmos maximizem dois dados). 

Esse sistema também é bem interessante quando você quer similar situações onde um número de pequenas hordas se torna mais poderosa graças à influência de um guerreiro mais poderoso, e também o impacto do mesmo: pense em um grupo de quatro aventureiros (Peso 4), enfrentando um Dragão Ancião (Peso 6) que tem como aliados um Bugbear (Peso 1) e  possui também uma horda de goblins (Peso 1). O total da oposição aos Aventureiros é Peso 8, o que oferece a toda a oposição (incluindo os Goblins) um dado Maximizado. Entretanto, basta que o Bugbear caia, e todos (incluindo o dragão) perdem a maximização (Peso caindo para 7). E se o Dragão cair, os Bugbears e Goblins estarão encrencados, pois serão _os aventureiros_ a terem a vantagem da maximização!

## Conclusão

Como tudo no Fate, a "regra correta" de Escala que você deve usar no seu jogo vai variar do clima que você deseja dar ao jogo. Nada impede que você combine várias dessas sugestões para lidar com as mais diferentes situações dentro de seu jogo.

O próximo artigo provavelmente será um pouco mais filosófico.

Então, até lá...

... Role +4!
