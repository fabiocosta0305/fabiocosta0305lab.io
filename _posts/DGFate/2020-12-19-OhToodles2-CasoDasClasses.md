---
title: "Oh, Toodles! 2 -  O Caso das Classes"
subheadline: Uma Introdução ao Fate 
date: 2020-10-07 09:45:00 +0300
layout: post
categories:
 - RPG
tags:
 - Fate
 - Fate Core
 - Fate-Core
 - Artigos
 - Dungeon Geek
 - Introdução ao Fate
 - introducao-ao-fate
 - Criar Vantagens
 - Domando o Fate
 - Classes
order: 19
header: no
excerpt_separator: <!-- excerpt -->
---

> Publicado Originalmente no Site da [Dungeon Geek](https://www.dungeongeek21.com/fate-oh-toodles-o-caso-das-classes/)

_Oh! Toodles!_

Uma das perguntas mais comuns para iniciantes do Fate é...

> "Como criar uma classe de personagem"
 
Isso infelizmente reflete um pensamento limitado do qual partimos nas origens do RPG, onde cada personagem refletia um determinado tipo de tropa em um _wargame_. Entretanto, o RPG evoluiu e meio que se desfez a necessidade das classes.

Esse conceito foi ressuscitado recentemente com as Cartilhas dos jogos PbtA (_Powered by the Apocalypse_), onde adotou-se a ideia de "restringir-se" o tipo de personagem para aprimorar-se a experiência do jogador dentro do que o jogo se propõe.

Na realidade, é possível criar algo similar em Fate.Isso  pode ser de fato interessante, vide as possibilidades que _Uprising_ oferece (e que adotei no meu jogo _Fair Leaves_), ou a solução de _Wearing the Cape_, onde todos os poderes e limitações do personagem estão de alguma forma associados a um ___Aspecto de Poder___.

Claro que isso torna a preparação do jogo um tanto mais complexa para o narrador e pode tirar um pouco da Narrativa Compartilhada ao restringir o que pode aparecer dentro do jogo. Entretanto, criar tais classes pode ajudar jogadores de primeira viagem ao oferecer parâmetros iniciais dos quais o jogador pode partir.

Dito isso, vamos ver algumas possibilidades.

Então digam: _Oh, Toodles!_

<!-- excerpt -->

## Apenas adicione jogadores

Uma das formas iniciais de resolver isso, sugeridas pelo próprio Fate em seu Módulo Básico, é a ideia de _pacotes_, uma ideia que jogadores de 3D&T e GURPS verão como muito familiar. Para isso, "fecha-se" o arquétipo básico desejado (seja classe, raça, profissão ou qualquer outra coisa) em um pacote de coisas que são ou podem incluídas como um todo na ficha, a custo de Aspectos/Perícias/Façanhas/Extras.

A vantagem disso, é que o personagem irá encontrar tudo que pode desejar em um pacote fechado. Entretanto, isso obviamente trava o jogo.

Vejamos alguns exemplos abaixo:

> ### O Homem-de-arma
> 
> + ___Permissão:___ colocar um Conceito indicando um treinamento militar, seja em um exército, milícia ou grupo de mercenários
> + ___Custo:___ Quatro _slots_ de perícia, duas façanhas
> 
> _Homens-de-arma_ são os combatentes principais em um grupo: guerreiros, soldados, paladinos, bárbaros, mirmidões, samurai e outros combatentes. Em geral, são treinados em combate armados e no uso de armaduras pesadas de  combate. Costumam ser poderosos no campo de batalha, mas em compensação podem ser rudes e destreinados nas coisas da corte e do sobrenatural.
> 
> #### Perícias
> 
> + ___Perícia-Pico:___ Escolha 1 das perícias a seguir: _Lutar, Atirar, Vigor, Atletismo_
> + ___Perícias abaixo do pico:___ 
>     + ___Pico-1:___ Escolha 2 das perícias que sobraram da lista anterior
>     + ___Pico-2:___ Escolha a perícia que sobrar
>     
> #### Façanhas
>
> Escolha 2 das Façanhas Abaixo: 
>
> + ___Proficiência de Arma:___ Escolha uma Arma qualquer: seu personagem recebe +2 ao _Atacar_ e _Defender_ usando essa Arma por _Lutar_ ou _Atirar_ (dependendo da Arma, escolha no momento da compra da Façanha). Entretanto, com uma _Defesa_ com Estilo do alvo, o alvo pode trocar o Impulso por colocar um Aspecto ___Desarmado___ da Arma em questão. Pode ser adquirida várias vezes, mas nunca para o mesmo tipo de arma
> + ___Arremesso de Arma:___ Ao arremessar preparadas para combate (Lanças, Alabardas, etc...), você pode usar o melhor entre _Atirar_, _Atletismo_ e _Vigor_
> + ___Treinamento em Armadura:___ nenhum Aspecto referente a uma Armadura que o personagem esteja usando não pode ser Forçado contra ele durante um Conflito físico
> + ___Comandante de Homens:___ pode utilizar _Lutar_ no lugar de _Comunicação_ para comandar homens em um campo de batalha
> + ___Pode mandar mais!:___ ao enfrentar _Grupos de Capangas_, os atacantes _não podem_ somar bônus por trabalho em equipe no Ataque e na Defesa. Entretanto, as caixas de Estresse continuam valendo.
> + ___Arma Especial:___ Ao comprar essa Façanha, crie um Aspecto com um nome e/ou descrição especial dessa Arma. Ela é considerada Arma:2 enquanto você a usa. É cumulativo com _Proficiência de Arma_. Pode ser adquirida várias vezes, mas nunca para a mesma Arma.

> ### O Conjurador
> 
> + ___Permissão:___ colocar um Conceito indicando pactos, acordos ou tradições que o personagem opera
> + ___Custo:___ Quatro _slots_ de perícia, duas façanhas
> 
> Treinado no uso das Artes Místicas, o Conjurador domina as energias do sobrenatural, seja pelo estudo profundo dos mistérios do mundo, por tradição carregada na Ascendência, ou por pactos e acordos com o sobrenatural. Em geral, Conjuradores são mais frágeis que a maioria quando confrontados com o mundo físico, mas entretanto seu domínio sobre o sobrenatural o oferece uma vantagem do imprevisível contra aqueles que apenas são capazes de lidar com o físico
> 
> #### Perícias
> 
> + ___Perícia-Pico:___ Escolha 1 das perícias a seguir: _Conhecimentos, Vontade, Empatia, Comunicação_
> + ___Perícias abaixo do pico:___ 
>     + ___Pico-1:___ Escolha 2 das perícias que sobraram da lista anterior
>     + ___Pico-2:___ Escolha a perícia que sobrar
>     
> #### Façanhas
>
> Você recebe ___Obrigatoriamente___ a Façanha ___Magia___ e pode escolher uma adicional da lista abaixo:
> 
> + __*Magia* (Obrigatória; *Permissão:* Conceito):__ escolha uma das perícias a seguir: _Conhecimentos, Vontade, Empatia, Comunicação_. Ela será sua Perícia Mágica e você pode utilizar a mesma para realizar suas magias. Tais magias são obtidas por pesquisa (_Conhecimento_), dobrar a realidade (_Vontade_), convencer entes mágicos para realizar tais magias (_Empatia_) ou tecendo pactos e acordos com entidades sobrenaturais dos mais diversos níveis (_Comunicação_). Os detalhes de como isso é feito deve ser debatido com o Narrador. Seja como for, você utilizará essa perícia ao realizar seus poderes mágicos. Pode ser adquirida várias vezes, mas nunca para a mesma perícia, e deve incluir um novo Aspecto indicando a nova Perícia Mágica.
>    + ___Superar:___ _Superar_ pode ser usada para uma série de coisas. Primeiro, será provavelmente por ela que você aprenderá novas magias ou realizará novos pactos e acordos. Segundo, ela permite que você identifique magias e locais mágicos, mesmo que as mesmas funcionem de maneira diferente à qual você usa suas magias. Terceiro, normalmente será essa a ação a ser usada para desfazer alguma magia. 
>    + ___Criar Vantagens:___ invocar suas magias é em geral uma ação de _Criar Vantagens_, seja para convencer as criaturas mágicas ou para forçar sua vontade diante da realidade. A dificuldade da magia depende da complexidade da magia e dos detalhes relacionados, como se o personagem conhece alguma base de magia relacionada ou tem pactos com entidades que podem influenciar nessa situação. Uma _Falha_ pode representar que você foi além da conta na manipulação da realidade, deixou energia demais fluir por você, ou desagradou alguma entidade com a qual você tenha algum acordo. Os Aspectos resultantes devem indicar qualquer situação envolvida, como uma ___Magia Selvagem___ ou o fato de que ___Ashteron, senhor dos Sete Portais, está em desagravo___ com você
>    + ___Atacar:___ Você normalmente não consegue atacar magicamente. Você pode tentar gerar uma ___Bola de Fogo___, mas em geral esse efeito pode provocar dano em qualquer coisa no seu caminho. Veja abaixo para Façanhas que permitem usar magia em combate;
>    + ___Defesa:___ Você pode usar sua magia para se Defender dos Ataques tanto de outros magos quando de criaturas mágicas envolvendo magias. Entretanto, isso não impede de um Djinn de te trespassar com garras poderosas (isso é _Atletismo_ ou _Vigor_). Veja abaixo para Façanhas que permitam você fazer Defesa mística contra Ataques físicos;
> + ___Mago de Combate (Permissão: Magia):___ o treinamento do conjurado em questão incluiu técnicas para uso ofensivo de magia. Você pode utilizar a sua Perícia Mágica (especificar qual caso tenha mais de uma) para Atacar alvos fisicamente. O alvo pode se defender normalmente e o Narrador pode definir uma dificuldade mínima que deve ser ao menos empatada para que a magia seja executada.
> + ___Escudo Mágico (Permissão: Magia):___ o conjurador em questão é capaz de usar sua magia para gerar um escudo mágico capaz de deter ataques físicos. Você pode usar sua Perícia Mágica (especificar qual caso tenha mais de uma) para se defender de ataques físicos.
> + ___Escudo Protetor (Permissão: Escudo Mágico):___ o conjurador pode ampliar seu escudo mágico para criar uma zona que englobe seus aliados próximos, protegendo a todos de ataques. Role uma ação de ___Criar Vantagem___ com sua Perícia Mágica (especificar qual caso tenha mais de uma). O resultado do rolamento funciona como uma dificuldade mínima que o alvo deve passar para que o Ataque seja bem-sucedido. Cada ataque que passar queima uma das _Invocações Gratuitas_ no Escudo
> + ___Magia Fantasmagórica/Necrótica (Permissão: Magia):___ sua magia possui uma característica peculiar e muitas vezes perigosa de manipular energias entrópicas. Qualquer alvo que seja atingido por suas magias deve usar como defesa _Vigor_ ou _Vontade_, o que for menor. O dano será da natureza desejada pelo conjurador, Físico ou Mental, independente da perícia usada para Defesa.
> + ___Imbuir:___ você é capaz de colocar energias mágicas em algum objeto, conferindo-lhe habilidades ou poderes especiais por um curto período, como uma ação ___Criar Vantagem___ com sua Perícia Mágica, com dificuldade dependendo da qualidade, tamanho e características do item. Você pode imbuir um número de Aspectos ao objeto até um máximo equivalente ao número de tensões obtidas no teste, entretanto apenas uma Invocação Gratuita (ou duas em um Sucesso com Estilo) poderá ser distribuída neles. Você não é obrigado a imbuir Aspectos adicionais a um objeto. A duração dessa habilidade é de 1 Cena para cada Aspecto colocado. Em caso de _Falha_ o objeto é destruído. Em caso de _Empate_, o Aspecto colocado desaparece tão logo seja usado
> + ___Patrono:___ você possui alguém com quem você pode contar para obter novas informações sobre magia. Dê a ele um _Conceito_ e o _Narrador_ criará uma _Motivação_ para o mesmo. _Uma vez por sessão_, você pode solicitar um favor ou resposta a uma pergunta e o Narrador será completamente honesto quanto a isso. Entretanto, ele não precisa dar todas as informações ou agir contra a _Motivação_ do mesmo. O Patrono também pode demandar favores, e caso o personagem não cumpra tais favores, ele se verá ___sem acesso ao Patrono___ como uma _Consequência Moderada_

A parte positiva é que isso permite que os "pacotes" sejam facilmente integrados e modificados conforme a necessidade. Entretanto, isso também aumenta a dificuldade para jogadores iniciantes e pode demandar algumas orientações adicionais, sem falar no _tuning_ do pacote, evitando que um pacote se sobreponha em relação aos outros.

Como sugestão, ao criar um "pacote", não se preocupe em criar façanhas similares a coisas que existem: perceba que não colocamos _Já li sobre isso!_ para o Conjurador ou _Golpe Matador_ para o Homem-De-Armas. Foque em criar Façanhas que representem coisas que aquele "pacote" seja melhor.

> ### Exemplo de personagem - ___Saron Silvercoins, o Sábio___
> 
> Nascido de uma rica família de Longride, uma cidade no meio de uma rota comercial muito bem sucedida, Saron é o mais novo de uma família de 4 irmãos. Seus irmãos mais velhos se dedicaram, respectivamente, aos Negócios, ao Sacerdócio de Lathar, senhor da Luz, e ao Comando das Tropas dos Caminhantes Verdes. Sendo muito franzino, entretanto, Saron percebeu que seria melhor seguir uma rota pelos Estudos. Com o tempo, descobriu em meio aos livros de seus preceptor alguns tomos sobre teoria arcana e começou a os estudar, desenvolvendo capacidades mágicas. Depois de algum tempo, se uniu a um grupo de aventureiros, procurando novos conhecimentos.
> 
> #### Aspectos
> 
> | ___Tipo___ | ___Aspecto___ |
> |-:|-|
> | __Conceito__ | Mago Autodidata de Longride |
> | __Dificuldade__ | O nome Silvercoins é famoso, mas trás certos... Empecilhos... Consigo |
> |  | Mente sobre Corpo |
> |  | Tsyar é um bruto... Mas é o NOSSO bruto |
> |  | Caliena não entende nem metade da divindade à qual diz seguir |
> 
> #### Perícias
> 
> | ___Nível___ | ___Perícia___ | ___Perícia___ | ___Perícia___ | ___Perícia___ |
> |-:|:-:|:-:|:-:|:-:|
> | __Ótimo (+4)__ | Conhecimentos | | | |
> | __Bom (+3)__ | Comunicação | Vontade | | |
> | __Razoável (+2)__ | Empatia | Recursos | Ofícios | | 
> | __Regular (+1)__ | Atletismo | Furtividade | Roubo | Provocar |
> 
> #### Façanhas [ Recarga: 2 ]
> 
> + ___Magia:___ usa _Conhecimentos_ como Perícia Mágica
> + ___Imbuir___
> + ___Escudo Mágico___
> + ___Escudo Protetor___

## Vestindo o Manto

Uma outra solução possível, que basicamente é uma variação da Anterior, é o uso de _Mantos_, e foi vista em _Dresden Files Accelerated_. Um Manto funciona como um pacote, mas ele usa o conceito de _Condições_ para representar situações especiais que podem ocorrer com o personagem, como uma _Mudança de Forma_ para um Lobisomem ou _Favores_ para alguém que possua pactos e coisas do gênero.

Normalmente, um Manto é descrito na forma de um conjunto de façanhas que podem representar um determinado tipo de personagem. Uma ou mais dessas Façanhas podem ser obrigatórios e/ou representar as _Condições_ citadas anteriormente, portanto fazendo parte de um "pacote básico" pelo qual normalmente o personagem não pagará nada. Além disso, Mantos podem ser considerados Aspectos no seu personagem. Façanhas opcionais podem ser listadas dentro do Manto que o personagem pode adquirir. Perceba que Façanhas que fazem parte de um Manto não podem normalmente ser adquiridas por personagens com outros Mantos: o Manto é tratado como _Permissão_ e/ou _Custo_ para poder obter essas Façanhas·

A principal diferença entre um Manto e um Pacote é que o Manto é mais "descritivos", além de mais "explicito" em sua função: todo personagem em um jogo que trabalhe com Mantos VAI TER um Manto. Isso restringe um pouco o tipo de personagens, mas no caso de jogos com uma experiência um pouco mais fechada isso vai funcionar muito bem.

Entretanto, verifique se você não se pega criando Mantos e mais Mantos do nada: se isso ocorrer, pode ser sintoma de que ou sua temática é mais ampla (e nesse caso, usar Pacotes possa ser mais versátil) ou que seus Mantos não estão devidamente trabalhos (nesse caso, um ajuste fino pode ser uma boa). 

Como exemplo, vamos criar um Manto que representa o Escopo de nosso cenário de exemplo _Fae Guardians_ (achou que tinha esquecido?)

> ### Guardiões
> 
> O multiverso, onde a nossa realidade é apenas uma entre muitas, demanda Guardiões, pessoas que protegem as múltiplas realidades das consequências das transições e interações entre elas. Guardiões surgem quando certas Entidades mais poderosas de uma ou outra realidade abduz pessoas e as imbuí com parte de seu poder e essência. Essas pessoas passam a serem capazes de passar de uma realidade para a outra, além de poderem beber desse poder e essência imbuídos neles, mas muitas vezes pagam o preço de se arriscarem a se perder nessa situação.
> 
> #### Condições
> 
> + _Escopo (Estável):_  ( )( )( )( )( ) essas Condições representam o quão próximo você está de perder-se, ao menos por um tempo, em meio à essência que foi imbuída em você, comportando-se como tal criatura. Isso pode ou não ser bom, mas seja como for, durante esse período você não é você. Quanto mais _Escopo_ você marcar, mais seu comportamento e aparência serão influenciados pela essência imbuída em você: com 1, talvez seus olhos fiquem amarelentos e com pupilas retas, ou seu cabelo esbranqueça levemente e suas feições se tornem suaves e bonachonas. Com 5, você basicamente vira um lobo gigante com sede de sangue (preferencialmente de crianças), ou você vire uma velha bondosa e sorridente, cantante e quase nojenta de tão doce! Você pode marcar Escopo como _Esforço Extra_ em suas ações. Limpe uma caixa por sessão ou sempre que puder fazer uma ação que vá de acordo com seu _Escopo_. Limpe todas se você entrar em _Sobrecarga_, perdendo o controle do seu personagem até o final da essa cena ou da próxima. Ao preencher a quinta caixa de Escopo, você automaticamente entra em _Sobrecarga_.
> 
> #### Façanhas Básicas
> 
> + _Reconhecer:_ qualquer Guardião pode reconhecer a presença de criaturas ou energias sobrenaturais, ainda que não consiga determinar exatamente do que se trata. Role _Percepção_, com dificuldade dependendo do quanto essa energia está liberada: perceber um  Guardião em Sobrecarga pode ser _Regular (+1)_, enquanto uma criatura se ocultando e suprimindo sua aura de magia pode ser _Excepcional (+5)_. Uma _Falha_ pode demandar marcar uma caixa de _Escopo_ para ser bem-sucedido, ou dar sua localização a um alvo. Você normalmente será capaz de perceber distância, direção ou intenções do alvo. Apenas com um _Sucesso com estilo_ você consegue tudo isso ao mesmo tempo;
> 
> #### Façanhas Opcionais
> 
> + _Espantar:_ Pessoas comuns, sem poderes excepcionais, podem se aparovar com a presença de um Guardião. Isso pode ir desde uma leve ojeriza, como se o Guardião não tivesse tomado banho no dia ou estive com algum odor corporal estranho, até mesmo trazer a ela uma visão real do poder mágico, apavorando-a diante de algo realmente fora da realidade. Marque um de Escopo e receba +3 no próximo teste de _Provocar_.
> + _Encantar:_ Você pode imbuir um objeto magicamente com parte da energia ou essência imbuída em você. Qualquer pessoa que porte ou consuma esse objeto pode ser rastreado por você enquanto ele estiver de posse de tal objeto. Além disso, enquanto a pessoa tiver esse objeto ou o consumir, ela estará sobre efeito de um encanto simples e efetivo, aumentando sua chance de sujeitar a vontade dessa pessoa à sua. Marque 1 caixa de _Escopo_. Isso trará para jogo um Aspecto sem Invocações Gratuita de ___Objeto Encantado___. Enquanto a pessoa possuir esse objeto ou consumir o mesmo (tipo, uma barra de chocolate, enquanto o organismo estiver o metabolizando), não será necessário fazer rolamentos para a _Reconhecer_ (apenas você; outros Guardiões e similares recebem +2 nos rolamentos para a Reconhecer). Além disso, o personagem pode eliminar esse Aspecto para obter os mesmos efeitos de um Impulso (o que na prática ele é).
> + _Fascinar (Requer Encantar):_ pessoas sob efeito de _Encantar_ são facilmente Fascináveis. Você recebe +2 em todos os testes de _Comunicação_ enquanto essa pessoa estiver sob efeito de _Encantar_
> + _( ) Patrono  (Permissão: Aspecto indicando o Patrono):_ Seu Guardião possui um poderoso patrono, alguém que pode lhe ajudar em momentos de necessidade. Você pode marcar essa caixa para conseguir alguns dos benefícios abaixo, e para limpar a caixa você deve cumprir algum tipo de favor ao seu Patrono (normalmente de valor similar ao usado, mas pode ser qualquer tipo):
>     + Seu Patrono irá lhe oferecer alguns capangas para lutar ou prestar pequenos serviços em seu nome. Eles não são exatamente espertos ou poderosos (_Qualidade **Regular (+1)**_), mas podem ser úteis em suas limitações;
>     + Seu Patrono _per se_ pode cumprir algum favor para você. Entretanto, ele nunca fará nada que vai contra seus objetivos.
>     + Seu Patrono pode ser questionado sobre algo e responderá de maneira honesta e verdadeira. Ele ainda assim poderá ocultar e omitir informações, e ele não será onisciente.

Perceba que esse tipo de uso provido pelas Condições em um Manto torna ele interessante para emular coisas como _Poderes Concedidos_, Cargas de Equipamentos, Munição, ou mesmo Desejos. Basta notar que as caixas devem ser marcadas por alguma razão (como o Esforço Extra do Escopo ou para usar as capacidades do Patrono), e PODEM possuir alguma forma de limpar tais caixas (agir de acordo com o Escopo ou realizar alguma tarefa ao seu Patrono).

Importante notar que essas Condições funcionam como as indicadas na página 12 do _Fate Ferramentas de Sistema_ e como mencionamos quando falamos sobre Extras e Fractal. Isso também é importante pois você pode oferecer diferentes formas de limpeza de Condições: baseadas em tempo, coisas a fazer, etc...

Dito isso, uma das maiores vantagens dos Mantos é que você pode restringir um pouco combinações esdrúxulas: basta limitar que um personagem possa pegar no máximo 1 ou 2 Mantos, tendo que abandonar um quando adotar outro. Além disso, você pode autorizar ou não que um personagem traga Façanhas de um Manto quando assumir outros, desde que tenha Recarga para tal.

Como desvantagem, você pode se ver obrigado a preparar vários Mantos previamente, e perceber-se fazendo coisas demais apenas pela flexibilidade. Ao mesmo tempo, pode tolher um pouco a imaginação dos jogadores, ao "Fechar o pacote" via Manto.

Vejamos como criar um Personagem usando o Manto que criamos anteriormente:

> ### Tessalia "Tess" Williams, a portadora da Sininho
> 
> Várias crianças e adultos no passado foram tragados para a Terra do Nunca, então Portadores e Guardiões das essências desses personagens tão icônicos são muito comuns. Há quem diga que J. M. Barrie, criador da história _Peter Pan & Wendy_, de fato era um desses Portadores.
> 
> Entretanto, outra personagem icônica desse livro tem Guardiões com menor regularidade: Sininho, ou _Tinkerbell_ em inglês.
> 
> Tessa Williams é a mais recente portadora da Sininho.
> 
> Radical, bissexual e totalmente _grrl-power_ é a definição suprema de Tessa, antes e depois de receber seu Escopo. Nascida no Kentucky, se envolveu em todo tipo de esporte radical: flertou com o automobilismo, mas o machismo das pistas (e algumas brigas sérias com medalhões) a fez migrar para os _X-Games_, onde se sentiu em casa, em especial em modalidades mais arriscadas, como _Snowboard_, _Rallycross_ e Skate Vertical. Sua postura ultracompetitiva intimidava alguns, incomodava outros, e criava uma paixão nas pistas e fãs em volumosa.
> 
> Isso foi até o _X-Games_ em Coventry Garden.
> 
> Prova final da Megarampa e ela estava para saltar. Ela baixou o _skate_, pegou impulso, se preparou para fazer suas manobras radicais no vão... 
> 
> Quando por alguma razão um portal para um local estranho se abriu. E ela se viu apenas com seu Skate em uma versão surreal de _Querida, Encolhi as Crianças._
> 
> Conforme foi fazendo seu caminho e encontrando os habitantes do local, descobriu onde se enfiara: algum tipo de magia a trouxera até _Pixie Hollow_, lar das fadas na Terra do Nunca. Por alguma razão estranha, ela foi escolhida para ser uma emissária das _Pixies_ da Terra do Nunca em nosso mundo, uma Guardiã das realidades, uma _Fae Guardian_.
> 
> De imediato, isso a emputeceu em níveis astronômicos, o que não é de se admirar: fadas da Terra do Nunca (e Portadores das mesmas) possuem o péssimo costume, devido ao seu tamanho, de sentir apenas uma coisa por vez. E ela estava emputecida por terem lhe "sequestrado" justo quando se sagraria campeã dos X-Games! Seu humor piorou ainda mais quando ela viu o vestidinho rosa bebê que tinha substituído suas camisetas radicais!
> 
> Entretanto, conforme aos pouquinhos foi aprendendo sobre o que ela se tornara, ela passou a se divertir: magia, voo, oportunar caras rudes e curtir a farra com Fadas, o que ela poderia querer mais?
> 
> Chegou então o dia em que ela teve que voltar para nosso mundo, e ela decidiu que, já que agora ela tinha magia, uma nova forma e ainda era radical, ela ia ser radical em mudar o mundo!
>
> #### Aspectos
> 
> | ***Tipo*** | ***Aspecto*** |
> |-:|-|
> | ***Conceito*** | Esportista radical Portadora da _Sininho_  |
> | ***Dificuldade*** | Vive intensivamente... Demais para seu próprio bem |
> | ***Abdução*** | Uma Megarampa para a Terra do Nunca |
> | ***Retorno*** | Pronta para Sentir e experimentar tudo... Uma coisa de cada vez |
> | ***Adaptação*** | Magia, Skate e sexualidade à flor da pele |
>
> #### Perícias
>
> | ***Nível*** | ***Perícia*** | ***Perícia*** |  ***Perícia*** |  ***Perícia*** | 
> |-:|:-:|:-:|:-:|:-:|
> | ***Ótimo (+4)*** | Atletismo | | | | 
> | ***Bom (+3)*** | Direção | Comunicação | | | 
> | ***Razoável (+2)*** | Provocar | Empatia | Conhecimentos | | 
> | ***Regular (+1)*** | Ofícios | Percepção | Investigar | Vontade | 
> 
> #### Manto - Guardiã
> 
> #### Façanhas [Recarga: 2]
> 
> + Reconhecer
> + Encantar
> + Fascinar
> + ( ) Patrono (Fadas da Terra do Nunca)
> + ___Minha Fama me Precede:___ como _Já li sobre isso_, mas usando _Comunicação_ ao invés de _Conhecimentos_
> 
> #### Estresse/Consequências/Condições
> 
> + ___Estresse:___
>     + ___Físico:___ (1)(2)( )( )
>     + ___Mental:___ (1)(2)(3)( )
> + ___Consequências:___
>     + ___Suave (2):___ 
>     + ___Moderada (4):___ 
>     + ___Severa (6):___ 
> + ___Condições:___
>     + ___Escopo:___ ( )( )( )( )( )

### Os Arquétipos e suas possibilidades

Uma forma bem inteligente que pode-se trazer dos jogos baseados em _Apocalypse World_ (conhecidos como AWE ou PbtA) para o Fate de maneira simples é a ideia de Arquétipos. O Fate é especialmente interessante para colocar-se Arquétipos, já que isso pode ser feito de maneira muito simples, tanto que foi uma solução adotada em _Uprising - The Dystopian Universe RPG_ para emular os arquétipos esperados em um ambiente de distopia _cyberpunk_. Curiosamente, é a solução que adotei no meu futuro jogo _Family Friendly_ Fair Leaves.

Primeiramente, vamos ver como criar o Arquétipo. No caso, utilizaremos o Fate Acelerado como base para facilitar as coisas:

A primeira parte é ___Nomear o Arquétipo___. Esse nome deve descrever a função do personagem no cenário de alguma forma, e funciona como um Aspecto de Personagem, similar ao ___Conceito___. Pode ser um nome bem curto, que reflita o personagem de maneira geral: ___O Soldado___, ___O Certinho___, etc... 

É interessante que, ao criar-se Arquétipos, seja listado motivos pelos quais o Arquétipo pode ser invocado ou forçado. Isso também oferece parâmetros sobre o que o jogador pode esperar que pode acontecer de favorável e desfavorável ao personagem.

Dito isso, os Aspectos do Personagem passam a ser definidos a partir de ___Perguntas___. O ideal é que sejam de três a cinco perguntas: três perguntas oferece espaço ao personagem para expandir interesses fora do seu Arquétipo e interagir mais com o mundo por meio dos Aspectos, enquanto cinco permite cristalizar o personagem dentro do seu Arquétipo, mas com espaço para detalhamentos que o tornem mais vivos.

Ao criar as perguntas, é importante que exista algum tipo de pergunta que ligue os personagens entre si de alguma forma. Em _Uprising_, essa pergunta é __*O que você acha da* Resistance?__, enquanto em _Fair leaves_ é __*O que você pensa da Turma?*__. Perceba que em momento nenhum o personagem é obrigado a responder de maneira conformista: nada impede de que apareçam Aspectos como ___Um bando de babacas___. Entretanto, esse Aspecto tem a função de ligar os personagens entre si, e portanto deve ser tão ou mais bidirecional que os demais.

Como opção, pode-se colocar sugestões de distribuição de Perícias/Abordagens/Atitudes/Competências em geral para serem distribuídos. Isso pode tanto ser na forma de pacotes com a distribuição ou por citação de pontos fortes e fracos, a critério do narrador. Ao oferecer isso, pode-se guiar o jogador a uma distribuição que adeque o personagem aos seus interesses.

Por fim, da mesma forma que no caso de Pacotes e Mantos, o personagem pode comprar Façanhas. Aqui você pode adotar todo tipo de Estratégia: em _Uprising_ o personagem só pode escolher entre Façanhas do seu Arquétipo, como se fosse um Manto. Em _Fair Leaves_, cada Arquétipo provê uma _Façanhas Exclusiva_, extremamente mais potente que uma Façanha comum e que apenas personagens daquele Arquétipo podem usar. Fica a critério do Narrador oferecer e/ou restringir as Façanhas que um personagem poderá comprar a uma lista provida junto com o Arquétipo.

Além disso, como no caso do Manto, você pode ou não adotar _Condições_ que ofereçam vantagens mecânicas ou narrativas específicas para esse Arquétipo, ou que funcionem como "combustível" para determinados poderes.

O Arquétipo tem como ponto forte e fraco sua característica de ser um arquétipo (duuhh!): por um lado, sabe-se mais ou menos o que esperar de um personagem dentro do jogo, ao pegar-se o Arquétipo específico. Entretanto, isso também pode restringir o potencial de narração do personagem. Isso não é exatamente ruim, mas pode tirar parte do potencial do Fate de permitir personagens diferenciados.

Vamos criar alguns exemplos rápidos de Arquétipos para você usar como exemplo:

> #### O Homem de Armas
> 
> Guerreiro, Bárbaro, Paladino, Mirmidão... O homem de armas é um guerreiro, alguém capaz de feitos poderosos com a Espada, o Arco e a Lança, entre outras Armas. Além disso, Homens de Armas tendem a serem treinados, seja formalmente ou pela vida, em todo tipo de atividade relacionada com o combate: distribuição de tropas, treinamento, suprimento, táticas, estratégia...
> 
> + ___Invoque para:___ Feitos de Força; Orientar em Combate; Intimidar
> + ___Force para:___ Ser reconhecido como forte; Demandar combate honrado quando lhe for prejudicial; Buscar os oponentes mais poderosos
> 
> ##### Aspectos
> 
> + ___Por que você recorreu ao caminho das Armas?___
> + ___Qual é, em sua opinião, sua maior fraqueza em combate?___
> + ___Qual sua opinião em relação ao grupo?___
> 
> ##### Sugestão de Abordagens
> 
> + __Bom (+3) *Poderoso*; Razoável (+2) *Ágil, Esperto*; Regular (+1) *Cuidadoso, Sorrateiro*; Medíocre (+0) *Estiloso*__
> 
> ##### Façanha Exclusiva
> 
> + ___Arma preferida:___ Você possui um determinado tipo de Arma que prefere em relação à todas as demais. Você recebe +2 ao Atacar _e_ Defender usando essa Arma. Pague 1 PD para que tal Ataque deva ser absorvido diretamente nas Consequências;

> #### O Sacerdote
> 
> Sua Fé o guia em todos os seus momentos. Desde que descobriu sua Fé, descobriu seu lugar no mundo. Não importa se os Deuses estão presentes ou não, você sabe no fundo do seu coração a Verdade Revelada por sua dividade. E essa lhe promove poder para curar, abençoar e enfrentar o mal... Ou o seu exato oposto. Você também sabe da importância sua e de seus pares, mesmo entre devotos de entidades rivais ou mesmo inimigas, nos povos e culturas, e isso lhe dá uma vantagem inestimável.
> 
> + ___Invoque para:___ Respeitado pelo contato com as Divindades; Autoridade em nome de sua Divindade; Relembrar contos e lendas
> + ___Force para:___ Ser reconhecido por Religiosos de Divindades Inimigas; Se ver em maus lençóis ao obedecer sua divindade; Ter que cometer um pecado e depois pagar uma penitência
> 
> ##### Aspectos
> 
> + ___Qual sua Divindade e por que você a segue?___
> + ___Quando a sua Fé fraqueja?___
> + ___Qual sua opinião em relação ao grupo?___
> 
> ##### Sugestão de Abordagens
> 
> + __Bom (+3) *Cuidadoso*; Razoável (+2) *Sorrateiro, Estiloso*; Regular (+1) *Poderoso, Esperto*; Medíocre (+0) *Ágil*__
> 
> ##### Façanha Exclusiva
> 
> + ___Milagres:___ Sua Divindade lhe oferece Poderes relacionados aos objetivos e domínios da mesma. Escolha um número de Domínios igual a seu nível de _Cuidadoso_. Cada Domínio é representado por uma palavra simples (Vida, Morte, Paz, Guerra...) e funcionam como Aspectos. Sempre que você quiser realizar um feito relacionado ao Domínio da Divindade, role Cuidadoso contra dificuldade _Medíocre (+0)_: cada duas Tensões (arredondado para cima) coloca 1 Invocação Gratuita em um Aspecto relacionado ao Milagre desejado (como _Arma Abençoada_ ou _Comida Purificada_). Com o gasto de 1 PD, dobre o número de Invocações Gratuitas;

> #### O Criminoso
> 
> Você vive de sua esperteza, manha, trapaças e uma ocasional adaga entre as costelas. O mundo para você é bruto, sujo e injusto, e já que as coisas são assim... Chega a ser irônico que você tenha decidido usar as armas do mundo contra ele mesmo, não?
> 
> + ___Invoque para:___ Agir nas sombras; Conhecer o submundo; Procurar e obter uma situação mais favorável
> + ___Force para:___ Ser reconhecido por feitos criminosos (seus ou não, passados ou não); Ser cínico quanto ao mundo; Ser ou parecer duro e frio
> 
> ##### Aspectos
> 
> + ___O que te levou a caminhar entre as sombras?___
> + ___Quais limites você nunca passa se puder evitar?___
> + ___Qual sua opinião em relação ao grupo?___
> 
> ##### Sugestão de Abordagens
> 
> + __Bom (+3) *Sorrateiro*; Razoável (+2) *Cuidadoso, Esperto*; Regular (+1) *Poderoso, Ágil*; Medíocre (+0) *Estiloso*__
> 
> ##### Façanha Exclusiva
> 
> + ___Nas sombras:___ Sempre que você puder pegar um inimigo desprevenido, enganado ou de qualquer outra forma em uma situação onde ele não espere uma ação hostil, você pode realizar uma ação hostil _Sorrateira_, considerando o alvo _Passivo_ (dificuldade igual ao _Ágil_ ou _Cuidadoso_ do alvo, o que for menor). Pague 1 PD para considerar o alvo _Medíocre_ para efeito dessa ação.

> #### O Fanfarrão
> 
> Você é alguém que tem apostas altas: você luta de maneira espirituosa, solta bravatas desmensuradas, e seus inimigos ficam malucos quando você se aproxima. Seu conhecimento sobre histórias, contos e lendas é tão famoso quanto seu pendão para os excessos. Você não se satisfaz com a vitória: você quer o reconhecimento de que você está um patamar acima!
> 
> + ___Invoque para:___ Fanfarronice; Chamar a atenção de maneira favorável a você ou ao grupo; Agradar as pessoas
> + ___Force para:___ Chamar a atenção nos piores momentos; Buscar aventura e riscos desnecessários; Se exibir no pior momento e se dar mal no processo
> 
> ##### Aspectos
> 
> + ___O que te faz ser tão fanfarrão?___
> + ___Qual limite você se impôs na fanfarronada?___
> + ___Qual sua opinião em relação ao grupo?___
> 
> ##### Sugestão de Abordagens
> 
> + __Bom (+3) *Estiloso*; Razoável (+2) *Ágil, Esperto*; Regular (+1) *Poderoso, Sorrateiro*; Medíocre (+0) *Cuidadoso*__
> 
> ##### Façanha Exclusiva
> 
> + ___Bravatas:___ Antes de todos agirem, considerando que ao menos parte de seus Aliados possam agir, você pode realizar um rolamento de _Estiloso_ e tratá-lo como um Ataque contra todos os seus Alvos como se eles tivessem _Medíocre (+0)_ ***ou*** você pode criar uma vantagem ao ___Irritá-los___, com um total de Invocações Gratuitas igual a metade das Tensões obtidas (arredondado para cima). Pague 1 PD para obter ambos os efeitos. Não pode-se usar _Bravatas_ se por um acaso você for pego desprevenido. Caso contrário, você pode o fazer, mas apenas na sua ação.


### Combine tudo!

Na prática, você pode utilizar quaisquer e todos os elementos acima para ajustar seu jogo, criando pacotes, Mantos ou Arquétipos que se ajustem ao máximo possível ao seu jogo, sem esquecer que uma das grandes vantagens do Fate é a Flexibilidade.

Perceba que todos os exemplos acima são, na prática, Extras que recorrem ao _Fractal do Fate_ para atender as necessidades de seu jogo. Alguns exemplos são mais simples que outros, mas todos podem agregar ao seu jogo. E, como todo bom Extra, pode ter novos Extras: você incluir Pacotes em Arquétipos, amarrar Mantos aos Pacotes e assim por diante!

Fique à vontade para usar o que foi mostrado aqui, e para criar seus próprios baseado no que foi feito. Misture, mescle, adapte, e verifique o que é mais interessante. Com o tempo você será capaz de, intuitivamente, utilizar-se desses recursos para criar seu jogo da melhor maneira.

Até o próximo artigo...

_Está na mão, é só ação!_

<!--  LocalWords:  tags Djinn tuning Saron Silvercoins Longride Tsyar
 -->
<!--  LocalWords:  Lathar Caliena Dresden Accelerated Fae Guardians
 -->
<!--  LocalWords:  aparovar Tessalia Tess Williams Barrie Wendy Tessa
 -->
<!--  LocalWords:  Tinkerbell grrl power Kentucky Snowboard Coventry
 -->
<!--  LocalWords:  Rallycross Garden Megarampa Pixie Hollow Pixies
 -->
<!--  LocalWords:  Guardian oportunar World AWE Dystopian Universe
 -->
<!--  LocalWords:  cyberpunk Family Friendly Resistance leaves adeque
 -->
<!--  LocalWords:  duuhh
 -->
