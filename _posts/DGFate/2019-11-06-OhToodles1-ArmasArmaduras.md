---
title: "Oh Toodles! - Armas e Armaduras"
subheadline: Uma Introdução ao Fate 
date: 2019-11-06 12:1 0:00 +0300
layout: post
categories:
 - RPG
tags:
 - Fate
 - Fate Core
 - Fate-Core
 - Artigos
 - Dungeon Geek
 - Introdução ao Fate
 - introducao-ao-fate
 - Oh Toodles!
 - Armas
 - Armaduras
 - Criando NPCs
 - NPCs
order: 15
header: no
---

> Publicado Originalmente no Site da [Dungeon Geek](https://www.dungeongeek21.com/oh-toodles-armas-e-armaduras/)

> _"Oh, Toodles!"_
 
No desenho _A Casa do Mickey Mouse da Disney_, sempre que  Mickey e seus amigos dizem essa frase, Toodles, seu computador voador (é um desenho para crianças, relevem), aparece para que ele consiga alguma ferramenta (as _Mickey-engenhocas_) para ajudá-lo a resolver seus problemas.

E como sou o _Mr. Mickey_ do Fate Masters, achei legal chamar essa sessão dos meus artigos na Dungeon Geek dessa forma.

Aqui, vou debater algumas das melhores ferramentas narrativas e mecânicas que descobri em vários jogos baseados em Fate, como elas funcionam, por que elas são interessantes e como você pode as utilizar em seus jogos.

Esse artigo é muito diferente dos anteriores, pois aqui a coisa vai ser (talvez) um pouco menos explicativa e aprofundada e (muito) mais opinativo e de direto. A ideia ainda é ser um artigo útil para iniciantes, mas será um pouco mais focado em outro nível.

De qualquer forma, espero que vocês apreciem.

Então, vamos começar?

Então digam: _Oh, Toodles!_

## O Efeito 3D&T

No Fate, como em alguns outros RPGs, como 3D&T, a não ser que explicitamente seja especificado, não existe muita importância para que tipo de arma e armadura você está usando: é considerado que qualquer benefício que uma arma ou armadura comum seria capaz de fornecer já está incluído nas estatísticas do personagem.

Em especial no Fate, isso é parte de uma consideração quanto às Perícias do personagem: qualquer perícia que um personagem tenha em sua pirâmide implica não apenas em ter a perícia, mas também _qualquer ferramenta justificável necessária para sua aplicação_. De maneira geral, isso quer dizer que, se você tem Direção, não apenas você sabe dirigir, mas tem um veículo (dado que não exista nenhuma implicação narrativa real no fato de ter ou não um carro) ou os meios para conseguir um. De maneira reversa, ter Recursos não apenas implica em ter dinheiro, mas ter os mecanismos para maximizar seu aproveitamento.

Dito isso, em Fate se parte da premissa que, se você possui perícias de combate em sua pirâmide, como _Atletismo, Lutar_ e _Atirar_, você vai ter as ferramentas para provocar dano, ou seja, as armas.

Entretanto, tais armas não vão afetar o dano final: não importa se você utiliza um estilingue, um rifle ou o Canhão Orbital Mason-Schwartzman, o dano será o mesmo.

O mesmo vale para Armaduras: não importa se você usa uma jaqueta um pouco mais reforçada ou uma armadura transaniana subatomica, a sua defesa será a mesma, refletida provavelmente por _Atletismo_ ou _Vigor_.

Isso facilita demais a mecânica de combate: o dano é representado pelo número de Tensões pelo qual o Ataque passou a Defesa (com a opção de reduzir em 1 essa Tensão para obter um Impulso em um Sucesso com Estilo no Ataque). 

Entretanto, para alguns isso perde a característica tática e o realismo que um jogo de RPG pode demandar: afinal de contas, uma faca não consegue decapitar um braço como uma _katana_ ou uma Serra Elétrica.

A ideia então é comentarmos algumas soluções sobre Armas e Armaduras que permitam trazer uma característica tática, sem perder a parte importante do _Impacto Narrativo_

## Evite a guerra armamentista

Aqui, antes de começarmos a falar sobre regras, é importante lembrar que os jogadores poderão (e provavelmente o farão) tentar obter as armas mais poderosas possíveis, se perceberem que existe um sistema que pode permitir que as Armas oferecem vantagens contra seus inimigos. Como dito no próprio _Fate Básico_ (página 255 - _Poder Equivalente_) _"a primeira coisa que seus jogadores buscarão é eliminar a eficácia de seus adversários se equipando até os dentes. E a não ser que você queira que seus PdNs sejam abatidos facilmente, eventualmente você terá que fazer o mesmo."_

É bem clássico esse tipo de "corrida armamentista" nos RPGs. De fato, pode até ser interessante que isso ocorra: haja visto que _Dragonlance_ existe de certa forma por causa de um item poderoso matador de dragões, e _Caverna do Dragão_ também é muito sobre isso, como o Vingador tenta obter as armas dos jovens com o objetivo de destruir Tiamat e dominar os Reios, seja tentando matá-los, seja seduzindo-os com as promessas de os levar de volta para casa.

Entretanto, em se esse tipo de situação não for o que você deseja, tenha muito cuidado ao criar armas e armaduras. Pode acontecer (e com grande facilidade, devido à baixa granularidade do Fate) uma verdadeira corrida armamentista entre você e seus jogadores.

Se você pensar em incluir um sistema de regras de Armas e Armaduras, veja se ele é realmente importante, se vai compensar a "dor de cabeça" que você vai ter, tanto de planejar as regras quanto de lidar com as consequências narrativas de criar tais regras e nos resultados delas na narrativa.

Procure pensar em soluções que equilibrem as armas e armaduras em termos narrativos: uma Arma que oferece +3 de Estresse pode ser o suficiente para dizimar um pequeno grupo de capangas com um único golpe, ao melhor estilo jogos de _musou_. Uma Armadura que proteja mais de 3 pontos de estresse torna o personagem basicamente invulnerável a qualquer ataque, exceto aqueles vindos dos adversários mais poderosos e mais bem equipados. 

É justo que tais equipamentos mais raros e poderosos só sejam fabricados por armeiros talentosos e/ou reclusos e/ou que sirvam a reis com outros interesses no seu conflito e/o que possam demandar serviços e pagamentos e assim por diante. Lembre-se muito disso quando for definir essas armas. Elas serão raras, poderosas, poderão estar em mãos erradas, demandar serviços ou pagamentos ou qualquer combinação das anteriores. Isso dará a chance de tornar isso mais interessante, com espaço para narrativas interessantes e dramáticas, fazendo os personagens assumirem riscos sérios e tomar decisões proativas.

Dito isso...

## Aspectos sempre valem

A primeira e mais interessante forma de imediato de tornar Armas e Armaduras especialmente valiosas é descrever as mesmas como Aspectos, seja do personagem ou de cena.

Primeiramente, isso coloca uma possibilidade de demonstrar uma especialização de combate e fazer com que isso reflita em mecânica. Afinal de contas, apenas os ___Paladinos da Manopla Prateada___ são conhecidos por seus ataques devastadores, capazes de demolir muralhas (o +2 que é oferecido ao Invocar-se o Aspecto).

Isso adiciona também possibilidades dramáticas e ganchos de aventura que são recompensáveis com Pontos de Destino quando o personagem resolver tentar abusar. Lembre-se que os ___Paladinos da Manopla Prateada___ são reconhecíveis apenas pela sua característica Manopla de Prata... Que pode interessar a muitos, como os ___Necromantes da Escola de Revla___, que poderão ocasionalmente tentar roubar uma Manopla (e conseguir!) para seus nefastos fins! E durante esse período, é claro que o Paladino não poderá usar essa manopla (aplique como um Aspecto ___Sem Manopla___ no personagem após uma Forçada)

Um outro benefício disso é que você sempre pode utilizar esse sistema como uma forma de oferecer coisas interessantes quando os personagens estiverem procurando coisas em uma tumba (como uma ação de Criar Vantagem). Quem sabe aquela espada meio cheia de limo na verdade seja uma ___Espada dos Cruzados do Amanhã___, um grupo lendário de cavalaria de Eras anteriores?

Um ponto negativo, entretanto: ele "apenas" emula o fato de ter-se uma Arma mais diferenciada, mas não _o quão mais_ diferenciada ela é. Uma ___Espada Afiada___ e uma ___Vingadora Sagrada Dançarina das Sombras de Sangramento___ são _exatamente iguais em termos **mecânicos**_ (em termos narrativos obviamente os impactos seriam diferentes).  E isso pode ainda não ser satisfatório para jogadores vindos de sistemas mais clássicos, onde tais coisas se tornam mais importantes. Ou mesmo você pode considerar que, na própria narrativa, seja interessante que as diferenças mecânicas entre tais armas sejam impactantes.

Vamos então explorar algumas outras soluções.

## Armas e Armaduras: A mecânica

Uma forma que o próprio Fate Básico apresenta (página 253 a 255) é o sistema de _Arma_ e _Armadura_. Nele, cada Arma e Armadura possui uma gradação de potência. O nível de Arma é somado às Tensões provocadas pelo Ataque ___em caso de Acerto___, enquanto o nível de Armadura reduz o número de Tensões. De certa forma, nesse sistema a coisa seria assim:

+ ___Dano = Ataque - Defesa + (Arma - Armadura)___

Como uma consequência adicional, se o Ataque for bem-sucedido mas resultar em dano 0 devido a essa combinação, o personagem recebe um _Impulso_, como se tivesse empatado no Ataque (ainda assim adicional ao _Impulso_ em caso de _Sucesso com Estilo_).

> ___Exemplo:___ Tristan, o Cavaleiro do Espaço, está enfrentando seu inimigo mortal Reuben von Etzenbeth.
> 
> + ___Tristan, O Cavaleiro do Espaço:___ _Lutar_ Bom (+3); Sabre de Luz - Arma: 1; Escudo de Energia - Armadura: 2 
> + ___Reuben von Etzenbeth:___ _Lutar_ Razoável (+2);  Lança de Plasma - Arma: 2; Armadura de Fótons - Armadura: 2
> 
> No primeiro turno, Tristan ataca Reuben. Tristan, atacando com Lutar, consegue `-00+`{: .fate_font} (resultado final: _Bom (+3)_), enquanto Reuben, defendendo-se também com Lutar, consegue `+--+`{: .fate_font} (resultado final: _Razoável (+2)_). Como o Ataque de Tristan foi bem sucedido, seu _Estresse_ normal seria 1 (diferença dos resultados). Entretanto, Tristan tem seu Sabre de Luz que oferece +1 no Estresse (Arma: 1), elevando para 2. Entretanto, para azar de Trista, Reuben possui uma Armadura de Fótons (Armadura: 2), que remove 2 do Estresse provocado, o zerando... Não é de todo ruim, entretanto: Tristan consegue um Impulso, ___Desequilibrando___ Reuben...
> 
> .. o que é bom, já que é hora de Reuben retribuir o ataque! Ele consegue um `++++`{: .fate_font} (resultado final: _Fantástico (+6)_), enquanto Tristan consegue apenas um `00+`{: .fate_font} (resultado final: _Ótimo (+4)_). O _Estresse_ normal é 2, mas Reuben tem uma _Lança de Plasma_ (Arma:2) que aumenta para 4 o _Estresse_. Para sorte de Tristan, seu _Escudo de Energia_ (Armadura: 2) reduz novamente o Estresse final para 2. Tristan aproveita o fato de ele ter conseguido _Desequilibrar_ Reuben no ataque anterior e reduz o dano para 0. Ele ainda assim oferece a _Reuben_ um Impulso, ele agora estando _Desequilibrado_, mas a opção de levar dano não lhe é conveniente...

É importante notar que o Fate, devido à baixa granularidade, pode tornar tal sistema de Armas muito poderoso: uma Arma:4 é suficiente para derrubar, dependendo da situação, um grupo MUITO GRANDE de capangas, sendo quase como se você utilizasse um míssil antipessoal (!!!). Para isso, sugere-se que o tamanho da arma seja levado em consideração para determinar seu valor.

> + Arma: 1 - Faca ou Pistola; 
> + Arma: 2 - Espada ou Rifle; 
> + Arma 3: - Lança ou Metralhadora; 
> + Arma 4: Lança de Justa ou Granada antipessoal

Uma sugestão, caso adote isso, é fazer um "sistema quebrado". Uma arma média simples tem Arma: 0 (não muda nada). Aí você pode colocar modificadores por tamanho, como abaixo:

+ _Tamanho:_ Pequeno (-1); Médio (+0); Grande (+1); Muito Grande (+2)

E agregar modificadores baseados em elementos como _Lâmina_, _Obra-Prima_, _Perfuradora de Blindagem_, _Magia_, _Veneno_ e afins, onde cada um desses adiciona +1 à Arma.

Para as Armaduras, você pode fazer algo similar, partindo do tamanho básico, agregando uma Qualidade de Material:

+ _Qualidade de Material:_ Couro (+1); Couro Batido (+2); Cota de Malha (+3); Placas (+4)

E incluir outro elementos, similar ao caso das armas, como _Mágica_, _Abençoada_, _Obra-Prima_, _Adaptada_, etc...

> Calisto possui uma Arma e uma Armadura muito bacanas. A Arma é uma _Lança Anã_ (Grande (+1), Lâmina (+1), Obra-Prima (+1) = Resultado Final - Arma: 3) enquanto sua Armadura é uma _Armadura dos Guardiões Sagrados_ (Média (+0), Placas (+4), Obra-Prima (+1), Abençoada (+1) = Resultado Final - Armadura: 6)
 
E a grande vantagem em termos de combate é que isso permite criar uma série de Façanhas táticas interessantes:

> + ___Dim Mak:___ ao atacar com mãos nuas, ignore bônus de Armadura que não sejam relacionados a tamanho ou magia;
> + ___Estraçalhar:___ em caso de sucesso com estilo no Ataque, reduza em 1 o Estresse para ___Danificar___ a Armadura do Oponente, com um redutor na Armadura de _Dano_ equivalente a metade do Estresse provocado;
> + ___Ataque pontual:___ uma vez por conflito, declare esse ataque. Se acertar, ignora completamente Armadura;

Uma sugestão que aparece em _Ferramentas de Sistema_ (página 60) é que o nível de Arma e de Armadura representem o _mínimo de Estresse_ que é provocado pela arma e o _máximo de Estresse_ sofrido pelo usuário da Armadura. É um sistema que pode implicar em combates bem letais (imagine Calisto provocando no mínimo 3 de Estresse em qualquer ataque bem sucedido), mas com potencial para situações de demora em combate. (um cavaleiro com Armadura: 1 sofreria apenas 1 de Estresse todo turno).

### Qual o custo?

De maneira geral, em Fate, qualquer coisa que venha de uma Façanha deve representar um benefício de 2 pontos de Estresse. Em geral, você pode considerar que Arma: 2 ou Armadura: 2 sempre irá implicar em uma Façanha.

> + _Espada Bastarda:_ Arma: 2

_Entretanto_, uma sugestão que vem de _Wearing the Cape_ é fazer com que o benefício de comprar uma Arma por uma Façanha seja equivalente a Arma: 4. A principal justificativa é que o valor de _Arma_ é aplicado _apenas em caso de um Ataque bem-sucedido_, enquanto _Armadura_ vira um bônus passivo que fica o tempo todo enquanto a pessoa está na Armadura. Nesse caso, o benefício adicional em _Arma_ compensa o fato de ele nem sempre estar ativo.

De maneira geral, adote que qualquer Arma ou Armadura equivale a uma Façanha por 2 níveis (arredondado para cima).

> A Arma de Calisto custa 2 Façanhas (Arma:3) e a Armadura de Calisto custa 3 Façanhas (Armadura: 6)

## Munições e Armas descartáveis

Um tropo bastante comum para armas é a questão de que as muito poderosas tem como compensação limitações no número de usos ou recarga.

Se você desejar simular algo assim, utilize o conceito de _Condições_ (_Ferramentas de Sistema_ página 12) aplicado às Façanhas das Armas, de maneira similar ao caso das Condições dos Mantos de _Dresden Files Accelerated_.

A primeira coisa é entender o conceito: no caso, todas as _Condições_ usadas serão __Estáveis__. Condições desse tipo apenas desaparecem depois que uma determinada circunstância ocorre. Com isso, você pode propor uma série de possibilidades interessantes.

Um exemplo? Vamos ver uma Pistola convencional:

> + `6`{: .fate_font} ___Pistola de Serviço:___ Arma: 2, pode ser usada para Atacar tanto com Atirar com Lutar. Por Atirar, pode atingir alvos a até um número de Zonas de Distância igual ao nível de Atirar sem modificadores. A usar essa pistola, marque uma caixa. Quando todas as caixas estiverem marcada, a Pistola estará ___Descarregada___ e não poderá ser usada até ser recarregada. Recarregar demanda 1 Ponto de Destino em um Aspecto apropriado _ou_ um teste de _Atirar_ dificuldade ___Medíocre (+0)___ como se fosse uma ação de Criar Vantagem, onde cada sucesso limpa 1 caixa. Pode-se recarregar totalmente a Pistola fora de conflitos durante uma ação onde ele não faça mais nada além de ocasionalmente conversar.

Ela é bem mais poderosa que o normal: além de Arma: 2, ela pode ser usada tanto com _Lutar_ e _Atirar_, além de acertar a distância alvos sem modificadores.

O limite são as caixas: a cada tentativa de disparo (bem sucedida ou não) uma das caixas deve ser marcada. Isso cria um contador de disparos que logo pode se escassear. Recarregar a arma pode ser uma complicação interessante durante um combate, o que também ajuda a equilibrar as coisas.

Colocar esse contador pode ser bem interessante, pois cria uma noção de limite de uso na Arma, funcionando como uma boa compensação contra abusos. Além de prover ideias que, além de oferecer a parte tática interessante para os que gostam de jogos mais táticos, provê um arcabouço para momentos narrativos divertidos.

E pode-se adotar essa mesma regra para "armas consumíveis" (Coquetéis Molotov, Granadas, Pergaminhos Mágicos)? Claro! Vejamos um exemplo mágico:

> + `3`{: .fate_font} _Bracelete das Mil Magias:_ ao usar, marque uma caixa e escolha um efeito mágico qualquer da lista abaixo. Seu personagem pode usar Atirar para atacar alvos usando esse efeito mágico. Após todas as caixas serem consumidos, o item estará ___Exaurido___, e será necessário recarregá-lo por 24 horas.
>    + ___Bola de Fogo:___ Arma:2, Ataque em Área - Todos os alvos em uma mesma zona (incluindo Aliados), devem realizar suas Defesas individuais contra o mesmo nível do Ataque
>    + ___Mísseis Mágicos:___ escolha o alvo: ele é considerado _Passivo_ para efeito do Ataque (não rola dados). Em caso de Falha o Ataque é considerado um Empate
>    + ___Chuva de Meteoros:___ Arma: 6, ataca todos os Alvos (incluindo aliados e o próprio conjurador) em uma área igual ao nível de Atirar do usuário de raio, em zonas.
>    + ___Dano Necrótico:___ Arma: 2, o usuário pode desfazer Estresse e consequências no valor provocado de Estresse. Com 3 ou mais, é necessário um Teste de Vontade (Dificuldade igual ao Estresse). Se falhar, deve renomear um Aspecto e o tornar um _Aspecto Corrompido_.

Um item potencialmente poderoso e cheio de perigos! E com um número bem limitado de usos até ser recarregado, ou ainda que deva ser descartado (o Aspecto deixa de Existir) tão logo seja usado.

A vantagem dessa recarga é que você sempre pode colocar limites na munição/usos de itens mais poderosos. Isso pode ser inclusive interessante em jogos baseados em cenários de guerra, onde a munição limitada pode ser algo narrativamente interessante.

## Os Super-Extras do Mr. Mickey para Armas!

Bem, já que chamamos o _Toodles_, vamos ver alguns Super-Extras do Mr. Mickey para Armas! A ideia é combinar vários dos elementos que vimos anteriormente para desenvolver armas tatica e narrativamente interessantes.

Vamos então começar...

_Está na mão! É só ação!_

### Armas Brancas

Vamos começar pelas Armas Brancas, que são mais simples.

Primeiro, vamos definir alguns atributos:

O primeiro é _Tamanho_, que começa em -1 (Ruim) e pode ir até +3 (Bom). Pense em -1 como sendo uma faca ou punhal, enquanto +3 pode ser uma Alabarda ou uma Lança de Justa.

Em seguida, vamos estipular alguns _modificadores_ por meio de _marcas_ colocadas na Arma, cada uma adicionando +1 na Arma. O Narrador poderá colocar em Armaduras Vulnerabilidades _ou_ Invulnerabilidades contra tais _marcas_. Quando uma Armadura possuir Vulnerabilidade contra uma _marca_, o nível de Armadura é reduzido pela metade (apenas uma vez, não importa o número de marcas envolvidas). Quando ela tiver _Invulnerabilidade_, entretanto, o bônus da Arma _como um todo_ não pode ser aplicado. O Narrador tem a palavra final de se uma marca pode ser colocada na mesma ou não

> Exemplo de Marcas: _Ponta_, _Lâmina_, _Leve_, _Pesada_, _Sagrada_, _Profana_, _Obra-Prima_, _Mágica_, _Lendária_

Em seguida, pode-se colocar _Efeitos Especiais_ como se fossem Façanhas. Cada Efeito Especial aumenta o custo final da Arma em +2, mas não adiciona níveis de Arma.

Por fim, o custo final é igual a um número de Façanhas equivalente a metade do nível de Arma. Adicionar um Aspecto à mesma pode reduzir o custo em 1 Façanha (mínimo sempre de 1).

> Vamos tentar recriar a _Espada Olímpica_ (Light Vacuum Sword - _磁光真空剣 Jikō Shinkū Ken_) do Jiraya
> 
> Primeiro, vamos partir de uma _Katana_ básica. 
> 
> Uma _Katana_ tipicamente é uma arma de tamanho _Regular (+1)_, e tem mais duas _marcas_: _Lâmina_ e _Obra-Prima_. Desse modo, ela é uma Arma: 3 (Tamanho +1, +2 pelas Marcas)
> 
> A _Espada Olímpica_ possui ainda mais algumas marcas: _Leve_, devido a seu material alienígena, _Lendária_ e _Sagrada_, devido às histórias envolvidas na mesma e suas habilidades sobrenaturais.
> 
> Portanto, até aqui, a _Espada Olímpica_ é uma Arma: 6 (!!!) - Tamanho _Regular (+1)_, cinco marcas: _Lâmina, Obra-Prima, Leve, Sagrada, Lendária_
> 
> Mas a coisa melhora:
> 
> Vamos adicionar a Façanha relacionada à forma de _"Espada Laser"_, à qual vamos chamar de _Espada do Vazio_
> 
> + `1`{: .fate_font} ___Espada do Vazio (permissão: Linhagem Togakure, Provocar Dano prévio no alvo):___  Marque a caixa e determine o alvo: ele é considerado _Passivo_ para efeito desse Ataque. Só pode ser limpa após o fim do combate e só pode ser usada após um ataque que tenha provocado no mínimo Estresse 3 ao alvo estipulado (após considerar Armadura).
> 
> Parece forte... Mas colocamos uma permissão adicional (_Linhagem Togakure_) e além disso tem uma limitação na descrição de que só pode ser usada após acertar o alvo previamente com Estresse 3 final.
> 
> Como adicionamos a Façanha o custo teórico dessa Arma: 8 (Arma: 6 + Façanha) é de Quatro Façanhas... Mas vamos reduzir um pouco esse custo, colocando dois Aspectos.
> 
> + ___Espada de Aço Espacial; Arma representante do sucessor Togakure___
> 
> Com isso, o custo final é de Duas Façanhas.
> 
> + #### Espada Olímpica - _Light Vacuum Sword_ (Custo: 2 Façanhas)
> 
> Uma _katana_ feita de um material vindo do Espaço, a Espada Olímpica é muito leve, mas tão eficiente e mortal quanto qualquer _katana_ e até mais. O maior tesouro da família Yamaji, últimos praticantes do ninjutsu _Togakure-ryu_, atualmente está na mão de Toha, o filho mais velho, conhecido como _Jiraya_, em sua batalha contra o Império dos Ninjas
> 
> + ___Espada de Aço Espacial; Arma representante do sucessor Togakure___
>   + Arma: 6 
>      + ___Tamanho:___ _Regular (+1)
>      + ___Marcas:___ _Lâmina, Obra-Prima, Leve, Sagrada, Lendária_
>   + ( ) ___Espada do Vazio (permissão: Linhagem Togakure):___  Marque a caixa e determine o alvo: ele é considerado _Passivo_ para efeito desse Ataque. Só pode ser limpa após o fim do combate e só pode ser usada após um ataque que tenha provocado no mínimo Estresse 3 ao alvo estipulado (após considerar Armadura).

Bem... Essa é a solução para Armas Brancas.

Vamos então para Armas de Fogo

### Armas de Fogo

Começaremos com a ideia das Armas de Fogo, e que aqui pode significar armas de qualquer tamanho e na verdade de ataque à distância, o que inclui arcos e bestas, por exemplo.

Da mesma forma que para as Armas Brancas, começamos pelo _tamanho_ da Arma, de _Medíocre (+0)_ até _Bom (+3)_, sendo que o tamanho da Arma começa no _Medíocre (+0)_ com armas pequenas (pistolas) até _Bom (+3)_ para armas anti-veículos (RPGs e coisas do gênero)

Da mesma forma que no caso das Armas Brancas, você pode adicionar _marcas_, como _Perfuradora de Armadura_, _Antipessoal_, _Rajada_. Da mesma forma que no caso das Armas Brancas, cada _marca_ adiciona +1 no nível de Arma.

Façanhas podem ser agregadas normalmente, da mesma forma que nas Armas Brancas e aumenta o custo normalmente.

Agora a parte onde a coisa pode mudar é na parte de _Munição_: toda Arma possui um número de caixas de condições de _Munição_ igual ao custo final da _Arma_. Ao construir uma Arma, você pode _adicionar_ caixas de Munição, aumentando o custo da Arma em 1 Façanha para cada duas caixas (arredondado para cima). O mesmo vale ao contrário: pode-se reduzir o custo da arma reduzindo sua Munição em duas caixas para cada Façanhas de Custo (arredondado para baixo). Como regra opcional, pode-se, ao custo de 3 Façanhas, _remover_ a Munição, tornando-a uma arma de _Munição Infinita_

De resto, a determinação do custo é igual ao das Armas Brancas: metade do nível final de Arma (arredondado para cima), menos 1 por Aspecto colocado na Arma (mínimo 1)

> Vamos então criar uma Arma Futurista para uma Corporação de Mercenários, a _LionHeart_
> 
> Primeiramente, vamos começar com uma Arma de Tamanho _Razoável (+2)_, na prática uma metralhadora.
> 
> Em seguida, vamos pensar algumas _marcas_... Na prática, não há muita necessidade: essa será uma arma de uso mais geral, então não vamos adicionar nenhuma marca especial nela.
> 
> Quanto a Façanhas, essa metralhadora é conhecida por ser muito adaptável. De imediato, não vamos incluir nenhuma Façanha nela.
> 
> Nesse momento, temos uma arma de custo 2 (apenas pelo tamanho da Arma - 2) e nível de Arma:2. Com isso, a _Munição_ dela é de 2 condições de munição e custo em Façanhas de 1 Façanha. 
> 
> Entretanto, vamos adicionar 2 Façanhas no Custo para aumentar o número de caixas de Munição para 6. Para reduzir um pouco o custo, vamos colocar o Aspecto ___Arma Padrão da Corporação LionHeart___: essa arma é muito característica, e inimigos que vejam alguém usando essa arma podem imaginar o tipo de tática que seus usuários terão.
> 
> O Custo final da Metralhadora LionHeart é de 2 Façanhas
> 
> #### Metralhadora da Corporação LionHeart (Custo: 1 Façanha)
> 
> Uma arma feita sobre medida pelos melhores armeiros da Corporação LionHeart, ela é conhecida por ser uma arma versátil e confiável. Oferecida ao final do treinamento aos mercenários, ela é muito mais que uma arma, e sim algo que representa sua fidelidade e competência no campo de batalha: muitos mercenários da LionHeart presam sua metralhadora mais que sua vida.
> 
> + ___Arma Padrão da Corporação LionHeart___ 
> + `2`{: .fate_font} Arma: 2
>    + _Tamanho_: 2

### Matérias e Acessórios (Regra Opcional)

Você pode desejar oferecer aos jogadores armas básicas que eles queiram customizar. Para isso, colocamos uma regra agora para expandir suas Armas por meio de _Matérias_ (Armas Brancas) e _Acessórios_ (Armas de Fogo). Em ambos os casos, esses itens aparecem como Façanhas relacionadas às armas e "amarradas" às mesmas. Você, narrador, pode inclusive oferecer algumas Matérias e Acessórios previamente construídos. Como são Façanhas, eles adicionam pelo menos 2 ao nível de Arma em uma situação especial, à Munição, ou provocam um efeito equivalente.

> Alguns exemplos de Matérias e Acessórios:
>
> + ___Feito de Prata:___ A arma à qual essa Matéria é adicionada soma Arma: 2 nos ataques provocados contra criaturas espirituais;
> + ___Oricalco:___ A arma à qual essa Matéria é adicionada soma Arma: 2 nos ataques contra criaturas de planos externos;
> + ___Mira telescópica:___ esse Acessório aumenta em 2 o número de Zonas que o alvo possui de Alcance ao usar essa Arma
> + ___Fogo Supressivo:___ esse Acessório adiciona um modo defensivo à Arma: marque uma caixa de munição e role os dados em uma ação de Defesa. O resultado será o valor que qualquer alvo precisará passar para lhe atingir, como se fosse uma Defesa passiva. Dura até sua próxima Ação.
> + `1`{: .fate_font} ___Clipe de Recarga Rápida:___ marque esse Acessório e limpe todas as caixas de Munição quando estiver _Descarregado_. Só pode ser limpa após ser recarregada (em uma ação independente e similar a recarregar sua arma). Pode ser adquirido várias vezes, e cada Clipe é recarregado em uma ação independente.
> + ___Clipe de Munição Extra:___ ao adicionar esse Acessório, adicione duas caixas de Munição à sua Arma
> + ___Bala Traçante:___ ao adicionar esse Acessório, seu alvo recebe +1 ao se Defender de seu Ataque. Entretanto, em caso de _Sucesso com Estilo_, reduza 1 para provocar dano direto nas Consequências ___OU___ colocar um Aspecto _Em Chamas_ no alvo com uma Invocação Gratuita.
> + ___Bala de Urânio Empobrecido: (2 Façanhas)___ qualquer ataque bem sucedido ignora metade do valor de Armadura (arredondado para cima)

### Qual o limite?

Tecnicamente, nenhum... Dá inclusive para criar a _Gunblade_ de Final Fantasy com esse sistema:

> #### Gunblade (3 Façanhas)
> 
> Uma estranha combinação de Arma Branca e Arma de Fogo, ela só é usada pelos melhores combatentes, já que ela é muito desajeitada. Entretanto, na mão deles, essa estranha arma é de um nível de letalidade gigantesco, e os melhores são capazes de derrubar criaturas gigantescas apenas usando-a!
> 
> Custo final da Gunblade é de 3 Façanhas
> 
> + ___Grande e desajeitada; Uma Arma Para os Melhores___
> 
> + `3`{: .fate_font} Arma: 3
>    + Tamanho: Razoável (+2) (Espada Longa/Escopeta)
>    + _Marcas:_ Lâmina
>    + ___Versátil:___ pode ser usada tanto para Atacar à Distância por Atirar quanto em curta distância por Lutar. Entretanto, o alvo recebe +1 na Defesa. Ao atacar por Lutar, não precisa marcar Caixa de Munição
>    + ___Impulsão:___ ao lutar em curta distância, marque uma caixa de Munição e adicione +2 no nível de Arma
>    + `1`{: .fate_font} ___Limit Break (custo: Consequência):___ Uma vez que tenha recebido uma Consequência nesse combate, marque uma caixa de munição e a caixa de _Limit Break_ ao realizar um Ataque. Você faz uma sequência poderosa de golpes. Esse seu Ataque possui um bônus no rolamento equivalente à pior consequência sofrida nele. Em caso de Acerto, some também esse nível ao seu nível de Arma. Só pode ser usado em ataques de Curta Distância por Lutar.

### Armaduras e Escudos

Por fim, vamos criar uma regra de Armaduras.

Da mesma forma que no caso de Armas, vamos começar com um valor básico que reflete o tamanho da Armadura ou Escudo, partindo de +1 (_Regular_ - Peitoral Simples, Broquel) a +4 (_Ótimo_ - Armadura Completa, Escudo de Cavalaria)

Em seguida, adicionamos Marcas, na forma de Vulnerabilidades _ou_ Invulnerabilidades. Elas aumentam o custo da Arma em 1, mas não aumentam apenas aumentam sua Armadura em 1: 

+ Se uma Armadura possui uma _Vulnerabilidade_ a um determinado tipo de Marca, caso a fonte do Ataque tenha ao menos uma Marca envolvida, o nível de Armadura é reduzido pela metade (apenas uma vez).
+ Se uma Armadura possui uma _Invulnerabilidade_ a um determinado tipo de Marca, não conta-se para efeito de Dano o nível da Arma que originou o Ataque.

Caso uma Armadura possua marcas de _Vulnerabilidade_ e _Invulnerabilidade_ para a Arma envolvida, a Armadura é considerada normalmente, sem nenhuma das vantagens acima.

Da mesma forma que no caso das Armas, é somado o valor total da Armadura (Tamanho + Vulnerabilidades + Invulnerabilidades) e considera-se o custo final em Façanhas equivalente a metade do bônus de Armadura (arredondado para cima). Da mesma forma que no caso das Armas, Armaduras podem receber Aspectos que diminuem o custo em 1 por Aspecto (mínimo 1)

> Vamos criar uma Armadura exemplo, a Armadura do Caçador de Dragões
> 
> Essa será uma Armadura Completa (Tamanho _Ótimo_ - +4). 
> 
> Além disso, ela tem Invulnerabilidade a Ataques de Fogo, Gelo, Ácido e Eletricidade, as formas tradicionais de ataques provocados pelos Dragões, somando +4 pelas Invulnerabilidades à Armadura.
> 
> Entretanto, ela possui uma Vulnerabilidade a Ataques por Contusão, como os provocados por Maças, Martelos e afins, e a Ataques por Magia, como mísseis mágicos e afins. 
> 
> Com as Vulnerabilidades e Invulnerabilidades, o nível final de Armadura é Armadura: 10, custando 5 Façanhas (!!!)
> 
> Vamos reduzir o custo, colocando alguns Aspectos
> 
> + ___A Armadura Padrão dos Caçadores de Dragões; Feitas das Escamas de sua primeira presa___
> 
> Isso reduz o custo para 3 Façanhas
> 
> #### A Armadura do Caçador de Dragões (3 Façanhas)
> 
> Uma armadura usada apenas pelos melhores Caçadores de Dragões, reconhecidos pela Guilda Wyrmbane, essa Armadura é tipicamente forjada a partir de algumas das Escamas da Primeira Presa abatida por um Caçador de Dragões. Ela tem um tratamento especial que a torna especialmente resistente contra todo tipo de ataque vindo das terríveis baforadas de dragão. Entretanto, graças ao uso intensivo de magia na sua construção, ela se torna porosa contra ataques mágicos, além de pouco resistente contra ataques contusos, como os de Martelos e Maças. Além disso, o Caçador de Dragões tende a ser alvejado por qualquer Dragão próximo, pois eles consideram Anátema que alguém use suas Escamas para qualquer coisa.
> 
> + ___A Armadura Padrão dos Caçadores de Dragões; Feitas das Escamas de sua primeira presa___
> + ___Tamanho___ _Ótimo (+4 - Armadura Completa)_
> + ___Armadura:___ 10
> + ___Invulnerabilidades:___ _Fogo, Gelo, Ácido, Eletricidade_
> + ___Vulnerabilidades:___ Contusão, Magia



## Conclusão

É possível sim, dentro do Fate, criar sistemas de Armas que, ao mesmo tempo que oferecem características táticas relevantes, também tragam detalhes narrativos impactantes. Tudo é uma questão de verificar até que ponto o detalhamento tático é válido em termos narrativos e a partir de onde a tática está apenas engessando a ficção.

Lembrando sempre que em Fate, _"entre regras e narrativa, a narrativa vem primeiro!"_

Aguardo vocês no próximo artigo!

_Está na mão, é só ação!_

<!--  LocalWords:  Dungeon Geek Toodles Mickey Disney Mr transaniana
 -->
<!--  LocalWords:  Mickey-engenhocas Mason-Schwartzman subatomica Dim
 -->
<!--  LocalWords:  katana PdNs Dragonlance Tiamat Reios musou Revla
 -->
<!--  LocalWords:  Tristan Reuben Etzenbeth Obra-Prima Mak Wearing
 -->
<!--  LocalWords:  the Dresden Accelerated Super-Extras tatica Vacuum
 -->
<!--  LocalWords:  Sword Jikō Shinkū Ken Jiraya Togakure Yamaji Toha
 -->
<!--  LocalWords:  ninjutsu Togakure-ryu anti-veículos LionHeart
 -->
<!--  LocalWords:  Gunblade Fantasy Limit Wyrmbane
 -->
