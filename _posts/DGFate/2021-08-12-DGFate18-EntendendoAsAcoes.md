---
title: "Domando o Fate - Parte 18 - Entendendo as Ações"
subheadline: Uma Introdução ao Fate 
date: 2021-08-12 10:10:00 +0300
layout: post
categories:
 - RPG
tags:
 - Fate
 - Fate Core
 - Fate-Core
 - Artigos
 - Dungeon Geek
 - Introdução ao Fate
 - Domando o Fate
 - Iniciando no Fate
 - Ações
 - introducao-ao-fate
order: 21
header: no
---

> Publicado Originalmente no Site da [Dungeon Geek](https://www.dungeongeek21.com/domando-o-fate-entendendo-as-acoes-2/)

Como dissemos anteriormente, ao explicarmos os rolamentos e ações, tudo em Fate pode ser resumido em um conjunto de Quatro Ações: _Superar, Criar Vantagens, Atacar_ e _Defender_.

Entretanto, você pode se perguntar se isso não dá margens para confusões.

Em teoria, a resposta para isso é um sonoro ___NÃO___. 

Mas na prática existem certas nuances das ações que podem criar confusões. 

Então, esse artigo visa tentar esclarecer melhor como utilizar cada uma das ações e como não se confundir ao definir qual o tipo de ação que será usada em um determinado momento.

Importante notar que, como muitas coisas em Fate, as Ações serão ditadas pela _Narrativa_. Portanto, sempre fique atento a isso ao decidir como escolher suas ações.

## _Superar_: Resolvendo problemas

A ação que será mais usada no Fate para qualquer coisa é a ação de _Superar_. De fato,se você observar a lista das Perícias no _Fate Básico_, página 88, você verá que TODAS as perícias podem ser usadas para _Superar_.

Uma ação de _Superar_ basicamente visa remover algum tipo de adversidade ou problema representado por uma situação na história, seja ela representada ou não por Aspectos na história.

Uma outra maneira de entender _Superar_ é _"realizar aquilo para o que a Perícia foi planejada"_. Essa é uma forma interessante, porém perigosa e insuficiente de entender Superar: pense na Perícia de _Lutar_. Se pensarmos dessa maneira, a função de _Lutar_ é descer o braço no oponente, certo? Entretanto, isso é coberto na ação de _Atacar_, não? 

Na prática, uma forma de usar _Lutar_ para superar seria bem visto em uma Disputa, onde você tenta muito mais demonstrar sua capacidade de luta (seja em um campeonato ou uma demonstração de habilidade) do que _realmente_ quebrar a cara de alguém. Além disso, você poderia usar Superar com _Lutar_ para, por exemplo, verificar o estado de um equipamento de combate, como uma espada.

Em inglês, a ação de superar é chamada de _Overcome Obstacle_, _Superar um Obstáculo_. Podemos pensar nisso para ajudar a entender do que se trata essa ação: sempre que você tiver ___algo___ entre você e o que você deseja fazer, você irá usar uma ação de _Superar_. Esse ___algo___ pode ser tanto físico (barreiras, tempo, etc...) quanto mental (diferenças de idioma, mente fechada, etc...)

> ___Exemplo:___ Helen está estudando alguns livros antigos para entender o porque tantos meninos (e Bobby) estão virando burrinhos depois de beber uma estranha bebida vendida recentemente. Para isso, ela conseguiu obter uma garrafa com algumas sobras de tal líquido. O Narrador decide que essa é uma ação de Superar, já que Helen precisará entender como esse líquido funciona, e estudar ele de todas as maneiras que ela conhece. Ele pode optar tanto por _Investigar_ (Helen bancando o CSI), _Conhecimentos_ (Helen utilizando seus conhecimentos da época de escola) ou _Fábulas_ (Helen realizando procedimentos alquímicos e pesquisando alguns tomos de magia antigos que conseguiu com as Fadas Madrinhas). De qualquer forma, essa será uma ação de Superar, pois caso ela seja bem-sucedida, ela entenderá o que diabos é esse líquido, embora ela não receba nenhuma vantagem imediata.
 
### Superar e Aspectos

Uma coisa interessante: exceto para o caso dos Sucessos com Estilo, ações de Superar não proveem Aspectos (isso é função da ação de _Criar Vantagens_). ___Entretanto___, Superar é a ação padrão para REMOVER Aspectos em geral, tirando aquele Aspecto de jogo, tornando aquela situação não mais uma parte tão importante da cena que mereça ser descrita por meio de um Aspecto.

> ___Exemplo:___ Os _Fae Guardians_ salvaram a população de Nova Iorque de uma invasão de Trolls, mas parte da população está ___Apavorada ao ponto de provocar caos___, em especial quando instigados por uma ordem de pastores que odeiam qualquer coisa relacionada à magia. Pericles decide utilizar seu retrospecto como ___Investigador Particular Portador do Lobo Mau___ e o fato de ter sido um policial no passado para tentar acalmar essa turba que está se formando. Para isso, ele precisa rolar uma ação de _Superar_ usando sua perícia de _Comunicação_ (_"Voltem para suas casas e não provoquem pânico!"_)  ou talvez de _Empatia_ (_"Ok, todo mundo. Foi assustador mas o show acabou. Podem se acalmar e voltarem para suas casas."_). Se ele conseguir, tal Aspecto será removido de jogo, representando o fato de as pessoas, ainda perguntando que diabos aconteceu e que caracas era aquele estranho grupo que enfrentou os Trolls, irem para suas casas.

Ocasionalmente, você pode utilizar uma _Disputa_ como forma de permitir que uma ação de _Superar_ altere um Aspecto. Perceba que é um jogo perigoso: uma derrota em uma Disputa pode comprometer ainda mais a situação.

> ___Exemplo:___ Pericles perceber que alguns dos pastores, chamados de _Filhos de Gideão_, tentam instigar as pessoas contra os _Fae Guardians_, e decide bater boca com os mesmos. Como ambos estão disputando a atenção das pessoas, e apenas um poderá a ter, o Narrador decide que isso é uma Disputa. Se Robert vencer, as pessoas irão o ouvir e poderão até mesmo passarem para o lado dos _Fae Guardians_, mas caso ele seja derrotado, a narrativa fanática dos Gideões será suficiente para colocar as pessoas ainda mais contra os _Fae Guardians_

Como uma forma opcional de ver as coisas, uma ação de Superar pode gerar Aspectos em jogo. ___ENTRETANTO___, exceto no caso de um _Sucesso com Estilo_, você NÃO DEVE oferecer Invocações Gratuitas em tal Aspecto, pois isso entra na seara da ação de _Criar Vantagens_. Gerar um Aspecto pode ser uma forma de representar para os jogadores o caminho que suas ações está indicando, mas no caso das ações de Superar não deve oferecer nenhum outro benefício adicional

> ___Exemplo:___ Helen utiliza sua perícia de _Fábulas_ para investigar o que está acontecendo, já que ela suspeita que essa transformação bizarra seja provocada por algum ___malefício mágico___ e é bem sucedida. O Narrador trás para o jogo o ___Malefício Mágico___ como um Aspecto, mas sem invocações, dizendo _"Após ler os tomos de magia e fazer as análises apropriadas usando um laboratório alquímico improvisado, você descobre que o líquido em questão é_ **Água da Ilha dos Prazeres**, _uma fonte de água amaldiçoada que transforma todo aquele que o bebe em um burro. Lendo os relatos dos tomos, você sabe que existe uma poção mágica que pode reverter esse processo, mas ela só pode ser preparada com ingredientes mágicos especiais que existem apenas no Circo di Strombolli, na mesma Realidade Mágica de onde Bobby veio."_ Agora, se Helen vai conseguir utilizar isso para criar uma poção é outros quinhentos, já que ela não recebeu nenhuma Invocação Gratuita.

## _Criar Vantagens_: Preparando-se para o pior

A ação "gêmea" de _Superar_ é a ação de _Criar Vantagens_. Uma forma simples de entender _Criar Vantagens_ é exatamente dada pelo nome: você está _Criando_ algum tipo de _Vantagem_ que irá aparecer em jogo. Essa vantagem vem sempre na forma de um Aspecto no jogo, que pode ser _Descoberto_ (algo que estava lá desde o início e se provou útil) ou _Criado_ (você fez algo para melhorar suas chances).

Essa é uma ação que muitas vezes se confunde com a ação de Superar, e por isso mesmo a chamamos de "gêmea", mas uma forma interessante de distinguir uma da outra é pensar que uma é _Reativa_ e a outra é _proativa_: enquanto, ao usar _Superar_, você tentar resolver uma encrenca, a ação de _Criar Vantagem_ visa preparar algo que você possa usar para evitar que o problema perdure.

> ___Exemplo:___ Robert sabe que ele e os _Fae Guardians_ Pericles e Helen deverão entrar no misterioso culto dos _Filhos de Gedeão_ para entender como eles estão descobrindo e suprimindo outros _Fae Guardians_ de seus Escopos (ocasionalmente com resultados catastróficos e fatais). Para isso, ele decide dar uma passada escondido no _Filhos de Gedeão_, tentando observar as coisas, até mesmo fingindo ter interesse em participar de um culto, para analisar os pontos fracos da construção e tudo o mais. O narrador decide que Robert poderá usar tanto _Investigar_ (analisar o fluxo de pessoas, tirar algumas fotos) quanto _Roubo_ (notar pontos com baixa vigilância, câmeras de segurança, potenciais sensores ou alarmes) para criar essa vantagem para os _Fae Guardians_ ao analisar os ___Pontos Fracos da Capela dos Gideões___
 
Importante notar que uma ação de Criar Vantagem traz um Aspecto para o jogo ___ou também___ pode melhorar a vantagem produzida, ao adicionar _Invocações Gratuitas_ a um Aspecto já existente.

> ___Exemplo:___ Antes de começarem a invasão propriamente dita, Robert decide dar uma última checada para avaliar como a Capela dos Gideões é guardada à noite. Como o Aspecto já está em cena (ele foi bem sucedido ao avaliar os ___Pontos Fracos da Capela dos Gideões___) ele pode melhorar suas chances ao adicionar uma segunda _Invocação Gratuita_ ao mesmo. Entretanto, se ele falhar, pode ser que ele tenha _"dado bandeira"_ e chamado à atenção dos Guardas dos Gideões, que irão estar preparados e saberão dos ___Pontos Fracos___, tomando-lhe a Invocação Gratuita.

### A Ação de Criar Vantagens e os Aspectos

Uma coisa muito importante: uma ação de Criar Vantagens é uma ação que NUNCA PASSA ILESA! Ela ___SEMPRE___ criará um Aspecto, seja qual for. Isso é interessante no caso de falhas, onde os adversários ou mesmo terceiros não relacionados poderão descobrir algo sobre o que os personagens estão fazendo, e isso pode trazer todo tipo de complicação interessante.

> ___Exemplo:___ Robert falhou no teste para melhorar o conhecimento dos ___Pontos Fracos___, e isso faz com que o Narrador receba de Robert a _Invocação Gratuita_. O Narrador pode ficar a vontade para usar isso de todas as maneiras possíveis. Por exemplo:
> + A óbvia, que é fazer com que essa _Invocação Gratuita_ represente o fato que Robert deu bandeira e eles vão preparar uma armadilha;
> + Mas pode também representar um alarme silencioso que Robert não percebeu e disparou assim que os _Fae Guardians_ entram, e que foi usado para chamar a polícia;
> + Ou mesmo pode representar alguém que também tem algum interesse nos Gideões e vai aproveitar qualquer confusão para aprontar e jogar a culpa nos _Fae Guardians_;

Isso é muito, MUITO importante, devido à característica da ação de adicionar as _Invocações Gratuitas_: isso pode representar todo tipo de cenário interessante

+ ___Exemplo:___ imaginemos que Robert tenha tido um sucesso com estilo em uma primeira ação e depois falhado. O que isso pode representar? Entre muitas outras coisas, que Robert conhece uma série de ___Pontos Fracos___ (com apenas UMA invocação), mas que os Gideões perceberam tais Pontos Fracos e sabem que alguém está com algum interesse neles (representando a invocação que Robert foi obrigado a passar para o Narrador pela Falha)

Além disso, a ação de _Criar Vantagem_ é especialista na ideia de ___Alterar um Aspecto___ (não de o remover: isso é parte de _Superar_). De fato, convencer uma ___Turba Hostil___ a estar ___Pronta para Ouvir___ os personagens pode ser uma ação de Criar Vantagem usando _Empatia_ ou _Comunicação_, por exemplo:

> ___Exemplo:___ Helen percebe que a população em pânico está virando uma ___Turba Hostil___, graças às palavras dos Gideões. Helen então decide utilizar suas habilidades de discurso (refinadas por horas e horas de de preparação, _media training_ e estudos e treinos de retórica junto às Fadas Madrinhas) para explicar que os _Fae Guardians_ foram tão vítimas quanto a turba. Ao fazer isso, a ideia de Helen é fazer com que ao menos a turba esteja ___Disposta a Ouvir___ os _Fae Guardians_. Isso pode ser feito por um rolamento de _Criar Vantagem_ por _Empatia_ ou _Comunicação_. Claro que, caso ela falhe, ela pode simplesmente adicionar uma invocação gratuita adicional à ___Turba Hostil___, mas se ela for bem sucedida ela poderá sim conseguir com que as pessoas estejam ___Dispostas a Ouvir___ os Fae Guardians.

### _Criar Vantagens_ e _Descobrir_:

Em alguns sistemas baseados em Fate, como _Bulldogs!_ ou _Mindjammer_, a ação de _Criar Vantagem_ é desmembrada em duas versões: _Criar Vantagens_ e _Descobrir_.

Na prática, isso apenas é uma forma de tornar visível um fato que é comum dentro da ação de _Criar Vantagem_: você pode tanto _Criar uma Vantagem_ quanto _Descobrir uma Vantagem_, por assim dizer.

Isso se deve ao fato de que no Fate a ação de _Criar Vantagens_ ter, digamos assim, duas descrições:

> _"Use a ação de criar vantagem para_ criar aspectos de situação que trazem benefícios **ou** para receber os benefícios de qualquer aspecto _a qual tenha acesso."_ (Fate Básico página 128) 

Além disso, ao descrever as resoluções, o Fate Básico usa tanto _Se você estiver usando a ação criar vantagem para criar um aspecto_ (página 128) quanto _Se estiver criando vantagem em um aspecto existente..._ e no Fate Acelerado é descrito de maneira similar.

A diferença chave aqui é _criar aspectos_ versus _receber os benefícios de qualquer aspecto_. Na primeira situação, estamos deixando claro que queremos preparar algo do zero: por exemplo, fazer uma ___Arma Laser na Gambiarra___ usando peças da loja de eletrônica para matar os alienígenas. Na segunda, queremos aproveitar algum Aspecto que já estava lá, talvez o renomeando, como uma forma de melhorar as nossas chances: por exemplo, afundando-se ainda mais ___Nas Sombras___ para obter uma posição favorável contra inimigos.

Isso pode ser interessante, pois você pode utilizar ações assim para transformar, por exemplo, Impulsos (Aspectos que desaparecem rapidamente) em Aspectos mais duradouros (por exemplo, ao aproveitar para ___Derrubar no chão___ [Aspecto Duradouro] o Inimigo ___Desequilibrado___ [Impulso]). Também pode ser uma forma de, por exemplo, representar que você conseguiu algum tipo de _drop_, para usar a linguagem dos RPGs eletrônicos, relacionados a Aspectos que representem inimigos. (Por exemplo, tomar as ___Armaduras Escuras___ dos ___Soldados da Perdição___ que foram derrotados).

Exceto por essa diferença circunstancial, a Ação de _Criar Vantagem_ funciona de maneira similar, não importa se você está tentando preparar uma vantagem _"do zero"_ ou está tentando aproveitar o que o cenário o oferece.

> ___Exemplo:___ Robert pretende invadir o circo dos Palhaços Demoníacos ([baita tropo batido](https://tvtropes.org/pmwiki/pmwiki.php/Main/MonsterClown), hein, narrador?) e para isso, ele precisa improvisar algum tipo de figurino de palhaço para não ser detectado pelos mesmos. Ele pode, por exemplo, usar a ação de _Criar Vantagens_ por _Recursos_ para comprar uma fantasia pronta, ou por _Ofícios_ para, usando tecidos adequados, fazer uma, ou por _Roubo_ para, depois de nocautear um Palhaço Demoníaco, tomar-lhe as roupas. No primeiro e no último caso, a ação seria mais próximo de _Descobrir uma Vantagem_ (seja por encontrar uma ___Fantasia de Palhaço Diabólico___ ou por roubar as ___roupas do Palhaço Demoníaco___), enquanto a do meio representa uma possibilidade de _criar uma vantagem do zero_ (a não ser que o narrador tenha oferecido alguns ___Trapos Imundos___ que os personagens podem costurar).

Lembre-se que _nem tudo **é**_ um Aspecto, mas _tudo **pode tornar-se**_ Aspecto.

## Ataque - provocando prejuízo

Já foi dito várias vezes sobre a questão de como a ação de Ataque é diferenciada.

Primeiramente, é muito raro você usar uma ação de Ataque fora de um Conflito e/ou que não provoque um. Normativamente, qualquer ação de Ataque dá início _de imediato_ a um Conflito.

Além disso, é importante notar que a ação de Ataque ___SEMPRE___ visa provocar algum tipo de dano ou prejuízo, e que prejuízo aqui não pode ser reduzido a dano físico: perturbação mental, gafes sociais, prejuízo financeiro, tudo isso pode ser resultado de uma ação de Ataque.

De fato, em _Weird World News_ são usadas ações de _Ataque_ (!!!) para perseguições ao estilo _Scooby-Doo_. Parece estranho, mas quando você pensa que você vai perturbando o monstro (ou vai sendo assustado por ele) ao ponto de ele (ou você) não conseguir mais pensar direito, inegavelmente você tem uma situação de _prejuízo_ aqui (à sua estabilidade de raciocínio), e portanto temos uma ação de _Ataque_.

Dito isso, vamos ver algumas coisas importantes:

A Ação de Ataque é pouco presente diretamente: na prática, apenas _Atirar_, _Lutar_ e _Provocar_ são _per se_ usáveis em ações de _Ataque_. Quando comparamos com o fato de que TODAS as perícias podem ser usadas para _Criar Vantagem_ e _Superar_, e apenas 6 das 18 perícias não podem ser usadas para _Defesa_, isso aparentemente restringe bastante as possibilidades de _Ataque_.

Entretanto, a ação de _Ataque_ costuma ser muito visada para _Façanhas_ que adicionam uma ação a uma perícia.  Por exemplo, vamos lembrar de _Chevalier_, o Lugar-Tenente de Morgause, que vimos quando falamos de Conflitos. Ele possuía a Façanha ___Mago de Combate___ que permitia usar _Conhecimentos_ para _Atacar_ usando magias no lugar de _Atirar_. Isso adiciona, ao menos nessa situação específica de magias, a ação de _Atacar_ a perícia de Conhecimentos.

Um outro exemplo possível seria:

> + ___Carmaggedon:___ Enquanto pilotando veículos de médio porte (como carros), você pode _Atacar_ todos os alvos que estiverem a pelo menos um número de Zonas igual ao seu bônus de _Condução_. Todos os alvos devem absorver o mesmo valor de _Estresse_. Entretanto, se algum dos alvos for bem sucedido com estilo em uma ação de _Defender_, o veículo recebe o mesmo valor de Estresse que provocaria contra as pessoas. O veículo deve poder deslocar-se entre as zonas e para na zona onde terminar de atacar onde ele se deparar com uma Defesa bem-sucedida com estilo. O Ataque é ___Indiscriminado___, acertando aliados ou inimigos.
 
Aqui também é importante frisar que você deve tomar cuidado ao adicionar a ação de _Atacar_ a outras perícias: se você colocar sem muito cuidado, você pode desequilibrar o jogo, tornando o jogo ou pouco verossímil ou tirando o foco de outras perícias. Algumas formas de equilíbrio de Façanhas podem envolver pagar Pontos de Destino no uso (como no caso de _Já li sobre isso_ - Fate Básico, página 96), limitar os modo que ela pode ser usada (como no caso do ___Mago de Combate___ de Chevalier), encadear com outras Façanhas (novamente como no caso de ___Mago de Combate___ que demanda ___Sangue Élfico___), ou por fim colocando alguma cláusula que permita que os personagens escapem de maneira mais fácil ou possam colocar o usuário em enrascada.

Perceba que, quando criamos ___Carmaggedon___ colocamos alguns fatos complicadores:

1. O veículo precisa ser capaz de se deslocar por todas as zonas por onde atacar;
2. O Ataque é ___Indiscriminado___, acertando TODOS os alvos válidos nas zonas por onde ele passar;
3. O veículo obrigatoriamente para ao atingir qualquer tipo de barreira, como um muro ou um veículo maior;
4. O veículo sofre o mesmo ataque que provocou se, por um acaso, um dos personagens for bem-sucedido com Estilo em uma ação de Defesa (pense que o cara fez o motorista ficar tão louco que ele se arremeteu contra uma parede ou debaixo de um caminhão).

Isso compensa o poder de simplesmente arremeter em um ataque demolidor que pega todo mundo no meio do caminho.

### Adicionando novos _Ataques por padrão_

Dito isso, como em muitas situações, o Fate pode prever situações onde, devido ao cenário, você pode adicionar a possibilidade de a ação de Ataque ser adicionada por padrão a perícias dependendo do cenário e da circunstância.

Por exemplo: em um cenário de horror, um monstro que tenha, por exemplo, a perícia de ___Assustar___ pode ter por padrão a ação de Ataque incluída na mesma. 

Cuidado, entretanto, ao fazer isso, pois pode minar algumas situações de equilíbrio e tirar um pouco o brilho de situações onde personagens podem, sem usar a ação de _Ataque_ adicionar brilho a um conflito.

### Quando um Ataque não é um Ataque

Em muitas situações o que aparenta ser um Ataque na prática não o é, podendo ser alguma das demais ações (em geral, _Criar Vantagem_). Provocar o adversário, jogar areia nos olhos, e assim por diante, são ações que podem ser resolvidas sem necessariamente resultar em um _Prejuízo_ ao alvo (ao menos não de maneira direta).

> ___Exemplo:___ Alexander, o Camundongo da Cidade, está fugindo um gato sarnento que está correndo atrás dele e de seus parentes nas ruas sujas da Praça da Sé do início do século XX. Ele percebe que seu primo Cláudio, o Palhaço Pirulito, está preparando para utilizar um truque de chamas para fazer esse sarnento ficar ___Assustado___ o bastante para ser derrotado nesse conflito. Alexander decide ajudar: ele volta-se para o gato, ganhando tempo e grita _"Na na na na na! Você não me pega, seu saco de pulgas fedorento!"_. Ao _Criar Vantagem_ para seu primo, Alexander garante que o gato vai estar tão ___Bravo___ por ter sido chamado de _"saco de pulgas fedorento"_ que ele não verá quando Pirulito, usando o ___Uísque "Escocês Legítimo"___ que Alexander lhe arrumou e um fósforo velho, cuspir uma Labareda de Chamas no mesmo.
 
Isso é importante de ser notado sempre: a palavra chave de um _Ataque_ é _Prejuízo_. Se a ação não provocar um _Prejuízo imediato_, ela não é um ataque.

## Defesa: evitando ser vítima

Por fim, a ação de _Defesa_ tem algumas características interessantes:

Primeiro, ela é uma ação que visa ___proteger você de todo tipo de coisa ruim que possa ser representada pela mecânica do jogo___. Portanto, em geral ela pode ser vista como uma espécie de "irmã do bem" da ação de Ataque.

Além disso, essa ação pode ter uma característica _reativa_ que a torna interessante: sempre que você for vítima de um Ataque, a não ser quando dito de outra forma (como quando você está sendo surpreendido e o lado atacante tem direito a uma ação de Ataque sem oposição), você pode executar uma ação de Defesa (e apenas de Defesa) sem precisar gastar sua ação do turno.

Por fim, você pode usar ela também contra ações de _Criar Vantagem_ ou de _Superar_. Entretanto, e em especial na primeira, você só pode o fazer se tiver total ciência do que está ocorrendo. Isso é importante, pois, ainda que _reativa_, a ação de Defesa só pode ser usada quando o personagem está _Ciente_ dos acontecimentos ao seu redor. De outra forma, o Narrador deveria tratar as ações contra o alvo como uma Oposição Passiva contra uma perícia adequada.

Dito isso, é possível que um personagem se defenda de ações de _Superar_, ainda que não seja algo muito habitual.

> ___Exemplo:___ O Investigador Profissional Josh McCrock foi contratado por uma pessoa (que ele não sabe que é na verdade a Suprema Sacerdotisa de Avalon ___Morgause___) para investigar os _Fae Guardians_. Ele começa a analisar ocorrências estranhas e lê sobre uma festa a fantasia na _High Society_ de Nova Iorque onde a anfitriã, _Helen Hemingway_, aparentemente surtou e se transformou na Fada Madrinha da Cinderela. O artigo cita o fato de que aparentemente tudo não passou de truques ilusórios, holografia e máscaras de látex de qualidade cinematográfica, mas Josh sabe o suficiente sobre os _Guardians_ e seus Escopos, e não seria de estranhar que uma pessoa sobre o Escopo da Fada Madrinha surtasse e fizesse o que o artigo declara.
> 
> Entretanto, Helen está muito atenta a essa situação (e a outras) e é experiente o suficiente no jogo da publicidade e da fama para saber como apagar seus rastros ocasionalmente. Ela ouve falar sobre esse Investigador e decide colocar-se de maneira ativa para se defender da ação de _Superar_ dele por _Investigar_ que visa apreender mais informação sobre os _Guardians_. Para isso, ela decide utilizar _Comunicação_, usando suas habilidades com a imprensa, para _Defender-se_. Ela cita o fato de que sempre sonhou em ser atriz e que tem os contatos certos junto à Disney, que lhe ajudaram a se tornar a Fada Madrinha durante a festa beneficente, em nome _"da fantasia e do sorriso das crianças menos afortunadas de nossa bela cidade"_. É de certa forma uma mentira, mas ela consegue construir essa narrativa de uma maneira boa o bastante para se Defender nesse caso. Caso contrário, o Narrador poderia tornar simplesmente um teste passivo, talvez contra sua _Comunicação_ ou _Recursos_, ou mesmo menor.

### Defesa não tem efeito cumulativo: o "efeito Espelho"

Se você olhar como as resoluções das ações de Defesa são descritas no [_Fate Básico_](http://fatesrdbrasil.gitlab.io/fate-srd-brasil/fate-basico/as-quatro-acoes/#d-defender), você irá se perguntar se ela é cumulativa em relação aos efeitos descritos nas demais ações.

Na prática, a resposta é _NÃO_: as resoluções são sempre descritas do ponto de vista _de quem as usa_. Elas não possuem nenhum tipo de efeito cumulativo, então ao conceder um _Impulso_ para o seu oponente em caso de empate em sua ação de Defesa após um _Ataque_ bem sucedido por parte do mesmo, o oponente ___não recebe___ dois Impulsos, mas sim apenas um.

Isso se deve graças ao _"efeito Espelho"_ que a descrição da ação de Defesa possui em relação às descrições das demais ações. Na prática, a única novidade da ação de Defesa envolve o Sucesso com Estilo, que veremos abaixo.

> ___Exemplo:___ Helen e Josh empataram na situação anterior. Isso quer dizer que Josh compra a versão de que Helen não é uma criatura estranha, apenas uma ricaça um tanto excêntrica, mas inofensiva... Entretanto, ele ainda possui a ___Intuição de que algo não cheira bem___ na história de Helen: será que esses maquiadores profissionais da Disney são quem ela afirma que são? Talvez compense escavar um pouco mais nessa direção, quem sabe ele não ache algum fio da meada para buscar mais informações.

### Defesa e Sucesso com Estilo

Uma das coisas mais oportunas de se debater na ação de Defesa é quando ocorre o _Sucesso com Estilo_:

> + *Quando for bem-sucedido na sua defesa, você consegue evitar o ataque ou a tentativa de criar vantagem sobre você.*
> + *Quando for bem-sucedido com estilo, encare como um sucesso normal, __mas você também ganha um impulso, o que pode permitir que você vire o jogo.__* (Glifo meu)
 
Lembre-se que Impulso é um Aspecto de curta duração que tão logo usado desaparece. ___Entretanto,___ aqui é importante notar uma situação que pode ser explorada em jogo como uma regra da casa (que de fato é usado em alguns jogos, como _Uprising_): se houver em jogo um Aspecto relevante, ao invés de aproveitar-se tal Impulso, o personagem pode adicionar tal Impulso como um Invocação Gratuita adicional a tal Aspecto. Isso pode ser uma forma interessante de representar, por exemplo, situações onde o personagem provocou uma mudança de ideia em inimigos ou coisas do gênero. 

> ___Exemplo:___ Ao tentar investigar novamente os _Guardians_, dessa vez por meio da questão dos maquiadores profissionais da Disney, Josh falhou em seu teste, sendo Helen _bem-sucedida com estilo_. Ela recebeu portanto um Impulso. Ela poderia o gastar, mas prefere adicionar como uma Invocação Gratuita a um Aspecto já em jogo, relacionado a ___Aliados mundanos contra Morgause___. Ela descreve a seguinte situação ao narrador: _"Assim que Josh chega na Main Street para conversar com minha amiga Anya, eu estou o esperando no lugar combinado, usando orelhas de Minnie, um chapéu de Mickey e dois_ lattes Venti _do Starbucks nas mãos, com um sorriso nos lábios. Quando percebo que ele se assusta eu digo:_ 'Vamos, relaxe um pouco. Não há necessidade de nenhuma atitude complicada. Somos adultos, afinal de contas.' _Ofereço a ele o chapéu e um dos_ lattes _e digo:_ 'Vou responder-lhe todas as perguntas que você precisa e quiser saber, mas já vou lhe adiantar duas coisas: sua empregadora não é quem afirma ser, e não sou uma inimiga, se isso ainda não lhe ocorreu. Agora... Vamos até o _Mad Hatter's Tea Party_, sem muito escândalo, enquanto tomamos um café e respondo suas perguntas. Acho que você vai aprender muito mais do que imaginaria... E talvez do que seria para seu próprio bem.'". E após essa conversa, Josh abandona o caso, tornado-se um aliado dos _Guardians_

## Conclusão

Entender bem as quatro ações é algo de fundamental importância em Fate, já que, como foi enfatizado em vários momentos nessa série, qualquer coisa no Fate praticamente pode ser resolvida por meio dessas ações. Desse modo, saber como elas funcionam e como aproveitar elas para propelir e representar mecanicamente pontos importantes da narrativa é primordial.

Até a próxima e role +4!
<!--  LocalWords:  Overcome Obstacle Bobby Fae Guardians Trolls Ok di
<!--  LocalWords:  Pericles Robert Strombolli training Bulldogs drop
<!--  LocalWords:  Mindjammer RPGs Weird World News Scooby-Doo Élfico
<!--  LocalWords:  Chevalier Lugar-Tenente Morgause Carmaggedon Josh
<!--  LocalWords:  McCrock Avalon High Society Hemingway Disney Main
<!--  LocalWords:  d-defender Uprising Street Anya Minnie Mickey Mad
<!--  LocalWords:  lattes Venti Starbucks Hatter's Tea Party
 -->
