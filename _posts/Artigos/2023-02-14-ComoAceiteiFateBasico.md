---
title: Como aceitei o Fate Básico
layout: post
categories:
 - RPG
comments: true
tags:
  - Artigos
header: no
excerpt_separator: <!-- excerpt -->
---

+ _Por Petri Leinonenb_
+ Postado originalmente em  <https://strangeworlder.medium.com/how-i-learned-to-accept-fate-core-3a578b29c77d>

Um amigo descreveu da melhor maneira suas experiências ao ler e tentar entender o [Fate Básico](http://www.evilhat.com/home/fate-core/) (e aqui vou de certa forma o parafrasear): _"Isso é o que as pessoas comuns sentem quando eles lêem de Dungeons & Dragons pela primeira vez - como jogadores, fomos inicados ao RPG parcialmente por osmose, e aprendemos os segredos de comos as coisas funcionam por meio das pessoas que estavam o tempo todo nisso. Fate Básico é algo tão alienígena para mim quanto D&D é para o cidadão médio."_

Então decidi tentar mudar um pouco isso. Eu narrei sessões de treinamento para pessoas que nunca jogaram Fate Básico. Me foquei em narrar combates, já que é nele que as regras costumam aparecer mais. Estou escrevendo isso para dividir algumas intuições que alcancei após tais jogos com vocês. São algumas ideias que não estão em nenhuma ordem em particular.

 <!-- excerpt -->

## Eles não estão brincando sobre os personagens

O Capítulo sobre os Personagens em Fate descreve os personagens jogadores em Fate como _Competentes, Proativos e Dramáticos_. E isso é o que você precisa ter para que o sistema funcione bem. Fate não funciona bem com coisas chatas, e nem com coisas do cotidiano.

Ele não é a melhor coisa quando você deseja que os jogadores passem tempo argumentando sobre o percentual da taxa de importação que pagarão junta a funcionários da aduana. Exceto se você pretenda tornar isso um momento (melo)dramático.

Faça os personagem e o jogo interessante e você terá uma vida muito mais fácil do que se você teria se tivesse criando pessoas mudanas, reativas e comuns.

## Os dados não importam (até importarem)

As Perícias no Fate, com seu valor numérico e tudo mais, são muito relevantes. Ter uma perícia em +4 é muito diferente do que ter, por exemplo, um +2.

Isso porque, quando você olha as estatísticas do rolamento de 4dF, você tem 62% de chance de obter resultados entre -1 e +1 nos dados. Isso significa que, em por volta de 2/3 das vezes, o seu nível de perícia é um ótimo indicador de quão bem você faz as coisas. Os dados não são coisas com as quais você irá se envolver muito, diferentemente de outros jogos onde você irá ver os dados contar muito.

Dito isso: quando o dado fizer diferença, será de maneira brutal. Um rolamento +4 ou -4 irá mudar completamente a noção que você tinha sobre algo (se você estiver jogando com personagens iniciantes que possuem um nível máximo de perícias em +4)

Mas as Perícias e os dados apenas dão um ponto de partida no qual a coisa vai andar. O jogo de verdade rola nos Aspectos. Aspectos são tudo!

## Aspectos são tudo

Aspectos estão em todo lado e tudo é um Aspecto. Se um quarto está escuro, então deve ter um Aspecto de ___Escuridão___ se isso fizer diferença para o que você está fazendo. Quando a escuridão desaparecer, então não importa quantas Invocações Gratuítas você colocou no Aspecto, ele não está mais lá.

Tudo pode ter Aspectos: personagens, locais, estações do ano, o mercado de ações, a sessão de jogo, a campanha, tudo. Se tiver uma relevância potencial na história, é um Aspecto. Criar e Invocar Aspectos é uma forma de na prática manter as coisas andando.

# Sempre tem algo que você pode fazer

Fate é o único jogo que já encontrei onde os jogadores nunca deveriam pensar no seu turno em "Não vou fazer nada". Sempre tem algo que pode ser feito em Fate.

Se nada mais, eles sempre podem se focar em si próprios, se preparando para a próxima ação: fazendo cálculos mentais, procurando potenciais rotas de fuga ao redor, dando uma ajeitada nas coisas, esfriando a cabeça enquanto fumam um cigarro... Qualquer coisa. E qualquer uma dessas ações importa. Você pode Criar uma Vantagem ao dar aquela fumada e com um rolamento de Vontade obter uma Invocação ou Duas naquele Aspecto novo ___Minhas mãos não estão mais tremendo___.

## Não existem pontos de vida

Isso é uma coisa que é importante entender no combate em Fate: não existe coisas como pontos de vida ou perder vida. Se você tomar um ponto de dano, você tá fora.

_"Mas e quanto a Estresse? E quanto a Consequẽncias?"_ você deve estar perguntando.

“What about Stress? What about Consequences?” you ask.

Eles não são pontos de vida. Você usa eles como forma de previnir sofrer dano, mas eles não são Pontos de Vida. Pode parecer apenas uma questão de terminologia, mas mudar a forma como você pensa sobre eles torna mais fácil entender como eles funcionam e o que eles significam. Enquanto você imaginar eles como Pontos de Vida, você está projetando uma ideia irreal sobre o que eles significam.

Estresse é um para-choque. Ele é um passe livre para fugir do dano. Marcar caixas de Estresse é de graça e, quando o combate acaba, o Estresse volta ao máximo. É um escudo, não sua saúde.

Consequências são mais severas. Algumas vezes as coisas ficam pesadas e você sofre uma. Mas novamente, eles não são uma forma de indicar sua saúde. Você pode sofrer consequências de ações Sociais tanto quanto sofre de ações Físicas. Eles mostram que você se colocou em uma má condição por causa do combate.

Agora, não existe nenhuma boa razão para você não querer usar Estresse e Consequências para negar os danso sofridos quando você está em um combate de verdade. Quando você é _tirado de jogo_ (o termo técnico para quando você é incapaz de previnir aquele último ponto de dano), cabe ao seu inimigo decidir o que vai acontecer. A morte está nas opções. Mas mesmo que seu personagem fique vivo, eles podem desejar que você preencha todas as suas Consequências e mude aquele Aspecto ___Nobre portador da Espada Sagrada de Mu___ para ___Antigo portador da agora destruída Espada Sagrada de Mu___. Sofrer uma consequência é um preço barato comparado a isso.

Agora, dependendo do lugar que você ocupa na ficção, o preço do dano pode ser terrível mas não precisa necessariamente o ser. Eles podem simplesmente forçar você a se render. Eles podem te nocautear. Isso pode ser apenas um leve soco na cabeça, se vocês estiverem treinando. É quem provocou o dano que decide. Mas isso sempre significa que você foi derrotado.

## Conceda com frequência

Conceder é uma das ações mais contraintuitivas que uma pessoa acostumada a formas tradicionais de jogo pode tomar. Se você ainda tem pontos de vida, você continua lutando! O combate é uma caixa preta da qual você emerge como um vencedor ou não emerge! Mas isso é a maneira tradicional de jogo, não a do Fate. Isso tem muito a ver com o que disse anteriormente: não existem pontos de vida. Você sofre um ponto der dano é você está fora, e é _o seu oponente_ quem determina o que é esse _fora_.

Conceder é uma forma de contornar isso. Você escolhe perder essa batalha para não perder a guerra e ainda coloca alguns Pontos de Destino no bolos. E, o mais importante, você perde mas tem alguma agência no que isso significa. Você pode ter bravamente recuar. Você pode ser nocauteado. É um acordo que é melhor que a alternativa.

Conceder é uma ação que deveria ser abraçada plenamente tanto pelo Narrador quanto pelos Jogadores. Faça seu NPC nomeado conceder quando os capangas começarem a cair como mosca. E como jogadores, algumas vezes ser capturado é uma ótima maneira da história seguir adiante.

## Todo mundo tem um motivo

E isso nos leva para o que falamos anteriormente: todos precisam de uma motivação. Se os seus inimigos apenas quiserem matar os personagens jogadores, existe menos espaço para negociar uma concessão interessante do que quando os inimigos estão ali para garantir que os personagens não vão conseguir evidências quanto aos assassinatos que eles estão investigando para a polícia. É mais fácil pensar em termos dos jogadores perderem sem morrerem quando para os inimigos tentar matar eles é a única vitória desejada.

## Do mega-amplo ao super-específico

O Fate é um jogo interessante. Ele tem Aspectos, que são usados de uma maneira que é mais ou menos casual: você usa um Ponto de Destino e diz que seu Aspecto X ajuda na situação atual por causa da razão Y. E então o Narrador sorri e concorda e aceita seu Ponto de Destino. Muitas explicações costumam funcionar. Apenas sorria e concorde.

Perícias, por outro lado, possuem usos muito específicos. Elas são definidas de maneira muito clara: normalmente você tem uma boa noção de que perícia você poderia usar em um rolamento.

E então aparecem as Façanhas, que são super-específicas. São o extremo oposto dos Aspectos no quanto elas podem ser usadas. Elas são extremamente específicas sobre quando podem aparecer em jogo e mesmo assim aplicam-se a perícias específicas em tais situações.

É um jogo bastante interessante. Só para constar.

## Fate funciona bem com Miniaturas e mapas

Claro que eles não são necessários, mas um belo mapa com 3 ou 4 zonas e miniatura nele torna o conflito muito fluído e as coisas muito fáceis de serem representadas. Você consegue visualizar onde todos estão e ter uma ideia do que está acontecendo.

Além disso, uma coisa muito boa sobre mapas no Fate é que os mapas podem ser realmente abstratos! "Essa zona representa o beco, essa outra o alto dos prédiios e essa outra é um bar..."

## Criando vantagens ao notar coisas

Falando sobre todos vendo tudos, eu masso as estatísticas dos caras maus que os personagens vão encarar. Eles veêm suas forças e fraquezas. Ele vêem seus Aspectos. Por que? Eles podem utilizar disparos mirados para Criar Vantagens dessa forma. Quando você sabe como jogador que o capanga tem um "Joelho fraco" como Aspecto, você pode fazer seu personagem Criar uma Vantagem para Notar isso, gerando Invocações Gratuítas nesse Aspecto.

No Fate, é mais fácil Criar uma Vantagem em um Aspecto que já exista no jogo do que criar um Aspecto completamente novo, dito de maneira mecânica (Você vence empates nesse caso). Desse modo é mais fácil se ___ocultar nas sombras___ que já existe na Ficção do que em um ___Esconderijo Perfeito___ que você descobriu do nada.

## Acho que por hoje é só, pessoal:

Provavelmente têm outros comentários, mas acho que por agora chegamos a uma conclusão. Espero que isso ajude de alguma forma.

