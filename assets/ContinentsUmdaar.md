# Continent

|        | `0`          | `+`          | `++`         | `+++`        | `++++`       |
|-------:|:------------:|:------------:|:------------:|:------------:|:------------:|
|    `0` | Core         | Freelands    | Freelands    | Wildernexus  | Dread Domain |
|    `-` | Freelands    | Freelands    | Wildernexus  | Dread Domain |              |
|   `--` | Freelands    | Wildernexus  | Dread Domain |              |              |
|  `---` | Wildernexus  | Dread Domain |              |              |              |
| `----` | Dread Domain |              |              |              |              |

# Sites

|          **Kind** | **Aspect/Skill/Level**                                                                                      |
|------------------:|-------------------------------------------------------------------------------------------------------------|
|      High Concept | A basic description of the location and core idea behind the location.                                      |
|        Population | What are some of the bioforms?                                                                              |
|            Values | What virtues or values are important?                                                                       |
|           Trouble | What is the location's tragic flaw? What could make it collapse?                                            |
|            Status | What is the current situation?                                                                              |
|           Aspects | Miscellaneous, double-sided aspects describing their weaknesses, strengths, unique features, and landmarks. |
| Skills (Optional) | site's size and scope through a skill rating                                                                |
|  Power (Optional) | Sites have at least one skill, which means they possess many people who excel at the profession or ability. |

