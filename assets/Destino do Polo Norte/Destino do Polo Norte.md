---
tags: Dry Fate, Hack, Natal
---

# Destino do Polo Norte

+ Baseado em informações e _lore_ do _site_ [Portable North Pole](https://www.portablenorthpole.com). Todos os direitos reservados. O uso desse módulo deve ser feito apenas para uso pessoal... A não ser que você queira entrar para a Lista dos Malvados

## A Vila de Natal e seus Elfos!

Em um local ao Norte, bem perto do Polo Norte, vive o Papai Noel. Desde eras antigas, ele trabalha em uma missão continua de, todos os anos, presentear crianças que tenham sido boas. Para algumas, essa pode ser até mesmo a única e melhor esperança de algum carinho no ano inteiro.

Entretanto, ele não conseguiria realizar tal missão sozinho. Para isso, ele conta com a ajuda da Mamãe Noel, sua companheira inestimável, e de suas várias renas, que por meio de um _liquen_ encantado que é colocado em sua comida podem voar pelos céus.

Ainda assim, com a grande quantidade de crianças que existem no mundo,  a tarefa poderia ser impossível de ser executada: como construir brinquedos para todas as crianças do mundo, ver quais são as Boas ou Ruins para que recebam aquilo que queiram (as Boas) ou aquilo que necessitam (as Ruins) e preparar tudo para a Viagem Anual pelo Mundo?

Ninguém lembra exatamente como começou, nem mesmo Sanoma, o Elfo Porta-Voz e um dos mais antigos em atividade com mais de 600 anos (ainda que diga não ter pareça mais velho que 110), mas o que se sabe é que, todos os anos na época do natal, das bonitas _poinsettias_, as Flores de Natal que a Mamãe Noel (e ocasionalmente outros Elfos) cultivam na Estufa Encantada (ocasionalmente em outros lugares) na Cidade de Natal, nascem ___Elfos, criaturas pequenas e benfazejas___ cuja função é ajudar o Papai Noel nessa função de ___oferecer esperança e alegria ao mundo___. 

Os Elfos nascem quando as _poinsettias_ se abrem, ainda que nem toda _poinsettia_ tenha um Elfo dentro. Normalmente nasce apenas um Elfo por _poinsettias_, raramente nascendo gêmeos. Há apenas um caso de gêmeas nascidas de uma mesma _poinsettia_. Eles são cuidados pelos demais até que cresçam o bastante para começarem a ser educados no que se espera deles. 

Inicialmente, após aprenderem o básico do que se espera de um Elfo, como construir brinquedos, usar pequenos truques mágicos e assim por diante, além obviamente de falar, fazer contas e das histórias e lendas envolvendo o Natal, os pequenos Elfos passam a conviver com os vários Clãs de Elfos, até que tenham aprendido tarefas mais básicos e descobertos aptidões para as quais estão mais preparados. Em especial, nesse período costumam passar bastante tempo com os _Fazem-Bem_, para aprenderem na prática a diferenciar o Bem do Mal (o que os _Fazem-Bem_ fazem com maestria).

Os Elfos são organizados em Clãs, mais associados a suas aptidões que ao nascimento ou qualquer coisa que o valha. O exemplo maior é o das citadas trigêmeas, Nörra (uma Holhooja), Nikäri (Inventineira) e Nöli (uma Carteirista), onde cada uma foi para um dos Clãs dos Elfos. Os Clãs se colaboram mutuamente, ainda que disputas e competições saudáveis possam ocorrer entre os Elfos (em especial para isso existem os Jogos de Verão).

Os Seis Clãs dos Elfos são:

+ ___Carteiristas:___ são responsáveis por lerem as Cartas (e _e-mails_, nessa era mais moderna) contendo os pedidos das crianças para o Papai Noel. Eles literalmente leem _CADA. CARTA._, repassando ao Papai Noel aqueles casos mais complicados. Com isso, eles conhecem as histórias de muitas, muitas, ___MUITAS___ crianças, sendo eles grandes contadores de histórias justamente por isso. Sanoma, o Elfo Porta-Voz, é oficialmente um Carteirista, portanto acredita-se que sejam eles também quem mantenham o contato externo entre a Vila de Natal e o Mundo de Fora.
+ ___Construtores:___ possuem uma função muito importante, já que são eles quem constroem os brinquedos que serão levados pelo Papai Noel para as crianças de todo mundo. Embora possam ocasionalmente rivalizar com os Inventineiros, os Construtores sabem da importância dos mesmos e vice-versa. Além disso, são considerados os Elfos mais hábeis na Magia dos Elfos, já que são capazes de criar facilmente magias novas, que usam para encantar alguns brinquedos ocasionalmente.
+ ___Fazem-bem:___ são a maioria dos Elfos, e são responsável por observar as Crianças (e Famílias) e registrar o que elas fazem de Bom e de Ruim no Livro sobre as mesmas. Isso os torna especialmente hábeis em discernir as pessoas. Além disso, são os Elfos com maior conhecimento sobre o Mundo Fora da Vila do Natal. Costumam também utilizar sutilmente a magia para tentar assegurar que aqueles a quem supervisionam fiquem na Lista dos Bonzinhos. Entretanto, também são conhecidos por pegarem pequenos _mementos_ dessas crianças, em especial meias. Também costumam apreciar o sabor de menta de pastas de dente, sendo uma forma de descobrir a presença de Elfos em uma casa.
+ ___Inventineiros:___ são inventores e pesquisadores, capazes de ___descobrir e criar as coisas mais incríveis___. Entretanto, facilmente são pegos metendo os pés pelas mãos quando as invenções ficam descontroladas.
+ ___Holhooja:___ ou _Céu-Abertos_, são responsáveis por cuidarem da parte de fora da Vila de Natal, sendo habituados a viver fora da Vila de Natal, mesmo no frio inclemente. São responsáveis por cuidar tanto das Renas, a quem alimentam com um _liquen_ mágico que encontram nos Bosques próximos para fazer elas voarem, e também das Corujas de Inverno que ajudam a coletar o pirlimpipim, um pó que cai do céu quando estrelas cadentes caem na Terra e que é muito importante por ser usado em qualquer magia que o Papai Noel ou os Elfos precisem. Devido a isso, são muito fortes e habituados ao ar livre, além de serem capazes de falar com animais (de verdade!);
+ ___Kapunki:___ ou _Vilarejinos_ (embora gostem de ser chamados pelo nome _Kapunki_) são muito bonzinhos e cheios de ideias. Sua função é manter tudo na Vila de Natal funcionando direitinho. São eles quem fazem a comida, mantém tudo limpo e organizado e em devido funcionamento (com a ajuda dos demais Elfos, claro). Uma responsabilidade importante dos _Kapunki_ é manter organizada a Biblioteca onde todos cópias dos Livros que os Fazem-Bem registram são mantidas com as informações mais atualizadas. Isso ajuda o Papai Noel e os demais Elfos na mais diversas funções. Além disso, trabalham em conjunto com a Mamãe Noel nas Estufas Encantadas para garantir que as _poinsettias_ brotem e novos Elfos nasçam. Atualmente um dos _Kapunki_ mais conhecidos é _Gordot_, o responsável pela Cozinha.

A vida de um Elfo do Papai Noel é em geral bem ordeira, trabalhando nas atividades do seu Clã (ou naquele no qual está no momento, caso seja um _Em Treinamento_), mas todos os anos existem alguns grandes eventos:

+ _Páscoa e Halloween_ são comemorados na Vila de Natal como em qualquer outro lugar do mundo. Em especial, tenta-se fazer com que tenha muita comida para festejar essas datas, assim como o Aniversário da Mamãe Noel e do Papai Noel;
+ _Os Jogos de Verão_ são importantes por serem um momento de relaxamento para os Elfos como um todo: os Inventineiros e Construtores trabalham para criar todo tipo de jogos e brincadeiras, onde todos os elfos podem disputar. Normalmente os Holhooja vencem muitas delas, que costumam ocorrer em uma estepe próxima à Vila de Natal. Além disso, as renas também participam de jogos especialmente preparados para testar coisas como a capacidade de voar, bravura (com todos os jatos por aí, é necessário), integridade, coração generoso e afim, para que o Papai Noel, junto com os Holhooja possam escolher quem serão as renas escolhidas para _A Grande Viagem_ daquele ano. Para os Holhooja, é um grande orgulho uma rena treinada por eles ser escolhida para _A Grande Viagem_
+ _A Grande Viagem_ do Dia de Natal é o momento mais importante para a Vila, quando todos os brinquedos (e surpresas para os Malvados) estão prontos e O Papai Noel pode iniciar sua viagem ao redor do mundo distribuindo os presentes. Pode-se dizer que, no fim das contas, a função de todos na Vila de Natal é garantir que ___A Grande Viagem ocorra sem percalços___. Para isso que os Fazem-Bem coletam as informações sobre as Crianças no livro, os Carteiristas leem as cartas, os Holhooja treinam as Renas e as Corujas, os Construtores constroem os Brinquedos, os Inventineiros criam novos brinquedos e outras coisas, os Kapunki mantém tudo em ordem e cuidam das _poinsettias_ junto com a Mamãe Noel. Nunca o Papai Noel falhou, graças ao trabalho de todos, e já teve várias vezes em que ocorreram problemas... Sanoma pode contar sobre aquela _"Nevasca terrível de Dezembro de 1747!"_

## Criando Personagens

O Destino do Polo Norte prevê o uso do [_Dry Fate_](http://fabiocosta0305.gitlab.io/fate/fate%20b%C3%A1sico/fate%20acelerado/hacks/minimalista/DryFate/), por ser um jogo mais simplificado e divertido. Entretanto, ele também tem suas regras específicas.

Os ___Aspectos___ de Cada Elfo será determinado a partir da Resposta às _Perguntas_ abaixo

1. Eu sou um Elfo do Clã...
2. Eu sou um Elfo...
3. Eu sou conhecido por...
4. Eu quero...
5. Eu gosto de...

Lembrando que a primeira pergunta tem que ser respondida dando o nome de um dos Clãs ou _Em Treinamento_. 

Além disso, cada personagem pode Escolher duas Competências em _Razoável (+2)_, e duas em _Regular (+1)_. O Aspecto _"Eu sou um Elfo do Clã..."_ é automaticamente definido com uma Competência _Boa (+3)_, ou seja, tudo que for necessário rolar para suas funções enquanto Elfo, o jogador rola usando _Bom (+3)_ como Competência. 

> Ricardo vai criar seu Elfo para um jogo onde seus amigos Leonardo, Ana e Mirela vão estar juntos. Eles vão criar Elfos que se conhecem a bastante tempo, já que todos nasceram na mesma época das _poinsettias._ Como eles pretendem explorar melhor a Vila do Natal e as Enrascadas que os Elfos arrumam, nenhum deles decidiu ser um _Faz-Bem._ Ele está na dúvida entre jogar com um Carteirista ou um Kapunki, já que gosta da ideia de ser um personagem mais de suporte aos demais. Como Mirela quer jogar com um Carteirista, Sören, Ricardo decide então criar uma Kapunki, Syllia.
> 
> Com isso, seu primeiro Aspecto está definido, mostrando que Syllia é ___Um Elfo do Clã Kapunki___. Isso vai dar a Syllia vantagens ao manter coisas organizadas, mas vai colocar complicações, em especial caso tenham que sair da Vila de Natal, de onde ela saiu pela última vez enquanto estava em Treinamento... Em 1822! Além disso, Syllia recebe a **Façanha Exclusiva:** ___Identificando Padrões___ que irá a ajudar a perceber coisas erradas. Entretanto... Ela não consegue lidar bem com desordem!
> 
> Ricardo então decide definir o Aspecto _"Eu sou um elfo..."_ para dizer no que Syllia se diferencia dos demais Kapunki. Ele decide colocar o Aspecto como _"Sabichona e Sociável"_. Essas não são características muito comuns nos Kapunki, mas ele pensa que Syllia é alguém que conversa com todo mundo, mas que quando revela que alguém esqueceu uma meia ou roupa fora do lugar gosta de "pegar no pé" um pouco além da conta, por lembrar onde as coisas estão graças a sua esperteza.
> 
> Em seguida, ele escolhe responder _"Sou conhecido por..."_: como Syllia tem "um pezinho" junto aos Carteiristas, e provavelmente aos Faz-Bem, ele decide que Syllia é conhecida por _"Saber onde estão todos os Livros e Cartas catalogados na Biblioteca do Papai Noel"_, um fato que impressionou até mesmo Gordot, que gosta da ajuda de Syllia, o que livra ele para o que ele gosta mais de fazer: comida!
> 
> Em seguida, ele escolhe definir o Aspecto _"Eu gosto de"_, e decide que pode ser legal ligar-se a um dos personagens. Ricardo conversa com Mirela para ver se seria legal que exista uma relação de amizade um pouco maior entre Sören e Syllia. Mirela acha bacana, ainda mais que Sören é um tanto certinho, o que é uma característica mais comum nos Kapunki que nos Carteiristas, o que mostra que Sören e Syllia possuem interesses compartilhados ao ponto de que um poderia estar no lugar do outro. Então ele define que Syllia Gosta de _Conversar sobre o Mundo Lá Fora com Sören_. Sören tem milhares de histórias para contar das cartas que leu como Carteirista, e a Kapunki é o melhor ouvido que Sören poderia querer
> 
> Por fim, o último Aspecto que está em aberto é _"Eu desejo..."_, o que Syllia quer, além do que todo Elfo quer, que é que a _Grande Viagem_ ocorra de maneira correta. Ricardo decide que Syllia deseja _"Poder ler tudo o que for possível sobre O Mundo Lá Fora"_. Na prática, Syllia poderia ser uma Carteirista ou Faz-Bem muito boa, mas seu desejo de ficar em um local mais confortável e seguro falou mais alto, o que a levou a se juntar aos Kapunki. Mas ainda assim, sempre que pode, ela é vista lendo os relatos do Mundo Lá Fora acumulados na Biblioteca, seja nos Livros ou nas Cartas. Sanoma admira isso na jovem Elfa.
> 
> Para encerrar, Ricardo deve distribuir os níveis de Competência entre seus Aspectos. Como visto anterior, ele tem o Aspecto ___Eu sou um Elfo do Clã Kapunki___ em __*Bom (+3)*__. Ele decide que quer que o fato de Syllia ___"Saber onde estão todos os Livros e Cartas catalogados na Biblioteca do Papai Noel"___ e _**"Sabichona e Sociável"**_ sejam suas Competências __*Razoáveis (+2)*__, o que faz com que seus outros dois Aspectos ___"Poder ler tudo o que for possível sobre O Mundo Lá Fora"___ e _**"Conversar sobre o Mundo Lá Fora com Sören"**_ sejam __*Regulares (+1)*__. Isso mostra que ela não é tão chata quanto aparenta, ainda que ocasionalmente pegue no pé dos seus amigos, além de ainda ter muito o que ler sobre O Mundo Lá Fora. 

## O Aspecto ___Eu sou um Elfo do Clã___

O Aspecto _"Eu sou um Elfo do Clã"_ substitui o ___Conceito___ do Dry Fate e serve para indicar o Clã ao qual o Elfo faz parte. Isso libera alguns usos especiais do Aspecto, além de uma Façanha Exclusiva, mas traz consigo também problemas que outros elfos podem não ter.

+ Todo Elfo pode invocar seu  Aspecto de ___Elfo___
  + Para demonstrar carinho e bem-fazer e com isso conseguir ajuda nos momentos mais improváveis
  + Realizar Feitos de Magia, em especial envolvendo se esconder
  + Conhecer Fatos e lendas do Natal
  + Reconhecer o Bem e o Mal
+ Todo Elfo pode ter seu Aspecto de ___Elfo___ Forçado:
  + Quando ser bonzinho e carinhoso for uma péssima ideia
  + Terem certos tiques que possam indicar sua presença no local (roubar meias, por exemplo)
  + Fazerem pequenas trapalhadas nos piores momentos possíveis
  + Ao tentar ajudar, meter os pés pelas mãos.

Além disso, cada clã possui uma ___Façanha Exclusiva___ que representa a capacidade daquele Clã em especial. Alguns Elfos podem possuir mais de uma delas, por já ter passado por mais de um Clã durante suas longas vidas, mas personagens recém-criados não podem ter mais de uma delas:

+ Carteiristas
  + ___Podem Invocar seu Aspecto para:___ lembrar de histórias e cartas
  + ___Podem ser Forçados por causa de:___ divagar nos piores momentos
  + **Façanha Exclusiva:** ___Histórias de Vida___ - +2 ao reconhecer a necessidade de uma pessoa baseando-se em alguma coisa que já leu
+ Construtores
  + ___Podem Invocar  seu Aspecto para:___ construir coisas úteis e/ou divertidas, com as mãos ou por magia
  + ___Podem ser Forçados por causa de:___ Mexerem no que não devem
  + **Façanha Exclusiva:** ___Tenho o que precisamos!___ - +2 ao construir alguma coisa útil para _Superar_ algum obstáculo
+ Inventineiros
  + ___Podem Invocar seu Aspecto para:___ inventar coisas incríveis
  + ___Podem ser Forçados por causa de:___ invenções descontroladas
  + **Façanha Exclusiva:** ___Tenho uma ideia!___ - +2 ao ___Criar Vantagens___ envolvendo mecanismos 
+ Fazem-bem
  + ___Podem Invocar  seu Aspecto para:___ revelar conhecimento sobre o mundo fora da Vila de Natal
  + ___Podem ser Forçados por causa de:___ se envolverem em encrencas no mundo
  + **Façanha Exclusiva:** ___O Livro!___ - cada Faz-Bem possui um Livro onde registra todas as coisas boas (ou não) que aquelas crianças que ele observa fez (e demais pessoas próximas). +2 para tentar prever o comportamento dessa criança ou entender mudanças de comportamento.
+ Holhooja
  + ___Podem Invocar para:___ ficar à vontade ao ar livre
  + ___Podem ser Forçados por causa de:___ desconforto com espaços mais apertados
  + **Façanha Exclusiva:** ___Falar com Animais (Literalmente):___ Holhooja são adeptos em falar com todo tipo de animal selvagem ou domestico. Portanto, podem sempre conversar com eles. Entretanto, os animais não saberão responder nada que não saibam ou não entendam (por exemplo, sobre magia ou coisas tecnológicas)
+ Kapunki
  + ___Podem Invocar  seu Aspecto para:___ criar situações agradáveis 
  + ___Podem ser Forçados por causa de:___ quando tudo está bagunçado
  + **Façanha Exclusiva:** ___Identificando Padrões___ - Kapunki costumam ser elfos que percebem quando algo está fora de lugar. +2 ao _Superar_ obstáculos, reconhecendo padrões que mostrem  coisas que estão fora do lugar
+ Em Treinamento
  + ___Podem Invocar seu Aspecto para:___ demonstrar potencial para algum clã, ver soluções onde ninguém mais esperava
  + ___Podem ser Forçados por causa de:___ inexperiência e meter os pés pelas mãos (em especial quando não supervisionados)
  + **Façanha Exclusiva:** ___Ainda aprendendo___ - Pode utilizar as Façanhas de todos os clãs. Entretanto, só podem utilizar cada uma delas _uma vez por sessão_, e com um bônus reduzido para +1. 

## Façanhas Adicionais

Personagens ainda podem comprar Façanhas Adicionais, como indicado na [sessão específica do Dry Fate](http://fabiocosta0305.gitlab.io/fate/fate%20b%C3%A1sico/fate%20acelerado/hacks/minimalista/DryFate/#façanhas). Entretanto, como já possuem uma Façanha Exclusiva pelo Clã, eles podem comprar apenas uma outra Façanha antes de reduzir sua Recarga. A Recarga inicial dos Elfos é de 3, como normal pelo Dry Fate. Da mesma forma que no Dry Fate, pode-se reduzir a Recarga para comprar Façanhas Adicionais, com a mesma limitação de não poder baixar para menos de 1 a Façanha.

> Ricardo pensa em mais Façanhas para Syllia, e ele decide que quer uma Façanha que ajude ela no dia a dia enquanto Kapunki, mas que reflita o gosto dela por conhecer coisas. Ele decide então que Syllia ___Sabe Onde Tudo Fica na Vila de Natal___, dando a ela +2 para se deslocar dentro da Vila. Entretanto, se falhar no Teste, ela se enfiou em algum lugar onde não deveria
> 
> + ___Sabe Onde Tudo Fica na Vila de Natal:___ Syllia conhece tudo sobre a Cidade de Natal, recebendo +2 ao Superar Obstáculos ao se deslocar dentro da Vila. Entretanto, se falhar no Teste, ela se enfiou em algum lugar onde não deveria ou arrumou alguma outra enrascada!!!

### Syllia, a Kapunki

#### Aspectos

+ ___Eu sou uma Elfa do Clã Kapunki___ _Bom (+3)_
+ ___Eu sou uma Elfa Sabichona e Confiável___ _Razoável (+2)_
+ ___Eu sou conhecida por Saber onde estão todos os Livros e Cartas catalogados na Biblioteca do Papai Noel___ _Razoável (+2)_
+ ___Eu desejo Poder ler tudo o que for possível sobre O Mundo Lá Fora___ _Regular (+1)_
+ ___Eu gosto de Conversar sobre o Mundo Lá Fora com Sören___ _Regular (+1)_

#### Façanhas

+ **Façanha Exclusiva:** ___Identificando Padrões___ - Kapunki costumam ser elfos que percebem quando algo está fora de lugar. +2 ao _Superar_ obstáculos, reconhecendo padrões que mostrem  coisas que estão fora do lugar
+ ___Sabe Onde Tudo Fica na Vila de Natal:___ Syllia conhece tudo sobre a Cidade de Natal, recebendo +2 ao Superar Obstáculos ao se deslocar dentro da Vila. Entretanto, se falhar no Teste, ela se enfiou em algum lugar onde não deveria ou arrumou alguma outra enrascada!!!

## Magia dos Elfos

A Magia Natalina, ou Magia dos Elfos, é uma forma de magia que os elfos, o Papai Noel e a Mamãe Noel podem usar para fazer truques que os ajudem nas sua missão. É por meio desse tipo de magia, por exemplo, que o Papai Noel pode entrar em qualquer casa simplesmente se espremendo em chaminés (ou outras entradas), não importem o quão pequenas sejam.

Embora a Magia Natalina aparentemente seja superpoderosa, já que não existem limites aparentes para o que ela pode fazer, na prática ela é bem restrita:

Primeiro, a Magia Natalina tende a ser ___discreta___: de fato, a maior parte dos truques que podem ser feitos com a Magia Natalina envolvem ___não ser percebido___. Coisas como induzir ao sono, diminuir de tamanho, eliminar ruídos ou ficar invisível são parte da seara de coisas que podem ser feitas pela Magia Natalina. Coisas espalhafatosas não costumam ser parte da Magia Natalina, e apenas os melhores na Magia Natalina são capazes de fazer tal coisa.

Segundo, a Magia Natalina ___não é uma magia agressiva___. Ainda que existam pequenos truques que Elfos possam fazer para se proteger no Mundo Lá Fora, como usar uma bola de luz intensa para cegar um Cachorrão que esteja os perseguindo, ainda assim dificilmente Elfos seriam capazes de provocar dano aos outros.

Terceiro, a Magia Natalina ___é em um foco menor___. Dificilmente o uso de Magia Natalina poderá envolver mais que uma dúzia de alvos (uma família grande, por exemplo), já que Elfos utilizam sua Magia muito mais para passar desapercebido que qualquer outra coisa. Claro, existe a possibilidade de usar determinadas mágicas que fogem um pouco das ideias comuns dos Elfos (por exemplo, provocar um show pirotécnico de fogos de artifício mágicos), mas mesmo assim isso seria muito complicado.

Quarto, todas as formas de Magia Natalina são dependentes do ___pirlimpimpim___, um pó mágico obtido quando estrelas cadentes entram no céu. Isso é tão importante que a própria localização da Vila do Natal, lar do Papai Noel e dos Elfos, deve-se ao fato de ser um dos pontos onde é mais fácil obter o pirlimpimpim, em especial com a ajuda das Corujas da Neve. Normalmente, a quantidade usada é muito pequena, servindo apenas como forma de canalizar a magia. Entretanto, um Elfo ___Sem Pirlimpimpim___ pode-se ver em problemas, já que dificilmente conseguiria fazer qualquer magia sem o mesmo.

Dito isso...

Em termos mecânicos, realizar uma magia é normalmente um teste de ___Superar obstáculo___ ou ___Criar Vantagem___, com uma dificuldade inicial de __*Razoável (+2)*__, modificada pelos fatores abaixo. Se houver um alvo involuntário, ele tem direito a resistir normalmente, rolando contra o resultado do alvo. Entretanto, a dificuldade mínima sempre será determinada como mostrado, e em caso de falha no teste, um Sucesso a Custo pode implicar na Magia ficar ___Destrambelhada___, seus efeitos potencializados ou tornados selvagens por algum motivo.

|                                                                            ***Circunstância*** | ***Modificador*** |
|-----------------------------------------------------------------------------------------------:|-------------------|
|                                                                        O alvo é o próprio Elfo | -2                |
|             O(s) alvo(s) é(são) outra(s) criatura(s) com contato mágico (outro Elfo, uma Rena) | -1                |
|                                                        A magia é espalhafatosa de alguma forma | +2                |
|                                          A magia pode provocar dano físico ou mental duradouro | +2                |
|                                                        A magia tem como objetivo proteger algo | -2                |
|                                                                                Múltiplos alvos | +1                |
| A magia vai afetar um número grande de alvos ou uma região grande (maior que uma casa/família) | +2                |
|             A magia vai afetar um número enorme de alvos ou região imensa (uma cidade inteira) | +4                |
|                                            O alvo está ciente do Elfo (só válido para humanos) | +2                |
|                                                   O alvo é voluntário (só válido para humanos) | -2                |
|                              Ao menos um alvo não acredita (ou acredita ___demais___) em Magia | +4                |

Pode ser usado qualquer Competência adequada para o teste, mas normalmente você irá usar o ___Bom (+3)___ por ___Sou um Elfo do Clã...___

Toda Magia Natalina pode ser _Instantânea_ (___Superar___) ou pode permanecer como um Aspecto (___Criar Vantagem___)

> OK... Syllia realmente achou que era uma boa ideia acompanhar Lanky, o _Faz-Bem_ em suas visitas regulares a crianças com alguns novos Elfos...
> 
> Mas isso foi até ela, Märko e Guildë se perderem em Tóquio!
> 
> Syllia, como a mais velha dos Elfos ali (Märko e Guildë ainda estão _Em Treinamento_), decide conduzir os mesmos, mas antes ela resolve colocar todos sobre uma Magia para ___esconder a presença___ dos mesmos. Dito isso, ela começa a preparar a magia e pegar de uma bolsinha um pouco de pirlimpimpim e entoa rapidamente algo que ela aprendeu com Timmennen, sua amiga Construtora.
> 
> Syllia tem como alvos ela própria, Märko e Guildë (-2 por si mesma, +1 por por múltiplos alvos, -1 por esses outros alvos serem com contato mágico). A magia não necessariamente vai os proteger, apenas os esconder, então nenhum dos outros modificadores se aplica. A dificuldade, somada ao ___Razoável (+2)___ padrão (-2 de modificadores final),  é __*Medíocre (+0)*__. Ela rola contra sua Competência enquanto ___Uma Elfa do Clã Kapunki___ ___Bom (+3)___ e consegue um ( )(-)( )( ) para um Esforço final __*Razoável (+2)*__. Ela percebe a pequena aura formando-se ao redor deles, e percebe, olhando para o lado que ela vê Märko e Guildë como "sombras", o que indica que eles estão ___Invisíveis___ para qualquer outra pessoal.
> 
> _"Tudo bem..."_ ela diz baixinho _"Agora nada de pânico: vamos nos encontrar com Lanky ou achar uma forma de avisar a Vila que nos perdemos... Até lá, vamos tentar encontrar um local para ficarmos."_ enquanto eles andam pela cidade apinhada de gente
 
### Furando a Ilusão

Ocasionalmente, a Magia Natalina pode falhar: em especial contra pessoas que não acreditam (ou acredita ___demais___) em Magia, a Magia Natalina pode falhar.

Isso é mostrado da seguinte forma: qualquer pessoa que ostensivamente procure coisas estranhas pode, com um rolamento de _Superar_ bem-sucedido contra o _Esforço_ obtido na magia  para _Furar a Ilusão_, vendo um Elfo escondido, ou fazendo uma ilusão se desfazer. 

Para Furar a Ilusão, entretanto, isso tem que ser feito de maneira ___Ativa___: não é possível Furar a Ilusão "apenas por que sim".

> ___Exemplo:___ enquanto Märko e Guildë estão devidamente escondido em um canto de um armário em uma casa em Shibuya, Syllia decida aproveitar que a casa parece meio quieta e observar a mesma, para vê se encontra comida e alguma forma de entrar em contato com a Vila de Natal.
> 
> Entretanto, ela não percebeu que Märko deu uma bandeira, ao roubar uma meia de alguém da casa para sua ___Coleção de Meias de Todos os Lugares do Mundo___!
> 
> Com isso, a dona da meia, uma garota chamada Mariko ficou ciente da potencial presença de Elfos e decidiu ver o que está acontecendo. Como ela ___Acredita em Magia___, ela pode ser alguém capaz de _Furar a Ilusão_ da Magia Natalina que Syllia colocou sobre todos. 
> 
> E exatamente no momento em que Mariko está voltando para o quarto, Syllia saiu.
> 
> O Narrador, rolando por Mariko, vai tentar Furar a Ilusão contra uma dificuldade ___Razoável (+2)___ (o Esforço obtido na Magia de Invisibilidade que Syllia usou anteriormente). Mariko é uma ___Garotinha Muito Perceptível___ com uma Competência ___Razoável (+2)___ nisso, e consegue um (+)( )(+)(+) para um resultado ___Excepcional (+5)___ contra o ___Razoável (+2)___ da Magia Natalina de Syllia! Um Sucesso com Estilo! Ela vê Syllia em seu 1,30m de altura, com cabelos cobreados e sardas por todo o rosto e exclama: "Henge! _Um Elfo!_"
> 
> Syllia se assusta, e percebe que a garota a notou... Agora... Como garantir que as coisas não vão ficar ainda mais bagunçadas?

### Magia Destrambelhada

Um Elfo que tente realizar uma magia e falhe tem a opção de um Sucesso a Custo, que é tornar a Magia ___Destrambelhada___. Uma Magia Destrambelhada é muito mais potente que a magia deveria ser, mas de uma maneira ruim: o Elfo não possui o mínimo controle de que diabos está acontecendo, podendo fazer com ele até mesmo corra muito perigo. Afinal de contas, a Magia não é uma coisa para ser usada a toa!

> Agora Syllia se complicou de vez: ela teve a ideia de fazer um favor para Gordot, o Chefe da Cozinha da Vila, indo buscar alguns ingredientes com Fësto, um Faz-Bem que estava em Nova Iorque, mas acabou saindo no lugar errado e indo parar em uma Escola cheia de Crianças!
> 
> Ela decide usar um Truque de _Piscar_, fazendo com que ela pareça estar piscando para as crianças, de modo que elas imaginem que elas estão vendo coisas. Entretanto, ela está em uma grande Enrascada, já que está em uma sala cheia de crianças (+4 - número enorme de alvos) que acreditam ___demais___ em Magia (+4) e que estão cientes da presença de Syllia (+2), dando uma dificuldade de +10, ___Acima de Lendário!___ Nem o Papai Noel se coloca em tamanha enrascada!
> 
> Syllia decide rolar, e paga 1 Ponto de Destino para invocar o Fato de ser ___Uma Elfa do Clã Kapunki___ e outro para lembrar de coisas sobre Magia e crianças que ___conversou com Sören___, o Carteirista. Isso eleva sua competência para __*Épica (+7)*__. Ainda assim ela precisa de um rolamento muito bom para conseguir.
> 
> E ela não consegue: o rolamento (+)(-)(+)(+) é bom (+2 para um Esforço Final ___Acima de Lendário___ +9), mas não o bastante. Ela não tem mais Pontos de Destino e não tem como aceita uma Falha... A última coisa que precisa é de um bando de crianças que podem tentar aprontar coisas bem complicadas com ela (e Lanky conta cada história que dá medo sobre crianças Malvadas)!
> 
> Ela então decide recorrer a um Sucesso a Custo e o Narrador decide que a Magia de Piscar ficou Destrambelhada. Ela começa a ser vista em todos os cantos pelas crianças... Porque a magia alterou tanto o deslocamento de Syllia que ela ___não consegue andar em linha reta___, quase como se ela fosse a Vanellope de Detona Ralph quando bugava! Ela vai ter muitos problemas!
> 
> Ela ao menos espera que seja lá o que Gordot esteja preparando, valha a pena!

Uma Magia pode ficar ___Destrambelhada___ também se o alvo tiver um Sucesso com Estilo na Defesa contra a magia ou ao _Furar a Ilusão_ e for interessante fazer a magia ficar Destrambelhada.

> No exemplo que mostramos acima, Mariko teve um Sucesso com Estilo. O Narrador poderia fazer a Magia de Syllia ficar Destrambelhada, a tal ponto que nem mesmo Märko e Guildë a localizassem depois, ou fazer com que Mariko conseguisse ver ___qualquer Elfo da Região___, ao menos por algum tempo. Entretanto, não havia a necessidade de o fazer. O máximo que ele decide deixar é um _Impulso_ para Mariko saber que ___Tem Elfos na Minha Casa!___
 
### Desfazendo ("derrubando") a Magia

Existem duas Formas de Desfazer-se a Magia:

A primeira, onde o Elfo simplesmente a Desfaz naturalmente, declarando sua intenção. Não é necessário nenhum rolamento.

A segunda, onde ela é "derrubada" por alguém Furando a Ilusão. Isso é o resultado natural do teste de Superar Obstáculo removendo o Aspecto.

A diferença na Segunda é que, caso hajam múltiplos alvos da magia, apenas aqueles que tiveram a Ilusão Furada perdem o Aspecto.

> Syllia reflete rapidamente: a reação de Mariko não pareceu muito hostil, ou tendendo à hostilidade. Ela decide que a coisa já meio que subiu no telhado, então é melhor colocar Mariko do seu lado e depois ver como lidar com ela (já que Elfos não deveriam se revelar a humanos). Ela decide desfazer totalmente a magia, e ela vê que Märko está segurando a meia de Mariko que ele roubou...
> 
> _"Märko, isso não se faz."_ ela diz, repreendendo-o na frende de Mariko _"Perdoe o Märko, ele ainda está Em Treinamento. Essa é Guildë e eu sou Syllia... E sim, somos Elfos da Vila de Natal. Nos perdemos do nosso amigo Lanky, e temos que o encontrar até amanhã, ou ficaremos perdidos aqui. Será que você pode nos ajudar?"_ pergunta Syllia, já sabendo que está em uma incrível enrascada, ainda mais que Kapunki não são preparados para essas situações fora da Vila!
 
### Algumas Magias simples (apenas descrições) que Todo Elfo Deve Saber

+ ___Invisibilidade:___ Basicamente criar uma aura mágica onde os sentidos de quem ver o Elfo são manipulados para não o perceberem. Isso é um pouco diferente de _Piscar_ (veja abaixo) por ser feita _antes_ do Elfo ir a um local onde pessoas podem o perceber (normalmente é feita antes de o Elfo ir a um local). Pode durar do anoitecer ao amanhecer (ou vice-versa), ou pode ser "derrubada" antes pelo Elfo
+ ___Piscar:___ quando percebido, um Elfo não pode pura e simplesmente usar Invisibilidade, isso funcionaria exatamente ao contrário. Nesse caso, a melhor solução é usar _Piscar_: essa magia faz com que o Elfo seja percebido como se estivesse "piscando", como se a pessoa o visse por uma televisão velha e falha. Isso dá tempo para o Elfo fugir e usar algo melhor
+ ___Ventriloquismo:___ às vezes, Elfos criativos se safam de serem percebidos usando sons ou vozes que eles imitem, usando essa magia para fazer com que tais sons pareçam vir de outra fonte, como de fora do quarto ou coisas do gênero. Mais de um Elfo deixou de ser notado por meio desse truque
+ ___Mover objetos:___ quando você precisa dar um jeito em um Malvado, muitas vezes um belo susto é a melhor opção. E nada como fazer uma toalha branca voar parecendo um fantasma para pregar o maior susto. Esse truque é bastante útil e muitas vezes é bem divertido também
+ ___Animar:___ às vezes, você pode precisar de ajuda para resolvler algum problema, mas não ter um amigo Elfo do seu lado (normalmente ocorre com os _Faz-Bem_). Para isso, usar essa Magia é bem útil: ao aplicado a alguma coisa (como um brinquedo), você coloca nele um "arremedo de vida". Ele não é exatamente esperto (No máximo um Aspecto Nominal em nível ___Regular (+1)___), mas é capaz de entender ordens e responder perguntas simples. Ele pode receber pequenas peculiaridades relacionadas à sua forma (um rato de brinquedo pode querer comer queijo ou sentir medo de gatos), mas a magia logo expira. É possível, com mais potência na magia (normalmente usando muito pirlimpimpim - +2 na dificuldade), aplicar um efeito mais "permanente" em algo. Isso deu origem, por exemplo, às lendas de bonecos de neve que se mexem para ajudar crianças.
+ ___Espremer-se:___ O Truque mais clássico do repertório da Magia Natalina, permite aquele que esteja sobre efeito dessa magia simplesmente ignorar qualquer barreira em uma localidade específica, entrando em qualquer lugar não importa o quão pequena seja a fretas. É na prática o que garante que o Papai Noel sempre consiga entrar em qualquer casa: suas Roupas são encantadas com essa Magia todos os anos, reforçando o poder da mesma.
+ ___Luzes:___ seja para criar pequenas luzes suaves e hipnóticas para fazer uma criança dormir melhor, fazer parecer que os fogos de artifício do Ano Novo começaram mais cedo para fugir, ou mesmo usar uma bela bomba luminosa na cara do gato que está tentando os pegar, Luzes é um truque bastante versátil no repertório de um Elfo;
+ ___Encolher/Crescer:___ muitos Elfos preferem se ocultar ficando bastante pequenos, ou pregam sustos em Malvados fazendo alguns brinquedos ficarem assustadoramente grandes! Essa magia é bastante interessante
+ ___Apagar Rastros:___ todo Elfo que valha seu chapéu já cometeu esse tipo de bobagem: entrou no banheiro e comeu um pouquinho da pasta de dente, ou roubou uma meia ou um lencinho das gavetas de uma criança. É compreensível, o gosto de menta é realmente algo que lembra a Vila de Natal, e as cores das meias e lencinhos também remetem à casa. Isso inclusive é um hábito bem comum entre os _Fazem-bem_. Para isso, a melhor forma de evitar problemas é Apagando seus Rastros para que ninguém veja pequenas pegadas élicas voltando para um Armário e coisas do gênero.
+ ___Aqui e Acolá:___ um truque interessante que Elfos usam para se deslocarem rapidamente de um local para o outro. Útil para sair do alcance de um Cachorro ou de um Malvado, mas já colocou mais de um Elfo em enrascada quando a magia ficou Destrambelhada e o arremessou para Timbuktu ou para dentro de um avião ou coisas ainda piores!
+ ___Enebular:___ OK... Você foi percebido, mas a criança aparenta estar sonolenta... A melhor coisa nesse caso é usar um pequeno truque de Enebular, fazendo a criança imaginar que tudo ainda é um sonho ou que ela está tão cansada que ela está vendo coisas que não existem. Simples e prático.
+ ___Esquecer:___ Simples e direta, apenas faz com que a criança esqueça a sua presença. Não é muito fácil de ser usada senão a for imediatamente após o Elfo ser notado.
+ ___Sugestionar:___ Bem... A coisa subiu no telhado e a criança te notou. Você pode usar Sugestão para fazer ela acreditar que imaginou você, como um amigo imaginário ou coisas do gênero. Ela não irá esquecer o evento, mas irá o ver por uma outra ótica. Ademais, se as pessoas soubessem o quanto de escritores de fantasia, em especial envolvendo o natal, foram sugestionadas quando crianças, elas ficariam realmente assustadas. (OBS: _Sugestionar_ é uma magia poderosa. Ela adiciona +2 à dificuldade básica da magia apenas pelo uso)
+ ___Geas:___ Esse truque é uma última opção, sendo que não é qualquer Elfo que sabe a usar. A pessoa tem uma mente forte e tudo mais deu errado. Usar um _geas_ é uma constrição poderosa: uma pessoa sobre um _geas_ automaticamente entra para a lista dos Malvados se tentar qualquer coisa contra Elfos ou o Papai Noel e isso pode colocar a mesma sob uma Maldição bem séria. Não é o tipo de coisa que se faça habitualmente. Há lendas terríveis sobre o que aconteceu com pessoas que romperam o _geas_, algumas dessas lendas sendo verdade. Creia-me... Não é uma história para Elfos que nem saíram direito das _poinsettias_! (OBS: _Geas_ é uma magia tão poderosa que ela adiciona +4 à dificuldade básica)

### Pirlimpimpim

Como dito anteriormente, _Pirlimpimpim_ é algo muito importante na Magia Natalina: toda magia envolve ao menos uma pitadinha de _Pirlimpimpim_. 

Uma coisa que pode acontecer é o Elfo recorrer a todo seu Estoque de Pirlimpimpim. Seja por não ter notado que estava no fim, seja porque ele deseja ___REALMENTE___ garantir que a Magia que ele vai usar vai funcionar, ele pode ficar ___SEM PIRLIMPIMPIM___.

Ficar ___SEM PIRLIMPIMPIM___ pode ocorrer em duas situações:

A primeira é no caso de Sucesso a Custo, como uma opção para evitar uma _Magia Destrambelhada_

A segunda é caso o Elfo deseje melhorar suas chances na Magia mas não tenha ou não queira usar Pontos de Destino.

Em ambos os casos, ele recebe uma Condição ___Sem Pirlimpimpim___ e, enquanto estiver sob essa condição, não pode utilizar Magia Natalina (como regra opcional, usar Magia Natalina ___Sem Pirlimpimpim___ adiciona +4 à dificuldade da Magia). Entretanto, ficar ___Sem Pirlimpimpim___ voluntariamente (ou seja, não como parte de um Sucesso a Custo) adiciona +4 ao rolamento da Magia.

> Syllia conseguiu encontrar Lanky e estão todos se preparando para ir embora. Entretanto, eles ainda tem que lidar com o fato de que Mariko sabe sobre eles. Syllia não é muito à favor disso, mas sabe que precisam fazer Mariko esquecer eles. Então, ela decide ao menos fazer com que isso seja um bom sonho, sem Sugestionar ou mesmo deixar Lanky aplicar um _Geas_ em Mariko (é uma criança, poxa vida!). Syllia então espera Mariko deitar em seu Futon e olha nos olhos dela dizendo: _"Bem... Espero que você lembre da gente em seus sonhos... Apenas não contem para ninguém. Tenha certeza que foi um sonho bom...."_ enquanto esvazia seu Saquinho de Pirlimpimpim sob Mariko.
> 
> A magia de _Enebular_ de Syllia não é fácil, já que Mariko está ciente de Syllia (+1) e acredita demais em Magia (+4), mandando a dificuldade para +7. Entretanto, como Mariko não apresentou nenhuma resistência especial, ela é considerada um alvo voluntário (-2), reduzindo a dificuldade para __*Excepcional (+5)*__. Syllia utiliza sua Competência enquanto ___Elfa do Clã Kapunki___ __*Bom (+3)*__, somando +4 ao ficar voluntariamente ___Sem Pirlimpimpim___, para __*Épico (+7)*__. Mesmo o rolamento não tão bom de ( )(-)( )( ) garante um resultado __*Fantástico (+6)*__, o que garante que as memórias de Mariko serão _enebuladas_: ela não vai esquecer os vários Elfos e como ela os ajudou a se reencontrar e descobrir o melhor caminho para a Vila de Natal, mas esse será um sonho tão bom e tão gostoso que Mariko jamais se esquecerá...
> 
> Essa é a certeza que Syllia tem ao deixar para trás a casa, uma lágrima escorrendo de seus olhos. Lanky se aproxima e diz: _"Não é uma escolha fácil, mas é o que deve ser feito, Syllia... Ao menos você a deixou com uma boa lembrança, e com certeza ela nunca vai deixar a Lista dos Bonzinhos... Para uma Kapunki, não acostumada ao Mundo Aqui Fora, você agiu muito bem. Estou orgulhoso, e o Papai Noel também ficará."_ Enquanto eles desaparecem com um truque de Lanky para eles chegarem logo na praça onde fica o Portal de volta para a Vila do Natal.

## Ganchos de Aventura

+ Gordot, o Chefe da Cozinha, quer preparar uma coisa especial para todos para depois da Grande Viagem do Papai Noel. Entretanto, ele precisa de ingredientes de todos os lugares do Mundo. Sem um Faz-Bem disponível para o Ajudar, ele recorre aos PCs.
+ Por algum motivo, o liquen mágico que faz as renas voarem está se escasseando nos Bosques mais próximos da Vila de Natal. Nörra, a Holhooja trigêmea, está procurando alguns Elfos que topem ir até um pico próximo, onde se imagina que possam conseguir mais liquen. Os demais Holhooja estão ocupados demais ajudando Kara a preparar as Renas para a Grande Viagem. Os PCs podem achar divertido sair um pouco da Rotina dentro da Vila de Natal
+ Nikäri, a Inventineira, conseguiu terminar um projeto muito especial, com a ajuda de sua gêmea Nöli, a Carteirista: agora os Fazem-Bem terão como enviar as informações dos Livros sobre as Crianças mais rapidamente. Entretanto, agora o problema é distribuir todas as Unidades dos Leitores dos Livros para algumas centenas de milhares de Fazem-Bem ao redor de todo o mundo. Ela certamente questionará os PCs por ajuda
+ Sanoma está preocupado: as previsões do tempo para a Viagem desse ano não estão confiáveis. A mudança climática tá bagunçando tudo. Ele propõe então que alguns Elfos dos demais Clãs atuem em conjunto com Fazem-Bem esse ano para garantir que o clima não seja um problema para a Grande Viagem
+ Tuvula conseguiu! Sua maior ambição, conseguir publicar um livro com todas as suas histórias sobre a Vila do Natal está quase concluída: uma pequena editora aceitou publicar seu Livro das Histórias dos Elfos do Natal. Entretanto, ela não pode se revelar como uma Elfa. Há quem diga que é possível que, por um tempo, um Elfo se passe por um humano comum, mas é algo arriscado, e nem Sanoma gosta de comentar sobre esse assunto. Como permitir isso? E como garantir que nenhuma informação mais sensível seja divulgada.
+ Lanky está com um problema bem sério: alguns Fazem-Bem não reportaram recentemente com as novas informações de seus Livros para a Vila de Natal. Isso está colocando todo o cronograma das listas em atraso! Eles não reportaram nenhuma tentativa de comunicação recente, e o pior é que uma das Corujas da Neve que foi enviada para saber o que está acontecendo, voltou ferida, com a asa quebrada. Quem poderia ser tão ruim a ponto de prejudicar uma ave? Seria um predador natural das corujas? Uma Criança Malvada? Ou coisa ainda pior?
+ Ajatus, o Inventineiro, precisa de algo simples: alguém precisa pegar algumas encomendas que ele fez com novos materiais e livros com ideias para suas invenções. Parece não ser algo muito complicado, então nada demais para os personagens resolver, certo?

## Personagens Exemplo

### Sören, o Carteirista

#### Aspectos 

+ ___Eu sou um Elfo do Clã Carteirista___ _Bom (+3)_
+ ___Eu sou um Elfo Responsável e Discreto___ _Regular (+1)_
+ ___Eu sou conhecido por ser tão organizado que faço pilhas gigantes com as cartas vindas do Mundo Lá Fora___ _Regular (+1)_
+ ___Eu desejo Conhecer melhor as pessoas que ajudamos___ _Razoável (+2)_
+ ___Eu gosto de ter momentos de paz e sossego, como os que Syllia provê quando conto histórias___ _Razoável (+2)_

#### Façanhas

+ **Façanha Exclusiva:** ___Histórias de Vida___ - +2 ao reconhecer a necessidade de uma pessoa baseando-se em alguma coisa que já leu
+ ***Todos mantenham a calma, por favor!*** - _uma vez por sessão_, pode remover um Aspecto relacionado a bagunça, desordem, medo ou qualquer outra forma de caos que tenha se instaurado, sem necessidade de rolamento

### Timmennen, a Construtora

#### Aspectos

+ ___Eu sou uma Elfa do Clã Construtor___ _Bom (+3)_
+ ___Eu sou uma Elfa Esperta e Sonhadora___ _Razoável (+2)_
+ ___Eu sou conhecida por ser muito detalhista ao construir brinquedos___ _Regular (+1)_
+ ___Eu desejo desenvolver todo um estilo ao criar brinquedos___ _Razoável (+2)_
+ ___Eu gosto de desenhar croquis de brinquedos, novos ou estilizados___ _Regular (+1)_

#### Façanhas

+ **Façanha Exclusiva:** ___Tenho o que precisamos!___ - +2 ao construir alguma coisa útil para _Superar_ algum obstáculo
+ ***Desenhos Detalhados:*** - _uma vez por sessão_, posso fazer um ___Desenho Detalhado___ de qualquer coisa, baseado nas descrições das pessoas, tão detalhado que ele pode ser útil. Esse ___Desenho Detalhado___ entra em jogo com um Uso Gratuito, sem necessidade de testes

### Selyse, o Inventineiro

#### Aspectos

+ ___Eu sou um Elfo do Clã Inventineiro___ _Bom (+3)_
+ ___Eu sou um Elfo Austero e Focado___ _Razoável (+2)_
+ ___Eu sou conhecido por remontar qualquer coisa que por um acaso exploda na minha mão___ _Regular (+1)_
+ ___Eu desejo criar coisas que não se destruam, não importa o quanto você as castigue___ _Regular (+1)_
+ ___Eu gosto de desenhar croquis de brinquedos, novos ou estilizados___ _Razoável (+2)_

#### Façanhas

+ **Façanha Exclusiva:** ___Tenho uma ideia!___ - +2 ao ___Criar Vantagens___ envolvendo mecanismos 
+ ***Mestre de Quebra Cabeças:*** - sou um mestre dos quebra-cabeças, então recebo +2 ao tentar _Superar Obstáculos_ envolvendo charadas e quebra-cabeças

### Fësto, o Faz-Bem

#### Aspectos

+ ___Eu sou um Elfo do Clã Faz-Bem___ _Bom (+3)_
+ ___Eu sou um Elfo Alegre e Otimista___ _Razoável (+2)_
+ ___Eu sou conhecido por não ter quase nenhuma criança na Lista dos Malvados___ _Razoável (+2)_
+ ___Eu desejo chegar no nível de ser o Chefe dos Faz-Bem no Mundo Lá Fora___ _Regular (+1)_
+ ___Eu gosto de mostrar como o Mundo Lá Fora é interessante___ _Regular (+1)_

#### Façanhas

+ **Façanha Exclusiva:** ___O Livro!___ - cada Faz-Bem possui um Livro onde registra todas as coisas boas (ou não) que aquelas crianças que ele observa fez (e demais pessoas próximas). +2 para tentar prever o comportamento dessa criança ou entender mudanças de comportamento.
+ ***Eu já estive aqui!*** - +2 ao _Superar Obstáculos_ relacionados a locais ou pessoas estranhas

### Yilliä, a Holhooja

#### Aspectos

+ ___Eu sou uma Elfa do Clã Holhooja___ _Bom (+3)_
+ ___Eu sou uma Elfa Tranquila e Desbravadora___ _Razoável (+2)_
+ ___Eu sou conhecida por ser capaz de aguentar qualquer clima, não importa o quão duro___ _Regular (+1)_
+ ___Eu desejo explorar os locais selvagens do Mundo Lá Fora e registrar tudo___ _Regular (+1)_
+ ___Eu gosto de sentar perto de uma fogueira que eu mesmo fiz, e comer com meus amigos___ _Razoável (+2)_

#### Façanhas

+ **Façanha Exclusiva:** ___Falar com Animais (Literalmente):___ Holhooja são adeptos em falar com todo tipo de animal selvagem ou domestico. Portanto, podem sempre conversar com eles. Entretanto, os animais não saberão responder nada que não saibam ou não entendam (por exemplo, sobre magia ou coisas tecnológicas)
+ ***Aqui está muito bom!*** - ao acampar ou preparar um local para descanso, não importa o quão ruim seja, não pode ter nenhum Aspecto relacionado ao Ambiente forçado contra ela ou seus amigos, se tiver sido bem-sucedida em tal ação.

### Kypip, o Elfo em Treinamento

#### Aspectos

+ ___Eu sou um Elfo em Treinamento___ _Bom (+3)_
+ ___Eu sou um Elfo Curioso e que não consegue parar quieto___ _Razoável (+2)_
+ ___Eu sou conhecido por tentar o melhor que pode sair das enrascadas em que me enfio___ _Regular (+1)_
+ ___Eu desejo provar que posso ser um bom Elfo de qualquer clã___ _Regular (+1)_
+ ___Eu gosto de aprender todas essas coisas novas___ _Razoável (+2)_

#### Façanhas

+ **Façanha Exclusiva:** ___Ainda aprendendo___ - Pode utilizar as Façanhas de todos os clãs. Entretanto, só podem utilizar cada uma delas _uma vez por sessão_, e com um bônus reduzido para +1. 
+ **Talento Potencial:*** Escolha uma Façanha Exclusiva que você tenha usado nessa sessão. Você pode a usar novamente ao custo de 1 Ponto de Destino, apenas uma única vez adicional, mas com bônus total.

### Syllia, a Kapunki

#### Aspectos

+ ___Eu sou uma Elfa do Clã Kapunki___ _Bom (+3)_
+ ___Eu sou uma Elfa Sabichona e Confiável___ _Razoável (+2)_
+ ___Eu sou conhecida por Saber onde estão todos os Livros e Cartas catalogados na Biblioteca do Papai Noel___ _Razoável (+2)_
+ ___Eu desejo Poder ler tudo o que for possível sobre O Mundo Lá Fora___ _Regular (+1)_
+ ___Eu gosto de Conversar sobre o Mundo Lá Fora com Sören___ _Regular (+1)_

#### Façanhas

+ **Façanha Exclusiva:** ___Identificando Padrões___ - Kapunki costumam ser elfos que percebem quando algo está fora de lugar. +2 ao _Superar_ obstáculos, reconhecendo padrões que mostrem  coisas que estão fora do lugar
+ ___Sabe Onde Tudo Fica na Vila de Natal:___ Syllia conhece tudo sobre a Cidade de Natal, recebendo +2 ao Superar Obstáculos ao se deslocar dentro da Vila. Entretanto, se falhar no Teste, ela se enfiou em algum lugar onde não deveria ou arrumou alguma outra enrascada!!!


<!--  LocalWords:  Portable North liquen Sanoma poinsettias Fazem-Bem
 -->
<!--  LocalWords:  poinsettia Nörra Holhooja Nikäri Inventineira Nöli
 -->
<!--  LocalWords:  Inventineiros Fazem-bem Céu-Abertos pirlimpipim
 -->
<!--  LocalWords:  Kapunki Vilarejinos Shibuya Mariko Henge Timbuktu
 -->
<!--  LocalWords:  Enebular OBS Futon enebuladas Kara Tuvula Ajatus
 -->
<!--  LocalWords:  Inventineiro Timmennen Selyse Fësto Yilliä Kypip
 -->
