---
layout: page
show_meta: false
title: "RPG-A-Day 2019"
fate: 2016-08-08
teaser: "Minhas opiniões sobre RPG desse ano"
header:
  image_fullwidth: FundoBlog.png
tags:
  - rpg-a-day
permalink: "/rpg-a-day/2019/"
---

RPG-a-Day é uma proposta do blog [Autocratik][1]  para postagens sobre RPG durante o mês de agosto, sobre assuntos específicos. Os assuntos sugeridos para este mês são

[![As 31 perguntas do RPG-A-Day 2019](/assets/img/rpg-a-day-2019.jpg)](/assets/img/rpg-a-day-2019.jpg) a [tradução online das mesmas][2]

Essa semana, as "perguntas" são abertas, baseadas em palavras para cada dia

Então, vamos nessa!

### 1. Primeiro

Bem... Aqui fica meio sem dúvida sobre o que dizer.

Meu primeiro RPG, foi o velho e bom _RPG - Aventuras Fantásticas_ (em Inglês _Fighting Fantasy_). Um livrinho pequeno, bacana, simples, e muito provavelmente a única (ou uma das únicas) experiências realmente _Old School_ que chegou a ser realizado no Brasil. No Brasil apenas não veio o livro Allansia, que era o "quarto" livro básico dos RPGs, que era composto pelo _RPG - Aventuras Fantásticas_ e dos três livro do _Aventuras Fantásticas Avançadas_: Dungeoneer, Blacksand! e Allansia.

E ele era lindinho

[![Aventuras Fantásticas](/assets/img/aventurasFantasticas2.jpg)](/assets/img/aventurasFantasticas2.jpg)

### 2. Único

OK... Aqui vou comentar o que acho de único em Fate.

Fate herdou coisas de seu pai, o Fudge, e as aperfeiçoou.

Acho muito bom como os Aspectos funcionam como uma _ponte_ que interliga a narrativa e interpretação, o cenário e as mecânicas, e como isso faz com que, através dele, as narrativas ganhem uma _importância legítima_ e sejam bem usada.

Gosto muito do Fractal, pois isso permite que qualquer subsistema que você precise, graças a alguma ideia engraçadinha dos jogadores, possa ser rapidamente preparada.

E gosto de todos os cenários que saem e saíram para ele, que mostram coisas bem fora da caixa, ou coisas tradicionais, mas com seu próprio _twist_.

### 3. Engajar

Eu procuro ajudar no engajamento, escrevendo, levando mesas e fazendo o [Fate Masters][3], além do [em hiato Rolando +4][4] e de outras iniciativas, como o [Mesas Predestinadas][5]. Mas acho que não dá para esperar que apenas 1 se engaje, senão é como tentar levar o oceano de um lado a outro com um baldinho de praia.

### 4. Compartilhar

Acho que precisamos compartilhar tudo o que podemos. A essência do RPG é compartilhar... Tempo, referências, ideias...

### 5. Espaço

O melhor espaço real para jogar RPG certamente é na Omniverse, durante a Dungeon Geek. Quanto a espaços virtuais, Discord é seu amigo!

### 6. Emocional

Estou apelando e trocando _Antigo_ para _Emocional_

Acho que o jogo de RPG deve gerar algum vínculo emocional sim. De outra forma, corre-se o risco de eliminar dele a única vantagem que ele tem e torná-lo apenas um jogo qualquer. Não é necessário ser uma coisa como em um psicodrama, mas o vínculo emocional é importante sim.

### 7. Familiar

Jogos Familiares.

Acho que o RPG pode gerar isso, que é tão necessário no nosso tempo: um momento ___de qualidade___ para famílias.

### 8. Obscuro

Sobre Obscuro...

Acho que uma das ideias mais obscuras que tive foi trazer minha própria releitura/homenagem/cópia da Green Meadows de _Nossa Turma_ para Fair Leaves. Essa ideia um pouco perdida de que pode-se ter um momento onde o mais importante é maravilhar-se com as pequenas coisas da vida do que tentar salvar o mundo. Por isso Fate é tão bom para ele, pois o Fate parte da premissa que você _Não precisa salvar o mundo todos os dias._

![[A Nossa Turma](/assets/img/GetAlongGang.jpg)](/assets/img/GetAlongGang.jpg)

### 9. Crítico

Minha mão ruim é lendária:

Ao menos duas vezes usei três pontos de Destino para rerolagem, e consegui -4, -3 e -3.

Entretanto, uma certa vez critei um Beholder em AD&D com um 20 natural...

### 10. Foco

Uma das maiores dificuldades é garantir o foco dos jogadores no jogo. Entretanto, no meu caso particular, parto da premissa de que ele não vai se importar se algo sair errado com seu personagem por ele não estar focado.

### 11. Examinar

Examinar...

Sempre examine o seu jogo.

Examine seus jogadores.

Examine a você mesmo.

Se alguma dessas coisas não estiver correta e não garantir a diversão para todos, você está errando feio, e deve corrigir.

### 12. Amizade

A parte mais importante no meu jogo _Fair Leaves_ é a amizade: nenhum deles consegue (ou deveria) resolver tudo sozinho. Para isso, procuro enfatizar muito em mesa a regra de _Ajuda passiva_ do Fate (cada personagem que não agir naquele turno pode oferecer +1 no rolamento de outro personagem). Além disso, em termo de design, quando criei a ideia de Façanhas Exclusivas, eu quis enfatizar algo que cada personagem poderia utilizar para ajudar o grupo, como o Certinho que pode usar sua Façanha _Não fui eu!_ para mostrar que não foram os personagens que aprontaram algo.

### 13. Mistério

Um mistério...

Acho que mistérios são sempre legais, mas o maior mistério para mim é como existe essa tendência de tentar "terroficar" a infância, a fantasia.

Já temos problemas demais como adultos, não precisamos destruir isso.

### 14. Guia

Uma linha guia que desenvolvi partindo do Fate: _"em caso de conflito entre Narrativa e Regras, a narrativa vem primeiro"_. Isso não é o mesmo que a _Regra de Ouro_ (e seu abuso): é necessário que você conheça as regras, até para saber quando você pode burlar as mesmas. Mas sim dar predileção à história que está sendo contada.

### 15. Porta

RPGs são portas de entrada para a imaginação: deixe a coisa fluir. Você pode sim ter sua linha guia ao construir uma aventura, mas procure manter-se aberto a modificar as situações: nem tudo é A-B-C, você pode tentar modificar as ordens de eventos. Mantenha as portas da mente abertas, e você verá como tudo será bem melhor

### 16. Sonho

O Sonho é algo que você deve sempre aproveitar: deixe o Lírico do Sonho fluir. Não apenas o assustador, mas o lírico.

Em _Nunarihyon no Mago_, a definição para a fonte do poder dos Yokai é chamada de _Osore (畏)_ em Japonês, traduzida para o português apenas como _Pavor_. Na prática, esse pavor não vem necessariamente do medo, mas da percepção da infinitude, de beleza e terror que existe além da compreensão do ser humano.

Essa é uma fonte importante para o RPGista: descobrir o contato com essa forma de ver o mundo.

### 17. Um

Acho que é sempre bom investir seu tempo, em especial como Narrador, em um ou poucos sistemas. Isso permite que você consiga arbitrar melhor o jogo na parte de regras, determinar o feeling e ritmo do jogo na narrativa, e auxiliar os jogadores nas regras.

No meu caso, sem sombra de dúvidas meu foco tem sido no Fate, além de alguns saltos ocasionais em sistemas como _Castelo Falkenstein_ e _Good Society_

### 18. Muitos

Por outro lado, sempre que possível experimente coisas diferentes, em jogos que você não domine. Existem muitos jogos por aí. É sempre interessante ler e jogar coisas diferentes, pois você acaba pegando nuances que você não percebeu anteriormente, mas que podem ser interessante de se trazer ao seu jogo.

No meu caso, mantenho uma curiosa regra de evitar _jogar Fate enquanto jogador_. Isso até evita que eu, como jogador, estrague a diversão ao tentar, mesmo que inconscientemente, potencializar ações por meio do conhecimento de regras nos piores momentos.

### 19. Assustador

O que é assustador?

Mais que Cthulhu?

É a capacidade do Jogador de jogar pela janela seus planos, para o bem e para o mal.

Em alguns casos, isso é maravilhoso, pois eles apresentam para você uma nova rota que você não tinha visualizado no seu jogo.

Em outras, é simplesmente aquele momento de desespero onde toda a sua preparação para uma aventura bacana desce pela privada porque você descobre que sua mesa é composta apenas de _murderhobos_.

Isso deve manter em sua mente que TODOS na mesa tem direito de se divertir, INCLUINDO VOCÊ!

### 20. Nobre

É nobre o sentimento de narrar uma boa aventura.

É nobre ceder controle no momento certo.

Mas é nobre também não deixar sua aventura (e portanto sua diversão) ser pura e simplesmente destruída porque os jogadores são hostis à sua proposta.

### 21. Vasto

Possibilidades vastas de história... Não importa se a história começa no mesmo ponto, e não importa se você quer que ela chegue no mesmo ponto. Primeiro, isso pode não acontecer, já que as ações dos personagens sempre puxam a história para outros. Segundo, mesmo que ocorra, certamente o caminho será diferente.

É interessante que isso quer dizer que você, enquanto narrador, tem que estar preparado a aprender e improvisar e a aceitar incluir essa opção para usos futuros.

Algumas das melhores histórias que criei envolveram essa troca de vastidões de opções.

### 22. Perdido

Quando você está perdido, e quer chegar em qualquer lugar, qualquer caminho serve. Uma sábia lição dada por um certo Gato a uma certa menina em um certo País das Maravilhas.

### 23. Surpreso

Eu não fico mais muito surpreso com as ações dos jogadores, nem com as possibilidades de histórias interessantes. Quando você cria um culto bizarro de vilões chamados _Riachuelo_ e você enfrenta brócolis mutantes com a versão Zootopia da Besta Fera de Caerbannog... Nada mais lhe impressiona.

Ou melhor... Tudo te impressiona de imediato, mas você já encara o que vier.

### 24. Triunfo

Violência é triunfo? Destruir o adversário é triunfo?

Acredito que não.

Vencer é alcançar uma superioridade reconhecida até pelos seus opositores.

Como dito em _A Arte da Guerra_: _"Derrotar o inimigo em cem batalhas não é a excelência suprema; a excelência suprema consiste em vencer o inimigo sem ser preciso lutar."_

### 25. Calamidade

Calamidade...

Quando tudo descamba, quando você se vê em uma situação onde tudo se destrói.

Quando tudo vira terra devastada, histórias de insensatez e destruição egoística e niilista.

Isso é a calamidade.

### 26. Ideia

Sempre se pergunte, ao invés de dizer _"Essa ideia é idiota!"_, _"Essa ideia é_ realmente _idiota!"_

Porque, na realidade, muitas das boas ideias aparentam ser idiotas, até que você as coloca em uso.

Lembre-se que mesmo os gênios erram quando fecham a porta para ideias.

Em algum momento _"640kb eram suficientes para qualquer um"_

### 27. Suspense

Suspense...

Em mesa, o suspense para mim é:

_"Como os jogadores irão me surpreender?"_

Será dessa vez que os jogadores irão transformar uma torre de energia em um mini-Sol?

Será dessa vez que os jogadores vão enfrentar brócolis zumbis gerados por lasanha energizada por radiação espacial?

Qual será a surpresa?

### 28. Amor

Amor pelo hobby do RPG. Simples assim. Algo que jogo e faço desde a infância.

Amor pelo ato de contar histórias.

### 29. Evolução

Cada aventura gera XP (ou melhor, _Marcos_)

E não só para os personagens, mas também para os jogadores e, acima de tudo, para o Narrador.

Pois você sempre acaba aprendendo novos truques, se você se mantiver aberto a opções.

### 30. Conexão

Não importa o quanto você jogue, você nunca esquece aquelas conexões que surgiram durante uma mesa de jogo.

Mesmo que isso se torne uma _Lenda Lendária_, um episodio anedótico que você mantenha na cabeça como uma história a contar e dar umas risadas ocasionais, a conexão vai existir.

Criar algo junto da magnitude de uma história em uma aventura de RPG é impossível sem gerar uma conexão com todos os elementos envolvidos.

Os jogadores, os personagens, o narrador, o cenário, o sistema.

Você se conecta, invariavelmente.

E isso demanda responsabilidade.

### 31. Último

Nunca acaba.

Aquele jogo é o último, até não mais ser.

Sempre terá algo novo.

O fim de uma saga começara uma nova.

Até a morte não é o fim.

[1]: http://autocratik.blogspot.com/2019/07/launching-rpgaday-2019.html
[2]: http://www.dungeongeek21.com/rpgaday-2019-trivia-do-rpg-deste-ano/
[3]: https://fatemasters.gitlab.io
[4]: https://rolandomaisquatro.gitlab.io
[5]: https://mesaspredestinadas.gitlab.io
