---
layout: page
show_meta: false
title: "RPG-A-Day 2018"
fate: 2016-08-08
teaser: "Minhas opiniões sobre RPG desse ano"
header:
  image_fullwidth: FundoBlog.png
tags:
  - rpg-a-day
permalink: "/rpg-a-day/2018/"
---

RPG-a-Day é uma proposta do blog [Autocratik][1]  para postagens sobre RPG durante o mês de agosto, sobre assuntos específicos. Os assuntos sugeridos para este mês são

[![As 31 perguntas do RPG-A-Day 2017](/assets/img/rpgaday2018.png)](/assets/img/rpgaday2018.png) a [tradução online das mesmas][2]

Nesse ano, as perguntas são divididas em blocos.

Então, vamos nessa!

### Primeira semana: O QUE...

#### 1. ... você ama sobre RPG’s?

Criar histórias interessantes e divertida em conjunto. Uma mesma aventura que eu preparo gera histórias diversas

#### 2. ... procura em um RPG?

Temas diferentes, regras flexíveis e concisas

#### 3. ... dá ao jogo “poder de ficar”?

Ter algo fantástico, que permita que você se maravilhe, mesmo que se passe na vida real

#### 4. Qual NPC mais memorável?

O aluno de 1° Ano da Lufa-Lufa, Hans Guntenmeyer, o mais novo de quadrigêmeos que salvaram o grupo de personagem em minha campanha de Harry Potter, ao mostrar-se como um Animago em Treinamento e, transformando-se em coelho, conduziu o grupo por meio de um ataque de Comensais da Morte até o salão comunal da Lufa-Lufa e com isso os colocou em segurança por tempo o bastante para eles prepararem um contra-ataque.

#### 5. Qual o NPC recorrente mais memorável?

Acho que é foi o pequeno herói genial Joshua McCarthy, conhecido como Grilo Falante, em aventuras de _Wearing the Cape_

### Segunda semana: COMO...

#### 6. ... jogadores podem fazer um mundo parecer real?

Mantenha o cenário vivo, e coloque coisas simples: faça os personagens caminharem no meio de feiras, parar para comer algo em uma barraca na estrada (não uma taverna, literalmente uma barraca), comprar suprimentos comuns, ouvir os detalhes da moda local, músicas e baladas simples. Enfim, detalhes pequenos que, de outra forma, desapareceriam. Descreva isso dentro da ótica dos jogadores e deixe que eles então sintam o mundo pulsar.

#### 7. ... um Mestre (GM) pode tornar as apostas importantes?

Coloque consequências, mesmo que não visíveis no curto prazo. Nada deve sair incólume. Guarde cada decisão dos jogadores e faça com que elas tenham importância a curto, médio e longo prazo. A menina que eles não salvaram dos goblins pode ser o estopim de uma guerra genocida

#### 8. ... podemos fazer para fazer mais pessoas jogarem RPG?

1. Criar e oferecer sistemas mais baratos ou mesmo de graça
2. Oferecer espaços seguros onde as pessoas podem jogar sem medo, independentemente de quem sejam

#### 9. ... um jogo te surpreendeu?

+ ___Positivamente:___ Black Hack. Muito funcional para o que eu esperava de um jogo Old-School e que criou soluções viáveis e simples para problemas sérios de _accounting_ no jogo na minha opinião
+ ___Negativamente:___ DCC. Achei um pouco supervalorizado. Além disso, na minha opinião pessoal, a dependência excessiva de tabelas é coisa de Narrador preguiçoso.

#### 10. ... jogar te mudou?

Me tornou uma pessoa mais aberta às coisas, ao mesmo tempo que me colocou a parte de saber bem o que eu gosto e estar disposto a mostrar elas

#### 11. Qual o nome de personagem mais ousado?

Eron Sweetheart - meu bobo da corte de Tormenta clássico

#### 12. Conceito mais ousado de um personagem?

O mesmo acima, um bobo da corte em um cenário medieval que seguiu uma aventura após ser ressuscitado

### Terceira semana: DESCREVA...

#### 13. ... como seu jogo evoluiu.

Eu consegui me abrir a ideias mais novas baseadas em jogos mais narrativos

#### 14. ... um fracasso se tornou surpreendente.

Quando alguns jogadores nukaram uma aventura minha de supers por acharem que "podiam tudo". Me mostrou que não posso também ter muita paciência e tenho que fazer o jogador pagar pelas suas ações. Isso não tem nada a ver com tirar a agência, mas com manter a coerência do mundo.

#### 15. ... uma experiência de RPG complicada que você gostou.

Utilizar o Spark para uma sessão de criação de cenário, personagens e aventura em 4 horas.

#### 16. ... seus planos para o próximo jogo.

Sem muitos planos, exceto jogar _Shadow of the Century_

#### 17. ... o melhor elogio que recebeu enquanto jogava.

Ser um narrador flexível, capaz de fazer as coisas acontecerem

#### 18. Que arte inspira seus jogos?

De tudo... Exceto aquilo mais "vanilla" (com a exceção de Tolkien)

#### 19. Que música engrandece seus jogos?

Música Eletrônica ou clássica

### Quarta semana: QUAL ...

#### 20. ... mecânica do jogo te inspira durante o jogo?

Pontos de Destino. Sabendo usar direitinho, ela favorece de maneira mecânica quem interage bem de maneira narrativa e vice versa

#### 21. ... mecânica de dado é mais atrativo para você?

Dados Fate. Viva o Desvio Padrão!

#### 22. ... sistema sem dados é mais atrativo para você?

_Good Society_. Uma ideia de cenário muito boa e que parece funcionar muito bem

#### 23. ... jogo você espera jogar novamente?

Castelo Falkenstein. Ele é muito lindo

#### 24. ... RPG merecia ter um reconhecimento maior, de acordo com sua opinião?

Castelo Falkenstein. Ele é pensado para e consegue entregar uma verdadeira experiência, desde a ideia do cenário ser descrito internamente quando a metalinguagem e o furo de 4a parede

#### 25. Diga um jogo que te impactou este ano.

_Uprising_ - originalmente não dei nada, até pelo gênero não ser bem o que eu gosto, mas me surpreendi positivamente

#### 26. Sua ambição de jogos para o ano que vêm.

Cada vez mais Fate

### Quinta semana: COMPARTILHE...

#### 27. ... um ótimo stream / jogo real.

Sinceramente, não tenho nenhuma em especial

#### 28. ... cuja excelência em jogos você é grato por.

+ Ao Stephan O'Sullivan pelo Fudge
+ Ao Mike Pondsmith por Castelo Falkenstein
+ Ao Fred Hicks e o Rob Donaghue pelo Fate
+ Ao Igor Moreno por Bukatsu!
+ À Carrie Harrie e a Amanda Valentine pelo Young Centurions
+ Ao Mark Diaz-Truman pelo Do

#### 29. ... uma amizade por causa dos RPGs.

Sinceramente... Não consigo especificar apenas um nome, mas todas as amizades me são queridas

#### 30. ... algo que você aprendeu sobre jogando seu personagem.

Nunca desistir... Recuar se necessário, mas aí é para pegar o impulso para entrar na voadora!

#### 31. ... por que faz parte do RPG A DAY.

Porque é minha forma de agradecer à comunidade do RPG.

[1]: https://autocratik.blogspot.com/2018/07/announcing-rpgaday2018.html
[2]: https://www.dungeongeek21.com/bg/rpgaday
