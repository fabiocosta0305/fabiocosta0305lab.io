---
layout: page
show_meta: false
subheadline: "Uma Introdução ao Fate, com os segredos que o MB não fala!"
title: "Introdução ao Fate"
permalink: "/intro-fate/"
---

Originalmente publicado no site da [Dungeon Geek](https://www.dungeongeek21.com.br)
       
<ul>
    {% assign artigos = site.tags.introducao-ao-fate | sort: 'order'  %}
    {% for post in artigos %}
    <li><a href="{{ post.url }}">{{ post.title | markdownify | remove: '<p>' | remove: '</p>' }}</a></li>
    {% endfor %}
</ul>

