---
layout: page
show_meta: false
title: "Exemplo de Jogo"
permalink: "/ExemploJogo/"
---

Exemplos de jogo fictício que criei para facilitar a compreensão tanto do Fate como um todo como de certos cenários específicos do meu agrado

<ul> 
    {% for post in site.tags.ExemploDeJogo %}
    <li><a href="{{ post.url }}">{{ post.title | markdownify | remove: '<p>' | remove: '</p>' }}</a></li>
    {% comment %}
    {{ post.content }}
    {% endcomment %}
    {% endfor %}
</ul>

